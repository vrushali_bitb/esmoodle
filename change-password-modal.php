<?php 
session_start();
if(isset($_SESSION['username'])) {
    $user 	= $_SESSION['username'];
	$role 	= $_SESSION['role'];
	$userid = $_SESSION['userId'];
}
else {
    header('location: index.php');
} ?>
<style type="text/css">

.js-hidden {
  display: none;
}
.tooltip{
		position: absolute;
	}
</style>
<div id="load_popup_modal_contant" class="" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Change Password</h4>
            </div>
            <div class="modal-body">
            	<form id="change_pwd_form" name="change_pwd_form" method="post" class="smart-form client-form password-strength">
                	<input type="hidden" name="change_pwd" value="1" />
                    <div class="usermanage-form">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                	<label for="password-input">New Password: <span class="d-inline-block" tabindex="0" data-toggle="tooltip" title="Add 8 charachters or more, lowercase letters, uppercase letters, numbers and symbols to make the password really strong!"><i class="fa fa-info-circle" aria-hidden="true"></i></span></label>
                                    <div class="register_password1">
										<input class="password-strength__input form-control" type="password" id="npwd" name="npwd" placeholder="Password" required="" autocomplete="off">
										<div class="input-group-append chANGEpass">
											<button class="password-strength__visibility btn btn-outline-secondary" type="button">
											<span class="password-strength__visibility-icon" data-visible="hidden"><i class="fa fa-eye-slash"></i></span>
											<span class="password-strength__visibility-icon js-hidden" data-visible="visible"><i class="fa fa-eye"></i></span></button>
										</div>
									</div>									
									<!-- <input class="password-strength__input form-control" type="password" id="npwd" name="npwd" required autocomplete="off" />
                                    <div class="input-group-append">
                                      <button class="password-strength__visibility btn btn-outline-secondary" type="button">
                                      <span class="password-strength__visibility-icon" data-visible="hidden"><i class="fa fa-eye-slash"></i></span>
                                      <span class="password-strength__visibility-icon js-hidden" data-visible="visible"><i class="fa fa-eye"></i></span></button>
                                    </div> -->
                                    <small class="password-strength__error text-danger js-hidden">This symbol is not allowed!</small>
                                </div>
                                <div class="password-strength__bar-block progress1 mb-4">
                                  <div class="password-strength__bar progress-bar bg-danger" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="password-input">Confirm password: <span class="d-inline-block" tabindex="0" data-toggle="tooltip" title="Add 8 charachters or more, lowercase letters, uppercase letters, numbers and symbols to make the password really strong!"><i class="fa fa-info-circle" aria-hidden="true"></i></span></label>
                                    <div class="input-group" id="show_hide_password1">
                                        <input type="password" class="form-control" id="rpwd" name="rpwd" required autocomplete="off" />
                                        <div class="input-group-addon">
                                        	<a href="javascript:void(0);"><i class="fa fa-eye-slash" aria-hidden="true"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="usermanage-add clearfix">
                    	<button type="submit" name="change_pwd" id="change_pwd" class="btn btn-primary password-strength__submit btn" disabled="disabled">Submit</button>
                    </div>
                </form>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>
<script src="content/js/pwd-script.js"></script>
<script type="text/javascript">
$(function () {
  $('[data-toggle="tooltip"]').tooltip();
})
$("#show_hide_password1 a").on('click', function(event) {
	event.preventDefault();
	if($('#show_hide_password1 input').attr("type") == "text"){
		$('#show_hide_password1 input').attr('type', 'password');
		$('#show_hide_password1 i').addClass( "fa-eye-slash");
		$('#show_hide_password1 i').removeClass( "fa-eye");
	}else if($('#show_hide_password1 input').attr("type") == "password"){
		$('#show_hide_password1 input').attr('type', 'text');
		$('#show_hide_password1 i').removeClass( "fa-eye-slash");
		$('#show_hide_password1 i').addClass( "fa-eye");
	}
});
$("#change_pwd_form").on('submit', (function(e) {
	e.preventDefault();
	$.LoadingOverlay("show");
	$.ajax({
		url: 'includes/process.php',
		type: "POST",
		data: new FormData(this),
		contentType: false,
		cache: false,
		processData:false,
		success: function(resdata) {
			var res = $.parseJSON(resdata);
			if (res.success == true) {
				$.LoadingOverlay("hide");
				swal({text: res.msg, buttons: false, icon: "success", timer: msgtimer});
				setTimeout(function() { window.location.href = res.return_url; }, time);
			}
			else if (res.success == false) {
				$.LoadingOverlay("hide");
				swal({text: res.msg, buttons: false, icon: "error", timer: msgtimer});
			}
		}, error: function() {
			$.LoadingOverlay("hide");
			swal({text: oopsmsg, buttons: false, icon: "error", timer: msgtimer });
		}
	});
}));
</script>
<?php 
ob_end_flush();
