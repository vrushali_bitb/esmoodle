<?php 
ob_start();
session_start();
if (isset($_SESSION['username'])) {
  $user     = $_SESSION['username'];
  $role     = $_SESSION['role'];
  $userid   = $_SESSION['userId'];
  $cid	    = (isset($_SESSION['client_id'])) ? $_SESSION['client_id'] : exit('Oops. something went wrong please try again');
} else {
  header('location: index.php');
}
require_once 'config/db.class.php';
require_once 'includes/learners_menu.php';
$db = new DBConnection; ?>
<style>
  admin-dashboard.Ev2 .supDASHTOP {
      margin: 0px;
  }
  .cls-1 {
      stroke: transparent !important;
  }
  .flexRI.client.clients {
      display: flex;
  }
  .owl-nav {
    display: none;
}

.owl-nav {
		display: none;
	}

	.owl-dots {
		display: block;
		margin-top: 5px;
	}
	.owl-theme .owl-dots .owl-dot.active span{		
		background: #000 !important;
	}

	.owl-theme .owl-dots .owl-dot span {
		width: 17px !important;
		height: 17px !important;
		border: 1px solid #000;
		background: transparent !important;
		border-radius:50% !important;
	}
</style>
<link rel="stylesheet" href="content/js/daterangepicker/daterangepicker.css" />
<link rel="stylesheet" href="content/css/owlcarousel/owl.carousel.min.css" />
<link rel="stylesheet" href="content/css/owlcarousel/owl.theme.default.css" />
<div class="admin-dashboard Ev2">
  <div class="container-fluid">
    <div class="L-calender">
      <div class="input-group calenderclient col-sm-3">
        <input type="text" id="reportrange" class="form-control" readonly="readonly" />
        <span class="input-group-addon"><span class="fa fa-calendar"></span></span></div>
    </div>
    <div class="flexItem flexItemsdMinDash AutHorDahBan">
      <div class="flextlistbaner hometab clearfix owl-carousel owl-theme active">
        <div class="item">
          <div class="flextlistitem">
            <div class="supDASHTOPtextB">              
              <div class="flexRI">
                <p class="noofsubtextno"><?php echo $db->getReviewerNewSim($userid); ?></p>
              </div>
              <div class="flexRI client">
                <p class="noofsubtext">REVIEW SIMULATIONS</p>
              </div>
            </div>
          </div>
        </div>
        <div class="item">
          <div class="flextlistitem SIMulationT1">
            <div class="textFlex CAtextflex2">
              <div class="col-sm-6">
                <div class="Tright"><?php echo $db->getReviewerTotalSim($userid); ?></div>
                <div class="A16F">SIMULATIONS REVIEWED</div>
              </div>
              <div class="col-sm-6">
                <div class="Tright"><?php echo $db->getReviewerPendingSim($userid); ?></div>
                <div class="A16F">PENDING REVIEWS</div>
              </div>
            </div>
          </div>
        </div>
        <div class="item">
          <div class="flextlistitem">
            <div id="ready_to_publish" style="height: 170px; width: 100%;"></div>
          </div>
        </div>
        <div class="item">
          <div class="flextlistitem T1">
            <div id="review_fixed" style="height: 170px; width: 100%;"></div>
          </div>
        </div>
        <div class="item">
          <div class="flextlistitem">
            <div id="published" style="height: 170px; width: 100%;"></div>
          </div>
        </div>
        <div class="item">
          <div class="flextlistitem T1">
            <div id="ready_review_sim" style="height: 170px; width: 100%;"></div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="tab-content">
    <div id="home" class="tab-pane fade in active">
      <div class="adminT">
        <div class="col-sm-4"></div>
        <div class="col-sm-2">
          <a href="review-simulation.php">
            <div class="dashboard-box colorBoxW"> <img class="img-responsive width70 svg" src="img/dash_icon/Review Simulation-reviewer.svg">
              <div class="dash-sub-title">Review Simulation</div>
              <div class="dash-msg clearfix">
                <div class="notino"><img class="img-responsive" src="img/massage.png"> <span class="notifi-icon"><?php echo $db->getTotalNewUsers(); ?></span></div>
              </div>
            </div>
          </a> 
        </div>
        <div class="col-sm-2">
          <a href="reviewer-report.php">
            <div class="dashboard-box colorBoxW"> <img class="img-responsive width70 svg" src="img/dash_icon/view_report1.svg">
              <div class="dash-sub-title">View Report</div>
              <div class="dash-msg clearfix">
                <div class="notino"><img class="img-responsive" src="img/massage.png"><span class="notifi-icon">1</span></div>
              </div>
            </div>
          </a>
        </div>
      </div>
    </div>
  </div>
</div>
<script src="content/js/daterangepicker/moment.min.js"></script>
<script src="content/js/daterangepicker/daterangepicker.js"></script>
<script src="content/js/canvasjs.min.js"></script>
<script src="content/js/owlcarousel/owl.carousel.min.js"></script>
<script>
$(document).ready(function() {
  $('.owl-carousel').owlCarousel({
		loop:false,
		margin:5,
		nav:true,
		mouseDrag:false,
		responsive:{
			0:{
				items:1
			},
			700:{
				items:3
			},
			1200:{
				items:6
			}
		}
	})
  $.LoadingOverlay("show");
  
  var start = moment().subtract(29, 'days');
  var end   = moment();
  
  function cb(start, end) {
    $('#reportrange span').html(start.format('YYYY-MM-DD') + ' - ' + end.format('YYYY-MM-DD'));
  }
  
  $('#reportrange').daterangepicker({
    startDate: start,
    endDate: end,
    locale: { format: 'YYYY-MM-DD' },
    ranges: {
      'Today': [moment(), moment()],
      'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
      'Last 7 Days': [moment().subtract(6, 'days'), moment()],
      'Last 30 Days': [moment().subtract(29, 'days'), moment()],
      'This Month': [moment().startOf('month'), moment().endOf('month')],
      'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
    } }, cb);
  cb(start, end);
  
  function defaultGraphData(date){
    $.ajax({
      type: "POST",
      url: 'includes/client-reviewer-dash-grid-data.php',
      data: 'getReviewerDashData='+ true + '&date='+ date,
      cache: false,
      success: function (resdata) {
        $.LoadingOverlay("hide");
        var res = $.parseJSON(resdata);
        if (res.success == true) {
          CanvasJS.addColorSet("colorcode", ["#6d3073", "#3b602d", "#c07744", "#678087", "#678087"]);
          /* IN REVIEW */
          if (res.data.review_sim.length) {
            var reviewSimChart = new CanvasJS.Chart("review_fixed", {
                colorSet: "colorcode",
                title: { text: "REVIEW FIXED"},
                animationEnabled: true,
                theme: "light2",
                backgroundColor: '#fff',
                axisX: { gridThickness: 0, tickLength: 0, lineThickness: 0, labelFontColor: "black", labelFormatter: function() { return " "; } },
                axisY: { gridThickness: 0, tickLength: 0, lineThickness: 0, labelFormatter: function() { return " "; } },
                toolTip: { content: "<div class='wordwrap'>{name}</div>" },
                data: [{
                  type: "area",
                  indexLabel: "{y}",
                  indexLabelPlacement: "inside",
                  indexLabelFontWeight: "bolder",
                  indexLabelFontColor: "black",
                  dataPoints: res.data.review_sim
                }]
              });
              reviewSimChart.render();
          }
          /* READY TO PUBLISH */
          if (res.data.ready_publish_sim.length) {
            var readyPublishChart = new CanvasJS.Chart("ready_to_publish", {
                colorSet: "colorcode",
                title: { text: "READY TO PUBLISH"},
                animationEnabled: true,
                theme: "light2",
                backgroundColor: '#fff',
                axisX: { gridThickness: 0, tickLength: 0, lineThickness: 0, labelFontColor: "black", labelFormatter: function() { return " "; } },
                axisY: { gridThickness: 0, tickLength: 0, lineThickness: 0, labelFormatter: function() { return " "; } },
                toolTip: { content: "<div class='wordwrap'>{name}</div>" },
                data: [{
                  type: "column",
                  indexLabel: "{y}",
                  indexLabelPlacement: "inside",
                  indexLabelFontWeight: "bolder",
                  indexLabelFontColor: "white",
                  dataPoints: res.data.ready_publish_sim
                }]
              });
              readyPublishChart.render();
          }
          /* PUBLISHED */
          if (res.data.publish_sim.length) {
            var publishSimChart = new CanvasJS.Chart("published", {
                colorSet: "colorcode",
                title: { text: "PUBLISHED"},
                animationEnabled: true,
                theme: "light2",
                backgroundColor: '#fff',
                axisX: { gridThickness: 0, tickLength: 0, lineThickness: 0, labelFontColor: "black", labelFormatter: function() { return " "; } },
                axisY: { gridThickness: 0, tickLength: 0, lineThickness: 0, labelFormatter: function() { return " "; } },
                toolTip: { content: "<div class='wordwrap'>{name}</div>" },
                data: [{
                  type: "line",
                  indexLabel: "{y}",
                  indexLabelPlacement: "inside",
                  indexLabelFontWeight: "bolder",
                  indexLabelFontColor: "black",
                  dataPoints: res.data.publish_sim
                }]
              });
              publishSimChart.render();
          }
          /* Ready to Review */
          if (res.data.ready_review_sim.length) {
            var readyReviewSimChart = new CanvasJS.Chart("ready_review_sim", {
                colorSet: "colorcode",
                title: { text: "Ready to Review"},
                animationEnabled: true,
                theme: "light2",
                backgroundColor: '#fff',
                axisX: { gridThickness: 0, tickLength: 0, lineThickness: 0, labelFontColor: "black", labelFormatter: function() { return " "; } },
                axisY: { gridThickness: 0, tickLength: 0, lineThickness: 0, labelFormatter: function() { return " "; } },
                toolTip: { content: "<div class='wordwrap'>{name}</div>" },
                data: [{
                  type: "doughnut",
                  indexLabel: "{y}",
                  indexLabelPlacement: "inside",
                  indexLabelFontWeight: "bolder",
                  indexLabelFontColor: "white",
                  dataPoints: res.data.ready_review_sim
                }]
              });
              readyReviewSimChart.render();
          }
        }
      },error: function() {
        $.LoadingOverlay("hide");
        swal({text: 'Oops. something went wrong please try again.', buttons: false, icon: "error", timer: 2000});
      }
    });
  }
  
  var date = $('#reportrange').val();

  defaultGraphData(date);
  
  $('#reportrange').on('change', function() {
	  var date = $(this).val();
	  $.LoadingOverlay("show");
	  defaultGraphData(date);
  });  
});
</script>
<?php 
require_once 'includes/footer.php';
