﻿<?php 
ob_start();
if(!isset($_SESSION)) 
{ 
session_start();
}
?>
<!doctype html>
<!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]><html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--><html class="no-js" lang="en"> <!--<![endif]-->
<head>
  <title>EasySIM: Home Page</title>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="shortcut icon" type="image/x-icon" href="favicon.ico" />
  <!-- Chrome, Firefox OS and Opera -->
  <meta name="theme-color" content="#2c3545">
  <!-- Windows Phone -->
  <meta name="msapplication-navbutton-color" content="#2c3545">
  <!-- iOS Safari -->
  <meta name="apple-mobile-web-app-status-bar-style" content="#2c3545">
  <link rel="stylesheet" type="text/css" href="content/css/bootstrap.css" />
  <link rel="stylesheet" type="text/css" href="content/css/style.css?v=<?php echo time() ?>" />
  <link rel="stylesheet" type="text/css" href="content/css/responsive.css?v=<?php echo time() ?>" />
  <script type="text/javascript" src="content/js/jquery-3.3.1.min.js"></script>
  <script type="text/javascript" src="content/js/bootstrap.3.3.7.min.js"></script>
  <script type="text/javascript" src="content/js/timezone.js"></script>
</head>
<body>
<?php 

require_once 'config/db.class.php';

$obj    = new DBConnection;
$data   = $obj->getCMSPage(1);
$domain = (isset($_SESSION['domain_name'])) ? $_SESSION['domain_name'] : $_SERVER['HTTP_HOST'];
$bgImg  = ( ! empty($data['bg_img'])) ? 'scenario/upload/'. $domain . '/' . $data['bg_img'] : 'img/homepage/bglaptop.png';
$logo   = ( ! empty($obj->siteLogo)) ? $obj->siteLogo : 'EasySIM_logo.svg';
$brand  = $obj->webBranding();
# Check if already login then redirect
if (isset($_SESSION['username']) && isset($_SESSION['role'])):
	$user   = $_SESSION['username'];
	$role	  = $_SESSION['role'];
	$userid = $_SESSION['userId'];
  //echo $role;die;
	if (isset($role)) $obj->userRoleRedirect($role);
endif; 
echo ( ! empty($brand)) ? $brand : ''; ?>
<style type="text/css">
  .home_page { background-image: url('<?php echo $obj->getBaseUrl($bgImg); ?>') !important; }
  header { background: #fff; }
</style>
<div class="home_page">
  <!-- <div class="headergray yellow"></div> -->
  <header>
    <div class='iframe-hide'>
      <div class="home_header">
        <ul class="logowhite">
          <li><img class="white-svg-logo" src="<?php echo $obj->getBaseUrl('img/logo/'. $logo); ?>"></li>
          <li><img class="white-svg-logo1" src="<?php echo $obj->getBaseUrl('img/homepage/logo.svg'); ?>"></li>
        </ul>
      </div>
      <div class="home_header home_headerbtn">
        <ul class="regbtn">
          <li class="indeslogbtn indeslogbtn1"><a href="<?php echo $obj->getBaseUrl('register') ?>"><img class="svg" src="<?php echo $obj->getBaseUrl('img/homepage/register.svg') ?>"><span>REGISTER</span></a></li>
          <li class="indeslogbtn"><a href="<?php echo $obj->getBaseUrl('login') ?>"><img class="svg" src="<?php echo $obj->getBaseUrl('img/homepage/login.svg') ?>"><span>LOGIN</span></a></li>
        </ul>
      </div>
</div>
  </header>
  <div class="container">
    <div class="row Home_text_div">
      <div class="Home_text"><?php echo $data['page_content']; ?></div>
    </div>
  </div>
  <div class="footergray yellow"> 
    <!-- <div class="footertext">Powered by Easy<i>SIM</i><sup>®</sup></div> --> 
  </div>
  <script type="text/javascript" src="content/js/script.js?v=<?php echo time() ?>"></script>
  <script type="text/javascript" src="content/js/app.js?v=<?php echo time() ?>"></script>
</div>
</body>
</html>
