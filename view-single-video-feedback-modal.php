<?php 
session_start();
if (isset($_SESSION['username'])):
	$user 	= $_SESSION['username'];
	$role 	= $_SESSION['role'];
	$userid	= $_SESSION['userId'];
	$domain = (isset($_SESSION['domain_name'])) ? $_SESSION['domain_name'] : $_SERVER['HTTP_HOST'];
else:
    header('location: index.php');
endif;
require_once 'config/db.class.php';
$sid		= (isset($_POST['sid'])) ? $_POST['sid'] : exit('error');
$qid		= (isset($_POST['qid'])) ? $_POST['qid'] : exit('error');
$aid		= (isset($_POST['aid'])) ? $_POST['aid'] : '';
$report		= (isset($_POST['report'])) ? $_POST['report'] : '';
$target		= (isset($_POST['target'])) ? $_POST['target'] : '';
$current	= (isset($_POST['current'])) ? $_POST['current'] : '';
$db			= new DBConnection();
$feedData	= $db->getFeedback($qid, $aid);
$root_path	= $db->rootPath() . 'scenario/upload/'.$domain.'/';
$path		= $db->getBaseUrl('scenario/upload/'.$domain.'/');
$aPoster	= $db->getBaseUrl('scenario/img/audio-poster.jpg');
$vtheme		= 'forest'; #city, fantasy, forest, sea
$feedAsset  = '';
if ( ! empty($feedData['feedback'])):
	$feedAsset = TRUE;
elseif ( ! empty($feedData['feed_speech_text'])):
	$feedAsset = TRUE;
elseif ( ! empty($feedData['feed_audio']) && file_exists($root_path . $feedData['feed_audio'])):
	$feedAsset = TRUE;
elseif ( ! empty($feedData['feed_video']) && file_exists($root_path . $feedData['feed_video'])):
	$feedAsset = TRUE;
elseif ( ! empty($feedData['feed_screen']) && file_exists($root_path . $feedData['feed_screen'])):
	$feedAsset = TRUE;
elseif ( ! empty($feedData['feed_image']) && file_exists($root_path . $feedData['feed_image'])):
	$feedAsset = TRUE;
elseif ( ! empty($feedData['feed_document']) && file_exists($root_path . $feedData['feed_document'])):
	$feedAsset = TRUE;
endif;
if ( ! empty($feedAsset)): ?>
<link rel="stylesheet" href="videojs/node_modules/video.js/dist/video-js.min.css" />
<link rel="stylesheet" href="videojs/node_modules/video.js/dist/themes/forest/index.css" />
<script type="text/javascript" src="videojs/node_modules/video.js/dist/video.min.js"></script>
<div id="load_popup_modal_contant" class="feedback_box" role="dialog">
	<div class="modal-dialog modal-dialog-centered">
    	<div class="modal-content">
        	<div class="modal-header feedback-header">
            	<button type="button" class="feedclose" data-show-report="<?php echo $report; ?>" data-target="<?php echo $target; ?>" data-cur="<?php echo $current; ?>" data-dismiss="modal">&times;</button>
     			<h3 class="modal-title" id="exampleModalLabel">FEEDBACK</h3>
      		</div>
      		<div class="modal-body">
                <div class="row feedtextarea">
                    <div class="row feed">                        
                        <div class="flexfeed">
							<div class="flexfeed img-videobox">
								<?php if ( ! empty($feedData['feed_audio']) && file_exists($root_path . $feedData['feed_audio'])): ?>
								<audio id="fvideo-1" class="video-js vjs-theme-<?php echo $vtheme ?>" controls preload="auto" width="300px" height="100px" data-setup="{}" data-type="audio" poster="<?php echo $aPoster ?>">
									<source src="<?php echo $path . $feedData['feed_audio'] ?>" type="audio/<?php echo strtolower(pathinfo($feedData['feed_audio'], PATHINFO_EXTENSION)); ?>" />
								</audio>
								<?php elseif ( ! empty($feedData['feed_video']) && file_exists($root_path . $feedData['feed_video'])): ?>
								<video id="fvideo-1" class="video-js vjs-theme-<?php echo $vtheme ?>" controls preload="auto" width="640" height="264" data-setup="{}" data-type="video">
									<source src="<?php echo $path . $feedData['feed_video'] ?>" type="video/<?php echo strtolower(pathinfo($feedData['feed_video'], PATHINFO_EXTENSION)); ?>" />
								</video>
								<?php elseif ( ! empty($feedData['feed_screen']) && file_exists($root_path . $feedData['feed_screen'])): ?>
								<video id="fvideo-1" class="video-js vjs-theme-<?php echo $vtheme ?>" controls preload="auto" width="640" height="264" data-setup="{}" data-type="video">
									<source src="<?php echo $path . $feedData['feed_screen'] ?>" type="video/<?php echo strtolower(pathinfo($feedData['feed_screen'], PATHINFO_EXTENSION)); ?>" />
								</video>
								<?php elseif ( ! empty($feedData['feed_image']) && file_exists($root_path . $feedData['feed_image'])): ?>
								<a href="<?php echo $path . $feedData['feed_image']; ?>" data-fancybox data-caption="<?php echo stripslashes($feedData['feedback']); ?>">
									<img class="img-fluid" src="<?php echo $path . $feedData['feed_image']; ?>" />
								</a>
								<?php elseif ( ! empty($feedData['feed_document']) && file_exists($root_path . $feedData['feed_document'])): ?>
								<a href="javascript:;" data-src="<?php echo $path . $feedData['feed_document']; ?>" data-fancybox data-type="iframe" data-caption="<?php echo stripslashes($feedData['feedback']); ?>" class="document">
									<img class="img-fluid pdfclissicsIm" src="img/icons/pdf_icon.svg">
								</a>
								<?php elseif ( ! empty($feedData['feed_speech_text'])): ?>
									<button onclick="speak_feedback('div.feedback_intro_1')"><i id="playicon_feedback" class="fa fa-play playicon" aria-hidden="true"></i><i id="pauseicon_feedback" class="fa fa-pause pauseicon" style="display:none" aria-hidden="true"></i></button>
									<div class="feedback_intro_1" style="display:none;"><?php echo $feedData['feed_speech_text'] ?></div>
								<?php endif; ?>
							</div>
                            <div class="feedbacktext"><?php echo ( ! empty($feedData['feedback'])) ? stripslashes($feedData['feedback']) : ''; ?></div>
						</div>
				   </div>
                </div>
      		</div>
  		</div>
  	</div>
</div>
<script src="content/js/launch-sim-single-video-feedback.js?v=<?php echo time() ?>"></script>
<?php else: ?>
<style type="text/css">
#load_popup_modal_show { display:none !important;}
</style>
<div class="feedclose" data-show-report="<?php echo $report; ?>" data-target="<?php echo $target; ?>" data-cur="<?php echo $current; ?>"></div>
<script src="content/js/launch-sim-single-video.js?v=<?php echo time() ?>"></script>
<?php endif; ?>
