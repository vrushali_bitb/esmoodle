<?php 
ob_start();
session_start();
if (isset($_SESSION['username'])) {
  $user   = $_SESSION['username'];
  $role   = $_SESSION['role'];
  $userid = $_SESSION['userId'];
  $cid    = (isset($_SESSION['client_id'])) ? $_SESSION['client_id'] : exit('Oops. something went wrong please try again');
} else {
  header('location: index.php');
}
require_once 'config/db.class.php';
require_once 'includes/learners_menu.php';
$db = new DBConnection(); ?>
<link rel="stylesheet" media="screen" href="content/css/report_page.css" />
<style type="text/css">
  .table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th {
    padding: 10px;
  }
  .canvasjs-chart-canvas {
    padding-top: 0px !important;
  }
  .wordwrap {
    white-space: pre-wrap;
    white-space: -moz-pre-wrap;
    white-space: -pre-wrap;
    white-space: -o-pre-wrap;
    word-wrap: break-word;
  }
  .pull-right {
      padding: unset !important;
  }
  .checkbox1.radiobtn input:after, .radio:after{
    display:none;
  }
  .section1, .section2, .section3, .section4, .section5, .section6, .section7 {
    margin-bottom: 15px;
    margin-left: 0px !important;
  }
  .section4_main_div {
    border: 1px solid #ccc;
    width: 49%;
    padding: 15px 15px;
  }
  .section4_img {
    width: 50%;
    padding-top: 5%;
    padding-bottom: 5%;
    margin-left: 5%;
  }
  .section4_percent_pass {
    color: #8cc63f;
    font-size: 44px;
    margin-top: 3%;
    text-align: center;
  }
  .section4_percent_fail {
    color: red;
    font-size: 44px;
    margin-top: 3%;
    text-align: center;
  }
  .row.section4 {
      display: flex;
  }
  .passedLearsys {
      font-size: 44px;
      color: #8cc63f;
      margin-top: 6%;
      text-align: center;
  }
  .passedLearsys div:nth-child(2) {
      font-size: 14px;
      color: #000;
      font-weight: bold;
      line-height: 16px;
  }
  .section7_percent {
    color: #1185bb;
    font-size: 44px;
  }
  .section4_Learners {
    font-size: 17px;
    font-weight: bold;
  }
  .admin_report_simulation_name {
    font-weight: bold;
    font-size: 19px;
  }
  .admin_report_group_leader {
    color: #1185bb;
    font-size: 15px;
    font-weight: 600;
  }
  .view_detail_btn {
    font-weight: bold;
    color: #118bc6;
    font-size: 15px;
  }
  .section7 .col-sm-12 {
    border: 1px solid #ccc;
    padding: 15px 15px;
  }
  hr.new4 {
    margin-top: 0px;
    margin-bottom: 5px;
  }
  .learner_rep {
    margin-bottom: 15px;
  }
  .drop_dwn {
    float: left !important;
    width: 20% !important;
    margin-right: 15px;
    margin-top: 0px !important;
    margin-bottom: 0px !important;
  }
  .col-sm-1.sn {
    width: 3%;
  }
  .learner_rep .col-sm-8 {
    width: 76.666667%;
  }

  button.collapse-init.btn.btn-primary.pull-right {
      margin-bottom: 20px;
      margin-right: 15px;
  }
  .learner_rep .col-sm-2 {
    text-align: center;
    width: 9.666667%;
  }
  #Comptencies {
    display: none;
  }

  span.multiselect-native-select {
      position: relative;
      width: 120px;
  }
  label {
      margin-top: 20px;
      margin-right: 11PX;
  }
  .lernar-dashboard.learner-progress-Banner.admin-report .usermanage-form {
      padding-left: 5px;
  }
  .input-group input.form-control.multiselect-search {
      margin-top: 0px;
      margin-bottom: 0px;
      height: 34px;
    width: 125px;
  }
  @media only screen and (max-width: 996px){
    .section4_percent_pass {
      font-size: 30px;
  }
  .section4_percent_fail {
      color: red;
      font-size: 30px;
  }
  .passedLearsys {
      font-size: 30px;
  }
  }
  @media only screen and (max-width: 767px){
    .section4_main_div {
      text-align: center;
    }
  }
</style>
<div class="bottomheader">
  <div class="container-fluid">
    <ul class="breadcrumb">
      <?php echo $db->breadcrumbs(); ?>
    </ul>
  </div>
</div>
<div class="admin-report GROUPpreFormancemain">
  <div class="bottomheader1">
    <div class="container-fluid">
      <div class="row">
        <div class="col-sm-4">
          <h3>Systemwide Analytics</h3>
        </div>
        <div class="col-sm-4">
          <div class="alert alert-warning alert-dismissible" role="alert" style="display: <?php echo (isset($_SESSION['msg'])) ? 'block' : 'none'; ?>">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <strong>Warning!</strong> <?php echo (isset($_SESSION['msg'])) ? $_SESSION['msg'] : ''; ?>
          </div>
        </div>
        <div class="col-sm-3 L-calender GPeducater usermanage-form">
          <select class="form-control" id="report">
            <option selected="selected" hidden>Systemwide Analytics</option>
            <option value="1">Overview</option>
            <option value="2">Group's Performance</option>
          </select>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-12">
          <form name="report_form" id="report_form" method="post">
            <input type="hidden" name="get_educator[]" id="get_educator">
            <div class="usermanage-form GrouPerformainbanner">
              <div class="row">
                <div class="col-sm-4">
                  <div class="form-group form-select">
                    <select name="category" id="category" class="form-control" data-placeholder="Category List" required="required">
                      <option value="0" selected="selected" disabled="disabled" hidden>Select Category</option>
                      <?php foreach ($db->getCategory() as $category): ?>
                        <option value="<?php echo $category['cat_id'] ?>"><?php echo ucwords($category['category']) ?></option>
                      <?php endforeach; ?>
                    </select>
                  </div>
                </div>
                <div class="col-sm-4">
                  <div class="form-group form-select">
                    <select name="scenario" id="scenario" class="form-control multiselect-ui selection" required="required" data-placeholder="Search & Select Simulations" multiple="multiple">
                    </select>
                  </div>
                </div>
              </div>
              <div class="row GrouPerforbanner">
                <div class="col-sm-2">
                  <div class="Radio-box GrouPerfor">
                    <label class="radiostyle">
                      <input type="radio" class="checkbox1 filter_type" name="filter_type" id="System_Wise" value="1" checked="checked">
                      <span class="radiomark"></span>
                    </label>
                    <span>Systemwide</span>
                  </div>
                </div>
                <div class="col-sm-3">
                  <div class="Radio-box GrouPerfor">
                    <label class="radiostyle">
                      <input type="radio" class="checkbox1 filter_type" name="filter_type" id="filter_educator" value="2" disabled="disabled">
                      <span class="radiomark"></span>
                    </label>
                    <div class="form-group form-select">
                      <select name="educator[]" id="educator" class="form-control multiselect-ui selection multiselection" multiple="multiple" data-placeholder="Educator">
                      </select>
                    </div>
                  </div>
                </div>
                <div class="col-sm-2" style="display: none;">
                  <div class="Radio-box GrouPerfor">
                    <label class="radiostyle">
                        <input type="radio" class="checkbox1 filter_type" name="filter_type" id="filter_learner" value="3" disabled="disabled">
                        <span class="radiomark"></span>
                    </label>
                    <div class="form-group form-select">
                      <select name="learner[]" id="learner" class="form-control multiselect-ui selection multiselection" multiple="multiple" data-placeholder="Learner">
                      </select>
                    </div>
                  </div>
                </div>
                <div class="col-sm-3">
                  <div class="Radio-box GrouPerfor">
                    <label class="radiostyle">
                      <input type="radio" class="checkbox1 filter_type other_filter" name="filter_type" value="4" disabled="disabled">
                      <span class="radiomark"></span>
                    </label>
                    <div class="form-group form-select">
                      <select name="unit[]" id="unit" class="form-control multiselect-ui selection multiselection" multiple="multiple" data-placeholder="Unit">
                      </select>
                    </div>
                  </div>
                </div>
                <div class="col-sm-2">
                  <div class="Radio-box GrouPerfor">
                    <label class="radiostyle">
                        <input type="radio" class="checkbox1 filter_type other_filter" name="filter_type" value="5" disabled="disabled">
                        <span class="radiomark"></span>
                    </label>
                    <div class="form-group form-select">
                      <select name="location[]" id="location" class="form-control multiselect-ui selection multiselection" multiple="multiple" data-placeholder="Location">
                      </select>
                    </div>
                  </div>
                </div>
                <div class="col-sm-2">
                  <div class="Radio-box GrouPerfor">
                    <label class="radiostyle">
                        <input type="radio" class="checkbox1 filter_type other_filter" name="filter_type" value="6" disabled="disabled">
                        <span class="radiomark"></span>
                    </label>
                    <div class="form-group form-select">
                      <select name="designation[]" id="designation" class="form-control multiselect-ui selection multiselection" multiple="multiple" data-placeholder="Designation">
                      </select>
                    </div>
                  </div>
                </div>
              </div>
              <div class="row GrouPerforbanner">
                  <div class="col-sm-3">
                    <div class="form-group">
                      <div class="input-group date form_datetime" data-date-format="yyyy-mm-dd" data-link-field="dtp_input1">
                        <input class="form-control" name="fdate" type="text" readonly placeholder="From Date" />
                          <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                      </div>
                    </div>
                  </div>
                  <div class="col-sm-3">
                    <div class="form-group">
                      <div class="input-group date form_datetime" data-date-format="yyyy-mm-dd" data-link-field="dtp_input1">
                        <input class="form-control" name="tdate" type="text" readonly placeholder="To Date" />
                        <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                      </div>
                    </div>
                  </div>
                </div>
              <div class="usermanage-add clearfix">
                  <input type="submit" name="submit" id="submit" class="btn btn-primary Search_btn" value="Search">
                </div>
            </div>
          </form>
          <?php 
          if ($_SERVER['REQUEST_METHOD'] == 'POST' && ! empty($_POST['scenario'])):
            extract($_POST);
            if (empty($fdate) && empty($tdate)):
              $_SESSION['msg'] = 'Please select start or end date.!';
              header('Refresh: 0; url=learner-system-report.php');
              exit;
            elseif (empty($scenario)):
                $_SESSION['msg'] = 'Please select Simulation.';
                header('Refresh: 0; url=learner-system-report.php');
                exit;
            elseif ( ! empty($fdate) && ! empty($tdate)):
              $earlier  = new DateTime($fdate);
              $later    = new DateTime($tdate);
              $abs_diff = $later->diff($earlier)->format("%a");
              if ($abs_diff > 30):
                $_SESSION['msg'] = 'please select between one month.!';
                header('Refresh: 0; url=learner-system-report.php');
                exit;
              endif;
            endif;
            $sim_data       = $db->getSim('Scenario_title,passing_marks,sim_temp_type', $scenario)[0];
            $passing_marks  = ( ! empty($sim_data) && $sim_data['passing_marks'] > 0) ? $sim_data['passing_marks'] : 0;
            $temp_type      = $sim_data['sim_temp_type'];
            $sqlComp        = "SELECT (SUM(comp_val_1) + SUM(comp_val_2) + SUM(comp_val_3) + SUM(comp_val_4) + SUM(comp_val_5) + SUM(comp_val_6)) as totalCompval FROM competency_tbl WHERE scenario_id = '" . $scenario . "'";
            $sqlCompExc     = $db->prepare($sqlComp); $sqlCompExc->execute();
            $compRow        = $sqlCompExc->fetch(PDO::FETCH_ASSOC);
            $totalCompval   = $compRow['totalCompval'];
            if ($filter_type == 1):
              #----System-Wise-------
              $filter_type = 'Systemwide';
              $sql = "SELECT GROUP_CONCAT(DISTINCT IF(group_id = '', null, group_id)) AS gid, GROUP_CONCAT(DISTINCT IF(learner_id = '', null, learner_id)) AS lid FROM assignment_tbl WHERE scenario_id = '" . $scenario . "'";
              $exc = $db->prepare($sql); $exc->execute();
              $row = $exc->fetch(PDO::FETCH_ASSOC);
              $gid = ( ! empty($row['gid'])) ? rtrim($row['gid'], ',') : '';
              $lid = ( ! empty($row['lid'])) ? rtrim($row['lid'], ',') : '';
              foreach ($db->getLearnerByGroup($gid) as $gdata):
                $getlearnerId[] = $gdata['learner'];
              endforeach;
              if ( ! empty($tdate) && ! empty($fdate)):
                $get_learner_id = $db->addMultiIds($getlearnerId);
                $date_sql = "SELECT GROUP_CONCAT(userid) AS learner FROM scenario_attempt_tbl WHERE scenario_id = $scenario AND userid IN ($get_learner_id) AND DATE(attempt_date) BETWEEN '". $fdate ."' AND '". $tdate ."'";
                $date_exc = $db->prepare($date_sql); $date_exc->execute();
                $date_row = $date_exc->fetch(PDO::FETCH_ASSOC);
                $get_learner = $date_row['learner'];
              elseif ( ! empty($tdate) && empty($fdate)):
                $get_learner_id = $db->addMultiIds($getlearnerId);
                $date_sql = "SELECT GROUP_CONCAT(userid) AS learner FROM scenario_attempt_tbl WHERE scenario_id = $scenario AND userid IN ($get_learner_id) AND DATE(attempt_date) = '". $tdate ."'";
                $date_exc = $db->prepare($date_sql); $date_exc->execute();
                $date_row = $date_exc->fetch(PDO::FETCH_ASSOC);
                $get_learner = $date_row['learner'];
              elseif ( ! empty($fdate) && empty($tdate)):
                $get_learner_id = $db->addMultiIds($getlearnerId);
                $date_sql = "SELECT GROUP_CONCAT(userid) AS learner FROM scenario_attempt_tbl WHERE scenario_id = $scenario AND userid IN ($get_learner_id) AND DATE(attempt_date) = '". $fdate ."'";
                $date_exc = $db->prepare($date_sql); $date_exc->execute();
                $date_row = $date_exc->fetch(PDO::FETCH_ASSOC);
                $get_learner = $date_row['learner'];
              else:
                $get_learner = $db->addMultiIds($getlearnerId);
              endif;
            elseif ($filter_type == 2):
              #----Educator-------
              $filter_type = 'Educator';
              $filter_data = $db->addMultiIds($get_educator);
              if ( ! empty($tdate) && ! empty($fdate)):
                $get_learner_id = $db->addMultiIds($educator);
                $date_sql = "SELECT GROUP_CONCAT(userid) AS learner FROM scenario_attempt_tbl WHERE scenario_id = $scenario AND userid IN ($get_learner_id) AND DATE(attempt_date) BETWEEN '". $fdate ."' AND '". $tdate ."'";
                $date_exc = $db->prepare($date_sql); $date_exc->execute();
                $date_row = $date_exc->fetch(PDO::FETCH_ASSOC);
                $get_learner = $date_row['learner'];
              elseif ( ! empty($tdate) && empty($fdate)):
                $get_learner_id = $db->addMultiIds($educator);
                $date_sql = "SELECT GROUP_CONCAT(userid) AS learner FROM scenario_attempt_tbl WHERE scenario_id = $scenario AND userid IN ($get_learner_id) AND DATE(attempt_date) = '". $tdate ."'";
                $date_exc = $db->prepare($date_sql); $date_exc->execute();
                $date_row = $date_exc->fetch(PDO::FETCH_ASSOC);
                $get_learner = $date_row['learner'];
              elseif ( ! empty($fdate) && empty($tdate)):
                $get_learner_id = $db->addMultiIds($educator);
                $date_sql = "SELECT GROUP_CONCAT(userid) AS learner FROM scenario_attempt_tbl WHERE scenario_id = $scenario AND userid IN ($get_learner_id) AND DATE(attempt_date) = '". $fdate ."'";
                $date_exc = $db->prepare($date_sql); $date_exc->execute();
                $date_row = $date_exc->fetch(PDO::FETCH_ASSOC);
                $get_learner = $date_row['learner'];
              else:
                $get_learner = $db->addMultiIds($educator);
              endif;
            elseif ($filter_type == 3):
              #----Learner------
              $filter_type = 'Learner';
              $get_learner = $db->addMultiIds($learner);
            elseif ($filter_type == 4 || $filter_type == 5 || $filter_type == 6):
              #----Unit------
              if ($filter_type == 4):
                $filter_type      = 'Unit';
                $get_filter_data  = implode("','", $unit);
                $filter_data      = $db->addMultiIds($unit);
                $column           = 'department';
              elseif ($filter_type == 5):
                #----Location------
                $filter_type      = 'Location';
                $get_filter_data  = implode("','", $location);
                $filter_data      = $db->addMultiIds($location);
                $column           = 'location';
              elseif ($filter_type == 6):
                #----Designation-----
                $filter_type      = 'Designation';
                $get_filter_data  = implode("','", $designation);
                $filter_data      = $db->addMultiIds($designation);
                $column           = 'designation';
              endif;
              $sql = "SELECT GROUP_CONCAT(DISTINCT IF(group_id = '', null, group_id)) AS gid, GROUP_CONCAT(DISTINCT IF(learner_id = '', null, learner_id)) AS lid FROM assignment_tbl WHERE scenario_id = '" . $scenario . "'";
              $exc = $db->prepare($sql); $exc->execute();
              $row = $exc->fetch(PDO::FETCH_ASSOC);
              $gid = ( ! empty($row['gid'])) ? rtrim($row['gid'], ',') : '';
              $lid = ( ! empty($row['lid'])) ? rtrim($row['lid'], ',') : '';
              foreach ($db->getLearnerByGroup($gid) as $gdata):
                $getlearnerId[] = $gdata['learner'];
              endforeach;
              $get_unit_learner = $db->addMultiIds($getlearnerId);
              $cond = "$column IN ('$get_filter_data')";
              $get_other_learner = $db->GetUsersByFilter($get_unit_learner, 'id', $cond);
              if ( ! empty($tdate) && ! empty($fdate)):
                $get_learner_id = $db->addMultiIds($get_other_learner);
                $date_sql = "SELECT GROUP_CONCAT(userid) AS learner FROM scenario_attempt_tbl WHERE scenario_id = $scenario AND userid IN ($get_learner_id) AND DATE(attempt_date) BETWEEN '". $fdate ."' AND '". $tdate ."'";
                $date_exc = $db->prepare($date_sql); $date_exc->execute();
                $date_row = $date_exc->fetch(PDO::FETCH_ASSOC);
                $get_learner = $date_row['learner'];
              elseif ( ! empty($tdate) && empty($fdate)):
                $get_learner_id = $db->addMultiIds($get_other_learner);
                $date_sql = "SELECT GROUP_CONCAT(userid) AS learner FROM scenario_attempt_tbl WHERE scenario_id = $scenario AND userid IN ($get_learner_id) AND DATE(attempt_date) = '". $tdate ."'";
                $date_exc = $db->prepare($date_sql); $date_exc->execute();
                $date_row = $date_exc->fetch(PDO::FETCH_ASSOC);
                $get_learner = $date_row['learner'];
              elseif ( ! empty($fdate) && empty($tdate)):
                $get_learner_id = $db->addMultiIds($get_other_learner);
                $date_sql = "SELECT GROUP_CONCAT(userid) AS learner FROM scenario_attempt_tbl WHERE scenario_id = $scenario AND userid IN ($get_learner_id) AND DATE(attempt_date) = '". $fdate ."'";
                $date_exc = $db->prepare($date_sql); $date_exc->execute();
                $date_row = $date_exc->fetch(PDO::FETCH_ASSOC);
                $get_learner = $date_row['learner'];
              else:
                $get_learner = $db->addMultiIds($get_other_learner);
              endif;
            endif;
            /* $user_sql = "SELECT id FROM users WHERE id IN ($get_learner)";
            $user_q = $db->prepare($user_sql); $user_q->execute();
            if ($user_q->rowCount() > 0):
              foreach ($user_q->fetchAll(PDO::FETCH_ASSOC) as $user_row):
                $user_data[] = $user_row['id'];
              endforeach;
              $user_data_ids = $db->addMultiIds($user_data);
            else:
              $user_data_ids = [];
            endif; */
            #$get_all_learner    = ( ! empty($user_data_ids)) ? explode(',', $user_data_ids) : [];
            $get_all_learner    = ( ! empty($get_learner)) ? explode(',', $get_learner) : [];
            $get_total_learner  = count($get_all_learner);
            if ($get_total_learner > 0):
              $lastAttempt = [];
              foreach ($get_all_learner as $ldata):
                $get_learner_data = $db->getLastAttemptMultiSIM($scenario, $ldata);
                if ( ! empty($get_learner_data)):
                  $lastAttempt[] = $get_learner_data;
                endif;
              endforeach;
              $getLastAttempt = ( ! empty($lastAttempt[0])) ? call_user_func_array('array_merge', array_filter($lastAttempt)) : [];
              $pass = $fail = 0;
              $pass_learner_data = $fail_learner_data = [];
              if ( ! empty($getLastAttempt)):
                foreach ($getLastAttempt as $lastData):
                  if ($temp_type == 1 || $temp_type == 8):
                    $sqlAns = "SELECT (SUM(a.ans_val1) + SUM(a.ans_val2) + SUM(a.ans_val3) + SUM(a.ans_val4) + SUM(a.ans_val5) + SUM(a.ans_val6)) as totalAnsval, sc.userid FROM score_tbl sc LEFT JOIN answer_tbl a ON sc.choice_option_id = a.answer_id WHERE sc.scenario_id = '" . $scenario . "' AND sc.uid = '" . $lastData['uid'] . "'";
                  elseif ($temp_type == 2):
                    $sqlAns = "SELECT (SUM(q.ques_val_1) + SUM(q.ques_val_2) + SUM(q.ques_val_3) + SUM(q.ques_val_4) + SUM(q.ques_val_5) + SUM(q.ques_val_6)) AS totalAnsval, sc.userid FROM multi_score_tbl sc LEFT JOIN question_tbl q ON sc.question_id = q.question_id WHERE sc.status = 1 AND sc.scenario_id = '" . $scenario . "' AND sc.uid = '" . $lastData['uid'] . "'";
                  elseif ($temp_type == 7):
                    $sqlAns = "SELECT (SUM(a.ans_val1) + SUM(a.ans_val2) + SUM(a.ans_val3) + SUM(a.ans_val4) + SUM(a.ans_val5) + SUM(a.ans_val6)) as totalAnsval, a.userid FROM open_res_tbl a LEFT JOIN competency_tbl c ON a.scenario_id = c.scenario_id WHERE a.scenario_id = '" . $scenario . "' AND a.uid = '" . $lastData['uid'] . "'";
                  endif;
                  $sqlAnsExc      = $db->prepare($sqlAns); $sqlAnsExc->execute();
                  $ansRow         = $sqlAnsExc->fetch(PDO::FETCH_ASSOC);
                  $totalAnsval    = $ansRow['totalAnsval'];
                  $achieved_score = $db->get_percentage($totalCompval, $totalAnsval);
                  if ($temp_type == 1 || $temp_type == 8):
                    if ($db->checkCriticalClassicTemp($scenario, $lastData['uid']) == TRUE || $db->checkCriticalClassicTemp($scenario, $lastData['uid']) == 2):
                      if ($achieved_score > 0 && $achieved_score >= $passing_marks):
                        $pass++;
                        $pass_learner_data[] = $lastData['userid'];
                      else :
                        $fail++;
                        $fail_learner_data[] = $lastData['userid'];
                      endif;
                    else:
                      $fail++;
                      $fail_learner_data[] = $lastData['userid'];
                    endif;
                  elseif ($temp_type == 2):
                    if ($db->checkCriticalMultiTemp($scenario, $lastData['uid']) == TRUE || $db->checkCriticalMultiTemp($scenario, $lastData['uid']) == 2):
                      if ($achieved_score > 0 && $achieved_score >= $passing_marks):
                        $pass++;
                        $pass_learner_data[] = $lastData['userid'];
                      else :
                        $fail++;
                        $fail_learner_data[] = $lastData['userid'];
                      endif;
                    else:
                      $fail++;
                      $fail_learner_data[] = $lastData['userid'];
                    endif;
                  elseif ($temp_type == 7):
                    if ($db->checkCriticalOpenRes($scenario, $lastData['uid']) == TRUE || $db->checkCriticalOpenRes($scenario, $lastData['uid']) == 2):
                      if ($achieved_score > 0 && $achieved_score >= $passing_marks):
                        $pass++;
                        $pass_learner_data[] = $lastData['userid'];
                      else :
                        $fail++;
                        $fail_learner_data[] = $lastData['userid'];
                      endif;
                    else:
                      $fail++;
                      $fail_learner_data[] = $lastData['userid'];
                    endif;
                  endif;
                endforeach;
              endif;
            endif;
            $pass_learner = ( ! empty($get_total_learner) && ! empty($pass)) ? $db->get_percentage($get_total_learner, $pass) : 0;
            $fail_learner = ( ! empty($get_total_learner) && ! empty($fail)) ? $db->get_percentage($get_total_learner, $fail) : 0; ?>
            <div class="print_data">
              <div class="row section3">
                  <label class="admin_report_simulation_name"><?php echo ( ! empty($sim_data['Scenario_title'])) ? $sim_data['Scenario_title'] : ''; ?></label>
                  <p class="admin_report_group_leader"><?php echo ( ! empty($filter_type)) ? $filter_type : ''; ?></p>
                  <?php echo ( ! empty($filter_data)) ? ': ' . $filter_data : ''; ?>
              </div>
              <div class="row section4">
                <div class="col-sm-6 section4_main_div" style="margin-right: 10px;">
                  <div class="section4_Learners"> PASS % OF LEARNERS</div>
                  <div class="row">
                    <div class="col-sm-4"><img class="section4_img" src="img/list/happy.png"></div>
                    <div class="col-sm-4 section4_percent_pass"><?php echo ( ! empty($pass_learner)) ? $pass_learner : 0 ?>%</div>
                    <div class="col-sm-4">                    
                      <div class="passedLearsys">
                        <div><?php echo count($pass_learner_data); ?>/<?php echo $get_total_learner; ?></div>
                        <div class="view_detail_btn">
                        <?php if ( ! empty($pass_learner_data)): ?>
                          <a data-toggle="popover" title="View Learners" href="<?php echo $db->getBaseUrl('includes/ajax.php?get_learner=true&learnerId=' . $db->addMultiIds($pass_learner_data)) ?>" onclick="return false">View Detail</a>
                        <?php else : ?>
                          <a title="View Learners" href="javascript:void(0);" onclick="return false">View Detail</a>
                        <?php endif; ?>
                      </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-sm-6 section4_main_div">
                  <div class="section4_Learners"> REMEDIATION REQUIRED % OF LEARNERS</div>
                  <div class="row">
                    <div class="col-sm-4"><img class="section4_img" src="img/list/sad.png"></div>
                    <div class="col-sm-4 section4_percent_fail"><?php echo ( ! empty($fail_learner)) ? $fail_learner : 0 ?>% </div>
                    <div class="col-sm-4">
                      <div class="passedLearsys red">
                        <div><?php echo count($fail_learner_data); ?>/<?php echo $get_total_learner; ?></div>
                        <div class="view_detail_btn">
                        <?php if ( ! empty($fail_learner_data)): ?>
                          <a data-toggle="popover" title="View Learners" href="<?php echo $db->getBaseUrl('includes/ajax.php?get_learner=true&learnerId=' . $db->addMultiIds($fail_learner_data)) ?>" onclick="return false">View Detail</a>
                        <?php else: ?>
                          <a title="View Learners" href="javascript:void(0);" onclick="return false">View Detail</a>
                        <?php endif; ?>
                      </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <button class="collapse-init btn btn-primary pull-right">Expand All</button>
              <div class="row section7">
                <div class="col-sm-12">
                  <div class="section4_Learners">% OF LEARNERS WHO MISSED QUESTIONS</div>
                  <div>
                    <?php $comData = $db->getScenarioCompetency(md5($scenario)); ?>
                    <div class="panel-group" id="accordion">
                      <?php if ( ! empty($comData['comp_col_1']) && ! empty($comData['comp_val_1'])): ?>
                        <div class="panel panel-default">
                          <div class="panel-heading">
                            <h4 class="panel-title"> <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse1"><strong>1. Competency :</strong> <?php echo $comData['comp_col_1'] ?></a></h4>
                          </div>
                          <div id="collapse1" class="panel-collapse collapse">
                            <div class="panel-body">
                              <div class="panel-group" id="nested">
                                <?php $ques = 0; $missedQues1 = 0;
                                foreach ($db->getQuestionsByScenario(md5($scenario)) as $quesdata): ?>
                                  <div class="panel panel-default">
                                    <div class="panel-heading">
                                      <h4 class="panel-title">
                                        <a class="collapsed" data-toggle="collapse" data-parent="#nested" href="#nested-collapse<?php echo $ques ?>">
                                          <?php echo $quesdata['questions'] ?> &nbsp;
                                          <?php $missedQues1 = $db->GetPercentageMissedQues($temp_type, $getLastAttempt, $quesdata['question_id'], 'ques_val_1', 'ans_val1', $get_total_learner);
                                          echo ( ! empty($missedQues1['percentage'])) ? $missedQues1['percentage'] : 0; ?>%</a>
                                      </h4>
                                    </div>
                                    <div id="nested-collapse<?php echo $ques ?>" class="panel-collapse collapse">
                                      <div class="panel-body">
                                        <table class="table table-striped">
                                          <thead class="Theader">
                                            <tr>
                                              <th>User Name</th>
                                              <th>Full Name</th>
                                              <th>Location</th>
                                              <th>Company</th>
                                              <th>Unit</th>
                                            </tr>
                                          </thead>
                                          <tbody>
                                          <?php if ( ! empty($missedQues1['user_data'])):
                                            foreach ($missedQues1['user_data'] as $udata): ?>
                                                <tr>
                                                  <td><?php echo $udata['username'] ?></td>
                                                  <td><?php echo $udata['full_name'] ?></td>
                                                  <td><?php echo $udata['location'] ?></td>
                                                  <td><?php echo $udata['company'] ?></td>
                                                  <td><?php echo $udata['department'] ?></td>
                                                </tr>
                                            <?php endforeach; endif; ?>
                                          </tbody>
                                        </table>
                                      </div>
                                    </div>
                                  </div>
                                <?php $ques++; endforeach; ?>
                              </div>
                            </div>
                          </div>
                        </div>
                      <?php endif; ?>

                      <?php if ( ! empty($comData['comp_col_2']) && ! empty($comData['comp_val_2'])): ?>
                        <div class="panel panel-default">
                          <div class="panel-heading">
                            <h4 class="panel-title"> <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse2"><strong>2. Competency :</strong> <?php echo $comData['comp_col_2'] ?></a> </h4>
                          </div>
                          <div id="collapse2" class="panel-collapse collapse">
                            <div class="panel-body">
                              <div class="panel-group" id="nested">
                                <?php $ques1 = $ques; $missedQues2 = 0;
                                foreach ($db->getQuestionsByScenario(md5($scenario)) as $quesdata2): ?>
                                  <div class="panel panel-default">
                                    <div class="panel-heading">
                                      <h4 class="panel-title">
                                        <a class="collapsed" data-toggle="collapse" data-parent="#nested" href="#nested-collapse<?php echo $ques1 ?>">
                                          <?php echo $quesdata2['questions'] ?> &nbsp;
                                          <?php $missedQues2 = $db->GetPercentageMissedQues($temp_type, $getLastAttempt, $quesdata2['question_id'], 'ques_val_1', 'ans_val1', $get_total_learner);
                                          echo ( ! empty($missedQues2['percentage'])) ? $missedQues2['percentage'] : 0; ?>%</a>
                                      </h4>
                                    </div>
                                    <div id="nested-collapse<?php echo $ques1 ?>" class="panel-collapse collapse">
                                      <div class="panel-body">
                                        <table class="table table-striped">
                                          <thead class="Theader">
                                            <tr>
                                              <th>User Name</th>
                                              <th>Full Name</th>
                                              <th>Location</th>
                                              <th>Company</th>
                                              <th>Unit</th>
                                            </tr>
                                          </thead>
                                          <tbody>
                                          <?php if ( ! empty($missedQues2['user_data'])):
                                              foreach ($missedQues2['user_data'] as $udata): ?>
                                                <tr>
                                                  <td><?php echo $udata['username'] ?></td>
                                                  <td><?php echo $udata['full_name'] ?></td>
                                                  <td><?php echo $udata['location'] ?></td>
                                                  <td><?php echo $udata['company'] ?></td>
                                                  <td><?php echo $udata['department'] ?></td>
                                                </tr>
                                            <?php endforeach; endif; ?>
                                          </tbody>
                                        </table>
                                      </div>
                                    </div>
                                  </div>
                                <?php $ques1++; endforeach; ?>
                              </div>
                            </div>
                          </div>
                        </div>
                      <?php endif; ?>

                      <?php if ( ! empty($comData['comp_col_3']) && ! empty($comData['comp_val_3'])): ?>
                        <div class="panel panel-default">
                          <div class="panel-heading">
                            <h4 class="panel-title"> <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse3"><strong>3. Competency :</strong> <?php echo $comData['comp_col_3'] ?></a> </h4>
                          </div>
                          <div id="collapse3" class="panel-collapse collapse">
                            <div class="panel-body">
                              <div class="panel-group" id="nested">
                                <?php $ques2 = $ques1; $missedQues3 = 0;
                                foreach ($db->getQuestionsByScenario(md5($scenario)) as $quesdata3): ?>
                                  <div class="panel panel-default">
                                    <div class="panel-heading">
                                      <h4 class="panel-title">
                                        <a class="collapsed" data-toggle="collapse" data-parent="#nested" href="#nested-collapse<?php echo $ques2 ?>">
                                          <?php echo $quesdata3['questions'] ?> &nbsp;
                                          <?php $missedQues3 = $db->GetPercentageMissedQues($temp_type, $getLastAttempt, $quesdata3['question_id'], 'ques_val_1', 'ans_val1', $get_total_learner);
                                          echo ( ! empty($missedQues3['percentage'])) ? $missedQues3['percentage'] : 0; ?>%</a>
                                      </h4>
                                    </div>
                                    <div id="nested-collapse<?php echo $ques2 ?>" class="panel-collapse collapse">
                                      <div class="panel-body">
                                        <table class="table table-striped">
                                          <thead class="Theader">
                                            <tr>
                                              <th>User Name</th>
                                              <th>Full Name</th>
                                              <th>Location</th>
                                              <th>Company</th>
                                              <th>Unit</th>
                                            </tr>
                                          </thead>
                                          <tbody>
                                          <?php if ( ! empty($missedQues3['user_data'])):
                                              foreach ($missedQues3['user_data'] as $udata): ?>
                                                <tr>
                                                  <td><?php echo $udata['username'] ?></td>
                                                  <td><?php echo $udata['full_name'] ?></td>
                                                  <td><?php echo $udata['location'] ?></td>
                                                  <td><?php echo $udata['company'] ?></td>
                                                  <td><?php echo $udata['department'] ?></td>
                                                </tr>
                                            <?php endforeach; endif; ?>
                                          </tbody>
                                        </table>
                                      </div>
                                    </div>
                                  </div>
                                <?php $ques2++; endforeach; ?>
                              </div>
                            </div>
                          </div>
                        </div>
                      <?php endif; ?>

                      <?php if ( ! empty($comData['comp_col_4']) && ! empty($comData['comp_val_4'])): ?>
                        <div class="panel panel-default">
                          <div class="panel-heading">
                            <h4 class="panel-title"> <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse4"><strong>4. Competency :</strong> <?php echo $comData['comp_col_4'] ?></a> </h4>
                          </div>
                          <div id="collapse4" class="panel-collapse collapse">
                            <div class="panel-body">
                              <div class="panel-group" id="nested">
                                <?php $ques3 = $ques2; $missedQues4 = 0;
                                foreach ($db->getQuestionsByScenario(md5($scenario)) as $quesdata4): ?>
                                  <div class="panel panel-default">
                                    <div class="panel-heading">
                                      <h4 class="panel-title">
                                        <a class="collapsed" data-toggle="collapse" data-parent="#nested" href="#nested-collapse<?php echo $ques3 ?>">
                                          <?php echo $quesdata4['questions'] ?> &nbsp;
                                          <?php $missedQues4 = $db->GetPercentageMissedQues($temp_type, $getLastAttempt, $quesdata4['question_id'], 'ques_val_1', 'ans_val1', $get_total_learner);
                                          echo ( ! empty($missedQues4['percentage'])) ? $missedQues4['percentage'] : 0; ?>%</a>
                                      </h4>
                                    </div>
                                    <div id="nested-collapse<?php echo $ques3 ?>" class="panel-collapse collapse">
                                      <div class="panel-body">
                                        <table class="table table-striped">
                                          <thead class="Theader">
                                            <tr>
                                              <th>User Name</th>
                                              <th>Full Name</th>
                                              <th>Location</th>
                                              <th>Company</th>
                                              <th>Unit</th>
                                            </tr>
                                          </thead>
                                          <tbody>
                                          <?php if ( ! empty($missedQues4['user_data'])):
                                              foreach ($missedQues4['user_data'] as $udata): ?>
                                                <tr>
                                                  <td><?php echo $udata['username'] ?></td>
                                                  <td><?php echo $udata['full_name'] ?></td>
                                                  <td><?php echo $udata['location'] ?></td>
                                                  <td><?php echo $udata['company'] ?></td>
                                                  <td><?php echo $udata['department'] ?></td>
                                                </tr>
                                            <?php endforeach; endif; ?>
                                          </tbody>
                                        </table>
                                      </div>
                                    </div>
                                  </div>
                                <?php $ques3++; endforeach; ?>
                              </div>
                            </div>
                          </div>
                        </div>
                      <?php endif; ?>

                      <?php if ( ! empty($comData['comp_col_5']) && ! empty($comData['comp_val_5'])): ?>
                        <div class="panel panel-default">
                          <div class="panel-heading">
                            <h4 class="panel-title"> <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse5"><strong>5. Competency :</strong> <?php echo $comData['comp_col_5'] ?></a> </h4>
                          </div>
                          <div id="collapse5" class="panel-collapse collapse">
                            <div class="panel-body">
                              <div class="panel-group" id="nested">
                                <?php $ques4 = $ques3; $missedQues5 = 0;
                                foreach ($db->getQuestionsByScenario(md5($scenario)) as $quesdata5): ?>
                                  <div class="panel panel-default">
                                    <div class="panel-heading">
                                      <h4 class="panel-title">
                                        <a class="collapsed" data-toggle="collapse" data-parent="#nested" href="#nested-collapse<?php echo $ques4 ?>">
                                          <?php echo $quesdata5['questions'] ?> &nbsp;
                                          <?php $missedQues5 = $db->GetPercentageMissedQues($temp_type, $getLastAttempt, $quesdata5['question_id'], 'ques_val_1', 'ans_val1', $get_total_learner);
                                          echo (!empty($missedQues5['percentage'])) ? $missedQues5['percentage'] : 0; ?>%</a>
                                      </h4>
                                    </div>
                                    <div id="nested-collapse<?php echo $ques4 ?>" class="panel-collapse collapse">
                                      <div class="panel-body">
                                        <table class="table table-striped">
                                          <thead class="Theader">
                                            <tr>
                                              <th>User Name</th>
                                              <th>Full Name</th>
                                              <th>Location</th>
                                              <th>Company</th>
                                              <th>Unit</th>
                                            </tr>
                                          </thead>
                                          <tbody>
                                          <?php if ( ! empty($missedQues5['user_data'])):
                                              foreach ($missedQues5['user_data'] as $udata): ?>
                                                <tr>
                                                  <td><?php echo $udata['username'] ?></td>
                                                  <td><?php echo $udata['full_name'] ?></td>
                                                  <td><?php echo $udata['location'] ?></td>
                                                  <td><?php echo $udata['company'] ?></td>
                                                  <td><?php echo $udata['department'] ?></td>
                                                </tr>
                                            <?php endforeach; endif; ?>
                                          </tbody>
                                        </table>
                                      </div>
                                    </div>
                                  </div>
                                <?php $ques4++; endforeach; ?>
                              </div>
                            </div>
                          </div>
                        </div>
                      <?php endif; ?>

                      <?php if ( ! empty($comData['comp_col_6']) && ! empty($comData['comp_val_6'])): ?>
                      <div class="panel panel-default">
                          <div class="panel-heading">
                            <h4 class="panel-title"> <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse6"><strong>6. Competency :</strong> <?php echo $comData['comp_col_6'] ?></a></h4>
                          </div>
                          <div id="collapse6" class="panel-collapse collapse">
                            <div class="panel-body">
                              <div class="panel-group" id="nested">
                                <?php $ques5 = $ques4; $missedQues6 = 0;
                                foreach ($db->getQuestionsByScenario(md5($scenario)) as $quesdata6) : ?>
                                  <div class="panel panel-default">
                                    <div class="panel-heading">
                                      <h4 class="panel-title">
                                        <a class="collapsed" data-toggle="collapse" data-parent="#nested" href="#nested-collapse<?php echo $ques5 ?>">
                                          <?php echo $quesdata6['questions'] ?> &nbsp;
                                          <?php $missedQues6 = $db->GetPercentageMissedQues($temp_type, $getLastAttempt, $quesdata6['question_id'], 'ques_val_1', 'ans_val1', $get_total_learner);
                                          echo ( ! empty($missedQues6['percentage'])) ? $missedQues6['percentage'] : 0; ?>%</a>
                                      </h4>
                                    </div>
                                    <div id="nested-collapse<?php echo $ques5 ?>" class="panel-collapse collapse">
                                      <div class="panel-body">
                                        <table class="table table-striped">
                                          <thead class="Theader">
                                            <tr>
                                              <th>User Name</th>
                                              <th>Full Name</th>
                                              <th>Location</th>
                                              <th>Company</th>
                                              <th>Unit</th>
                                            </tr>
                                          </thead>
                                          <tbody>
                                          <?php if ( ! empty($missedQues6['user_data'])):
                                              foreach ($missedQues6['user_data'] as $udata): ?>
                                                <tr>
                                                  <td><?php echo $udata['username'] ?></td>
                                                  <td><?php echo $udata['full_name'] ?></td>
                                                  <td><?php echo $udata['location'] ?></td>
                                                  <td><?php echo $udata['company'] ?></td>
                                                  <td><?php echo $udata['department'] ?></td>
                                                </tr>
                                            <?php endforeach; endif; ?>
                                          </tbody>
                                        </table>
                                      </div>
                                    </div>
                                  </div>
                                <?php $ques5++; endforeach; ?>
                              </div>
                            </div>
                          </div>
                        </div>
                      <?php endif; ?>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <?php endif; ?>
        </div>
      </div>
    </div>
  </div>
</div>
<style type="text/css">
  @media screen and (-ms-high-contrast: active),
  (-ms-high-contrast: none) {
    p.circleChart_text {
      left: 350%;
    }
  }

  p.circleChart_text {
    font-size: 25px !important;
  }

  .comp-incomp-topbanner {
    background: #fff;
    min-height: 429px;
    border-radius: 0px;
    margin-bottom: 20px;
    padding: 20px;
    border: 1px solid #e2e2e2;
    position: relative;
  }

  .lernar-dashboard-boxes {
    padding: 10px;
  }

  .lernar-dashboard-boxes span {
    display: table-cell;
    font-size: 40px;
    vertical-align: middle;
  }

  .lernar-dashboard-boxes p {
    padding-left: 10px;
  }

  @media(max-width: 1450px) and (min-width: 1024px) {
    .lernar-dashboard-boxes p {
      padding-right: 0px;
    }
  }

  .wordwrap {
    white-space: pre-wrap;
    white-space: -moz-pre-wrap;
    white-space: -pre-wrap;
    white-space: -o-pre-wrap;
    word-wrap: break-word;
  }

  canvas.canvasjs-chart-canvas {
    padding-top: 0px;
  }
</style>
<script type="text/javascript" src="content/js/bootstrap-datetimepicker.js" charset="UTF-8"></script>
<script src="content/js/tooltip.js"></script>
<script type="text/javascript">
  $(document).ready(function() {
    $('.form_datetime').datetimepicker({
      weekStart: 1,
      todayBtn:  0,
      autoclose: 1,
      todayHighlight: 1,
      startView: 2,
      forceParse: 0,
      minView: 2,
      showMeridian: 1,
      clearBtn: 1,
      endDate:new Date()
    });    
    var $active = true;
    $('.collapse-init').on('click', function() {
      if ( ! $active) {
        $active = true;
        $('.panel-title > a').attr('data-toggle', 'collapse');
        $('.panel-collapse').collapse('hide');
        $(this).html('Expand All');
      } else {
        $active = false;
        $('.panel-collapse').collapse('show');
        $('.panel-title > a').attr('data-toggle', '');
        $(this).html('Collapse All');
      }
    });

    $('#educator').on('change', function() {
      var selected = $(this).find("option:selected");
      var arrSelected = [];
      selected.each(function() {
        arrSelected.push($(this).html());
      });
      $('#get_educator').val(arrSelected);
    });

    $('#report').on('change', function() {
      var id = $(this).val();
      if (id == 1) window.location.href = 'admin-report.php';
      if (id == 2) window.location.href = 'group-performance-report.php';
    });
  });

  $('.multiselection').multiselect({
    disableIfEmpty: true,
    includeSelectAllOption: true,
    filterPlaceholder: 'Search & Select',
    enableCaseInsensitiveFiltering: true,
    enableFiltering: true,
    buttonWidth: '250px',
    maxHeight: 250
  });

  $('.filter_type').on('change', function() {
    var ftype = $(this).val();
    $('#educator,#learner,#unit,#location,#designation').multiselect("deselectAll", false);
    $('#educator,#learner,#unit,#location,#designation').multiselect('refresh');
    $('#educator,#learner,#unit,#location,#designation').multiselect('disable');
    if (ftype == 1) {
      $('#educator,#learner,#unit,#location,#designation').multiselect('disable');
    } else if (ftype == 2) {
      $('#educator').multiselect('enable');
    } else if (ftype == 3) {
      $('#learner').multiselect('enable');
    } else if (ftype == 4) {
      $('#unit').multiselect('enable');
    } else if (ftype == 5) {
      $('#location').multiselect('enable');
    } else if (ftype == 6) {
      $('#designation').multiselect('enable');
    }
  });

  $('#category').on('change', function() {
    var catid = $(this).val();
    if (catid != '')
      $("#report_form").LoadingOverlay("show");
      $('#scenario').empty();
      $('#scenario').multiselect('refresh');
      $('#System_Wise').prop("checked", true);
      $('#filter_educator,#filter_learner,.other_filter').attr('disabled', true);
      $('#educator,#learner,#unit,#location,#designation').empty();
      $('#educator,#learner,#unit,#location,#designation').multiselect("deselectAll", false);
      $('#educator,#learner,#unit,#location,#designation').multiselect('refresh');
      $('#educator,#learner,#unit,#location,#designation').multiselect('disable');
      $.getJSON('includes/ajax.php?getScenarioUpdate=true&catid=' + catid, function(res) {
        $("#report_form").LoadingOverlay("hide", true);
        $('#scenario').empty();
        if (res.success == true) {
          $('#scenario').multiselect('dataprovider', res.data);
        } else if (res.success == false) {
          $('#scenario').multiselect('rebuild');
          $('#scenario').multiselect('refresh');
        }
      });
  });
  
  /* SIM LIST */
  var maxCount = 1;
  $('#scenario').multiselect({
    disableIfEmpty: true,
    filterPlaceholder: 'Search & Select Simulations',
    enableCaseInsensitiveFiltering: true,
    enableFiltering: true,
    maxHeight: 250,
    numberDisplayed: 1,
    includeSelectAllOption: false,
    onChange: function(option, checked, select) {
      $.LoadingOverlay("show");
      var selectID = $(option).parent().attr('id');

      //Get selected count
      var selectedCount = $('#'+ selectID).find("option:selected").length;
      
      //If the selected count is equal to the max allowed count, then disable any unchecked boxes
      if (selectedCount >= maxCount) {
        $('#'+ selectID +" option:not(:selected)").prop('disabled', true);
        $('#'+ selectID).multiselect('refresh');
        var sim_id = $('#'+ selectID).find("option:selected").val();
        $('#System_Wise').prop("checked", true);
        $('#educator,#learner,#unit,#location,#designation').empty();
        $('#educator,#learner,#unit,#location,#designation').multiselect("deselectAll", false);
        $('#educator,#learner,#unit,#location,#designation').multiselect('refresh');
        $('#educator,#learner,#unit,#location,#designation').multiselect('disable');
        $.getJSON('includes/ajax.php?get_system_reporting_other_data=true&sim_id=' + sim_id, function(res) {
          if (res.success == true) {
            $.LoadingOverlay("hide");
            if (res.assign_group == true) {
              $('#filter_educator,.other_filter').removeAttr('disabled', true);
              $('#filter_learner').attr('disabled', true);
              $('#educator').multiselect('dataprovider', res.data);
              $('#unit').multiselect('dataprovider', res.unit);
              $('#location').multiselect('dataprovider', res.location);
              $('#designation').multiselect('dataprovider', res.designation);
              $('#educator,#learner,#unit,#location,#designation').multiselect('disable');
            } else if (res.assign_group == false) {
              $('#filter_learner,.other_filter').removeAttr('disabled', true);
              $('#filter_educator').attr('disabled', true);
              $('#learner').multiselect('dataprovider', res.data);
              $('#unit').multiselect('dataprovider', res.unit);
              $('#location').multiselect('dataprovider', res.location);
              $('#designation').multiselect('dataprovider', res.designation);
              $('#educator,#learner,#unit,#location,#designation').multiselect('disable');
            }
          } else {
            $.LoadingOverlay("hide");
            swal({text: 'Simulation data not found.', buttons: false, icon: "warning", timer: 4000});
            $('#System_Wise').prop("checked", true);
            $('#filter_educator,#filter_learner,.other_filter').attr('disabled', true);
            $('#educator,#learner,#unit,#location,#designation').multiselect("deselectAll", false);
            $('#educator,#learner,#unit,#location,#designation').multiselect('refresh');
            $('#educator,#learner,#unit,#location,#designation').multiselect('disable');
          }
        });
      }
      else {
        $.LoadingOverlay("hide");
        $('#'+ selectID +" option:disabled").prop('disabled', false);
        $('#'+ selectID).multiselect('refresh');
        $('#System_Wise').prop("checked", true);
        $('#filter_educator,#filter_learner,.other_filter').attr('disabled', true);
        $('#educator,#learner,#unit,#location,#designation').multiselect("deselectAll", false);
        $('#educator,#learner,#unit,#location,#designation').multiselect('refresh');
        $('#educator,#learner,#unit,#location,#designation').multiselect('disable');
      }
    }
  });
</script>
<?php 
unset($_SESSION['msg']);
require_once 'includes/footer.php';
