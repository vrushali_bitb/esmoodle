<?php 
ob_start();
session_start();
if (isset($_SESSION['username'])) {
    $user   = $_SESSION['username'];
    $role   = $_SESSION['role'];
    $userid = $_SESSION['userId'];
}
else {
    header('location: index.php');
}
require_once 'config/db.class.php';
require_once 'includes/learners_menu.php';
require_once 'config/pagination.php';
$db					= new DBConnection();
$type_icon_path		= 'img/type_icon/';
$action_icon_path	= 'img/list/'; ?>
<div class="bottomheader">
    <div class="container-fluid">
        <ul class="breadcrumb">
			<?php echo $db->breadcrumbs(); ?>
        </ul>
     </div>	
</div>
<div class="Group_managment">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
               <div class="usermanage_main">
               		<div class="table-box">
                    	<div class="tableheader clearfix">
                        	<div class="tableheader_left">New Simulations</div>
                            <div class="tableheader_right">
                            	<div class="Searchbox">
                                    <form method="get">
										<input type="text" class="serchtext" name="serchnew" placeholder="Search" required>
										<div class="serchBTN">
											<img class="img-responsive" src="img/dash_icon/search.svg">
										</div>
									</form>
                                </div>
                                <div class="btn-group btn-shortBY">
                                    <button type="button" class="btn btn-Transprate"><span class="sort">Sort by</span></button>
                                    <button type="button" class="btn btn-Transprate dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img  src="img/dash_icon/Drop_Dwon.png"></button>
                                    <div class="dropdown-menu shortBYMenu">
                                        <a class="dropdown-item" href="<?php echo $db->getBaseUrl($db->getFile()); ?>">Default Sorting</a>
                                        <a class="dropdown-item" href="<?php echo $db->getBaseUrl($db->getFile().'?norderby=ntype'); ?>">Type</a>
                                        <a class="dropdown-item" href="<?php echo $db->getBaseUrl($db->getFile().'?norderby=nstatus'); ?>">Status</a>
                                    </div>
                                </div>
                            </div>
                    	</div>
                    </div>
                 	<div class="usermanage-form1">
                    	<div class="table-responsive">
						<?php 
							#----------Serch----------------
							$search = '';
							if (isset($_GET['serchnew']) && ! empty($_GET['serchnew'])):
								extract($_GET);
								$search = " AND ( c.category LIKE '%$serchnew%' OR s.Scenario_title LIKE '%$serchnew%' )";
							endif;
							#----------Sort by----------------
							if (isset($_GET['norderby']) && $_GET['norderby'] == 'ntype'):
								$norderby = (( ! empty($search)) ? $search : " ")." ORDER BY s.author_scenario_type";
							elseif (isset($_GET['norderby']) && $_GET['norderby'] == 'nstatus'):
								$norderby = (( ! empty($search)) ? $search : " ")." ORDER BY s.status";
							else:
								$norderby = (( ! empty($search)) ? $search : " ")." ORDER BY s.scenario_id DESC";
							endif;
							$nsqlr      = "SELECT DISTINCT s.scenario_id, s.Scenario_title, s.assign_start_date, s.assign_end_date, s.assign_reviewer, s.scenario_type, s.scenario_category, s.created_on, c.category, st.status_name FROM scenario_master s INNER JOIN group_tbl g LEFT JOIN category_tbl c ON s.scenario_category = c.cat_id LEFT JOIN scenario_status st ON s.status = st.sid WHERE s.author_scenario_type = '0' AND s.sim_ques_type = '0' AND (CASE WHEN s.assign_author IS NOT NULL THEN FIND_IN_SET('". $userid ."', s.assign_author) WHEN s.assign_group IS NOT NULL THEN FIND_IN_SET('". $userid ."', g.author) END) $norderby";
							$q          = $db->prepare($nsqlr); $q->execute();
							$rowCount   = $q->rowCount();
							$count	    = ($nsqlr) ? $rowCount : 0;
							$npage      = (isset($_REQUEST['npage']) && ! empty($_REQUEST['npage'])) ? $_REQUEST['npage'] : 1;
							$show  	    = 15;
							$start 	    = ($npage - 1) * $show;
							$pagination = getPaginationString($count, $show, $npage, 1, basename(__FILE__), '?npage=');
							$nsql       = $nsqlr . " LIMIT $start, $show";
							$nresults   = $db->prepare($nsql); ?>
                        	<table class="table table-striped">
                                <thead class="Theader">
                                    <tr>
                                        <th>Category Name</th>
                                        <th>Simulation Title</th>
                                        <th>Creation Date</th>
                                        <th>Start Date</th>
                                        <th>End Date</th>
                                        <th>Simulation Type</th>
                                        <th>Reviewer Name</th>
                                        <th>Status</th>
                                        <th>Create</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php if ($nresults->execute() && $rowCount > 0):
									foreach ($nresults->fetchAll(PDO::FETCH_ASSOC) as $row): ?>
                                    <tr>
                                        <td><?php echo $row['category']; ?></td>
                                        <td title="<?php echo $row['Scenario_title']; ?>"><?php echo $db->truncateText($row['Scenario_title'], 40); ?></td>
                                        <td><?php echo $db->dateFormat($row['created_on'], 'd-M-y'); ?></td>
                                        <td><?php echo ($row['assign_start_date'] != '0000-00-00') ? $db->dateFormat($row['assign_start_date'], 'd-M-y') : '--' ?></td>
                                        <td><?php echo ($row['assign_end_date'] != '0000-00-00') ? $db->dateFormat($row['assign_end_date'], 'd-M-y') : '--' ?></td>
                                        <td><?php foreach ($db->getMultiScenarioType($row['scenario_type']) as $scenario_type): ?>
											<img src="<?php echo $type_icon_path . $scenario_type['type_icon'] ?>" class="type_icon" title="<?php echo $scenario_type['type_name'] ?>" />
											<?php endforeach; ?>
										</td>
                                        <td><?php if ( ! empty($row['assign_reviewer'])): ?>
											<a data-toggle="popover" title="View Reviewer" href="<?php echo $db->getBaseUrl('includes/ajax.php?getTooltipData=true&ids='. $row['assign_reviewer']); ?>" onclick="return false"><i class="fa fa-info-circle fa-2x" aria-hidden="true"></i></a>
											<?php endif; ?>
										</td>
										<td><strong><?php echo $row['status_name']; ?></strong></td>
                                        <td><a class="tooltiptop" href="create-simulation.php?createsim=true&sim_id=<?php echo md5($row['scenario_id']); ?>"><span class="tooltiptext">Create Simulation</span><img class="svg" src="img/list/create_sim.svg"></a></td>
                                    </tr>
                                    <?php endforeach; else: ?>
                                    <tr><td colspan="9" style="text-align:center"><strong>No data available in database.</strong></td></tr>
									<?php endif; ?>
                                </tbody>
                             </table>
                        </div>
                        <div class="pull-right">
                        	<nav aria-label="navigation"><?php echo ( ! empty($pagination)) ? $pagination : '&nbsp;'; ?></nav>
                        </div>
                    </div>
               </div>
               <div class="usermanage_main">
               		<div class="table-box">
                    	<div class="tableheader clearfix">
                        	<div class="tableheader_left">Assigned Simulation</div>
                            <div class="tableheader_right">
                            	<div class="Searchbox">
                                	<form method="get">
										<input type="text" class="serchtext" name="searchkey" placeholder="Search" required>
										<div class="serchBTN">
											<img class="img-responsive" src="img/dash_icon/search.svg">
										</div>
									</form>
                                </div>
                                <div class="btn-group btn-shortBY">
                                    <button type="button" class="btn btn-Transprate"><span class="sort">Sort by</span></button>
                                    <button type="button" class="btn btn-Transprate dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img  src="img/dash_icon/Drop_Dwon.png"></button>
                                    <div class="dropdown-menu shortBYMenu">                                	
                                    <a class="dropdown-item" href="<?php echo $db->getBaseUrl($db->getFile()); ?>">Default Sorting</a>
                                    <a class="dropdown-item" href="<?php echo $db->getBaseUrl($db->getFile().'?orderby=type'); ?>">Type</a>
                                    <a class="dropdown-item" href="<?php echo $db->getBaseUrl($db->getFile().'?orderby=status'); ?>">Status</a>
                                  </div>
                                </div>
                           </div>
                    	</div>
                  	</div>
                    <div class="usermanage-form1">
                    	<div class="table-responsive">
						<?php 
							#----------Serch----------------
							$searchText = '';
							if (isset($_GET['searchkey']) && ! empty($_GET['searchkey'])):
								extract($_GET);
								$searchText = " AND ( c.category LIKE '%$searchkey%' OR s.Scenario_title LIKE '%$searchkey%' )";
							endif;
							#----------Sort by----------------
							if (isset($_GET['orderby']) && $_GET['orderby'] == 'type'):
								$orderby = (( ! empty($searchText)) ? $searchText : " ")." ORDER BY s.author_scenario_type";
							elseif (isset($_GET['orderby']) && $_GET['orderby'] == 'nstatus'):
								$orderby = (( ! empty($searchText)) ? $searchText : " ")." ORDER BY s.status";
							else:
								$orderby = (( ! empty($searchText)) ? $searchText : " ")." ORDER BY s.scenario_id DESC";
							endif;
							$sqlr           = "SELECT DISTINCT s.scenario_id, s.Scenario_title, s.assign_start_date, s.assign_end_date, s.assign_reviewer, s.scenario_type, s.scenario_category, s.duration, s.status , s.created_on, s.scenario_xm_file, s.sim_ques_type, s.sim_temp_type, s.update_sim, c.category, st.status_name, t.type_name, t.type_icon FROM scenario_master s INNER JOIN group_tbl g LEFT JOIN category_tbl c ON s.scenario_category = c.cat_id LEFT JOIN scenario_status st ON s.status = st.sid LEFT JOIN scenario_type t ON s.author_scenario_type = t.st_id WHERE s.author_scenario_type != '0' AND (CASE WHEN s.assign_author IS NOT NULL THEN FIND_IN_SET('". $userid ."', s.assign_author) WHEN s.assign_group IS NOT NULL THEN FIND_IN_SET('". $userid ."', g.author) END) $orderby";
							$q              = $db->prepare($sqlr); $q->execute();
                            $rowCount       = $q->rowCount();
                            $count	        = ($sqlr) ? $rowCount : 0;
                            $page           = (isset($_REQUEST['page']) && ! empty($_REQUEST['page'])) ? $_REQUEST['page'] : 1;
                            $show  	        = 15;
                            $start 	        = ($page - 1) * $show;
                            $npagination    = getPaginationString($count, $show, $page, 1, basename(__FILE__), '?page=');
                            $sql            = $sqlr ." LIMIT $start, $show";
							$results        = $db->prepare($sql); ?>
                            <table class="table table-striped">
                                <thead class="Theader">
                                    <tr>
                                        <th>Category Name</th>
                                        <th>Simulation Title</th>
                                        <th>Creation Date</th>
                                        <th>Start Date</th>
                                        <th>End Date</th>
                                        <th>Reviewer Name</th>
                                        <th>Duration in min</th>
										<th>Total Question</th>
                                        <th>Simulation Type</th>
                                        <th>Stage</th>
										<th colspan="5">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php if ($results->execute() && $rowCount > 0):
                                    foreach ($results->fetchAll(PDO::FETCH_ASSOC) as $nrow): 
                                        $sim_id = md5($nrow['scenario_id']); ?>
                                    <tr>
                                        <td><?php echo $nrow['category']; ?></td>
                                        <td title="<?php echo $nrow['Scenario_title']; ?>"><?php echo $db->truncateText($nrow['Scenario_title'], 40); ?></td>
                                        <td><?php echo $db->dateFormat($nrow['created_on'], 'd-M-y'); ?></td>
                                        <td><?php echo ($nrow['assign_start_date'] != '0000-00-00') ? $db->dateFormat($nrow['assign_start_date'], 'd-M-y') : '--' ?></td>
                                        <td><?php echo ($nrow['assign_end_date'] != '0000-00-00') ? $db->dateFormat($nrow['assign_end_date'], 'd-M-y') : '--' ?></td>
                                        <td><?php if ( ! empty($nrow['assign_reviewer'])): ?><a data-toggle="popover" title="View Reviewer" href="<?php echo $db->getBaseUrl('includes/ajax.php?getTooltipData=true&ids='.$nrow['assign_reviewer']); ?>" onclick="return false"><i class="fa fa-info-circle fa-2x" aria-hidden="true"></i></a><?php endif; ?></td>
                                        <td><?php echo ( ! empty($nrow['duration'])) ? $nrow['duration'] : 0; ?></td>
                                        <td><?php echo $db->getTotalQuestionsByScenario($sim_id); ?></td>
										<td><img src="<?php echo $type_icon_path . $nrow['type_icon'] ?>" class="type_icon" title="<?php echo $nrow['type_name'] ?>" /></td>
                                        <td><strong><?php echo $nrow['status_name']; ?></strong></td>
										<td><a class="tooltiptop update_stage" data-scenario-id="<?php echo $sim_id ?>" href="javascript:void(0);"><span class="tooltiptext">Update Stage</span><img class="svg web_icon" src="<?php echo $action_icon_path ?>update_stage.svg"></a></td>
                                        <td><a class="tooltiptop" href="<?php echo $db->getEditAuthPage($nrow['sim_ques_type'], $nrow['sim_temp_type'], $sim_id); ?>"><span class="tooltiptext">Edit Simulation</span><img class="svg" src="<?php echo $action_icon_path ?>create_sim.svg" /></a></td>
                                        <td><a class="tooltiptop" href="<?php echo $db->getPreviewAuthPage($nrow['sim_ques_type'], $nrow['sim_temp_type'], $sim_id); ?>" target="_blank"><span class="tooltiptext">Preview</span><img class="svg" src="<?php echo $action_icon_path ?>preview1.svg"></a></td>
                                        <td><a class="tooltiptop clone_sim" data-scenario-id="<?php echo $sim_id ?>" href="javascript:void(0);"><span class="tooltiptext">Duplicate</span><img class="svg web_icon" src="<?php echo $action_icon_path ?>duplicate Sim.svg"></a></td>
                                        <td><a class="tooltipLeft" href="review-simulation-details.php?review=true&sim_id=<?php echo $sim_id; ?>" target="_blank"><span class="tooltiptext">Review</span><img class="svg" src="<?php echo $action_icon_path; ?>review_list.svg"></a></td>
                                    </tr>
                                    <?php endforeach; else: ?>
									<tr><td colspan="14" style="text-align:center"><strong>No data available in database.</strong></td></tr>
                                    <?php endif; ?>
                                </tbody>
                            </table>
                        </div>
                        <div class="pull-right">
                            <nav aria-label="navigation"><?php echo ( ! empty($npagination)) ? $npagination : '&nbsp;'; ?></nav>
                        </div>
                    </div>
              	</div>
            </div>
        </div>
    </div>
</div>

<div id="load_popup_modal_show" class="modal fade" tabindex="-1"></div>

<script type="text/javascript">
$(function () {
  $('[data-toggle="tooltip"]').tooltip()
});

$('.update_stage').click(function () {
	var id = $(this).attr('data-scenario-id');
	if (id != '') {
		$.LoadingOverlay("show");
		var $modal = $('#load_popup_modal_show');
		$modal.load('edit-scenario-stage-modal.php', {'data-id': id }, function(res) {
			if (res != '') {
				$.LoadingOverlay("hide");
				$modal.modal('show');
			}
			else {
				$.LoadingOverlay("hide");
				swal("Error", 'Oops. something went wrong please try again.?', "error");
			}
		});
	}
});

$('.edit').click(function () {
	var id = $(this).attr('data-scenario-id');
	if (id != '') {
		$.LoadingOverlay("show");
		var $modal = $('#load_popup_modal_show');
		$modal.load('edit-scenario-author-modal.php', {'data-id': id }, function(res) {
			if (res != '') {
				$.LoadingOverlay("hide");
				$modal.modal('show');
			}
			else {
				$.LoadingOverlay("hide");
				$("#showmsg").hide();
				swal("Error", 'Oops. something went wrong please try again.?', "error");
			}
		});
	}
});

$('.clone_sim').click(function () {
    var sim_id 		= $(this).attr('data-scenario-id');
	var dataString	= 'sim_id='+ sim_id + '&clone_sim='+ true;
    if (sim_id != '') {
		swal({
			title: "Are you sure?",
			text: "Clone this Simulation.",
			icon: "warning",
			buttons: [true, 'Clone'],
			dangerMode: true, }).then((willDelete) => { if (willDelete) {
				$.LoadingOverlay("show");
				$.ajax({
					url: "includes/process.php",
					type: "POST",
					data: dataString,
					cache: false,
					success: function(resdata) {
						var res = $.parseJSON(resdata);
						$.LoadingOverlay("hide");
						if (res.success == false) {
							swal({text: res.msg, buttons: false, icon: "error", timer: 3000});
						}
						else if (res.success == true) {
							swal({text: res.msg, buttons: false, icon: "success", timer: 3000});
							setTimeout(function() { window.location.reload(); }, 3000);
						}
					},error: function() {
						$.LoadingOverlay("hide");
						swal({text: oopsmsg, buttons: false, icon: "error", timer: 3000});
					}
				});
			}
		});
	}
});
</script>
<script src="content/js/tooltip.js"></script>
<?php 
require_once 'includes/footer.php';
