# Steps to use CodeIgniter 4 Framework

#####      Requirements : PHP 7.3 and above  #######

1. Download latest version of (CodeIgniter 4 Framework) from below link,
   https://codeigniter.com/download

2. Created a "api" folder in the Easy Sim App.
3. Extract the above zip downloaded.
4. paste all the content to this "api" folder.
5. Open cmd and go to api folder.
   Example :  cd C:\xampp_php7_3\htdocs\easy-sim-app\api
6. Enable extensions, To enable extensions, verify that they are enabled in your .ini files:
    - C:\xampp_php7_3\php\php.ini
7. Add below command,
   composer update
8. Then below command,
    php spark serve
9. You should see below,

    CodeIgniter v4.1.4 Command Line Tool - Server Time: 2021-10-21 23:41:47 UTC-05:00

    CodeIgniter development server started on http://localhost:8080
    Press Control-C to stop.
