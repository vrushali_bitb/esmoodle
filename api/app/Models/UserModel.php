<?php namespace App\Models;

use CodeIgniter\Model;

class UserModel extends Model
{
    protected $table      = 'users';
    protected $primaryKey = 'id';

    protected $returnType     = 'array';
    protected $useSoftDeletes = false;

    protected $allowedFields = [
      'fname', 
      'lname', 
      'username', 
      'password', 
      'mob', 
      'email',
      'role',
      'location',
      'company', 
      'department',
      'designation',      
      'webpage',
      'facebook',
      'twitter',
      'linkedIn',
      'date_of_join',
      'img',
      'img_thumb_50',
      'date'
    ];

    protected $useTimestamps = true;
   // protected $createdField  = 'created_at';
    //protected $updatedField  = 'updated_at';    

    protected $validationRules    = [
      'fname'        => 'required',
      'username'     => 'required|min_length[6]|valid_email|is_unique[users.username]'
    ];
    
    protected $validationMessages = [
        'fname' => [
          'required' => 'Email cannot be empty'
        ],
        'username' => [
          'required' => 'Email cannot be empty',
          'valid_email' => 'Email format is not appropriate',
          'is_unique' => 'This email is already registered with us'
        ]
    ];

 
  
      

 
  
}