<?php namespace App\Models;

use CodeIgniter\Model;

class AssignmentModel extends Model
{
    protected $table      = 'assignment_tbl';
    protected $primaryKey = 'assignment_id';

    protected $returnType     = 'array';
    protected $useSoftDeletes = false;

    protected $allowedFields = [
      'add_date', 
      'lname', 
      'end_date', 
      'group_id', 
      'learner_id', 
      'no_attempts',
      'scenario_id',
      'start_date',
      'user_id'
    ];

    protected $useTimestamps = true;
   // protected $createdField  = 'created_at';
    //protected $updatedField  = 'updated_at';    

   

 
  
}