<?php namespace App\Models;

use CodeIgniter\Model;

class GroupModel extends Model
{
    protected $table      = 'group_tbl';
    protected $primaryKey = 'group_id';

    protected $returnType     = 'array';
    protected $useSoftDeletes = false;

    protected $allowedFields = [
      'author', 
      'group_date', 
      'group_des', 
      'group_icon', 
      'group_leader', 
      'group_name',
      'learner',
      'reviewer',
      'status', 
      'uid'
    ];

    protected $useTimestamps = true;
   // protected $createdField  = 'created_at';
    //protected $updatedField  = 'updated_at';    

   

 
  
}