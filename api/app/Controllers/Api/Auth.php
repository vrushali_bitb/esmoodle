<?php namespace App\Controllers\Api;

header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Methods: GET, OPTIONS");

use CodeIgniter\RESTful\ResourceController;

class Auth extends ResourceController
{
  protected $allowed = [
    'post' => ['login','register','thirdPartyLogin', 'thirdLogin'],
    'get' => ['dwnd']
    
  ];

  public function __construct(){        
    $this->users= model('App\Models\UserModel'); 
    $this->group= model('App\Models\GroupModel'); 
    $this->assignment= model('App\Models\AssignmentModel'); 
    $this->session = session();   
  }

  public function thirdPartyLogin()
  {
    //echo "hi";die;
    $ret = ['success'=> false,'flag'=> '0', 'data' => [], 'message' => []];
    //echo $this->request->getPost();die;
    $data = $this->users
     ->where('username',$this->request->getPost('username'))
     ->where('password',md5($this->request->getPost('password')))
     ->where('status','1')
     ->find();
    //echo json_encode($data);die;
    if(isset($data[0])){
      $ret['success'] = true;
      $ret['message'] = ['Logging in.. please wait..'];

      $userData = [
        'username'  => $this->request->getPost('username'),
        'userId'     => $data['id'],
        'role' => 'learner',
      ];     
      $this->session->set($userData);
      $ret['url'] = 'learner-dashboard.php';
      echo json_encode($data);die;
    }else{
          ///Register//////////
         
    }
    echo json_encode($ret);
  }
/// Get User & simulations data///////////
  public function getUserData()
  {
    
    $ret = ['success'=> false,'flag'=> '0', 'data' => [], 'message' => []];
    //echo $this->request->getPost();die;


    // $data = $this->users
    //  ->where('username',$this->request->getPost('username'))
    //  ->where('password',md5($this->request->getPost('password')))
    //  ->where('status','1')
    //  ->find();

     $data = $this->users
     ->where('username',$this->request->getPost('username'))
     ->where('password',md5($this->request->getPost('password')))
     ->where('status','1')
     ->find();
     
     $user_id = 2;//neel
     $dataGrp = $this->group
     ->select('group_id')
     ->where('find_in_set("'.$user_id.'", learner) <> 0')
     ->findAll();
     //echo json_encode($dataGrp) ;die;

     $newArray = array();
 
     //echo var_dump($dataGrp) ;die;
     foreach($dataGrp as $key => $value) {
      //echo $key . " => " . $value . "<br>";
      $newArray[] = $value['group_id'];
    }
    //echo json_encode($newArray) ;die;
     $data1 = $this->assignment
     ->whereIn('assignment_tbl.group_id',$newArray)
     ->join('group_tbl','group_tbl.group_id = assignment_tbl.group_id')
     ->findAll();

     echo json_encode($data1) ;die;

    echo json_encode($newArray);die;
    //echo json_encode($dataGrp[0]['group_id']);die;
    if(isset($data[0])){
      $ret['success'] = true;
      $ret['data'] = $data;     
    }else{
      $ret['message'] = 'User does not exists...';
    }
    echo json_encode($ret);
  }


  public function dwnd()
  {
    
    //Read the filename
    $filename = 'D:\vrushali\KS\projects\svasu-react\svasu\api\writable\published_scorms\user21\test.zip';
    //Check the file exists or not
    if(file_exists($filename)) {
    
    //Define header information
    header('Content-Description: File Transfer');
    header('Content-Type: application/octet-stream');
    header("Cache-Control: no-cache, must-revalidate");
    header("Expires: 0");
    header('Content-Disposition: attachment; filename="'.basename($filename).'"');
    header('Content-Length: ' . filesize($filename));
    header('Pragma: public');
    
    //Clear system output buffer
    flush();
    
    //Read the size of the file
    readfile($filename);
    
    //Terminate from the script
    die();
    }
    else{
    echo "File does not exist.";
    }
   
  }


  



}