<?php 
ini_set('max_execution_time', 300);
session_start();
if (isset($_SESSION['username'])):
	$user   = $_SESSION['username'];
	$role 	= $_SESSION['role'];
	$userid	= $_SESSION['userId'];
else:
    header('location: index.php');
endif;

if ($_SERVER['REQUEST_METHOD'] == 'POST' && ! empty($_POST['scenario_id'])):
	require_once 'config/db.class.php';
	require_once 'config/array-to-xml.php';
	$db  = new DBConnection();
	$sid = $_POST['scenario_id'];
	$sql = "SELECT s.scenario_id, s.Scenario_title AS title, s.image_url AS avatar, s.video_url AS video, s.audio_url AS audio, s.sim_back_img, s.sim_char_img, s.sim_char_left_img, s.sim_char_right_img, s.char_own_choice, com.comp_id, com.comp_col_1, com.comp_val_1, com.comp_col_2, com.comp_val_2, com.comp_col_3, com.comp_val_3, com.comp_col_4, com.comp_val_4, com.comp_col_5, com.comp_val_5, com.comp_col_6, com.comp_val_6 FROM 
			scenario_master s LEFT JOIN 
			competency_tbl com ON com.scenario_id = s.scenario_id WHERE 
			MD5(s.scenario_id) = '". $sid ."'";
    $results = $db->prepare($sql);
	if ($results->execute()):
		$sim_data = [];
		foreach ($results->fetchAll(PDO::FETCH_ASSOC) as $row):
			$sim_data = array('sid' 		=> $row['scenario_id'],
							  '#title'		=> $row['title'],
							  'avatar' 		=> $row['avatar'],
							  'video' 		=> $row['video'],
							  'audio' 		=> $row['audio'],
							  'bg' 			=> $row['sim_back_img'],
							  'char'		=> $row['sim_char_img'],
							  'own_char'	=> $row['char_own_choice'],
							  'lchar'		=> $row['sim_char_left_img'],
							  'rchar'		=> $row['sim_char_right_img'],
							  'comp_1' 		=> $row['comp_col_1'],
							  'comp_val_1'	=> $row['comp_val_1'],
							  'comp_2' 		=> $row['comp_col_2'],
							  'comp_val_2' 	=> $row['comp_val_2'],
							  'comp_3' 		=> $row['comp_col_3'],
							  'comp_val_3' 	=> $row['comp_val_3'],
							  'comp_4' 		=> $row['comp_col_4'],
							  'comp_val_4' 	=> $row['comp_val_4'],
							  'comp_5' 		=> $row['comp_col_5'],
							  'comp_val_5' 	=> $row['comp_val_5'],
							  'comp_6' 		=> $row['comp_col_6'],
							  'comp_val_6' 	=> $row['comp_val_6']);
			$qsql = "SELECT question_id, question_type as qtype, questions AS qlabel, speech_text, audio, video, screen, image, feedback, feed_speech_text, feed_audio, feed_video, feed_screen, feed_image, true_options, videoq_media_file, videoq_cue_point, ques_val_1, ques_val_2, ques_val_3, ques_val_4, ques_val_5, ques_val_6 FROM question_tbl WHERE MD5(scenario_id) = '". $sim_id ."'";
			$qresults = $db->prepare($qsql);
			if ($qresults->execute()):
				$i = 1;
				foreach ($qresults->fetchAll(PDO::FETCH_ASSOC) as $qrow):
					$qdata = array('qid'			=> $qrow['question_id'],
								   'qtype'			=> $qrow['qtype'],
								   '#qlabel'		=> $qrow['qlabel'],
								   'qtts'			=> $qrow['speech_text'],
								   'audio'			=> $qrow['audio'],
								   'video'			=> $qrow['video'],
								   'screen'			=> $qrow['screen'],
								   'image'			=> $qrow['image'],
								   'feedback'		=> $qrow['feedback'],
								   'ftts'			=> $qrow['feed_speech_text'],
								   'faudio'			=> $qrow['feed_audio'],
								   'fvideo'			=> $qrow['feed_video'],
								   'fscreen'		=> $qrow['feed_screen'],
								   'fimage'			=> $qrow['feed_image'],
								   'true_options'	=> $qrow['true_options'],
								   'videoq_file'	=> $qrow['videoq_media_file'],
								   'videoq_time'	=> $qrow['videoq_cue_point'],
								   'ques_val_1'		=> $qrow['ques_val_1'],
								   'ques_val_2' 	=> $qrow['ques_val_2'],
								   'ques_val_3' 	=> $qrow['ques_val_3'],
								   'ques_val_4' 	=> $qrow['ques_val_4'],
								   'ques_val_5' 	=> $qrow['ques_val_5'],
								   'ques_val_6'		=> $qrow['ques_val_6'],
								   'answers'		=> $db->getAnswers($qrow['question_id']));
					$question['question'. $i]	= $qdata;
					$sim_data['questions']		= $question;
					$i++;
				endforeach;
			endif;
		endforeach;
	endif;
	
	$xmldata['scenario'] = $sim_data;
	$xml 	= new ArrayToXML();
	$xdata	= $xml->buildXML($xmldata);
	$fname	= 'Simulation-'. $sid .'-'. date('d-M-y-h.i.s').'.xml';
	$path 	= 'xml/';
	
	/* Create folder if not exist */
	if ( ! is_dir($path)) mkdir($path);

	/* Generated XML */
	if (file_put_contents($path . $fname, $xdata) && file_exists($path. $fname)):
		$oldXmlFile = ( ! empty($db->getOldXml($sid))) ? $db->getOldXml($sid) : '';
		if ( ! empty($oldXmlFile) && file_exists($path. $oldXmlFile)):
			@unlink($path . $oldXmlFile);
		endif;
		$db->UpdateXml($fname, $sid);
		$resdata = array('success' => TRUE, 'msg' => 'XML Generated successfully. now this Simulation ready for SCORM or Web Object Published.');
	else:
		$resdata = array('success' => FALSE, 'msg' => 'XML not Generated. Please try again later?');
	endif;
	echo json_encode($resdata);
endif;
