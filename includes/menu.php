<?php 
$db 			    = new DBConnection();
$getUsers 		= ( ! empty($userid)) ? $db->getUsers(md5($userid)) : [];
$clientId	    = ( ! empty($_SESSION['client_id'])) ? $_SESSION['client_id'] : exit('Oops. something went wrong please try again');
$profileImg		= ( ! empty($getUsers['img_thumb_50'])) ? 'img/profile/'.$getUsers['img_thumb_50'] : 'img/profile.png';
$profileImg2  = ( ! empty($getUsers['img_thumb_125'])) ? 'img/profile/'.$getUsers['img_thumb_125'] : 'img/profile.png';
$clientData   = ( ! empty($clientId)) ? $db->getClientData("mhome,mbrand", "client_tbl", " WHERE client_id = ". $clientId)[0] : '';
$getSiteLogo	= ( ! empty($db->siteLogo)) ? $db->siteLogo : 'EasySIM_logo.svg';
$iconpath		  = $db->getBaseUrl('img/left_menu/');
$dashboard		= $category = $mhome = $mbrand = '';

/* Check Last Change Password */
if ($db->checkLastChangePassword($userid) == TRUE && $cur_page != 'change-password'):
	@header("location: change-password.php");
	exit;
endif;

/* Role wise URL */
if (isset($role) && $role == 'superAdmin'):
	$dashboard  = 'super-admin-dashboard.php';
elseif (isset($role) && $role == 'admin'):
  $dashboard	= 'admin-dashboard.php';
  $category	  = 'category-management.php';
  $mhome	    = 'home-page-management.php';
  $mbrand	    = 'manage-branding.php';
elseif (isset($role) && $role == 'author'):
	$dashboard	= 'author-dashboard.php';
elseif (isset($role) && $role == 'learner'):
	$dashboard	= 'learner-dashboard.php';
elseif (isset($role) && $role == 'reviewer'):
	$dashboard	= 'reviewer-dashboard.php';
elseif (isset($role) && $role == 'educator'):
	$dashboard	= 'educator-dashboard.php';
elseif (isset($role) && $role == 'clientAdmin'):
  $dashboard	= 'client-admin-dashboard.php';
endif; ?>
<header class="header <?php echo (isset($_SESSION['autologin'])) ? 'iframe-hide' : '' ?>">
  <div class="container-fluid">
    <div class="logo-brand">
    <a href="login"><img class="img-responsive logoImg white-svg-logo" src="img/logo/<?php echo $getSiteLogo; ?>"></a></div>
    <div class="menuToggle">
      <div class="userprofile">
        <span><?php echo (isset($user)) ? "Welcome, ". ucfirst($user)."!" : ''; ?></span>		
      	<img src="img/left_menu/user.svg">
      </div>
      <div class="toggleIcon menuopen"><span class="one"></span><span class="two"></span> <span class="three"></span></div>
    </div>
  </div>
</header>
<div class="menu">
  <div class="dropdownmenu">
    <div class="menuToggle menuToggleMB">
        <span><?php echo (isset($user)) ? "Welcome, ". ucfirst($user)."!" : ''; ?></span>
    	  <img src="<?php echo $iconpath ?>user.svg">
    </div>
    <div class="dropdiv3">
      <div class="col-sm-4">
      	<a href="<?php echo $dashboard; ?>">
      	<img class="svg MeNugray" src="<?php echo $iconpath ?>dashboard.svg">
        <p>Dashboard</p></a>
      </div>
      <?php if ( ! empty($category)): ?>
      <div class="col-sm-4">
      	<a href="<?php echo $category; ?>">
      	<img class="svg MeNugray" src="<?php echo $iconpath ?>category.svg">
        <p>Category Management</p></a>
      </div>
      <?php endif; ?>
      <?php if ( ! empty($mhome) && ! empty($clientData['mhome'])): ?>
      <div class="col-sm-4">
        <a href="<?php echo $mhome; ?>">
      	<img class="svg MeNugray" src="<?php echo $iconpath ?>homepage.svg">
        <p>Home Page Management</p></a>
      </div>
      <?php endif; ?>
      <?php if ( ! empty($mbrand) && ! empty($clientData['mbrand'])): ?>
      <div class="col-sm-4">
      	<a href="<?php echo $mbrand; ?>">
      	<img class="svg MeNugray" src="img/dash_icon/branding.svg">
        <p>Branding Management</p></a>
      </div>
      <?php endif; ?>
      <div class="col-sm-4">
      	<a href="profile.php">
        <img class="svg MeNugray" src="<?php echo $iconpath ?>profile.svg">
        <p>Profile</p>
        </a> </div>
      <div class="col-sm-4">
      	<a href="javascript:void(0);" id="change_password">
        <img class="svg MeNugray" src="<?php echo $iconpath ?>change_password.svg">
        <p>Change Password</p>
        </a> 
	   </div>
      <div class="col-sm-4">
      	<a href="support.php">
        <img class="svg MeNugray" src="<?php echo $iconpath ?>support.svg">
        <p>Support</p>
        </a> </div>
      <div class="col-sm-4">
      	<a href="javascript:void(0);">
        <img class="svg MeNugray" src="<?php echo $iconpath ?>help.svg">
        <p>Help</p>
        </a> </div>
      <div class="col-sm-4">
      	<a href="logout.php">
        <img class="svg MeNugray" src="<?php echo $iconpath ?>logout.svg">
        <p>Logout</p>
        </a> </div>
    </div>
  </div>
</div>
