<?php 
require_once dirname(dirname(dirname(__FILE__))) . '/config/db.class.php';
$db 		    = new DBConnection();
$client_id      = ( isset($_SESSION['client_id'])) ? $_SESSION['client_id'] : '';
$clientData     = ( ! empty($client_id)) ? $db->getClient($client_id) : '';
$siteLogo       = ( ! empty($db->siteLogo)) ? $db->siteLogo : 'EasySIM_logo.svg';

#---------GET-CLIENT-SMTP-DETAILS--------------
$smtp_host_name = ( ! empty($clientData['smtp_host_name'])) ? $clientData['smtp_host_name'] : '';
$smtp_user_name = ( ! empty($clientData['smtp_user_name'])) ? $clientData['smtp_user_name'] : '';
$smtp_pwd       = ( ! empty($clientData['smtp_pwd'])) ? $clientData['smtp_pwd'] : '';
$smtp_port      = ( ! empty($clientData['smtp_port'])) ? $clientData['smtp_port'] : '';

#---------OTHER-DETAILS--------------
$organization   = ( ! empty($clientData['organization'])) ? $clientData['organization'] : 'EasySIM';
$domain_name    = ( ! empty($clientData['domain_name'])) ? $clientData['domain_name'] : '';
$template_url   = $_SERVER['DOCUMENT_ROOT'] . '/includes/email-template/';
$template_logo  = $db->getRootUrl('img/logo/'. $siteLogo);

#---------SET-CLIENT-SMTP-DETAILS----------
define('TOMAIL', $smtp_user_name);
define('HOST_NAME', $smtp_host_name);
define('SETFROM', $smtp_user_name);
define('USER_NAME', $smtp_user_name);
define('SMTP_PASSWORD', $smtp_pwd);
define('SMTP_PORT', $smtp_port);

#---------SET-CLIENT-OTHER-DETAILS---------
define('MAIN_SUPPORT_MAIL', 'support@easysim.cloud');
define('CLIENT_SUPPORT_MAIL', $smtp_user_name);
define('TEMPLATE_URL', $template_url);
define('TEMPLATE_HEADER_LOGO', $template_logo);
define('LOGIN_URL', $db->getRootUrl('login'));
define('CONTACT_EMAIL', $smtp_user_name);
define('FTITLE', $domain_name);
define('NAME', $organization . ' Admin');
