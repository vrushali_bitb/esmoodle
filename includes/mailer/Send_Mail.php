<?php 
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

require_once 'config.php';

function sendMail($to, $template, $values, $subject) {
	if ( ! empty(HOST_NAME) && ! empty(USER_NAME) && ! empty(SMTP_PASSWORD)):
		require_once 'vendor/autoload.php';
		$content  = file_get_contents(TEMPLATE_URL . 'header.php');
		$content .= $template;
		$content .= file_get_contents(TEMPLATE_URL . 'footer.php');
		foreach ( $values as $key => $value ):
			$content = str_replace('{' . $key . '}', $value, $content);
		endforeach;
		$from = SETFROM;
		$mail = new PHPMailer(TRUE);
		try {
			$mail->isSMTP();
			$mail->SMTPDebug	= 0;
			$mail->Host			= HOST_NAME;
			$mail->Port 		= SMTP_PORT;
			$mail->SMTPAuth 	= TRUE;
			$mail->Username 	= USER_NAME; 
			$mail->Password 	= SMTP_PASSWORD;
			$mail->SMTPSecure 	= 'tls';
			$mail->setFrom(SETFROM, NAME);
			$mail->addReplyTo(SETFROM, NAME);
			$mail->addAddress($to, NAME);
			$mail->Subject = $subject;
			$mail->isHTML(TRUE);
			$mail->msgHTML($content);
			$mail->send();
			return TRUE;
		}
		catch (Exception $e) {
			#echo "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
			return FALSE;
		}
	endif;
}

function sendSupportMail($to, $template, $values, $subject, $attachment) {
	if ( ! empty(HOST_NAME) && ! empty(USER_NAME) && ! empty(SMTP_PASSWORD)):
		require_once 'vendor/autoload.php';
		$content  = file_get_contents(TEMPLATE_URL . 'header.php');
		$content .= $template;
		$content .= file_get_contents(TEMPLATE_URL . 'footer.php');
		foreach ( $values as $key => $value ):
			$content = str_replace('{' . $key . '}', $value, $content);
		endforeach;
		$from = SETFROM;
		$mail = new PHPMailer(TRUE);
		try {
			$mail->isSMTP();
			$mail->SMTPDebug	= 0;
			$mail->Host			= HOST_NAME;
			$mail->Port 		= SMTP_PORT;
			$mail->SMTPAuth 	= TRUE;
			$mail->Username 	= USER_NAME; 
			$mail->Password 	= SMTP_PASSWORD;
			$mail->SMTPSecure 	= 'tls';
			$mail->setFrom(SETFROM, NAME);
			$mail->addReplyTo(SETFROM, NAME);
			$mail->addAddress($to, NAME);
			$mail->Subject = $subject;
			if ( ! empty($attachment)):
				$mail->addAttachment($attachment); // Add attachments
			endif;
			$mail->isHTML(TRUE);
			$mail->msgHTML($content);
			$mail->send();
			return TRUE;
		}
		catch (Exception $e) {
			#echo "Message could not be sent. Mailer Error: {$mail->ErrorInfo}"; exit;
			return FALSE;
		}
	endif;
}

function sendAttachmentMail($to, $template, $values, $subject, $attachment, $filename) {
	if ( ! empty(HOST_NAME) && ! empty(USER_NAME) && ! empty(SMTP_PASSWORD)):
		require_once 'vendor/autoload.php';
		$content  = file_get_contents(TEMPLATE_URL . 'header.php');
		$content .= $template;
		$content .= file_get_contents(TEMPLATE_URL . 'footer.php');
		foreach ( $values as $key => $value ):
			$content = str_replace('{' . $key . '}', $value, $content);
		endforeach;
		$from = SETFROM;
		$mail = new PHPMailer(TRUE);
		try {
			$mail->isSMTP();
			$mail->SMTPDebug	= 0;
			$mail->Host			= HOST_NAME;
			$mail->Port 		= SMTP_PORT;
			$mail->SMTPAuth 	= TRUE;
			$mail->Username 	= USER_NAME; 
			$mail->Password 	= SMTP_PASSWORD;
			$mail->SMTPSecure 	= 'tls';
			$mail->setFrom(SETFROM, NAME);
			$mail->addReplyTo(SETFROM, NAME);
			$mail->addAddress($to, NAME);
			$mail->Subject = $subject;
			if ( ! empty($attachment)):
				$mail->addAttachment($attachment, $filename); // Add attachments
			endif;
			$mail->isHTML(TRUE);
			$mail->msgHTML($content);
			$mail->send();
			return TRUE;
		}
		catch (Exception $e) {
			#echo "Message could not be sent. Mailer Error: {$mail->ErrorInfo}"; exit;
			return FALSE;
		}
	endif;
}
