<?php 
session_start();

/******************* Class ********************/
require_once dirname(dirname(__FILE__)) . '/config/db.class.php';

/******************* DBConnection ********************/
$db 		= new DBConnection();
$user		= (isset($_SESSION['username'])) ? $_SESSION['username'] : '';
$role		= (isset($_SESSION['role'])) ? $_SESSION['role'] : '';
$userId		= (isset($_SESSION['userId'])) ? $_SESSION['userId'] : '';
$cid		= (isset($_SESSION['client_id'])) ? $_SESSION['client_id'] : exit('Oops. something went wrong please try again');

//-------------------------Get-Client-Admin-Default-Data--------------------
if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['getTopLearner'])):
	$date		= ( ! empty($_POST['date'])) ? explode(' - ', $_POST['date']) : exit('error');
	$sdate		= $date[0];
	$edate		= $date[1];
	$limit		= '0, 5';
	$learner	= [];
	
	#----TOP-5-LEARNER----
	$sql = "SELECT max(aid) as maid FROM `scenario_attempt_tbl` WHERE DATE(attempt_date) BETWEEN '". $sdate ."' AND '". $edate ."' AND uid IN (SELECT uid FROM score_tbl) OR uid IN (SELECT uid FROM multi_score_tbl) GROUP BY userid ORDER BY maid DESC LIMIT $limit";
	$results = $db->prepare($sql); $results->execute();
	foreach ($results->fetchAll(PDO::FETCH_ASSOC) as $row):
		$attemptId	= $db->getAttemptData($row['maid']);
		$scenario	= $db->getScenario(md5($attemptId['scenario_id']));
		$simType	= $scenario['sim_temp_type'];
		if ($simType == 2):
			$sql1 = "SELECT (SUM(q.ques_val_1) + SUM(q.ques_val_2) + SUM(q.ques_val_3) + SUM(q.ques_val_4) + SUM(q.ques_val_5) + SUM(q.ques_val_6)) as totalQesval,
					  u.username, s.Scenario_title, (c.comp_val_1 + c.comp_val_2 + c.comp_val_3 + c.comp_val_4 + c.comp_val_5 + c.comp_val_6) as totalCompval FROM 
					  multi_score_tbl sc LEFT JOIN 
					  question_tbl q ON sc.question_id = q.question_id LEFT JOIN 
					  users u ON u.id = sc.userid LEFT JOIN 
					  scenario_master s ON sc.scenario_id = s.scenario_id LEFT JOIN 
					  competency_tbl c ON s.scenario_id = c.scenario_id WHERE sc.uid = '". $attemptId['uid'] ."'";
			$sc1 = $db->prepare($sql1); $sc1->execute();
			$row1 = $sc1->fetch(PDO::FETCH_ASSOC);
			$learner[] = array('y' => $row1['totalQesval'], 'p' => $row1['totalCompval'], 'label' => ucwords($row1['username']), 's' => $row1['Scenario_title']);
		elseif ($simType == 1):
			$sql2 = "SELECT (SUM(ans.ans_val1) + SUM(ans.ans_val2) + SUM(ans.ans_val3) + SUM(ans.ans_val4) + SUM(ans.ans_val5) + SUM(ans.ans_val6)) as totalAnsval,
					  u.username, s.Scenario_title, (c.comp_val_1 + c.comp_val_2 + c.comp_val_3 + c.comp_val_4 + c.comp_val_5 + c.comp_val_6) as totalCompval FROM 
					  score_tbl sc LEFT JOIN 
					  answer_tbl ans ON ans.answer_id = sc.choice_option_id LEFT JOIN 
					  users u ON u.id = sc.userid LEFT JOIN 
					  scenario_master s ON sc.scenario_id = s.scenario_id LEFT JOIN 
					  competency_tbl c ON s.scenario_id = c.scenario_id WHERE sc.uid = '". $attemptId['uid'] ."'";
			$sc2 = $db->prepare($sql2); $sc2->execute();
			$row2 = $sc2->fetch(PDO::FETCH_ASSOC);
			$learner[] = array('p' => $row2['totalCompval'], 'y' => $row2['totalAnsval'], 'label' => ucwords($row2['username']), 's' => $row2['Scenario_title']);
		endif;
	endforeach;
	$resdata = array('success' => TRUE, 'data' => array('learner' => $learner));
	echo json_encode($resdata, JSON_NUMERIC_CHECK);
endif;

//-------------------------Get-Client-Admin-Default-Data--------------------
if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['getLeadGroupData'])):
	$date		= ( ! empty($_POST['date'])) ? explode(' - ', $_POST['date']) : exit('error');
	$sdate		= $date[0];
	$edate		= $date[1];
	$limit		= '0, 5';
	$leading_group = [];

	#----LEADING-GROUP----
	$gq = "SELECT group_name, learner FROM group_tbl WHERE learner IS NOT NULL AND (learner IN (SELECT userid FROM scenario_attempt_tbl WHERE attempt_date >= DATE(NOW()) - INTERVAL 1 DAY)) LIMIT $limit";
	$gexe = $db->prepare($gq); $gexe->execute();
	foreach ($gexe->fetchAll(PDO::FETCH_ASSOC) as $groupRow):
		$getlearner = explode(',', $groupRow['learner']);
		$reslastdata[$groupRow['group_name']] = 0;
		foreach ($getlearner as $learnerId):
			$getLastSIM = $db->getLastMultiSIM($learnerId, 1);
			foreach ($getLastSIM as $getGroupData):
				$attemptData = $db->getAttemptData($getGroupData['maid']);
				$scenario	 = $db->getScenario(md5($attemptData['scenario_id']));
				$simType	 = $scenario['sim_temp_type'];
				if ($simType == 2):
					$sqlSc2 = "SELECT (SUM(q.ques_val_1) + SUM(q.ques_val_2) + SUM(q.ques_val_3) + SUM(q.ques_val_4) + SUM(q.ques_val_5) + SUM(q.ques_val_6)) as totalAnsval FROM 
							   multi_score_tbl sc LEFT JOIN 
							   question_tbl q ON sc.question_id = q.question_id WHERE 
							   sc.scenario_id = '". $attemptData['scenario_id'] ."' AND sc.uid = '". $attemptData['uid'] ."'";
					$scg = $db->prepare($sqlSc2); $scg->execute();
					$scgRow = $scg->fetch(PDO::FETCH_ASSOC);
					$reslastdata[$groupRow['group_name']] += $scgRow['totalAnsval'];
				elseif ($simType == 1):
					$sqlSc2 = "SELECT (SUM(ans.ans_val1) + SUM(ans.ans_val2) + SUM(ans.ans_val3) + SUM(ans.ans_val4) + SUM(ans.ans_val5) + SUM(ans.ans_val6)) as totalAnsval FROM 
							   score_tbl sc LEFT JOIN 
							   answer_tbl ans ON sc.choice_option_id = ans.answer_id WHERE 
							   sc.scenario_id = '". $attemptData['scenario_id'] ."' AND sc.uid = '". $attemptData['uid'] ."'";
					$scg = $db->prepare($sqlSc2); $scg->execute();
					$scgRow = $scg->fetch(PDO::FETCH_ASSOC);
					$reslastdata[$groupRow['group_name']] += $scgRow['totalAnsval'];
				endif;
			endforeach;
		endforeach;
	endforeach;

	if ( ! empty($reslastdata)):
		foreach ($reslastdata as $k => $v):
			$leading_group[] = array('y' => $v, 'name' => ucwords($k));
		endforeach;
	endif;
	$resdata = array('success' => TRUE, 'data' => array('group' => $leading_group));
	echo json_encode($resdata, JSON_NUMERIC_CHECK);
endif;

//-------------------------Get-Client-Admin-Default-Data--------------------
if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['getTrendSimData'])):
	$date	= ( ! empty($_POST['date'])) ? explode(' - ', $_POST['date']) : exit('error');
	$sdate	= $date[0];
	$edate	= $date[1];
	$limit	= '0, 5';
	$sim	= [];

	#----TRENDING-SIM-----
	$sql2 = "SELECT COUNT(a.aid) as ta, s.Scenario_title FROM 
			 scenario_attempt_tbl a LEFT JOIN 
			 scenario_master s ON a.scenario_id = s.scenario_id WHERE 
			 DATE(a.attempt_date) BETWEEN '". $sdate ."' AND '". $edate ."' GROUP BY a.scenario_id ORDER BY ta DESC LIMIT $limit";
	$results2 = $db->prepare($sql2); $results2->execute();
	foreach ($results2->fetchAll(PDO::FETCH_ASSOC) as $row2):
		$sim[] = array('y' => $row2['ta'], 'label' => ucwords($row2['Scenario_title']));
	endforeach;
	$resdata = array('success' => TRUE, 'data' => array('sim' => $sim));
	echo json_encode($resdata, JSON_NUMERIC_CHECK);
endif;

//-------------------------Get-Client-Admin-Default-Data--------------------
if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['getTopCompData'])):
	$date	= ( ! empty($_POST['date'])) ? explode(' - ', $_POST['date']) : exit('error');
	$sdate	= $date[0];
	$edate	= $date[1];
	$limit	= '0, 10';
	$comp 	= [];
	#----TOP-COMPETENCIES----
	$sql3 = "SELECT max(aid) as maid, scenario_id, userid FROM `scenario_attempt_tbl` WHERE DATE(attempt_date) BETWEEN '". $sdate ."' AND '". $edate ."' AND uid IN (SELECT uid FROM score_tbl) OR uid IN (SELECT uid FROM multi_score_tbl) GROUP BY userid ORDER BY maid DESC LIMIT $limit";
	$result3 = $db->prepare($sql3); $result3->execute();
	foreach ($result3->fetchAll(PDO::FETCH_ASSOC) as $row3):
		$attemptId3 = $db->getAttemptData($row3['maid']);
		$scenario	= $db->getScenario(md5($attemptId3['scenario_id']));
		$simType	= $scenario['sim_temp_type'];
		if ($simType == 2):
			$sqlbest = "SELECT DISTINCT comp_col_1, comp_col_2, comp_col_3, comp_col_4, comp_col_5, comp_col_6, com_score_1, com_score_2, com_score_3, com_score_4, com_score_5, com_score_6 FROM 
			(SELECT c.comp_col_1, c.comp_col_2, c.comp_col_3, c.comp_col_4, c.comp_col_5, c.comp_col_6, sc.uid, SUM(q.ques_val_1) as com_score_1, SUM(q.ques_val_2) as com_score_2, 
			SUM(q.ques_val_3) as com_score_3, SUM(q.ques_val_4) as com_score_4, SUM(q.ques_val_5) as com_score_5, SUM(q.ques_val_6) as com_score_6, 
			(SUM(q.ques_val_1) + SUM(q.ques_val_2) + SUM(q.ques_val_3) + SUM(q.ques_val_4) + SUM(q.ques_val_5) + SUM(q.ques_val_6)) as totalAnsval FROM 
			competency_tbl c LEFT JOIN 
			multi_score_tbl sc ON c.scenario_id = sc.scenario_id LEFT JOIN 
			question_tbl q ON sc.question_id = q.question_id LEFT JOIN 
			scenario_attempt_tbl a ON sc.uid = a.uid WHERE 
			sc.scenario_id = '". $attemptId3['scenario_id'] ."' AND a.userid = '". $attemptId3['userid'] ."' GROUP BY sc.uid) tmptbl ORDER BY totalAnsval DESC LIMIT 1";
			$sc = $db->prepare($sqlbest); $sc->execute();
			$bestRow = $sc->fetch(PDO::FETCH_ASSOC);
			$bestcom_data[] = array($bestRow['comp_col_1'] => $bestRow['com_score_1'],
									$bestRow['comp_col_2'] => $bestRow['com_score_2'],
									$bestRow['comp_col_3'] => $bestRow['com_score_3'],
									$bestRow['comp_col_4'] => $bestRow['com_score_4'],
									$bestRow['comp_col_5'] => $bestRow['com_score_5'],
									$bestRow['comp_col_6'] => $bestRow['com_score_6']);
		
		elseif ($simType == 1):
			$sqlbest = "SELECT DISTINCT comp_col_1, comp_col_2, comp_col_3, comp_col_4, comp_col_5, comp_col_6, com_score_1, com_score_2, com_score_3, com_score_4, com_score_5, com_score_6 FROM 
						(SELECT c.comp_col_1, c.comp_col_2, c.comp_col_3, c.comp_col_4, c.comp_col_5, c.comp_col_6, sc.uid, SUM(ans.ans_val1) as com_score_1, SUM(ans.ans_val2) as com_score_2, 
						SUM(ans.ans_val3) as com_score_3, SUM(ans.ans_val4) as com_score_4, SUM(ans.ans_val5) as com_score_5, SUM(ans.ans_val6) as com_score_6, 
						(SUM(ans.ans_val1) + SUM(ans.ans_val2) + SUM(ans.ans_val3) + SUM(ans.ans_val4) + SUM(ans.ans_val5) + SUM(ans.ans_val6)) as totalAnsval FROM 
						competency_tbl c LEFT JOIN 
						score_tbl sc ON c.scenario_id = sc.scenario_id LEFT JOIN 
						answer_tbl ans ON sc.choice_option_id = ans.answer_id LEFT JOIN 
						scenario_attempt_tbl a ON sc.uid = a.uid WHERE 
						sc.scenario_id = '". $attemptId3['scenario_id'] ."' AND a.userid = '". $attemptId3['userid'] ."' GROUP BY sc.uid) tmptbl ORDER BY totalAnsval DESC LIMIT 1";
						$sc = $db->prepare($sqlbest); $sc->execute();
						$bestRow = $sc->fetch(PDO::FETCH_ASSOC);
						$bestcom_data[] = array($bestRow['comp_col_1'] => $bestRow['com_score_1'],
												$bestRow['comp_col_2'] => $bestRow['com_score_2'],
												$bestRow['comp_col_3'] => $bestRow['com_score_3'],
												$bestRow['comp_col_4'] => $bestRow['com_score_4'],
												$bestRow['comp_col_5'] => $bestRow['com_score_5'],
												$bestRow['comp_col_6'] => $bestRow['com_score_6']);
		endif;
	endforeach;
	if ( ! empty($bestcom_data)):
		$topcompdata = call_user_func_array('array_merge', array_filter(array_map('array_filter', $bestcom_data)));
		foreach ($topcompdata as $k => $v):
			$comp[] = array('y' => $v, 'name' => $k);
		endforeach;
	else:
		$comp[] = array();
	endif;

	$resdata = array('success' => TRUE, 'data' => array('competency' => $comp));
	echo json_encode($resdata, JSON_NUMERIC_CHECK);
endif;

$db->closeConnection();
