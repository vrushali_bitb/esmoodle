<?php 
ob_start();
session_start();

/******************* Class ********************/
require_once dirname(dirname(__FILE__)) . '/config/upload.class.php';
require_once dirname(dirname(__FILE__)) . '/config/src/class.upload.php';

$obj 				= new Uploads;
$user  				= (isset($_SESSION['username'])) ? $_SESSION['username'] : '';
$role   			= (isset($_SESSION['role'])) ? $_SESSION['role'] : '';
$userId				= (isset($_SESSION['userId'])) ? $_SESSION['userId'] : '';
$domain 			= (isset($_SESSION['domain_name'])) ? $_SESSION['domain_name'] : $_SERVER['HTTP_HOST'];
$img_valid_exts		= array('jpeg', 'jpg', 'png', 'svg');
$video_valid_exts	= array('flv', 'mp4');
$audio_valid_exts	= array('mp3');
$doc_valid_exts		= array('pdf');

/******************* Folder Path ********************/
$uploadpath 		= dirname(dirname(__FILE__)) . '/scenario/upload/'. $domain .'/';
$type_icon_path		= dirname(dirname(__FILE__)) . '/img/type_icon/';
$group_icon_path	= dirname(dirname(__FILE__)) . '/img/group_icon/';
$client_logo_path	= dirname(dirname(__FILE__)) . '/img/logo/';

ini_set('upload_max_filesize', '80M');
ini_set('post_max_size', '100M');
ini_set('max_input_time', 300);
ini_set('max_execution_time', 300);

/******************* Create folder if not exist ********************/
if ( ! is_dir($uploadpath)) mkdir($uploadpath, 0777, TRUE);
if ( ! is_dir($type_icon_path)) mkdir($type_icon_path, 0777, TRUE);
if ( ! is_dir($group_icon_path)) mkdir($group_icon_path, 0777, TRUE);
if ( ! is_dir($client_logo_path)) mkdir($client_logo_path, 0777, TRUE);

if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_FILES) && isset($_POST['splash_img'])):
	$ad_img = (isset($_FILES['file'])) ? $_FILES['file'] : '';
	$error  = FALSE;
	if ( empty($ad_img['name'])):
		$resdata = array('success' => FALSE, 'msg' => 'Please select file.');
		$error = TRUE;
	elseif ( ! empty($ad_img) && ! in_array(strtolower(pathinfo($ad_img['name'], PATHINFO_EXTENSION)), $img_valid_exts)):
		$resdata = array('success' => FALSE, 'msg' => 'Please select valid file. accept only jpeg, jpg, png, svg');
		$error = TRUE;
	elseif ($error == FALSE && ! empty($ad_img) && $ad_img['size'] > 1048576):
		$resdata = array('success' => FALSE, 'msg' => 'File size must be less than 1 MB.');
		$error = TRUE;
	elseif ($error == FALSE):
		$ad_img_name = $obj->uploadFile($ad_img['name'], $ad_img['tmp_name'], $uploadpath);
		$resdata = ( ! empty($ad_img_name)) ? array('success' => TRUE, 'img_name' => $ad_img_name, 'msg' => 'File uploaded successfully.') : array('success' => FALSE, 'msg' => 'Error: file not uploaded. try again later.');
	else:
		$resdata = array('success' => FALSE, 'msg' => 'Oops. something went wrong please try again.');
	endif;
	echo json_encode($resdata);
endif;

if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_FILES) && isset($_POST['splash_video'])):
	$ad_video	= (isset($_FILES['file'])) ? $_FILES['file'] : '';
	$name		= explode(".", $ad_video['name']);
	$continue 	= strtolower($name[1]) == 'zip' ? TRUE : FALSE;
	$error  	= FALSE;
	if ( ! $continue):
		if ( ! empty($ad_video) && ! in_array(strtolower(pathinfo($ad_video['name'], PATHINFO_EXTENSION)), $video_valid_exts)):
			$resdata = array('success' => FALSE, 'msg' => 'Please select valid file. accept only flv, mp4');
			$error = TRUE;
		elseif ($error == FALSE && ! empty($ad_video) && $ad_video['size'] > 20971520):
			$resdata = array('success' => FALSE, 'msg' => 'Video file size must be less than 20 MB.');
			$error = TRUE;
		elseif ($error == FALSE):
			$ad_video_name = $obj->uploadFile($ad_video['name'], $ad_video['tmp_name'], $uploadpath);
			$resdata = ( ! empty($ad_video_name)) ? array('success' => TRUE, 'video_name' => $ad_video_name, 'msg' => 'Video file uploaded successfully.') : array('success' => FALSE, 'msg' => 'Error: file not uploaded. try again later.');
		else:
			$resdata = array('success' => FALSE, 'msg' => 'Oops. something went wrong please try again.?');
		endif;
	else:
		$accepted_types = array('application/zip', 'application/x-zip-compressed', 'multipart/x-zip', 'application/x-compressed');
		foreach ($accepted_types as $mime_type):
			if ($mime_type == $ad_video['type']):
				$okay = TRUE;
				break;
			endif;
		endforeach;
		$zipname = $obj->uploadZipFile($ad_video['name'], $ad_video['tmp_name'], $uploadpath);
		if ($zipname['filename']):
			$zip = new ZipArchive();
			$x   = $zip->open($uploadpath . $zipname['filename']);
			if ($x === TRUE):
				$zip->extractTo($uploadpath);
				$zip->close();
				@unlink($uploadpath . $zipname['filename']);
			endif;
			$resdata = ( ! empty($zipname)) ? array('success' => TRUE, 'video_name' => $zipname['zipname'], 'msg' => 'Zip file uploaded successfully.') : array('success' => FALSE, 'msg' => 'Error: file not uploaded. try again later.');
		else:
			$resdata = array('success' => FALSE, 'msg' => 'Oops. something went wrong please try again.?');
		endif;
	endif;
	echo json_encode($resdata);
endif;

if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_FILES) && isset($_POST['splash_audio'])):
	$ad_audio = (isset($_FILES['file'])) ? $_FILES['file'] : '';
	$error  = FALSE;
	if ( empty($ad_audio['name'])):
		$resdata = array('success' => FALSE, 'msg' => 'Please select file.');
		$error = TRUE;
	elseif ($error == FALSE && ! empty($ad_audio) && ! in_array(strtolower(pathinfo($ad_audio['name'], PATHINFO_EXTENSION)), $audio_valid_exts)):
		$resdata = array('success' => FALSE, 'msg' => 'Please select valid file. accept only mp3.');
		$error = TRUE;
	elseif ($error == FALSE && ! empty($ad_audio) && $ad_audio['size'] > 1048576):
		$resdata = array('success' => FALSE, 'msg' => 'File size must be less than 1 MB.');
		$error = TRUE;
	elseif ($error == FALSE):
		$ad_audio_name	= $obj->uploadFile($ad_audio['name'], $ad_audio['tmp_name'], $uploadpath);
		$resdata = ( ! empty($ad_audio_name)) ? array('success' => TRUE, 'audio_name' => $ad_audio_name, 'msg' => 'Audio file uploaded successfully.') : array('success' => FALSE, 'msg' => 'Error: file not uploaded. try again later.');
	else:
		$resdata = array('success' => FALSE, 'msg' => 'Oops. something went wrong please try again.?');
	endif;
	echo json_encode($resdata);
endif;

if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_FILES) && isset($_POST['type_icon_img'])):
	$ad_img = (isset($_FILES['file'])) ? $_FILES['file'] : '';
	$error  = FALSE;
	if ( empty($ad_img['name'])):
		$resdata = array('success' => FALSE, 'msg' => 'Please select file.');
		$error = TRUE;
	elseif ($error == FALSE && ! empty($ad_img) && ! in_array(strtolower(pathinfo($ad_img['name'], PATHINFO_EXTENSION)), $img_valid_exts)):
		$resdata = array('success' => FALSE, 'msg' => 'Please select valid file. accept only jpeg, jpg, png, svg');
		$error = TRUE;
	elseif ($error == FALSE && ! empty($ad_img) && $ad_img['size'] > 1048576):
		$resdata = array('success' => FALSE, 'msg' => 'File size must be less than 1 MB.');
		$error = TRUE;
	elseif ($error == FALSE):
		$ad_img_name = $obj->uploadFile($ad_img['name'], $ad_img['tmp_name'], $type_icon_path);
		$resdata = ( ! empty($ad_img_name)) ? array('success' => TRUE, 'img_name' => $ad_img_name, 'msg' => 'Icon uploaded successfully.') : array('success' => FALSE, 'msg' => 'Error: file not uploaded. try again later.');
	else:
		$resdata = array('success' => FALSE, 'msg' => 'Oops. something went wrong please try again.?');
	endif;
	echo json_encode($resdata);
endif;

if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_FILES) && isset($_POST['group_icon_img'])):
	$ad_img = (isset($_FILES['file'])) ? $_FILES['file'] : '';
	$error  = FALSE;
	if ( empty($ad_img['name'])):
		$resdata = array('success' => FALSE, 'msg' => 'Please select file.');
		$error = TRUE;
	elseif ($error == FALSE && ! empty($ad_img) && ! in_array(strtolower(pathinfo($ad_img['name'], PATHINFO_EXTENSION)), $img_valid_exts)):
		$resdata = array('success' => FALSE, 'msg' => 'Please select valid file. accept only jpeg, jpg, png, svg');
		$error = TRUE;
	elseif ($error == FALSE && ! empty($ad_img) && $ad_img['size'] > 1048576):
		$resdata = array('success' => FALSE, 'msg' => 'File size must be less than 1 MB.');
		$error = TRUE;
	elseif ($error == FALSE):
		$ad_img_name = $obj->uploadFile($ad_img['name'], $ad_img['tmp_name'], $group_icon_path);
		$resdata = ( ! empty($ad_img_name)) ? array('success' => TRUE, 'img_name' => $ad_img_name, 'msg' => 'Icon uploaded successfully.') : array('success' => FALSE, 'msg' => 'Error: file not uploaded. try again later.');
	else:
		$resdata = array('success' => FALSE, 'msg' => 'Oops. something went wrong please try again.?');
	endif;
	echo json_encode($resdata);
endif;

if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_FILES) && isset($_POST['scenario_media_file'])):
	$file = (isset($_FILES['file'])) ? $_FILES['file'] : '';
	if ( ! empty($file['name'])):
		$type = explode('/', $file['type'])[0];
		if ($type == 'image'):
			$error  = FALSE;
			if ( ! empty($file) && ! in_array(strtolower(pathinfo($file['name'], PATHINFO_EXTENSION)), $img_valid_exts)):
				$resdata = array('success' => FALSE, 'msg' => 'Please select valid file. accept only jpeg, jpg, png, svg');
				$error = TRUE;
			elseif ($error == FALSE && ! empty($file) && $file['size'] > 1048576):
				$resdata = array('success' => FALSE, 'msg' => 'File size must be less than 1 MB.');
				$error = TRUE;
			elseif ($error == FALSE):
				$ad_img_name = $obj->uploadFile($file['name'], $file['tmp_name'], $uploadpath);
				$resdata = ( ! empty($ad_img_name)) ? array('success' => TRUE, 'zip' => FALSE, 'file_name' => $ad_img_name, 'msg' => 'Image uploaded successfully.') : array('success' => FALSE, 'msg' => 'Error: Image not uploaded. try again later.');
			else:
				$resdata = array('success' => FALSE, 'msg' => 'Oops. something went wrong please try again.?');
			endif;
		elseif ($type == 'audio'):
			$error  = FALSE;
			if ( ! empty($file) && ! in_array(strtolower(pathinfo($file['name'], PATHINFO_EXTENSION)), $audio_valid_exts)):
				$resdata = array('success' => FALSE, 'msg' => 'Please select valid file. accept only mp3.');
				$error = TRUE;
			elseif ($error == FALSE && ! empty($file) && $file['size'] > 1048576):
				$resdata = array('success' => FALSE, 'msg' => 'File size must be less than 1 MB.');
				$error = TRUE;
			elseif ($error == FALSE):
				$ad_audio_name	= $obj->uploadFile($file['name'], $file['tmp_name'], $uploadpath);
				$resdata = ( ! empty($ad_audio_name)) ? array('success' => TRUE, 'zip' => FALSE, 'file_name' => $ad_audio_name, 'msg' => 'Audio file uploaded successfully.') : array('success' => FALSE, 'msg' => 'Error: file not uploaded. try again later.');
			else:
				$resdata = array('success' => FALSE, 'msg' => 'Oops. something went wrong please try again.?');
			endif;
		elseif ($type == 'video'):
			$error  = FALSE;
			if ( ! empty($file) && ! in_array(strtolower(pathinfo($file['name'], PATHINFO_EXTENSION)), $video_valid_exts)):
				$resdata = array('success' => FALSE, 'msg' => 'Please select valid file. accept only flv, mp4');
				$error = TRUE;
			elseif ($error == FALSE && ! empty($file) && $file['size'] > 20971520):
				$resdata = array('success' => FALSE, 'msg' => 'Video file size must be less than 20 MB.');
				$error = TRUE;
			elseif ($error == FALSE):
				$ad_video_name = $obj->uploadFile($file['name'], $file['tmp_name'], $uploadpath);
				$resdata = ( ! empty($ad_video_name)) ? array('success' => TRUE, 'zip' => FALSE, 'file_name' => $ad_video_name, 'msg' => 'Video file uploaded successfully.') : array('success' => FALSE, 'msg' => 'Error: Video not uploaded. try again later.');
			else:
				$resdata = array('success' => FALSE, 'msg' => 'Oops. something went wrong please try again.?');
			endif;
		elseif ($type == 'application'):
			$name	= explode(".", $file['name']);
			$zip	= strtolower($name[1]) == 'zip' ? TRUE : FALSE;
			$error	= FALSE;
			if ( ! empty($file) && $file['size'] > 52428800):
				$error   = TRUE;
				$resdata = array('success' => FALSE, 'msg' => 'Web object file size must be less than 50 MB.');
				echo json_encode($resdata); exit;
			endif;
			if ($zip == TRUE && $error == FALSE):
				$accepted_types = array('application/zip', 'application/x-zip-compressed', 'multipart/x-zip', 'application/x-compressed');
				foreach ($accepted_types as $mime_type):
					if ($mime_type == $file['type']):
						$okay = TRUE;
						break;
					endif;
				endforeach;
				$zipname = $obj->uploadZipFile($file['name'], $file['tmp_name'], $uploadpath);
				if ($zipname['filename']):
					/**** Create Zip file folder ****/
					$zipfname = explode(".", $zipname['filename'])[0];
					if ( ! is_dir($zipfname)) mkdir($uploadpath . $zipfname, 0777, TRUE);
					/* Upload Zip File */
					$zip = new ZipArchive();
					$x   = $zip->open($uploadpath . $zipname['filename']);
					if ($x === TRUE):
						$zip->extractTo($uploadpath . $zipfname);
						$zip->close();
						@unlink($uploadpath . $zipname['filename']);
						$resdata = array('success' => TRUE, 'zip' => TRUE, 'file_name' => $zipfname, 'msg' => 'Web object file uploaded successfully.');
					endif;
				else:
					$resdata = array('success' => FALSE, 'msg' => 'Please select valid file. accept only web object zip file.');
				endif;
			else:
				$resdata = array('success' => FALSE, 'msg' => 'Please select valid file. accept only web object zip file.');
			endif;
		else:
			$resdata = array('success' => FALSE, 'msg' => 'Please select valid file. accept only image, video, audio, web object file.');
		endif;
		echo json_encode($resdata);
	endif;
endif;

/* Upload Web Object */
if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_FILES) && isset($_POST['web_object_file'])):
	$file = (isset($_FILES['file'])) ? $_FILES['file'] : '';
	if ( ! empty($file['name'])):
		$type = explode('/', $file['type'])[0];
		if ($type == 'application'):
			$name	= explode(".", $file['name']);
			$zip	= strtolower($name[1]) == 'zip' ? TRUE : FALSE;
			$error	= FALSE;
			if ( ! empty($file) && $file['size'] > 52428800):
				$error   = TRUE;
				$resdata = array('success' => FALSE, 'msg' => 'Web object file size must be less than 50 MB.');
				echo json_encode($resdata); exit;
			endif;
			if ($zip == TRUE && $error == FALSE):
				$accepted_types = array('application/zip', 'application/x-zip-compressed', 'multipart/x-zip', 'application/x-compressed');
				foreach ($accepted_types as $mime_type):
					if ($mime_type == $file['type']):
						$okay = TRUE;
						break;
					endif;
				endforeach;
				$zipname = $obj->uploadZipFile($file['name'], $file['tmp_name'], $uploadpath);
				if ($zipname['filename']):
					/**** Create Zip file folder ****/
					$zipfname = explode(".", $zipname['filename'])[0];
					if ( ! is_dir($zipfname)) mkdir($uploadpath . $zipfname, 0777, TRUE);
					/* Upload Zip File */
					$zip = new ZipArchive();
					$x   = $zip->open($uploadpath . $zipname['filename']);
					if ($x === TRUE):
						$zip->extractTo($uploadpath . $zipfname);
						$zip->close();
						@unlink($uploadpath . $zipname['filename']);
						$resdata = array('success' => TRUE, 'zip' => TRUE, 'file_name' => $zipfname, 'msg' => 'Web object file uploaded successfully.');
					endif;
				else:
					$resdata = array('success' => FALSE, 'msg' => 'Please select valid file. accept only web object zip file.');
				endif;
			else:
				$resdata = array('success' => FALSE, 'msg' => 'Please select valid file. accept only web object zip file.');
			endif;
		else:
			$resdata = array('success' => FALSE, 'msg' => 'Please select valid file. accept only web object zip file.');
		endif;
	else:
		$resdata = array('success' => FALSE, 'msg' => 'Please select valid file. accept only web object zip file.');
	endif;
	echo json_encode($resdata);
endif;

/***********Upload Client Logo************/
if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_FILES) && isset($_POST['add_client_logo'])):
	$ad_img = (isset($_FILES['logo'])) ? $_FILES['logo'] : '';
	$error  = FALSE;
	if ( ! empty($ad_img) && ! in_array(strtolower(pathinfo($ad_img['name'], PATHINFO_EXTENSION)), $img_valid_exts)):
		$resdata = array('success' => FALSE, 'msg' => 'Please select valid file. accept only jpeg, jpg, png, svg');
		$error = TRUE;
	elseif ($error == FALSE && ! empty($ad_img) && $ad_img['size'] > 1048576):
		$resdata = array('success' => FALSE, 'msg' => 'File size must be less than 1 MB.');
		$error = TRUE;
	elseif ($error == FALSE):
		$handle = new Upload($ad_img);
		if ($handle->uploaded) {
			$handle->file_safe_name 	= TRUE;
			$handle->file_overwrite 	= FALSE;
			$handle->file_auto_rename	= TRUE;
			$handle->Process($client_logo_path);
			if ($handle->processed) {
				$ad_img_name = $handle->file_dst_name;
			}
			$handle->Clean();
		}
		$resdata = ( ! empty($ad_img_name)) ? array('success' => TRUE, 'logo' => $ad_img_name) : array('success' => FALSE, 'msg' => 'Error: Logo not uploaded. try again later.!');
	else:
		$resdata = array('success' => FALSE, 'msg' => 'Oops. something went wrong please try again.?');
	endif;
	echo json_encode($resdata);
endif;

/*********** Upload Questions Image Files ************/
if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_FILES) && isset($_POST['qImgfiles'])):
	$file  = (isset($_FILES['file'])) ? $_FILES['file'] : '';
	$error = FALSE;
	if ( ! empty($file) && ! in_array(strtolower(pathinfo($file['name'], PATHINFO_EXTENSION)), $img_valid_exts)):
		$resdata = array('success' => FALSE, 'msg' => 'Please select valid file. accept only jpeg, jpg, png, svg');
		$error = TRUE;
	elseif ($error == FALSE && ! empty($file) && $file['size'] > 1048576):
		$resdata = array('success' => FALSE, 'msg' => 'File size must be less than 1 MB.');
		$error = TRUE;
	elseif ($error == FALSE):
		$handle = new Upload($file);
		if ($handle->uploaded) {
			$handle->image_resize       = TRUE;
			$handle->image_ratio_y      = TRUE;
			$handle->image_x      		= 1000;
			$handle->file_safe_name 	= TRUE;
			$handle->file_overwrite 	= FALSE;
			$handle->file_auto_rename	= TRUE;
			#$handle->image_convert 	= 'jpeg';
			#$handle->jpeg_quality 		= 50;
			#$handle->jpeg_size         = 81920;
			$handle->Process($uploadpath);
			if ($handle->processed) {
				$file_dst_name = $handle->file_dst_name;
			}
			$handle->Clean();
		}
	$resdata = ( ! empty($file_dst_name)) ? array('success' => TRUE, 'file_name' => $file_dst_name, 'msg' => 'File uploaded successfully.', 'path' => $uploadpath) : array('success' => FALSE, 'msg' => 'Error: file not uploaded. try again later.');
	else:
		$resdata = array('success' => FALSE, 'msg' => 'Oops. something went wrong please try again.?');
	endif;
	echo json_encode($resdata);
endif;

/*********** Upload Questions Doc Files ************/
if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_FILES) && isset($_POST['qDocfiles'])):
	$file  = (isset($_FILES['file'])) ? $_FILES['file'] : '';
	$error = FALSE;
	if ( ! empty($file) && ! in_array(strtolower(pathinfo($file['name'], PATHINFO_EXTENSION)), $doc_valid_exts)):
		$resdata = array('success' => FALSE, 'msg' => 'Please select valid file. accept only pdf');
		$error = TRUE;
	elseif ($error == FALSE && ! empty($file) && $file['size'] > 1048576):
		$resdata = array('success' => FALSE, 'msg' => 'File size must be less than 1 MB.');
		$error = TRUE;
	elseif ($error == FALSE):
		$handle = new Upload($file);
		if ($handle->uploaded) {
			$handle->file_safe_name 	= TRUE;
			$handle->file_overwrite 	= FALSE;
			$handle->file_auto_rename	= TRUE;
			$handle->Process($uploadpath);
			if ($handle->processed) {
				$file_dst_name = $handle->file_dst_name;
			}
			$handle->Clean();
		}
	$resdata = ( ! empty($file_dst_name)) ? array('success' => TRUE, 'file_name' => $file_dst_name, 'msg' => 'File uploaded successfully.', 'path' => $uploadpath) : array('success' => FALSE, 'msg' => 'Error: file not uploaded. try again later.');
	else:
		$resdata = array('success' => FALSE, 'msg' => 'Oops. something went wrong please try again.?');
	endif;
	echo json_encode($resdata);
endif;

/*********** Upload Questions Audio Files ************/
if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_FILES) && isset($_POST['qAudiofiles'])):
	$file  = (isset($_FILES['file'])) ? $_FILES['file'] : '';
	$error = FALSE;
	if ( ! empty($file) && ! in_array(strtolower(pathinfo($file['name'], PATHINFO_EXTENSION)), $audio_valid_exts)):
		$resdata = array('success' => FALSE, 'msg' => 'Please select valid file. accept only mp3.');
		$error = TRUE;
	elseif ($error == FALSE && ! empty($file) && $file['size'] > 1048576):
		$resdata = array('success' => FALSE, 'msg' => 'File size must be less than 1 MB.');
		$error = TRUE;
	elseif ($error == FALSE):
		$handle = new Upload($file);
		if ($handle->uploaded) {
			$handle->file_safe_name 	= TRUE;
			$handle->file_overwrite 	= FALSE;
			$handle->file_auto_rename	= TRUE;
			$handle->Process($uploadpath);
			if ($handle->processed) {
				$file_dst_name = $handle->file_dst_name;
			}
			$handle->Clean();
		}
		$resdata = ( ! empty($file_dst_name)) ? array('success' => TRUE, 'file_name' => $file_dst_name, 'msg' => 'File uploaded successfully.', 'path' => $uploadpath) : array('success' => FALSE, 'msg' => 'Error: file not uploaded. try again later.');
	else:
		$resdata = array('success' => FALSE, 'msg' => 'Oops. something went wrong please try again.?');
	endif;
	echo json_encode($resdata);
endif;

/*********** Upload Questions Video Files ************/
if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_FILES) && isset($_POST['qVideofiles'])):
	$file  = (isset($_FILES['file'])) ? $_FILES['file'] : '';
	$error = FALSE;
	if ( empty($file['name'])):
		$resdata = array('success' => FALSE, 'msg' => 'Please select file.');
		$error = TRUE;
	elseif ($error == FALSE && ! empty($file) && ! in_array(strtolower(pathinfo($file['name'], PATHINFO_EXTENSION)), $video_valid_exts)):
		$resdata = array('success' => FALSE, 'msg' => 'Please select valid file. accept only flv, mp4');
		$error = TRUE;
	elseif ($error == FALSE && ! empty($file) && $file['size'] > 20971520):
		$resdata = array('success' => FALSE, 'msg' => 'Video file size must be less than 20 MB.');
		$error = TRUE;
	elseif ($error == FALSE):
		$handle = new Upload($file);
		if ($handle->uploaded) {
			$handle->file_safe_name 	= TRUE;
			$handle->file_overwrite 	= FALSE;
			$handle->file_auto_rename	= TRUE;
			$handle->Process($uploadpath);
			if ($handle->processed) {
				$file_dst_name = $handle->file_dst_name;
			}
			$handle->Clean();
		}
		$resdata = ( ! empty($file_dst_name)) ? array('success' => TRUE, 'file_name' => $file_dst_name, 'msg' => 'File uploaded successfully.', 'path' => $uploadpath) : array('success' => FALSE, 'msg' => 'Error: file not uploaded. try again later.');
	else:
		$resdata = array('success' => FALSE, 'msg' => 'Oops. something went wrong please try again.?');
	endif;
	echo json_encode($resdata);
endif;

/*********** Upload Record Audio ************/
if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_FILES) && isset($_POST['rec_audio'])):
	$file	= (isset($_FILES['file'])) ? $_FILES['file'] : '';
	$error  = FALSE;
	$handle = new Upload($file);
	if ($handle->uploaded) {
		$handle->file_safe_name 	= TRUE;
		$handle->file_overwrite 	= FALSE;
		$handle->file_auto_rename	= TRUE;
		$handle->Process($uploadpath);
		if ($handle->processed) {
			$file_dst_name = $handle->file_dst_name;
		}
		$handle->Clean();
	}
	$resdata = ( ! empty($file_dst_name)) ? array('success' => TRUE, 'file_name' => $file_dst_name, 'msg' => 'Recording uploaded successfully.', 'path' => $uploadpath) : array('success' => FALSE, 'msg' => 'an upload error occurred!');
	echo json_encode($resdata);
endif;

/*********** Upload Record Video ************/
if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_FILES) && isset($_POST['rec_video'])):
	$file	= (isset($_FILES['file'])) ? $_FILES['file'] : '';
	$error  = FALSE;
	$handle = new Upload($file);
	if ($handle->uploaded) {
		$handle->file_safe_name 	= TRUE;
		$handle->file_overwrite 	= FALSE;
		$handle->file_auto_rename	= TRUE;
		$handle->Process($uploadpath);
		if ($handle->processed) {
			$file_dst_name = $handle->file_dst_name;
		}
		$handle->Clean();
	}
	$resdata = ( ! empty($file_dst_name)) ? array('success' => TRUE, 'file_name' => $file_dst_name, 'msg' => 'Recording uploaded successfully.', 'path' => $uploadpath) : array('success' => FALSE, 'msg' => 'an upload error occurred!');
	echo json_encode($resdata);
endif;

/*********** Upload Record Screen ************/
if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_FILES) && isset($_POST['rec_screen'])):
	$file	= (isset($_FILES['file'])) ? $_FILES['file'] : '';
	$error  = FALSE;
	$handle = new Upload($file);
	if ($handle->uploaded) {
		$handle->file_safe_name 	= TRUE;
		$handle->file_overwrite 	= FALSE;
		$handle->file_auto_rename	= TRUE;
		$handle->Process($uploadpath);
		if ($handle->processed) {
			$file_dst_name = $handle->file_dst_name;
		}
		$handle->Clean();
	}
	$resdata = ( ! empty($file_dst_name)) ? array('success' => TRUE, 'file_name' => $file_dst_name, 'msg' => 'Recording uploaded successfully.', 'path' => $uploadpath) : array('success' => FALSE, 'msg' => 'an upload error occurred!');
	echo json_encode($resdata);
endif;

/*********** Upload Video Questions Template Main Files ************/
if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_FILES) && isset($_POST['videoq_file'])):
	$file  = (isset($_FILES['file'])) ? $_FILES['file'] : '';
	$error = FALSE;
	if ( empty($file['name'])):
		$resdata = array('success' => FALSE, 'msg' => 'Please select file.');
		$error = TRUE;
	elseif ($error == FALSE && ! empty($file) && ! in_array(strtolower(pathinfo($file['name'], PATHINFO_EXTENSION)), $video_valid_exts)):
		$resdata = array('success' => FALSE, 'msg' => 'Please select valid file. accept only flv, mp4');
		$error = TRUE;
	elseif ($error == FALSE && ! empty($file) && $file['size'] > 62914560):
		$resdata = array('success' => FALSE, 'msg' => 'Video file size must be less than 60 MB.');
		$error = TRUE;
	elseif ($error == FALSE):
		$handle = new Upload($file);
		if ($handle->uploaded) {
			$handle->file_safe_name 	= TRUE;
			$handle->file_overwrite 	= FALSE;
			$handle->file_auto_rename	= TRUE;
			$handle->Process($uploadpath);
			if ($handle->processed) {
				$file_dst_name = $handle->file_dst_name;
			}
			$handle->Clean();
		}
		$resdata = ( ! empty($file_dst_name)) ? array('success' => TRUE, 'file_name' => $file_dst_name, 'msg' => 'File uploaded successfully.', 'path' => $uploadpath, 'loadPath' => "scenario/upload/$domain/") : array('success' => FALSE, 'msg' => 'Error file not uploaded. try again later.');
	else:
		$resdata = array('success' => FALSE, 'msg' => 'Oops. something went wrong please try again.?');
	endif;
	echo json_encode($resdata);
endif;

/***********Upload Simulation Cover Image************/
if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_FILES) && isset($_POST['scenario_cover_file'])):
	$file  = (isset($_FILES['file'])) ? $_FILES['file'] : '';
	$error = FALSE;
	if ( ! empty($file) && ! in_array(strtolower(pathinfo($file['name'], PATHINFO_EXTENSION)), $img_valid_exts)):
		$resdata = array('success' => FALSE, 'msg' => 'Please select valid file. accept only jpeg, jpg, png, svg');
		$error = TRUE;
	elseif ($error == FALSE && ! empty($file) && $file['size'] > 1048576):
		$resdata = array('success' => FALSE, 'msg' => 'File size must be less than 1 MB.');
		$error = TRUE;
	elseif ($error == FALSE):
		$handle = new Upload($file);
		if ($handle->uploaded) {
			$handle->image_resize       = TRUE;
			$handle->image_x            = 210;
			$handle->image_y            = 120;
			$handle->file_safe_name 	= TRUE;
			$handle->file_overwrite 	= FALSE;
			$handle->file_auto_rename	= TRUE;
			$handle->Process($uploadpath);
			if ($handle->processed) {
				$ad_img_name = $handle->file_dst_name;
			}
			$handle->Clean();
		}
		$resdata = ( ! empty($ad_img_name)) ? array('success' => TRUE, 'file_name' => $ad_img_name, 'msg' => 'Image uploaded successfully.') : array('success' => FALSE, 'msg' => 'Error: Image not uploaded. try again later.');
	else:
		$resdata = array('success' => FALSE, 'msg' => 'Oops. something went wrong please try again.?');
	endif;
	echo json_encode($resdata);
endif;

#----------------------------------------Add background of your own choice----------------------------------------
if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['add_own_background']) && ! empty($_POST['add_own_background']) && isset($_FILES)):
	$file 	= (isset($_FILES['file'])) ? $_FILES['file'] : '';
	$handle	= new Upload($file);
	$error  = FALSE;
	if ( ! empty($file) && ! in_array(strtolower(pathinfo($file['name'], PATHINFO_EXTENSION)), $img_valid_exts)):
		$resdata = array('success' => FALSE, 'msg' => 'Please select valid file. accept only jpeg, jpg, png, svg');
		$error   = TRUE;
	elseif ($error == FALSE && ! empty($file) && $file['size'] > 1048576):
		$resdata = array('success' => FALSE, 'msg' => 'File size must be less than 1 MB.');
		$error	 = TRUE;
	elseif ($error == FALSE && $handle->uploaded):
		$handle->image_resize       = TRUE;
		$handle->image_x            = 1920;
		$handle->image_y            = 1024;
		$handle->file_safe_name 	= TRUE;
		$handle->file_overwrite 	= FALSE;
		$handle->file_auto_rename	= TRUE;
		$handle->Process($uploadpath);
		if ($handle->processed) {
			$img_name = $handle->file_dst_name;
		}
		$handle->Clean();
		$resdata = ( ! empty($img_name)) ? array('success' => TRUE, 'file_name' => $img_name, 'msg' => 'Image uploaded successfully.') : array('success' => FALSE, 'msg' => 'Error: Image not uploaded. try again later.');
	else:
		$resdata = array('success' => FALSE, 'msg' => 'Oops. something went wrong please try again.?');
	endif;
	echo json_encode($resdata);
endif;

#----------------------------------------Add background of your own choice----------------------------------------
if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['add_char']) && ! empty($_POST['add_char']) && isset($_FILES)):
	$file 	= (isset($_FILES['file'])) ? $_FILES['file'] : '';
	$handle	= new Upload($file);
	$error  = FALSE;
	if ( ! empty($file) && ! in_array(strtolower(pathinfo($file['name'], PATHINFO_EXTENSION)), $img_valid_exts)):
		$resdata = array('success' => FALSE, 'msg' => 'Please select valid file. accept only jpeg, jpg, png, svg');
		$error   = TRUE;
	elseif ($error == FALSE && ! empty($file) && $file['size'] > 1048576):
		$resdata = array('success' => FALSE, 'msg' => 'File size must be less than 1 MB.');
		$error	 = TRUE;
	elseif ($error == FALSE && $handle->uploaded):
		$handle->image_resize       = TRUE;
		$handle->image_x            = 920;
		$handle->image_y            = 1024;
		$handle->file_safe_name 	= TRUE;
		$handle->file_overwrite 	= FALSE;
		$handle->file_auto_rename	= TRUE;
		$handle->Process($uploadpath);
		if ($handle->processed) {
			$img_name = $handle->file_dst_name;
		}
		$handle->Clean();
		$resdata = ( ! empty($img_name)) ? array('success' => TRUE, 'file_name' => $img_name, 'msg' => 'Image uploaded successfully.') : array('success' => FALSE, 'msg' => 'Error: Image not uploaded. try again later.');
	else:
		$resdata = array('success' => FALSE, 'msg' => 'Oops. something went wrong please try again.?');
	endif;
	echo json_encode($resdata);
endif;

#----------------------------------------Add-Home-Page-BG-Image--------------------------------------
if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['page_bg_img']) && ! empty($_POST['page_bg_img']) && isset($_FILES)):
	$file 	= (isset($_FILES['file'])) ? $_FILES['file'] : '';
	$handle = new Upload($file);
	$error  = FALSE;
	if ( empty($file['name'])):
		$resdata = array('success' => FALSE, 'msg' => 'Please select file.');
		$error = TRUE;
	elseif ( ! empty($file) && ! in_array(strtolower(pathinfo($file['name'], PATHINFO_EXTENSION)), $img_valid_exts)):
		$resdata = array('success' => FALSE, 'msg' => 'Please select valid file. accept only jpeg, jpg, png, svg');
		$error   = TRUE;
	elseif ($error == FALSE && ! empty($file) && $file['size'] > 1048576):
		$resdata = array('success' => FALSE, 'msg' => 'File size must be less than 1 MB.');
		$error	 = TRUE;
	elseif ($error == FALSE && $handle->uploaded):
		$handle->image_resize       = FALSE;
		$handle->file_safe_name 	= TRUE;
		$handle->file_overwrite 	= FALSE;
		$handle->file_auto_rename	= TRUE;
		$handle->Process($uploadpath);
		if ($handle->processed) {
			$img_name = $handle->file_dst_name;
		}
		$handle->Clean();
		$resdata = ( ! empty($img_name)) ? array('success' => TRUE, 'file_name' => $img_name, 'msg' => 'Image uploaded successfully.') : array('success' => FALSE, 'msg' => 'Error: Image not uploaded. try again later.');
	else:
		$resdata = array('success' => FALSE, 'msg' => 'Oops. something went wrong please try again.?');
	endif;
	echo json_encode($resdata);
endif;

/*********** Upload Record Proctor Video ************/
if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_FILES) && isset($_POST['proctor_video'])):
	$proctor_path = $uploadpath .'proctor';
	if ( ! is_dir($proctor_path)) mkdir($proctor_path, 0777, TRUE);
	$file	= (isset($_FILES['file'])) ? $_FILES['file'] : '';
	$error  = FALSE;
	$handle = new Upload($file);
	if ($handle->uploaded) {
		$handle->file_safe_name 	= TRUE;
		$handle->file_overwrite 	= FALSE;
		$handle->file_auto_rename	= TRUE;
		$handle->Process($proctor_path);
		if ($handle->processed) {
			$file_dst_name = $handle->file_dst_name;
		}
		$handle->Clean();
	}
	$resdata = ( ! empty($file_dst_name)) ? array('success' => TRUE, 'file_name' => $file_dst_name, 'msg' => 'Recording uploaded successfully.', 'path' => $proctor_path) : array('success' => FALSE, 'msg' => 'an upload error occurred!');
	echo json_encode($resdata);
endif;

/*********** Upload Face Verify ************/
if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['face_verify'])):
	$proctor_path = $uploadpath .'proctor';
	if ( ! is_dir($proctor_path)) mkdir($proctor_path, 0777, TRUE);
	$file			= (isset($_POST['file'])) ? $_POST['file'] : '';
	$fileImg_parts 	= explode(";base64,", $file);
    $image_type_aux	= explode("image/", $fileImg_parts[0]);
    $image_type 	= $image_type_aux[1];
    $image_base64 	= base64_decode($fileImg_parts[1]);
    $results 		= $proctor_path . '/'. $userId . '.'. $image_type;
	file_put_contents($results, $image_base64);
	$resdata = ( ! empty($results)) ? array('success' => TRUE, 'file_name' => $results, 'msg' => 'Recording uploaded successfully.', 'path' => $proctor_path) : array('success' => FALSE, 'msg' => 'an upload error occurred!');
	echo json_encode($resdata);
endif;

ob_flush();
unset($obj);
