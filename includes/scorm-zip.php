<?php 
#------------------------------SCORM-ZIP------------------------------
if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['scorm_publish'])):
	$HtmlCode	= ob_get_contents();
	$HtmlCode  .= '<style type="text/css">.scorm_publishs { display:none !important; }</style>';
	$scorm		= 'scorm-zip';
	$sim_name	= $_POST['sim_name'];
	$scorm_type	= $_POST['scorm_type'];
	$scorm_path	= $scorm .'/'. $sim_name. '-scorm-'. date("M-j-Y-g-i-s-a");
	
	if ( ! is_dir($scorm)) mkdir($scorm);
	if ( ! is_dir($scorm_path)) mkdir($scorm_path);

	$sfh = fopen($scorm_path . '/index.html', 'w');
	fwrite($sfh, $simHtml);
	fclose($sfh);
	
	$fh = fopen($scorm_path . '/launch.html', 'w');
	fwrite($fh, $HtmlCode);
	fclose($fh);

	$scromXml = 'scorm/imsmanifest.xml';
	if (file_exists($scromXml)):
		$content = simplexml_load_file($scromXml);
		$content->organizations->organization->title = $sim_name;
		$content->organizations->organization->item->title = $sim_name;
		$content->asXML($scromXml);
	endif;

	function xcopy($src, $dest) {
		foreach (scandir($src) as $file):
			if ( ! is_readable($src . '/' . $file)) continue;
			if ( is_dir($src .'/' . $file) && ($file != '.') && ($file != '..') && ($file != 'ckeditor') && ($file != 'dataTables')):
				if ( ! is_dir($dest . '/' . $file)) mkdir($dest . '/' . $file);
				xcopy($src . '/' . $file, $dest . '/' . $file);
			else:
				if ( ! file_exists($dest . '/' . $file)) copy($src . '/' . $file, $dest . '/' . $file);
			endif;
		 endforeach;
	}
	
	function delete_directory($dirname) {
		if (is_dir($dirname))
			$dir_handle = opendir($dirname);
			if ( ! $dir_handle) return FALSE;
				while ($file = readdir($dir_handle)):
					if ($file != "." && $file != ".."):
						if ( ! is_dir($dirname."/".$file))
							unlink($dirname."/".$file);
						else
							delete_directory($dirname.'/'.$file);
						endif;
					endwhile;
			closedir($dir_handle);
			rmdir($dirname);
			return TRUE;
		}

	xcopy('scorm', $scorm_path);
	
	require_once 'includes/zip-backup.php';
	$zipname = $scorm_path .'.zip';
	zipData($scorm_path, $zipname);
	delete_directory($scorm_path);
endif;
