<?php 
ob_start();
session_start();

/******************* Class ********************/
require_once dirname(dirname(__FILE__)) . '/config/db.class.php';

/******************* DBConnection ********************/
$db 		= new DBConnection();
$user		= (isset($_SESSION['username'])) ? $_SESSION['username'] : '';
$role		= (isset($_SESSION['role'])) ? $_SESSION['role'] : '';
$userId		= (isset($_SESSION['userId'])) ? $_SESSION['userId'] : '';
$cid		= (isset($_SESSION['client_id'])) ? $_SESSION['client_id'] : exit('Oops. something went wrong please try again');
$es_db		= dirname(dirname(__FILE__)) . '/db/es_client_db.sql';
$fpath		= dirname(dirname(__FILE__)) . '/scenario/upload/';
$dtime		= date('Y-m-d H:i:s a');
$cdate		= date('Y-m-d');
$dom_prefix	= '.easysim.app';
$db_prefix	= 'proddb_easysimapp_';

/* Connect main DB */
try {
	$main_conn_db = new PDO("mysql:host=$db->main_servername;dbname=$db->main_db_name", $db->main_username, $db->main_password);
	$main_conn_db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
}
catch(PDOException $e) {
	echo "Main Connection failed:". $e->getMessage();
	exit;
}

//--------------------------------Create-Client-Account---------------------------
if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['create_client_account'])):
	extract($_POST);
	$error = FALSE;
	$client_db_name = $db_prefix . $db_name;
	if (empty($db_name) || empty($domain_name) || empty($organization) || empty($userName) || empty($password) || empty($sim_tmp)):
		$resdata = array('success' => FALSE, 'msg' => 'Please enter all required fields.');
		$error = TRUE;
	elseif ( ! empty($domain_name) && ! preg_match('~^[A-Za-z0-9_]{3,20}$~i', $domain_name)):
		$resdata = array('success' => FALSE, 'msg' => 'Enter valide subdomain.');
		$error = TRUE;
	elseif ($error == FALSE):
		$stmt = $main_conn_db->query("SELECT COUNT(*) FROM INFORMATION_SCHEMA.SCHEMATA WHERE SCHEMA_NAME = '". $client_db_name ."'"); $checkDB = $stmt->fetchColumn();
		if ($checkDB == 1):
			$resdata = array('success' => FALSE, 'msg' => 'Database already exists. choose a different database name.');
		elseif ($checkDB == 0):
			$sql = "CREATE DATABASE IF NOT EXISTS $client_db_name CHARACTER SET utf8 COLLATE utf8_general_ci";
			$res = $main_conn_db->prepare($sql);
			if ($res->execute()):
				#---------Create-Client-Account-------
				$data = array('domain'		=> trim($domain_name) . $dom_prefix,
							  'db_name'		=> $client_db_name,
							  'atype'		=> $orgtype,
							  'org'			=> ( ! empty($organization)) ? $organization : '',
							  'country'		=> ( ! empty($company_country)) ? $company_country : '',
							  'state'		=> ( ! empty($company_state)) ? $company_state : '',
							  'city'		=> ( ! empty($company_city)) ? $company_city : '',
							  'add'			=> ( ! empty($bill_add)) ? $bill_add : '',
							  'tax_id'		=> ( ! empty($tax_id)) ? $tax_id : '',
							  'logo'		=> ( ! empty($logo_name)) ? $logo_name : '',
							  'type'		=> $accounttype,
							  'role'		=> $db->addMultiIds($role),
							  'sdate'		=> ( ! empty($start_date)) ? $start_date : '',
							  'edate'		=> ( ! empty($end_date)) ? $end_date : '',
							  'hspace'		=> ( ! empty($hosting_space)) ? $hosting_space : '',
							  'amount'		=> ( ! empty($amount)) ? $amount : '',
							  'amt_type'	=> ( ! empty($amount_type)) ? $amount_type : '',
							  'amt_code'	=> ( ! empty($currency_code)) ? $currency_code : '',
							  'pay_rec'		=> ( ! empty($amount_rec)) ? $amount_rec : '',
							  'smtp_hname'	=> ( ! empty($smtp_host_name)) ? $smtp_host_name : '',
							  'smtp_uname'	=> ( ! empty($smtp_user_name)) ? $smtp_user_name : '',
							  'smtp_pwd'	=> ( ! empty($smtp_pwd)) ? $smtp_pwd : '',
							  'smtp_port'	=> ( ! empty($smtp_port)) ? $smtp_port : '',
							  'mhome'		=> ( ! empty($mhome)) ? $mhome : '',
							  'mbrand'		=> ( ! empty($mbrand)) ? $mbrand : '',
							  'mdate'		=> $dtime,
							  'cdate'		=> $dtime);
				$addSql = "INSERT INTO `client_tbl` (`domain_name`, `db_name`, `account_type`, `organization`, `country`, `state`, `city`, `address`, `tax_id`, `logo`, `type`, `role`, `start_date`, `end_date`, `hosting_space`, `amount`, `amount_type`, `amount_cc`, `pay_received`, `smtp_host_name`, `smtp_user_name`, `smtp_pwd`, `smtp_port`, `mhome`, `mbrand`, `modified_date_time`, `create_date_time`) 
						   VALUES (:domain, :db_name, :atype, :org, :country, :state, :city, :add, :tax_id, :logo, :type, :role, :sdate, :edate, :hspace, :amount, :amt_type, :amt_code, :pay_rec, :smtp_hname, :smtp_uname, :smtp_pwd, :smtp_port, :mhome, :mbrand, :mdate, :cdate)";
				$stmt 	 = $main_conn_db->prepare($addSql); $stmt->execute($data);
				$mapid	 = $main_conn_db->lastInsertId();
				
				#---------Client-Mapping-------
				$mapdata = array('cid' => $cid, 'sub_cid' => $mapid);
				$mapSql  = "INSERT INTO `mapping_tbl` (`client_id`, `sub_client_id`) VALUES (:cid, :sub_cid)";
				$mapstmt = $main_conn_db->prepare($mapSql); $mapstmt->execute($mapdata);

				#---------Client-Contact-Detail-----
				#-------Executive-------
				if ( ! empty($executive_name)):
					$exeData = array('cid'		=> $mapid,
									'name'		=> $executive_name,
									'email'		=> ( ! empty($executive_email)) ? $executive_email : '',
									'pcode'		=> ( ! empty($executive_pcode)) ? $executive_pcode : '',
									'mob' 		=> ( ! empty($executive_mob)) ? $executive_mob : '',
									'status' 	=> 1,
									'type' 		=> 1);
					$exeSql  = "INSERT INTO `client_contact_detail` (`client_id`, `name`, `email`, `phone_code`, `phone`, `status`, `type`) 
								VALUES (:cid, :name, :email, :pcode, :mob, :status, :type)";
					$exestmt = $main_conn_db->prepare($exeSql); $exestmt->execute($exeData);
				endif;
				
				#-------Decision-Maker-------
				if ( ! empty($decision_name)):
					$deciData = array('cid'		=> $mapid,
									  'name'	=> $decision_name,
									  'email'	=> ( ! empty($decision_email)) ? $decision_email : '',
									  'pcode'	=> ( ! empty($decision_pcode)) ? $decision_pcode : '',
									  'mob' 	=> ( ! empty($decision_mob)) ? $decision_mob : '',
									  'status'	=> 1,
									  'type' 	=> 2);
					$deciSql  = "INSERT INTO `client_contact_detail` (`client_id`, `name`, `email`, `phone_code`, `phone`, `status`, `type`) 
								 VALUES (:cid, :name, :email, :pcode, :mob, :status, :type)";
					$decistmt = $main_conn_db->prepare($deciSql); $decistmt->execute($deciData);
				endif;

				#-------Billing-Contact-------
				if ( ! empty($billing_name)):
					$billData = array('cid' 	=> $mapid,
									  'name'	=> $billing_name,
									  'email'	=> ( ! empty($billing_email)) ? $billing_email : '',
									  'pcode'	=> ( ! empty($billing_pcode)) ? $billing_pcode : '',
									  'mob' 	=> ( ! empty($billing_mob)) ? $billing_mob : '',
									  'status'	=> 1,
									  'type' 	=> 3);
					$billSql  = "INSERT INTO `client_contact_detail` (`client_id`, `name`, `email`, `phone_code`, `phone`, `status`, `type`) 
								 VALUES (:cid, :name, :email, :pcode, :mob, :status, :type)";
					$billstmt = $main_conn_db->prepare($billSql); $billstmt->execute($billData);
				endif;

				#---------Client-Permissions-----
				$cPermisData = array('cid'		=> $mapid,
									 'cat'		=> ( ! empty($no_cat)) ? $no_cat : '',
									 'sim' 		=> ( ! empty($no_sim)) ? $no_sim : '',
									 'admin' 	=> ( ! empty($no_admin)) ? $no_admin : '',
									 'auth' 	=> ( ! empty($no_auth)) ? $no_auth : '',
									 'reviewer' => ( ! empty($no_reviewer)) ? $no_reviewer : '',
									 'learner'	=> ( ! empty($no_learner)) ? $no_learner : '',
									 'educator'	=> ( ! empty($no_educator)) ? $no_educator : '',
									 'temp'		=> ( ! empty($sim_tmp)) ? $db->addMultiIds($sim_tmp) : '',
									 'proctor'	=> ( ! empty($proctor)) ? $proctor : '');
				$cPermisSql  = "INSERT INTO `client_permissions` (`client_id`, `category`, `sim`, `admin`, `auth`, `reviewer`, `learner`, `educator`, `templates`, `auth_templates`, `proctor`) 
								VALUES (:cid, :cat, :sim, :admin, :auth, :reviewer, :learner, :educator, :temp, :temp, :proctor)";
				$cPermisstmt = $main_conn_db->prepare($cPermisSql); $cPermisstmt->execute($cPermisData);
				
				#--------Install-Client-DB-------
				$main_conn_db->query("USE $client_db_name");
				$database = (file_exists($es_db)) ? file_get_contents($es_db) : '';
				if ( ! empty($database)):
					$main_conn_db->query($database);
				else:
					json_encode(array('success' => FALSE, 'msg' => 'Server Error:: Database not Install.'));
					exit;
				endif;
				
				#--------Create-Client-Login------
				if ( ! empty($userName) && ! empty($password) && ! empty($loginType)):
					$userdata = array('username'	=> $userName,
									  'password' 	=> md5($password),
									  'role' 		=> $loginType,
									  'status' 		=> 1,
									  'lcp' 		=> $cdate,
									  'date' 		=> $cdate);
					$userSql  = "INSERT INTO `users` (`username`, `password`, `role`, `status`, `last_change_pwd`, `date`) 
								 VALUES (:username, :password, :role, :status, :lcp, :date)";
					$userstmt = $main_conn_db->prepare($userSql); $userstmt->execute($userdata);
				endif;
				$resdata = array('success' => TRUE, 'msg' => 'Account successfully created.');
			else:
				$resdata = array('success' => FALSE, 'msg' => 'Account not created. try again later.');
			endif;
		endif;
	endif;
	echo json_encode($resdata);
endif;

//--------------------------------Update-Client-Account---------------------------
if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['update_client_account'])):
	extract($_POST);
	$error = FALSE;
	if (empty($organization)):
		$resdata = array('success' => FALSE, 'msg' => 'Please enter all required fields.');
		$error = TRUE;
	elseif ($error == FALSE):
		#---------Update-Client-Account-------
		$data = array('atype'		=> $orgtype,
					  'org'			=> ( ! empty($organization)) ? $organization : '',
					  'country'		=> ( ! empty($company_country)) ? $company_country : '',
					  'state'		=> ( ! empty($company_state)) ? $company_state : '',
					  'city'		=> ( ! empty($company_city)) ? $company_city : '',
					  'address'		=> ( ! empty($bill_add)) ? $bill_add : '',
					  'tax_id'		=> ( ! empty($tax_id)) ? $tax_id : '',
					  'logo'		=> ( ! empty($logo_name)) ? $logo_name : '',
					  'type'		=> $accounttype,
					  'role'		=> $db->addMultiIds($role),
					  'sdate'		=> ( ! empty($start_date)) ? $start_date : '',
					  'edate'		=> ( ! empty($end_date)) ? $end_date : '',
					  'hspace'		=> ( ! empty($hosting_space)) ? $hosting_space : '',
					  'amount'		=> ( ! empty($amount)) ? $amount : '',
					  'amt_type'	=> ( ! empty($amount_type)) ? $amount_type : '',
					  'amt_code'	=> ( ! empty($currency_code)) ? $currency_code : '',
					  'pay_rec'		=> ( ! empty($amount_rec)) ? $amount_rec : '',
					  'smtp_hname'	=> ( ! empty($smtp_host_name)) ? $smtp_host_name : '',
					  'smtp_uname'	=> ( ! empty($smtp_user_name)) ? $smtp_user_name : '',
					  'smtp_pwd'	=> ( ! empty($smtp_pwd)) ? $smtp_pwd : '',
					  'smtp_port'	=> ( ! empty($smtp_port)) ? $smtp_port : '',
					  'mhome'		=> ( ! empty($mhome)) ? $mhome : '',
					  'mbrand'		=> ( ! empty($mbrand)) ? $mbrand : '',
					  'mdate'		=> $dtime,
					  'cid'			=> $client_id);
		$updateSql = "UPDATE `client_tbl` SET `account_type`=:atype, `organization`=:org, `country`=:country, `state`=:state, `city`=:city, `address`=:address, 
					  `tax_id`=:tax_id, `logo`=:logo, `type`=:type, `role`=:role, `start_date`=:sdate, `end_date`=:edate, `hosting_space`=:hspace, `amount`=:amount, 
					  `amount_type`=:amt_type, `amount_cc`=:amt_code, `pay_received`=:pay_rec, `smtp_host_name`=:smtp_hname, `smtp_user_name`=:smtp_uname, 
					  `smtp_pwd`=:smtp_pwd, `smtp_port`=:smtp_port, `mhome`=:mhome, `mbrand`=:mbrand, `modified_date_time`=:mdate WHERE `client_id`=:cid";
		$stmt = $main_conn_db->prepare($updateSql); $stmt->execute($data);
		
		#----Delete-Old-Client-Contact-Detail--
		$delCon	 = "DELETE FROM client_contact_detail WHERE client_id = '". $client_id ."'";
		$delConq = $main_conn_db->prepare($delCon); $delConq->execute();
		
		#---------Client-Contact-Detail-----
			#-------Executive-------
			if ( ! empty($executive_name)):
				$exeData = array('cid'		=> $client_id,
								 'name'		=> $executive_name,
								 'email'	=> ( ! empty($executive_email)) ? $executive_email : '',
								 'pcode'	=> ( ! empty($executive_pcode)) ? $executive_pcode : '',
								 'mob' 		=> ( ! empty($executive_mob)) ? $executive_mob : '',
								 'status' 	=> 1,
								 'type' 	=> 1);
				$exeSql  = "INSERT INTO `client_contact_detail` (`client_id`, `name`, `email`, `phone_code`, `phone`, `status`, `type`) 
							VALUES (:cid, :name, :email, :pcode, :mob, :status, :type)";
				$exestmt = $main_conn_db->prepare($exeSql); $exestmt->execute($exeData);
			endif;
			
			#-------Decision-Maker-------
			if ( ! empty($decision_name)):
				$deciData = array('cid'		=> $client_id,
								  'name'	=> $decision_name,
								  'email'	=> ( ! empty($decision_email)) ? $decision_email : '',
								  'pcode'	=> ( ! empty($decision_pcode)) ? $decision_pcode : '',
								  'mob' 	=> ( ! empty($decision_mob)) ? $decision_mob : '',
								  'status'	=> 1,
								  'type' 	=> 2);
				$deciSql  = "INSERT INTO `client_contact_detail` (`client_id`, `name`, `email`, `phone_code`, `phone`, `status`, `type`) 
							VALUES (:cid, :name, :email, :pcode, :mob, :status, :type)";
				$decistmt = $main_conn_db->prepare($deciSql); $decistmt->execute($deciData);
			endif;

			#-------Billing-Contact-------
			if ( ! empty($billing_name)):
				$billData = array('cid' 	=> $client_id,
								  'name'	=> $billing_name,
								  'email'	=> ( ! empty($billing_email)) ? $billing_email : '',
								  'pcode'	=> ( ! empty($billing_pcode)) ? $billing_pcode : '',
								  'mob' 	=> ( ! empty($billing_mob)) ? $billing_mob : '',
								  'status'	=> 1,
								  'type' 	=> 3);
				$billSql  = "INSERT INTO `client_contact_detail` (`client_id`, `name`, `email`, `phone_code`, `phone`, `status`, `type`) 
							VALUES (:cid, :name, :email, :pcode, :mob, :status, :type)";
				$billstmt = $main_conn_db->prepare($billSql); $billstmt->execute($billData);
			endif;

			#----Delete-Old-Client-Permissions--
			$delCpQ	 = "DELETE FROM client_permissions WHERE client_id = '". $client_id ."'";
			$delexe = $main_conn_db->prepare($delCpQ); $delexe->execute();

			#---------Client-Permissions-----
			$cPermisData = array('cat'		=> ( ! empty($no_cat)) ? $no_cat : '',
								 'sim' 		=> ( ! empty($no_sim)) ? $no_sim : '',
								 'admin' 	=> ( ! empty($no_admin)) ? $no_admin : '',
								 'auth' 	=> ( ! empty($no_auth)) ? $no_auth : '',
								 'reviewer' => ( ! empty($no_reviewer)) ? $no_reviewer : '',
								 'learner'	=> ( ! empty($no_learner)) ? $no_learner : '',
								 'educator'	=> ( ! empty($no_educator)) ? $no_educator : '',
								 'temp'		=> ( ! empty($sim_tmp)) ? $db->addMultiIds($sim_tmp) : '',
								 'proctor'	=> ( ! empty($proctor)) ? $proctor : '',
								 'cid'		=> $client_id);
			$cPermisSql  = "INSERT INTO `client_permissions` (`client_id`, `category`, `sim`, `admin`, `auth`, `reviewer`, `learner`, `educator`, `templates`, `auth_templates`, `proctor`) 
							VALUES (:cid, :cat, :sim, :admin, :auth, :reviewer, :learner, :educator, :temp, :temp, :proctor)";
			$cPermisstmt = $main_conn_db->prepare($cPermisSql); $cPermisstmt->execute($cPermisData);
		$resdata = array('success' => TRUE, 'msg' => 'Account successfully update.');
	else:
		$resdata = array('success' => FALSE, 'msg' => 'Account not update. try again later.');
	endif;
	echo json_encode($resdata);
endif;

//--------------------------------Create-Sub-Client-Account--------------------------
if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['create_sub_client_account'])):
	extract($_POST);
	$error = FALSE;
	$client_db_name = $db_prefix . $db_name;
	if (empty($db_name) || empty($domain_name) || empty($organization) || empty($userName) || empty($password)):
		$resdata = array('success' => FALSE, 'msg' => 'Please enter all required fields.');
		$error = TRUE;
	elseif ( ! empty($domain_name) && ! preg_match('~^[A-Za-z0-9_]{3,20}$~i', $domain_name)):
		$resdata = array('success' => FALSE, 'msg' => 'Enter valide subdomain.');
		$error = TRUE;
	elseif ($error == FALSE):
		$stmt = $main_conn_db->query("SELECT COUNT(*) FROM INFORMATION_SCHEMA.SCHEMATA WHERE SCHEMA_NAME = '". $client_db_name ."'"); $checkDB = $stmt->fetchColumn();
		if ($checkDB == 1):
			$resdata = array('success' => FALSE, 'msg' => 'Database already exists. choose a different database name.');
		elseif ($checkDB == 0):
			$sql = "CREATE DATABASE IF NOT EXISTS $client_db_name CHARACTER SET utf8 COLLATE utf8_general_ci";
			$res = $main_conn_db->prepare($sql);
			if ($res->execute()):
				#---------Create-Sub-Client-Account-------
				$data = array('domain'		=> trim($domain_name) . $dom_prefix,
							  'db_name'		=> $client_db_name,
							  'atype'		=> $orgtype,
							  'org'			=> ( ! empty($organization)) ? $organization : '',
							  'country'		=> ( ! empty($company_country)) ? $company_country : '',
							  'state'		=> ( ! empty($company_state)) ? $company_state : '',
							  'city'		=> ( ! empty($company_city)) ? $company_city : '',
							  'add'			=> ( ! empty($bill_add)) ? $bill_add : '',
							  'tax_id'		=> ( ! empty($tax_id)) ? $tax_id : '',
							  'logo'		=> ( ! empty($logo_name)) ? $logo_name : '',
							  'type'		=> 'subclient',
							  'role'		=> $db->addMultiIds($role),
							  'sdate'		=> ( ! empty($start_date)) ? $start_date : '',
							  'edate'		=> ( ! empty($end_date)) ? $end_date : '',
							  'hspace'		=> ( ! empty($hosting_space)) ? $hosting_space : '',
							  'amount'		=> ( ! empty($amount)) ? $amount : '',
							  'amt_type'	=> ( ! empty($amount_type)) ? $amount_type : '',
							  'amt_code'	=> ( ! empty($currency_code)) ? $currency_code : '',
							  'pay_rec'		=> ( ! empty($amount_rec)) ? $amount_rec : '',
							  'smtp_hname'	=> ( ! empty($smtp_host_name)) ? $smtp_host_name : '',
							  'smtp_uname'	=> ( ! empty($smtp_user_name)) ? $smtp_user_name : '',
							  'smtp_pwd'	=> ( ! empty($smtp_pwd)) ? $smtp_pwd : '',
							  'smtp_port'	=> ( ! empty($smtp_port)) ? $smtp_port : '',
							  'mhome'		=> ( ! empty($mhome)) ? $mhome : '',
							  'mbrand'		=> ( ! empty($mbrand)) ? $mbrand : '',
							  'mdate'		=> $dtime,
							  'cdate'		=> $dtime);
				$addSql = "INSERT INTO `client_tbl` (`domain_name`, `db_name`, `account_type`, `organization`, `country`, `state`, `city`, `address`, `tax_id`, `logo`, `type`, `role`, `start_date`, `end_date`, `hosting_space`, `amount`, `amount_type`, `amount_cc`, `pay_received`, `smtp_host_name`, `smtp_user_name`, `smtp_pwd`, `smtp_port`, `mhome`, `mbrand`, `modified_date_time`, `create_date_time`) 
						   VALUES (:domain, :db_name, :atype, :org, :country, :state, :city, :add, :tax_id, :logo, :type, :role, :sdate, :edate, :hspace, :amount, :amt_type, :amt_code, :pay_rec, :smtp_hname, :smtp_uname, :smtp_pwd, :smtp_port, :mhome, :mbrand, :mdate, :cdate)";
				$stmt 	 = $main_conn_db->prepare($addSql); $stmt->execute($data);
				$mapid	 = $main_conn_db->lastInsertId();
				
				#---------Client-Mapping-------
				$mapdata = array('cid' => $cid, 'sub_cid' => $mapid);
				$mapSql  = "INSERT INTO `mapping_tbl` (`client_id`, `sub_client_id`) VALUES (:cid, :sub_cid)";
				$mapstmt = $main_conn_db->prepare($mapSql); $mapstmt->execute($mapdata);

				#---------Client-Contact-Detail-----
				#-------Executive-------
				if ( ! empty($executive_name)):
					$exeData = array('cid'		=> $mapid,
									'name'		=> $executive_name,
									'email'		=> ( ! empty($executive_email)) ? $executive_email : '',
									'pcode'		=> ( ! empty($executive_pcode)) ? $executive_pcode : '',
									'mob' 		=> ( ! empty($executive_mob)) ? $executive_mob : '',
									'status' 	=> 1,
									'type' 		=> 1);
					$exeSql  = "INSERT INTO `client_contact_detail` (`client_id`, `name`, `email`, `phone_code`, `phone`, `status`, `type`) 
								VALUES (:cid, :name, :email, :pcode, :mob, :status, :type)";
					$exestmt = $main_conn_db->prepare($exeSql); $exestmt->execute($exeData);
				endif;
				
				#-------Decision-Maker-------
				if ( ! empty($decision_name)):
					$deciData = array('cid'		=> $mapid,
									  'name'	=> $decision_name,
									  'email'	=> ( ! empty($decision_email)) ? $decision_email : '',
									  'pcode'	=> ( ! empty($decision_pcode)) ? $decision_pcode : '',
									  'mob' 	=> ( ! empty($decision_mob)) ? $decision_mob : '',
									  'status'	=> 1,
									  'type' 	=> 2);
					$deciSql  = "INSERT INTO `client_contact_detail` (`client_id`, `name`, `email`, `phone_code`, `phone`, `status`, `type`) 
								 VALUES (:cid, :name, :email, :pcode, :mob, :status, :type)";
					$decistmt = $main_conn_db->prepare($deciSql); $decistmt->execute($deciData);
				endif;

				#-------Billing-Contact-------
				if ( ! empty($billing_name)):
					$billData = array('cid' 	=> $mapid,
									  'name'	=> $billing_name,
									  'email'	=> ( ! empty($billing_email)) ? $billing_email : '',
									  'pcode'	=> ( ! empty($billing_pcode)) ? $billing_pcode : '',
									  'mob' 	=> ( ! empty($billing_mob)) ? $billing_mob : '',
									  'status'	=> 1,
									  'type' 	=> 3);
					$billSql  = "INSERT INTO `client_contact_detail` (`client_id`, `name`, `email`, `phone_code`, `phone`, `status`, `type`) 
								 VALUES (:cid, :name, :email, :pcode, :mob, :status, :type)";
					$billstmt = $main_conn_db->prepare($billSql); $billstmt->execute($billData);
				endif;

				#---------Client-Permissions-----
				$cPermisData = array('cid'		=> $mapid,
									 'category'	=> ( ! empty($no_cat)) ? $no_cat : '',
									 'sim' 		=> ( ! empty($no_sim)) ? $no_sim : '',
									 'admin' 	=> ( ! empty($no_admin)) ? $no_admin : '',
									 'auth' 	=> ( ! empty($no_auth)) ? $no_auth : '',
									 'reviewer' => ( ! empty($no_reviewer)) ? $no_reviewer : '',
									 'learner'	=> ( ! empty($no_learner)) ? $no_learner : '',
									 'educator'	=> ( ! empty($no_educator)) ? $no_educator : '',
									 'temp'		=> ( ! empty($sim_tmp)) ? $db->addMultiIds($sim_tmp) : '',
									 'proctor'	=> ( ! empty($proctor)) ? $proctor : '');
				$cPermisSql  = "INSERT INTO `client_permissions` (`client_id`, `category`, `sim`, `admin`, `auth`, `reviewer`, `learner`, `educator`, `templates`, `auth_templates`, `proctor`) 
								VALUES (:cid, :category, :sim, :admin, :auth, :reviewer, :learner, :educator, :temp, :temp, :proctor)";
				$cPermisstmt = $main_conn_db->prepare($cPermisSql); $cPermisstmt->execute($cPermisData);
				
				#--------Install-Client-DB-------
				$main_conn_db->query("USE $client_db_name");
				$database = (file_exists($es_db)) ? file_get_contents($es_db) : '';
				if ( ! empty($database)):
					$main_conn_db->query($database);
				else:
					json_encode(array('success' => FALSE, 'msg' => 'Server Error:: Database not Install.'));
					exit;
				endif;
				
				#--------Create-Client-Login-------
				if ( ! empty($userName) && ! empty($password) && ! empty($loginType)):
					$userdata = array('username'	=> $userName,
									  'password' 	=> md5($password),
									  'role' 		=> $loginType,
									  'status' 		=> 1,
									  'lcp' 		=> $cdate,
									  'date' 		=> $cdate);
					$userSql  = "INSERT INTO `users` (`username`, `password`, `role`, `status`, `last_change_pwd`, `date`) 
								 VALUES (:username, :password, :role, :status, :lcp, :date)";
					$userstmt = $main_conn_db->prepare($userSql); $userstmt->execute($userdata);
				endif;
				$resdata = array('success' => TRUE, 'msg' => 'Account successfully created.');
			else:
				$resdata = array('success' => FALSE, 'msg' => 'Account not created. try again later.');
			endif;
		endif;
	endif;
	echo json_encode($resdata);
endif;

//--------------------------------Update-Sub-Client-Account---------------------------
if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['update_sub_client_account'])):
	extract($_POST);
	$error = FALSE;
	if (empty($organization)):
		$resdata = array('success' => FALSE, 'msg' => 'Please enter all required fields.');
		$error = TRUE;
	elseif ($error == FALSE):
		#---------Update-Client-Account-------
		$data = array('atype'		=> $orgtype,
					  'org'			=> ( ! empty($organization)) ? $organization : '',
					  'country'		=> ( ! empty($company_country)) ? $company_country : '',
					  'state'		=> ( ! empty($company_state)) ? $company_state : '',
					  'city'		=> ( ! empty($company_city)) ? $company_city : '',
					  'address'		=> ( ! empty($bill_add)) ? $bill_add : '',
					  'tax_id'		=> ( ! empty($tax_id)) ? $tax_id : '',
					  'logo'		=> ( ! empty($logo_name)) ? $logo_name : '',
					  'role'		=> $db->addMultiIds($role),
					  'sdate'		=> ( ! empty($start_date)) ? $start_date : '',
					  'edate'		=> ( ! empty($end_date)) ? $end_date : '',
					  'hspace'		=> ( ! empty($hosting_space)) ? $hosting_space : '',
					  'amount'		=> ( ! empty($amount)) ? $amount : '',
					  'amt_type'	=> ( ! empty($amount_type)) ? $amount_type : '',
					  'amt_code'	=> ( ! empty($currency_code)) ? $currency_code : '',
					  'pay_rec'		=> ( ! empty($amount_rec)) ? $amount_rec : '',
					  'smtp_hname'	=> ( ! empty($smtp_host_name)) ? $smtp_host_name : '',
					  'smtp_uname'	=> ( ! empty($smtp_user_name)) ? $smtp_user_name : '',
					  'smtp_pwd'	=> ( ! empty($smtp_pwd)) ? $smtp_pwd : '',
					  'smtp_port'	=> ( ! empty($smtp_port)) ? $smtp_port : '',
					  'mhome'		=> ( ! empty($mhome)) ? $mhome : '',
					  'mbrand'		=> ( ! empty($mbrand)) ? $mbrand : '',
					  'mdate'		=> $dtime,
					  'cid'			=> $client_id);
		$updateSql = "UPDATE `client_tbl` SET `account_type`=:atype, `organization`=:org, `country`=:country, `state`=:state, `city`=:city, `address`=:address, 
					  `tax_id`=:tax_id, `logo`=:logo, `role`=:role, `start_date`=:sdate, `end_date`=:edate, `hosting_space`=:hspace, `amount`=:amount, 
					  `amount_type`=:amt_type, `amount_cc`=:amt_code, `pay_received`=:pay_rec, `smtp_host_name`=:smtp_hname, `smtp_user_name`=:smtp_uname, 
					  `smtp_pwd`=:smtp_pwd, `smtp_port`=:smtp_port, `mhome`=:mhome, `mbrand`=:mbrand, `modified_date_time`=:mdate WHERE `client_id`=:cid";
		$stmt = $main_conn_db->prepare($updateSql); $stmt->execute($data);
		
		#----Delete-Old-Client-Contact-Detail--
		$delCon	 = "DELETE FROM client_contact_detail WHERE client_id = '". $client_id ."'";
		$delConq = $main_conn_db->prepare($delCon); $delConq->execute();
		
		#---------Client-Contact-Detail-----
			#-------Executive-------
			if ( ! empty($executive_name)):
				$exeData = array('cid'		=> $client_id,
								 'name'		=> $executive_name,
								 'email'	=> ( ! empty($executive_email)) ? $executive_email : '',
								 'pcode'	=> ( ! empty($executive_pcode)) ? $executive_pcode : '',
								 'mob' 		=> ( ! empty($executive_mob)) ? $executive_mob : '',
								 'status' 	=> 1,
								 'type' 	=> 1);
				$exeSql  = "INSERT INTO `client_contact_detail` (`client_id`, `name`, `email`, `phone_code`, `phone`, `status`, `type`) 
							VALUES (:cid, :name, :email, :pcode, :mob, :status, :type)";
				$exestmt = $main_conn_db->prepare($exeSql); $exestmt->execute($exeData);
			endif;
			
			#-------Decision-Maker-------
			if ( ! empty($decision_name)):
				$deciData = array('cid'		=> $client_id,
								  'name'	=> $decision_name,
								  'email'	=> ( ! empty($decision_email)) ? $decision_email : '',
								  'pcode'	=> ( ! empty($decision_pcode)) ? $decision_pcode : '',
								  'mob' 	=> ( ! empty($decision_mob)) ? $decision_mob : '',
								  'status'	=> 1,
								  'type' 	=> 2);
				$deciSql  = "INSERT INTO `client_contact_detail` (`client_id`, `name`, `email`, `phone_code`, `phone`, `status`, `type`) 
								VALUES (:cid, :name, :email, :pcode, :mob, :status, :type)";
				$decistmt = $main_conn_db->prepare($deciSql); $decistmt->execute($deciData);
			endif;

			#-------Billing-Contact-------
			if ( ! empty($billing_name)):
				$billData = array('cid' 	=> $client_id,
								  'name'	=> $billing_name,
								  'email'	=> ( ! empty($billing_email)) ? $billing_email : '',
								  'pcode'	=> ( ! empty($billing_pcode)) ? $billing_pcode : '',
								  'mob' 	=> ( ! empty($billing_mob)) ? $billing_mob : '',
								  'status'	=> 1,
								  'type' 	=> 3);
				$billSql  = "INSERT INTO `client_contact_detail` (`client_id`, `name`, `email`, `phone_code`, `phone`, `status`, `type`) 
								VALUES (:cid, :name, :email, :pcode, :mob, :status, :type)";
				$billstmt = $main_conn_db->prepare($billSql); $billstmt->execute($billData);
			endif;

			#----Delete-Old-Client-Permissions--
			$delCpQ	 = "DELETE FROM client_permissions WHERE client_id = '". $client_id ."'";
			$delexe = $main_conn_db->prepare($delCpQ); $delexe->execute();

			#---------Client-Permissions-----
			$cPermisData = array('cat'		=> ( ! empty($no_cat)) ? $no_cat : '',
								 'sim' 		=> ( ! empty($no_sim)) ? $no_sim : '',
								 'admin' 	=> ( ! empty($no_admin)) ? $no_admin : '',
								 'auth' 	=> ( ! empty($no_auth)) ? $no_auth : '',
								 'reviewer' => ( ! empty($no_reviewer)) ? $no_reviewer : '',
								 'learner'	=> ( ! empty($no_learner)) ? $no_learner : '',
								 'educator'	=> ( ! empty($no_educator)) ? $no_educator : '',
								 'temp'		=> ( ! empty($sim_tmp)) ? $db->addMultiIds($sim_tmp) : '',
								 'proctor'	=> ( ! empty($proctor)) ? $proctor : '',
								 'cid'		=> $client_id);
			$cPermisSql  = "INSERT INTO `client_permissions` (`client_id`, `category`, `sim`, `admin`, `auth`, `reviewer`, `learner`, `educator`, `templates`, `auth_templates`, `proctor`) 
							VALUES (:cid, :cat, :sim, :admin, :auth, :reviewer, :learner, :educator, :temp, :temp, :proctor)";
			$cPermisstmt = $main_conn_db->prepare($cPermisSql); $cPermisstmt->execute($cPermisData);
		$resdata = array('success' => TRUE, 'msg' => 'Account successfully update.');
	else:
		$resdata = array('success' => FALSE, 'msg' => 'Account not update. try again later.');
	endif;
	echo json_encode($resdata);
endif;

//-----------------------Delete-Client-Account---------------------------
if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['delete_client'])):
	extract($_POST);
	$client = $db->getClient($client_id);
	if ( ! empty($client)):
		#-------------Remove-Client-DB-------------
		$sql = "DROP DATABASE ". $client['db_name'];
		$q = $main_conn_db->prepare($sql);
		if ($q->execute()):
			$msql  = "DELETE t1.*, t2.*, t3.*, t4.*, t5.* ";
			$msql .= "FROM client_tbl t1 
					  LEFT JOIN mapping_tbl t2 ON t1.client_id = t2.sub_client_id 
					  LEFT JOIN client_permissions t3 ON t1.client_id = t3.client_id 
					  LEFT JOIN client_contact_detail t4 ON t1.client_id = t4.client_id 
					  LEFT JOIN client_branding t5 ON t1.client_id = t5.client_id 
					  WHERE t1.client_id = '". $client['client_id'] ."'";
			$mq = $main_conn_db->prepare($msql);
			if ($mq->execute()):
				#-------------Remove-Client-Upload-Directory-------------
				if ( is_dir($fpath . $client['domain_name'])) @unlink($fpath . $client['domain_name']);
				$res = array('success' => TRUE, 'msg' => 'Account deleted successfully.');
			else:
				$res = array('success' => FALSE, 'msg' => 'Account not deleted. Please try again later.');
			endif;
		else:
			$res = array('success' => FALSE, 'msg' => 'Account not deleted. Please try again later.');
		endif;
	else:
		$res = array('success' => FALSE, 'msg' => 'Account not deleted. Please try again later.');
	endif;
	echo json_encode($res);
endif;

//-----------------------Active-Client-Account---------------------------
if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['active_client'])):
	$getCid	= $_POST['client_id'];
	$sql	= "UPDATE client_tbl SET status = 0 WHERE client_id = '". $getCid ."'";
	$q 		= $main_conn_db->prepare($sql);
	$res 	= ($q->execute()) ? array('success' => TRUE, 'msg' => 'Account active successfully.') : array('success' => FALSE, 'msg' => 'Account not active. Please try again later.');
	echo json_encode($res);
endif;

//-----------------------Inactive-Client-Account---------------------------
if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['inactive_client'])):
	$getCid	= $_POST['client_id'];
	$sql 	= "UPDATE client_tbl SET status = 1 WHERE client_id = '". $getCid ."'";
	$q 		= $main_conn_db->prepare($sql);
	$res 	= ($q->execute()) ? array('success' => TRUE, 'msg' => 'Account inactive successfully.') : array('success' => FALSE, 'msg' => 'Account not inactive. Please try again later.');
	echo json_encode($res);
endif;

//-------------------------------Site-Branding--------------------------
if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['update_branding'])):
	extract($_POST);
	#----Delete-Old-Detail--
	$delCon	 = "DELETE FROM client_branding WHERE client_id = '". $client_id ."'";
	$delConq = $main_conn_db->prepare($delCon); $delConq->execute();
	
	#----Add-New-Detail--
	$exeData = array('cid'			=> $client_id,
					 'btn_color'	=> ( ! empty($btn_color)) ? $btn_color : '',
					 'btn_hcolor'	=> ( ! empty($btn_hover_color)) ? $btn_hover_color : '',
					 'icon_color'	=> ( ! empty($icon_color)) ? $icon_color : '',
					 'icon_hcolor' 	=> ( ! empty($icon_hover_color)) ? $icon_hover_color : '',
					 'font_type' 	=> ( ! empty($font_type)) ? $font_type : '',
					 'font_color' 	=> ( ! empty($font_color)) ? $font_color : '',
					 'font_size' 	=> ( ! empty($font_size)) ? $font_size : '',
					 'date_time'	=> $dtime);
	$exeSql  = "INSERT INTO `client_branding` (`client_id`, `btn_color`, `btn_hcolor`, `icon_color`, `icon_hcolor`, `font_type`, `font_color`, `font_size`, `added_date_time`) 
				VALUES (:cid, :btn_color, :btn_hcolor, :icon_color, :icon_hcolor, :font_type, :font_color, :font_size, :date_time)";
	$exestmt = $main_conn_db->prepare($exeSql);
	$res = ($exestmt->execute($exeData)) ? array('success' => TRUE, 'msg' => 'Branding Added successfully.', 'return_url' => 'manage-branding.php') : array('success' => FALSE, 'msg' => 'Branding not added. Please try again later.');
	echo json_encode($res);
endif;

//------Get-Country-State-City---------
if (isset($_GET['type'])):
	$type = $_GET['type'];
	if ($type == 'getCountry'):
		$res = $db->getCountry();
	elseif ($type == 'getState' && ! empty($_GET['countryId'])):
		$res = $db->getState($_GET['countryId']);
	elseif ($type == 'getCity' && ! empty($_GET['stateId'])):
		$res = $db->getCity($_GET['stateId']);
	endif;
	echo json_encode($res);
endif;

//--------------------------Get-Client-List--------------------------
if ($_SERVER['REQUEST_METHOD'] == 'GET' && isset($_GET['getClients'])):
	if (isset($_GET['getSubClients'])):
		$cdata = $db->getClientData('c.client_id, c.domain_name, c.organization', "client_tbl AS c LEFT JOIN mapping_tbl AS m ON c.client_id = m.sub_client_id", " WHERE m.client_id = '". $cid ."'  AND status = 0");
	else:
		$cdata = $db->getClientData('client_id, domain_name, organization', 'client_tbl', " WHERE type != 'super_admin'");
	endif;
	if ( ! empty($cdata)):
		foreach ($cdata as $val):
			$data[] = array('value' => $val['client_id'], 'label' => $val['domain_name'], 'title' => $val['domain_name']. ' ('. $val['organization'] .')');
		endforeach;
		$res = array('success' => TRUE, 'data' => $data);
	else:
		$res = array('success' => FALSE, 'data' => NULL);
	endif;
	echo json_encode($res);
endif;

//--------------------------Get-Default-Data----------------------
if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['getDefaultData'])):
	$group	= $admin = $auth = $rev = $learn = $edu = 0;
	$cdata	= $db->getClient($cid);
    $org    = $cdata['organization'];
    
	/*** Total Client ***/
    $client = $db->getTotalClient(1);
    /*** Total Sub-Client ***/
    $subclient = $db->getTotalClient(2);

	$ocdata = $db->getClientData('db_name', 'client_tbl', " WHERE type != 'super_admin' AND status = 0");
	if ( ! empty($ocdata)):
		foreach ($ocdata as $data):
			$stmt	= $main_conn_db->query("SELECT COUNT(*) FROM INFORMATION_SCHEMA.SCHEMATA WHERE SCHEMA_NAME = '". $data['db_name'] ."'");
			$check	= $stmt->fetchColumn();
			if ($check == 1):
				/* Select Client Database */
				$main_conn_db->query("USE ". $data['db_name']);
				/*** Total GROUP ***/
				$gsql 	= "SELECT group_id FROM group_tbl";
				$gq	  	= $main_conn_db->prepare($gsql); $gq->execute();
				$group	+= $gq->rowCount();
				/*** Total ADMIN ***/
				$adsql	= "SELECT id FROM users WHERE role = 'admin'";
				$adq	= $main_conn_db->prepare($adsql); $adq->execute();
				$admin	+= $adq->rowCount();
				/*** Total AUTHOR ***/
				$ausql	= "SELECT id FROM users WHERE role = 'author'";
				$auq	= $main_conn_db->prepare($ausql); $auq->execute();
				$auth	+= $auq->rowCount();
				/*** Total REVIEWER ***/
				$revsql	= "SELECT id FROM users WHERE role = 'reviewer'";
				$revq	= $main_conn_db->prepare($revsql); $revq->execute();
				$rev	+= $revq->rowCount();
				/*** Total LEARNER ***/
				$lesql	= "SELECT id FROM users WHERE role = 'learner'";
				$leq	= $main_conn_db->prepare($lesql); $leq->execute();
				$learn	+= $leq->rowCount();
				/*** Total EDUCATOR ***/
				$eduql	= "SELECT id FROM users WHERE role = 'educator'";
				$eduq	= $main_conn_db->prepare($eduql); $eduq->execute();
				$edu	+= $eduq->rowCount();
			endif;
		endforeach;
	endif;
	$res  = array('success'			=> TRUE,
				  'organization'	=> $org,
				  'client'          => $client,
				  'subclient'       => $subclient,
				  'data'			=> array('group' 	=> $group,
											 'admin' 	=> $admin,
											 'auth'  	=> $auth,
											 'reviewer'	=> $rev,
											 'learner'	=> $learn,
											 'educator'	=> $edu)
				);
	echo json_encode($res);
endif;

//--------------------------Get-Client-Other-Data----------------------
if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['getClientData']) && ! empty($_POST['client_id'])):
	$getCid = $_POST['client_id'];
	$cdata	= $db->getClient($getCid);
	$org	= $cdata['organization'];
	$sdate	= $cdata['start_date'];
	$edate	= $cdata['end_date'];
	$hspace	= $cdata['hosting_space'];
	$uspace = $db->folderSize($fpath . $cdata['domain_name']);
	#---Check-DB---
	$stmt	= $main_conn_db->query("SELECT COUNT(*) FROM INFORMATION_SCHEMA.SCHEMATA WHERE SCHEMA_NAME = '". $cdata['db_name'] ."'");
	$check	= $stmt->fetchColumn();
	if ($check == 1):
		/* GET CLIENT DETAILS */
		/*** Total CLIENT ***/
		if ($cdata['type'] == 'client'):
			$csql 	= "SELECT mid FROM mapping_tbl WHERE client_id = '". $cid ."'";
			$cq	  	= $main_conn_db->prepare($csql); $cq->execute();
			$client	= $cq->rowCount();
		else:
			$client = FALSE;
		endif;
		/* Select Client Database */
		$main_conn_db->query("USE ". $cdata['db_name']);
		/*** Total GROUP ***/
		$gsql 	= "SELECT group_id FROM group_tbl";
		$gq	  	= $main_conn_db->prepare($gsql); $gq->execute();
		$group	= $gq->rowCount();
		/*** Total ADMIN ***/
		$adsql	= "SELECT id FROM users WHERE role = 'admin'";
		$adq	= $main_conn_db->prepare($adsql); $adq->execute();
		$admin	= $adq->rowCount();
		/*** Total AUTHOR ***/
		$ausql	= "SELECT id FROM users WHERE role = 'author'";
		$auq	= $main_conn_db->prepare($ausql); $auq->execute();
		$auth	= $auq->rowCount();
		/*** Total REVIEWER ***/
		$revsql	= "SELECT id FROM users WHERE role = 'reviewer'";
		$revq	= $main_conn_db->prepare($revsql); $revq->execute();
		$rev	= $revq->rowCount();
		/*** Total LEARNER ***/
		$lesql	= "SELECT id FROM users WHERE role = 'learner'";
		$leq	= $main_conn_db->prepare($lesql); $leq->execute();
		$learn	= $leq->rowCount();
		/*** Total EDUCATOR ***/
		$eduql	= "SELECT id FROM users WHERE role = 'educator'";
		$eduq	= $main_conn_db->prepare($eduql); $eduq->execute();
		$edu	= $eduq->rowCount();

		$res  = array('success' 		=> TRUE,
					  'organization'	=> $org,
					  'start_date'		=> $sdate,
					  'end_date'		=> $edate,
					  'space'			=> $hspace .' MB',
					  'used_space'		=> $uspace,
					  'data'			=> array('client' 	=> $client,
					  							 'group' 	=> $group,
												 'admin' 	=> $admin,
												 'auth'  	=> $auth,
												 'reviewer'	=> $rev,
												 'learner'	=> $learn,
												 'educator'	=> $edu)
					);
	else:
		$res = array('success' => FALSE, 'msg' => 'Client Database not found.');
	endif;
	echo json_encode($res);
endif;

//-------------------------Get-Client-Admin-Default-Data--------------------
if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['getClientAdminDefaultData'])):
	$group	= $admin = $auth = $rev = $learn = $edu = 0;
	$cdata	= $db->getClient($cid);
    $org    = $cdata['organization'];
    /*** Total Sub-Client ***/
    $subclient = $db->getTotalSubClient($cid);
	$ocdata = $db->getClientData('c.db_name', "client_tbl AS c LEFT JOIN mapping_tbl AS m ON c.client_id = m.sub_client_id", " WHERE m.client_id = '". $cid ."'  AND status = 0");
	if ( ! empty($ocdata)):
		foreach ($ocdata as $data):
			$stmt	= $main_conn_db->query("SELECT COUNT(*) FROM INFORMATION_SCHEMA.SCHEMATA WHERE SCHEMA_NAME = '". $data['db_name'] ."'");
			$check	= $stmt->fetchColumn();
			if ($check == 1):
				/* Select Client Database */
				$main_conn_db->query("USE ". $data['db_name']);
				/*** Total GROUP ***/
				$gsql 	= "SELECT group_id FROM group_tbl";
				$gq	  	= $main_conn_db->prepare($gsql); $gq->execute();
				$group	+= $gq->rowCount();
				/*** Total ADMIN ***/
				$adsql	= "SELECT id FROM users WHERE role = 'admin'";
				$adq	= $main_conn_db->prepare($adsql); $adq->execute();
				$admin	+= $adq->rowCount();
				/*** Total AUTHOR ***/
				$ausql	= "SELECT id FROM users WHERE role = 'author'";
				$auq	= $main_conn_db->prepare($ausql); $auq->execute();
				$auth	+= $auq->rowCount();
				/*** Total REVIEWER ***/
				$revsql	= "SELECT id FROM users WHERE role = 'reviewer'";
				$revq	= $main_conn_db->prepare($revsql); $revq->execute();
				$rev	+= $revq->rowCount();
				/*** Total LEARNER ***/
				$lesql	= "SELECT id FROM users WHERE role = 'learner'";
				$leq	= $main_conn_db->prepare($lesql); $leq->execute();
				$learn	+= $leq->rowCount();
				/*** Total EDUCATOR ***/
				$eduql	= "SELECT id FROM users WHERE role = 'educator'";
				$eduq	= $main_conn_db->prepare($eduql); $eduq->execute();
				$edu	+= $eduq->rowCount();
			endif;
		endforeach;
	endif;
	$res  = array('success'			=> TRUE,
				  'organization'	=> $org,
				  'subclient'       => $subclient,
				  'data'			=> array('group' 	=> $group,
											 'admin' 	=> $admin,
											 'auth'  	=> $auth,
											 'reviewer'	=> $rev,
											 'learner'	=> $learn,
											 'educator'	=> $edu)
				);
	echo json_encode($res);
endif;

ob_flush();
$db->closeConnection();
unset($db);
