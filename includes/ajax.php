<?php 
ob_start();
session_start();

/* SET-TIMEZONE */
if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['timezone'])):
	$_SESSION['timezone'] = $_POST['timezone'];
	$_SESSION['offset']   = $_POST['offset'];
endif;

/******************* Class ********************/
require_once dirname(dirname(__FILE__)) . '/config/db.class.php';

/******************* DBConnection ********************/
$db 		= new DBConnection();
$user		= (isset($_SESSION['username'])) ? $_SESSION['username'] : '';
$role		= (isset($_SESSION['role'])) ? $_SESSION['role'] : '';
$userId		= (isset($_SESSION['userId'])) ? $_SESSION['userId'] : '';
$domain 	= (isset($_SESSION['domain_name'])) ? $_SESSION['domain_name'] : $_SERVER['HTTP_HOST'];
$curdate	= date('Y-m-d H:i:s a');

/* RETURN SIM DATA NOT ASSIGNED  */
if ($_SERVER['REQUEST_METHOD'] == 'GET' && isset($_GET['getScenario'])):
	$catid = (isset($_GET['catid'])) ? $_GET['catid'] : '';
	if ( ! empty($catid)):
		$sim_id = $db->getAssignSim();
    	$sql = "SELECT scenario_id AS value, Scenario_title AS label, Scenario_title AS title FROM scenario_master WHERE scenario_category = '". $catid ."' AND status = '8'";
		if ( ! empty($sim_id)):
		 $sql .= " AND scenario_id NOT IN ($sim_id)";
		endif;
		$results = $db->prepare($sql);
		if ($results->execute() && $results->rowCount() > 0):
			foreach ($results->fetchAll(PDO::FETCH_ASSOC) as $row):
				$data[] = $row;
			endforeach;
			$res = array('success' => TRUE, 'data' => $data);
		else:
			$res = array('success' => FALSE, 'data' => NULL);
		endif;
	else:
		$res = array('success' => FALSE, 'data' => NULL);
	endif;
	echo json_encode($res);
endif;

if ($_SERVER['REQUEST_METHOD'] == 'GET' && isset($_GET['getAssignScenario'])):
	$catid = (isset($_GET['catid'])) ? $_GET['catid'] : '';
	if ( ! empty($catid)):
		$sim_id = $db->getAssignSim();
		if ( ! empty($sim_id)):
			$sql1 = "SELECT scenario_id FROM scenario_master WHERE scenario_category = '". $catid ."' AND status = '8' AND scenario_id IN ($sim_id)";
			$result = $db->prepare($sql1);
			if ($result->execute() && $result->rowCount() > 0):
				foreach ($result->fetchAll(PDO::FETCH_ASSOC) as $rowdata):
					$seleetd_data[] = $rowdata;
					$getSimdata[] = $rowdata['scenario_id'];
				endforeach;
			endif;
		endif;
		#---Get-Sim-Data--
		$sql = "SELECT scenario_id, Scenario_title FROM scenario_master WHERE scenario_category = '". $catid ."' AND status = '8'";
		$results = $db->prepare($sql);
		if ($results->execute() && $results->rowCount() > 0):
			$i = 0;
			foreach ($results->fetchAll(PDO::FETCH_ASSOC) as $row):
				if ( ! empty($seleetd_data[$i]['scenario_id']) && $seleetd_data[$i]['scenario_id'] == $row['scenario_id']):
					$selected = true;
				else:
					$selected = '';
				endif;
				$data[] = array('label' => $row['Scenario_title'], 'title' => $row['Scenario_title'], 'value' => $row['scenario_id'], 'selected' => $selected);
				$i++;
			endforeach;
		endif;
		#---Get-Sim-Other-Data--
		$getSimId = ( ! empty($getSimdata)) ? $db->addMultiIds($getSimdata) : '';
		if ( ! empty($getSimId)):
			$other_sql = "SELECT assignment_id, scenario_id, group_id, learner_id, start_date, end_date, no_attempts FROM assignment_tbl WHERE scenario_id IN ($getSimId)";
			$other_results = $db->prepare($other_sql);
			if ($other_results->execute() && $other_results->rowCount() > 0):
				foreach ($other_results->fetchAll(PDO::FETCH_ASSOC) as $other_row):
					$other_data[] = array('assignment_id'	=> $other_row['assignment_id'],
										  'scenario_id'		=> $other_row['scenario_id'],
										  'group_id'		=> $other_row['group_id'],
										  'learner_id' 		=> $other_row['learner_id'],
										  'start_date' 		=> $other_row['start_date'],
										  'end_date' 		=> $other_row['end_date'],
										  'no_attempts' 	=> $other_row['no_attempts']);
				endforeach;
			endif;
		endif;
		if ( ! empty($other_data)):
			$res = array('success' => TRUE, 'data' => $data, 'other_data' => $other_data);
		else:
			$res = array('success' => FALSE, 'data' => NULL, 'other_data' => NULL);
		endif;
	else:
		$res = array('success' => FALSE, 'data' => NULL);
	endif;
	echo json_encode($res);
endif;

if ($_SERVER['REQUEST_METHOD'] == 'GET' && isset($_GET['getScenarioUpdate'])):
	$catid = (isset($_GET['catid'])) ? $_GET['catid'] : '';
	if ( ! empty($catid)):
		$sim_id = $db->getAssignSim();
		$sql = "SELECT scenario_id AS value, Scenario_title AS label, Scenario_title AS title FROM scenario_master WHERE scenario_category = '". $catid ."' AND status = '8'";
		if ( ! empty($sim_id)):
		 $sql .= " AND scenario_id IN ($sim_id)";
		endif;
    	$results = $db->prepare($sql);
		if ($results->execute() && $results->rowCount() > 0):
			foreach ($results->fetchAll(PDO::FETCH_ASSOC) as $row):
				$data[] = $row;
			endforeach;
			$res = array('success' => TRUE, 'data' => $data);
		else:
			$res = array('success' => FALSE, 'data' => NULL);
		endif;
	else:
		$res = array('success' => FALSE, 'data' => NULL);
	endif;
	echo json_encode($res);
endif;

if ($_SERVER['REQUEST_METHOD'] == 'GET' && isset($_GET['getAssignScenarioData']) &&  ! empty($_GET['sim_id'])):
	$sim_id = (isset($_GET['sim_id'])) ? $_GET['sim_id'] : '';
	if ( ! empty($sim_id)):
		$other_sql = "SELECT assignment_id, scenario_id, group_id, learner_id, start_date, end_date, no_attempts FROM assignment_tbl WHERE FIND_IN_SET($sim_id, scenario_id)";
		$other_results = $db->prepare($other_sql);
		if ($other_results->execute() && $other_results->rowCount() > 0):
			foreach ($other_results->fetchAll(PDO::FETCH_ASSOC) as $other_row):
				$other_data[] = array('assignment_id'	=> $other_row['assignment_id'],
									  'scenario_id'		=> $other_row['scenario_id'],
									  'group_id'		=> $other_row['group_id'],
									  'learner_id' 		=> $other_row['learner_id'],
									  'start_date' 		=> $other_row['start_date'],
									  'end_date' 		=> $other_row['end_date'],
									  'no_attempts' 	=> $other_row['no_attempts']);
			endforeach;
		endif;
		if ( ! empty($other_data)):
			$res = array('success' => TRUE, 'other_data' => $other_data);
		else:
			$res = array('success' => FALSE, 'other_data' => NULL);
		endif;
	else:
		$res = array('success' => FALSE, 'other_data' => NULL);
	endif;
	echo json_encode($res);
endif;

if ($_SERVER['REQUEST_METHOD'] == 'GET' && isset($_GET['getLearner'])):
	$sim_id = (isset($_GET['sim_id'])) ? $_GET['sim_id'] : '';
	if ( ! empty($sim_id)):
		$sql = "SELECT GROUP_CONCAT(DISTINCT group_id) AS groups, GROUP_CONCAT(DISTINCT learner_id) AS learners FROM assignment_tbl WHERE FIND_IN_SET('". $sim_id ."', scenario_id)";
		$q = $db->prepare($sql);
		if ($q->execute() && $q->rowCount() > 0):
			foreach ($q->fetchAll(PDO::FETCH_ASSOC) as $row):
				if ( ! empty($row['groups'])):
					foreach ($db->getLearnerByGroup($row['groups']) as $gdata):
						$groupData[] = array('value' => $gdata['learner'], 'label' => $gdata['group_name'], 'title' => $gdata['group_name']);
					endforeach;
					$group = TRUE;
				else:
					$groupData[] = array();
					$group = FALSE;
				endif;
				if ( ! empty($row['learners'])):
					$ll = explode(',', $row['learners']);
					$ldata = $db->addMultiIds(array_filter(array_unique($ll)));
					$learnerData = $db->getLearnerMultiId($ldata);
					$learner = TRUE;
				else:
					$learnerData[] = array();
					$learner = FALSE;
				endif;
			endforeach;
			$res = array('success' => TRUE, 'group' => $group, 'groupData' => $groupData, 'learner' => $learner, 'learnerData' => $learnerData);
		else:
			$res = array('success' => FALSE, 'group' => FALSE, 'learner' => FALSE);
		endif;
	endif;
	echo json_encode($res);
endif;

if ($_SERVER['REQUEST_METHOD'] == 'GET' && isset($_GET['get_system_reporting_other_data'])):
	$sim_id = (isset($_GET['sim_id'])) ? $_GET['sim_id'] : '';
	$assign_group = ''; $assign_learner = [];
	if ( ! empty($sim_id)):
		$sql = "SELECT assignment_id, GROUP_CONCAT(DISTINCT IF(group_id = '', null, group_id)) AS gid, GROUP_CONCAT(DISTINCT IF(learner_id = '', null, learner_id)) AS lid FROM assignment_tbl WHERE scenario_id IN ($sim_id) AND scenario_id IN (SELECT scenario_id FROM scenario_attempt_tbl)";
		$q = $db->prepare($sql);
		if ($q->execute() && $q->rowCount() > 0):
			$unit = $location = $designation = [];
			foreach ($q->fetchAll(PDO::FETCH_ASSOC) as $row):
				if ( ! empty($row['gid'])):
					foreach ($db->GetEducator($row['gid']) as $edata):
						$educator_data[] = array('label' => $edata['username'] . '-' . $edata['group_name'], 'title' => $edata['username'], 'value' => $edata['learner']);
						$getlearnerId[]  = $edata['learner'];
					endforeach;
					$getGroup = $db->getLearnerByGroup($row['gid']);
					foreach ($getGroup as $data):
						$group_data[] = array('label' => $data['group_name'], 'title' => $data['group_name'], 'value' => $data['learner']);
					endforeach;
					$assign_learner[] = $educator_data;
					$assign_group     = TRUE;
					if ( ! empty($getlearnerId)):
						$lids 		 = $db->addMultiIds($getlearnerId);
						$unit 		 = $db->GetFilterUsers($lids, 'department', 'department');
						$location 	 = $db->GetFilterUsers($lids, 'location', 'location');
						$designation = $db->GetFilterUsers($lids, 'designation', 'designation');
					endif;
				elseif ( ! empty($row['lid']) && empty($row['gid'])):
					foreach ($db->getLearnerMultiId($row['lid']) as $ldata):
						$learner_data[] = array('label' => $ldata['username'], 'title' => $ldata['username'], 'value' => $ldata['id']);
					endforeach;
					$assign_learner[] = $learner_data;
					$assign_group 	  = FALSE;
					$unit 			  = $db->GetFilterUsers($row['lid'], 'department', 'department');
					$location 		  = $db->GetFilterUsers($row['lid'], 'location', 'location');
					$designation 	  = $db->GetFilterUsers($row['lid'], 'designation', 'designation');
				endif;
			endforeach;
			$res = array('success' => TRUE, 'assign_group' => $assign_group, 'data' => $assign_learner[0], 'unit' => $unit, 'location' => $location, 'designation' => $designation, 'group' => $group_data);
		else:
			$res = array('success' => FALSE, 'data' => NULL);
		endif;
	endif;
	echo json_encode($res);
endif;

if ($_SERVER['REQUEST_METHOD'] == 'GET' && isset($_GET['getResetSimLearner'])):
	$sim_id = (isset($_GET['sim_id'])) ? $_GET['sim_id'] : '';
	if ( ! empty($sim_id)):
		$sql = "SELECT GROUP_CONCAT(DISTINCT group_id) AS groups, GROUP_CONCAT(DISTINCT learner_id) AS learners FROM assignment_tbl WHERE FIND_IN_SET('". $sim_id ."', scenario_id)";
		$q = $db->prepare($sql);
		if ($q->execute() && $q->rowCount() > 0):
			foreach ($q->fetchAll(PDO::FETCH_ASSOC) as $row):
				if ( ! empty($row['groups'])):
					foreach ($db->GetResetLearner($row['groups']) as $data):
						$user_data[] = array('value' => $data['id'], 'label' => $data['username'], 'title' => $data['username']. ' ('. $data['full_name'] .' - '. $data['email'] .')');
					endforeach;
				else:
					$user_data[] = array();
				endif;
			endforeach;
			$res = array('success' => TRUE, 'user_data' => $user_data);
		else:
			$res = array('success' => FALSE, 'user_data' => NULL);
		endif;
	endif;
	echo json_encode($res);
endif;

if ($_SERVER['REQUEST_METHOD'] == 'GET' && isset($_GET['getTooltipData'])):
	$ids = (isset($_GET['ids'])) ? $_GET['ids'] : '';
	if ( ! empty($ids)): ?>
    <table class="table">
      <thead>
        <tr>
          <th scope="col">#</th>
          <th scope="col">User Name</th>
          <th scope="col">Email</th>
        </tr>
      </thead>
      <tbody>
      <?php $i = 1; foreach ($db->getMultiUsers($ids) as $users): ?>
        <tr>
          <th scope="row"><?php echo $i ?></th>
          <td><strong><?php echo $users['fname'] .' '. $users['lname'] ?></strong></td>
          <td><?php echo $users['email'] ?></td>
        </tr>
       <?php $i++; endforeach; ?>
      </tbody>
    </table>
    <?php 
	endif;
endif;

if ($_SERVER['REQUEST_METHOD'] == 'GET' && isset($_GET['get_learner'])):
	$lid = (isset($_GET['learnerId'])) ? $_GET['learnerId'] : '';
	if ( ! empty($lid)): ?>
    <table class="table">
      <thead>
        <tr>
          <th scope="col">#</th>
          <th scope="col">User Name</th>
          <th scope="col">Full Name</th>
          <th scope="col">Location</th>
          <th scope="col">Company</th>
          <th scope="col">Unit</th>
          <th scope="col">Designation</th>
        </tr>
      </thead>
      <tbody>
      <?php $columns = 'username, fname, lname, location, company, department, designation';
	  $i = 1; foreach ($db->getUsersColumns($lid, $columns) as $user): ?>
        <tr>
          <th scope="row"><?php echo $i ?></th>
          <td><?php echo $user['username']; ?></td>
          <td><?php echo $user['fname']. ' '.$user['lname']; ?></td>
          <td><?php echo $user['location']; ?></td>
          <td><?php echo $user['company']; ?></td>
          <td><?php echo $user['department']; ?></td>
          <td><?php echo $user['designation']; ?></td>
        </tr>
       <?php $i++; endforeach; ?>
      </tbody>
    </table>
    <?php 
	endif;
endif;

if ($_SERVER['REQUEST_METHOD'] == 'GET' && isset($_GET['getCharBackground']) && isset($_GET['type'])):
	$type = (isset($_GET['type'])) ? $_GET['type'] : '';
	if ( ! empty($type)):
		$data = $db->getUpload($type);
		if ( ! empty($data)):
			$res = array('success' => TRUE, 'data' => $data);
		else:
			$res = array('success' => FALSE, 'data' => NULL);
		endif;
	else:
		$res = array('success' => FALSE, 'data' => NULL);
	endif;
	echo json_encode($res);
endif;

if ($_SERVER['REQUEST_METHOD'] == 'GET' && isset($_GET['getLearnerByGroup']) && ! empty($_GET['group_id'])):
	$group_id = (isset($_GET['group_id'])) ? $_GET['group_id'] : '';
	if ( ! empty($group_id)):
		foreach ($db->GetResetLearner($group_id) as $data):
			$user_data[] = array('value' => $data['id'], 'label' => $data['username'], 'title' => $data['username']. ' ('. $data['full_name'] .' - '. $data['email'] .')');
		endforeach;
		if ( ! empty($user_data)):
			$res = array('success' => TRUE, 'data' => $user_data);
		else:
			$res = array('success' => FALSE, 'data' => NULL);
		endif;
	else:
		$res = array('success' => FALSE, 'data' => NULL);
	endif;
	echo json_encode($res);
endif;

/*************** Get-Educator-SIM ***************/
if ($_SERVER['REQUEST_METHOD'] == 'GET' && isset($_GET['getEducatorScenario'])):
	$catid = (isset($_GET['catid'])) ? $_GET['catid'] : '';
	if ( ! empty($catid)):
		$getGroups = $db->getGroupByEducator("GROUP_CONCAT(DISTINCT group_id SEPARATOR ',') as col", $userId);
		if ( ! empty($getGroups[0]['col'])):
			$sim_id  = $db->getAssignSimByGroupId($getGroups[0]['col']);
			$sql 	 = "SELECT scenario_id AS value, Scenario_title AS label, Scenario_title AS title FROM scenario_master WHERE scenario_category = '". $catid ."' AND status = '8' AND scenario_id IN ($sim_id)";
    		$results = $db->prepare($sql);
			if ($results->execute() && $results->rowCount() > 0):
				foreach ($results->fetchAll(PDO::FETCH_ASSOC) as $row):
					$data[] = $row;
				endforeach;
				$res = array('success' => TRUE, 'data' => $data);
			else:
				$res = array('success' => FALSE, 'data' => NULL);
			endif;
		else:
			$res = array('success' => FALSE, 'data' => NULL);
		endif;
	else:
		$res = array('success' => FALSE, 'data' => NULL);
	endif;
	echo json_encode($res);
endif;

/*************** Get-Educator-SIM-Group ***************/
if ($_SERVER['REQUEST_METHOD'] == 'GET' && isset($_GET['getEducatorScenarioGroup'])):
	$sim_id = (isset($_GET['sim_id'])) ? $_GET['sim_id'] : '';
	if ( ! empty($sim_id)):
		$simGroup = $db->getAssignSimGroup($sim_id);
		$getAssignSim = $db->getLearnerByGroup($simGroup, $userId);
		if ( ! empty($getAssignSim)):
			foreach ($getAssignSim as $group):
				$data[] = array('value' => $group['group_id'], 'label' => $group['group_name']);
			endforeach;
			$res = array('success' => TRUE, 'data' => $data);
		else:
			$res = array('success' => FALSE, 'data' => NULL);
		endif;
	else:
		$res = array('success' => FALSE, 'data' => NULL);
	endif;
	echo json_encode($res);
endif;

/***************Educator-Dash-Learner-Report***************/
if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['getlearnerData']) && isset($_POST['learner_id']) && isset($_POST['scenario_id'])):
	$learner_id	 = $_POST['learner_id'];
	$scenario_id = $_POST['scenario_id'];
	$lastAttempt = $db->getLastAttemptOpenResp($scenario_id, $learner_id);
	if ( ! empty($lastAttempt)):
		$uid			= $lastAttempt['uid'];
		$scenario_data  = $db->getScenario(md5($scenario_id));
		$allocated_time = ( ! empty($scenario_data['duration'])) ? gmdate('H:i:s', ($scenario_data['duration']) * 60) : '00';

		#-----------COMPLETE-INCOMPLETE-----------
		$totalQuestion	  = $db->getTotalQuestionsByScenario(md5($scenario_id));
		$totalAttemptQues = $db->getTotalAttemptQuesOpenResp($scenario_id, $learner_id);

		#-----------Progress-Time-----------
		$totalTime  = $db->getOpenResLastTotalTime($scenario_id, $learner_id, $uid);
		$taken_time =  ( ! empty($totalTime['timeSum'])) ? $totalTime['timeSum'] : '00';

		$sqlcom 	= "SELECT (SUM(comp_val_1) + SUM(comp_val_2) + SUM(comp_val_3) + SUM(comp_val_4) + SUM(comp_val_5) + SUM(comp_val_6)) as totalCompval FROM competency_tbl WHERE scenario_id = '". $scenario_id ."'";
		$comexc 	= $db->prepare($sqlcom); $comexc->execute();
		$comdata 	= $comexc->fetch(PDO::FETCH_ASSOC);
		$totalComp	= $comdata['totalCompval'];

		$sqlans		= "SELECT (SUM(ans_val1) + SUM(ans_val2) + SUM(ans_val3) + SUM(ans_val4) + SUM(ans_val5) + SUM(ans_val6)) as totalAnsval FROM open_res_tbl WHERE scenario_id = '". $scenario_id ."' AND uid = '". $uid ."'";
		$ansexc		= $db->prepare($sqlans); $ansexc->execute();
		$ansdata	= $ansexc->fetch(PDO::FETCH_ASSOC);
		$totalAns	= $ansdata['totalAnsval'];

		$passing_marks	= ($scenario_data['passing_marks'] > 0) ? $scenario_data['passing_marks'] : 0;
		$achieved_score	= $db->get_percentage($totalComp, $totalAns);
		$result			=  ($achieved_score > 0 && $achieved_score >= $passing_marks) ? 'PASS' : 'REMEDIATION REQUIRED';

		#-----------JOB READINESS--------
		$sql = "SELECT *, (SUM(weightage_1) + SUM(weightage_2) + SUM(weightage_3) + SUM(weightage_4) + SUM(weightage_5) + SUM(weightage_6)) as totalWScore FROM competency_tbl WHERE scenario_id = '". $scenario_id ."'";
		$q	 = $db->prepare($sql); $q->execute();
		$row = $q->fetch(PDO::FETCH_ASSOC);
		$totalWScore = $row['totalWScore'];

		$sqlSc = "SELECT SUM(ans_val1) as com_score_1, SUM(ans_val2) as com_score_2, SUM(ans_val3) as com_score_3, SUM(ans_val4) as com_score_4, SUM(ans_val5) as com_score_5, SUM(ans_val6) as com_score_6 FROM open_res_tbl ans WHERE scenario_id = '". $scenario_id ."' AND uid = '". $uid ."'";
		$sc = $db->prepare($sqlSc); $sc->execute();
		foreach ($sc->fetchAll(PDO::FETCH_ASSOC) as $scRow):
			$wedata[] = $scRow;
		endforeach;

		$sqlQSc = "SELECT SUM(q.ques_val_1) as com_qes_score_1, SUM(q.ques_val_2) as com_qes_score_2, SUM(q.ques_val_3) as com_qes_score_3, SUM(q.ques_val_4) as com_qes_score_4, SUM(q.ques_val_5) as com_qes_score_5, SUM(q.ques_val_6) as com_qes_score_6 FROM open_res_tbl sc LEFT JOIN question_tbl q ON sc.question_id = q.question_id WHERE sc.scenario_id = '". $scenario_id ."' AND sc.uid = '". $uid ."'";
		$qsc = $db->prepare($sqlQSc); $qsc->execute();
		foreach ($qsc->fetchAll(PDO::FETCH_ASSOC) as $QscRow):
			$qdata[] = $QscRow;
		endforeach;

		$getweightage_1 = ( ! empty($qdata[0]['com_qes_score_1'])) ? $qdata[0]['com_qes_score_1'] / $wedata[0]['com_score_1'] : '';
		$getweightage_2 = ( ! empty($qdata[0]['com_qes_score_2'])) ? $qdata[0]['com_qes_score_2'] / $wedata[0]['com_score_2'] : '';
		$getweightage_3 = ( ! empty($qdata[0]['com_qes_score_3'])) ? $qdata[0]['com_qes_score_3'] / $wedata[0]['com_score_3'] : '';
		$getweightage_4 = ( ! empty($qdata[0]['com_qes_score_4'])) ? $qdata[0]['com_qes_score_4'] / $wedata[0]['com_score_4'] : '';
		$getweightage_5 = ( ! empty($qdata[0]['com_qes_score_5'])) ? $qdata[0]['com_qes_score_5'] / $wedata[0]['com_score_5'] : '';
		$getweightage_6 = ( ! empty($qdata[0]['com_qes_score_6'])) ? $qdata[0]['com_qes_score_6'] / $wedata[0]['com_score_6'] : '';

		$weightage_achieved = array(( ! empty($getweightage_1) && ! empty($row['comp_col_1'])) ? round($row['weightage_1'] / $getweightage_1) : 0,
									( ! empty($getweightage_2) && ! empty($row['comp_col_2'])) ? round($row['weightage_2'] / $getweightage_2) : 0,
									( ! empty($getweightage_3) && ! empty($row['comp_col_3'])) ? round($row['weightage_3'] / $getweightage_3) : 0,
									( ! empty($getweightage_4) && ! empty($row['comp_col_4'])) ? round($row['weightage_4'] / $getweightage_4) : 0,
									( ! empty($getweightage_5) && ! empty($row['comp_col_5'])) ? round($row['weightage_5'] / $getweightage_5) : 0,
									( ! empty($getweightage_6) && ! empty($row['comp_col_6'])) ? round($row['weightage_6'] / $getweightage_6) : 0);
		$totalWAScore = array_sum($weightage_achieved);

		$res = array('success' => TRUE, 'data' => array('allow_time' 	=> $allocated_time,
														'taken_time' 	=> $taken_time,
														'total_ques' 	=> ( ! empty($totalQuestion)) ? $totalQuestion : 0,
														'taken_ques' 	=> ( ! empty($totalAttemptQues)) ? $totalAttemptQues : 0,
														'total_score' 	=> ( ! empty($totalComp)) ? $totalComp : 0,
														'achieve_score'	=> ( ! empty($totalAns)) ? $totalAns : 0,
														'result'		=> $result,
														'totalWScore'	=> $totalWScore,
														'totalWAScore'	=> $totalWAScore
													));
	else:
		$res = array('success' => FALSE, 'msg' => 'Data not found.');
	endif;
	echo json_encode($res);
endif;

/* Educator Group Report */
if ($_SERVER['REQUEST_METHOD'] == 'GET' && isset($_GET['group_performance_educator'])):
	$sim_id = (isset($_GET['sim_id'])) ? $_GET['sim_id'] : '';
	if ( ! empty($sim_id)):
		$sql = "SELECT GROUP_CONCAT(DISTINCT IF(group_id = '', null, group_id)) AS gid FROM assignment_tbl WHERE scenario_id IN ($sim_id) AND scenario_id IN (SELECT scenario_id FROM scenario_attempt_tbl)";
		$q = $db->prepare($sql);
		if ($q->execute() && $q->rowCount() > 0):
			foreach ($q->fetchAll(PDO::FETCH_ASSOC) as $row):
				if ( ! empty($row['gid'])):
					$getGroup = $db->getLearnerByGroup($row['gid'], $userId);
					foreach ($getGroup as $data):
						$group_data[] = array('label' => $data['group_name'], 'title' => $data['group_name'], 'value' => $data['learner']);
						$learnerId[]  = $data['learner'];
					endforeach;
					if ( ! empty($learnerId)):
						$lids 		 = $db->addMultiIds($learnerId);
						$unit 		 = $db->GetFilterUsers($lids, 'department', 'department');
						$location 	 = $db->GetFilterUsers($lids, 'location', 'location');
						$designation = $db->GetFilterUsers($lids, 'designation', 'designation');
					endif;
				endif;
			endforeach;
			$res = array('success' => TRUE, 'data' => $group_data, 'unit' => $unit, 'location' => $location, 'designation' => $designation);
		else:
			$res = array('success' => FALSE, 'data' => NULL);
		endif;
	endif;
	echo json_encode($res);
endif;

/* Educator Group Report */
if ($_SERVER['REQUEST_METHOD'] == 'GET' && isset($_GET['group_performance_educator_other'])):
	$group_id = (isset($_GET['group_id'])) ? $_GET['group_id'] : '';
	if ( ! empty($group_id)):
		$learnerId[]  = $group_id;
		if ( ! empty($learnerId)):
			$lids 		 = $db->addMultiIds($learnerId);
			$unit 		 = $db->GetFilterUsers($lids, 'department', 'department');
			$location 	 = $db->GetFilterUsers($lids, 'location', 'location');
			$designation = $db->GetFilterUsers($lids, 'designation', 'designation');
			$res = array('success' => TRUE, 'unit' => $unit, 'location' => $location, 'designation' => $designation);
		else:
			$res = array('success' => FALSE);
		endif;
	endif;
	echo json_encode($res);
endif;

/* Educator Dash : Group Report */
if ($_SERVER['REQUEST_METHOD'] == 'GET' && isset($_GET['getGroupReport']) && ! empty($_GET['gid']) && ! empty($_GET['sid'])):
	$gid 	= $_GET['gid'];
	$sid 	= $_GET['sid'];
	$gdata	= $db->getGroupColumns($gid, 'learner');
	$col	= ( ! empty($gdata[0]['col'])) ? $gdata[0]['col'] : [];
	$totalL	= $attempted = $pass = $fail = '';
	if ( ! empty($col)):
		if (strpos($col, ',') == TRUE):
			$lids	= explode(',', $col);
			$totalL = count($lids);
		else:
			$totalL = count($col);
		endif;

		/* COMPETENCY TOTAL SCORE */
		$sqlcom 	= "SELECT *, (SUM(comp_val_1) + SUM(comp_val_2) + SUM(comp_val_3) + SUM(comp_val_4) + SUM(comp_val_5) + SUM(comp_val_6)) AS totalCompval, (SUM(weightage_1) + SUM(weightage_2) + SUM(weightage_3) + SUM(weightage_4) + SUM(weightage_5) + SUM(weightage_6)) AS totalWeightage FROM competency_tbl WHERE scenario_id = '". $sid ."'";
		$comexc  	= $db->prepare($sqlcom); $comexc->execute();
		$comp 	 	= $comexc->fetch(PDO::FETCH_ASSOC);
		$comval	 	= $comp['totalCompval'];
		$tWeightage = $comp['totalWeightage'];

		/* GET LAST ATTEMPTED SIM DATA */
		$sql = "SELECT t.* FROM (SELECT DISTINCT st.aid, st.uid, st.userid, s.scenario_id, s.sim_temp_type, s.Scenario_title AS sim_title, s.passing_marks FROM 
				scenario_attempt_tbl AS st LEFT JOIN 
				scenario_master AS s ON s.scenario_id = st.scenario_id WHERE 
				st.scenario_id IN ($sid) AND 
				st.userid IN ($col) AND 
				(st.uid IN (SELECT uid FROM score_tbl) OR st.uid IN (SELECT uid FROM multi_score_tbl) OR st.uid IN (SELECT uid FROM open_res_tbl)) GROUP BY st.userid ORDER BY st.aid DESC) t";
		$q	= $db->prepare($sql); $q->execute();
		$rows = $q->rowCount();
		if ($rows > 0):
			$pass = 0; $fail = 0;
			foreach ($q->fetchAll(PDO::FETCH_ASSOC) as $row):
				$temp_type 		= $row['sim_temp_type'];
				$passing_marks	= ($row['passing_marks'] > 0) ? $row['passing_marks'] : 0;
				if ($temp_type == 2):
					$sqlans = "SELECT (SUM(q.ques_val_1) + SUM(q.ques_val_2) + SUM(q.ques_val_3) + SUM(q.ques_val_4) + SUM(q.ques_val_5) + SUM(q.ques_val_6)) AS totalAnsval FROM 
							   multi_score_tbl sc LEFT JOIN question_tbl q ON sc.question_id = q.question_id WHERE 
							   sc.status = 1 AND sc.scenario_id = '". $sid ."' AND sc.uid = '". $row['uid'] ."'";
				elseif ($temp_type == 7):
					$sqlans  = "SELECT (SUM(ans_val1) + SUM(ans_val2) + SUM(ans_val3) + SUM(ans_val4) + SUM(ans_val5) + SUM(ans_val6)) AS totalAnsval FROM 
								open_res_tbl WHERE scenario_id = '". $sid ."' AND uid = '". $row['uid'] ."'";
				elseif ($temp_type == 1 || $temp_type == 8):
					$sqlans  = "SELECT (SUM(a.ans_val1) + SUM(a.ans_val2) + SUM(a.ans_val3) + SUM(a.ans_val4) + SUM(a.ans_val5) + SUM(a.ans_val6)) AS totalAnsval FROM 
								score_tbl sc LEFT JOIN answer_tbl a ON sc.choice_option_id = a.answer_id WHERE 
								sc.scenario_id = '". $sid ."' AND sc.uid = '". $row['uid'] ."'";
				endif;
				$ansexc  = $db->prepare($sqlans); $ansexc->execute();
				$ans 	 = $ansexc->fetch(PDO::FETCH_ASSOC);
				$ansval	 = $ans['totalAnsval'];
				$score	 = $db->get_percentage($comval, $ansval);
				if ($score > 0 && $score >= $passing_marks):
					$pass++;
				else:
					$fail++;
				endif;
			endforeach;
		endif;
		$attempted = ( ! empty($rows)) ? $db->get_percentage($totalL, $rows) : 0;
		$totalPass = ( ! empty($pass)) ? $db->get_percentage($totalL, $pass) : 0;
		$totalFail = ( ! empty($fail)) ? $db->get_percentage($totalL, $fail) : 0;
		$jReadines = ( ! empty($pass)) ? round($totalL / $pass) : 0;
		$res 	   = ['success'		=> TRUE,
					  'totalL'		=> $totalL,
					  'attempted'	=> $attempted,
					  'totalpass'	=> $totalPass,
					  'totalfail'	=> $totalFail,
					  'jreadines'	=> $jReadines
					];
	else:
		$res = ['success' => FALSE];
	endif;
	echo json_encode($res);
endif;

/* GET USERS DATA BY USER ID */
if ($_SERVER['REQUEST_METHOD'] == 'GET' && isset($_GET['getUserData']) && ! empty($_GET['uid'])):
	$uid = (isset($_GET['uid'])) ? $_GET['uid'] : '';
	if ( ! empty($uid)):
		$column	= "id, CONCAT(fname, ' ', lname) AS fullname, username, email, company, department, location, designation";
		$udata	= $db->getUsersColumns($uid, $column);
		if ( ! empty($udata)):
			foreach ($udata as $data):
				$user_data[] = array('value' => $data['id'], 'label' => $data['username']. ' ('. $data['fullname'] .' - '. $data['email'] .')', 'title' => $data['username']. ' ('. $data['company'] .' - '. $data['department'] .' - '. $data['location'] .' - '. $data['designation'] .')');
			endforeach;
			$res = array('success' => TRUE, 'data' => $user_data);
		else:
			$res = array('success' => FALSE, 'data' => NULL);
		endif;
	else:
		$res = array('success' => FALSE, 'data' => NULL);
	endif;
	echo json_encode($res);
endif;

/* GET SIM ATTEMPTS DATA BY USER ID & SIM ID */
if ($_SERVER['REQUEST_METHOD'] == 'GET' && isset($_GET['getUserSimAttempts']) && ! empty($_GET['uid']) && ! empty($_GET['sid'])):
	$sid = (isset($_GET['sid'])) ? $_GET['sid'] : '';
	$uid = (isset($_GET['uid'])) ? $_GET['uid'] : '';
	if ( ! empty($sid) && ! empty($uid)):
		$udata = $db->getLastAttemptMultiSIM($sid, $uid, 100);
		if ( ! empty($udata)):
			$res = array('success' => TRUE, 'data' => $udata);
		else:
			$res = array('success' => FALSE, 'data' => NULL);
		endif;
	else:
		$res = array('success' => FALSE, 'data' => NULL);
	endif;
	echo json_encode($res);
endif;

$db->closeConnection();
ob_flush();
