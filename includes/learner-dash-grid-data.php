<?php 
@ob_start();
@session_start();



/******************* Class ********************/
require_once dirname(dirname(__FILE__)) . '/config/db.class.php';

/******************* DBConnection ********************/
$db 		= new DBConnection();
$user		= (isset($_SESSION['username'])) ? $_SESSION['username'] : '';
$role		= (isset($_SESSION['role'])) ? $_SESSION['role'] : '';
$userId		= (isset($_SESSION['userId'])) ? $_SESSION['userId'] : '';
$cid		= (isset($_SESSION['client_id'])) ? $_SESSION['client_id'] : exit('Oops. something went wrong please try again');
$user		= (isset($_SESSION['username'])) ? $_SESSION['username'] : '';
//-------------------------GET-LEARNER-SIM-LIST-------------------------
if ($_SERVER['REQUEST_METHOD'] == 'GET' && isset($_GET['getLearnerSim'])):
//	echo "test";die;
	$date		= ( ! empty($_GET['date'])) ? explode(' - ', $_GET['date']) : exit('error');
	$sdate		= $date[0];
	$edate		= $date[1];
	$sim_data 	= [];
	//$userId = ( ! empty($_GET['userId'])) ? $_GET['userId'] : '';
	$sim_list	= $db->getAssignSimData($userId);
	//print_r($sim_list);die;
	/* GET LAST ATTEMPLETD SIM */
	$sql = "SELECT t.* FROM (SELECT DISTINCT st.aid, st.uid, st.userid, s.scenario_id, s.sim_temp_type, s.Scenario_title AS sim_title, s.passing_marks FROM 
			scenario_attempt_tbl AS st LEFT JOIN 
			scenario_master AS s ON s.scenario_id = st.scenario_id WHERE 
			s.scenario_id IN ($sim_list) AND st.userid = $userId AND DATE(attempt_date) BETWEEN '". $sdate ."' AND '". $edate ."' AND 
			(st.uid IN (SELECT uid FROM score_tbl) OR st.uid IN (SELECT uid FROM multi_score_tbl) OR st.uid IN (SELECT uid FROM open_res_tbl)) ORDER BY st.aid DESC) t GROUP BY t.scenario_id";
			//print_r( $sql);die;
	$q	= $db->prepare($sql); $q->execute();
	if ($q->rowCount() > 0):
		foreach ($q->fetchAll(PDO::FETCH_ASSOC) as $row):
			$sim_data[] = array('value' => $row['scenario_id'], 'label' => $row['sim_title'], 'title' => $row['sim_title']);
		endforeach;
	else:
		$sim_data = [];
	endif;
	$resdata = ['success' => TRUE, 'sim_data' => $sim_data];
	echo json_encode($resdata);
endif;

//-------------------------GET-TOP-FILTER-DATA--------------------------
if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['topFilterData']) && ! empty($_POST['sim_id'])):
	$date		= ( ! empty($_POST['date'])) ? explode(' - ', $_POST['date']) : exit('error');
	$sim_id		= $_POST['sim_id'];
	$sdate		= $date[0];
	$edate		= $date[1];
	$userId = ( ! empty($_POST['userId'])) ? $_POST['userId'] : '';
	
	$cur_learner_data = $other_learner_data = $ul_learner_data = $cur_learner_time = $other_cur_learner_time = $assignGroup = [];
	$strongCompLable1 = $strongCompLable2 = $strongCompLable3 = $strongCompLable4 = $strongCompLable5 = $strongCompLable6 = $strongCompScore1 = $strongCompScore2 = $strongCompScore3 = $strongCompScore4 = $strongCompScore5 = $strongCompScore6 = '';
	$weakCompLable1   = $weakCompLable2 = $weakCompLable3 = $weakCompLable4 = $weakCompLable5 = $weakCompLable6 = $weakCompScore1 = $weakCompScore2 = $weakCompScore3 = $weakCompScore4 = $weakCompScore5 = $weakCompScore6 = $pass1url = $pass2url = $report_page = '';
	$other_learner_average_time = '00:00:00';
	$pass1 = $pass2 = $fail = $pass1score = $pass2score = $max = $max2 = $ul_max = $ul_max2 = 0;

	/* GET CUR-LEARNER DATA */
	$cur_learner 			= $db->getUsers(md5($userId));
	$cur_learner_unit		= $cur_learner['department'];
	$cur_learner_location	= $cur_learner['location'];

	/* COMPETENCY TOTAL SCORE */
	$sqlcom 	= "SELECT *, (SUM(comp_val_1) + SUM(comp_val_2) + SUM(comp_val_3) + SUM(comp_val_4) + SUM(comp_val_5) + SUM(comp_val_6)) AS totalCompval, (SUM(weightage_1) + SUM(weightage_2) + SUM(weightage_3) + SUM(weightage_4) + SUM(weightage_5) + SUM(weightage_6)) AS totalWeightage FROM competency_tbl WHERE scenario_id = '". $sim_id ."'";
	$comexc  	= $db->prepare($sqlcom); $comexc->execute();
	$comp 	 	= $comexc->fetch(PDO::FETCH_ASSOC);
	$comval	 	= $comp['totalCompval'];
	$tWeightage = $comp['totalWeightage'];

	/* Total Attempted Simulation */
	$totalAttempt = $db->getTotalAttempt($sim_id, $userId);
	
	/* GET LAST ATTEMPTED SIM DATA CURRENT LEARNER */
	$sql = "SELECT t.* FROM (SELECT DISTINCT st.aid, st.uid, st.userid, s.scenario_id, s.sim_temp_type, s.Scenario_title AS sim_title, s.passing_marks FROM 
			scenario_attempt_tbl AS st LEFT JOIN 
			scenario_master AS s ON s.scenario_id = st.scenario_id WHERE 
			st.scenario_id IN ($sim_id) AND 
			st.userid = $userId AND 
			DATE(attempt_date) BETWEEN '". $sdate ."' AND '". $edate ."' AND 
			(st.uid IN (SELECT uid FROM score_tbl) OR st.uid IN (SELECT uid FROM multi_score_tbl) OR st.uid IN (SELECT uid FROM open_res_tbl)) ORDER BY st.aid DESC) t";
	$q	= $db->prepare($sql); $q->execute();
	if ($q->rowCount() > 0): $totaltime = '';
		foreach ($q->fetchAll(PDO::FETCH_ASSOC) as $row):
			$temp_type 		= $row['sim_temp_type'];
			$passing_marks	= ($row['passing_marks'] > 0) ? $row['passing_marks'] : 0;
			if ($temp_type == 2):
				$sqlans = "SELECT SUM(q.ques_val_1) AS com_score_1, SUM(q.ques_val_2) AS com_score_2, SUM(q.ques_val_3) AS com_score_3, SUM(q.ques_val_4) AS com_score_4, SUM(q.ques_val_5) AS com_score_5, SUM(q.ques_val_6) AS com_score_6, 
						   (SUM(q.ques_val_1) + SUM(q.ques_val_2) + SUM(q.ques_val_3) + SUM(q.ques_val_4) + SUM(q.ques_val_5) + SUM(q.ques_val_6)) AS totalAnsval FROM 
						   multi_score_tbl sc LEFT JOIN question_tbl q ON sc.question_id = q.question_id WHERE 
						   sc.status = 1 AND sc.scenario_id = '". $sim_id ."' AND sc.uid = '". $row['uid'] ."'";
				$sqlcom = "SELECT TRIM(c.comp_col_1) AS comp_1, TRIM(c.comp_col_2) AS comp_2, TRIM(c.comp_col_3) AS comp_3, TRIM(c.comp_col_4) AS comp_4, TRIM(c.comp_col_5) AS comp_5, TRIM(c.comp_col_6) AS comp_6, SUM(q.ques_val_1) AS score1, SUM(q.ques_val_2) AS score2, SUM(q.ques_val_3) AS score3, SUM(q.ques_val_4) AS score4, SUM(q.ques_val_5) AS score5, SUM(q.ques_val_6) AS score6 FROM 
						   multi_score_tbl sc LEFT JOIN 
						   question_tbl q ON sc.question_id = q.question_id LEFT JOIN 
						   competency_tbl c ON sc.scenario_id = c.scenario_id WHERE 
						   sc.status = 1 AND sc.scenario_id = '". $sim_id ."' AND sc.uid = '". $row['uid'] ."'";
				$sqlQSc = "SELECT SUM(ques_val_1) AS com_qes_score_1, SUM(ques_val_2) AS com_qes_score_2, SUM(ques_val_3) AS com_qes_score_3, SUM(ques_val_4) AS com_qes_score_4, SUM(ques_val_5) AS com_qes_score_5, SUM(ques_val_6) AS com_qes_score_6 FROM 
						   question_tbl WHERE scenario_id = '". $sim_id ."'";
				$getTotalTime = $db->getLastTotalTimeMulti($sim_id, $userId, $row['uid']);
			elseif ($temp_type == 7):
				$sqlans  = "SELECT SUM(ans_val1) as com_score_1, SUM(ans_val2) as com_score_2, SUM(ans_val3) as com_score_3, SUM(ans_val4) as com_score_4, SUM(ans_val5) as com_score_5, SUM(ans_val6) as com_score_6, 
							(SUM(ans_val1) + SUM(ans_val2) + SUM(ans_val3) + SUM(ans_val4) + SUM(ans_val5) + SUM(ans_val6)) AS totalAnsval FROM 
							open_res_tbl WHERE scenario_id = '". $sim_id ."' AND uid = '". $row['uid'] ."'";
				$sqlcom	 = "SELECT TRIM(c.comp_col_1) AS comp_1, TRIM(c.comp_col_2) AS comp_2, TRIM(c.comp_col_3) AS comp_3, TRIM(c.comp_col_4) AS comp_4, TRIM(c.comp_col_5) AS comp_5, TRIM(c.comp_col_6) AS comp_6, SUM(a.ans_val1) AS score1, SUM(a.ans_val2) AS score2, SUM(a.ans_val3) AS score3, SUM(a.ans_val4) AS score4, SUM(a.ans_val5) AS score5, SUM(a.ans_val6) AS score6 FROM 
							open_res_tbl a LEFT JOIN 
							competency_tbl c ON a.scenario_id = c.scenario_id WHERE 
							a.scenario_id = '". $sim_id ."' AND a.uid = '". $row['uid'] ."'";
				$sqlQSc  = "SELECT SUM(q.ques_val_1) as com_qes_score_1, SUM(q.ques_val_2) as com_qes_score_2, SUM(q.ques_val_3) as com_qes_score_3, SUM(q.ques_val_4) as com_qes_score_4, SUM(q.ques_val_5) as com_qes_score_5, SUM(q.ques_val_6) as com_qes_score_6 FROM open_res_tbl sc LEFT JOIN question_tbl q ON sc.question_id = q.question_id WHERE sc.scenario_id = '". $sim_id ."' AND sc.uid = '". $row['uid'] ."'";
				$getTotalTime = $db->getOpenResLastTotalTime($sim_id, $userId, $row['uid']);
			elseif ($temp_type == 1 || $temp_type == 8):
				$sqlans  = "SELECT SUM(a.ans_val1) AS com_score_1, SUM(a.ans_val2) AS com_score_2, SUM(a.ans_val3) AS com_score_3, SUM(a.ans_val4) AS com_score_4, SUM(a.ans_val5) AS com_score_5, SUM(a.ans_val6) AS com_score_6, 
							(SUM(a.ans_val1) + SUM(a.ans_val2) + SUM(a.ans_val3) + SUM(a.ans_val4) + SUM(a.ans_val5) + SUM(a.ans_val6)) AS totalAnsval FROM 
							score_tbl sc LEFT JOIN answer_tbl a ON sc.choice_option_id = a.answer_id WHERE 
							sc.scenario_id = '". $sim_id ."' AND sc.uid = '". $row['uid'] ."'";
				$sqlcom	 = "SELECT TRIM(c.comp_col_1) AS comp_1, TRIM(c.comp_col_2) AS comp_2, TRIM(c.comp_col_3) AS comp_3, TRIM(c.comp_col_4) AS comp_4, TRIM(c.comp_col_5) AS comp_5, TRIM(c.comp_col_6) AS comp_6, SUM(a.ans_val1) AS score1, SUM(a.ans_val2) AS score2, SUM(a.ans_val3) AS score3, SUM(a.ans_val4) AS score4, SUM(a.ans_val5) AS score5, SUM(a.ans_val6) AS score6 FROM 
							score_tbl sc LEFT JOIN 
							answer_tbl a ON sc.choice_option_id = a.answer_id LEFT JOIN 
							competency_tbl c ON sc.scenario_id = c.scenario_id WHERE 
							sc.scenario_id = '". $sim_id ."' AND sc.uid = '". $row['uid'] ."'";
				$sqlQSc  = "SELECT SUM(q.ques_val_1) AS com_qes_score_1, SUM(q.ques_val_2) AS com_qes_score_2, SUM(q.ques_val_3) AS com_qes_score_3, SUM(q.ques_val_4) AS com_qes_score_4, SUM(q.ques_val_5) AS com_qes_score_5, SUM(q.ques_val_6) AS com_qes_score_6 FROM 
							score_tbl sc LEFT JOIN 
							question_tbl q ON q.question_id = sc.question_id WHERE 
							sc.scenario_id = '". $sim_id ."' AND sc.uid = '". $row['uid'] ."'";
				$getTotalTime = $db->getLastTotalTime($sim_id, $userId, $row['uid']);
			endif;
			$ansexc  = $db->prepare($sqlans); $ansexc->execute();
			$ans 	 = $ansexc->fetch(PDO::FETCH_ASSOC);
			$ansval	 = $ans['totalAnsval'];
			$score	 = $db->get_percentage($comval, $ansval);
			$cur_learner_data[] = $score;

			/* JOB READINESS */
			$qsc    = $db->prepare($sqlQSc); $qsc->execute();
			$QscRow = $qsc->fetch(PDO::FETCH_ASSOC);
			
			$weightage_1 = ( ! empty($QscRow['com_qes_score_1']) && ! empty($ans['com_score_1'])) ? $QscRow['com_qes_score_1'] / $ans['com_score_1'] : '';
			$weightage_2 = ( ! empty($QscRow['com_qes_score_2']) && ! empty($ans['com_score_2'])) ? $QscRow['com_qes_score_2'] / $ans['com_score_2'] : '';
			$weightage_3 = ( ! empty($QscRow['com_qes_score_3']) && ! empty($ans['com_score_3'])) ? $QscRow['com_qes_score_3'] / $ans['com_score_3'] : '';
			$weightage_4 = ( ! empty($QscRow['com_qes_score_4']) && ! empty($ans['com_score_4'])) ? $QscRow['com_qes_score_4'] / $ans['com_score_4'] : '';
			$weightage_5 = ( ! empty($QscRow['com_qes_score_5']) && ! empty($ans['com_score_5'])) ? $QscRow['com_qes_score_5'] / $ans['com_score_5'] : '';
			$weightage_6 = ( ! empty($QscRow['com_qes_score_6']) && ! empty($ans['com_score_6'])) ? $QscRow['com_qes_score_6'] / $ans['com_score_6'] : '';
			
			$job_readiness = array_sum(array_filter([( ! empty($weightage_1) && ! empty($comp['comp_col_1'])) ? round($comp['weightage_1'] / $weightage_1) : 0,
													 ( ! empty($weightage_2) && ! empty($comp['comp_col_2'])) ? round($comp['weightage_2'] / $weightage_2) : 0,
													 ( ! empty($weightage_3) && ! empty($comp['comp_col_3'])) ? round($comp['weightage_3'] / $weightage_3) : 0,
													 ( ! empty($weightage_4) && ! empty($comp['comp_col_4'])) ? round($comp['weightage_4'] / $weightage_4) : 0,
													 ( ! empty($weightage_5) && ! empty($comp['comp_col_5'])) ? round($comp['weightage_5'] / $weightage_5) : 0,
													 ( ! empty($weightage_6) && ! empty($comp['comp_col_6'])) ? round($comp['weightage_6'] / $weightage_6) : 0]));
													
			$job_readiness_score = $db->get_percentage($tWeightage, $job_readiness);
			
			/* STRONGEST COMPETENCIES AND WEAKEST COMPETENCIES */
			$comexc = $db->prepare($sqlcom); $comexc->execute();
			foreach ($comexc->fetchAll(PDO::FETCH_ASSOC) as $comrow):
				/* STRONGEST COMPETENCIES */
				if ( ! empty($comrow['comp_1']) && $comp['comp_val_1'] == $comrow['score1']):
					$strongCompLable1 = $comrow['comp_1'];
					$strongCompScore1 = $db->get_percentage($comp['comp_val_1'], $comrow['score1']);
				endif;
				if ( ! empty($comrow['comp_2']) && $comp['comp_val_2'] == $comrow['score2']):
					$strongCompLable2 = $comrow['comp_2'];
					$strongCompScore2 = $db->get_percentage($comp['comp_val_2'], $comrow['score2']);
				endif;
				if ( ! empty($comrow['comp_3']) && $comp['comp_val_3'] == $comrow['score3']):
					$strongCompLable3 = $comrow['comp_3'];
					$strongCompScore3 = $db->get_percentage($comp['comp_val_3'], $comrow['score3']);
				endif;
				if ( ! empty($comrow['comp_4']) && $comp['comp_val_4'] == $comrow['score4']):
					$strongCompLable4 = $comrow['comp_4'];
					$strongCompScore4 = $db->get_percentage($comp['comp_val_4'], $comrow['score4']);
				endif;
				if ( ! empty($comrow['comp_5']) && $comp['comp_val_5'] == $comrow['score5']):
					$strongCompLable5 = $comrow['comp_5'];
					$strongCompScore5 = $db->get_percentage($comp['comp_val_5'], $comrow['score5']);
				endif;
				if ( ! empty($comrow['comp_6']) && $comp['comp_val_6'] == $comrow['score6']):
					$strongCompLable6 = $comrow['comp_6'];
					$strongCompScore6 = $db->get_percentage($comp['comp_val_6'], $comrow['score6']);
				endif;
				/* WEAKEST COMPETENCIES */
				if ( ! empty($comrow['comp_1']) && ! empty($comrow['score1']) && $comrow['score1'] < $comp['comp_val_1']):
					$weakCompLable1 = $comrow['comp_1'];
					$weakCompScore1 = $db->get_percentage($comp['comp_val_1'], $comrow['score1']);
				endif;
				if ( ! empty($comrow['comp_2']) && ! empty($comrow['score2']) && $comrow['score2'] < $comp['comp_val_2']):
					$weakCompLable2 = $comrow['comp_2'];
					$weakCompScore2 = $db->get_percentage($comp['comp_val_2'], $comrow['score2']);
				endif;
				if ( ! empty($comrow['comp_3']) && ! empty($comrow['score3']) && $comrow['score3'] < $comp['comp_val_3']):
					$weakCompLable3 = $comrow['comp_3'];
					$weakCompScore3 = $db->get_percentage($comp['comp_val_3'], $comrow['score3']);
				endif;
				if ( ! empty($comrow['comp_4']) && ! empty($comrow['score4']) && $comrow['score4'] < $comp['comp_val_4']):
					$weakCompLable4 = $comrow['comp_4'];
					$weakCompScore4 = $db->get_percentage($comp['comp_val_4'], $comrow['score4']);
				endif;
				if ( ! empty($comrow['comp_5']) && ! empty($comrow['score5']) && $comrow['score5'] < $comp['comp_val_5']):
					$weakCompLable5 = $comrow['comp_5'];
					$weakCompScore5 = $db->get_percentage($comp['comp_val_5'], $comrow['score5']);
				endif;
				if ( ! empty($comrow['comp_6']) && ! empty($comrow['score6']) && $comrow['score6'] < $comp['comp_val_6']):
					$weakCompLable6 = $comrow['comp_6'];
					$weakCompScore6 = $db->get_percentage($comp['comp_val_6'], $comrow['score6']);
				endif;
			endforeach;

			/* AVERAGE TIME TAKEN */
			$cur_learner_time[] = $getTotalTime['timeSum'];
			$timestamp = strtotime($getTotalTime['timeSum']);
			$totaltime += $timestamp;
		endforeach;
	endif;

	/* MY AVERAGE TIME TAKEN */
	$average_time = ( ! empty($cur_learner_time)) ? ($totaltime / count($cur_learner_time)) : '';
	$cur_learner_average_time = ( ! empty($average_time)) ? date('H:i:s', $average_time) : '00:00:00';
	
	/* GET ASSIGN SIM GROUPS DATA */
	$assignGroups = $db->getAssignSimGroup($sim_id);
	if (strpos($assignGroups, ',') == true):
		$lids = explode(',', $assignGroups);
		foreach ($lids as $gid):
			$sql = "SELECT GROUP_CONCAT(DISTINCT learner) AS lid, group_id, group_name FROM group_tbl WHERE learner != '' AND group_id = $gid AND FIND_IN_SET('". $userId ."', learner)";
			$q   = $db->prepare($sql); $q->execute();
			if ($q->rowCount() > 0):
				$data = $q->fetch(PDO::FETCH_ASSOC);
				if ( ! empty( $data['lid'])):
					$other_lid = $data['lid'];
					$assignGroup[] = ['value' => $data['group_id'], 'label' => $data['group_name'], 'title' => $data['group_name']];
				endif;
			endif;
		endforeach;
	else:
		$sql = "SELECT GROUP_CONCAT(DISTINCT learner) AS lid, group_id, group_name FROM group_tbl WHERE learner != '' AND group_id = $assignGroups AND FIND_IN_SET('". $userId ."', learner)";
		$q   = $db->prepare($sql); $q->execute();
		if ($q->rowCount() > 0):
			$data = $q->fetch(PDO::FETCH_ASSOC);
			if ( ! empty( $data['lid'])):
				$other_lid = $data['lid'];
				$assignGroup[] = ['value' => $data['group_id'], 'label' => $data['group_name'], 'title' => $data['group_name']];
			endif;
		endif;
	endif;
	
	if ( ! empty($other_lid)):
		/* GET LAST ATTEMPTED SIM DATA OTHER LEARNER */
		$sqlo = "SELECT t.* FROM (SELECT DISTINCT st.aid, st.uid, st.userid, s.scenario_id, s.sim_temp_type, s.Scenario_title AS sim_title, s.passing_marks FROM 
				 scenario_attempt_tbl AS st LEFT JOIN 
				 scenario_master AS s ON s.scenario_id = st.scenario_id WHERE 
				 st.scenario_id IN ($sim_id) AND 
				 st.userid != $userId AND 
				 st.userid IN ($other_lid) AND 
				 DATE(attempt_date) BETWEEN '". $sdate ."' AND '". $edate ."' AND 
				 (st.uid IN (SELECT uid FROM score_tbl) OR st.uid IN (SELECT uid FROM multi_score_tbl) OR st.uid IN (SELECT uid FROM open_res_tbl)) ORDER BY st.aid DESC) t";
		$qo	= $db->prepare($sqlo); $qo->execute();
		if ($qo->rowCount() > 0):
			$other_totaltime = '';
			foreach ($qo->fetchAll(PDO::FETCH_ASSOC) as $other_row):
				$temp_type_oth 	= $other_row['sim_temp_type'];
				$passing_marks	= ($other_row['passing_marks'] > 0) ? $other_row['passing_marks'] : 0;
				if ($temp_type_oth == 2):
					$sqlans = "SELECT (SUM(q.ques_val_1) + SUM(q.ques_val_2) + SUM(q.ques_val_3) + SUM(q.ques_val_4) + SUM(q.ques_val_5) + SUM(q.ques_val_6)) AS totalAnsval FROM 
								multi_score_tbl sc LEFT JOIN question_tbl q ON sc.question_id = q.question_id WHERE 
								sc.status = 1 AND sc.scenario_id = '". $sim_id ."' AND sc.uid = '". $other_row['uid'] ."'";
					$getTotalTimeOT = $db->getLastTotalTimeMulti($sim_id, $other_row['userid'], $other_row['uid']);
				elseif ($temp_type_oth == 7):
					$sqlans  = "SELECT (SUM(ans_val1) + SUM(ans_val2) + SUM(ans_val3) + SUM(ans_val4) + SUM(ans_val5) + SUM(ans_val6)) AS totalAnsval FROM 
								open_res_tbl WHERE scenario_id = '". $sim_id ."' AND uid = '". $other_row['uid'] ."'";
					$getTotalTimeOT = $db->getOpenResLastTotalTime($sim_id, $other_row['userid'], $other_row['uid']);
				elseif ($temp_type_oth == 1 || $temp_type_oth == 8):
					$sqlans  = "SELECT (SUM(a.ans_val1) + SUM(a.ans_val2) + SUM(a.ans_val3) + SUM(a.ans_val4) + SUM(a.ans_val5) + SUM(a.ans_val6)) AS totalAnsval FROM 
								score_tbl sc LEFT JOIN answer_tbl a ON sc.choice_option_id = a.answer_id WHERE 
								sc.scenario_id = '". $sim_id ."' AND sc.uid = '". $other_row['uid'] ."'";
					$getTotalTimeOT = $db->getLastTotalTime($sim_id, $other_row['userid'], $other_row['uid']);
				endif;
				$ansexc  = $db->prepare($sqlans); $ansexc->execute();
				$ans 	 = $ansexc->fetch(PDO::FETCH_ASSOC);
				$ansval	 = $ans['totalAnsval'];
				$score	 = $db->get_percentage($comval, $ansval);
				$other_learner_data[] = $score;
				/* MY AVERAGE TIME TAKEN */
				$other_cur_learner_time[] = $getTotalTimeOT['timeSum'];
				$other_timestamp = strtotime($getTotalTimeOT['timeSum']);
				$other_totaltime += $other_timestamp;
			endforeach;
			for ($i = 0; $i < count($other_learner_data); $i++):
				if ($i == 0) { $max2 = $other_learner_data[$i]; }
				if ($other_learner_data[$i] > $max) { $max = $other_learner_data[$i]; }
				if ($max > $other_learner_data[$i] && $other_learner_data[$i] > $max2) { $max2 = $other_learner_data[$i]; }
			endfor;
		endif;
		
		/* OTHER AVERAGE TIME TAKEN */
		$other_average_time = ( ! empty($other_cur_learner_time)) ? ($other_totaltime / count($other_cur_learner_time)) : '';
		$other_learner_average_time = ( ! empty($other_average_time)) ? date('H:i:s', $other_average_time) : '00:00:00';

		/* MY STANDING IN MY UNIT/LOCATION */
		$filter_condi = "id != $userId";

		if ( ! empty($cur_learner_unit)):
			$filter_condi .= " AND department = '". $cur_learner_unit ."'";
		endif;

		if ( ! empty($cur_learner_location)):
			$filter_condi .= " AND location = '". $cur_learner_location ."'";
		endif;

		$filter_learner = $db->GetUsersByFilter($other_lid, "id", $filter_condi);

		/* GET SIM DATA OTHER LEARNER AND SAME UNIT AND LOCATION */
		if ( ! empty($filter_learner)):
			$otherids = $db->addMultiIds($filter_learner);
			$sqlul = "SELECT t.* FROM (SELECT DISTINCT st.aid, st.uid, st.userid, s.scenario_id, s.sim_temp_type, s.Scenario_title AS sim_title, s.passing_marks FROM 
					  scenario_attempt_tbl AS st LEFT JOIN 
					  scenario_master AS s ON s.scenario_id = st.scenario_id WHERE 
					  st.scenario_id IN ($sim_id) AND 
					  st.userid != $userId AND 
					  st.userid IN ($otherids) AND 
					  DATE(attempt_date) BETWEEN '". $sdate ."' AND '". $edate ."' AND 
					  (st.uid IN (SELECT uid FROM score_tbl) OR st.uid IN (SELECT uid FROM multi_score_tbl) OR st.uid IN (SELECT uid FROM open_res_tbl)) ORDER BY st.aid DESC) t";
			$qul = $db->prepare($sqlul); $qul->execute();
			if ($qul->rowCount() > 0):
				foreach ($qul->fetchAll(PDO::FETCH_ASSOC) as $ul_row):
					$temp_type = $ul_row['sim_temp_type'];
					if ($temp_type == 2):
						$sqlans = "SELECT (SUM(q.ques_val_1) + SUM(q.ques_val_2) + SUM(q.ques_val_3) + SUM(q.ques_val_4) + SUM(q.ques_val_5) + SUM(q.ques_val_6)) AS totalAnsval FROM 
									multi_score_tbl sc LEFT JOIN question_tbl q ON sc.question_id = q.question_id WHERE 
									sc.status = 1 AND sc.scenario_id = '". $sim_id ."' AND sc.uid = '". $ul_row['uid'] ."'";
					elseif ($temp_type == 7):
						$sqlans  = "SELECT (SUM(ans_val1) + SUM(ans_val2) + SUM(ans_val3) + SUM(ans_val4) + SUM(ans_val5) + SUM(ans_val6)) AS totalAnsval FROM 
									open_res_tbl WHERE scenario_id = '". $sim_id ."' AND uid = '". $ul_row['uid'] ."'";
					elseif ($temp_type == 1 || $temp_type == 8):
						$sqlans  = "SELECT (SUM(a.ans_val1) + SUM(a.ans_val2) + SUM(a.ans_val3) + SUM(a.ans_val4) + SUM(a.ans_val5) + SUM(a.ans_val6)) AS totalAnsval FROM 
									score_tbl sc LEFT JOIN answer_tbl a ON sc.choice_option_id = a.answer_id WHERE 
									sc.scenario_id = '". $sim_id ."' AND sc.uid = '". $ul_row['uid'] ."'";
					endif;
					$ansexc  = $db->prepare($sqlans); $ansexc->execute();
					$ans 	 = $ansexc->fetch(PDO::FETCH_ASSOC);
					$ansval	 = $ans['totalAnsval'];
					$score	 = $db->get_percentage($comval, $ansval);
					$ul_learner_data[] = $score;
				endforeach;
				for ($i = 0; $i < count($ul_learner_data); $i++):
					if ($i == 0) { $ul_max2 = $ul_learner_data[$i]; }
					if ($ul_learner_data[$i] > $ul_max) { $ul_max = $ul_learner_data[$i]; }
					if ($ul_max > $ul_learner_data[$i] && $ul_learner_data[$i] > $ul_max2) { $ul_max2 = $ul_learner_data[$i]; }
				endfor;
			endif;
		endif;
	endif;
	
	/* PASS IN 1st & 2nd ATTEMPT AND FAIL */
	$last2sql = "SELECT * FROM scenario_attempt_tbl WHERE scenario_id = '". $sim_id ."' AND userid = '". $userId ."' AND DATE(attempt_date) BETWEEN '". $sdate ."' AND '". $edate ."'";
	$last2exe = $db->prepare($last2sql); $last2exe->execute();
	if ($last2exe->rowCount() > 0):
		$sn = 0;
		foreach ($last2exe->fetchAll(PDO::FETCH_ASSOC) as $last2row):
			$pass_sql = "SELECT DISTINCT st.aid, st.uid, st.userid, s.scenario_id, s.sim_temp_type, s.passing_marks FROM 
						 scenario_attempt_tbl AS st LEFT JOIN 
						 scenario_master AS s ON s.scenario_id = st.scenario_id WHERE st.uid = '". $last2row['uid'] ."' AND (st.uid IN (SELECT uid FROM score_tbl) OR st.uid IN (SELECT uid FROM multi_score_tbl) OR st.uid IN (SELECT uid FROM open_res_tbl))";
			$pass_q	= $db->prepare($pass_sql); $pass_q->execute();
			if ($pass_q->rowCount() > 0):
				foreach ($pass_q->fetchAll(PDO::FETCH_ASSOC) as $pass_row):
					$temp_type 		= $pass_row['sim_temp_type'];
					$passing_marks	= ($pass_row['passing_marks'] > 0) ? $pass_row['passing_marks'] : 0;
					if ($temp_type == 2):
						$sqlans = "SELECT (SUM(q.ques_val_1) + SUM(q.ques_val_2) + SUM(q.ques_val_3) + SUM(q.ques_val_4) + SUM(q.ques_val_5) + SUM(q.ques_val_6)) AS totalAnsval FROM 
									multi_score_tbl sc LEFT JOIN question_tbl q ON sc.question_id = q.question_id WHERE 
									sc.status = 1 AND sc.scenario_id = '". $sim_id ."' AND sc.uid = '". $row['uid'] ."'";
						$report_page = 'learner-progress-linear-multiple.php';
					elseif ($temp_type == 7):
						$sqlans  = "SELECT (SUM(ans_val1) + SUM(ans_val2) + SUM(ans_val3) + SUM(ans_val4) + SUM(ans_val5) + SUM(ans_val6)) AS totalAnsval FROM 
									open_res_tbl WHERE scenario_id = '". $sim_id ."' AND uid = '". $row['uid'] ."'";
						$report_page = 'open-response-learner-progress.php';
					elseif ($temp_type == 1 || $temp_type == 8):
						$sqlans  = "SELECT (SUM(a.ans_val1) + SUM(a.ans_val2) + SUM(a.ans_val3) + SUM(a.ans_val4) + SUM(a.ans_val5) + SUM(a.ans_val6)) AS totalAnsval FROM 
									score_tbl sc LEFT JOIN answer_tbl a ON sc.choice_option_id = a.answer_id WHERE 
									sc.scenario_id = '". $sim_id ."' AND sc.uid = '". $row['uid'] ."'";
						if ($temp_type == 1):
							$report_page = 'learner-progress.php';
						elseif ($temp_type == 8):
							$report_page = 'learner-progress-single-video.php';
						endif;
					endif;
					$ansexc  = $db->prepare($sqlans); $ansexc->execute();
					$ans 	 = $ansexc->fetch(PDO::FETCH_ASSOC);
					$ansval	 = $ans['totalAnsval'];
					$score	 = $db->get_percentage($comval, $ansval);
					if ($sn == 0 && $score > 0 && $score >= $passing_marks):
						$pass1 		= 1;
						$pass1score	= $score;
						$pass1url   = $report_page .'?report=true&sid='. md5($pass_row['scenario_id']) .'&uid='. $pass_row['uid'];
					endif;
					if ($sn == 1 && $score > 0 && $score >= $passing_marks):
						$pass2		= 1;
						$pass2score	= $score;
						$pass2url	= $report_page .'?report=true&sid='. md5($pass_row['scenario_id']) .'&uid='. $pass_row['uid'];
					endif;
					if ($score < $passing_marks):
						$fail++;
					endif;
				endforeach;
			endif; $sn++;
		endforeach;
	endif;
	
	$resdata = ['success'	=> TRUE,
				'data'		=> ['cur_top_score' 		=> ( ! empty($cur_learner_data)) ? max($cur_learner_data) : 0,
								'other_top_score1'		=> $max,
								'other_top_score2'		=> $max2,
								'other_top_ul_score1'	=> $ul_max,
								'other_top_ul_score2'	=> $ul_max2,
								'cur_average_time'		=> $cur_learner_average_time,
								'other_average_time'	=> $other_learner_average_time,
								'strongCompLable1'		=> $strongCompLable1,
								'strongCompScore1'		=> $strongCompScore1,
								'strongCompLable2'		=> $strongCompLable2,
								'strongCompScore2'		=> $strongCompScore2,
								'strongCompLable3'		=> $strongCompLable3,
								'strongCompScore3'		=> $strongCompScore3,
								'strongCompLable4'		=> $strongCompLable4,
								'strongCompScore4'		=> $strongCompScore4,
								'strongCompLable5'		=> $strongCompLable5,
								'strongCompScore5'		=> $strongCompScore5,
								'strongCompLable6'		=> $strongCompLable6,
								'strongCompScore6'		=> $strongCompScore6,
								'weakCompLable1'		=> $weakCompLable1,
								'weakCompScore1'		=> $weakCompScore1,
								'weakCompLable2'		=> $weakCompLable2,
								'weakCompScore2'		=> $weakCompScore2,
								'weakCompLable3'		=> $weakCompLable3,
								'weakCompScore3'		=> $weakCompScore3,
								'weakCompLable4'		=> $weakCompLable4,
								'weakCompScore4'		=> $weakCompScore4,
								'weakCompLable5'		=> $weakCompLable5,
								'weakCompScore5'		=> $weakCompScore5,
								'weakCompLable6'		=> $weakCompLable6,
								'weakCompScore6'		=> $weakCompScore6,
								'assignGroup'			=> $assignGroup,
								'totalAttempt'			=> $totalAttempt,
								'pass_1st'				=> $pass1,
								'pass_2nd'				=> $pass2,
								'fail'					=> $fail,
								'job_readiness'			=> $job_readiness_score,
								'pass1score'			=> $pass1score,
								'pass2score'			=> $pass2score,
								'pass1url'				=> $pass1url,
								'pass2url'				=> $pass2url,
								]
							];
	echo json_encode($resdata);
endif;

//-------------------------GET-GROUP-FILTER-DATA--------------------------
if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['groupFilterData']) && ! empty($_POST['sim_id'])):
	$date		= ( ! empty($_POST['date'])) ? explode(' - ', $_POST['date']) : exit('error');
	$sim_id		= ( ! empty($_POST['sim_id'])) ? $_POST['sim_id'] : [];
	$group_id	= ( ! empty($_POST['group_id'])) ? $_POST['group_id'] : [];
	$sdate		= $date[0];
	$edate		= $date[1];
	$learner_data = [];
	$userId = ( ! empty($_POST['userId'])) ? $_POST['userId'] : '';
	/* COMPETENCY-TOTAL-SCORE */
	$sqlcom  = "SELECT comp_col_1, comp_col_2, comp_col_3, comp_col_4, comp_col_5, comp_col_6, comp_val_1, comp_val_2, comp_val_3, comp_val_4, comp_val_5, comp_val_6, (SUM(comp_val_1) + SUM(comp_val_2) + SUM(comp_val_3) + SUM(comp_val_4) + SUM(comp_val_5) + SUM(comp_val_6)) AS totalCompval FROM competency_tbl WHERE scenario_id = '". $sim_id ."'";
	$comexc  = $db->prepare($sqlcom); $comexc->execute();
	$comp 	 = $comexc->fetch(PDO::FETCH_ASSOC);
	$comval	 = $comp['totalCompval'];
	
	$sql = "SELECT GROUP_CONCAT(DISTINCT learner) AS lid, group_id, group_name FROM group_tbl WHERE learner != '' AND group_id = $group_id AND FIND_IN_SET('". $userId ."', learner)";
	$q   = $db->prepare($sql); $q->execute();
	if ($q->rowCount() > 0):
		$data = $q->fetch(PDO::FETCH_ASSOC);
		$lid  = $data['lid'];
		if ( ! empty($lid)):
			/* GET ATTEMPTED SIM DATA */
			$sqlo = "SELECT t.* FROM (SELECT DISTINCT st.aid, st.uid, st.userid, s.scenario_id, s.sim_temp_type, s.Scenario_title AS sim_title, s.passing_marks FROM 
					 scenario_attempt_tbl AS st LEFT JOIN 
					 scenario_master AS s ON s.scenario_id = st.scenario_id WHERE 
					 st.scenario_id IN ($sim_id) AND 
					 st.userid IN ($lid) AND 
					 DATE(attempt_date) BETWEEN '". $sdate ."' AND '". $edate ."' AND 
					 (st.uid IN (SELECT uid FROM score_tbl) OR st.uid IN (SELECT uid FROM multi_score_tbl) OR st.uid IN (SELECT uid FROM open_res_tbl)) ORDER BY st.aid DESC) t";
			$qo	= $db->prepare($sqlo); $qo->execute();
			if ($qo->rowCount() > 0):
				$other_totaltime = '';
				foreach ($qo->fetchAll(PDO::FETCH_ASSOC) as $other_row):
					$temp_type_oth 	= $other_row['sim_temp_type'];
					$passing_marks	= ($other_row['passing_marks'] > 0) ? $other_row['passing_marks'] : 0;
					if ($temp_type_oth == 2):
						$sqlans = "SELECT (SUM(q.ques_val_1) + SUM(q.ques_val_2) + SUM(q.ques_val_3) + SUM(q.ques_val_4) + SUM(q.ques_val_5) + SUM(q.ques_val_6)) AS totalAnsval FROM 
									multi_score_tbl sc LEFT JOIN question_tbl q ON sc.question_id = q.question_id WHERE 
									sc.status = 1 AND sc.scenario_id = '". $sim_id ."' AND sc.uid = '". $other_row['uid'] ."'";
					elseif ($temp_type_oth == 7):
						$sqlans  = "SELECT (SUM(ans_val1) + SUM(ans_val2) + SUM(ans_val3) + SUM(ans_val4) + SUM(ans_val5) + SUM(ans_val6)) AS totalAnsval FROM 
									open_res_tbl WHERE scenario_id = '". $sim_id ."' AND uid = '". $other_row['uid'] ."'";
					elseif ($temp_type_oth == 1 || $temp_type_oth == 8):
						$sqlans  = "SELECT (SUM(a.ans_val1) + SUM(a.ans_val2) + SUM(a.ans_val3) + SUM(a.ans_val4) + SUM(a.ans_val5) + SUM(a.ans_val6)) AS totalAnsval FROM 
									score_tbl sc LEFT JOIN answer_tbl a ON sc.choice_option_id = a.answer_id WHERE 
									sc.scenario_id = '". $sim_id ."' AND sc.uid = '". $other_row['uid'] ."'";
					endif;
					$ansexc  = $db->prepare($sqlans); $ansexc->execute();
					$ans 	 = $ansexc->fetch(PDO::FETCH_ASSOC);
					$ansval	 = $ans['totalAnsval'];
					$score	 = $db->get_percentage($comval, $ansval);
					$learner_data[$other_row['userid']] = $score;
				endforeach;
				arsort($learner_data);
				$position = 0; $lscore = '';
				if ( ! empty($learner_data)): $i = 0;
					foreach ($learner_data as $key => $value):
						if ($userId == $key):
							$position = $i;
							$lscore = $value;
							break;
						endif; $i++;
					endforeach;
					$resdata = ['success'	=> TRUE,
								'data'		=> ['top_score'	=> ( ! empty($learner_data)) ? max($learner_data) : 0,
												'position'	=> ( ! empty($position)) ? $position : 0,
												'lscore'	=> ( ! empty($lscore)) ? $lscore : 0,
												'totall'	=> ( ! empty($learner_data)) ? count($learner_data) : 0,
												]
								];
					echo json_encode($resdata); exit;
				endif;
			endif;
		endif;
	endif;
endif;

//-----------------------------Get-Default-Learner-Data-------------------------
if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['getLearnerDefaultData'])):
	//	echo "getLearnerDefaultData";die;
	$date		= ( ! empty($_POST['date'])) ? explode(' - ', $_POST['date']) : exit('error');
	$sim_id		= ( ! empty($_POST['sim_id'])) ? $_POST['sim_id'] : [];
//	echo $userId;die;
//	if(!isset($userId)){
		//$userId = ( ! empty($_POST['userId'])) ? $_POST['userId'] : '';
//	}
	$sdate		= $date[0];
	$edate		= $date[1];
	$resdata	= $passing_dataPoints = $achieved_dataPoints = $totalQ_dataPoints = $totalAttemptQ_dataPoints = $number_of_attempt_dataPoints = $score_dataPoints = [];
	$comp		= $achive = $totalAttemptQ = 0; $pass_fail = '';
	/* GET LAST ATTEMPLETD SIM */
	$sql = "SELECT t.* FROM (SELECT DISTINCT st.aid, st.uid, st.userid, s.scenario_id, s.sim_temp_type, s.Scenario_title AS sim_title, s.passing_marks FROM 
			scenario_attempt_tbl AS st LEFT JOIN 
			scenario_master AS s ON s.scenario_id = st.scenario_id WHERE ".(( ! empty($sim_id)) ? "st.scenario_id IN ($sim_id) AND " : "")."
			st.userid = $userId AND DATE(attempt_date) BETWEEN '". $sdate ."' AND '". $edate ."' AND 
			(st.uid IN (SELECT uid FROM score_tbl) OR st.uid IN (SELECT uid FROM multi_score_tbl) OR st.uid IN (SELECT uid FROM open_res_tbl)) ORDER BY st.aid DESC) t GROUP BY t.scenario_id";
		//	print_r($sql);
	$q	= $db->prepare($sql); $q->execute();
	if ($q->rowCount() > 0):
		foreach ($q->fetchAll(PDO::FETCH_ASSOC) as $row):
			$type 	= $row['sim_temp_type'];
			$title	= $row['sim_title'];
			$totalQ	= $db->getTotalQuestionsByScenario(md5($row['scenario_id']));
			/* PASS FAIL GRAPH */
				/* TOTAL COMPETENCY SCORE */
				$sqlcom = "SELECT (SUM(comp_val_1) + SUM(comp_val_2) + SUM(comp_val_3) + SUM(comp_val_4) + SUM(comp_val_5) + SUM(comp_val_6)) AS totalCompval FROM 
						   competency_tbl WHERE scenario_id = '". $row['scenario_id'] . "'";
				$qcomexc = $db->prepare($sqlcom); $qcomexc->execute();
				$crow	 = $qcomexc->fetch(PDO::FETCH_ASSOC);
				$comp	 = $crow['totalCompval'];
				/* ACHIEVED SCORE */
				if ($type == 1 || $type == 8):
					$sqlachive		= "SELECT (SUM(a.ans_val1) + SUM(a.ans_val2) + SUM(a.ans_val3) + SUM(a.ans_val4) + SUM(a.ans_val5) + SUM(a.ans_val6)) AS totalAnsval FROM 
									   score_tbl sc LEFT JOIN answer_tbl a ON sc.choice_option_id = a.answer_id WHERE sc.scenario_id = '". $row['scenario_id'] ."' AND sc.uid = '". $row['uid'] ."'";
					$totalAttemptQ	= $db->getTotalAttemptQues($row['scenario_id'], $userId);
				elseif ($type == 2):
					$sqlachive		= "SELECT (SUM(q.ques_val_1) + SUM(q.ques_val_2) + SUM(q.ques_val_3) + SUM(q.ques_val_4) + SUM(q.ques_val_5) + SUM(q.ques_val_6)) AS totalAnsval FROM 
									   multi_score_tbl sc LEFT JOIN 
									   question_tbl q ON sc.question_id = q.question_id WHERE 
									   sc.status = 1 AND sc.scenario_id = '". $row['scenario_id'] ."' AND sc.uid = '". $row['uid'] ."'";
					$totalAttemptQ	= $db->getTotalAttemptQuesMulti($row['scenario_id'], $userId);
				elseif ($type == 7):
					$sqlachive  	= "SELECT (SUM(ans_val1) + SUM(ans_val2) + SUM(ans_val3) + SUM(ans_val4) + SUM(ans_val5) + SUM(ans_val6)) AS totalAnsval FROM 
										open_res_tbl WHERE scenario_id = '". $row['scenario_id'] ."' AND uid = '". $row['uid'] ."'";
					$totalAttemptQ	= $db->getTotalAttemptQuesOpenResp($row['scenario_id'], $userId);
				endif;
				if ( ! empty($sqlachive)):
					$achiveexe 		= $db->prepare($sqlachive); $achiveexe->execute();
					$arow   		= $achiveexe->fetch(PDO::FETCH_ASSOC);
					$achieve_score	= ( ! empty($arow['totalAnsval'])) ? $arow['totalAnsval'] : 0;
				else:
					$achieve_score = 0;
				endif;
				$passing_marks	= ($row['passing_marks'] > 0) ? $row['passing_marks'] : 0;
				$achieved_score	= $db->get_percentage($comp, $achieve_score);
				if ($type == 1 || $type == 8):
					if ($db->checkCriticalClassicTemp($row['scenario_id'], $row['uid']) == TRUE || $db->checkCriticalClassicTemp($row['scenario_id'], $row['uid']) == 2):
						if ($achieved_score > 0 && $achieved_score >= $passing_marks):
							$pass_fail = 'PASS';
						else:
							$pass_fail = 'FAIL';
						endif;
					else:
						$pass_fail = 'FAIL';
					endif;
				elseif ($type == 2):
					if ($db->checkCriticalMultiTemp($row['scenario_id'], $row['uid']) == TRUE || $db->checkCriticalMultiTemp($row['scenario_id'], $row['uid']) == 2):
						if ($achieved_score > 0 && $achieved_score >= $passing_marks):
							$pass_fail = 'PASS';
						else:
							$pass_fail = 'FAIL';
						endif;
					else:
						$pass_fail = 'FAIL';
					endif;
				elseif ($type == 7):
					if ($db->checkCriticalOpenRes($row['scenario_id'], $row['uid']) == TRUE || $db->checkCriticalOpenRes($row['scenario_id'], $row['uid']) == 2):
						if ($achieved_score > 0 && $achieved_score >= $passing_marks):
							$pass_fail = 'PASS';
						else:
							$pass_fail = 'FAIL';
						endif;
					else:
						$pass_fail = 'FAIL';
					endif;
				endif;
				$passing_dataPoints[] 	= ['y' => $comp, 'label' => $title];
				$achieved_dataPoints[]	= ['y' => $achieve_score, 'label' => $title .'('. $pass_fail .')'];
			
				/* COMPLETE / INCOMPLETE */
				if ($totalAttemptQ > 0 && $totalAttemptQ == $totalQ):
					$comp_incomp = 'COMPLETE';
				else:
					$comp_incomp = 'INCOMPLETE';
				endif;
				$totalQ_dataPoints[]		= ['y' => $totalQ, 'label' => $title];
				$totalAttemptQ_dataPoints[]	= ['y' => $totalAttemptQ, 'label' => $title .'('. $comp_incomp .')'];
				
				/* NUMBER OF ATTEMPT */
				$noatt_sql = "SELECT COUNT(aid) AS tas FROM 
							  scenario_attempt_tbl WHERE ".(( ! empty($sim_id)) ? "scenario_id IN ($sim_id) AND " : "scenario_id = '". $row['scenario_id'] ."' AND ")." 
							  userid = $userId AND DATE(attempt_date) BETWEEN '". $sdate ."' AND '". $edate ."' AND 
							  (uid IN (SELECT uid FROM score_tbl) OR uid IN (SELECT uid FROM multi_score_tbl) OR uid IN (SELECT uid FROM open_res_tbl))";
				 $noattq = $db->prepare($noatt_sql); $noattq->execute();
				 if ($noattq->rowCount() > 0):
					$noattdata = $noattq->fetch(PDO::FETCH_ASSOC);
					$tas = $noattdata['tas'];
				 else:
					$tas = 0;
				 endif;
				$number_of_attempt_dataPoints[]	= ['y' => $tas, 'label' => $title];
				/* SCORE */
				$score_dataPoints[]	= ['y' => $achieve_score, 'label' => $title];
		endforeach;
	endif;
	$resdata = ['success'	=> TRUE,
				'data'		=> ['passing_dataPoints'	=> $passing_dataPoints,
								'achieved_dataPoints'	=> $achieved_dataPoints,
								'totalq_dataPoints'		=> $totalQ_dataPoints,
								'attemptq_dataPoints'	=> $totalAttemptQ_dataPoints,
								'attempt_dataPoints'	=> $number_of_attempt_dataPoints,
								'score_dataPoints'		=> $score_dataPoints
								]];
	echo json_encode($resdata, JSON_NUMERIC_CHECK);
endif;

//echo "after if";die;
ob_flush();
$db->closeConnection();
unset($db);
