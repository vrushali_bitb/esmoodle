<!doctype html>
<!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]><html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--><html class="no-js" lang="en"> <!--<![endif]-->
<head>
	<title>EasySim</title>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" type="image/x-icon" href="<?php echo $db->getBaseUrl('favicon.ico'); ?>" />
    <!-- Chrome, Firefox OS and Opera -->
    <meta name="theme-color" content="#2c3545">
    <!-- Windows Phone -->
    <meta name="msapplication-navbutton-color" content="#2c3545">
    <!-- iOS Safari -->
    <meta name="apple-mobile-web-app-status-bar-style" content="#2c3545">
    <link type="text/css" rel="stylesheet" href="<?php echo $db->getBaseUrl('content/css/bootstrap.min.css'); ?>" />
    <link type="text/css" rel="stylesheet" href="<?php echo $db->getBaseUrl('content/css/bootstrap-responsive.css'); ?>" />
    <link type="text/css" rel="stylesheet" href="<?php echo $db->getBaseUrl('content/css/bootstrap-editable.css'); ?>" />
    <link type="text/css" rel="stylesheet" href="<?php echo $db->getBaseUrl('content/css/bootstrap-toggle.css'); ?>" />
    <link type="text/css" rel="stylesheet" href="<?php echo $db->getBaseUrl('content/css/font-awesome.min.css'); ?>" />
	<link type="text/css" rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link type="text/css" rel="stylesheet" href="<?php echo $db->getBaseUrl('content/css/style.css'); ?>" />
    <link type="text/css" rel="stylesheet" href="<?php echo $db->getBaseUrl('content/css/responsive.css'); ?>" />
    <link type="text/css" rel="stylesheet" href="<?php echo $db->getBaseUrl('content/css/component.css'); ?>" />
    <link type="text/css" rel="stylesheet" href="<?php echo $db->getBaseUrl('content/css/jquery.modalLink-1.0.0.css'); ?>" />
    <script type="text/javascript" src="<?php echo $db->getBaseUrl('content/js/jquery-3.3.1.min.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo $db->getBaseUrl('content/js/bootstrap.3.3.7.min.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo $db->getBaseUrl('content/js/sweetalert.min.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo $db->getBaseUrl('content/js/loadingoverlay.min.js'); ?>"></script>
    <link type="text/css" rel="stylesheet" href="<?php echo $db->getBaseUrl('content/css/owlcarousel/owl.carousel.min.css'); ?>" />
    <link type="text/css" rel="stylesheet" href="<?php echo $db->getBaseUrl('content/css/owlcarousel/owl.theme.default.css'); ?>" />
    <link type="text/css" rel="stylesheet" href="<?php echo $db->getBaseUrl('content/css/animate.css'); ?>" />
    <link type="text/css" rel="stylesheet" href="<?php echo $db->getBaseUrl('content/css/qustemplate.css'); ?>" />
    <link type="text/css" rel="stylesheet" href="<?php echo $db->getBaseUrl('content/css/questionresponsive.css'); ?>" />
    <link type="text/css" rel="stylesheet" href="<?php echo $db->getBaseUrl('content/css/draganddrop.css'); ?>" />
    <link type="text/css" rel="stylesheet" href="<?php echo $db->getBaseUrl('content/css/template-updated.css'); ?>" />
    <link type="text/css" rel="stylesheet" href="https://vjs.zencdn.net/7.11.4/video-js.css"  />
    <link type="text/css" rel="stylesheet" href="https://unpkg.com/@videojs/themes@1/dist/forest/index.css"  />
    <script src="https://vjs.zencdn.net/7.11.4/video.min.js"></script>
    <link rel="stylesheet" href="<?php echo $db->getBaseUrl('content/fancybox/jquery.fancybox.min.css'); ?>" />
    <script type="text/javascript" src="<?php echo $db->getBaseUrl('content/fancybox/jquery.fancybox.min.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo $db->getBaseUrl('content/js/jquery.simple.timer.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo $db->getBaseUrl('content/js/draganddrop.js'); ?>"></script>
</head>
<body class="">
