<?php 
#ini_set('display_errors', 1);
#error_reporting(E_ALL);
ob_start();
session_start();

/******************* Class ********************/
require_once dirname(dirname(__FILE__)) . '/config/db.class.php';
require_once dirname(dirname(__FILE__)) . '/config/upload.class.php';
require_once dirname(dirname(__FILE__)) . '/config/src/class.upload.php';
require_once dirname(dirname(__FILE__)) . '/config/securimage/securimage.php';
require_once dirname(__FILE__) . '/mailer/Send_Mail.php';

/******************* DBConnection ********************/
$db 				= new DBConnection();
$user				= (isset($_SESSION['username'])) ? $_SESSION['username'] : '';
$role				= (isset($_SESSION['role'])) ? $_SESSION['role'] : '';
$userId				= (isset($_SESSION['userId'])) ? $_SESSION['userId'] : '';
$client_id			= (isset($_SESSION['client_id'])) ? $_SESSION['client_id'] : exit('Oops. something went wrong please try again');
$domain 			= (isset($_SESSION['domain_name'])) ? $_SESSION['domain_name'] : $_SERVER['HTTP_HOST'];
$img_valid_exts		= array('jpeg', 'jpg', 'png', 'svg');
$video_valid_exts	= array('flv', 'mp4');
$audio_valid_exts	= array('mp3', 'MP3');
$curdate			= date('Y-m-d H:i:s a');

/******************* Folder Path ********************/
$char_bg_path		= dirname(dirname(__FILE__)) . '/img/char_bg/';
$type_icon_path 	= dirname(dirname(__FILE__)) . '/img/type_icon/';
$group_icon_path 	= dirname(dirname(__FILE__)) . '/img/group_icon/';
$uploadProfilePath 	= dirname(dirname(__FILE__)) . '/img/profile/';
$uploadpath 		= dirname(dirname(__FILE__)) . '/scenario/upload/'. $domain .'/';
$root_path			= $db->rootPath() . 'scenario/upload/'. $domain .'/';

/******************* Create folder if not exist ********************/
if ( ! is_dir($char_bg_path)) mkdir($char_bg_path, 0777, TRUE);
if ( ! is_dir($type_icon_path)) mkdir($type_icon_path, 0777, TRUE);
if ( ! is_dir($group_icon_path)) mkdir($group_icon_path, 0777, TRUE);
if ( ! is_dir($uploadProfilePath)) mkdir($uploadProfilePath, 0777, TRUE);
if ( ! is_dir($uploadpath)) mkdir($uploadpath, 0777, TRUE);

#-------------------------------Update-Sim-Type-----------------------------
if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['update_scenario_type_id'])):
	extract($_POST);
	$type_id = (isset($update_scenario_type_id)) ? $update_scenario_type_id : '';
	if ( ! empty($icon_edit_image_name)):
		$icon_url = $icon_edit_image_name;
		( ! empty($icon_old_image_name)) ? @unlink($type_icon_path . $icon_old_image_name) : '';
	else:
		$icon_url = $icon_old_image_name;
	endif;
	if ( empty($scenario_name)):
		$resdata = array('success' => FALSE, 'msg' => 'Please enter all required fields');
	elseif ( ! empty($scenario_name)):
		$sql = "UPDATE scenario_type SET type_name = '". $scenario_name ."', type_icon = '". $icon_url ."', type_des = '". $scenario_description ."' WHERE st_id = '". $type_id ."'";
		$results = $db->prepare($sql);
		if ($results->execute()):
			$resdata = array('success' => TRUE, 'msg' => 'Simulation type update successfully.');
		else:
			$resdata = array('success' => FALSE, 'msg' => 'Simulation type not update. try again later.');
		endif;
	endif;
	echo json_encode($resdata);
endif;

#-------------------------------Update-Sim-Status-----------------------------
if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['update_scenario_status_id'])):
	extract($_POST);
	$status_id = (isset($update_scenario_status_id)) ? $update_scenario_status_id : '';
	if (empty($status_name)):
		$resdata = array('success' => FALSE, 'msg' => 'Please enter all required fields');
	elseif ( ! empty($status_name)):
		$sql = "UPDATE scenario_status SET status_name = '". $status_name ."', status_des = '". $status_description ."' WHERE sid = '". $status_id ."'";
		$results = $db->prepare($sql);
		if ($results->execute()):
			$resdata = array('success' => TRUE, 'msg' => 'Simulation status update successfully.');
		else:
			$resdata = array('success' => FALSE, 'msg' => 'Simulation status not update. try again later.');
		endif;
	endif;
	echo json_encode($resdata);
endif;

#-------------------------------Add-Sim------------------------------------
if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['create_simulation'])):
	extract($_POST);
	$sim_permission	= $db->getClientPermission($client_id);
	$allow_sim		= ( ! empty($sim_permission['sim'])) ? $sim_permission['sim'] : '';
	$totalSim		= $db->getTotalScenario();
	$error 			= FALSE;
	if (empty($scenario_name) || empty($scenario_category) || empty($scenario_type)):
		$resdata = array('success' => FALSE, 'msg' => 'Please enter all required fields');
		$error	 = TRUE;
	elseif ( ! empty($allow_sim) && $totalSim >= $allow_sim):
		$resdata = array('success' => FALSE, 'msg' => 'Simulation creation limit full.');
		$error 	 = TRUE;
	elseif ($error == FALSE):
		try {
				$status 	= 2;
				$assignee	= NULL;
				$createdby	= $userId;
				$createdon	= $curdate;
				$modifiedby = $userId;
				$modifiedon = $curdate;
				$data 		= array('title'				=> addslashes($scenario_name),
									'stype' 			=> ( ! empty($scenario_type)) ? $db->addMultiIds($scenario_type) : '',
									'scategory' 		=> $scenario_category,
									'status' 			=> $status,
									'start_date' 		=> ( ! empty($start_date)) ? $start_date : '',
									'end_date' 			=> ( ! empty($end_date)) ? $end_date : '',
									'assign_author' 	=> ( ! empty($author)) ? $db->addMultiIds($author) : '',
									'assign_reviewer'	=> ( ! empty($reviewer)) ? $db->addMultiIds($reviewer) : '',
									'createdby' 		=> $createdby,
									'createdon' 		=> $createdon,
									'modifiedby' 		=> $modifiedby,
									'modifiedon' 		=> $modifiedon);
				$sql = "INSERT INTO `scenario_master` (`Scenario_title`, `scenario_type`, `scenario_category`, `status`, `assign_start_date`, `assign_end_date`, `assign_author`, `assign_reviewer`, `created_by`, `created_on`, `modified_by`, `modified_on`) VALUES (:title, :stype, :scategory, :status, :start_date, :end_date, :assign_author, :assign_reviewer, :createdby, :createdon, :modifiedby, :modifiedon)";
				$stmt = $db->prepare($sql);
				if ($stmt->execute($data)):
					$resdata = array('success' => TRUE, 'msg' => 'Simulation created successfully.');
				else:
					$resdata = array('success' => FALSE, 'msg' => 'Simulation not create. Please try again later');
				endif;
			}
            catch (PDOException $e) {
				$resdata = array('success' => FALSE, 'msg' => 'Simulation not create. Please try again later');
            }
	endif;
	echo json_encode($resdata);
endif;

#----------------------------Update-simulation--------------------------
if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['simulation_update']) && ! empty($_POST['sim_id'])):
	extract($_POST);
	if (empty($scenario_name) || empty($scenario_category) || empty($scenario_type)):
		$resdata = array('success' => FALSE, 'msg' => 'Please enter all required fields');
	elseif ( ! empty($scenario_name) && ! empty($scenario_category) && ! empty($scenario_type)):
		$sql = "UPDATE scenario_master SET Scenario_title = '". addslashes($scenario_name) ."', scenario_category = '". $scenario_category ."' 
			   ".((isset($scenario_type) && ! empty($scenario_type)) ? ", scenario_type = '". $db->addMultiIds($scenario_type) ."'" : " ")." 
			   ".((isset($author) && ! empty($author)) ? ", assign_author = '". $db->addMultiIds($author) ."'" : " ")." 
			   ".((isset($reviewer) && ! empty($reviewer)) ? ", assign_reviewer = '". $db->addMultiIds($reviewer) ."'" : " ").", 
			   assign_start_date = '". $start_date ."', assign_end_date = '". $end_date ."' WHERE scenario_id = '". $sim_id ."'";
		$results = $db->prepare($sql);
		if ($results->execute()):
			$resdata = array('success' => TRUE, 'msg' => 'Simulation update successfully.');
		else:
			$resdata = array('success' => FALSE, 'msg' => 'Simulation not update. try again later.');
		endif;
	endif;
	echo json_encode($resdata);
endif;

#------------------------Update-simulation-Name------------------------
if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['simulation_update_name']) && ! empty($_POST['sim_id'])):
	extract($_POST);
	if (empty($scenario_name)):
		$resdata = array('success' => FALSE, 'msg' => 'Please enter simulation name');
	elseif ( ! empty($scenario_name)):
		$sql = "UPDATE scenario_master SET Scenario_title = '". addslashes($scenario_name) ."' WHERE scenario_id = '". $sim_id ."'";
		$results = $db->prepare($sql);
		if ($results->execute()):
			$resdata = array('success' => TRUE, 'msg' => 'Simulation update successfully.');
		else:
			$resdata = array('success' => FALSE, 'msg' => 'Simulation not update. try again later.');
		endif;
	endif;
	echo json_encode($resdata);
endif;

#-------------------------------Add Email-Template------------------------
if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['add_email_tmp'])):
	extract($_POST);
	$url = '../email-management.php';
	if ( empty($email_subject) || empty($email_type) || empty($tmp_des)):
		$_SESSION['msg'] = 'Please enter all required fields';
		header('location:'. $url);
	elseif ( ! empty($email_subject) && ! empty($email_type) && ! empty($tmp_des)):
		try {
			$data = array('subject'	=> addslashes($email_subject),
						  'type'	=> $email_type,
						  'tmp_des'	=> $tmp_des,
						  'comment'	=> $comment,
						  'user_id' => $userId,
						  'status' 	=> $status,
						  'edate'	=> $curdate);
				$sql = "INSERT INTO `email_template_tbl` (`subject`, `type`, `tmp_des`, `comment`, `user_id`, `status`, `edate`) VALUES (:subject, :type, :tmp_des, :comment, :user_id, :status, :edate)";
				$stmt = $db->prepare($sql);
				if ($stmt->execute($data)):
					$_SESSION['msg'] = 'Template create successfully.';					
				else:
					$_SESSION['msg'] = 'Template not create. Please try again later.';
				endif;
				header('location:'. $url);
            }
			catch (PDOException $e) {
				echo 'There is some problem in connection: "'.$e->getMessage().'"';
				exit;
			}
	endif;
endif;

#-------------------------------Update Email-Template--------------------------
if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['update_email_tmp'])):
	extract($_POST);
	if ( empty($email_subject) || empty($email_type) || empty($tmp_des)):
		$_SESSION['msg'] = 'Please enter all required fields';
		header('location: ../edit-email-template.php?edit=true&eid='. md5($email_tmp_id));
	elseif ( ! empty($email_subject) && ! empty($email_type) && ! empty($tmp_des)):
		try {
			$data = array('subject'	=> $email_subject,
						  'type'	=> $email_type,
						  'tmp_des'	=> $tmp_des,
						  'comment'	=> $comment,
						  'user_id' => $userId,
						  'status' 	=> $status,
						  'edate' 	=> $curdate,
						  'eid' 	=> $email_tmp_id);
			$sql = "UPDATE email_template_tbl SET subject=:subject, type=:type, tmp_des=:tmp_des, comment=:comment, user_id=:user_id, status=:status, edate=:edate WHERE eid=:eid";
			$stmt = $db->prepare($sql);
			if ($stmt->execute($data)):
				$_SESSION['msg'] = 'Template update successfully.';
				$url = '../email-management.php';
			else:
				$_SESSION['msg'] = 'Template not update. Please try again later.';
				$url = '../edit-email-template.php?edit=true&eid='. md5($email_tmp_id);
			endif;
			header('location:'. $url);
		}
		catch (PDOException $e) {
			echo 'There is some problem in connection: "'.$e->getMessage().'"';
			exit;
		}
	endif;
endif;

#-------------------------------------------Update Group----------------------
if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['update_group_id'])):
	extract($_POST);
	$group_id = (isset($update_group_id)) ? $update_group_id : '';
	if ( ! empty($group_edit_image_name)):
		$icon_url = $group_edit_image_name;
		( ! empty($group_old_image_name)) ? @unlink($group_icon_path . $group_old_image_name) : '';
	else:
		$icon_url = $group_old_image_name;
	endif;
	if ( empty($group_name) && empty($group_leaders)):
		$resdata = array('success' => FALSE, 'msg' => 'Please enter all required fields');
	elseif ( ! empty($group_name) ):
		$sql = "UPDATE group_tbl SET group_name = '". addslashes($group_name) ."', group_icon = '". $icon_url ."', group_leader = '". $group_leaders ."', group_des = '". $group_description ."' WHERE group_id = '". $group_id ."'";
		$results = $db->prepare($sql);
		if ($results->execute()):
			$resdata = array('success' => TRUE, 'msg' => 'Group update successfully.');
		else:
			$resdata = array('success' => FALSE, 'msg' => 'Group not update. try again later.');
		endif;
	endif;
	echo json_encode($resdata);
endif;

#-------------------------------------------Add User in Group------------------------------------------
if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['add_user_group']) && isset($_POST['group_id'])):
	extract($_POST);
	if (empty($group_id)):
		$resdata = array('success' => FALSE, 'msg' => 'Please select users.');
	elseif ( ! empty($group_id)):
		$sqlL = "UPDATE group_tbl SET ".((isset($learner) && ! empty($learner)) ? " learner = '". $db->addMultiIds($learner) ."'" : " learner = '' ")." WHERE group_id = '". $group_id ."'";
		$lresultL = $db->prepare($sqlL);  $lresultL->execute();
		
		$sqlA = "UPDATE group_tbl SET ".((isset($author) && ! empty($author)) ? " author = '". $db->addMultiIds($author) ."'" : " author = '' ")." WHERE group_id = '". $group_id ."'";
		$lresultA = $db->prepare($sqlA);  $lresultA->execute();
		
		$sqlR = "UPDATE group_tbl SET ".((isset($reviewer) && ! empty($reviewer)) ? " reviewer = '". $db->addMultiIds($reviewer) ."'" : " reviewer = ''")." WHERE group_id = '". $group_id ."'";
		$resultsR = $db->prepare($sqlR);
		if ($resultsR->execute()):
			$resdata = array('success' => TRUE, 'msg' => 'User added in this group successfully.');
		else:
			$resdata = array('success' => FALSE, 'msg' => 'User not added. try again later.');
		endif;
	endif;
	echo json_encode($resdata);
endif;

#------------------------------------Add Sim Category------------------------
if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['create_category'])):
	extract($_POST);
	$cat_permission	= $db->getClientPermission($client_id);
	$allow_cat		= ( ! empty($cat_permission['category'])) ? $cat_permission['category'] : '';
	$totalCategory	= $db->getTotalCategory();
	$error 			= FALSE;
	if (empty($category)):
		$resdata = array('success' => FALSE, 'msg' => 'Please enter category name.');
		$error = TRUE;
	elseif ( ! empty($category) && $db->categoryExist($category) == TRUE):
		$resdata = array('success' => FALSE, 'msg' => 'Category name already exists.');
		$error = TRUE;
	elseif ( ! empty($allow_cat) && $totalCategory >= $allow_cat):
		$resdata = array('success' => FALSE, 'msg' => 'Category creation limit full.');
		$error = TRUE;
	elseif ($error == FALSE):
		$data = array('category'	=> addslashes($category),
					  'status'		=> 1,
					  'uid'			=> $userId,
					  'datetime'	=> $curdate);
		$sql  = "INSERT INTO `category_tbl` (`category`, `status`, `uid`, `datetime`) VALUES (:category, :status, :uid, :datetime)";
		$stmt = $db->prepare($sql);
		if ($stmt->execute($data)):
			$resdata = array('success' => TRUE, 'msg' => 'Category added successfully.');
		else:
			$resdata = array('success' => TRUE, 'msg' => 'Category not added. Please try again later.');
		endif;
	endif;
	echo json_encode($resdata);
endif;

#------------------------------------Update-Sim-Category------------------------
if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['category_update']) && ! empty($_POST['cat_id'])):
	extract($_POST);
	$error = FALSE;
	if (empty($category)):
		$resdata = array('success' => FALSE, 'msg' => 'Please enter category name.');
		$error = TRUE;
	elseif ( ! empty($category) && ! empty($old_cat) && $old_cat != $category && $db->categoryExist($category) == TRUE):
		$resdata = array('success' => FALSE, 'msg' => 'Category name already exists.');
		$error = TRUE;
	elseif ($error == FALSE && ! empty($category)):
		$sql = "UPDATE category_tbl SET category = '". $category ."' WHERE cat_id = '". $cat_id ."'";
		$stmt = $db->prepare($sql);
		if ($stmt->execute()):
			$resdata = array('success' => TRUE, 'msg' => 'Category update successfully.');
		else:
			$resdata = array('success' => TRUE, 'msg' => 'Category not update. Please try again later.');
		endif;
	endif;
	echo json_encode($resdata);
endif;

#------------------------------------Delete-Sim-Category------------------------
if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['delete_cat']) && ! empty($_POST['cat_id'])):
	extract($_POST);
	$q = $db->prepare("DELETE FROM category_tbl WHERE cat_id = ?");
	if ($q->execute(array($cat_id))):
		$resdata = array('success' => TRUE, 'msg' => 'Category deleted successfully.');
	else:
		$resdata = array('success' => TRUE, 'msg' => 'Category not deleted. Please try again later.');
	endif;
	echo json_encode($resdata);
endif;

#------------------------------------Selected Sim Assign To Learner--------------
if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['simulation_assignment'])):
	extract($_POST);
	if (empty($category) || empty($scenario[0])):
		$resdata = array('success' => FALSE, 'msg' => 'Please enter all required fields.');
	elseif ( ! empty($category) && ! empty($scenario[0])):
		$i = 0;
		$group = array_values($group);
		foreach ($group as $gdata):
			$data = array('sid'		=> $db->addMultiIds($scenario),
						  'gid'		=> $group[$i],
						  'lid'		=> (isset($learner[$i])) ? $db->addMultiIds($learner[$i]) : '',
						  'sdate'	=> $start_date[$i],
						  'edate'	=> $end_date[$i],
						  'no_att'	=> $attempt_no[$i],
						  'uid'		=> $userId,
						  'date'	=> $curdate);
			$sql = "INSERT INTO `assignment_tbl` (`scenario_id`, `group_id`, `learner_id`, `start_date`, `end_date`, `no_attempts`, `user_id`, `add_date`) 
					VALUES (:sid, :gid, :lid, :sdate, :edate, :no_att, :uid, :date)";
			$stmt = $db->prepare($sql);
			$res = $stmt->execute($data);
		$i++;
		endforeach;
		if ($res):
			$resdata = array('success' => TRUE, 'msg' => 'Simulation assigned successfully.');
		else:
			$resdata = array('success' => FALSE, 'msg' => 'Simulation not assigned. try again later.');
		endif;
	endif;
	echo json_encode($resdata);
endif;

#---------------------------------------------Update Sim ------------------------
if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['update_simulation'])):
	extract($_POST);
	if (empty($update_category) || empty($update_scenario)):
		$resdata = array('success' => FALSE, 'msg' => 'Please enter all required fields.');
	elseif ( ! empty($update_category) && ! empty($update_scenario)):
		$i = 0;
		$group = array_values($update_group);
		foreach ($group as $gdata):
			$data = array('sid'		=> $update_scenario,
						  'gid'		=> $update_group[$i],
						  'lid'		=> (isset($update_learner[$i])) ? $db->addMultiIds($update_learner[$i]) : '',
						  'sdate'	=> $update_start_date[$i],
						  'edate'	=> $update_end_date[$i],
						  'no_att'	=> $update_attempt_no[$i],
						  'uid'		=> $userId,
						  'date'	=> $curdate);
			$sql = "INSERT INTO `assignment_tbl` (`scenario_id`, `group_id`, `learner_id`, `start_date`, `end_date`, `no_attempts`, `user_id`, `add_date`) 
					VALUES (:sid, :gid, :lid, :sdate, :edate, :no_att, :uid, :date)";
			$stmt = $db->prepare($sql);
			$res = $stmt->execute($data);
		$i++;
		endforeach;
		if ($res):
			$getuid = $db->addMultiIds($update_assignment_id);
			$delete_sql = "DELETE FROM assignment_tbl WHERE assignment_id IN (". $getuid .")";
			$q = $db->prepare($delete_sql); $q->execute();
			$resdata = array('success' => TRUE, 'msg' => 'Simulation updated successfully.');
		else:
			$resdata = array('success' => FALSE, 'msg' => 'Simulation not updated. try again later.');
		endif;
	endif;
	echo json_encode($resdata);
endif;

#---------------------------------------------Add Selected Sim Template------------------------------
if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['create_sim_template']) && isset($_POST['sim_id'])):
	extract($_POST);
	$error	= FALSE;
	$url	= $get_sim_tmp = '';
	if ( ! empty($sim_ques_type) && $sim_ques_type == 3):
		$get_sim_tmp = ( ! empty($open_sim_tmp)) ? $open_sim_tmp : '';
	elseif ( ! empty($sim_ques_type) && $sim_ques_type == 4):
		$get_sim_tmp = ( ! empty($single_vsim_tmp)) ? $single_vsim_tmp : '';
	else:
		$get_sim_tmp = ( ! empty($sim_tmp)) ? $sim_tmp : '';
	endif;
	if (empty($sim_title)):
		$resdata = array('success' => FALSE, 'msg' => 'Please enter simulation name.');
		$error = TRUE;
	elseif (empty($sim_type) || empty($sim_id) || empty($sim_ques_type) || empty($get_sim_tmp)):
		$resdata = array('success' => FALSE, 'msg' => 'Please select at-least one template.');
		$error = TRUE;
	elseif ($error == FALSE):
		$sql = "UPDATE scenario_master SET Scenario_title = '". $sim_title ."', author_scenario_type = '". $sim_type ."', sim_ques_type = '". $sim_ques_type ."', sim_temp_type = '". $get_sim_tmp ."', sim_cover_img = '". $scenario_cover_file ."' WHERE scenario_id = '". $sim_id ."'";
		$results = $db->prepare($sql);
		if ($results->execute()):
			if ($sim_ques_type == 1):
				if ($get_sim_tmp == 1):
					$url = 'linear-classic-template.php';
				elseif ($get_sim_tmp == 2):
					$url = 'linear-multiple-template.php';
				endif;
			elseif ($sim_ques_type == 2):
				if ($get_sim_tmp == 4):
					$url = 'branching-classic-template.php';
				elseif ($get_sim_tmp == 5):
					$url = 'branching-multiple-template.php';
				endif;
			elseif ($sim_ques_type == 3):
				if ($get_sim_tmp == 7):
					$url = 'open-response-template.php';
				endif;
			elseif ($sim_ques_type == 4):
				if ($get_sim_tmp == 8):
					$url = 'single-video-template.php';
				endif;
			elseif ($sim_ques_type == 5):
				if ($get_sim_tmp == 6):
					$url = 'linear-classic-video-template.php';
				elseif ($get_sim_tmp == 9):
					$url = 'linear-video-multiple-template.php';
				endif;
			elseif ($sim_ques_type == 6):
				if ($get_sim_tmp == 10):
					$url = 'branching-video-classic-template.php';
				elseif ($get_sim_tmp == 11):
					$url = 'branching-video-multiple-template.php';
				endif;
			endif;
			if ($get_sim_tmp != 3 && $get_sim_tmp != 8 && $get_sim_tmp != 9 && $get_sim_tmp != 6 && $get_sim_tmp != 10 && $get_sim_tmp != 11):
				$quesData	= array('sid'		=> $sim_id,
									'questions'	=> 'Question 1',
									'status' 	=> 1,
									'user_id'	=> $userId,
									'cur_date'	=> $curdate);
				$ques_sql	= "INSERT INTO `question_tbl` (`scenario_id`, `questions`, `status`, `uid`, `cur_date`) VALUES (:sid, :questions, :status, :user_id, :cur_date)";
				$ques_stmt	= $db->prepare($ques_sql); $ques_stmt->execute($quesData);
				$ques_id  	= $db->lastInsertId();
				$resdata	= array('success' =>  TRUE, 'msg' => 'Simulation template selected successfully.', 'return_url' => $url . '?add_data=true&sim_id='. md5($sim_id) .'&temp_type='. base64_encode($sim_ques_type));
			else:
				$resdata = array('success' => TRUE, 'msg' => 'Simulation template selected successfully.', 'return_url' => $url . '?add_data=true&sim_id='. md5($sim_id) .'&temp_type='. base64_encode($sim_ques_type));
			endif;
		else:
			$resdata = array('success' => FALSE, 'msg' => 'Simulation template not selected. try again later.');
		endif;
	endif;
	echo json_encode($resdata);
endif;

#---------------------------------------------Update Selected Sim Template------------------------------
if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['edit_sim_template']) && isset($_POST['sim_id'])):
	extract($_POST);
	if (empty($sim_type) || empty($sim_id) || empty($sim_ques_type)):
		$resdata = array('success' => FALSE, 'msg' => 'Please select at-least one template.');
	elseif ( ! empty($sim_type) && ! empty($sim_id)):
		$sql = "UPDATE scenario_master SET author_scenario_type = '". $sim_type ."', sim_ques_type = '". $sim_ques_type ."' WHERE scenario_id = '". $sim_id ."'";
		$results = $db->prepare($sql);
		if ($results->execute()):
			$resdata = array('success' => TRUE, 'msg' => 'Simulation template updated successfully.', 'return_url' => 'update-simulation.php?editsim=true&sim_id='. md5($sim_id).'&temp_type='.base64_encode($sim_ques_type));
		else:
			$resdata = array('success' => FALSE, 'msg' => 'Simulation template not updated. try again later.');
		endif;
	endif;
	echo json_encode($resdata);
endif;

#-----------------------------------------Add-Update-Linear-Milti-Sim ----------------------------------
if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['update_linear_sim']) && isset($_POST['sim_id'])):
	extract($_POST);
	$error = FALSE;
	if (empty($sim_page_name[0]) || empty($sim_duration) || empty($sim_title) || empty($passing_marks)):
		$resdata = array('success' => FALSE, 'msg' => 'Please fill all the mandatory fields on add Simulation and Simulation details to proceed.');
		$error = TRUE;
		echo json_encode($resdata); exit;
	elseif (isset($competency_name_1) && empty($competency_name_1) || empty($competency_score_1) || empty($weightage_1)):
		$resdata = array('success' => FALSE, 'msg' => 'Please fill all the mandatory fields on Simulation details to proceed.');
		$error = TRUE;
		echo json_encode($resdata); exit;
	elseif ($error == FALSE):
		$sql = "UPDATE scenario_master SET `Scenario_title` = '". $sim_title ."', `duration` = '". $sim_duration ."', `passing_marks` = '". $passing_marks ."', `sim_cover_img` = '". $scenario_cover_file ."'";
		if (isset($character_type) && $character_type == 1):
			$sql .= ", sim_char_img = '', sim_char_left_img = '', sim_char_right_img = '', char_own_choice = ''";
		elseif (isset($charoption) && $charoption == 1 && isset($scenario_char_file) && ! empty($scenario_char_file)):
			$sql .= ", char_own_choice = '". $scenario_char_file ."', sim_char_img = '', sim_char_left_img = '', sim_char_right_img = ''";
		elseif (isset($charoption) && $charoption == 2 && isset($sim_char_img) && ! empty($sim_char_img)):
			$sql .= ", sim_char_img = '". $sim_char_img ."', sim_char_left_img = '', sim_char_right_img = ''";
		elseif (isset($sim_two_char_img) && count($sim_two_char_img) > 0):
			if (isset($sim_two_char_img[0])):
				$r = explode('|', $sim_two_char_img[0]);
				if (isset($r[1]) && $r[1] == 'R'):
					$sql .= ", sim_char_img = '', char_own_choice = '', sim_char_right_img = '". $r[0] ."'";
				elseif (isset($r[1]) && $r[1] == 'L'):
					$sql .= ", sim_char_img = '', char_own_choice = '', sim_char_left_img = '". $r[0] ."'";
				endif;
			endif;
			if (isset($sim_two_char_img[1])):
				$l = explode('|', $sim_two_char_img[1]);
				if (isset($l[1]) && $l[1] == 'L'):
					$sql .= ", sim_char_img = '', char_own_choice = '', sim_char_left_img = '". $l[0] ."'";
				elseif (isset($l[1]) && $l[1] == 'R'):
					$sql .= ", sim_char_img = '', char_own_choice = '', sim_char_right_img = '". $l[0] ."'";
				endif;
			endif;
		endif;
		#--Add-Web-Object--
		if ( ! empty($web_object)):
			$sql .= ", web_object = '". $web_object ."', web_object_title = '". htmlspecialchars($web_object_title) ."'";
		endif;
		#--Sim-Media-File--
		if ( ! empty($scenario_media_file) && in_array(strtolower(pathinfo($scenario_media_file, PATHINFO_EXTENSION)), $img_valid_exts)):
			$sql .= ", image_url = '". $scenario_media_file ."', video_url = NULL, audio_url = NULL";
		elseif ( ! empty($scenario_media_file) && in_array(strtolower(pathinfo($scenario_media_file, PATHINFO_EXTENSION)), $video_valid_exts)):
			$sql .= ", video_url = '". $scenario_media_file ."', image_url = NULL, audio_url = NULL";
		elseif ( ! empty($scenario_media_file) && in_array(strtolower(pathinfo($scenario_media_file, PATHINFO_EXTENSION)), $audio_valid_exts)):
			$sql .= ", audio_url = '". $scenario_media_file ."', video_url = NULL, image_url = NULL";
		elseif ( ! empty($scenario_media_file) && ! in_array(strtolower(pathinfo($scenario_media_file, PATHINFO_EXTENSION)), $audio_valid_exts) && 
				! in_array(strtolower(pathinfo($scenario_media_file, PATHINFO_EXTENSION)), $img_valid_exts) && 
				! in_array(strtolower(pathinfo($scenario_media_file, PATHINFO_EXTENSION)), $video_valid_exts)):
			$sql .= ", web_object_file = '". $scenario_media_file ."', video_url = NULL, image_url = NULL, audio_url = NULL";
		endif;
		#--Sim-Backgrounds--
		if (isset($bgoption) && $bgoption == 1 && ! empty($bg_color)):
			$sql .= ", bg_color = '". $bg_color ."', bg_own_choice = '', sim_back_img = ''";
		elseif (isset($bgoption) && $bgoption == 2 && ! empty($scenario_bg_file)):
			$sql .= ", bg_own_choice = '". $scenario_bg_file ."', bg_color = '', sim_back_img = ''";
		elseif (isset($bgoption) && $bgoption == 3 && ! empty($sim_background_img)):
			$sql .= ", sim_back_img = '". $sim_background_img ."', bg_own_choice = '', bg_color = ''";
		endif;
		$sql .= " WHERE scenario_id = '". $sim_id ."'";
		$results = $db->prepare($sql);
		if ($results->execute()):
			#----Competency----
			$delCom	 = "DELETE FROM competency_tbl WHERE scenario_id = '". $sim_id ."'";
			$delComq = $db->prepare($delCom); $delComq->execute();
			$data	 = array('scenario_id'	=> $sim_id,
							 'comp_col_1'	=> (isset($competency_name_1) && ! empty($competency_name_1)) ? addslashes($competency_name_1) : '',
							 'comp_val_1'	=> (isset($competency_score_1) && ! empty($competency_score_1)) ? addslashes($competency_score_1) : '',
							 'comp_col_2'	=> (isset($competency_name_2) && ! empty($competency_name_2)) ? addslashes($competency_name_2) : '',
							 'comp_val_2'	=> (isset($competency_score_2) && ! empty($competency_score_2)) ? $competency_score_2 : '',
							 'comp_col_3'	=> (isset($competency_name_3) && ! empty($competency_name_3)) ? addslashes($competency_name_3) : '',
							 'comp_val_3'	=> (isset($competency_score_3) && ! empty($competency_score_3)) ? $competency_score_3 : '',
							 'comp_col_4'	=> (isset($competency_name_4) && ! empty($competency_name_4)) ? addslashes($competency_name_4) : '',
							 'comp_val_4'	=> (isset($competency_score_4) && ! empty($competency_score_4)) ? $competency_score_4 : '',
							 'comp_col_5'	=> (isset($competency_name_5) && ! empty($competency_name_5)) ? addslashes($competency_name_5) : '',
							 'comp_val_5'	=> (isset($competency_score_5) && ! empty($competency_score_5)) ? $competency_score_5 : '',
							 'comp_col_6'	=> (isset($competency_name_6) && ! empty($competency_name_6)) ? addslashes($competency_name_6) : '',
							 'comp_val_6'	=> (isset($competency_score_6) && ! empty($competency_score_6)) ? $competency_score_6 : '',
							 'weightage_1'	=> (isset($weightage_1) && ! empty($weightage_1)) ? $weightage_1 : '',
							 'weightage_2'	=> (isset($weightage_2) && ! empty($weightage_2)) ? $weightage_2 : '',
							 'weightage_3'	=> (isset($weightage_3) && ! empty($weightage_3)) ? $weightage_3 : '',
							 'weightage_4'	=> (isset($weightage_4) && ! empty($weightage_4)) ? $weightage_4 : '',
							 'weightage_5'	=> (isset($weightage_5) && ! empty($weightage_5)) ? $weightage_5 : '',
							 'weightage_6'	=> (isset($weightage_6) && ! empty($weightage_6)) ? $weightage_6 : '',
							 'status' 		=> 1,
							 'user_id' 		=> $userId,
							 'cur_date'		=> $curdate);
			$com_sql  = "INSERT INTO `competency_tbl` (`scenario_id`, `comp_col_1`, `comp_val_1`, `comp_col_2`, `comp_val_2`, `comp_col_3`, `comp_val_3`, `comp_col_4`, `comp_val_4`, `comp_col_5`, `comp_val_5`, `comp_col_6`, `comp_val_6`, `weightage_1`, `weightage_2`, `weightage_3`, `weightage_4`, `weightage_5`, `weightage_6`, `status`, `uid`, `cur_date`) VALUES (:scenario_id, :comp_col_1, :comp_val_1, :comp_col_2, :comp_val_2, :comp_col_3, :comp_val_3, :comp_col_4, :comp_val_4, :comp_col_5, :comp_val_5, :comp_col_6, :comp_val_6, :weightage_1, :weightage_2, :weightage_3, :weightage_4, :weightage_5, :weightage_6, :status, :user_id, :cur_date)";
			$com_stmt = $db->prepare($com_sql);
			$com_stmt->execute($data);
			
			#----PAGES----
			$delPage  = "DELETE FROM sim_pages WHERE scenario_id = '". $sim_id ."'";
			$delPageq = $db->prepare($delPage); $delPageq->execute();
			if ( ! empty($sim_page_name)): $sp = 0;
				foreach ($sim_page_name as $pdata):
					$pageq 		= "INSERT INTO `sim_pages` (`scenario_id`, `sim_page_name`, `sim_page_desc`, `userid`, `status`, `added_date`) VALUES (:sid, :pname, :pdesc, :user_id, :status, :adate)";
					$page_stmt	= $db->prepare($pageq);
					$page_stmt->execute(array('sid'		=> $sim_id,
											  'pname'	=> ( ! empty($pdata)) ? htmlspecialchars($pdata) : '',
											  'pdesc'	=> ( ! empty($sim_page_desc[$sp])) ? $sim_page_desc[$sp] : '',
											  'status' 	=> 1,
											  'user_id'	=> $userId,
											  'adate'	=> $curdate));
				$sp++; endforeach;
			endif;
			
			#----Branding----
			$bandq	  	= "DELETE FROM sim_branding_tbl WHERE scenario_id = '". $sim_id ."'";
			$bandqexe 	= $db->prepare($bandq); $bandqexe->execute();
			$brand_data = array('sid'				=> $sim_id,
								'bg'				=> ( ! empty($ques_bg_color)) ? $ques_bg_color : '',
								'bg_tr'				=> ( ! empty($ques_bg_transp)) ? $ques_bg_transp : '',
								'obg'				=> ( ! empty($ques_option_bg_color)) ? $ques_option_bg_color : '',
								'obg_tr'			=> ( ! empty($ques_option_bg_transp)) ? $ques_option_bg_transp : '',
								'ohover'			=> ( ! empty($ques_option_hover_color)) ? $ques_option_hover_color : '',
								'oh_tr'				=> ( ! empty($ques_option_hover_transp)) ? $ques_option_hover_transp : '',
								'oselected'			=> ( ! empty($ques_option_select_color)) ? $ques_option_select_color : '',
								'oselected_tr'		=> ( ! empty($ques_option_select_transp)) ? $ques_option_select_transp : '',
								'btn_bg'			=> ( ! empty($btn_bg_color)) ? $btn_bg_color : '',
								'btn_bg_tr'			=> ( ! empty($btn_bg_transp)) ? $btn_bg_transp : '',
								'btn_hover'			=> ( ! empty($btn_hover_color)) ? $btn_hover_color : '',
								'btn_hover_tr'		=> ( ! empty($btn_hover_transp)) ? $btn_hover_transp : '',
								'btn_selected'		=> ( ! empty($btn_select_color)) ? $btn_select_color : '',
								'btn_selected_tr'	=> ( ! empty($btn_select_transp)) ? $btn_select_transp : '',
								'font_type'			=> ( ! empty($font_type)) ? $font_type : '',
								'font_color'		=> ( ! empty($font_color)) ? $font_color : '',
								'font_size'			=> ( ! empty($font_size)) ? $font_size : '',
								'user_id' 			=> $userId,
								'added_date'		=> $curdate);
			$com_sql  = "INSERT INTO `sim_branding_tbl` (`scenario_id`, `ques_bg`, `ques_bg_transparency`, `option_bg`, `option_bg_transparency`, `option_hover`, `option_hover_transparency`, `option_selected`, `option_selected_transparency`, `btn_bg`, `btn_bg_transparency`, `btn_hover`, `btn_hover_transparency`, `btn_selected`, `btn_selected_transparency`, `font_type`, `font_color`, `font_size`, `user_id`, `added_date`) 
						 VALUES (:sid, :bg, :bg_tr, :obg, :obg_tr, :ohover, :oh_tr, :oselected, :oselected_tr, :btn_bg, :btn_bg_tr, :btn_hover, :btn_hover_tr, :btn_selected, :btn_selected_tr, :font_type, :font_color, :font_size, :user_id, :added_date)";
			$com_stmt = $db->prepare($com_sql); $com_stmt->execute($brand_data);
		endif;
		//------START-MATCH-THE-FOLLOWING-----
		if ($question_type == 1):
			#-------QUESTIONS-DATA---
			$qaudio = '';
			if ( ! empty($mtfq_qaudio)):
				$qaudio = $mtfq_qaudio;
			elseif ( ! empty($mtfq_rec_audio)):
				$qaudio = $mtfq_rec_audio;
			endif;
			
			$audio = '';
			if ( ! empty($mtf_qaudio)):
				$audio = $mtf_qaudio;
			elseif ( ! empty($mtf_rec_audio)):
				$audio = $mtf_rec_audio;
			endif;
			
			$video = '';
			if ( ! empty($mtf_video)):
				$video = $mtf_video;
			elseif ( ! empty($mtf_rec_video)):
				$video = $mtf_rec_video;
			endif;
			
			#------Correct-FEEDBACK-DATA---
			$feed_audio = '';
			if ( ! empty($mtf_feedback_audio)):
				$feed_audio = $mtf_feedback_audio;
			elseif ( ! empty($mtf_feedback_rec_audio)):
				$feed_audio = $mtf_feedback_rec_audio;
			endif;
			
			$feed_video = '';
			if ( ! empty($mtf_feedback_video)):
				$feed_video = $mtf_feedback_video;
			elseif ( ! empty($mtf_feedback_rec_video)):
				$feed_video = $mtf_feedback_rec_video;
			endif;
			
			#------Incorrect-FEEDBACK-DATA---
			$ifeed_audio = '';
			if ( ! empty($mtfi_feedback_audio)):
				$ifeed_audio = $mtfi_feedback_audio;
			elseif ( ! empty($mtfi_feedback_rec_audio)):
				$ifeed_audio = $mtfi_feedback_rec_audio;
			endif;
			
			$ifeed_video = '';
			if ( ! empty($mtfi_feedback_video)):
				$ifeed_video = $mtfi_feedback_video;
			elseif ( ! empty($mtfi_feedback_rec_video)):
				$ifeed_video = $mtfi_feedback_rec_video;
			endif;
			
			$quesData = array('questions'		=> ( ! empty($mtfq)) ? htmlspecialchars($mtfq) : '',
							  'qtype'			=> ( ! empty($question_type)) ? $question_type : '',
							  'qaudio'			=> ( ! empty($qaudio)) ? $qaudio : '',
							  'qspeech_text'	=> ( ! empty($mtfqq_text_to_speech)) ? htmlspecialchars($mtfqq_text_to_speech) : '',
							  'speech_text'		=> ( ! empty($mtfq_text_to_speech)) ? htmlspecialchars($mtfq_text_to_speech) : '',
							  'audio'			=> ( ! empty($audio)) ? $audio : '',
							  'video'			=> ( ! empty($video)) ? $video : '',
							  'screen'			=> ( ! empty($mtf_rec_screen)) ? $mtf_rec_screen : '',
							  'image'			=> ( ! empty($mtf_img)) ? $mtf_img : '',
							  'document'		=> ( ! empty($mtf_doc)) ? $mtf_doc : '',
							  'ques_val_1'		=> ( ! empty($ques_val_1)) ? $ques_val_1 : '',
							  'ques_val_2'		=> ( ! empty($ques_val_2)) ? $ques_val_2 : '',
							  'ques_val_3'		=> ( ! empty($ques_val_3)) ? $ques_val_3 : '',
							  'ques_val_4'		=> ( ! empty($ques_val_4)) ? $ques_val_4 : '',
							  'ques_val_5'		=> ( ! empty($ques_val_5)) ? $ques_val_5 : '',
							  'ques_val_6'		=> ( ! empty($ques_val_6)) ? $ques_val_6 : '',
							  'critical'		=> ( ! empty($mtf_criticalQ)) ? $mtf_criticalQ : '',
							  'status' 			=> 1,
							  'user_id'			=> $userId,
							  'cur_date'		=> $curdate,
							  'qid'				=> $ques_id);
			$ques_sql  = "UPDATE `question_tbl` SET `questions`=:questions, `question_type`=:qtype, `qaudio`=:qaudio, `qspeech_text`=:qspeech_text, `speech_text`=:speech_text, `audio`=:audio, `video`=:video, `screen`=:screen, `image`=:image, `document`=:document, `ques_val_1`=:ques_val_1, `ques_val_2`=:ques_val_2, `ques_val_3`=:ques_val_3, `ques_val_4`=:ques_val_4, `ques_val_5`=:ques_val_5, `ques_val_6`=:ques_val_6, `critical`=:critical, `status`=:status, `uid`=:user_id, `cur_date`=:cur_date WHERE question_id=:qid";
			$ques_stmt = $db->prepare($ques_sql);
			$ques_stmt->execute($quesData);
			
			#-----Remove-Old-Feedback-------
			$db->feedback_remove($ques_id);
			
			#--------Correct-Feedback----------
			$cfeedData = array('qid'			=> ( ! empty($ques_id)) ? $ques_id : '',
							   'feedback'		=> ( ! empty($mtf_cfeedback)) ? htmlspecialchars($mtf_cfeedback) : '',
							   'ftype'			=> 1,
							   'speech_text'	=> ( ! empty($mtf_feedback_text_to_speech)) ? htmlspecialchars($mtf_feedback_text_to_speech) : '',
							   'audio'			=> ( ! empty($feed_audio)) ? $feed_audio : '',
							   'video'			=> ( ! empty($feed_video)) ? $feed_video : '',
							   'screen'			=> ( ! empty($mtf_feedback_rec_screen)) ? $mtf_feedback_rec_screen : '',
							   'image'			=> ( ! empty($mtf_feedback_img)) ? $mtf_feedback_img : '',
							   'document'		=> ( ! empty($mtf_feedback_doc)) ? $mtf_feedback_doc : '');
			$cfeed_sql  = "INSERT INTO `feedback_tbl` (`question_id`, `feedback`, `feedback_type`, `feed_speech_text`, `feed_audio`, `feed_video`, `feed_screen`, `feed_image`, `feed_document`) VALUES (:qid, :feedback, :ftype, :speech_text, :audio, :video, :screen, :image, :document)";
			$cfeed_stmt = $db->prepare($cfeed_sql);
			$cfeed_stmt->execute($cfeedData);
			
			#--------Incorrect-Feedback-------
			$ifeedData = array('qid'			=> ( ! empty($ques_id)) ? $ques_id : '',
							   'feedback'		=> ( ! empty($mtf_ifeedback)) ? htmlspecialchars($mtf_ifeedback) : '',
							   'ftype'			=> 2,
							   'speech_text'	=> ( ! empty($mtfi_feedback_text_to_speech)) ? htmlspecialchars($mtfi_feedback_text_to_speech) : '',
							   'audio'			=> ( ! empty($ifeed_audio)) ? $ifeed_audio : '',
							   'video'			=> ( ! empty($ifeed_video)) ? $ifeed_video : '',
							   'screen'			=> ( ! empty($mtfi_feedback_rec_screen)) ? $mtfi_feedback_rec_screen : '',
							   'image'			=> ( ! empty($mtfi_feedback_img)) ? $mtfi_feedback_img : '',
							   'document'		=> ( ! empty($mtfi_feedback_doc)) ? $mtfi_feedback_doc : '');
			$ifeed_sql  = "INSERT INTO `feedback_tbl` (`question_id`, `feedback`, `feedback_type`, `feed_speech_text`, `feed_audio`, `feed_video`, `feed_screen`, `feed_image`, `feed_document`) VALUES (:qid, :feedback, :ftype, :speech_text, :audio, :video, :screen, :image, :document)";
			$ifeed_stmt = $db->prepare($ifeed_sql);
			$ifeed_stmt->execute($ifeedData);
			
			#----------Shuffle-OFF--------------
			if ( ! empty($mtf_choice[0]) && ! empty($mtf_choice_order[0]) && ! empty($mtf_match_order[0]) && ! empty($mtf_match[0]) && ! empty($ques_id)):
				$soff = 0;
				foreach ($mtf_choice as $answerData):
					if ( ! empty($updateAnswerid[$soff])):
						$ansSql = "UPDATE `answer_tbl` SET `choice_option`=:choice_option, `choice_order_no`=:choice_order_no, `match_order_no`=:match_order_no, `match_option`=:match_option, `status`=:status, `uid`=:user_id, `cur_date`=:cur_date WHERE answer_id=:answer_id";
						$stmt 	= $db->prepare($ansSql);
						$stmt->execute(array('answer_id' 		=> $updateAnswerid[$soff],
											 'choice_option'	=> ( ! empty($mtf_choice[$soff])) ? htmlspecialchars($mtf_choice[$soff]) : '',
											 'choice_order_no'	=> ( ! empty($mtf_choice_order[$soff])) ? $mtf_choice_order[$soff] : '',
											 'match_order_no'	=> ( ! empty($mtf_match_order[$soff])) ? $mtf_match_order[$soff] : '',
											 'match_option'		=> ( ! empty($mtf_match[$soff])) ? htmlspecialchars($mtf_match[$soff]) : '',
											 'status' 			=> 1,
											 'user_id' 			=> $userId,
											 'cur_date'			=> $curdate));
					else:
						$ansSql = "INSERT INTO `answer_tbl` (`question_id`, `choice_option`, `choice_order_no`, `match_order_no`, `match_option`, `status`, `uid`, `cur_date`) 
								   VALUES (:qid, :choice_option, :choice_order_no, :match_order_no, :match_option, :status, :user_id, :cur_date)";
						$stmt = $db->prepare($ansSql);
						$stmt->execute(array('qid'				=> $ques_id,
											 'choice_option'	=> ( ! empty($mtf_choice[$soff])) ? htmlspecialchars($mtf_choice[$soff]) : '',
											 'choice_order_no'	=> ( ! empty($mtf_choice_order[$soff])) ? $mtf_choice_order[$soff] : '',
											 'match_order_no'	=> ( ! empty($mtf_match_order[$soff])) ? $mtf_match_order[$soff] : '',
											 'match_option'		=> ( ! empty($mtf_match[$soff])) ? htmlspecialchars($mtf_match[$soff]) : '',
											 'status' 			=> 1,
											 'user_id' 			=> $userId,
											 'cur_date'			=> $curdate));
					endif;
				$soff++;
				endforeach;
			endif;
			
			#----------Shuffle-ON--------------
			if ( ! empty($mtf_choice2[0]) && ! empty($mtf_match2[0]) && empty($mtf_match_order[0]) && empty($mtf_match[0])):
				$sON = 0;
				foreach ($mtf_choice2 as $answerData):
					if ( ! empty($updateAnswerid[$sON])):
						$ansSql = "UPDATE `answer_tbl` SET `choice_option`=:choice_option, `match_option`=:match_option, `status`=:status, `uid`=:user_id, `cur_date`=:cur_date WHERE answer_id=:answer_id";
						$stmt 	= $db->prepare($ansSql);
						$stmt->execute(array('answer_id' 		=> $updateAnswerid[$sON],
											 'choice_option'	=> ( ! empty($mtf_choice2[$sON])) ? htmlspecialchars($mtf_choice2[$sON]) : '',
											 'match_option'		=> ( ! empty($mtf_match2[$sON])) ? htmlspecialchars($mtf_match2[$sON]) : '',
											 'status' 			=> 1,
											 'user_id' 			=> $userId,
											 'cur_date'			=> $curdate));
					else:
						$ansSql = "INSERT INTO `answer_tbl` (`question_id`, `choice_option`, `match_option`, `status`, `uid`, `cur_date`) 
								   VALUES (:qid, :choice_option, :match_option, :status, :user_id, :cur_date)";
						$stmt = $db->prepare($ansSql);
						$stmt->execute(array('qid'				=> $ques_id,
											 'choice_option'	=> ( ! empty($mtf_choice2[$sON])) ? htmlspecialchars($mtf_choice2[$sON]) : '',
											 'match_option'		=> ( ! empty($mtf_match2[$sON])) ? htmlspecialchars($mtf_match2[$sON]) : '',
											 'status' 			=> 1,
											 'user_id' 			=> $userId,
											 'cur_date'			=> $curdate));
					endif;
					$sON++;
				endforeach;
			endif;
		//------END-MATCH-THE-FOLLOWING------
		
		//-----------START-SEQUENCE--------
		elseif ($question_type == 2):
			#-------QUESTIONS-DATA---
			$qaudio = '';
			if ( ! empty($sqq_qaudio)):
				$qaudio = $sqq_qaudio;
			elseif ( ! empty($sqq_rec_audio)):
				$qaudio = $sqq_rec_audio;
			endif;
			
			$audio = '';
			if ( ! empty($sq_qaudio)):
				$audio = $sq_qaudio;
			elseif ( ! empty($sq_rec_audio)):
				$audio = $sq_rec_audio;
			endif;
			
			$video = '';
			if ( ! empty($sq_video)):
				$video = $sq_video;
			elseif ( ! empty($sq_rec_video)):
				$video = $sq_rec_video;
			endif;
			
			#------Correct-FEEDBACK-DATA---
			$feed_audio = '';
			if ( ! empty($sq_feedback_audio)):
				$feed_audio = $sq_feedback_audio;
			elseif ( ! empty($sq_feedback_rec_audio)):
				$feed_audio = $sq_feedback_rec_audio;
			endif;
			
			$feed_video = '';
			if ( ! empty($sq_feedback_video)):
				$feed_video = $sq_feedback_video;
			elseif ( ! empty($sq_feedback_rec_video)):
				$feed_video = $sq_feedback_rec_video;
			endif;
			
			#------Incorrect-FEEDBACK-DATA---
			$ifeed_audio = '';
			if ( ! empty($sqi_feedback_audio)):
				$ifeed_audio = $sqi_feedback_audio;
			elseif ( ! empty($sqi_feedback_rec_audio)):
				$ifeed_audio = $sqi_feedback_rec_audio;
			endif;
			
			$ifeed_video = '';
			if ( ! empty($sqi_feedback_video)):
				$ifeed_video = $sqi_feedback_video;
			elseif ( ! empty($sqi_feedback_rec_video)):
				$ifeed_video = $sqi_feedback_rec_video;
			endif;
			
			$quesData = array('questions'		=> ( ! empty($sq)) ? htmlspecialchars($sq) : '',
							  'qtype'			=> ( ! empty($question_type)) ? $question_type : '',
							  'qaudio'			=> ( ! empty($qaudio)) ? $qaudio : '',
							  'qspeech_text'	=> ( ! empty($sqq_text_to_speech)) ? htmlspecialchars($sqq_text_to_speech) : '',
							  'speech_text'		=> ( ! empty($sq_text_to_speech)) ? htmlspecialchars($sq_text_to_speech) : '',
							  'audio'			=> ( ! empty($audio)) ? $audio : '',
							  'video'			=> ( ! empty($video)) ? $video : '',
							  'screen'			=> ( ! empty($sq_rec_screen)) ? $sq_rec_screen : '',
							  'image'			=> ( ! empty($sq_img)) ? $sq_img : '',
							  'document'		=> ( ! empty($sq_doc)) ? $sq_doc : '',
							  'shuffle'			=> (isset($sq_shuffle) && ! empty($sq_shuffle)) ? 1 : 0,
							  'ques_val_1'		=> ( ! empty($sq_ques_val_1)) ? $sq_ques_val_1 : '',
							  'ques_val_2'		=> ( ! empty($sq_ques_val_2)) ? $sq_ques_val_2 : '',
							  'ques_val_3'		=> ( ! empty($sq_ques_val_3)) ? $sq_ques_val_3 : '',
							  'ques_val_4'		=> ( ! empty($sq_ques_val_4)) ? $sq_ques_val_4 : '',
							  'ques_val_5'		=> ( ! empty($sq_ques_val_5)) ? $sq_ques_val_5 : '',
							  'ques_val_6'		=> ( ! empty($sq_ques_val_6)) ? $sq_ques_val_6 : '',
							  'critical'		=> ( ! empty($sq_criticalQ)) ? $sq_criticalQ : '',
							  'status' 			=> 1,
							  'user_id'			=> $userId,
							  'cur_date'		=> $curdate,
							  'qid'				=> $ques_id);
			$ques_sql  = "UPDATE `question_tbl` SET `questions`=:questions, `question_type`=:qtype, `qaudio`=:qaudio, `qspeech_text`=:qspeech_text, `speech_text`=:speech_text, `audio`=:audio, `video`=:video, `screen`=:screen, `image`=:image, `document`=:document, `shuffle`=:shuffle, `ques_val_1`=:ques_val_1, `ques_val_2`=:ques_val_2, `ques_val_3`=:ques_val_3, `ques_val_4`=:ques_val_4, `ques_val_5`=:ques_val_5, `ques_val_6`=:ques_val_6, `critical`=:critical, `status`=:status, `uid`=:user_id, `cur_date`=:cur_date WHERE question_id=:qid";
			$ques_stmt = $db->prepare($ques_sql);
			$ques_stmt->execute($quesData);
			
			#-----Remove-Old-Feedback-------
			$db->feedback_remove($ques_id);
			
			#--------Correct-Feedback----------
			$cfeedData = array('qid'			=> ( ! empty($ques_id)) ? $ques_id : '',
							   'feedback'		=> ( ! empty($sq_feedback)) ? htmlspecialchars($sq_feedback) : '',
							   'ftype'			=> 1,
							   'speech_text'	=> ( ! empty($sq_feedback_text_to_speech)) ? htmlspecialchars($sq_feedback_text_to_speech) : '',
							   'audio'			=> ( ! empty($feed_audio)) ? $feed_audio : '',
							   'video'			=> ( ! empty($feed_video)) ? $feed_video : '',
							   'screen'			=> ( ! empty($sq_feedback_rec_screen)) ? $sq_feedback_rec_screen : '',
							   'image'			=> ( ! empty($sq_feedback_img)) ? $sq_feedback_img : '',
							   'document'		=> ( ! empty($sq_feedback_doc)) ? $sq_feedback_doc : '');
			
			$cfeed_sql  = "INSERT INTO `feedback_tbl` (`question_id`, `feedback`, `feedback_type`, `feed_speech_text`, `feed_audio`, `feed_video`, `feed_screen`, `feed_image`, `feed_document`) VALUES (:qid, :feedback, :ftype, :speech_text, :audio, :video, :screen, :image, :document)";
			$cfeed_stmt = $db->prepare($cfeed_sql);
			$cfeed_stmt->execute($cfeedData);
			
			#--------Incorrect-Feedback-------
			$ifeedData = array('qid'			=> ( ! empty($ques_id)) ? $ques_id : '',
							   'feedback'		=> ( ! empty($sq_ifeedback)) ? htmlspecialchars($sq_ifeedback) : '',
							   'ftype'			=> 2,
							   'speech_text'	=> ( ! empty($sqi_feedback_text_to_speech)) ? htmlspecialchars($sqi_feedback_text_to_speech) : '',
							   'audio'			=> ( ! empty($ifeed_audio)) ? $ifeed_audio : '',
							   'video'			=> ( ! empty($ifeed_video)) ? $ifeed_video : '',
							   'screen'			=> ( ! empty($sqi_feedback_rec_screen)) ? $sqi_feedback_rec_screen : '',
							   'image'			=> ( ! empty($sqi_feedback_img)) ? $sqi_feedback_img : '',
							   'document'		=> ( ! empty($sqi_feedback_doc)) ? $sqi_feedback_doc : '');
			
			$ifeed_sql  = "INSERT INTO `feedback_tbl` (`question_id`, `feedback`, `feedback_type`, `feed_speech_text`, `feed_audio`, `feed_video`, `feed_screen`, `feed_image`, `feed_document`) VALUES (:qid, :feedback, :ftype, :speech_text, :audio, :video, :screen, :image, :document)";
			$ifeed_stmt = $db->prepare($ifeed_sql);
			$ifeed_stmt->execute($ifeedData);
			
			#----------ADD-OPTIONS-------------
			if ( ! empty($sq_ans[0])):
				$sqi = 0; $sqi_order = 1;
				foreach ($sq_ans as $answerData):
					if ( ! empty($updateSQAnswerid[$sqi])):
						$ansSql = "UPDATE `answer_tbl` SET `choice_option`=:choice_option, `match_order_no`=:order_no, `status`=:status, `uid`=:user_id, `cur_date`=:cur_date WHERE answer_id=:answer_id";
						$stmt	= $db->prepare($ansSql);
						$stmt->execute(array('answer_id' 		=> $updateSQAnswerid[$sqi],
											 'choice_option'	=> ( ! empty($sq_ans[$sqi])) ? htmlspecialchars($sq_ans[$sqi]) : '',
											 'order_no'			=> $sqi_order,
											 'status' 			=> 1,
											 'user_id' 			=> $userId,
											 'cur_date'			=> $curdate));
					else:
						$ansSql = "INSERT INTO `answer_tbl` (`question_id`, `choice_option`, `match_order_no`, `status`, `uid`, `cur_date`) 
								   VALUES (:qid, :choice_option, :order_no, :status, :user_id, :cur_date)";
						$stmt = $db->prepare($ansSql);
						$stmt->execute(array('qid'				=> $ques_id,
											 'choice_option'	=> ( ! empty($sq_ans[$sqi])) ? htmlspecialchars($sq_ans[$sqi]) : '',
											 'order_no'			=> $sqi_order,
											 'status' 			=> 1,
											 'user_id' 			=> $userId,
											 'cur_date'			=> $curdate));
					endif;
					$sqi++; $sqi_order++;
				endforeach;
			endif;
		//-----------END-SEQUENCE----------
	
		//-----------START-SORTING----------
		elseif ($question_type == 3):
			#-------QUESTIONS-DATA---
			$qaudio = '';
			if ( ! empty($sortqq_qaudio)):
				$qaudio = $sortqq_qaudio;
			elseif ( ! empty($sortqq_rec_audio)):
				$qaudio = $sortqq_rec_audio;
			endif;
			
			$audio = '';
			if ( ! empty($sortq_qaudio)):
				$audio = $sortq_qaudio;
			elseif ( ! empty($sortq_rec_audio)):
				$audio = $sortq_rec_audio;
			endif;
			
			$video = '';
			if ( ! empty($sortq_video)):
				$video = $sortq_video;
			elseif ( ! empty($sortq_rec_video)):
				$video = $sortq_rec_video;
			endif;
			
			#------Correct-FEEDBACK-DATA---
			$feed_audio = '';
			if ( ! empty($sortq_feedback_audio)):
				$feed_audio = $sortq_feedback_audio;
			elseif ( ! empty($sortq_feedback_rec_audio)):
				$feed_audio = $sortq_feedback_rec_audio;
			endif;
			
			$feed_video = '';
			if ( ! empty($sortq_feedback_video)):
				$feed_video = $sortq_feedback_video;
			elseif ( ! empty($sortq_feedback_rec_video)):
				$feed_video = $sortq_feedback_rec_video;
			endif;
			
			#------Incorrect-FEEDBACK-DATA---
			$ifeed_audio = '';
			if ( ! empty($sortqi_feedback_audio)):
				$ifeed_audio = $sortqi_feedback_audio;
			elseif ( ! empty($sortqi_feedback_rec_audio)):
				$ifeed_audio = $sortqi_feedback_rec_audio;
			endif;
			
			$ifeed_video = '';
			if ( ! empty($sortqi_feedback_video)):
				$ifeed_video = $sortqi_feedback_video;
			elseif ( ! empty($sortqi_feedback_rec_video)):
				$ifeed_video = $sortqi_feedback_rec_video;
			endif;
						
			$quesData = array('questions'		=> ( ! empty($sortq)) ? htmlspecialchars($sortq) : '',
							  'qtype'			=> ( ! empty($question_type)) ? $question_type : '',
					  		  'qaudio'			=> ( ! empty($qaudio)) ? $qaudio : '',
					  		  'qspeech_text'	=> ( ! empty($sortqq_text_to_speech)) ? htmlspecialchars($sortqq_text_to_speech) : '',
							  'speech_text'		=> ( ! empty($sortq_text_to_speech)) ? htmlspecialchars($sortq_text_to_speech) : '',
							  'audio'			=> ( ! empty($audio)) ? $audio : '',
							  'video'			=> ( ! empty($video)) ? $video : '',
							  'screen'			=> ( ! empty($sortq_rec_screen)) ? $sortq_rec_screen : '',
							  'image'			=> ( ! empty($sortq_img)) ? $sortq_img : '',
							  'document'		=> ( ! empty($sortq_doc)) ? $sortq_doc : '',
							  'shuffle'			=> (isset($sort_shuffle) && ! empty($sort_shuffle)) ? 1 : 0,
							  'ques_val_1'		=> ( ! empty($sort_ques_val_1)) ? $sort_ques_val_1 : '',
							  'ques_val_2'		=> ( ! empty($sort_ques_val_2)) ? $sort_ques_val_2 : '',
							  'ques_val_3'		=> ( ! empty($sort_ques_val_3)) ? $sort_ques_val_3 : '',
							  'ques_val_4'		=> ( ! empty($sort_ques_val_4)) ? $sort_ques_val_4 : '',
							  'ques_val_5'		=> ( ! empty($sort_ques_val_5)) ? $sort_ques_val_5 : '',
							  'ques_val_6'		=> ( ! empty($sort_ques_val_6)) ? $sort_ques_val_6 : '',
							  'critical'		=> ( ! empty($sort_criticalQ)) ? $sort_criticalQ : '',
							  'status' 			=> 1,
							  'user_id'			=> $userId,
							  'cur_date'		=> $curdate,
							  'qid'				=> $ques_id);
			$ques_sql  = "UPDATE `question_tbl` SET `questions`=:questions, `question_type`=:qtype, `qaudio`=:qaudio, `qspeech_text`=:qspeech_text, `speech_text`=:speech_text, `audio`=:audio, `video`=:video, `screen`=:screen, `image`=:image, `document`=:document, `shuffle`=:shuffle, `ques_val_1`=:ques_val_1, `ques_val_2`=:ques_val_2, `ques_val_3`=:ques_val_3, `ques_val_4`=:ques_val_4, `ques_val_5`=:ques_val_5, `ques_val_6`=:ques_val_6, `critical`=:critical, `status`=:status, `uid`=:user_id, `cur_date`=:cur_date WHERE question_id=:qid";
			$ques_stmt = $db->prepare($ques_sql);
			$ques_stmt->execute($quesData);
			
			#-----Remove-Old-Feedback-----
			$db->feedback_remove($ques_id);
			
			#--------Correct-Feedback----------
			$cfeedData = array('qid'			=> ( ! empty($ques_id)) ? $ques_id : '',
							   'feedback'		=> ( ! empty($sortq_cfeedback)) ? htmlspecialchars($sortq_cfeedback) : '',
							   'ftype'			=> 1,
							   'speech_text'	=> ( ! empty($sortq_feedback_text_to_speech)) ? htmlspecialchars($sortq_feedback_text_to_speech) : '',
							   'audio'			=> ( ! empty($feed_audio)) ? $feed_audio : '',
							   'video'			=> ( ! empty($feed_video)) ? $feed_video : '',
							   'screen'			=> ( ! empty($sortq_feedback_rec_screen)) ? $sortq_feedback_rec_screen : '',
							   'image'			=> ( ! empty($sortq_feedback_img)) ? $sortq_feedback_img : '',
							   'document'		=> ( ! empty($sortq_feedback_doc)) ? $sortq_feedback_doc : '');
			$cfeed_sql  = "INSERT INTO `feedback_tbl` (`question_id`, `feedback`, `feedback_type`, `feed_speech_text`, `feed_audio`, `feed_video`, `feed_screen`, `feed_image`, `feed_document`) VALUES (:qid, :feedback, :ftype, :speech_text, :audio, :video, :screen, :image, :document)";
			$cfeed_stmt = $db->prepare($cfeed_sql);
			$cfeed_stmt->execute($cfeedData);
			
			#--------Incorrect-Feedback-------
			$ifeedData = array('qid'			=> ( ! empty($ques_id)) ? $ques_id : '',
							   'feedback'		=> ( ! empty($sortq_ifeedback)) ? htmlspecialchars($sortq_ifeedback) : '',
							   'ftype'			=> 2,
							   'speech_text'	=> ( ! empty($sortqi_feedback_text_to_speech)) ? htmlspecialchars($sortqi_feedback_text_to_speech) : '',
							   'audio'			=> ( ! empty($ifeed_audio)) ? $ifeed_audio : '',
							   'video'			=> ( ! empty($ifeed_video)) ? $ifeed_video : '',
							   'screen'			=> ( ! empty($sortqi_feedback_rec_screen)) ? $sortqi_feedback_rec_screen : '',
							   'image'			=> ( ! empty($sortqi_feedback_img)) ? $sortqi_feedback_img : '',
							   'document'		=> ( ! empty($sortqi_feedback_doc)) ? $sortqi_feedback_doc : '');
			
			$ifeed_sql  = "INSERT INTO `feedback_tbl` (`question_id`, `feedback`, `feedback_type`, `feed_speech_text`, `feed_audio`, `feed_video`, `feed_screen`, `feed_image`, `feed_document`) VALUES (:qid, :feedback, :ftype, :speech_text, :audio, :video, :screen, :image, :document)";
			$ifeed_stmt = $db->prepare($ifeed_sql);
			$ifeed_stmt->execute($ifeedData);
			
			#----------ADD-OPTIONS----------
			$sortqi = 0;
			foreach ($sortq_sorting_items as $optionData):
				if ( ! empty($updateSortAnswerid[$sortqi])):
					$ansSql = "UPDATE `answer_tbl` SET `choice_option`=:choice_option, `image`=:image, `status`=:status, `uid`=:user_id, `cur_date`=:cur_date WHERE answer_id=:answer_id";
					$stmt	= $db->prepare($ansSql);
					$stmt->execute(array('answer_id'		=> $updateSortAnswerid[$sortqi],
										 'choice_option'	=> (isset($sortq_sorting_items[$sortqi]) && ! empty($sortq_sorting_items[$sortqi])) ? htmlspecialchars($sortq_sorting_items[$sortqi]) : '',
										 'image'			=> (isset($sortq_item_img[$sortqi]) && ! empty($sortq_item_img[$sortqi])) ? $sortq_item_img[$sortqi] : '',
										 'status' 			=> 1,
										 'user_id' 			=> $userId,
										 'cur_date'			=> $curdate));
					if ( ! empty($updateSubOptionid[$sortqi])):
						$sortqop = 0;
						foreach ($sortq_drag_items[$sortqi]['option'] as $subOptionData):
							if ( ! empty($updateSubOptionid[$sortqi][$sortqop])):
								$ansOpSql = "UPDATE `sub_options_tbl` SET `sub_option`=:sub_option WHERE sub_options_id=:sub_options_id";
								$opstmt	  = $db->prepare($ansOpSql);
								$opstmt->execute(array('sub_options_id'	=> $updateSubOptionid[$sortqi][$sortqop],
													   'sub_option'	 	=> ( ! empty($subOptionData)) ? htmlspecialchars($subOptionData) : ''));
							else:
								$opSql	= "INSERT INTO `sub_options_tbl` (`answer_id`, `sub_option`) VALUES (:aid, :option)";
								$opstmt = $db->prepare($opSql);
								$opstmt->execute(array('aid'	=> $updateSortAnswerid[$sortqi],
													   'option'	=> ( ! empty($subOptionData)) ? htmlspecialchars($subOptionData) : ''));
							endif;
							$sortqop++;
						endforeach;
					endif;
				else:
					$ansSql = "INSERT INTO `answer_tbl` (`question_id`, `choice_option`, `image`, `status`, `uid`, `cur_date`) VALUES (:qid, :choice_option, :image, :status, :user_id, :cur_date)";
					$stmt	= $db->prepare($ansSql);
					$stmt->execute(array('qid'				=> $ques_id,
										 'choice_option'	=> ( ! empty($sortq_sorting_items[$sortqi])) ? htmlspecialchars($sortq_sorting_items[$sortqi]) : '',
										 'image'			=> (isset($sortq_item_img[$sortqi]) && ! empty($sortq_item_img[$sortqi])) ? $sortq_item_img[$sortqi] : '',
										 'status' 			=> 1,
										 'user_id' 			=> $userId,
										 'cur_date'			=> $curdate));
					$ans_id = $db->lastInsertId();
					if ( ! empty($sortq_drag_items)):
						foreach ($sortq_drag_items[$sortqi]['option'] as $subOptionData):
							$opSql  = "INSERT INTO `sub_options_tbl` (`answer_id`, `sub_option`) VALUES (:aid, :option)";
							$opstmt = $db->prepare($opSql);
							$opstmt->execute(array('aid' => $ans_id,'option' => ( ! empty($subOptionData)) ? htmlspecialchars($subOptionData) : ''));
						endforeach;
					endif;
				endif;
				$sortqi++;
			endforeach;
		//-----------END-SORTING----------
	
		//-----------START-MCQ----------
		elseif ($question_type == 4):
			#-------QUESTIONS-DATA---
			$qaudio = '';
			if ( ! empty($mcqq_qaudio)):
				$qaudio = $mcqq_qaudio;
			elseif ( ! empty($mcqq_rec_audio)):
				$qaudio = $mcqq_rec_audio;
			endif;
			
			$audio = '';
			if ( ! empty($mcq_audio)):
				$audio = $mcq_audio;
			elseif ( ! empty($mcq_rec_audio)):
				$audio = $mcq_rec_audio;
			endif;
			
			$video = '';
			if ( ! empty($mcq_video)):
				$video = $mcq_video;
			elseif ( ! empty($mcq_rec_video)):
				$video = $mcq_rec_video;
			endif;
			
			#------Correct-FEEDBACK-DATA---
			$feed_audio = '';
			if ( ! empty($mcq_feedback_audio)):
				$feed_audio = $mcq_feedback_audio;
			elseif ( ! empty($mcq_feedback_rec_audio)):
				$feed_audio = $mcq_feedback_rec_audio;
			endif;
			
			$feed_video = '';
			if ( ! empty($mcq_feedback_video)):
				$feed_video = $mcq_feedback_video;
			elseif ( ! empty($mcq_feedback_rec_video)):
				$feed_video = $mcq_feedback_rec_video;
			endif;
			
			#------Incorrect-FEEDBACK-DATA---
			$ifeed_audio = '';
			if ( ! empty($mcqi_feedback_audio)):
				$ifeed_audio = $mcqi_feedback_audio;
			elseif ( ! empty($mcqi_feedback_rec_audio)):
				$ifeed_audio = $mcqi_feedback_rec_audio;
			endif;
			
			$ifeed_video = '';
			if ( ! empty($mcqi_feedback_video)):
				$ifeed_video = $mcqi_feedback_video;
			elseif ( ! empty($mcqi_feedback_rec_video)):
				$ifeed_video = $mcqi_feedback_rec_video;
			endif;
						
			$quesData = array('questions'		=> ( ! empty($mcq)) ? htmlspecialchars($mcq) : '',
							  'qtype'			=> ( ! empty($question_type)) ? $question_type : '',
							  'qaudio'			=> ( ! empty($qaudio)) ? $qaudio : '',
							  'qspeech_text'	=> ( ! empty($mcqq_text_to_speech)) ? htmlspecialchars($mcqq_text_to_speech) : '',
							  'speech_text'		=> ( ! empty($mcq_text_to_speech)) ? htmlspecialchars($mcq_text_to_speech) : '',
							  'audio'			=> ( ! empty($audio)) ? $audio : '',
							  'video'			=> ( ! empty($video)) ? $video : '',
							  'screen'			=> ( ! empty($mcq_rec_screen)) ? $mcq_rec_screen : '',
							  'image'			=> ( ! empty($mcq_img)) ? $mcq_img : '',
							  'document'		=> ( ! empty($mcq_doc)) ? $mcq_doc : '',
							  'true_option'		=> ( ! empty($mcq_true_option)) ? $mcq_true_option : '',
							  'shuffle'			=> (isset($mcq_shuffle) && ! empty($mcq_shuffle)) ? 1 : 0,
							  'ques_val_1'		=> ( ! empty($mcq_ques_val_1)) ? $mcq_ques_val_1 : '',
							  'ques_val_2'		=> ( ! empty($mcq_ques_val_2)) ? $mcq_ques_val_2 : '',
							  'ques_val_3'		=> ( ! empty($mcq_ques_val_3)) ? $mcq_ques_val_3 : '',
							  'ques_val_4'		=> ( ! empty($mcq_ques_val_4)) ? $mcq_ques_val_4 : '',
							  'ques_val_5'		=> ( ! empty($mcq_ques_val_5)) ? $mcq_ques_val_5 : '',
							  'ques_val_6'		=> ( ! empty($mcq_ques_val_6)) ? $mcq_ques_val_6 : '',
							  'critical'		=> ( ! empty($mcq_criticalQ)) ? $mcq_criticalQ : '',
							  'status' 			=> 1,
							  'user_id'			=> $userId,
							  'cur_date'		=> $curdate,
							  'qid'				=> $ques_id);
			$ques_sql  = "UPDATE `question_tbl` SET `questions`=:questions, `question_type`=:qtype, `qaudio`=:qaudio, `qspeech_text`=:qspeech_text, `speech_text`=:speech_text, `audio`=:audio, `video`=:video, `screen`=:screen, `image`=:image, `document`=:document, `true_options`=:true_option, `shuffle`=:shuffle, `ques_val_1`=:ques_val_1, `ques_val_2`=:ques_val_2, `ques_val_3`=:ques_val_3, `ques_val_4`=:ques_val_4, `ques_val_5`=:ques_val_5, `ques_val_6`=:ques_val_6, `critical`=:critical, `status`=:status, `uid`=:user_id, `cur_date`=:cur_date WHERE question_id=:qid";
			$ques_stmt = $db->prepare($ques_sql);
			$ques_stmt->execute($quesData);
			
			#-----Remove-Old-Feedback-------
			$db->feedback_remove($ques_id);
			
			#--------Correct-Feedback----------
			$cfeedData = array('qid'			=> ( ! empty($ques_id)) ? $ques_id : '',
							   'feedback'		=> ( ! empty($mcq_cfeedback)) ? htmlspecialchars($mcq_cfeedback) : '',
							   'ftype'			=> 1,
							   'speech_text'	=> ( ! empty($mcq_feedback_text_to_speech)) ? htmlspecialchars($mcq_feedback_text_to_speech) : '',
							   'audio'			=> ( ! empty($feed_audio)) ? $feed_audio : '',
							   'video'			=> ( ! empty($feed_video)) ? $feed_video : '',
							   'screen'			=> ( ! empty($mcq_feedback_rec_screen)) ? $mcq_feedback_rec_screen : '',
							   'image'			=> ( ! empty($mcq_feedback_img)) ? $mcq_feedback_img : '',
							   'document'		=> ( ! empty($mcq_feedback_doc)) ? $mcq_feedback_doc : '');
			
			$cfeed_sql = "INSERT INTO `feedback_tbl` (`question_id`, `feedback`, `feedback_type`, `feed_speech_text`, `feed_audio`, `feed_video`, `feed_screen`, `feed_image`, `feed_document`) VALUES (:qid, :feedback, :ftype, :speech_text, :audio, :video, :screen, :image, :document)";
			$cfeed_stmt = $db->prepare($cfeed_sql);
			$cfeed_stmt->execute($cfeedData);
			
			#--------Incorrect-Feedback-------
			$ifeedData = array('qid'			=> ( ! empty($ques_id)) ? $ques_id : '',
							   'feedback'		=> ( ! empty($mcq_ifeedback)) ? htmlspecialchars($mcq_ifeedback) : '',
							   'ftype'			=> 2,
							   'speech_text'	=> ( ! empty($mcqi_feedback_text_to_speech)) ? htmlspecialchars($mcqi_feedback_text_to_speech) : '',
							   'audio'			=> ( ! empty($ifeed_audio)) ? $ifeed_audio : '',
							   'video'			=> ( ! empty($ifeed_video)) ? $ifeed_video : '',
							   'screen'			=> ( ! empty($mcqi_feedback_rec_screen)) ? $mcqi_feedback_rec_screen : '',
							   'image'			=> ( ! empty($mcqi_feedback_img)) ? $mcqi_feedback_img : '',
							   'document'		=> ( ! empty($mcqi_feedback_doc)) ? $mcqi_feedback_doc : '');
			
			$ifeed_sql  = "INSERT INTO `feedback_tbl` (`question_id`, `feedback`, `feedback_type`, `feed_speech_text`, `feed_audio`, `feed_video`, `feed_screen`, `feed_image`, `feed_document`) VALUES (:qid, :feedback, :ftype, :speech_text, :audio, :video, :screen, :image, :document)";
			$ifeed_stmt = $db->prepare($ifeed_sql);
			$ifeed_stmt->execute($ifeedData);
			
			#----------ADD-OPTIONS-------------
			if ( ! empty($mcq_option[0])):
				$mcqi = 0; $mcqtrue = 1;
				foreach ($mcq_option as $answerData):
					if ( ! empty($updateMCQAnswerid[$mcqi])):
						$ansSql = "UPDATE `answer_tbl` SET `choice_option`=:option, `true_option`=:true, `status`=:status, `uid`=:user_id, `cur_date`=:cur_date WHERE answer_id=:aid";
						$stmt = $db->prepare($ansSql);
						$stmt->execute(array('aid'		=> $updateMCQAnswerid[$mcqi],
											 'option'	=> ( ! empty($mcq_option[$mcqi])) ? htmlspecialchars($mcq_option[$mcqi]) : '',
											 'true'		=> ( ! empty($mcq_true_option) && $mcq_true_option == $mcqtrue) ? 1 : '',
											 'status'	=> 1,
											 'user_id' 	=> $userId,
											 'cur_date'	=> $curdate));

					else:
						$ansSql = "INSERT INTO `answer_tbl` (`question_id`, `choice_option`, `true_option`, `status`, `uid`, `cur_date`) VALUES (:qid, :option, :true, :status, :user_id, :cur_date)";
						$stmt = $db->prepare($ansSql);
						$stmt->execute(array('qid'		=> $ques_id,
											 'option'	=> ( ! empty($mcq_option[$mcqi])) ? htmlspecialchars($mcq_option[$mcqi]) : '',
											 'true'		=> ( ! empty($mcq_true_option) && $mcq_true_option == $mcqtrue) ? 1 : '',
											 'status'	=> 1,
											 'user_id' 	=> $userId,
											 'cur_date'	=> $curdate));
					endif;
					$mcqi++; $mcqtrue++;
				endforeach;
			endif;
		//-----------END-MCQ----------
	
		//-----------START-MMCQ----------
		elseif ($question_type == 5):
			#-------QUESTIONS-DATA---
			$qaudio = '';
			if ( ! empty($mmcqq_qaudio)):
				$qaudio = $mmcqq_qaudio;
			elseif ( ! empty($mmcqq_rec_audio)):
				$qaudio = $mmcqq_rec_audio;
			endif;

			$audio = '';
			if ( ! empty($mmcq_audio)):
				$audio = $mmcq_audio;
			elseif ( ! empty($mmcq_rec_audio)):
				$audio = $mmcq_rec_audio;
			endif;
			
			$video = '';
			if ( ! empty($mmcq_video)):
				$video = $mmcq_video;
			elseif ( ! empty($mmcq_rec_video)):
				$video = $mmcq_rec_video;
			endif;
			
			#------Correct-FEEDBACK-DATA---
			$feed_audio = '';
			if ( ! empty($mmcq_feedback_audio)):
				$feed_audio = $mmcq_feedback_audio;
			elseif ( ! empty($mmcq_feedback_rec_audio)):
				$feed_audio = $mmcq_feedback_rec_audio;
			endif;
			
			$feed_video = '';
			if ( ! empty($mmcq_feedback_video)):
				$feed_video = $mmcq_feedback_video;
			elseif ( ! empty($mmcq_feedback_rec_video)):
				$feed_video = $mmcq_feedback_rec_video;
			endif;
			
			#------Incorrect-FEEDBACK-DATA---
			$ifeed_audio = '';
			if ( ! empty($mmcqi_feedback_audio)):
				$ifeed_audio = $mmcqi_feedback_audio;
			elseif ( ! empty($mmcqi_feedback_rec_audio)):
				$ifeed_audio = $mmcqi_feedback_rec_audio;
			endif;
			
			$ifeed_video = '';
			if ( ! empty($mmcqi_feedback_video)):
				$ifeed_video = $mmcqi_feedback_video;
			elseif ( ! empty($mmcqi_feedback_rec_video)):
				$ifeed_video = $mmcqi_feedback_rec_video;
			endif;
			
			$quesData = array('questions'		=> ( ! empty($mmcq)) ? htmlspecialchars($mmcq) : '',
							  'qtype'			=> ( ! empty($question_type)) ? $question_type : '',
					  		  'qaudio'			=> ( ! empty($qaudio)) ? $qaudio : '',
					  		  'qspeech_text'	=> ( ! empty($mmcqq_text_to_speech)) ? htmlspecialchars($mmcqq_text_to_speech) : '',
							  'speech_text'		=> ( ! empty($mmcq_text_to_speech)) ? htmlspecialchars($mmcq_text_to_speech) : '',
							  'audio'			=> ( ! empty($audio)) ? $audio : '',
							  'video'			=> ( ! empty($video)) ? $video : '',
							  'screen'			=> ( ! empty($mmcq_rec_screen)) ? $mmcq_rec_screen : '',
							  'image'			=> ( ! empty($mmcq_img)) ? $mmcq_img : '',
							  'document'		=> ( ! empty($mmcq_doc)) ? $mmcq_doc : '',
							  'true_option'		=> ( ! empty($mmcq_true_option)) ? $db->addMultiIds($mmcq_true_option) : '',
							  'shuffle'			=> (isset($mmcq_shuffle) && ! empty($mmcq_shuffle)) ? 1 : 0,
							  'ques_val_1'		=> ( ! empty($mmcq_ques_val_1)) ? $mmcq_ques_val_1 : '',
							  'ques_val_2'		=> ( ! empty($mmcq_ques_val_2)) ? $mmcq_ques_val_2 : '',
							  'ques_val_3'		=> ( ! empty($mmcq_ques_val_3)) ? $mmcq_ques_val_3 : '',
							  'ques_val_4'		=> ( ! empty($mmcq_ques_val_4)) ? $mmcq_ques_val_4 : '',
							  'ques_val_5'		=> ( ! empty($mmcq_ques_val_5)) ? $mmcq_ques_val_5 : '',
							  'ques_val_6'		=> ( ! empty($mmcq_ques_val_6)) ? $mmcq_ques_val_6 : '',
							  'critical'		=> ( ! empty($mmcq_criticalQ)) ? $mmcq_criticalQ : '',
							  'status' 			=> 1,
							  'user_id'			=> $userId,
							  'cur_date'		=> $curdate,
							  'qid'				=> $ques_id);
			$ques_sql  = "UPDATE `question_tbl` SET `questions`=:questions, `question_type`=:qtype, `qaudio`=:qaudio, `qspeech_text`=:qspeech_text, `speech_text`=:speech_text, `audio`=:audio, `video`=:video, `screen`=:screen, `image`=:image, `document`=:document, `true_options`=:true_option, `shuffle`=:shuffle, `ques_val_1`=:ques_val_1, `ques_val_2`=:ques_val_2, `ques_val_3`=:ques_val_3, `ques_val_4`=:ques_val_4, `ques_val_5`=:ques_val_5, `ques_val_6`=:ques_val_6, `critical`=:critical, `status`=:status, `uid`=:user_id, `cur_date`=:cur_date WHERE question_id=:qid";
			$ques_stmt = $db->prepare($ques_sql);
			$ques_stmt->execute($quesData);
			
			#-----Remove-Old-Feedback-------
			$db->feedback_remove($ques_id);
			
			#--------Correct-Feedback----------
			$cfeedData = array('qid'			=> ( ! empty($ques_id)) ? $ques_id : '',
							   'feedback'		=> ( ! empty($mmcq_cfeedback)) ? htmlspecialchars($mmcq_cfeedback) : '',
							   'ftype'			=> 1,
							   'speech_text'	=> ( ! empty($mmcq_feedback_text_to_speech)) ? htmlspecialchars($mmcq_feedback_text_to_speech) : '',
							   'audio'			=> ( ! empty($feed_audio)) ? $feed_audio : '',
							   'video'			=> ( ! empty($feed_video)) ? $feed_video : '',
							   'screen'			=> ( ! empty($mmcq_feedback_rec_screen)) ? $mmcq_feedback_rec_screen : '',
							   'image'			=> ( ! empty($mmcq_feedback_img)) ? $mmcq_feedback_img : '',
							   'document'		=> ( ! empty($mmcq_feedback_doc)) ? $mmcq_feedback_doc : '');
			
			$cfeed_sql = "INSERT INTO `feedback_tbl` (`question_id`, `feedback`, `feedback_type`, `feed_speech_text`, `feed_audio`, `feed_video`, `feed_screen`, `feed_image`, `feed_document`) VALUES (:qid, :feedback, :ftype, :speech_text, :audio, :video, :screen, :image, :document)";
			$cfeed_stmt = $db->prepare($cfeed_sql);
			$cfeed_stmt->execute($cfeedData);
			
			#--------Incorrect-Feedback-------
			$ifeedData = array('qid'			=> ( ! empty($ques_id)) ? $ques_id : '',
							   'feedback'		=> ( ! empty($mmcq_ifeedback)) ? htmlspecialchars($mmcq_ifeedback) : '',
							   'ftype'			=> 2,
							   'speech_text'	=> ( ! empty($mmcqi_feedback_text_to_speech)) ? htmlspecialchars($mmcqi_feedback_text_to_speech) : '',
							   'audio'			=> ( ! empty($ifeed_audio)) ? $ifeed_audio : '',
							   'video'			=> ( ! empty($ifeed_video)) ? $ifeed_video : '',
							   'screen'			=> ( ! empty($mmcqi_feedback_rec_screen)) ? $mmcqi_feedback_rec_screen : '',
							   'image'			=> ( ! empty($mmcqi_feedback_img)) ? $mmcqi_feedback_img : '',
							   'document'		=> ( ! empty($mmcqi_feedback_doc)) ? $mmcqi_feedback_doc : '');
			
			$ifeed_sql = "INSERT INTO `feedback_tbl` (`question_id`, `feedback`, `feedback_type`, `feed_speech_text`, `feed_audio`, `feed_video`, `feed_screen`, `feed_image`, `feed_document`) VALUES (:qid, :feedback, :ftype, :speech_text, :audio, :video, :screen, :image, :document)";
			$ifeed_stmt = $db->prepare($ifeed_sql);
			$ifeed_stmt->execute($ifeedData);
			
			#----------ADD-OPTIONS-------------
			if ( ! empty($mmcq_option[0])):
				$mmcqi = 0; $mmcqtrue = 1;
				foreach ($mmcq_option as $answerData):
					if ( ! empty($updateMMCQAnswerid[$mmcqi])):
						$ansSql = "UPDATE `answer_tbl` SET `choice_option`=:option, `true_option`=:true, `status`=:status, `uid`=:user_id, `cur_date`=:cur_date WHERE answer_id=:aid";
						$stmt = $db->prepare($ansSql);
						$stmt->execute(array('aid'		=> $updateMMCQAnswerid[$mmcqi],
											 'option'	=> ( ! empty($mmcq_option[$mmcqi])) ? htmlspecialchars($mmcq_option[$mmcqi]) : '',
											 'true'		=> ( ! empty($mmcq_true_option[$mmcqi]) && $mmcq_true_option[$mmcqi] == $mmcqtrue) ? 1 : '',
											 'status'	=> 1,
											 'user_id' 	=> $userId,
											 'cur_date'	=> $curdate));
					else:
						$ansSql = "INSERT INTO `answer_tbl` (`question_id`, `choice_option`, `true_option`, `status`, `uid`, `cur_date`) values (:qid, :option, :true, :status, :user_id, :cur_date)";
						$stmt = $db->prepare($ansSql);
						$stmt->execute(array('qid'		=> $ques_id,
											 'option'	=> ( ! empty($mmcq_option[$mmcqi])) ? htmlspecialchars($mmcq_option[$mmcqi]) : '',
											 'true'		=> ( ! empty($mmcq_true_option[$mmcqi]) && $mmcq_true_option[$mmcqi] == $mmcqtrue) ? 1 : '',
											 'status'	=> 1,
											 'user_id' 	=> $userId,
											 'cur_date'	=> $curdate));
					endif;
					$mmcqi++; $mmcqtrue++;
				endforeach;
			endif;
		//-----------END-MMCQ----------
	
		//-----------START-SWIPING----------
		elseif ($question_type == 7):
			#-------QUESTIONS-DATA---
			$qaudio = '';
			if ( ! empty($swipqq_qaudio)):
				$qaudio = $swipqq_qaudio;
			elseif ( ! empty($swipqq_rec_audio)):
				$qaudio = $swipqq_rec_audio;
			endif;

			$audio = '';
			if ( ! empty($swipq_audio)):
				$audio = $swipq_audio;
			elseif ( ! empty($swipq_rec_audio)):
				$audio = $swipq_rec_audio;
			endif;
			
			$video = '';
			if ( ! empty($swipq_video)):
				$video = $swipq_video;
			elseif ( ! empty($swipq_rec_video)):
				$video = $swipq_rec_video;
			endif;
			
			#------Correct-FEEDBACK-DATA---
			$feed_audio = '';
			if ( ! empty($swipq_feedback_audio)):
				$feed_audio = $swipq_feedback_audio;
			elseif ( ! empty($swipq_feedback_rec_audio)):
				$feed_audio = $swipq_feedback_rec_audio;
			endif;
			
			$feed_video = '';
			if ( ! empty($swipq_feedback_video)):
				$feed_video = $swipq_feedback_video;
			elseif ( ! empty($swipq_feedback_rec_video)):
				$feed_video = $swipq_feedback_rec_video;
			endif;
			
			#------Incorrect-FEEDBACK-DATA---
			$ifeed_audio = '';
			if ( ! empty($swipqi_feedback_audio)):
				$ifeed_audio = $swipqi_feedback_audio;
			elseif ( ! empty($swipqi_feedback_rec_audio)):
				$ifeed_audio = $swipqi_feedback_rec_audio;
			endif;
			
			$ifeed_video = '';
			if ( ! empty($swipqi_feedback_video)):
				$ifeed_video = $swipqi_feedback_video;
			elseif ( ! empty($swipqi_feedback_rec_video)):
				$ifeed_video = $swipqi_feedback_rec_video;
			endif;
						
			$quesData = array('questions'		=> ( ! empty($swipq)) ? htmlspecialchars($swipq) : '',
							  'qtype'			=> ( ! empty($question_type)) ? $question_type : '',
							  'qaudio'			=> ( ! empty($qaudio)) ? $qaudio : '',
							  'qspeech_text'	=> ( ! empty($swipqq_text_to_speech)) ? htmlspecialchars($swipqq_text_to_speech) : '',
							  'speech_text'		=> ( ! empty($swipq_text_to_speech)) ? htmlspecialchars($swipq_text_to_speech) : '',
							  'audio'			=> ( ! empty($audio)) ? $audio : '',
							  'video'			=> ( ! empty($video)) ? $video : '',
							  'screen'			=> ( ! empty($swipq_rec_screen)) ? $swipq_rec_screen : '',
							  'image'			=> ( ! empty($swipq_img)) ? $swipq_img : '',
							  'document'		=> ( ! empty($swipq_doc)) ? $swipq_doc : '',
							  'true_option'		=> ( ! empty($swipq_true_option)) ? $swipq_true_option : '',
							  'shuffle'			=> (isset($swipq_shuffle) && ! empty($swipq_shuffle)) ? 1 : 0,
							  'ques_val_1'		=> ( ! empty($swipq_ques_val_1)) ? $swipq_ques_val_1 : '',
							  'ques_val_2'		=> ( ! empty($swipq_ques_val_2)) ? $swipq_ques_val_2 : '',
							  'ques_val_3'		=> ( ! empty($swipq_ques_val_3)) ? $swipq_ques_val_3 : '',
							  'ques_val_4'		=> ( ! empty($swipq_ques_val_4)) ? $swipq_ques_val_4 : '',
							  'ques_val_5'		=> ( ! empty($swipq_ques_val_5)) ? $swipq_ques_val_5 : '',
							  'ques_val_6'		=> ( ! empty($swipq_ques_val_6)) ? $swipq_ques_val_6 : '',
							  'critical'		=> ( ! empty($swip_criticalQ)) ? $swip_criticalQ : '',
							  'status' 			=> 1,
							  'user_id'			=> $userId,
							  'cur_date'		=> $curdate,
							  'qid'				=> $ques_id);
			$ques_sql  = "UPDATE `question_tbl` SET `questions`=:questions, `question_type`=:qtype, `qaudio`=:qaudio, `qspeech_text`=:qspeech_text, `speech_text`=:speech_text, `audio`=:audio, `video`=:video, `screen`=:screen, `image`=:image, `document`=:document, `true_options`=:true_option, `shuffle`=:shuffle, `ques_val_1`=:ques_val_1, `ques_val_2`=:ques_val_2, `ques_val_3`=:ques_val_3, `ques_val_4`=:ques_val_4, `ques_val_5`=:ques_val_5, `ques_val_6`=:ques_val_6, `critical`=:critical, `status`=:status, `uid`=:user_id, `cur_date`=:cur_date WHERE question_id=:qid";
			$ques_stmt = $db->prepare($ques_sql);
			$ques_stmt->execute($quesData);
			
			#-----Remove-Old-Feedback-------
			$db->feedback_remove($ques_id);
			
			#--------Correct-Feedback----------
			$cfeedData = array('qid'			=> ( ! empty($ques_id)) ? $ques_id : '',
							   'feedback'		=> ( ! empty($swipq_cfeedback)) ? htmlspecialchars($swipq_cfeedback) : '',
							   'ftype'			=> 1,
							   'speech_text'	=> ( ! empty($swipq_feedback_text_to_speech)) ? htmlspecialchars($swipq_feedback_text_to_speech) : '',
							   'audio'			=> ( ! empty($feed_audio)) ? $feed_audio : '',
							   'video'			=> ( ! empty($feed_video)) ? $feed_video : '',
							   'screen'			=> ( ! empty($swipq_feedback_rec_screen)) ? $swipq_feedback_rec_screen : '',
							   'image'			=> ( ! empty($swipq_feedback_img)) ? $swipq_feedback_img : '',
							   'document'		=> ( ! empty($swipq_feedback_doc)) ? $swipq_feedback_doc : '');
			
			$cfeed_sql  = "INSERT INTO `feedback_tbl` (`question_id`, `feedback`, `feedback_type`, `feed_speech_text`, `feed_audio`, `feed_video`, `feed_screen`, `feed_image`, `feed_document`) VALUES (:qid, :feedback, :ftype, :speech_text, :audio, :video, :screen, :image, :document)";
			$cfeed_stmt = $db->prepare($cfeed_sql);
			$cfeed_stmt->execute($cfeedData);
			
			#--------Incorrect-Feedback-------
			$ifeedData = array('qid'			=> ( ! empty($ques_id)) ? $ques_id : '',
							   'feedback'		=> ( ! empty($swipq_ifeedback)) ? htmlspecialchars($swipq_ifeedback) : '',
							   'ftype'			=> 2,
							   'speech_text'	=> ( ! empty($swipqi_feedback_text_to_speech)) ? htmlspecialchars($swipqi_feedback_text_to_speech) : '',
							   'audio'			=> ( ! empty($ifeed_audio)) ? $ifeed_audio : '',
							   'video'			=> ( ! empty($ifeed_video)) ? $ifeed_video : '',
							   'screen'			=> ( ! empty($swipqi_feedback_rec_screen)) ? $swipqi_feedback_rec_screen : '',
							   'image'			=> ( ! empty($swipqi_feedback_img)) ? $swipqi_feedback_img : '',
							   'document'		=> ( ! empty($swipqi_feedback_doc)) ? $swipqi_feedback_doc : '');
			
			$ifeed_sql  = "INSERT INTO `feedback_tbl` (`question_id`, `feedback`, `feedback_type`, `feed_speech_text`, `feed_audio`, `feed_video`, `feed_screen`, `feed_image`, `feed_document`) VALUES (:qid, :feedback, :ftype, :speech_text, :audio, :video, :screen, :image, :document)";
			$ifeed_stmt = $db->prepare($ifeed_sql);
			$ifeed_stmt->execute($ifeedData);
			
			#----------ADD-OPTIONS-------------
			$swipqi = 0; $swiptrue = 1;
			foreach ($swipq_option as $answerData):
				if ( ! empty($updateSwipqAnswerid[$swipqi])):
					$ansSql = "UPDATE `answer_tbl` SET `choice_option`=:option, `true_option`=:true, `image`=:image, `status`=:status, `uid`=:user_id, `cur_date`=:cur_date WHERE answer_id=:aid";
					$stmt	= $db->prepare($ansSql);
					$stmt->execute(array('aid' 		=> $updateSwipqAnswerid[$swipqi],
										 'option'	=> ( ! empty($swipq_option[$swipqi])) ? htmlspecialchars($swipq_option[$swipqi]) : '',
										 'image'	=> ( ! empty($swipq_item_img[$swipqi])) ? $swipq_item_img[$swipqi] : '',
										 'true'		=> ( ! empty($swipq_true_option) && $swipq_true_option == $swiptrue) ? 1 : '',
										 'status'	=> 1,
										 'user_id' 	=> $userId,
										 'cur_date'	=> $curdate));
				else:
					$ansSql = "INSERT INTO `answer_tbl` (`question_id`, `choice_option`, `true_option`, `image`, `status`, `uid`, `cur_date`) VALUES (:qid, :option, :true, :image, :status, :user_id, :cur_date)";
					$stmt = $db->prepare($ansSql);
					$stmt->execute(array('qid'		=> $ques_id,
										 'option'	=> ( ! empty($swipq_option[$swipqi])) ? htmlspecialchars($swipq_option[$swipqi]) : '',
										 'image'	=> ( ! empty($swipq_item_img[$swipqi])) ? $swipq_item_img[$swipqi] : '',
										 'true'		=> ( ! empty($swipq_true_option) && $swipq_true_option == $swiptrue) ? 1 : '',
										 'status'	=> 1,
										 'user_id' 	=> $userId,
										 'cur_date'	=> $curdate));
				endif;
				$swipqi++; $swiptrue++;
			endforeach;
		//-----------END-SWIPING----------
		
		//-----------START-DDQ----------
		elseif ($question_type == 8):
			#-------QUESTIONS-DATA---
			$qaudio = '';
			if ( ! empty($ddqq_qaudio)):
				$qaudio = $ddqq_qaudio;
			elseif ( ! empty($ddqq_rec_audio)):
				$qaudio = $ddqq_rec_audio;
			endif;
		
			$audio = '';
			if ( ! empty($ddq_audio)):
				$audio = $ddq_audio;
			elseif ( ! empty($ddq_rec_audio)):
				$audio = $ddq_rec_audio;
			endif;
			
			$video = '';
			if ( ! empty($ddq_video)):
				$video = $ddq_video;
			elseif ( ! empty($ddq_rec_video)):
				$video = $ddq_rec_video;
			endif;
			
			#------Correct-FEEDBACK-DATA---
			$feed_audio = '';
			if ( ! empty($ddq_feedback_audio)):
				$feed_audio = $ddq_feedback_audio;
			elseif ( ! empty($ddq_feedback_rec_audio)):
				$feed_audio = $ddq_feedback_rec_audio;
			endif;
			
			$feed_video = '';
			if ( ! empty($ddq_feedback_video)):
				$feed_video = $ddq_feedback_video;
			elseif ( ! empty($ddq_feedback_rec_video)):
				$feed_video = $ddq_feedback_rec_video;
			endif;
			
			#------Incorrect-FEEDBACK-DATA---
			$ifeed_audio = '';
			if ( ! empty($ddqi_feedback_audio)):
				$ifeed_audio = $ddqi_feedback_audio;
			elseif ( ! empty($ddqi_feedback_rec_audio)):
				$ifeed_audio = $ddqi_feedback_rec_audio;
			endif;
			
			$ifeed_video = '';
			if ( ! empty($ddqi_feedback_video)):
				$ifeed_video = $ddqi_feedback_video;
			elseif ( ! empty($ddqi_feedback_rec_video)):
				$ifeed_video = $ddqi_feedback_rec_video;
			endif;
			
			#--For-Multi-DROP-Options----
			if ($seleted_drag_option == 1):
				$quesData = array('questions'		=> ( ! empty($ddq)) ? htmlspecialchars($ddq) : '',
								  'seleted_drag' 	=> ( ! empty($seleted_drag_option)) ? $seleted_drag_option : '',
								  'qtype'			=> ( ! empty($question_type)) ? $question_type : '',
								  'qaudio'			=> ( ! empty($qaudio)) ? $qaudio : '',
								  'qspeech_text'	=> ( ! empty($ddqq_text_to_speech)) ? htmlspecialchars($ddqq_text_to_speech) : '',
								  'speech_text'		=> ( ! empty($ddq_text_to_speech)) ? htmlspecialchars($ddq_text_to_speech) : '',
								  'audio'			=> ( ! empty($audio)) ? $audio : '',
								  'video'			=> ( ! empty($video)) ? $video : '',
								  'screen'			=> ( ! empty($ddq_rec_screen)) ? $ddq_rec_screen : '',
								  'image'			=> ( ! empty($ddq_img)) ? $ddq_img : '',
								  'document'		=> ( ! empty($ddq_doc)) ? $ddq_doc : '',
								  'shuffle'			=> (isset($ddq_shuffle1) && ! empty($ddq_shuffle1)) ? 1 : 0,
								  'ques_val_1'		=> ( ! empty($ddq_ques_val_1)) ? $ddq_ques_val_1 : '',
								  'ques_val_2'		=> ( ! empty($ddq_ques_val_2)) ? $ddq_ques_val_2 : '',
								  'ques_val_3'		=> ( ! empty($ddq_ques_val_3)) ? $ddq_ques_val_3 : '',
								  'ques_val_4'		=> ( ! empty($ddq_ques_val_4)) ? $ddq_ques_val_4 : '',
								  'ques_val_5'		=> ( ! empty($ddq_ques_val_5)) ? $ddq_ques_val_5 : '',
								  'ques_val_6'		=> ( ! empty($ddq_ques_val_6)) ? $ddq_ques_val_6 : '',
								  'critical'		=> ( ! empty($ddq_criticalQ)) ? $ddq_criticalQ : '',
								  'status' 			=> 1,
								  'user_id'			=> $userId,
								  'cur_date'		=> $curdate,
								  'qid'				=> $ques_id);
				$ques_sql  = "UPDATE `question_tbl` SET `questions`=:questions, `question_type`=:qtype, `seleted_drag_option`=:seleted_drag, `qaudio`=:qaudio, `qspeech_text`=:qspeech_text, `speech_text`=:speech_text, `audio`=:audio, `video`=:video, `screen`=:screen, `image`=:image, `document`=:document, `shuffle`=:shuffle, `ques_val_1`=:ques_val_1, `ques_val_2`=:ques_val_2, `ques_val_3`=:ques_val_3, `ques_val_4`=:ques_val_4, `ques_val_5`=:ques_val_5, `ques_val_6`=:ques_val_6, `critical`=:critical, `status`=:status, `uid`=:user_id, `cur_date`=:cur_date WHERE question_id=:qid";
				$ques_stmt = $db->prepare($ques_sql);
				$ques_stmt->execute($quesData);

				#------OPTIONS------
				$ddqi = 0;
				foreach ($ddq_drop as $answerData):
					if ( ! empty($updateDDqAnswerid[$ddqi])):
						$ansSql = "UPDATE `answer_tbl` SET `choice_option`=:choice, `match_option`=:match, `image`=:image, `image2`=:image2, `status`=:status, `uid`=:user_id, `cur_date`=:cur_date WHERE answer_id=:aid";
						$stmt = $db->prepare($ansSql);
						$stmt->execute(array('aid' 		=> $updateDDqAnswerid[$ddqi],
											 'choice'	=> ( ! empty($ddq_drop[$ddqi])) ? htmlspecialchars($ddq_drop[$ddqi]) : '',
											 'match'	=> ( ! empty($ddq_drag[$ddqi])) ? htmlspecialchars($ddq_drag[$ddqi]) : '',
											 'image'	=> ( ! empty($ddq_item_img[$ddqi])) ? $ddq_item_img[$ddqi] : '',
											 'image2'	=> ( ! empty($ddq_drag_item_img[$ddqi])) ? $ddq_drag_item_img[$ddqi] : '',
											 'status'	=> 1,
											 'user_id' 	=> $userId,
											 'cur_date'	=> $curdate));
					else:
						$ansSql = "INSERT INTO `answer_tbl` (`question_id`, `choice_option`, `match_option`, `image`, `image2`, `status`, `uid`, `cur_date`) VALUES (:qid, :choice, :match, :image, :image2, :status, :user_id, :cur_date)";
						$stmt = $db->prepare($ansSql);
						$stmt->execute(array('qid'		=> $ques_id,
											 'choice'	=> ( ! empty($ddq_drop[$ddqi])) ? htmlspecialchars($ddq_drop[$ddqi]) : '',
											 'match'	=> ( ! empty($ddq_drag[$ddqi])) ? htmlspecialchars($ddq_drag[$ddqi]) : '',
											 'image'	=> ( ! empty($ddq_item_img[$ddqi])) ? $ddq_item_img[$ddqi] : '',
											 'image2'	=> ( ! empty($ddq_drag_item_img[$ddqi])) ? $ddq_drag_item_img[$ddqi] : '',
											 'status'	=> 1,
											 'user_id' 	=> $userId,
											 'cur_date'	=> $curdate));
					endif;
					$ddqi++;
				endforeach;
				
				#-----Remove-Old-Feedback-------
				$db->feedback_remove($ques_id);

				#--------Correct-Feedback-------
				$cfeedData = array('qid'			=> ( ! empty($ques_id)) ? $ques_id : '',
								   'feedback'		=> ( ! empty($ddq_cfeedback)) ? htmlspecialchars($ddq_cfeedback) : '',
								   'ftype'			=> 1,
								   'speech_text'	=> ( ! empty($ddq_feedback_text_to_speech)) ? htmlspecialchars($ddq_feedback_text_to_speech) : '',
								   'audio'			=> ( ! empty($feed_audio)) ? $feed_audio : '',
								   'video'			=> ( ! empty($feed_video)) ? $feed_video : '',
								   'screen'			=> ( ! empty($ddq_feedback_rec_screen)) ? $ddq_feedback_rec_screen : '',
								   'image'			=> ( ! empty($ddq_feedback_img)) ? $ddq_feedback_img : '',
								   'document'		=> ( ! empty($ddq_feedback_doc)) ? $ddq_feedback_doc : '');
				$cfeed_sql  = "INSERT INTO `feedback_tbl` (`question_id`, `feedback`, `feedback_type`, `feed_speech_text`, `feed_audio`, `feed_video`, `feed_screen`, `feed_image`, `feed_document`) VALUES (:qid, :feedback, :ftype, :speech_text, :audio, :video, :screen, :image, :document)";
				$cfeed_stmt = $db->prepare($cfeed_sql);
				$cfeed_stmt->execute($cfeedData);

				#--------Incorrect-Feedback-------
				$ifeedData = array('qid'			=> ( ! empty($ques_id)) ? $ques_id : '',
								   'feedback'		=> ( ! empty($ddq_ifeedback)) ? htmlspecialchars($ddq_ifeedback) : '',
								   'ftype'			=> 2,
								   'speech_text'	=> ( ! empty($ddqi_feedback_text_to_speech)) ? htmlspecialchars($ddqi_feedback_text_to_speech) : '',
								   'audio'			=> ( ! empty($ifeed_audio)) ? $ifeed_audio : '',
								   'video'			=> ( ! empty($ifeed_video)) ? $ifeed_video : '',
								   'screen'			=> ( ! empty($ddqi_feedback_rec_screen)) ? $ddqi_feedback_rec_screen : '',
								   'image'			=> ( ! empty($ddqi_feedback_img)) ? $ddqi_feedback_img : '',
								   'document'		=> ( ! empty($ddqi_feedback_doc)) ? $ddqi_feedback_doc : '');

				$ifeed_sql  = "INSERT INTO `feedback_tbl` (`question_id`, `feedback`, `feedback_type`, `feed_speech_text`, `feed_audio`, `feed_video`, `feed_screen`, `feed_image`, `feed_document`) VALUES (:qid, :feedback, :ftype, :speech_text, :audio, :video, :screen, :image, :document)";
				$ifeed_stmt = $db->prepare($ifeed_sql);
				$ifeed_stmt->execute($ifeedData);
			elseif ($seleted_drag_option == 2):
				$quesData = array('questions'		=> ( ! empty($ddq)) ? htmlspecialchars($ddq) : '',
								  'seleted_drag' 	=> ( ! empty($seleted_drag_option)) ? $seleted_drag_option : '',
								  'qtype'			=> ( ! empty($question_type)) ? $question_type : '',
								  'qaudio'			=> ( ! empty($qaudio)) ? $qaudio : '',
								  'qspeech_text'	=> ( ! empty($ddqq_text_to_speech)) ? $ddqq_text_to_speech : '',
								  'speech_text'		=> ( ! empty($ddq_text_to_speech)) ? $ddq_text_to_speech : '',
								  'audio'			=> ( ! empty($audio)) ? $audio : '',
								  'video'			=> ( ! empty($video)) ? $video : '',
								  'screen'			=> ( ! empty($ddq_rec_screen)) ? $ddq_rec_screen : '',
								  'image'			=> ( ! empty($ddq_img)) ? $ddq_img : '',
								  'document'		=> ( ! empty($ddq_doc)) ? $ddq_doc : '',
								  'shuffle'			=> (isset($ddq_shuffle2) && ! empty($ddq_shuffle2)) ? 1 : 0,
								  'true_options'	=> ( ! empty($ddq_true_option)) ? $ddq_true_option : '',
								  'ques_val_1'		=> ( ! empty($ddq_ques_val_1)) ? $ddq_ques_val_1 : '',
								  'ques_val_2'		=> ( ! empty($ddq_ques_val_2)) ? $ddq_ques_val_2 : '',
								  'ques_val_3'		=> ( ! empty($ddq_ques_val_3)) ? $ddq_ques_val_3 : '',
								  'ques_val_4'		=> ( ! empty($ddq_ques_val_4)) ? $ddq_ques_val_4 : '',
								  'ques_val_5'		=> ( ! empty($ddq_ques_val_5)) ? $ddq_ques_val_5 : '',
								  'ques_val_6'		=> ( ! empty($ddq_ques_val_6)) ? $ddq_ques_val_6 : '',
								  'critical'		=> ( ! empty($ddq_criticalQ)) ? $ddq_criticalQ : '',
								  'status' 			=> 1,
								  'user_id'			=> $userId,
								  'cur_date'		=> $curdate,
								  'qid'				=> $ques_id);
				$ques_sql  = "UPDATE `question_tbl` SET `questions`=:questions, `question_type`=:qtype, `seleted_drag_option`=:seleted_drag, `qaudio`=:qaudio, `qspeech_text`=:qspeech_text, `speech_text`=:speech_text, `audio`=:audio, `video`=:video, `screen`=:screen, `image`=:image, `document`=:document, `shuffle`=:shuffle, `true_options`=:true_options, `ques_val_1`=:ques_val_1, `ques_val_2`=:ques_val_2, `ques_val_3`=:ques_val_3, `ques_val_4`=:ques_val_4, `ques_val_5`=:ques_val_5, `ques_val_6`=:ques_val_6, `critical`=:critical, `status`=:status, `uid`=:user_id, `cur_date`=:cur_date WHERE question_id=:qid";
				$ques_stmt = $db->prepare($ques_sql);
				$ques_stmt->execute($quesData);
				
				#------OPTION------
				if ( ! empty($updateDDq2Answerid)):
					#----DROP-TARGET----
					$ansSql = "UPDATE `sub_options_tbl` SET `sub_option`=:option, `image`=:image WHERE sub_options_id=:sid";
					$stmt = $db->prepare($ansSql);
					$stmt->execute(array('sid' 		=> $updateDDq2Answerid,
										 'option'	=> ( ! empty($ddq2_drop)) ? htmlspecialchars($ddq2_drop) : '',
										 'image' 	=> ( ! empty($ddq2_item_img)) ? $ddq2_item_img : ''));
					#-----DRAG-ITEMS------
					$ddq2iu = 0; $ddq2true = 1;
					foreach ($ddq2_drag as $subAnswerData):
						if ( ! empty($updateDDq2SubAnswerid[$ddq2iu])):
							$getOpId	   = $updateDDq2SubAnswerid[$ddq2iu];
							$updateAnsSql  = "UPDATE `answer_tbl` SET `choice_option`=:option, `image`=:image, `true_option`=:true, `status`=:status, `uid`=:user_id, `cur_date`=:cur_date WHERE answer_id=:aid";
							$updateAnsstmt = $db->prepare($updateAnsSql);
							$updateAnsstmt->execute(array('aid' 		=> $getOpId,
														  'option'		=> ( ! empty($ddq2_drag[$ddq2iu])) ? htmlspecialchars($ddq2_drag[$ddq2iu]) : '',
														  'image'		=> ( ! empty($ddq2_drag_item_img[$ddq2iu])) ? $ddq2_drag_item_img[$ddq2iu] : '',
														  'true'		=> ( ! empty($ddq_true_option) && $ddq_true_option == $ddq2true) ? 1 : '',
														  'status'		=> 1,
														  'user_id' 	=> $userId,
														  'cur_date'	=> $curdate));
						else:
							$ansSql = "INSERT INTO `answer_tbl` (`question_id`, `choice_option`, `image`, `true_option`, `status`, `uid`, `cur_date`) VALUES (:qid, :option, :image, :true, :status, :user_id, :cur_date)";
							$stmt 	= $db->prepare($ansSql);
							$stmt->execute(array('qid'		=> $ques_id,
												 'option'	=> ( ! empty($ddq2_drag[$ddq2iu])) ? htmlspecialchars($ddq2_drag[$ddq2iu]) : '',
												 'image'	=> ( ! empty($ddq2_drag_item_img[$ddq2iu])) ? $ddq2_drag_item_img[$ddq2iu] : '',
												 'true'		=> ( ! empty($ddq_true_option) && $ddq_true_option == $ddq2true) ? 1 : '',
												 'status'	=> 1,
												 'user_id' 	=> $userId,
												 'cur_date'	=> $curdate));
						endif;
						$ddq2iu++; $ddq2true++;
					endforeach;
				else:
					#----DROP-TARGET----
					$ansSql = "INSERT INTO `sub_options_tbl` (`question_id`, `sub_option`, `image`) VALUES (:qid, :option, :image)";
					$stmt   = $db->prepare($ansSql);
					$stmt->execute(array('qid' 		=> $ques_id,
										 'option'	=> ( ! empty($ddq2_drop)) ? htmlspecialchars($ddq2_drop) : '',
										 'image' 	=> ( ! empty($ddq2_item_img)) ? $ddq2_item_img : ''));
					#-----DRAG-ITEMS------
					$ddq2i = 0; $ddq2itrue = 1;
					foreach ($ddq2_drag as $answerData):
						$ansSql = "INSERT INTO `answer_tbl` (`question_id`, `choice_option`, `image`, `true_option`, `status`, `uid`, `cur_date`) VALUES (:qid, :choice, :image, :true, :status, :user_id, :cur_date)";
						$stmt = $db->prepare($ansSql);
						$stmt->execute(array('qid'		=> $ques_id,
											 'choice'	=> ( ! empty($ddq2_drag[$ddq2i])) ? htmlspecialchars($ddq2_drag[$ddq2i]) : '',
											 'image'	=> ( ! empty($ddq2_drag_item_img[$ddq2i])) ? $ddq2_drag_item_img[$ddq2i] : '',
											 'true'		=> ( ! empty($ddq_true_option) && $ddq_true_option == $ddq2itrue) ? 1 : '',
											 'status'	=> 1,
											 'user_id' 	=> $userId,
											 'cur_date'	=> $curdate));
						$ddq2i++; $ddq2itrue++;
					endforeach;
				endif;

				#-----Remove-Old-Feedback-------
				$db->feedback_remove($ques_id);

				#--------Correct-Feedback-------
				$cfeedData = array('qid'			=> ( ! empty($ques_id)) ? $ques_id : '',
								   'feedback'		=> ( ! empty($ddq_cfeedback)) ? htmlspecialchars($ddq_cfeedback) : '',
								   'ftype'			=> 1,
								   'speech_text'	=> ( ! empty($ddq_feedback_text_to_speech)) ? htmlspecialchars($ddq_feedback_text_to_speech) : '',
								   'audio'			=> ( ! empty($feed_audio)) ? $feed_audio : '',
								   'video'			=> ( ! empty($feed_video)) ? $feed_video : '',
								   'screen'			=> ( ! empty($ddq_feedback_rec_screen)) ? $ddq_feedback_rec_screen : '',
								   'image'			=> ( ! empty($ddq_feedback_img)) ? $ddq_feedback_img : '',
								   'document'		=> ( ! empty($ddq_feedback_doc)) ? $ddq_feedback_doc : '');
				$cfeed_sql  = "INSERT INTO `feedback_tbl` (`question_id`, `feedback`, `feedback_type`, `feed_speech_text`, `feed_audio`, `feed_video`, `feed_screen`, `feed_image`, `feed_document`) VALUES (:qid, :feedback, :ftype, :speech_text, :audio, :video, :screen, :image, :document)";
				$cfeed_stmt = $db->prepare($cfeed_sql);
				$cfeed_stmt->execute($cfeedData);

				#--------Incorrect-Feedback-------
				$ifeedData = array('qid'			=> ( ! empty($ques_id)) ? $ques_id : '',
								   'feedback'		=> ( ! empty($ddq_ifeedback)) ? htmlspecialchars($ddq_ifeedback) : '',
								   'ftype'			=> 2,
								   'speech_text'	=> ( ! empty($ddqi_feedback_text_to_speech)) ? htmlspecialchars($ddqi_feedback_text_to_speech) : '',
								   'audio'			=> ( ! empty($ifeed_audio)) ? $ifeed_audio : '',
								   'video'			=> ( ! empty($ifeed_video)) ? $ifeed_video : '',
								   'screen'			=> ( ! empty($ddqi_feedback_rec_screen)) ? $ddqi_feedback_rec_screen : '',
								   'image'			=> ( ! empty($ddqi_feedback_img)) ? $ddqi_feedback_img : '',
								   'document'		=> ( ! empty($ddqi_feedback_doc)) ? $ddqi_feedback_doc : '');
				$ifeed_sql  = "INSERT INTO `feedback_tbl` (`question_id`, `feedback`, `feedback_type`, `feed_speech_text`, `feed_audio`, `feed_video`, `feed_screen`, `feed_image`, `feed_document`) VALUES (:qid, :feedback, :ftype, :speech_text, :audio, :video, :screen, :image, :document)";
				$ifeed_stmt = $db->prepare($ifeed_sql);
				$ifeed_stmt->execute($ifeedData);
			endif;
		endif;
	endif;
	if ( ! empty($submit_type) && $submit_type == 1):
		$resdata = array('success' => TRUE, 'all_update' => FALSE, 'msg' => 'Simulation details updated successfully.', 'return_url' => 'linear-multiple-template.php?add_data=true&sim_id='.md5($sim_id).'&ques_id='.base64_encode($ques_id));
	else:
		$resdata = array('success' => TRUE, 'all_update' => TRUE, 'msg' => 'All questions and answers updated successfully.', 'return_url' => 'author-simulation.php');
	endif;
	echo json_encode($resdata);
endif;

#-----------------------------------------Add-Update-Linear-video-Milti-Sim ----------------------------------
if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['update_linear_video_multi_sim']) && isset($_POST['sim_id'])):
	extract($_POST);
	$error = FALSE;
	if (empty($sim_page_name[0]) || empty($sim_duration) || empty($sim_title) || empty($passing_marks)):
		$resdata = array('success' => FALSE, 'msg' => 'Please fill all the mandatory fields on add Simulation and Simulation details to proceed.');
		$error = TRUE;
		echo json_encode($resdata); exit;
	elseif (isset($competency_name_1) && empty($competency_name_1) || empty($competency_score_1) || empty($weightage_1)):
		$resdata = array('success' => FALSE, 'msg' => 'Please fill all the mandatory fields on Simulation details to proceed.');
		$error = TRUE;
		echo json_encode($resdata); exit;
	elseif ($error == FALSE):
		$sql = "UPDATE scenario_master SET `Scenario_title` = '". $sim_title ."', `duration` = '". $sim_duration ."', `passing_marks` = '". $passing_marks ."', `sim_cover_img` = '". $scenario_cover_file ."'";		
		#--Add-Web-Object--
		if ( ! empty($web_object)):
			$sql .= ", web_object = '". $web_object ."', web_object_title = '". htmlspecialchars($web_object_title) ."'";
		endif;
		#--Sim-Media-File--
		if ( ! empty($scenario_media_file) && in_array(strtolower(pathinfo($scenario_media_file, PATHINFO_EXTENSION)), $img_valid_exts)):
			$sql .= ", image_url = '". $scenario_media_file ."', video_url = NULL, audio_url = NULL";
		elseif ( ! empty($scenario_media_file) && in_array(strtolower(pathinfo($scenario_media_file, PATHINFO_EXTENSION)), $video_valid_exts)):
			$sql .= ", video_url = '". $scenario_media_file ."', image_url = NULL, audio_url = NULL";
		elseif ( ! empty($scenario_media_file) && in_array(strtolower(pathinfo($scenario_media_file, PATHINFO_EXTENSION)), $audio_valid_exts)):
			$sql .= ", audio_url = '". $scenario_media_file ."', video_url = NULL, image_url = NULL";
		elseif ( ! empty($scenario_media_file) && ! in_array(strtolower(pathinfo($scenario_media_file, PATHINFO_EXTENSION)), $audio_valid_exts) && 
				! in_array(strtolower(pathinfo($scenario_media_file, PATHINFO_EXTENSION)), $img_valid_exts) && 
				! in_array(strtolower(pathinfo($scenario_media_file, PATHINFO_EXTENSION)), $video_valid_exts)):
			$sql .= ", web_object_file = '". $scenario_media_file ."', video_url = NULL, image_url = NULL, audio_url = NULL";
		endif;
		$sql .= " WHERE scenario_id = '". $sim_id ."'";
		$results = $db->prepare($sql);
		if ($results->execute()):
			#----Competency----
			$delCom	 = "DELETE FROM competency_tbl WHERE scenario_id = '". $sim_id ."'";
			$delComq = $db->prepare($delCom); $delComq->execute();
			$data	 = array('scenario_id'	=> $sim_id,
							 'comp_col_1'	=> (isset($competency_name_1) && ! empty($competency_name_1)) ? addslashes($competency_name_1) : '',
							 'comp_val_1'	=> (isset($competency_score_1) && ! empty($competency_score_1)) ? addslashes($competency_score_1) : '',
							 'comp_col_2'	=> (isset($competency_name_2) && ! empty($competency_name_2)) ? addslashes($competency_name_2) : '',
							 'comp_val_2'	=> (isset($competency_score_2) && ! empty($competency_score_2)) ? $competency_score_2 : '',
							 'comp_col_3'	=> (isset($competency_name_3) && ! empty($competency_name_3)) ? addslashes($competency_name_3) : '',
							 'comp_val_3'	=> (isset($competency_score_3) && ! empty($competency_score_3)) ? $competency_score_3 : '',
							 'comp_col_4'	=> (isset($competency_name_4) && ! empty($competency_name_4)) ? addslashes($competency_name_4) : '',
							 'comp_val_4'	=> (isset($competency_score_4) && ! empty($competency_score_4)) ? $competency_score_4 : '',
							 'comp_col_5'	=> (isset($competency_name_5) && ! empty($competency_name_5)) ? addslashes($competency_name_5) : '',
							 'comp_val_5'	=> (isset($competency_score_5) && ! empty($competency_score_5)) ? $competency_score_5 : '',
							 'comp_col_6'	=> (isset($competency_name_6) && ! empty($competency_name_6)) ? addslashes($competency_name_6) : '',
							 'comp_val_6'	=> (isset($competency_score_6) && ! empty($competency_score_6)) ? $competency_score_6 : '',
							 'weightage_1'	=> (isset($weightage_1) && ! empty($weightage_1)) ? $weightage_1 : '',
							 'weightage_2'	=> (isset($weightage_2) && ! empty($weightage_2)) ? $weightage_2 : '',
							 'weightage_3'	=> (isset($weightage_3) && ! empty($weightage_3)) ? $weightage_3 : '',
							 'weightage_4'	=> (isset($weightage_4) && ! empty($weightage_4)) ? $weightage_4 : '',
							 'weightage_5'	=> (isset($weightage_5) && ! empty($weightage_5)) ? $weightage_5 : '',
							 'weightage_6'	=> (isset($weightage_6) && ! empty($weightage_6)) ? $weightage_6 : '',
							 'status' 		=> 1,
							 'user_id' 		=> $userId,
							 'cur_date'		=> $curdate);
			$com_sql  = "INSERT INTO `competency_tbl` (`scenario_id`, `comp_col_1`, `comp_val_1`, `comp_col_2`, `comp_val_2`, `comp_col_3`, `comp_val_3`, `comp_col_4`, `comp_val_4`, `comp_col_5`, `comp_val_5`, `comp_col_6`, `comp_val_6`, `weightage_1`, `weightage_2`, `weightage_3`, `weightage_4`, `weightage_5`, `weightage_6`, `status`, `uid`, `cur_date`) VALUES (:scenario_id, :comp_col_1, :comp_val_1, :comp_col_2, :comp_val_2, :comp_col_3, :comp_val_3, :comp_col_4, :comp_val_4, :comp_col_5, :comp_val_5, :comp_col_6, :comp_val_6, :weightage_1, :weightage_2, :weightage_3, :weightage_4, :weightage_5, :weightage_6, :status, :user_id, :cur_date)";
			$com_stmt = $db->prepare($com_sql);
			$com_stmt->execute($data);
			
			#----PAGES----
			$delPage  = "DELETE FROM sim_pages WHERE scenario_id = '". $sim_id ."'";
			$delPageq = $db->prepare($delPage); $delPageq->execute();
			if ( ! empty($sim_page_name)): $sp = 0;
				foreach ($sim_page_name as $pdata):
					$pageq 		= "INSERT INTO `sim_pages` (`scenario_id`, `sim_page_name`, `sim_page_desc`, `userid`, `status`, `added_date`) VALUES (:sid, :pname, :pdesc, :user_id, :status, :adate)";
					$page_stmt	= $db->prepare($pageq);
					$page_stmt->execute(array('sid'		=> $sim_id,
											  'pname'	=> ( ! empty($pdata)) ? htmlspecialchars($pdata) : '',
											  'pdesc'	=> ( ! empty($sim_page_desc[$sp])) ? $sim_page_desc[$sp] : '',
											  'status' 	=> 1,
											  'user_id'	=> $userId,
											  'adate'	=> $curdate));
				$sp++; endforeach;
			endif;
			
			#----Branding----
			$bandq	  	= "DELETE FROM sim_branding_tbl WHERE scenario_id = '". $sim_id ."'";
			$bandqexe 	= $db->prepare($bandq); $bandqexe->execute();
			$brand_data = array('sid'				=> $sim_id,
								'bg'				=> ( ! empty($ques_bg_color)) ? $ques_bg_color : '',
								'bg_tr'				=> ( ! empty($ques_bg_transp)) ? $ques_bg_transp : '',
								'obg'				=> ( ! empty($ques_option_bg_color)) ? $ques_option_bg_color : '',
								'obg_tr'			=> ( ! empty($ques_option_bg_transp)) ? $ques_option_bg_transp : '',
								'ohover'			=> ( ! empty($ques_option_hover_color)) ? $ques_option_hover_color : '',
								'oh_tr'				=> ( ! empty($ques_option_hover_transp)) ? $ques_option_hover_transp : '',
								'oselected'			=> ( ! empty($ques_option_select_color)) ? $ques_option_select_color : '',
								'oselected_tr'		=> ( ! empty($ques_option_select_transp)) ? $ques_option_select_transp : '',
								'btn_bg'			=> ( ! empty($btn_bg_color)) ? $btn_bg_color : '',
								'btn_bg_tr'			=> ( ! empty($btn_bg_transp)) ? $btn_bg_transp : '',
								'btn_hover'			=> ( ! empty($btn_hover_color)) ? $btn_hover_color : '',
								'btn_hover_tr'		=> ( ! empty($btn_hover_transp)) ? $btn_hover_transp : '',
								'btn_selected'		=> ( ! empty($btn_select_color)) ? $btn_select_color : '',
								'btn_selected_tr'	=> ( ! empty($btn_select_transp)) ? $btn_select_transp : '',
								'font_type'			=> ( ! empty($font_type)) ? $font_type : '',
								'font_color'		=> ( ! empty($font_color)) ? $font_color : '',
								'font_size'			=> ( ! empty($font_size)) ? $font_size : '',
								'user_id' 			=> $userId,
								'added_date'		=> $curdate);
			$com_sql  = "INSERT INTO `sim_branding_tbl` (`scenario_id`, `ques_bg`, `ques_bg_transparency`, `option_bg`, `option_bg_transparency`, `option_hover`, `option_hover_transparency`, `option_selected`, `option_selected_transparency`, `btn_bg`, `btn_bg_transparency`, `btn_hover`, `btn_hover_transparency`, `btn_selected`, `btn_selected_transparency`, `font_type`, `font_color`, `font_size`, `user_id`, `added_date`) 
						 VALUES (:sid, :bg, :bg_tr, :obg, :obg_tr, :ohover, :oh_tr, :oselected, :oselected_tr, :btn_bg, :btn_bg_tr, :btn_hover, :btn_hover_tr, :btn_selected, :btn_selected_tr, :font_type, :font_color, :font_size, :user_id, :added_date)";
			$com_stmt = $db->prepare($com_sql); $com_stmt->execute($brand_data);
		endif;
		//------START-MATCH-THE-FOLLOWING-----
		if ($question_type == 1):
			#-------QUESTIONS-DATA---
			$qaudio = '';
			if ( ! empty($mtfq_qaudio)):
				$qaudio = $mtfq_qaudio;
			elseif ( ! empty($mtfq_rec_audio)):
				$qaudio = $mtfq_rec_audio;
			endif;
			
			$audio = '';
			if ( ! empty($mtf_qaudio)):
				$audio = $mtf_qaudio;
			elseif ( ! empty($mtf_rec_audio)):
				$audio = $mtf_rec_audio;
			endif;
			
			$video = '';
			if ( ! empty($mtf_video)):
				$video = $mtf_video;
			elseif ( ! empty($mtf_rec_video)):
				$video = $mtf_rec_video;
			endif;
			
			#------Correct-FEEDBACK-DATA---
			$feed_audio = '';
			if ( ! empty($mtf_feedback_audio)):
				$feed_audio = $mtf_feedback_audio;
			elseif ( ! empty($mtf_feedback_rec_audio)):
				$feed_audio = $mtf_feedback_rec_audio;
			endif;
			
			$feed_video = '';
			if ( ! empty($mtf_feedback_video)):
				$feed_video = $mtf_feedback_video;
			elseif ( ! empty($mtf_feedback_rec_video)):
				$feed_video = $mtf_feedback_rec_video;
			endif;
			
			#------Incorrect-FEEDBACK-DATA---
			$ifeed_audio = '';
			if ( ! empty($mtfi_feedback_audio)):
				$ifeed_audio = $mtfi_feedback_audio;
			elseif ( ! empty($mtfi_feedback_rec_audio)):
				$ifeed_audio = $mtfi_feedback_rec_audio;
			endif;
			
			$ifeed_video = '';
			if ( ! empty($mtfi_feedback_video)):
				$ifeed_video = $mtfi_feedback_video;
			elseif ( ! empty($mtfi_feedback_rec_video)):
				$ifeed_video = $mtfi_feedback_rec_video;
			endif;
			
			$quesData = array('questions'		=> ( ! empty($mtfq)) ? htmlspecialchars($mtfq) : '',
							  'qtype'			=> ( ! empty($question_type)) ? $question_type : '',
							  'qaudio'			=> ( ! empty($qaudio)) ? $qaudio : '',
							  'qspeech_text'	=> ( ! empty($mtfqq_text_to_speech)) ? htmlspecialchars($mtfqq_text_to_speech) : '',
							  'speech_text'		=> ( ! empty($mtfq_text_to_speech)) ? htmlspecialchars($mtfq_text_to_speech) : '',
							  'audio'			=> ( ! empty($audio)) ? $audio : '',
							  'video'			=> ( ! empty($video)) ? $video : '',
							  'screen'			=> ( ! empty($mtf_rec_screen)) ? $mtf_rec_screen : '',
							  'image'			=> ( ! empty($mtf_img)) ? $mtf_img : '',
							  'document'		=> ( ! empty($mtf_doc)) ? $mtf_doc : '',
							  'media_file'		=> ( ! empty($videoq_media_file)) ? $videoq_media_file : '',
							  'ques_val_1'		=> ( ! empty($ques_val_1)) ? $ques_val_1 : '',
							  'ques_val_2'		=> ( ! empty($ques_val_2)) ? $ques_val_2 : '',
							  'ques_val_3'		=> ( ! empty($ques_val_3)) ? $ques_val_3 : '',
							  'ques_val_4'		=> ( ! empty($ques_val_4)) ? $ques_val_4 : '',
							  'ques_val_5'		=> ( ! empty($ques_val_5)) ? $ques_val_5 : '',
							  'ques_val_6'		=> ( ! empty($ques_val_6)) ? $ques_val_6 : '',
							  'critical'		=> ( ! empty($mtf_criticalQ)) ? $mtf_criticalQ : '',
							  'status' 			=> 1,
							  'user_id'			=> $userId,
							  'cur_date'		=> $curdate,
							  'qid'				=> $ques_id);
			$ques_sql  = "UPDATE `question_tbl` SET `questions`=:questions, `question_type`=:qtype, `qaudio`=:qaudio, `qspeech_text`=:qspeech_text, `speech_text`=:speech_text, `audio`=:audio, `video`=:video, `screen`=:screen, `image`=:image, `document`=:document, `videoq_media_file`=:media_file, `ques_val_1`=:ques_val_1, `ques_val_2`=:ques_val_2, `ques_val_3`=:ques_val_3, `ques_val_4`=:ques_val_4, `ques_val_5`=:ques_val_5, `ques_val_6`=:ques_val_6, `critical`=:critical, `status`=:status, `uid`=:user_id, `cur_date`=:cur_date WHERE question_id=:qid";
			$ques_stmt = $db->prepare($ques_sql);
			$ques_stmt->execute($quesData);
			
			#-----Remove-Old-Feedback-------
			$db->feedback_remove($ques_id);
			
			#--------Correct-Feedback----------
			$cfeedData = array('qid'			=> ( ! empty($ques_id)) ? $ques_id : '',
							   'feedback'		=> ( ! empty($mtf_cfeedback)) ? htmlspecialchars($mtf_cfeedback) : '',
							   'ftype'			=> 1,
							   'speech_text'	=> ( ! empty($mtf_feedback_text_to_speech)) ? htmlspecialchars($mtf_feedback_text_to_speech) : '',
							   'audio'			=> ( ! empty($feed_audio)) ? $feed_audio : '',
							   'video'			=> ( ! empty($feed_video)) ? $feed_video : '',
							   'screen'			=> ( ! empty($mtf_feedback_rec_screen)) ? $mtf_feedback_rec_screen : '',
							   'image'			=> ( ! empty($mtf_feedback_img)) ? $mtf_feedback_img : '',
							   'document'		=> ( ! empty($mtf_feedback_doc)) ? $mtf_feedback_doc : '');
			$cfeed_sql  = "INSERT INTO `feedback_tbl` (`question_id`, `feedback`, `feedback_type`, `feed_speech_text`, `feed_audio`, `feed_video`, `feed_screen`, `feed_image`, `feed_document`) VALUES (:qid, :feedback, :ftype, :speech_text, :audio, :video, :screen, :image, :document)";
			$cfeed_stmt = $db->prepare($cfeed_sql);
			$cfeed_stmt->execute($cfeedData);
			
			#--------Incorrect-Feedback-------
			$ifeedData = array('qid'			=> ( ! empty($ques_id)) ? $ques_id : '',
							   'feedback'		=> ( ! empty($mtf_ifeedback)) ? htmlspecialchars($mtf_ifeedback) : '',
							   'ftype'			=> 2,
							   'speech_text'	=> ( ! empty($mtfi_feedback_text_to_speech)) ? htmlspecialchars($mtfi_feedback_text_to_speech) : '',
							   'audio'			=> ( ! empty($ifeed_audio)) ? $ifeed_audio : '',
							   'video'			=> ( ! empty($ifeed_video)) ? $ifeed_video : '',
							   'screen'			=> ( ! empty($mtfi_feedback_rec_screen)) ? $mtfi_feedback_rec_screen : '',
							   'image'			=> ( ! empty($mtfi_feedback_img)) ? $mtfi_feedback_img : '',
							   'document'		=> ( ! empty($mtfi_feedback_doc)) ? $mtfi_feedback_doc : '');
			$ifeed_sql  = "INSERT INTO `feedback_tbl` (`question_id`, `feedback`, `feedback_type`, `feed_speech_text`, `feed_audio`, `feed_video`, `feed_screen`, `feed_image`, `feed_document`) VALUES (:qid, :feedback, :ftype, :speech_text, :audio, :video, :screen, :image, :document)";
			$ifeed_stmt = $db->prepare($ifeed_sql);
			$ifeed_stmt->execute($ifeedData);
			
			#----------Shuffle-OFF--------------
			if ( ! empty($mtf_choice[0]) && ! empty($mtf_choice_order[0]) && ! empty($mtf_match_order[0]) && ! empty($mtf_match[0]) && ! empty($ques_id)):
				$soff = 0;
				foreach ($mtf_choice as $answerData):
					if ( ! empty($updateAnswerid[$soff])):
						$ansSql = "UPDATE `answer_tbl` SET `choice_option`=:choice_option, `choice_order_no`=:choice_order_no, `match_order_no`=:match_order_no, `match_option`=:match_option, `status`=:status, `uid`=:user_id, `cur_date`=:cur_date WHERE answer_id=:answer_id";
						$stmt 	= $db->prepare($ansSql);
						$stmt->execute(array('answer_id' 		=> $updateAnswerid[$soff],
											 'choice_option'	=> ( ! empty($mtf_choice[$soff])) ? htmlspecialchars($mtf_choice[$soff]) : '',
											 'choice_order_no'	=> ( ! empty($mtf_choice_order[$soff])) ? $mtf_choice_order[$soff] : '',
											 'match_order_no'	=> ( ! empty($mtf_match_order[$soff])) ? $mtf_match_order[$soff] : '',
											 'match_option'		=> ( ! empty($mtf_match[$soff])) ? htmlspecialchars($mtf_match[$soff]) : '',
											 'status' 			=> 1,
											 'user_id' 			=> $userId,
											 'cur_date'			=> $curdate));
					else:
						$ansSql = "INSERT INTO `answer_tbl` (`question_id`, `choice_option`, `choice_order_no`, `match_order_no`, `match_option`, `status`, `uid`, `cur_date`) 
								   VALUES (:qid, :choice_option, :choice_order_no, :match_order_no, :match_option, :status, :user_id, :cur_date)";
						$stmt = $db->prepare($ansSql);
						$stmt->execute(array('qid'				=> $ques_id,
											 'choice_option'	=> ( ! empty($mtf_choice[$soff])) ? htmlspecialchars($mtf_choice[$soff]) : '',
											 'choice_order_no'	=> ( ! empty($mtf_choice_order[$soff])) ? $mtf_choice_order[$soff] : '',
											 'match_order_no'	=> ( ! empty($mtf_match_order[$soff])) ? $mtf_match_order[$soff] : '',
											 'match_option'		=> ( ! empty($mtf_match[$soff])) ? htmlspecialchars($mtf_match[$soff]) : '',
											 'status' 			=> 1,
											 'user_id' 			=> $userId,
											 'cur_date'			=> $curdate));
					endif;
				$soff++;
				endforeach;
			endif;
			
			#----------Shuffle-ON--------------
			if ( ! empty($mtf_choice2[0]) && ! empty($mtf_match2[0]) && empty($mtf_match_order[0]) && empty($mtf_match[0])):
				$sON = 0;
				foreach ($mtf_choice2 as $answerData):
					if ( ! empty($updateAnswerid[$sON])):
						$ansSql = "UPDATE `answer_tbl` SET `choice_option`=:choice_option, `match_option`=:match_option, `status`=:status, `uid`=:user_id, `cur_date`=:cur_date WHERE answer_id=:answer_id";
						$stmt 	= $db->prepare($ansSql);
						$stmt->execute(array('answer_id' 		=> $updateAnswerid[$sON],
											 'choice_option'	=> ( ! empty($mtf_choice2[$sON])) ? htmlspecialchars($mtf_choice2[$sON]) : '',
											 'match_option'		=> ( ! empty($mtf_match2[$sON])) ? htmlspecialchars($mtf_match2[$sON]) : '',
											 'status' 			=> 1,
											 'user_id' 			=> $userId,
											 'cur_date'			=> $curdate));
					else:
						$ansSql = "INSERT INTO `answer_tbl` (`question_id`, `choice_option`, `match_option`, `status`, `uid`, `cur_date`) 
								   VALUES (:qid, :choice_option, :match_option, :status, :user_id, :cur_date)";
						$stmt = $db->prepare($ansSql);
						$stmt->execute(array('qid'				=> $ques_id,
											 'choice_option'	=> ( ! empty($mtf_choice2[$sON])) ? htmlspecialchars($mtf_choice2[$sON]) : '',
											 'match_option'		=> ( ! empty($mtf_match2[$sON])) ? htmlspecialchars($mtf_match2[$sON]) : '',
											 'status' 			=> 1,
											 'user_id' 			=> $userId,
											 'cur_date'			=> $curdate));
					endif;
					$sON++;
				endforeach;
			endif;
		//------END-MATCH-THE-FOLLOWING------
		
		//-----------START-SEQUENCE--------
		elseif ($question_type == 2):
			#-------QUESTIONS-DATA---
			$qaudio = '';
			if ( ! empty($sqq_qaudio)):
				$qaudio = $sqq_qaudio;
			elseif ( ! empty($sqq_rec_audio)):
				$qaudio = $sqq_rec_audio;
			endif;
			
			$audio = '';
			if ( ! empty($sq_qaudio)):
				$audio = $sq_qaudio;
			elseif ( ! empty($sq_rec_audio)):
				$audio = $sq_rec_audio;
			endif;
			
			$video = '';
			if ( ! empty($sq_video)):
				$video = $sq_video;
			elseif ( ! empty($sq_rec_video)):
				$video = $sq_rec_video;
			endif;
			
			#------Correct-FEEDBACK-DATA---
			$feed_audio = '';
			if ( ! empty($sq_feedback_audio)):
				$feed_audio = $sq_feedback_audio;
			elseif ( ! empty($sq_feedback_rec_audio)):
				$feed_audio = $sq_feedback_rec_audio;
			endif;
			
			$feed_video = '';
			if ( ! empty($sq_feedback_video)):
				$feed_video = $sq_feedback_video;
			elseif ( ! empty($sq_feedback_rec_video)):
				$feed_video = $sq_feedback_rec_video;
			endif;
			
			#------Incorrect-FEEDBACK-DATA---
			$ifeed_audio = '';
			if ( ! empty($sqi_feedback_audio)):
				$ifeed_audio = $sqi_feedback_audio;
			elseif ( ! empty($sqi_feedback_rec_audio)):
				$ifeed_audio = $sqi_feedback_rec_audio;
			endif;
			
			$ifeed_video = '';
			if ( ! empty($sqi_feedback_video)):
				$ifeed_video = $sqi_feedback_video;
			elseif ( ! empty($sqi_feedback_rec_video)):
				$ifeed_video = $sqi_feedback_rec_video;
			endif;
			
			$quesData = array('questions'		=> ( ! empty($sq)) ? htmlspecialchars($sq) : '',
							  'qtype'			=> ( ! empty($question_type)) ? $question_type : '',
							  'qaudio'			=> ( ! empty($qaudio)) ? $qaudio : '',
							  'qspeech_text'	=> ( ! empty($sqq_text_to_speech)) ? htmlspecialchars($sqq_text_to_speech) : '',
							  'speech_text'		=> ( ! empty($sq_text_to_speech)) ? htmlspecialchars($sq_text_to_speech) : '',
							  'audio'			=> ( ! empty($audio)) ? $audio : '',
							  'video'			=> ( ! empty($video)) ? $video : '',
							  'screen'			=> ( ! empty($sq_rec_screen)) ? $sq_rec_screen : '',
							  'image'			=> ( ! empty($sq_img)) ? $sq_img : '',
							  'document'		=> ( ! empty($sq_doc)) ? $sq_doc : '',
							  'shuffle'			=> (isset($sq_shuffle) && ! empty($sq_shuffle)) ? 1 : 0,
							  'media_file'		=> ( ! empty($videoq_media_file)) ? $videoq_media_file : '',
							  'ques_val_1'		=> ( ! empty($sq_ques_val_1)) ? $sq_ques_val_1 : '',
							  'ques_val_2'		=> ( ! empty($sq_ques_val_2)) ? $sq_ques_val_2 : '',
							  'ques_val_3'		=> ( ! empty($sq_ques_val_3)) ? $sq_ques_val_3 : '',
							  'ques_val_4'		=> ( ! empty($sq_ques_val_4)) ? $sq_ques_val_4 : '',
							  'ques_val_5'		=> ( ! empty($sq_ques_val_5)) ? $sq_ques_val_5 : '',
							  'ques_val_6'		=> ( ! empty($sq_ques_val_6)) ? $sq_ques_val_6 : '',
							  'critical'		=> ( ! empty($sq_criticalQ)) ? $sq_criticalQ : '',
							  'status' 			=> 1,
							  'user_id'			=> $userId,
							  'cur_date'		=> $curdate,
							  'qid'				=> $ques_id);
			$ques_sql  = "UPDATE `question_tbl` SET `questions`=:questions, `question_type`=:qtype, `qaudio`=:qaudio, `qspeech_text`=:qspeech_text, `speech_text`=:speech_text, `audio`=:audio, `video`=:video, `screen`=:screen, `image`=:image, `document`=:document, `shuffle`=:shuffle, `videoq_media_file`=:media_file, `ques_val_1`=:ques_val_1, `ques_val_2`=:ques_val_2, `ques_val_3`=:ques_val_3, `ques_val_4`=:ques_val_4, `ques_val_5`=:ques_val_5, `ques_val_6`=:ques_val_6, `critical`=:critical, `status`=:status, `uid`=:user_id, `cur_date`=:cur_date WHERE question_id=:qid";
			$ques_stmt = $db->prepare($ques_sql);
			$ques_stmt->execute($quesData);
			
			#-----Remove-Old-Feedback-------
			$db->feedback_remove($ques_id);
			
			#--------Correct-Feedback----------
			$cfeedData = array('qid'			=> ( ! empty($ques_id)) ? $ques_id : '',
							   'feedback'		=> ( ! empty($sq_feedback)) ? htmlspecialchars($sq_feedback) : '',
							   'ftype'			=> 1,
							   'speech_text'	=> ( ! empty($sq_feedback_text_to_speech)) ? htmlspecialchars($sq_feedback_text_to_speech) : '',
							   'audio'			=> ( ! empty($feed_audio)) ? $feed_audio : '',
							   'video'			=> ( ! empty($feed_video)) ? $feed_video : '',
							   'screen'			=> ( ! empty($sq_feedback_rec_screen)) ? $sq_feedback_rec_screen : '',
							   'image'			=> ( ! empty($sq_feedback_img)) ? $sq_feedback_img : '',
							   'document'		=> ( ! empty($sq_feedback_doc)) ? $sq_feedback_doc : '');
			
			$cfeed_sql  = "INSERT INTO `feedback_tbl` (`question_id`, `feedback`, `feedback_type`, `feed_speech_text`, `feed_audio`, `feed_video`, `feed_screen`, `feed_image`, `feed_document`) VALUES (:qid, :feedback, :ftype, :speech_text, :audio, :video, :screen, :image, :document)";
			$cfeed_stmt = $db->prepare($cfeed_sql);
			$cfeed_stmt->execute($cfeedData);
			
			#--------Incorrect-Feedback-------
			$ifeedData = array('qid'			=> ( ! empty($ques_id)) ? $ques_id : '',
							   'feedback'		=> ( ! empty($sq_ifeedback)) ? htmlspecialchars($sq_ifeedback) : '',
							   'ftype'			=> 2,
							   'speech_text'	=> ( ! empty($sqi_feedback_text_to_speech)) ? htmlspecialchars($sqi_feedback_text_to_speech) : '',
							   'audio'			=> ( ! empty($ifeed_audio)) ? $ifeed_audio : '',
							   'video'			=> ( ! empty($ifeed_video)) ? $ifeed_video : '',
							   'screen'			=> ( ! empty($sqi_feedback_rec_screen)) ? $sqi_feedback_rec_screen : '',
							   'image'			=> ( ! empty($sqi_feedback_img)) ? $sqi_feedback_img : '',
							   'document'		=> ( ! empty($sqi_feedback_doc)) ? $sqi_feedback_doc : '');
			
			$ifeed_sql  = "INSERT INTO `feedback_tbl` (`question_id`, `feedback`, `feedback_type`, `feed_speech_text`, `feed_audio`, `feed_video`, `feed_screen`, `feed_image`, `feed_document`) VALUES (:qid, :feedback, :ftype, :speech_text, :audio, :video, :screen, :image, :document)";
			$ifeed_stmt = $db->prepare($ifeed_sql);
			$ifeed_stmt->execute($ifeedData);
			
			#----------ADD-OPTIONS-------------
			if ( ! empty($sq_ans[0])):
				$sqi = 0; $sqi_order = 1;
				foreach ($sq_ans as $answerData):
					if ( ! empty($updateSQAnswerid[$sqi])):
						$ansSql = "UPDATE `answer_tbl` SET `choice_option`=:choice_option, `match_order_no`=:order_no, `status`=:status, `uid`=:user_id, `cur_date`=:cur_date WHERE answer_id=:answer_id";
						$stmt	= $db->prepare($ansSql);
						$stmt->execute(array('answer_id' 		=> $updateSQAnswerid[$sqi],
											 'choice_option'	=> ( ! empty($sq_ans[$sqi])) ? htmlspecialchars($sq_ans[$sqi]) : '',
											 'order_no'			=> $sqi_order,
											 'status' 			=> 1,
											 'user_id' 			=> $userId,
											 'cur_date'			=> $curdate));
					else:
						$ansSql = "INSERT INTO `answer_tbl` (`question_id`, `choice_option`, `match_order_no`, `status`, `uid`, `cur_date`) 
								   VALUES (:qid, :choice_option, :order_no, :status, :user_id, :cur_date)";
						$stmt = $db->prepare($ansSql);
						$stmt->execute(array('qid'				=> $ques_id,
											 'choice_option'	=> ( ! empty($sq_ans[$sqi])) ? htmlspecialchars($sq_ans[$sqi]) : '',
											 'order_no'			=> $sqi_order,
											 'status' 			=> 1,
											 'user_id' 			=> $userId,
											 'cur_date'			=> $curdate));
					endif;
					$sqi++; $sqi_order++;
				endforeach;
			endif;
		//-----------END-SEQUENCE----------
	
		//-----------START-SORTING----------
		elseif ($question_type == 3):
			#-------QUESTIONS-DATA---
			$qaudio = '';
			if ( ! empty($sortqq_qaudio)):
				$qaudio = $sortqq_qaudio;
			elseif ( ! empty($sortqq_rec_audio)):
				$qaudio = $sortqq_rec_audio;
			endif;
			
			$audio = '';
			if ( ! empty($sortq_qaudio)):
				$audio = $sortq_qaudio;
			elseif ( ! empty($sortq_rec_audio)):
				$audio = $sortq_rec_audio;
			endif;
			
			$video = '';
			if ( ! empty($sortq_video)):
				$video = $sortq_video;
			elseif ( ! empty($sortq_rec_video)):
				$video = $sortq_rec_video;
			endif;
			
			#------Correct-FEEDBACK-DATA---
			$feed_audio = '';
			if ( ! empty($sortq_feedback_audio)):
				$feed_audio = $sortq_feedback_audio;
			elseif ( ! empty($sortq_feedback_rec_audio)):
				$feed_audio = $sortq_feedback_rec_audio;
			endif;
			
			$feed_video = '';
			if ( ! empty($sortq_feedback_video)):
				$feed_video = $sortq_feedback_video;
			elseif ( ! empty($sortq_feedback_rec_video)):
				$feed_video = $sortq_feedback_rec_video;
			endif;
			
			#------Incorrect-FEEDBACK-DATA---
			$ifeed_audio = '';
			if ( ! empty($sortqi_feedback_audio)):
				$ifeed_audio = $sortqi_feedback_audio;
			elseif ( ! empty($sortqi_feedback_rec_audio)):
				$ifeed_audio = $sortqi_feedback_rec_audio;
			endif;
			
			$ifeed_video = '';
			if ( ! empty($sortqi_feedback_video)):
				$ifeed_video = $sortqi_feedback_video;
			elseif ( ! empty($sortqi_feedback_rec_video)):
				$ifeed_video = $sortqi_feedback_rec_video;
			endif;
						
			$quesData = array('questions'		=> ( ! empty($sortq)) ? htmlspecialchars($sortq) : '',
							  'qtype'			=> ( ! empty($question_type)) ? $question_type : '',
					  		  'qaudio'			=> ( ! empty($qaudio)) ? $qaudio : '',
					  		  'qspeech_text'	=> ( ! empty($sortqq_text_to_speech)) ? htmlspecialchars($sortqq_text_to_speech) : '',
							  'speech_text'		=> ( ! empty($sortq_text_to_speech)) ? htmlspecialchars($sortq_text_to_speech) : '',
							  'audio'			=> ( ! empty($audio)) ? $audio : '',
							  'video'			=> ( ! empty($video)) ? $video : '',
							  'screen'			=> ( ! empty($sortq_rec_screen)) ? $sortq_rec_screen : '',
							  'image'			=> ( ! empty($sortq_img)) ? $sortq_img : '',
							  'document'		=> ( ! empty($sortq_doc)) ? $sortq_doc : '',
							  'shuffle'			=> (isset($sort_shuffle) && ! empty($sort_shuffle)) ? 1 : 0,
							  'media_file'		=> ( ! empty($videoq_media_file)) ? $videoq_media_file : '',
							  'ques_val_1'		=> ( ! empty($sort_ques_val_1)) ? $sort_ques_val_1 : '',
							  'ques_val_2'		=> ( ! empty($sort_ques_val_2)) ? $sort_ques_val_2 : '',
							  'ques_val_3'		=> ( ! empty($sort_ques_val_3)) ? $sort_ques_val_3 : '',
							  'ques_val_4'		=> ( ! empty($sort_ques_val_4)) ? $sort_ques_val_4 : '',
							  'ques_val_5'		=> ( ! empty($sort_ques_val_5)) ? $sort_ques_val_5 : '',
							  'ques_val_6'		=> ( ! empty($sort_ques_val_6)) ? $sort_ques_val_6 : '',
							  'critical'		=> ( ! empty($sort_criticalQ)) ? $sort_criticalQ : '',
							  'status' 			=> 1,
							  'user_id'			=> $userId,
							  'cur_date'		=> $curdate,
							  'qid'				=> $ques_id);
			$ques_sql  = "UPDATE `question_tbl` SET `questions`=:questions, `question_type`=:qtype, `qaudio`=:qaudio, `qspeech_text`=:qspeech_text, `speech_text`=:speech_text, `audio`=:audio, `video`=:video, `screen`=:screen, `image`=:image, `document`=:document, `shuffle`=:shuffle, `videoq_media_file`=:media_file, `ques_val_1`=:ques_val_1, `ques_val_2`=:ques_val_2, `ques_val_3`=:ques_val_3, `ques_val_4`=:ques_val_4, `ques_val_5`=:ques_val_5, `ques_val_6`=:ques_val_6, `critical`=:critical, `status`=:status, `uid`=:user_id, `cur_date`=:cur_date WHERE question_id=:qid";
			$ques_stmt = $db->prepare($ques_sql);
			$ques_stmt->execute($quesData);
			
			#-----Remove-Old-Feedback-----
			$db->feedback_remove($ques_id);
			
			#--------Correct-Feedback----------
			$cfeedData = array('qid'			=> ( ! empty($ques_id)) ? $ques_id : '',
							   'feedback'		=> ( ! empty($sortq_cfeedback)) ? htmlspecialchars($sortq_cfeedback) : '',
							   'ftype'			=> 1,
							   'speech_text'	=> ( ! empty($sortq_feedback_text_to_speech)) ? htmlspecialchars($sortq_feedback_text_to_speech) : '',
							   'audio'			=> ( ! empty($feed_audio)) ? $feed_audio : '',
							   'video'			=> ( ! empty($feed_video)) ? $feed_video : '',
							   'screen'			=> ( ! empty($sortq_feedback_rec_screen)) ? $sortq_feedback_rec_screen : '',
							   'image'			=> ( ! empty($sortq_feedback_img)) ? $sortq_feedback_img : '',
							   'document'		=> ( ! empty($sortq_feedback_doc)) ? $sortq_feedback_doc : '');
			$cfeed_sql  = "INSERT INTO `feedback_tbl` (`question_id`, `feedback`, `feedback_type`, `feed_speech_text`, `feed_audio`, `feed_video`, `feed_screen`, `feed_image`, `feed_document`) VALUES (:qid, :feedback, :ftype, :speech_text, :audio, :video, :screen, :image, :document)";
			$cfeed_stmt = $db->prepare($cfeed_sql);
			$cfeed_stmt->execute($cfeedData);
			
			#--------Incorrect-Feedback-------
			$ifeedData = array('qid'			=> ( ! empty($ques_id)) ? $ques_id : '',
							   'feedback'		=> ( ! empty($sortq_ifeedback)) ? htmlspecialchars($sortq_ifeedback) : '',
							   'ftype'			=> 2,
							   'speech_text'	=> ( ! empty($sortqi_feedback_text_to_speech)) ? htmlspecialchars($sortqi_feedback_text_to_speech) : '',
							   'audio'			=> ( ! empty($ifeed_audio)) ? $ifeed_audio : '',
							   'video'			=> ( ! empty($ifeed_video)) ? $ifeed_video : '',
							   'screen'			=> ( ! empty($sortqi_feedback_rec_screen)) ? $sortqi_feedback_rec_screen : '',
							   'image'			=> ( ! empty($sortqi_feedback_img)) ? $sortqi_feedback_img : '',
							   'document'		=> ( ! empty($sortqi_feedback_doc)) ? $sortqi_feedback_doc : '');
			
			$ifeed_sql  = "INSERT INTO `feedback_tbl` (`question_id`, `feedback`, `feedback_type`, `feed_speech_text`, `feed_audio`, `feed_video`, `feed_screen`, `feed_image`, `feed_document`) VALUES (:qid, :feedback, :ftype, :speech_text, :audio, :video, :screen, :image, :document)";
			$ifeed_stmt = $db->prepare($ifeed_sql);
			$ifeed_stmt->execute($ifeedData);
			
			#----------ADD-OPTIONS----------
			$sortqi = 0;
			foreach ($sortq_sorting_items as $optionData):
				if ( ! empty($updateSortAnswerid[$sortqi])):
					$ansSql = "UPDATE `answer_tbl` SET `choice_option`=:choice_option, `image`=:image, `status`=:status, `uid`=:user_id, `cur_date`=:cur_date WHERE answer_id=:answer_id";
					$stmt	= $db->prepare($ansSql);
					$stmt->execute(array('answer_id'		=> $updateSortAnswerid[$sortqi],
										 'choice_option'	=> (isset($sortq_sorting_items[$sortqi]) && ! empty($sortq_sorting_items[$sortqi])) ? htmlspecialchars($sortq_sorting_items[$sortqi]) : '',
										 'image'			=> (isset($sortq_item_img[$sortqi]) && ! empty($sortq_item_img[$sortqi])) ? $sortq_item_img[$sortqi] : '',
										 'status' 			=> 1,
										 'user_id' 			=> $userId,
										 'cur_date'			=> $curdate));
					if ( ! empty($updateSubOptionid[$sortqi])):
						$sortqop = 0;
						foreach ($sortq_drag_items[$sortqi]['option'] as $subOptionData):
							if ( ! empty($updateSubOptionid[$sortqi][$sortqop])):
								$ansOpSql = "UPDATE `sub_options_tbl` SET `sub_option`=:sub_option WHERE sub_options_id=:sub_options_id";
								$opstmt	  = $db->prepare($ansOpSql);
								$opstmt->execute(array('sub_options_id'	=> $updateSubOptionid[$sortqi][$sortqop],
													   'sub_option'	 	=> ( ! empty($subOptionData)) ? htmlspecialchars($subOptionData) : ''));
							else:
								$opSql	= "INSERT INTO `sub_options_tbl` (`answer_id`, `sub_option`) VALUES (:aid, :option)";
								$opstmt = $db->prepare($opSql);
								$opstmt->execute(array('aid'	=> $updateSortAnswerid[$sortqi],
													   'option'	=> ( ! empty($subOptionData)) ? htmlspecialchars($subOptionData) : ''));
							endif;
							$sortqop++;
						endforeach;
					endif;
				else:
					$ansSql = "INSERT INTO `answer_tbl` (`question_id`, `choice_option`, `image`, `status`, `uid`, `cur_date`) VALUES (:qid, :choice_option, :image, :status, :user_id, :cur_date)";
					$stmt	= $db->prepare($ansSql);
					$stmt->execute(array('qid'				=> $ques_id,
										 'choice_option'	=> ( ! empty($sortq_sorting_items[$sortqi])) ? htmlspecialchars($sortq_sorting_items[$sortqi]) : '',
										 'image'			=> (isset($sortq_item_img[$sortqi]) && ! empty($sortq_item_img[$sortqi])) ? $sortq_item_img[$sortqi] : '',
										 'status' 			=> 1,
										 'user_id' 			=> $userId,
										 'cur_date'			=> $curdate));
					$ans_id = $db->lastInsertId();
					if ( ! empty($sortq_drag_items)):
						foreach ($sortq_drag_items[$sortqi]['option'] as $subOptionData):
							$opSql  = "INSERT INTO `sub_options_tbl` (`answer_id`, `sub_option`) VALUES (:aid, :option)";
							$opstmt = $db->prepare($opSql);
							$opstmt->execute(array('aid' => $ans_id,'option' => ( ! empty($subOptionData)) ? htmlspecialchars($subOptionData) : ''));
						endforeach;
					endif;
				endif;
				$sortqi++;
			endforeach;
		//-----------END-SORTING----------
	
		//-----------START-MCQ----------
		elseif ($question_type == 4):
			#-------QUESTIONS-DATA---
			$qaudio = '';
			if ( ! empty($mcqq_qaudio)):
				$qaudio = $mcqq_qaudio;
			elseif ( ! empty($mcqq_rec_audio)):
				$qaudio = $mcqq_rec_audio;
			endif;
			
			$audio = '';
			if ( ! empty($mcq_audio)):
				$audio = $mcq_audio;
			elseif ( ! empty($mcq_rec_audio)):
				$audio = $mcq_rec_audio;
			endif;
			
			$video = '';
			if ( ! empty($mcq_video)):
				$video = $mcq_video;
			elseif ( ! empty($mcq_rec_video)):
				$video = $mcq_rec_video;
			endif;
			
			#------Correct-FEEDBACK-DATA---
			$feed_audio = '';
			if ( ! empty($mcq_feedback_audio)):
				$feed_audio = $mcq_feedback_audio;
			elseif ( ! empty($mcq_feedback_rec_audio)):
				$feed_audio = $mcq_feedback_rec_audio;
			endif;
			
			$feed_video = '';
			if ( ! empty($mcq_feedback_video)):
				$feed_video = $mcq_feedback_video;
			elseif ( ! empty($mcq_feedback_rec_video)):
				$feed_video = $mcq_feedback_rec_video;
			endif;
			
			#------Incorrect-FEEDBACK-DATA---
			$ifeed_audio = '';
			if ( ! empty($mcqi_feedback_audio)):
				$ifeed_audio = $mcqi_feedback_audio;
			elseif ( ! empty($mcqi_feedback_rec_audio)):
				$ifeed_audio = $mcqi_feedback_rec_audio;
			endif;
			
			$ifeed_video = '';
			if ( ! empty($mcqi_feedback_video)):
				$ifeed_video = $mcqi_feedback_video;
			elseif ( ! empty($mcqi_feedback_rec_video)):
				$ifeed_video = $mcqi_feedback_rec_video;
			endif;
						
			$quesData = array('questions'		=> ( ! empty($mcq)) ? htmlspecialchars($mcq) : '',
							  'qtype'			=> ( ! empty($question_type)) ? $question_type : '',
							  'qaudio'			=> ( ! empty($qaudio)) ? $qaudio : '',
							  'qspeech_text'	=> ( ! empty($mcqq_text_to_speech)) ? htmlspecialchars($mcqq_text_to_speech) : '',
							  'speech_text'		=> ( ! empty($mcq_text_to_speech)) ? htmlspecialchars($mcq_text_to_speech) : '',
							  'audio'			=> ( ! empty($audio)) ? $audio : '',
							  'video'			=> ( ! empty($video)) ? $video : '',
							  'screen'			=> ( ! empty($mcq_rec_screen)) ? $mcq_rec_screen : '',
							  'image'			=> ( ! empty($mcq_img)) ? $mcq_img : '',
							  'document'		=> ( ! empty($mcq_doc)) ? $mcq_doc : '',
							  'true_option'		=> ( ! empty($mcq_true_option)) ? $mcq_true_option : '',
							  'shuffle'			=> (isset($mcq_shuffle) && ! empty($mcq_shuffle)) ? 1 : 0,
							  'media_file'		=> ( ! empty($videoq_media_file)) ? $videoq_media_file : '',
							  'ques_val_1'		=> ( ! empty($mcq_ques_val_1)) ? $mcq_ques_val_1 : '',
							  'ques_val_2'		=> ( ! empty($mcq_ques_val_2)) ? $mcq_ques_val_2 : '',
							  'ques_val_3'		=> ( ! empty($mcq_ques_val_3)) ? $mcq_ques_val_3 : '',
							  'ques_val_4'		=> ( ! empty($mcq_ques_val_4)) ? $mcq_ques_val_4 : '',
							  'ques_val_5'		=> ( ! empty($mcq_ques_val_5)) ? $mcq_ques_val_5 : '',
							  'ques_val_6'		=> ( ! empty($mcq_ques_val_6)) ? $mcq_ques_val_6 : '',
							  'critical'		=> ( ! empty($mcq_criticalQ)) ? $mcq_criticalQ : '',
							  'status' 			=> 1,
							  'user_id'			=> $userId,
							  'cur_date'		=> $curdate,
							  'qid'				=> $ques_id);
			$ques_sql  = "UPDATE `question_tbl` SET `questions`=:questions, `question_type`=:qtype, `qaudio`=:qaudio, `qspeech_text`=:qspeech_text, `speech_text`=:speech_text, `audio`=:audio, `video`=:video, `screen`=:screen, `image`=:image, `document`=:document, `true_options`=:true_option, `shuffle`=:shuffle, `videoq_media_file`=:media_file, `ques_val_1`=:ques_val_1, `ques_val_2`=:ques_val_2, `ques_val_3`=:ques_val_3, `ques_val_4`=:ques_val_4, `ques_val_5`=:ques_val_5, `ques_val_6`=:ques_val_6, `critical`=:critical, `status`=:status, `uid`=:user_id, `cur_date`=:cur_date WHERE question_id=:qid";
			$ques_stmt = $db->prepare($ques_sql);
			$ques_stmt->execute($quesData);
			
			#-----Remove-Old-Feedback-------
			$db->feedback_remove($ques_id);
			
			#--------Correct-Feedback----------
			$cfeedData = array('qid'			=> ( ! empty($ques_id)) ? $ques_id : '',
							   'feedback'		=> ( ! empty($mcq_cfeedback)) ? htmlspecialchars($mcq_cfeedback) : '',
							   'ftype'			=> 1,
							   'speech_text'	=> ( ! empty($mcq_feedback_text_to_speech)) ? htmlspecialchars($mcq_feedback_text_to_speech) : '',
							   'audio'			=> ( ! empty($feed_audio)) ? $feed_audio : '',
							   'video'			=> ( ! empty($feed_video)) ? $feed_video : '',
							   'screen'			=> ( ! empty($mcq_feedback_rec_screen)) ? $mcq_feedback_rec_screen : '',
							   'image'			=> ( ! empty($mcq_feedback_img)) ? $mcq_feedback_img : '',
							   'document'		=> ( ! empty($mcq_feedback_doc)) ? $mcq_feedback_doc : '');
			
			$cfeed_sql = "INSERT INTO `feedback_tbl` (`question_id`, `feedback`, `feedback_type`, `feed_speech_text`, `feed_audio`, `feed_video`, `feed_screen`, `feed_image`, `feed_document`) VALUES (:qid, :feedback, :ftype, :speech_text, :audio, :video, :screen, :image, :document)";
			$cfeed_stmt = $db->prepare($cfeed_sql);
			$cfeed_stmt->execute($cfeedData);
			
			#--------Incorrect-Feedback-------
			$ifeedData = array('qid'			=> ( ! empty($ques_id)) ? $ques_id : '',
							   'feedback'		=> ( ! empty($mcq_ifeedback)) ? htmlspecialchars($mcq_ifeedback) : '',
							   'ftype'			=> 2,
							   'speech_text'	=> ( ! empty($mcqi_feedback_text_to_speech)) ? htmlspecialchars($mcqi_feedback_text_to_speech) : '',
							   'audio'			=> ( ! empty($ifeed_audio)) ? $ifeed_audio : '',
							   'video'			=> ( ! empty($ifeed_video)) ? $ifeed_video : '',
							   'screen'			=> ( ! empty($mcqi_feedback_rec_screen)) ? $mcqi_feedback_rec_screen : '',
							   'image'			=> ( ! empty($mcqi_feedback_img)) ? $mcqi_feedback_img : '',
							   'document'		=> ( ! empty($mcqi_feedback_doc)) ? $mcqi_feedback_doc : '');
			
			$ifeed_sql  = "INSERT INTO `feedback_tbl` (`question_id`, `feedback`, `feedback_type`, `feed_speech_text`, `feed_audio`, `feed_video`, `feed_screen`, `feed_image`, `feed_document`) VALUES (:qid, :feedback, :ftype, :speech_text, :audio, :video, :screen, :image, :document)";
			$ifeed_stmt = $db->prepare($ifeed_sql);
			$ifeed_stmt->execute($ifeedData);
			
			#----------ADD-OPTIONS-------------
			if ( ! empty($mcq_option[0])):
				$mcqi = 0; $mcqtrue = 1;
				foreach ($mcq_option as $answerData):
					if ( ! empty($updateMCQAnswerid[$mcqi])):
						$ansSql = "UPDATE `answer_tbl` SET `choice_option`=:option, `true_option`=:true, `status`=:status, `uid`=:user_id, `cur_date`=:cur_date WHERE answer_id=:aid";
						$stmt = $db->prepare($ansSql);
						$stmt->execute(array('aid'		=> $updateMCQAnswerid[$mcqi],
											 'option'	=> ( ! empty($mcq_option[$mcqi])) ? htmlspecialchars($mcq_option[$mcqi]) : '',
											 'true'		=> ( ! empty($mcq_true_option) && $mcq_true_option == $mcqtrue) ? 1 : '',
											 'status'	=> 1,
											 'user_id' 	=> $userId,
											 'cur_date'	=> $curdate));

					else:
						$ansSql = "INSERT INTO `answer_tbl` (`question_id`, `choice_option`, `true_option`, `status`, `uid`, `cur_date`) VALUES (:qid, :option, :true, :status, :user_id, :cur_date)";
						$stmt = $db->prepare($ansSql);
						$stmt->execute(array('qid'		=> $ques_id,
											 'option'	=> ( ! empty($mcq_option[$mcqi])) ? htmlspecialchars($mcq_option[$mcqi]) : '',
											 'true'		=> ( ! empty($mcq_true_option) && $mcq_true_option == $mcqtrue) ? 1 : '',
											 'status'	=> 1,
											 'user_id' 	=> $userId,
											 'cur_date'	=> $curdate));
					endif;
					$mcqi++; $mcqtrue++;
				endforeach;
			endif;
		//-----------END-MCQ----------
	
		//-----------START-MMCQ----------
		elseif ($question_type == 5):
			#-------QUESTIONS-DATA---
			$qaudio = '';
			if ( ! empty($mmcqq_qaudio)):
				$qaudio = $mmcqq_qaudio;
			elseif ( ! empty($mmcqq_rec_audio)):
				$qaudio = $mmcqq_rec_audio;
			endif;

			$audio = '';
			if ( ! empty($mmcq_audio)):
				$audio = $mmcq_audio;
			elseif ( ! empty($mmcq_rec_audio)):
				$audio = $mmcq_rec_audio;
			endif;
			
			$video = '';
			if ( ! empty($mmcq_video)):
				$video = $mmcq_video;
			elseif ( ! empty($mmcq_rec_video)):
				$video = $mmcq_rec_video;
			endif;
			
			#------Correct-FEEDBACK-DATA---
			$feed_audio = '';
			if ( ! empty($mmcq_feedback_audio)):
				$feed_audio = $mmcq_feedback_audio;
			elseif ( ! empty($mmcq_feedback_rec_audio)):
				$feed_audio = $mmcq_feedback_rec_audio;
			endif;
			
			$feed_video = '';
			if ( ! empty($mmcq_feedback_video)):
				$feed_video = $mmcq_feedback_video;
			elseif ( ! empty($mmcq_feedback_rec_video)):
				$feed_video = $mmcq_feedback_rec_video;
			endif;
			
			#------Incorrect-FEEDBACK-DATA---
			$ifeed_audio = '';
			if ( ! empty($mmcqi_feedback_audio)):
				$ifeed_audio = $mmcqi_feedback_audio;
			elseif ( ! empty($mmcqi_feedback_rec_audio)):
				$ifeed_audio = $mmcqi_feedback_rec_audio;
			endif;
			
			$ifeed_video = '';
			if ( ! empty($mmcqi_feedback_video)):
				$ifeed_video = $mmcqi_feedback_video;
			elseif ( ! empty($mmcqi_feedback_rec_video)):
				$ifeed_video = $mmcqi_feedback_rec_video;
			endif;
			
			$quesData = array('questions'		=> ( ! empty($mmcq)) ? htmlspecialchars($mmcq) : '',
							  'qtype'			=> ( ! empty($question_type)) ? $question_type : '',
					  		  'qaudio'			=> ( ! empty($qaudio)) ? $qaudio : '',
					  		  'qspeech_text'	=> ( ! empty($mmcqq_text_to_speech)) ? htmlspecialchars($mmcqq_text_to_speech) : '',
							  'speech_text'		=> ( ! empty($mmcq_text_to_speech)) ? htmlspecialchars($mmcq_text_to_speech) : '',
							  'audio'			=> ( ! empty($audio)) ? $audio : '',
							  'video'			=> ( ! empty($video)) ? $video : '',
							  'screen'			=> ( ! empty($mmcq_rec_screen)) ? $mmcq_rec_screen : '',
							  'image'			=> ( ! empty($mmcq_img)) ? $mmcq_img : '',
							  'document'		=> ( ! empty($mmcq_doc)) ? $mmcq_doc : '',
							  'true_option'		=> ( ! empty($mmcq_true_option)) ? $db->addMultiIds($mmcq_true_option) : '',
							  'shuffle'			=> (isset($mmcq_shuffle) && ! empty($mmcq_shuffle)) ? 1 : 0,
							  'media_file'		=> ( ! empty($videoq_media_file)) ? $videoq_media_file : '',
							  'ques_val_1'		=> ( ! empty($mmcq_ques_val_1)) ? $mmcq_ques_val_1 : '',
							  'ques_val_2'		=> ( ! empty($mmcq_ques_val_2)) ? $mmcq_ques_val_2 : '',
							  'ques_val_3'		=> ( ! empty($mmcq_ques_val_3)) ? $mmcq_ques_val_3 : '',
							  'ques_val_4'		=> ( ! empty($mmcq_ques_val_4)) ? $mmcq_ques_val_4 : '',
							  'ques_val_5'		=> ( ! empty($mmcq_ques_val_5)) ? $mmcq_ques_val_5 : '',
							  'ques_val_6'		=> ( ! empty($mmcq_ques_val_6)) ? $mmcq_ques_val_6 : '',
							  'critical'		=> ( ! empty($mmcq_criticalQ)) ? $mmcq_criticalQ : '',
							  'status' 			=> 1,
							  'user_id'			=> $userId,
							  'cur_date'		=> $curdate,
							  'qid'				=> $ques_id);
			$ques_sql  = "UPDATE `question_tbl` SET `questions`=:questions, `question_type`=:qtype, `qaudio`=:qaudio, `qspeech_text`=:qspeech_text, `speech_text`=:speech_text, `audio`=:audio, `video`=:video, `screen`=:screen, `image`=:image, `document`=:document, `true_options`=:true_option, `shuffle`=:shuffle, `videoq_media_file`=:media_file, `ques_val_1`=:ques_val_1, `ques_val_2`=:ques_val_2, `ques_val_3`=:ques_val_3, `ques_val_4`=:ques_val_4, `ques_val_5`=:ques_val_5, `ques_val_6`=:ques_val_6, `critical`=:critical, `status`=:status, `uid`=:user_id, `cur_date`=:cur_date WHERE question_id=:qid";
			$ques_stmt = $db->prepare($ques_sql);
			$ques_stmt->execute($quesData);
			
			#-----Remove-Old-Feedback-------
			$db->feedback_remove($ques_id);
			
			#--------Correct-Feedback----------
			$cfeedData = array('qid'			=> ( ! empty($ques_id)) ? $ques_id : '',
							   'feedback'		=> ( ! empty($mmcq_cfeedback)) ? htmlspecialchars($mmcq_cfeedback) : '',
							   'ftype'			=> 1,
							   'speech_text'	=> ( ! empty($mmcq_feedback_text_to_speech)) ? htmlspecialchars($mmcq_feedback_text_to_speech) : '',
							   'audio'			=> ( ! empty($feed_audio)) ? $feed_audio : '',
							   'video'			=> ( ! empty($feed_video)) ? $feed_video : '',
							   'screen'			=> ( ! empty($mmcq_feedback_rec_screen)) ? $mmcq_feedback_rec_screen : '',
							   'image'			=> ( ! empty($mmcq_feedback_img)) ? $mmcq_feedback_img : '',
							   'document'		=> ( ! empty($mmcq_feedback_doc)) ? $mmcq_feedback_doc : '');
			
			$cfeed_sql = "INSERT INTO `feedback_tbl` (`question_id`, `feedback`, `feedback_type`, `feed_speech_text`, `feed_audio`, `feed_video`, `feed_screen`, `feed_image`, `feed_document`) VALUES (:qid, :feedback, :ftype, :speech_text, :audio, :video, :screen, :image, :document)";
			$cfeed_stmt = $db->prepare($cfeed_sql);
			$cfeed_stmt->execute($cfeedData);
			
			#--------Incorrect-Feedback-------
			$ifeedData = array('qid'			=> ( ! empty($ques_id)) ? $ques_id : '',
							   'feedback'		=> ( ! empty($mmcq_ifeedback)) ? htmlspecialchars($mmcq_ifeedback) : '',
							   'ftype'			=> 2,
							   'speech_text'	=> ( ! empty($mmcqi_feedback_text_to_speech)) ? htmlspecialchars($mmcqi_feedback_text_to_speech) : '',
							   'audio'			=> ( ! empty($ifeed_audio)) ? $ifeed_audio : '',
							   'video'			=> ( ! empty($ifeed_video)) ? $ifeed_video : '',
							   'screen'			=> ( ! empty($mmcqi_feedback_rec_screen)) ? $mmcqi_feedback_rec_screen : '',
							   'image'			=> ( ! empty($mmcqi_feedback_img)) ? $mmcqi_feedback_img : '',
							   'document'		=> ( ! empty($mmcqi_feedback_doc)) ? $mmcqi_feedback_doc : '');
			
			$ifeed_sql = "INSERT INTO `feedback_tbl` (`question_id`, `feedback`, `feedback_type`, `feed_speech_text`, `feed_audio`, `feed_video`, `feed_screen`, `feed_image`, `feed_document`) VALUES (:qid, :feedback, :ftype, :speech_text, :audio, :video, :screen, :image, :document)";
			$ifeed_stmt = $db->prepare($ifeed_sql);
			$ifeed_stmt->execute($ifeedData);
			
			#----------ADD-OPTIONS-------------
			if ( ! empty($mmcq_option[0])):
				$mmcqi = 0; $mmcqtrue = 1;
				foreach ($mmcq_option as $answerData):
					if ( ! empty($updateMMCQAnswerid[$mmcqi])):
						$ansSql = "UPDATE `answer_tbl` SET `choice_option`=:option, `true_option`=:true, `status`=:status, `uid`=:user_id, `cur_date`=:cur_date WHERE answer_id=:aid";
						$stmt = $db->prepare($ansSql);
						$stmt->execute(array('aid'		=> $updateMMCQAnswerid[$mmcqi],
											 'option'	=> ( ! empty($mmcq_option[$mmcqi])) ? htmlspecialchars($mmcq_option[$mmcqi]) : '',
											 'true'		=> ( ! empty($mmcq_true_option[$mmcqi]) && $mmcq_true_option[$mmcqi] == $mmcqtrue) ? 1 : '',
											 'status'	=> 1,
											 'user_id' 	=> $userId,
											 'cur_date'	=> $curdate));
					else:
						$ansSql = "INSERT INTO `answer_tbl` (`question_id`, `choice_option`, `true_option`, `status`, `uid`, `cur_date`) values (:qid, :option, :true, :status, :user_id, :cur_date)";
						$stmt = $db->prepare($ansSql);
						$stmt->execute(array('qid'		=> $ques_id,
											 'option'	=> ( ! empty($mmcq_option[$mmcqi])) ? htmlspecialchars($mmcq_option[$mmcqi]) : '',
											 'true'		=> ( ! empty($mmcq_true_option[$mmcqi]) && $mmcq_true_option[$mmcqi] == $mmcqtrue) ? 1 : '',
											 'status'	=> 1,
											 'user_id' 	=> $userId,
											 'cur_date'	=> $curdate));
					endif;
					$mmcqi++; $mmcqtrue++;
				endforeach;
			endif;
		//-----------END-MMCQ----------
	
		//-----------START-SWIPING----------
		elseif ($question_type == 7):
			#-------QUESTIONS-DATA---
			$qaudio = '';
			if ( ! empty($swipqq_qaudio)):
				$qaudio = $swipqq_qaudio;
			elseif ( ! empty($swipqq_rec_audio)):
				$qaudio = $swipqq_rec_audio;
			endif;

			$audio = '';
			if ( ! empty($swipq_audio)):
				$audio = $swipq_audio;
			elseif ( ! empty($swipq_rec_audio)):
				$audio = $swipq_rec_audio;
			endif;
			
			$video = '';
			if ( ! empty($swipq_video)):
				$video = $swipq_video;
			elseif ( ! empty($swipq_rec_video)):
				$video = $swipq_rec_video;
			endif;
			
			#------Correct-FEEDBACK-DATA---
			$feed_audio = '';
			if ( ! empty($swipq_feedback_audio)):
				$feed_audio = $swipq_feedback_audio;
			elseif ( ! empty($swipq_feedback_rec_audio)):
				$feed_audio = $swipq_feedback_rec_audio;
			endif;
			
			$feed_video = '';
			if ( ! empty($swipq_feedback_video)):
				$feed_video = $swipq_feedback_video;
			elseif ( ! empty($swipq_feedback_rec_video)):
				$feed_video = $swipq_feedback_rec_video;
			endif;
			
			#------Incorrect-FEEDBACK-DATA---
			$ifeed_audio = '';
			if ( ! empty($swipqi_feedback_audio)):
				$ifeed_audio = $swipqi_feedback_audio;
			elseif ( ! empty($swipqi_feedback_rec_audio)):
				$ifeed_audio = $swipqi_feedback_rec_audio;
			endif;
			
			$ifeed_video = '';
			if ( ! empty($swipqi_feedback_video)):
				$ifeed_video = $swipqi_feedback_video;
			elseif ( ! empty($swipqi_feedback_rec_video)):
				$ifeed_video = $swipqi_feedback_rec_video;
			endif;
						
			$quesData = array('questions'		=> ( ! empty($swipq)) ? htmlspecialchars($swipq) : '',
							  'qtype'			=> ( ! empty($question_type)) ? $question_type : '',
							  'qaudio'			=> ( ! empty($qaudio)) ? $qaudio : '',
							  'qspeech_text'	=> ( ! empty($swipqq_text_to_speech)) ? htmlspecialchars($swipqq_text_to_speech) : '',
							  'speech_text'		=> ( ! empty($swipq_text_to_speech)) ? htmlspecialchars($swipq_text_to_speech) : '',
							  'audio'			=> ( ! empty($audio)) ? $audio : '',
							  'video'			=> ( ! empty($video)) ? $video : '',
							  'screen'			=> ( ! empty($swipq_rec_screen)) ? $swipq_rec_screen : '',
							  'image'			=> ( ! empty($swipq_img)) ? $swipq_img : '',
							  'document'		=> ( ! empty($swipq_doc)) ? $swipq_doc : '',
							  'true_option'		=> ( ! empty($swipq_true_option)) ? $swipq_true_option : '',
							  'shuffle'			=> (isset($swipq_shuffle) && ! empty($swipq_shuffle)) ? 1 : 0,
							  'media_file'		=> ( ! empty($videoq_media_file)) ? $videoq_media_file : '',
							  'ques_val_1'		=> ( ! empty($swipq_ques_val_1)) ? $swipq_ques_val_1 : '',
							  'ques_val_2'		=> ( ! empty($swipq_ques_val_2)) ? $swipq_ques_val_2 : '',
							  'ques_val_3'		=> ( ! empty($swipq_ques_val_3)) ? $swipq_ques_val_3 : '',
							  'ques_val_4'		=> ( ! empty($swipq_ques_val_4)) ? $swipq_ques_val_4 : '',
							  'ques_val_5'		=> ( ! empty($swipq_ques_val_5)) ? $swipq_ques_val_5 : '',
							  'ques_val_6'		=> ( ! empty($swipq_ques_val_6)) ? $swipq_ques_val_6 : '',
							  'critical'		=> ( ! empty($swip_criticalQ)) ? $swip_criticalQ : '',
							  'status' 			=> 1,
							  'user_id'			=> $userId,
							  'cur_date'		=> $curdate,
							  'qid'				=> $ques_id);
			$ques_sql  = "UPDATE `question_tbl` SET `questions`=:questions, `question_type`=:qtype, `qaudio`=:qaudio, `qspeech_text`=:qspeech_text, `speech_text`=:speech_text, `audio`=:audio, `video`=:video, `screen`=:screen, `image`=:image, `document`=:document, `true_options`=:true_option, `shuffle`=:shuffle, `videoq_media_file`=:media_file, `ques_val_1`=:ques_val_1, `ques_val_2`=:ques_val_2, `ques_val_3`=:ques_val_3, `ques_val_4`=:ques_val_4, `ques_val_5`=:ques_val_5, `ques_val_6`=:ques_val_6, `critical`=:critical, `status`=:status, `uid`=:user_id, `cur_date`=:cur_date WHERE question_id=:qid";
			$ques_stmt = $db->prepare($ques_sql);
			$ques_stmt->execute($quesData);
			
			#-----Remove-Old-Feedback-------
			$db->feedback_remove($ques_id);
			
			#--------Correct-Feedback----------
			$cfeedData = array('qid'			=> ( ! empty($ques_id)) ? $ques_id : '',
							   'feedback'		=> ( ! empty($swipq_cfeedback)) ? htmlspecialchars($swipq_cfeedback) : '',
							   'ftype'			=> 1,
							   'speech_text'	=> ( ! empty($swipq_feedback_text_to_speech)) ? htmlspecialchars($swipq_feedback_text_to_speech) : '',
							   'audio'			=> ( ! empty($feed_audio)) ? $feed_audio : '',
							   'video'			=> ( ! empty($feed_video)) ? $feed_video : '',
							   'screen'			=> ( ! empty($swipq_feedback_rec_screen)) ? $swipq_feedback_rec_screen : '',
							   'image'			=> ( ! empty($swipq_feedback_img)) ? $swipq_feedback_img : '',
							   'document'		=> ( ! empty($swipq_feedback_doc)) ? $swipq_feedback_doc : '');
			
			$cfeed_sql  = "INSERT INTO `feedback_tbl` (`question_id`, `feedback`, `feedback_type`, `feed_speech_text`, `feed_audio`, `feed_video`, `feed_screen`, `feed_image`, `feed_document`) VALUES (:qid, :feedback, :ftype, :speech_text, :audio, :video, :screen, :image, :document)";
			$cfeed_stmt = $db->prepare($cfeed_sql);
			$cfeed_stmt->execute($cfeedData);
			
			#--------Incorrect-Feedback-------
			$ifeedData = array('qid'			=> ( ! empty($ques_id)) ? $ques_id : '',
							   'feedback'		=> ( ! empty($swipq_ifeedback)) ? htmlspecialchars($swipq_ifeedback) : '',
							   'ftype'			=> 2,
							   'speech_text'	=> ( ! empty($swipqi_feedback_text_to_speech)) ? htmlspecialchars($swipqi_feedback_text_to_speech) : '',
							   'audio'			=> ( ! empty($ifeed_audio)) ? $ifeed_audio : '',
							   'video'			=> ( ! empty($ifeed_video)) ? $ifeed_video : '',
							   'screen'			=> ( ! empty($swipqi_feedback_rec_screen)) ? $swipqi_feedback_rec_screen : '',
							   'image'			=> ( ! empty($swipqi_feedback_img)) ? $swipqi_feedback_img : '',
							   'document'		=> ( ! empty($swipqi_feedback_doc)) ? $swipqi_feedback_doc : '');
			
			$ifeed_sql  = "INSERT INTO `feedback_tbl` (`question_id`, `feedback`, `feedback_type`, `feed_speech_text`, `feed_audio`, `feed_video`, `feed_screen`, `feed_image`, `feed_document`) VALUES (:qid, :feedback, :ftype, :speech_text, :audio, :video, :screen, :image, :document)";
			$ifeed_stmt = $db->prepare($ifeed_sql);
			$ifeed_stmt->execute($ifeedData);
			
			#----------ADD-OPTIONS-------------
			$swipqi = 0; $swiptrue = 1;
			foreach ($swipq_option as $answerData):
				if ( ! empty($updateSwipqAnswerid[$swipqi])):
					$ansSql = "UPDATE `answer_tbl` SET `choice_option`=:option, `true_option`=:true, `image`=:image, `status`=:status, `uid`=:user_id, `cur_date`=:cur_date WHERE answer_id=:aid";
					$stmt	= $db->prepare($ansSql);
					$stmt->execute(array('aid' 		=> $updateSwipqAnswerid[$swipqi],
										 'option'	=> ( ! empty($swipq_option[$swipqi])) ? htmlspecialchars($swipq_option[$swipqi]) : '',
										 'image'	=> ( ! empty($swipq_item_img[$swipqi])) ? $swipq_item_img[$swipqi] : '',
										 'true'		=> ( ! empty($swipq_true_option) && $swipq_true_option == $swiptrue) ? 1 : '',
										 'status'	=> 1,
										 'user_id' 	=> $userId,
										 'cur_date'	=> $curdate));
				else:
					$ansSql = "INSERT INTO `answer_tbl` (`question_id`, `choice_option`, `true_option`, `image`, `status`, `uid`, `cur_date`) VALUES (:qid, :option, :true, :image, :status, :user_id, :cur_date)";
					$stmt = $db->prepare($ansSql);
					$stmt->execute(array('qid'		=> $ques_id,
										 'option'	=> ( ! empty($swipq_option[$swipqi])) ? htmlspecialchars($swipq_option[$swipqi]) : '',
										 'image'	=> ( ! empty($swipq_item_img[$swipqi])) ? $swipq_item_img[$swipqi] : '',
										 'true'		=> ( ! empty($swipq_true_option) && $swipq_true_option == $swiptrue) ? 1 : '',
										 'status'	=> 1,
										 'user_id' 	=> $userId,
										 'cur_date'	=> $curdate));
				endif;
				$swipqi++; $swiptrue++;
			endforeach;
		//-----------END-SWIPING----------
		
		//-----------START-DDQ----------
		elseif ($question_type == 8):
			#-------QUESTIONS-DATA---
			$qaudio = '';
			if ( ! empty($ddqq_qaudio)):
				$qaudio = $ddqq_qaudio;
			elseif ( ! empty($ddqq_rec_audio)):
				$qaudio = $ddqq_rec_audio;
			endif;
		
			$audio = '';
			if ( ! empty($ddq_audio)):
				$audio = $ddq_audio;
			elseif ( ! empty($ddq_rec_audio)):
				$audio = $ddq_rec_audio;
			endif;
			
			$video = '';
			if ( ! empty($ddq_video)):
				$video = $ddq_video;
			elseif ( ! empty($ddq_rec_video)):
				$video = $ddq_rec_video;
			endif;
			
			#------Correct-FEEDBACK-DATA---
			$feed_audio = '';
			if ( ! empty($ddq_feedback_audio)):
				$feed_audio = $ddq_feedback_audio;
			elseif ( ! empty($ddq_feedback_rec_audio)):
				$feed_audio = $ddq_feedback_rec_audio;
			endif;
			
			$feed_video = '';
			if ( ! empty($ddq_feedback_video)):
				$feed_video = $ddq_feedback_video;
			elseif ( ! empty($ddq_feedback_rec_video)):
				$feed_video = $ddq_feedback_rec_video;
			endif;
			
			#------Incorrect-FEEDBACK-DATA---
			$ifeed_audio = '';
			if ( ! empty($ddqi_feedback_audio)):
				$ifeed_audio = $ddqi_feedback_audio;
			elseif ( ! empty($ddqi_feedback_rec_audio)):
				$ifeed_audio = $ddqi_feedback_rec_audio;
			endif;
			
			$ifeed_video = '';
			if ( ! empty($ddqi_feedback_video)):
				$ifeed_video = $ddqi_feedback_video;
			elseif ( ! empty($ddqi_feedback_rec_video)):
				$ifeed_video = $ddqi_feedback_rec_video;
			endif;
			
			#--For-Multi-DROP-Options----
			if ($seleted_drag_option == 1):
				$quesData = array('questions'		=> ( ! empty($ddq)) ? htmlspecialchars($ddq) : '',
								  'seleted_drag' 	=> ( ! empty($seleted_drag_option)) ? $seleted_drag_option : '',
								  'qtype'			=> ( ! empty($question_type)) ? $question_type : '',
								  'qaudio'			=> ( ! empty($qaudio)) ? $qaudio : '',
								  'qspeech_text'	=> ( ! empty($ddqq_text_to_speech)) ? htmlspecialchars($ddqq_text_to_speech) : '',
								  'speech_text'		=> ( ! empty($ddq_text_to_speech)) ? htmlspecialchars($ddq_text_to_speech) : '',
								  'audio'			=> ( ! empty($audio)) ? $audio : '',
								  'video'			=> ( ! empty($video)) ? $video : '',
								  'screen'			=> ( ! empty($ddq_rec_screen)) ? $ddq_rec_screen : '',
								  'image'			=> ( ! empty($ddq_img)) ? $ddq_img : '',
								  'document'		=> ( ! empty($ddq_doc)) ? $ddq_doc : '',
								  'shuffle'			=> (isset($ddq_shuffle1) && ! empty($ddq_shuffle1)) ? 1 : 0,
								  'media_file'		=> ( ! empty($videoq_media_file)) ? $videoq_media_file : '',
								  'ques_val_1'		=> ( ! empty($ddq_ques_val_1)) ? $ddq_ques_val_1 : '',
								  'ques_val_2'		=> ( ! empty($ddq_ques_val_2)) ? $ddq_ques_val_2 : '',
								  'ques_val_3'		=> ( ! empty($ddq_ques_val_3)) ? $ddq_ques_val_3 : '',
								  'ques_val_4'		=> ( ! empty($ddq_ques_val_4)) ? $ddq_ques_val_4 : '',
								  'ques_val_5'		=> ( ! empty($ddq_ques_val_5)) ? $ddq_ques_val_5 : '',
								  'ques_val_6'		=> ( ! empty($ddq_ques_val_6)) ? $ddq_ques_val_6 : '',
								  'critical'		=> ( ! empty($ddq_criticalQ)) ? $ddq_criticalQ : '',
								  'status' 			=> 1,
								  'user_id'			=> $userId,
								  'cur_date'		=> $curdate,
								  'qid'				=> $ques_id);
				$ques_sql  = "UPDATE `question_tbl` SET `questions`=:questions, `question_type`=:qtype, `seleted_drag_option`=:seleted_drag, `qaudio`=:qaudio, `qspeech_text`=:qspeech_text, `speech_text`=:speech_text, `audio`=:audio, `video`=:video, `screen`=:screen, `image`=:image, `document`=:document, `shuffle`=:shuffle, `videoq_media_file`=:media_file, `ques_val_1`=:ques_val_1, `ques_val_2`=:ques_val_2, `ques_val_3`=:ques_val_3, `ques_val_4`=:ques_val_4, `ques_val_5`=:ques_val_5, `ques_val_6`=:ques_val_6, `critical`=:critical, `status`=:status, `uid`=:user_id, `cur_date`=:cur_date WHERE question_id=:qid";
				$ques_stmt = $db->prepare($ques_sql);
				$ques_stmt->execute($quesData);

				#------OPTIONS------
				$ddqi = 0;
				foreach ($ddq_drop as $answerData):
					if ( ! empty($updateDDqAnswerid[$ddqi])):
						$ansSql = "UPDATE `answer_tbl` SET `choice_option`=:choice, `match_option`=:match, `image`=:image, `image2`=:image2, `status`=:status, `uid`=:user_id, `cur_date`=:cur_date WHERE answer_id=:aid";
						$stmt = $db->prepare($ansSql);
						$stmt->execute(array('aid' 		=> $updateDDqAnswerid[$ddqi],
											 'choice'	=> ( ! empty($ddq_drop[$ddqi])) ? htmlspecialchars($ddq_drop[$ddqi]) : '',
											 'match'	=> ( ! empty($ddq_drag[$ddqi])) ? htmlspecialchars($ddq_drag[$ddqi]) : '',
											 'image'	=> ( ! empty($ddq_item_img[$ddqi])) ? $ddq_item_img[$ddqi] : '',
											 'image2'	=> ( ! empty($ddq_drag_item_img[$ddqi])) ? $ddq_drag_item_img[$ddqi] : '',
											 'status'	=> 1,
											 'user_id' 	=> $userId,
											 'cur_date'	=> $curdate));
					else:
						$ansSql = "INSERT INTO `answer_tbl` (`question_id`, `choice_option`, `match_option`, `image`, `image2`, `status`, `uid`, `cur_date`) VALUES (:qid, :choice, :match, :image, :image2, :status, :user_id, :cur_date)";
						$stmt = $db->prepare($ansSql);
						$stmt->execute(array('qid'		=> $ques_id,
											 'choice'	=> ( ! empty($ddq_drop[$ddqi])) ? htmlspecialchars($ddq_drop[$ddqi]) : '',
											 'match'	=> ( ! empty($ddq_drag[$ddqi])) ? htmlspecialchars($ddq_drag[$ddqi]) : '',
											 'image'	=> ( ! empty($ddq_item_img[$ddqi])) ? $ddq_item_img[$ddqi] : '',
											 'image2'	=> ( ! empty($ddq_drag_item_img[$ddqi])) ? $ddq_drag_item_img[$ddqi] : '',
											 'status'	=> 1,
											 'user_id' 	=> $userId,
											 'cur_date'	=> $curdate));
					endif;
					$ddqi++;
				endforeach;
				
				#-----Remove-Old-Feedback-------
				$db->feedback_remove($ques_id);

				#--------Correct-Feedback-------
				$cfeedData = array('qid'			=> ( ! empty($ques_id)) ? $ques_id : '',
								   'feedback'		=> ( ! empty($ddq_cfeedback)) ? htmlspecialchars($ddq_cfeedback) : '',
								   'ftype'			=> 1,
								   'speech_text'	=> ( ! empty($ddq_feedback_text_to_speech)) ? htmlspecialchars($ddq_feedback_text_to_speech) : '',
								   'audio'			=> ( ! empty($feed_audio)) ? $feed_audio : '',
								   'video'			=> ( ! empty($feed_video)) ? $feed_video : '',
								   'screen'			=> ( ! empty($ddq_feedback_rec_screen)) ? $ddq_feedback_rec_screen : '',
								   'image'			=> ( ! empty($ddq_feedback_img)) ? $ddq_feedback_img : '',
								   'document'		=> ( ! empty($ddq_feedback_doc)) ? $ddq_feedback_doc : '');
				$cfeed_sql  = "INSERT INTO `feedback_tbl` (`question_id`, `feedback`, `feedback_type`, `feed_speech_text`, `feed_audio`, `feed_video`, `feed_screen`, `feed_image`, `feed_document`) VALUES (:qid, :feedback, :ftype, :speech_text, :audio, :video, :screen, :image, :document)";
				$cfeed_stmt = $db->prepare($cfeed_sql);
				$cfeed_stmt->execute($cfeedData);

				#--------Incorrect-Feedback-------
				$ifeedData = array('qid'			=> ( ! empty($ques_id)) ? $ques_id : '',
								   'feedback'		=> ( ! empty($ddq_ifeedback)) ? htmlspecialchars($ddq_ifeedback) : '',
								   'ftype'			=> 2,
								   'speech_text'	=> ( ! empty($ddqi_feedback_text_to_speech)) ? htmlspecialchars($ddqi_feedback_text_to_speech) : '',
								   'audio'			=> ( ! empty($ifeed_audio)) ? $ifeed_audio : '',
								   'video'			=> ( ! empty($ifeed_video)) ? $ifeed_video : '',
								   'screen'			=> ( ! empty($ddqi_feedback_rec_screen)) ? $ddqi_feedback_rec_screen : '',
								   'image'			=> ( ! empty($ddqi_feedback_img)) ? $ddqi_feedback_img : '',
								   'document'		=> ( ! empty($ddqi_feedback_doc)) ? $ddqi_feedback_doc : '');

				$ifeed_sql  = "INSERT INTO `feedback_tbl` (`question_id`, `feedback`, `feedback_type`, `feed_speech_text`, `feed_audio`, `feed_video`, `feed_screen`, `feed_image`, `feed_document`) VALUES (:qid, :feedback, :ftype, :speech_text, :audio, :video, :screen, :image, :document)";
				$ifeed_stmt = $db->prepare($ifeed_sql);
				$ifeed_stmt->execute($ifeedData);
			elseif ($seleted_drag_option == 2):
				$quesData = array('questions'		=> ( ! empty($ddq)) ? htmlspecialchars($ddq) : '',
								  'seleted_drag' 	=> ( ! empty($seleted_drag_option)) ? $seleted_drag_option : '',
								  'qtype'			=> ( ! empty($question_type)) ? $question_type : '',
								  'qaudio'			=> ( ! empty($qaudio)) ? $qaudio : '',
								  'qspeech_text'	=> ( ! empty($ddqq_text_to_speech)) ? $ddqq_text_to_speech : '',
								  'speech_text'		=> ( ! empty($ddq_text_to_speech)) ? $ddq_text_to_speech : '',
								  'audio'			=> ( ! empty($audio)) ? $audio : '',
								  'video'			=> ( ! empty($video)) ? $video : '',
								  'screen'			=> ( ! empty($ddq_rec_screen)) ? $ddq_rec_screen : '',
								  'image'			=> ( ! empty($ddq_img)) ? $ddq_img : '',
								  'document'		=> ( ! empty($ddq_doc)) ? $ddq_doc : '',
								  'shuffle'			=> (isset($ddq_shuffle2) && ! empty($ddq_shuffle2)) ? 1 : 0,
								  'true_options'	=> ( ! empty($ddq_true_option)) ? $ddq_true_option : '',
								  'media_file'		=> ( ! empty($videoq_media_file)) ? $videoq_media_file : '',
								  'ques_val_1'		=> ( ! empty($ddq_ques_val_1)) ? $ddq_ques_val_1 : '',
								  'ques_val_2'		=> ( ! empty($ddq_ques_val_2)) ? $ddq_ques_val_2 : '',
								  'ques_val_3'		=> ( ! empty($ddq_ques_val_3)) ? $ddq_ques_val_3 : '',
								  'ques_val_4'		=> ( ! empty($ddq_ques_val_4)) ? $ddq_ques_val_4 : '',
								  'ques_val_5'		=> ( ! empty($ddq_ques_val_5)) ? $ddq_ques_val_5 : '',
								  'ques_val_6'		=> ( ! empty($ddq_ques_val_6)) ? $ddq_ques_val_6 : '',
								  'critical'		=> ( ! empty($ddq_criticalQ)) ? $ddq_criticalQ : '',
								  'status' 			=> 1,
								  'user_id'			=> $userId,
								  'cur_date'		=> $curdate,
								  'qid'				=> $ques_id);
				$ques_sql  = "UPDATE `question_tbl` SET `questions`=:questions, `question_type`=:qtype, `seleted_drag_option`=:seleted_drag, `qaudio`=:qaudio, `qspeech_text`=:qspeech_text, `speech_text`=:speech_text, `audio`=:audio, `video`=:video, `screen`=:screen, `image`=:image, `document`=:document, `shuffle`=:shuffle, `true_options`=:true_options, `videoq_media_file`=:media_file, `ques_val_1`=:ques_val_1, `ques_val_2`=:ques_val_2, `ques_val_3`=:ques_val_3, `ques_val_4`=:ques_val_4, `ques_val_5`=:ques_val_5, `ques_val_6`=:ques_val_6, `critical`=:critical, `status`=:status, `uid`=:user_id, `cur_date`=:cur_date WHERE question_id=:qid";
				$ques_stmt = $db->prepare($ques_sql);
				$ques_stmt->execute($quesData);
				
				#------OPTION------
				if ( ! empty($updateDDq2Answerid)):
					#----DROP-TARGET----
					$ansSql = "UPDATE `sub_options_tbl` SET `sub_option`=:option, `image`=:image WHERE answer_id=:aid";
					$stmt = $db->prepare($ansSql);
					$stmt->execute(array('aid' 		=> $updateDDq2Answerid,
										 'option'	=> ( ! empty($ddq2_drop)) ? htmlspecialchars($ddq2_drop) : '',
										 'image' 	=> ( ! empty($ddq2_item_img)) ? $ddq2_item_img : ''));
					#-----DRAG-ITEMS------
					$ddq2iu = 0; $ddq2true = 1;
					foreach ($ddq2_drag as $subAnswerData):
						if ( ! empty($updateDDq2SubAnswerid[$ddq2iu])):
							$getOpId	   = $updateDDq2SubAnswerid[$ddq2iu];
							$updateAnsSql  = "UPDATE `answer_tbl` SET `choice_option`=:option, `image`=:image, `true_option`=:true, `status`=:status, `uid`=:user_id, `cur_date`=:cur_date WHERE answer_id=:aid";
							$updateAnsstmt = $db->prepare($updateAnsSql);
							$updateAnsstmt->execute(array('aid' 		=> $getOpId,
														  'option'		=> ( ! empty($ddq2_drag[$ddq2iu])) ? htmlspecialchars($ddq2_drag[$ddq2iu]) : '',
														  'image'		=> ( ! empty($ddq2_drag_item_img[$ddq2iu])) ? $ddq2_drag_item_img[$ddq2iu] : '',
														  'true'		=> ( ! empty($ddq_true_option) && $ddq_true_option == $ddq2true) ? 1 : '',
														  'status'		=> 1,
														  'user_id' 	=> $userId,
														  'cur_date'	=> $curdate));
						else:
							$ansSql = "INSERT INTO `answer_tbl` (`question_id`, `choice_option`, `image`, `true_option`, `status`, `uid`, `cur_date`) VALUES (:qid, :option, :image, :true, :status, :user_id, :cur_date)";
							$stmt 	= $db->prepare($ansSql);
							$stmt->execute(array('qid'		=> $ques_id,
												 'option'	=> ( ! empty($ddq2_drag[$ddq2iu])) ? htmlspecialchars($ddq2_drag[$ddq2iu]) : '',
												 'image'	=> ( ! empty($ddq2_drag_item_img[$ddq2iu])) ? $ddq2_drag_item_img[$ddq2iu] : '',
												 'true'		=> ( ! empty($ddq_true_option) && $ddq_true_option == $ddq2true) ? 1 : '',
												 'status'	=> 1,
												 'user_id' 	=> $userId,
												 'cur_date'	=> $curdate));
						endif;
						$ddq2iu++; $ddq2true++;
					endforeach;
				else:
					#----DROP-TARGET----
					$ansSql = "INSERT INTO `sub_options_tbl` (`question_id`, `sub_option`, `image`) VALUES (:qid, :option, :image)";
					$stmt   = $db->prepare($ansSql);
					$stmt->execute(array('qid' 		=> $ques_id,
										 'option'	=> ( ! empty($ddq2_drop)) ? htmlspecialchars($ddq2_drop) : '',
										 'image' 	=> ( ! empty($ddq2_item_img)) ? $ddq2_item_img : ''));
					#-----DRAG-ITEMS------
					$ddq2i = 0; $ddq2itrue = 1;
					foreach ($ddq2_drag as $answerData):
						$ansSql = "INSERT INTO `answer_tbl` (`question_id`, `choice_option`, `image`, `true_option`, `status`, `uid`, `cur_date`) VALUES (:qid, :choice, :image, :true, :status, :user_id, :cur_date)";
						$stmt = $db->prepare($ansSql);
						$stmt->execute(array('qid'		=> $ques_id,
											 'choice'	=> ( ! empty($ddq2_drag[$ddq2i])) ? htmlspecialchars($ddq2_drag[$ddq2i]) : '',
											 'image'	=> ( ! empty($ddq2_drag_item_img[$ddq2i])) ? $ddq2_drag_item_img[$ddq2i] : '',
											 'true'		=> ( ! empty($ddq_true_option) && $ddq_true_option == $ddq2itrue) ? 1 : '',
											 'status'	=> 1,
											 'user_id' 	=> $userId,
											 'cur_date'	=> $curdate));
						$ddq2i++; $ddq2itrue++;
					endforeach;
				endif;

				#-----Remove-Old-Feedback-------
				$db->feedback_remove($ques_id);

				#--------Correct-Feedback-------
				$cfeedData = array('qid'			=> ( ! empty($ques_id)) ? $ques_id : '',
								   'feedback'		=> ( ! empty($ddq_cfeedback)) ? htmlspecialchars($ddq_cfeedback) : '',
								   'ftype'			=> 1,
								   'speech_text'	=> ( ! empty($ddq_feedback_text_to_speech)) ? htmlspecialchars($ddq_feedback_text_to_speech) : '',
								   'audio'			=> ( ! empty($feed_audio)) ? $feed_audio : '',
								   'video'			=> ( ! empty($feed_video)) ? $feed_video : '',
								   'screen'			=> ( ! empty($ddq_feedback_rec_screen)) ? $ddq_feedback_rec_screen : '',
								   'image'			=> ( ! empty($ddq_feedback_img)) ? $ddq_feedback_img : '',
								   'document'		=> ( ! empty($ddq_feedback_doc)) ? $ddq_feedback_doc : '');
				$cfeed_sql  = "INSERT INTO `feedback_tbl` (`question_id`, `feedback`, `feedback_type`, `feed_speech_text`, `feed_audio`, `feed_video`, `feed_screen`, `feed_image`, `feed_document`) VALUES (:qid, :feedback, :ftype, :speech_text, :audio, :video, :screen, :image, :document)";
				$cfeed_stmt = $db->prepare($cfeed_sql);
				$cfeed_stmt->execute($cfeedData);

				#--------Incorrect-Feedback-------
				$ifeedData = array('qid'			=> ( ! empty($ques_id)) ? $ques_id : '',
								   'feedback'		=> ( ! empty($ddq_ifeedback)) ? htmlspecialchars($ddq_ifeedback) : '',
								   'ftype'			=> 2,
								   'speech_text'	=> ( ! empty($ddqi_feedback_text_to_speech)) ? htmlspecialchars($ddqi_feedback_text_to_speech) : '',
								   'audio'			=> ( ! empty($ifeed_audio)) ? $ifeed_audio : '',
								   'video'			=> ( ! empty($ifeed_video)) ? $ifeed_video : '',
								   'screen'			=> ( ! empty($ddqi_feedback_rec_screen)) ? $ddqi_feedback_rec_screen : '',
								   'image'			=> ( ! empty($ddqi_feedback_img)) ? $ddqi_feedback_img : '',
								   'document'		=> ( ! empty($ddqi_feedback_doc)) ? $ddqi_feedback_doc : '');
				$ifeed_sql  = "INSERT INTO `feedback_tbl` (`question_id`, `feedback`, `feedback_type`, `feed_speech_text`, `feed_audio`, `feed_video`, `feed_screen`, `feed_image`, `feed_document`) VALUES (:qid, :feedback, :ftype, :speech_text, :audio, :video, :screen, :image, :document)";
				$ifeed_stmt = $db->prepare($ifeed_sql);
				$ifeed_stmt->execute($ifeedData);
			endif;
		endif;
	endif;
	if ( ! empty($submit_type) && ($submit_type == 1)):
		$resdata = array('success' => TRUE, 'all_update' => FALSE, 'msg' => 'Simulation details updated successfully.', 'return_url' => 'linear-video-multiple-template.php?add_data=true&sim_id='.md5($sim_id).'&ques_id='.base64_encode($ques_id));
	else:
		$resdata = array('success' => TRUE, 'all_update' => TRUE, 'msg' => 'All questions and answers updated successfully.', 'return_url' => 'author-simulation.php');
	endif;
	echo json_encode($resdata);
endif;

#-----------------------------------------Add-Update-Branching-Milti-Sim ----------------------------------
if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['update_branching_sim']) && isset($_POST['sim_id'])):
	extract($_POST);
	$error = FALSE;
	if (empty($sim_page_name[0]) || empty($sim_duration) || empty($sim_title) || empty($passing_marks)):
		$resdata = array('success' => FALSE, 'msg' => 'Please fill all the mandatory fields on add Simulation and Simulation details to proceed.');
		$error = TRUE;
		echo json_encode($resdata); exit;
	elseif (isset($competency_name_1) && empty($competency_name_1) || empty($competency_score_1) || empty($weightage_1)):
		$resdata = array('success' => FALSE, 'msg' => 'Please fill all the mandatory fields on Simulation details to proceed.');
		$error = TRUE;
		echo json_encode($resdata); exit;
	elseif ($error == FALSE):
		$sql = "UPDATE scenario_master SET `Scenario_title` = '". $sim_title ."', `duration` = '". $sim_duration ."', `passing_marks` = '". $passing_marks ."', `sim_cover_img` = '". $scenario_cover_file ."'";
		if (isset($character_type) && $character_type == 1):
			$sql .= ", sim_char_img = '', sim_char_left_img = '', sim_char_right_img = '', char_own_choice = ''";
		elseif (isset($charoption) && $charoption == 1 && isset($scenario_char_file) && ! empty($scenario_char_file)):
			$sql .= ", char_own_choice = '". $scenario_char_file ."', sim_char_img = '', sim_char_left_img = '', sim_char_right_img = ''";
		elseif (isset($charoption) && $charoption == 2 && isset($sim_char_img) && ! empty($sim_char_img)):
			$sql .= ", sim_char_img = '". $sim_char_img ."', sim_char_left_img = '', sim_char_right_img = ''";
		elseif (isset($sim_two_char_img) && count($sim_two_char_img) > 0):
			if (isset($sim_two_char_img[0])):
				$r = explode('|', $sim_two_char_img[0]);
				if (isset($r[1]) && $r[1] == 'R'):
					$sql .= ", sim_char_img = '', char_own_choice = '', sim_char_right_img = '". $r[0] ."'";
				elseif (isset($r[1]) && $r[1] == 'L'):
					$sql .= ", sim_char_img = '', char_own_choice = '', sim_char_left_img = '". $r[0] ."'";
				endif;
			endif;
			if (isset($sim_two_char_img[1])):
				$l = explode('|', $sim_two_char_img[1]);
				if (isset($l[1]) && $l[1] == 'L'):
					$sql .= ", sim_char_img = '', char_own_choice = '', sim_char_left_img = '". $l[0] ."'";
				elseif (isset($l[1]) && $l[1] == 'R'):
					$sql .= ", sim_char_img = '', char_own_choice = '', sim_char_right_img = '". $l[0] ."'";
				endif;
			endif;
		endif;
		#--Add-Web-Object--
		if ( ! empty($web_object)):
			$sql .= ", web_object = '". $web_object ."', web_object_title = '". htmlspecialchars($web_object_title) ."'";
		endif;
		#--Sim-Media-File--
		if ( ! empty($scenario_media_file) && in_array(strtolower(pathinfo($scenario_media_file, PATHINFO_EXTENSION)), $img_valid_exts)):
			$sql .= ", image_url = '". $scenario_media_file ."', video_url = NULL, audio_url = NULL";
		elseif ( ! empty($scenario_media_file) && in_array(strtolower(pathinfo($scenario_media_file, PATHINFO_EXTENSION)), $video_valid_exts)):
			$sql .= ", video_url = '". $scenario_media_file ."', image_url = NULL, audio_url = NULL";
		elseif ( ! empty($scenario_media_file) && in_array(strtolower(pathinfo($scenario_media_file, PATHINFO_EXTENSION)), $audio_valid_exts)):
			$sql .= ", audio_url = '". $scenario_media_file ."', video_url = NULL, image_url = NULL";
		elseif ( ! empty($scenario_media_file) && ! in_array(strtolower(pathinfo($scenario_media_file, PATHINFO_EXTENSION)), $audio_valid_exts) && 
				! in_array(strtolower(pathinfo($scenario_media_file, PATHINFO_EXTENSION)), $img_valid_exts) && 
				! in_array(strtolower(pathinfo($scenario_media_file, PATHINFO_EXTENSION)), $video_valid_exts)):
			$sql .= ", web_object_file = '". $scenario_media_file ."', video_url = NULL, image_url = NULL, audio_url = NULL";
		endif;
		#--Sim-Backgrounds--
		if (isset($bgoption) && $bgoption == 1 && ! empty($bg_color)):
			$sql .= ", bg_color = '". $bg_color ."', bg_own_choice = '', sim_back_img = ''";
		elseif (isset($bgoption) && $bgoption == 2 && ! empty($scenario_bg_file)):
			$sql .= ", bg_own_choice = '". $scenario_bg_file ."', bg_color = '', sim_back_img = ''";
		elseif (isset($bgoption) && $bgoption == 3 && ! empty($sim_background_img)):
			$sql .= ", sim_back_img = '". $sim_background_img ."', bg_own_choice = '', bg_color = ''";
		endif;
		$sql .= " WHERE scenario_id = '". $sim_id ."'";
		$results = $db->prepare($sql);
		if ($results->execute()):
			#----Competency----
			$delCom	 = "DELETE FROM competency_tbl WHERE scenario_id = '". $sim_id ."'";
			$delComq = $db->prepare($delCom); $delComq->execute();
			$data	 = array('scenario_id'	=> $sim_id,
							 'comp_col_1'	=> (isset($competency_name_1) && ! empty($competency_name_1)) ? addslashes($competency_name_1) : '',
							 'comp_val_1'	=> (isset($competency_score_1) && ! empty($competency_score_1)) ? addslashes($competency_score_1) : '',
							 'comp_col_2'	=> (isset($competency_name_2) && ! empty($competency_name_2)) ? addslashes($competency_name_2) : '',
							 'comp_val_2'	=> (isset($competency_score_2) && ! empty($competency_score_2)) ? $competency_score_2 : '',
							 'comp_col_3'	=> (isset($competency_name_3) && ! empty($competency_name_3)) ? addslashes($competency_name_3) : '',
							 'comp_val_3'	=> (isset($competency_score_3) && ! empty($competency_score_3)) ? $competency_score_3 : '',
							 'comp_col_4'	=> (isset($competency_name_4) && ! empty($competency_name_4)) ? addslashes($competency_name_4) : '',
							 'comp_val_4'	=> (isset($competency_score_4) && ! empty($competency_score_4)) ? $competency_score_4 : '',
							 'comp_col_5'	=> (isset($competency_name_5) && ! empty($competency_name_5)) ? addslashes($competency_name_5) : '',
							 'comp_val_5'	=> (isset($competency_score_5) && ! empty($competency_score_5)) ? $competency_score_5 : '',
							 'comp_col_6'	=> (isset($competency_name_6) && ! empty($competency_name_6)) ? addslashes($competency_name_6) : '',
							 'comp_val_6'	=> (isset($competency_score_6) && ! empty($competency_score_6)) ? $competency_score_6 : '',
							 'weightage_1'	=> (isset($weightage_1) && ! empty($weightage_1)) ? $weightage_1 : '',
							 'weightage_2'	=> (isset($weightage_2) && ! empty($weightage_2)) ? $weightage_2 : '',
							 'weightage_3'	=> (isset($weightage_3) && ! empty($weightage_3)) ? $weightage_3 : '',
							 'weightage_4'	=> (isset($weightage_4) && ! empty($weightage_4)) ? $weightage_4 : '',
							 'weightage_5'	=> (isset($weightage_5) && ! empty($weightage_5)) ? $weightage_5 : '',
							 'weightage_6'	=> (isset($weightage_6) && ! empty($weightage_6)) ? $weightage_6 : '',
							 'status' 		=> 1,
							 'user_id' 		=> $userId,
							 'cur_date'		=> $curdate);
			$com_sql  = "INSERT INTO `competency_tbl` (`scenario_id`, `comp_col_1`, `comp_val_1`, `comp_col_2`, `comp_val_2`, `comp_col_3`, `comp_val_3`, `comp_col_4`, `comp_val_4`, `comp_col_5`, `comp_val_5`, `comp_col_6`, `comp_val_6`, `weightage_1`, `weightage_2`, `weightage_3`, `weightage_4`, `weightage_5`, `weightage_6`, `status`, `uid`, `cur_date`) VALUES (:scenario_id, :comp_col_1, :comp_val_1, :comp_col_2, :comp_val_2, :comp_col_3, :comp_val_3, :comp_col_4, :comp_val_4, :comp_col_5, :comp_val_5, :comp_col_6, :comp_val_6, :weightage_1, :weightage_2, :weightage_3, :weightage_4, :weightage_5, :weightage_6, :status, :user_id, :cur_date)";
			$com_stmt = $db->prepare($com_sql);
			$com_stmt->execute($data);
			
			#----PAGES----
			$delPage  = "DELETE FROM sim_pages WHERE scenario_id = '". $sim_id ."'";
			$delPageq = $db->prepare($delPage); $delPageq->execute();
			if ( ! empty($sim_page_name)): $sp = 0;
				foreach ($sim_page_name as $pdata):
					$pageq 		= "INSERT INTO `sim_pages` (`scenario_id`, `sim_page_name`, `sim_page_desc`, `userid`, `status`, `added_date`) VALUES (:sid, :pname, :pdesc, :user_id, :status, :adate)";
					$page_stmt	= $db->prepare($pageq);
					$page_stmt->execute(array('sid'		=> $sim_id,
											  'pname'	=> ( ! empty($pdata)) ? htmlspecialchars($pdata) : '',
											  'pdesc'	=> ( ! empty($sim_page_desc[$sp])) ? $sim_page_desc[$sp] : '',
											  'status' 	=> 1,
											  'user_id'	=> $userId,
											  'adate'	=> $curdate));
				$sp++; endforeach;
			endif;
			
			#----Branding----
			$bandq	  	= "DELETE FROM sim_branding_tbl WHERE scenario_id = '". $sim_id ."'";
			$bandqexe 	= $db->prepare($bandq); $bandqexe->execute();
			$brand_data = array('sid'				=> $sim_id,
								'bg'				=> ( ! empty($ques_bg_color)) ? $ques_bg_color : '',
								'bg_tr'				=> ( ! empty($ques_bg_transp)) ? $ques_bg_transp : '',
								'obg'				=> ( ! empty($ques_option_bg_color)) ? $ques_option_bg_color : '',
								'obg_tr'			=> ( ! empty($ques_option_bg_transp)) ? $ques_option_bg_transp : '',
								'ohover'			=> ( ! empty($ques_option_hover_color)) ? $ques_option_hover_color : '',
								'oh_tr'				=> ( ! empty($ques_option_hover_transp)) ? $ques_option_hover_transp : '',
								'oselected'			=> ( ! empty($ques_option_select_color)) ? $ques_option_select_color : '',
								'oselected_tr'		=> ( ! empty($ques_option_select_transp)) ? $ques_option_select_transp : '',
								'btn_bg'			=> ( ! empty($btn_bg_color)) ? $btn_bg_color : '',
								'btn_bg_tr'			=> ( ! empty($btn_bg_transp)) ? $btn_bg_transp : '',
								'btn_hover'			=> ( ! empty($btn_hover_color)) ? $btn_hover_color : '',
								'btn_hover_tr'		=> ( ! empty($btn_hover_transp)) ? $btn_hover_transp : '',
								'btn_selected'		=> ( ! empty($btn_select_color)) ? $btn_select_color : '',
								'btn_selected_tr'	=> ( ! empty($btn_select_transp)) ? $btn_select_transp : '',
								'font_type'			=> ( ! empty($font_type)) ? $font_type : '',
								'font_color'		=> ( ! empty($font_color)) ? $font_color : '',
								'font_size'			=> ( ! empty($font_size)) ? $font_size : '',
								'user_id' 			=> $userId,
								'added_date'		=> $curdate);
			$com_sql  = "INSERT INTO `sim_branding_tbl` (`scenario_id`, `ques_bg`, `ques_bg_transparency`, `option_bg`, `option_bg_transparency`, `option_hover`, `option_hover_transparency`, `option_selected`, `option_selected_transparency`, `btn_bg`, `btn_bg_transparency`, `btn_hover`, `btn_hover_transparency`, `btn_selected`, `btn_selected_transparency`, `font_type`, `font_color`, `font_size`, `user_id`, `added_date`) 
						 VALUES (:sid, :bg, :bg_tr, :obg, :obg_tr, :ohover, :oh_tr, :oselected, :oselected_tr, :btn_bg, :btn_bg_tr, :btn_hover, :btn_hover_tr, :btn_selected, :btn_selected_tr, :font_type, :font_color, :font_size, :user_id, :added_date)";
			$com_stmt = $db->prepare($com_sql); $com_stmt->execute($brand_data);
		endif;
		//------START-MATCH-THE-FOLLOWING-----
		if ($question_type == 1):
			#-------QUESTIONS-DATA---
			$qaudio = '';
			if ( ! empty($mtfq_qaudio)):
				$qaudio = $mtfq_qaudio;
			elseif ( ! empty($mtfq_rec_audio)):
				$qaudio = $mtfq_rec_audio;
			endif;			
			$audio = '';
			if ( ! empty($mtf_qaudio)):
				$audio = $mtf_qaudio;
			elseif ( ! empty($mtf_rec_audio)):
				$audio = $mtf_rec_audio;
			endif;			
			$video = '';
			if ( ! empty($mtf_video)):
				$video = $mtf_video;
			elseif ( ! empty($mtf_rec_video)):
				$video = $mtf_rec_video;
			endif;			
			#------Correct-FEEDBACK-DATA---
			$feed_audio = '';
			if ( ! empty($mtf_feedback_audio)):
				$feed_audio = $mtf_feedback_audio;
			elseif ( ! empty($mtf_feedback_rec_audio)):
				$feed_audio = $mtf_feedback_rec_audio;
			endif;			
			$feed_video = '';
			if ( ! empty($mtf_feedback_video)):
				$feed_video = $mtf_feedback_video;
			elseif ( ! empty($mtf_feedback_rec_video)):
				$feed_video = $mtf_feedback_rec_video;
			endif;			
			#------Incorrect-FEEDBACK-DATA---
			$ifeed_audio = '';
			if ( ! empty($mtfi_feedback_audio)):
				$ifeed_audio = $mtfi_feedback_audio;
			elseif ( ! empty($mtfi_feedback_rec_audio)):
				$ifeed_audio = $mtfi_feedback_rec_audio;
			endif;			
			$ifeed_video = '';
			if ( ! empty($mtfi_feedback_video)):
				$ifeed_video = $mtfi_feedback_video;
			elseif ( ! empty($mtfi_feedback_rec_video)):
				$ifeed_video = $mtfi_feedback_rec_video;
			endif;			
			$quesData = array('questions'		=> ( ! empty($mtfq)) ? htmlspecialchars($mtfq) : '',
							  'qtype'			=> ( ! empty($question_type)) ? $question_type : '',
							  'qaudio'			=> ( ! empty($qaudio)) ? $qaudio : '',
							  'qspeech_text'	=> ( ! empty($mtfqq_text_to_speech)) ? htmlspecialchars($mtfqq_text_to_speech) : '',
							  'speech_text'		=> ( ! empty($mtfq_text_to_speech)) ? htmlspecialchars($mtfq_text_to_speech) : '',
							  'audio'			=> ( ! empty($audio)) ? $audio : '',
							  'video'			=> ( ! empty($video)) ? $video : '',
							  'screen'			=> ( ! empty($mtf_rec_screen)) ? $mtf_rec_screen : '',
							  'image'			=> ( ! empty($mtf_img)) ? $mtf_img : '',
							  'document'		=> ( ! empty($mtf_doc)) ? $mtf_doc : '',
							  'ques_val_1'		=> ( ! empty($ques_val_1)) ? $ques_val_1 : '',
							  'ques_val_2'		=> ( ! empty($ques_val_2)) ? $ques_val_2 : '',
							  'ques_val_3'		=> ( ! empty($ques_val_3)) ? $ques_val_3 : '',
							  'ques_val_4'		=> ( ! empty($ques_val_4)) ? $ques_val_4 : '',
							  'ques_val_5'		=> ( ! empty($ques_val_5)) ? $ques_val_5 : '',
							  'ques_val_6'		=> ( ! empty($ques_val_6)) ? $ques_val_6 : '',
							  'critical'		=> ( ! empty($mtf_criticalQ)) ? $mtf_criticalQ : '',
							  'status' 			=> 1,
							  'user_id'			=> $userId,
							  'cur_date'		=> $curdate,
							  'qid'				=> $ques_id);
			$ques_sql  = "UPDATE `question_tbl` SET `questions`=:questions, `question_type`=:qtype, `qaudio`=:qaudio, `qspeech_text`=:qspeech_text, `speech_text`=:speech_text, `audio`=:audio, `video`=:video, `screen`=:screen, `image`=:image, `document`=:document, `ques_val_1`=:ques_val_1, `ques_val_2`=:ques_val_2, `ques_val_3`=:ques_val_3, `ques_val_4`=:ques_val_4, `ques_val_5`=:ques_val_5, `ques_val_6`=:ques_val_6, `critical`=:critical, `status`=:status, `uid`=:user_id, `cur_date`=:cur_date WHERE question_id=:qid";
			$ques_stmt = $db->prepare($ques_sql); $ques_stmt->execute($quesData);
			#-----Remove-Old-Feedback-------
			$db->feedback_remove($ques_id);			
			#--------Correct-Feedback----------
			if (isset($chkCorMTFEndSim) && $chkCorMTFEndSim > 0){
				$mappedQuestionMtfCorrect = '';
			}
			$cfeedData 				= array(
				'qid'				=> ( ! empty($ques_id)) ? $ques_id : '',
				'nextQid'			=> ( ! empty($mappedQuestionMtfCorrect)) ? $mappedQuestionMtfCorrect : '',
				'EndSim'			=> (isset($chkCorMTFEndSim) && ! empty($chkCorMTFEndSim)) ? 1 : 0,
				'feedback'			=> ( ! empty($mtf_cfeedback)) ? addslashes($mtf_cfeedback) : '',
				'ftype'				=> 1,
				'speech_text'		=> ( ! empty($mtf_feedback_text_to_speech)) ? addslashes($mtf_feedback_text_to_speech) : '',
				'audio'				=> ( ! empty($feed_audio)) ? $feed_audio : '',
				'video'				=> ( ! empty($feed_video)) ? $feed_video : '',
				'screen'			=> ( ! empty($mtf_feedback_rec_screen)) ? $mtf_feedback_rec_screen : '',
				'image'				=> ( ! empty($mtf_feedback_img)) ? $mtf_feedback_img : '',
				'document'			=> ( ! empty($mtf_feedback_doc)) ? $mtf_feedback_doc : ''
			);		
			$cfeed_sql  			= "INSERT INTO `feedback_tbl` (`question_id`, `nextQid`,`EndSim`,`feedback`, `feedback_type`, `feed_speech_text`, `feed_audio`, `feed_video`, `feed_screen`, `feed_image`, `feed_document`) VALUES (:qid, :nextQid,:EndSim,:feedback, :ftype, :speech_text, :audio, :video, :screen, :image, :document)";
			$cfeed_stmt 			= $db->prepare($cfeed_sql);
			$cfeed_stmt->execute($cfeedData);			
			#--------Incorrect-Feedback-------
			if (isset($chkInMTFEndSim) && $chkInMTFEndSim > 0){
				$mappedQuestionMtfIncorrect = '';
			}
			$ifeedData 				= array(
				'qid'				=> ( ! empty($ques_id)) ? $ques_id : '',
				'nextQid'			=> ( ! empty($mappedQuestionMtfIncorrect)) ? $mappedQuestionMtfIncorrect : '',
				'EndSim'			=> (isset($chkInMTFEndSim) && ! empty($chkInMTFEndSim)) ? 1 : 0,
				'feedback'			=> ( ! empty($mtf_ifeedback)) ? addslashes($mtf_ifeedback) : '',
				'ftype'				=> 2,
				'speech_text'		=> ( ! empty($mtfi_feedback_text_to_speech)) ? addslashes($mtfi_feedback_text_to_speech) : '',
				'audio'				=> ( ! empty($ifeed_audio)) ? $ifeed_audio : '',
				'video'				=> ( ! empty($ifeed_video)) ? $ifeed_video : '',
				'screen'			=> ( ! empty($mtfi_feedback_rec_screen)) ? $mtfi_feedback_rec_screen : '',
				'image'				=> ( ! empty($mtfi_feedback_img)) ? $mtfi_feedback_img : '',
				'document'			=> ( ! empty($mtfi_feedback_doc)) ? $mtfi_feedback_doc : ''
			);			
			$ifeed_sql  			= "INSERT INTO `feedback_tbl` (`question_id`,  `nextQid`,`EndSim`,`feedback`, `feedback_type`, `feed_speech_text`, `feed_audio`, `feed_video`, `feed_screen`, `feed_image`, `feed_document`) VALUES (:qid,:nextQid,:EndSim, :feedback, :ftype, :speech_text, :audio, :video, :screen, :image, :document)";
			$ifeed_stmt 			= $db->prepare($ifeed_sql);
			$ifeed_stmt->execute($ifeedData);			
			#----------Shuffle-OFF--------------
			if ( ! empty($mtf_choice[0]) && ! empty($mtf_choice_order[0]) && ! empty($mtf_match_order[0]) && ! empty($mtf_match[0]) && ! empty($ques_id)):
				$soff 							= 0;
				foreach ($mtf_choice as $answerData):
					if ( ! empty($updateAnswerid[$soff])):
						$ansSql 				= "UPDATE `answer_tbl` SET `choice_option`=:choice_option, `choice_order_no`=:choice_order_no, `match_order_no`=:match_order_no, `match_option`=:match_option, `status`=:status, `uid`=:user_id, `cur_date`=:cur_date WHERE answer_id=:answer_id";
						$stmt 					= $db->prepare($ansSql);
						$stmt->execute(array(
							'answer_id' 		=> $updateAnswerid[$soff],
							'choice_option'		=> ( ! empty($mtf_choice[$soff])) ? $mtf_choice[$soff] : '',
							'choice_order_no'	=> ( ! empty($mtf_choice_order[$soff])) ? $mtf_choice_order[$soff] : '',
							'match_order_no'	=> ( ! empty($mtf_match_order[$soff])) ? $mtf_match_order[$soff] : '',
							'match_option'		=> ( ! empty($mtf_match[$soff])) ? $mtf_match[$soff] : '',
							'status' 			=> 1,
							'user_id' 			=> $userId,
							'cur_date'			=> $curdate
						));
					else:
						$ansSql 				= "INSERT INTO `answer_tbl` (`question_id`, `choice_option`, `choice_order_no`, `match_order_no`, `match_option`, `status`, `uid`, `cur_date`) 
						values (:qid, :choice_option, :choice_order_no, :match_order_no, :match_option, :status, :user_id, :cur_date)";
						$stmt 					= $db->prepare($ansSql);
						$stmt->execute(array(
							'qid'				=> $ques_id,
							'choice_option'		=> ( ! empty($mtf_choice[$soff])) ? addslashes($mtf_choice[$soff]) : '',
							'choice_order_no'	=> ( ! empty($mtf_choice_order[$soff])) ? $mtf_choice_order[$soff] : '',
							'match_order_no'	=> ( ! empty($mtf_match_order[$soff])) ? $mtf_match_order[$soff] : '',
							'match_option'		=> ( ! empty($mtf_match[$soff])) ? $mtf_match[$soff] : '',
							'status' 			=> 1,
							'user_id' 			=> $userId,
							'cur_date'			=> $curdate
						));
					endif;
				$soff++;
				endforeach;
			endif;			
			#----------Shuffle-ON--------------
			if ( ! empty($mtf_choice2[0]) && ! empty($mtf_match2[0]) && empty($mtf_match_order[0]) && empty($mtf_match[0])):
				$sON = 0;
				foreach ($mtf_choice2 as $answerData):
					if ( ! empty($updateAnswerid[$sON])):
						$ansSql = "UPDATE `answer_tbl` SET `choice_option`=:choice_option, `match_option`=:match_option, `status`=:status, `uid`=:user_id, `cur_date`=:cur_date WHERE answer_id=:answer_id";
						$stmt 	= $db->prepare($ansSql);
						$stmt->execute(array('answer_id' 		=> $updateAnswerid[$sON],
											 'choice_option'	=> ( ! empty($mtf_choice2[$sON])) ? $mtf_choice2[$sON] : '',
											 'match_option'		=> ( ! empty($mtf_match2[$sON])) ? $mtf_match2[$sON] : '',
											 'status' 			=> 1,
											 'user_id' 			=> $userId,
											 'cur_date'			=> $curdate));
					else:
						$ansSql = "INSERT INTO `answer_tbl` (`question_id`, `choice_option`, `match_option`, `status`, `uid`, `cur_date`) 
						values (:qid, :choice_option, :match_option, :status, :user_id, :cur_date)";
						$stmt = $db->prepare($ansSql);
						$stmt->execute(array('qid'				=> $ques_id,
											 'choice_option'	=> ( ! empty($mtf_choice2[$sON])) ? addslashes($mtf_choice2[$sON]) : '',
											 'match_option'		=> ( ! empty($mtf_match2[$sON])) ? addslashes($mtf_match2[$sON]) : '',
											 'status' 			=> 1,
											 'user_id' 			=> $userId,
											 'cur_date'			=> $curdate));
					endif;
					$sON++;
				endforeach;
			endif;
		//------END-MATCH-THE-FOLLOWING------		
		//-----------START-SEQUENCE--------
		elseif ($question_type == 2):
			#-------QUESTIONS-DATA---
			$qaudio = '';
			if ( ! empty($sqq_qaudio)):
				$qaudio = $sqq_qaudio;
			elseif ( ! empty($sqq_rec_audio)):
				$qaudio = $sqq_rec_audio;
			endif;
			
			$audio = '';
			if ( ! empty($sq_qaudio)):
				$audio = $sq_qaudio;
			elseif ( ! empty($sq_rec_audio)):
				$audio = $sq_rec_audio;
			endif;
			
			$video = '';
			if ( ! empty($sq_video)):
				$video = $sq_video;
			elseif ( ! empty($sq_rec_video)):
				$video = $sq_rec_video;
			endif;
			
			#------Correct-FEEDBACK-DATA---
			$feed_audio = '';
			if ( ! empty($sq_feedback_audio)):
				$feed_audio = $sq_feedback_audio;
			elseif ( ! empty($sq_feedback_rec_audio)):
				$feed_audio = $sq_feedback_rec_audio;
			endif;
			
			$feed_video = '';
			if ( ! empty($sq_feedback_video)):
				$feed_video = $sq_feedback_video;
			elseif ( ! empty($sq_feedback_rec_video)):
				$feed_video = $sq_feedback_rec_video;
			endif;
			
			#------Incorrect-FEEDBACK-DATA---
			$ifeed_audio = '';
			if ( ! empty($sqi_feedback_audio)):
				$ifeed_audio = $sqi_feedback_audio;
			elseif ( ! empty($sqi_feedback_rec_audio)):
				$ifeed_audio = $sqi_feedback_rec_audio;
			endif;
			
			$ifeed_video = '';
			if ( ! empty($sqi_feedback_video)):
				$ifeed_video = $sqi_feedback_video;
			elseif ( ! empty($sqi_feedback_rec_video)):
				$ifeed_video = $sqi_feedback_rec_video;
			endif;
			
			$quesData = array('questions'		=> ( ! empty($sq)) ? addslashes($sq) : '',
							  'qtype'			=> ( ! empty($question_type)) ? $question_type : '',
							  'qaudio'			=> ( ! empty($qaudio)) ? $qaudio : '',
							  'qspeech_text'	=> ( ! empty($sqq_text_to_speech)) ? $sqq_text_to_speech : '',
							  'speech_text'		=> ( ! empty($sq_text_to_speech)) ? $sq_text_to_speech : '',
							  'audio'			=> ( ! empty($audio)) ? $audio : '',
							  'video'			=> ( ! empty($video)) ? $video : '',
							  'screen'			=> ( ! empty($sq_rec_screen)) ? $sq_rec_screen : '',
							  'image'			=> ( ! empty($sq_img)) ? $sq_img : '',
							  'document'		=> ( ! empty($sq_doc)) ? $sq_doc : '',
							  'shuffle'			=> (isset($sq_shuffle) && ! empty($sq_shuffle)) ? 1 : 0,
							  'ques_val_1'		=> ( ! empty($sq_ques_val_1)) ? $sq_ques_val_1 : '',
							  'ques_val_2'		=> ( ! empty($sq_ques_val_2)) ? $sq_ques_val_2 : '',
							  'ques_val_3'		=> ( ! empty($sq_ques_val_3)) ? $sq_ques_val_3 : '',
							  'ques_val_4'		=> ( ! empty($sq_ques_val_4)) ? $sq_ques_val_4 : '',
							  'ques_val_5'		=> ( ! empty($sq_ques_val_5)) ? $sq_ques_val_5 : '',
							  'ques_val_6'		=> ( ! empty($sq_ques_val_6)) ? $sq_ques_val_6 : '',
							  'critical'		=> ( ! empty($sq_criticalQ)) ? $sq_criticalQ : '',
							  'status' 			=> 1,
							  'user_id'			=> $userId,
							  'cur_date'		=> $curdate,
							  'qid'				=> $ques_id);
			
			$ques_sql  = "UPDATE `question_tbl` SET `questions`=:questions, `question_type`=:qtype, `qaudio`=:qaudio, `qspeech_text`=:qspeech_text, `speech_text`=:speech_text, `audio`=:audio, `video`=:video, `screen`=:screen, `image`=:image, `document`=:document, `shuffle`=:shuffle, `ques_val_1`=:ques_val_1, `ques_val_2`=:ques_val_2, `ques_val_3`=:ques_val_3, `ques_val_4`=:ques_val_4, `ques_val_5`=:ques_val_5, `ques_val_6`=:ques_val_6, `critical`=:critical, `status`=:status, `uid`=:user_id, `cur_date`=:cur_date WHERE question_id=:qid";
			$ques_stmt = $db->prepare($ques_sql);
			$ques_stmt->execute($quesData);
			
			#-----Remove-Old-Feedback-------
			$db->feedback_remove($ques_id);
			
			#--------Correct-Feedback----------
			if (isset($chkCorSWQEndSim) && $chkCorSWQEndSim > 0){
				$mappedQuestionSEQCorrect = '';
			}
			$cfeedData = array('qid'			=> ( ! empty($ques_id)) ? $ques_id : '',
								'nextQid'		=> ( ! empty($mappedQuestionSEQCorrect)) ? $mappedQuestionSEQCorrect : '',
								'EndSim'		=> ( ! empty($chkCorSWQEndSim)) ? $chkCorSWQEndSim : '',
							   'feedback'		=> ( ! empty($sq_feedback)) ? addslashes($sq_feedback) : '',
							   'ftype'			=> 1,
							   'speech_text'	=> ( ! empty($sq_feedback_text_to_speech)) ? addslashes($sq_feedback_text_to_speech) : '',
							   'audio'			=> ( ! empty($feed_audio)) ? $feed_audio : '',
							   'video'			=> ( ! empty($feed_video)) ? $feed_video : '',
							   'screen'			=> ( ! empty($sq_feedback_rec_screen)) ? $sq_feedback_rec_screen : '',
							   'image'			=> ( ! empty($sq_feedback_img)) ? $sq_feedback_img : '',
							   'document'		=> ( ! empty($sq_feedback_doc)) ? $sq_feedback_doc : '');
			
			$cfeed_sql = "INSERT INTO `feedback_tbl` (`question_id`, `nextQid`,`EndSim`,`feedback`, `feedback_type`, `feed_speech_text`, `feed_audio`, `feed_video`, `feed_screen`, `feed_image`, `feed_document`) VALUES (:qid, :nextQid,:EndSim,:feedback, :ftype, :speech_text, :audio, :video, :screen, :image, :document)";
			$cfeed_stmt = $db->prepare($cfeed_sql);
			$cfeed_stmt->execute($cfeedData);
			
			#--------Incorrect-Feedback-------
			if(isset($chkInSWQEndSim) && $chkInSWQEndSim > 0){
				$mappedQuestionSEQIncorrect = '';
			}
			$ifeedData = array('qid'			=> ( ! empty($ques_id)) ? $ques_id : '',
								'nextQid'		=> ( ! empty($mappedQuestionSEQIncorrect)) ? $mappedQuestionSEQIncorrect : '',
								'EndSim'		=> ( ! empty($chkInSWQEndSim)) ? $chkInSWQEndSim : '',
							   'feedback'		=> ( ! empty($sq_ifeedback)) ? addslashes($sq_ifeedback) : '',
							   'ftype'			=> 2,
							   'speech_text'	=> ( ! empty($sqi_feedback_text_to_speech)) ? addslashes($sqi_feedback_text_to_speech) : '',
							   'audio'			=> ( ! empty($ifeed_audio)) ? $ifeed_audio : '',
							   'video'			=> ( ! empty($ifeed_video)) ? $ifeed_video : '',
							   'screen'			=> ( ! empty($sqi_feedback_rec_screen)) ? $sqi_feedback_rec_screen : '',
							   'image'			=> ( ! empty($sqi_feedback_img)) ? $sqi_feedback_img : '',
							   'document'		=> ( ! empty($sqi_feedback_doc)) ? $sqi_feedback_doc : '');
			
			$ifeed_sql = "INSERT INTO `feedback_tbl` (`question_id`, `nextQid`,`EndSim`,`feedback`, `feedback_type`, `feed_speech_text`, `feed_audio`, `feed_video`, `feed_screen`, `feed_image`, `feed_document`) VALUES (:qid, :nextQid,:EndSim,:feedback, :ftype, :speech_text, :audio, :video, :screen, :image, :document)";
			$ifeed_stmt = $db->prepare($ifeed_sql);
			$ifeed_stmt->execute($ifeedData);
			
			#----------ADD-OPTIONS-------------
			if ( ! empty($sq_ans[0])):
				$sqi = 0; $sqi_order = 1;
				foreach ($sq_ans as $answerData):
					if ( ! empty($updateSQAnswerid[$sqi])):
						$ansSql = "UPDATE `answer_tbl` SET `choice_option`=:choice_option, `match_order_no`=:order_no, `status`=:status, `uid`=:user_id, `cur_date`=:cur_date WHERE answer_id=:answer_id";
						$stmt = $db->prepare($ansSql);
						$stmt->execute(array('answer_id' 		=> $updateSQAnswerid[$sqi],
											 'choice_option'	=> ( ! empty($sq_ans[$sqi])) ? $sq_ans[$sqi] : '',
											 'order_no'			=> $sqi_order,
											 'status' 			=> 1,
											 'user_id' 			=> $userId,
											 'cur_date'			=> $curdate));
					else:
						$ansSql = "INSERT INTO `answer_tbl` (`question_id`, `choice_option`, `match_order_no`, `status`, `uid`, `cur_date`) 
						values (:qid, :choice_option, :order_no, :status, :user_id, :cur_date)";
						$stmt = $db->prepare($ansSql);
						$stmt->execute(array('qid'				=> $ques_id,
											 'choice_option'	=> ( ! empty($sq_ans[$sqi])) ? addslashes($sq_ans[$sqi]) : '',
											 'order_no'			=> $sqi_order,
											 'status' 			=> 1,
											 'user_id' 			=> $userId,
											 'cur_date'			=> $curdate));
					endif;
					$sqi++; $sqi_order++;
				endforeach;
			endif;
		//-----------END-SEQUENCE----------
	
		//-----------START-SORTING----------
		elseif ($question_type == 3):
			#-------QUESTIONS-DATA---
			$qaudio = '';
			if ( ! empty($sortqq_qaudio)):
				$qaudio = $sortqq_qaudio;
			elseif ( ! empty($sortqq_rec_audio)):
				$qaudio = $sortqq_rec_audio;
			endif;
			
			$audio = '';
			if ( ! empty($sortq_qaudio)):
				$audio = $sortq_qaudio;
			elseif ( ! empty($sortq_rec_audio)):
				$audio = $sortq_rec_audio;
			endif;
			
			$video = '';
			if ( ! empty($sortq_video)):
				$video = $sortq_video;
			elseif ( ! empty($sortq_rec_video)):
				$video = $sortq_rec_video;
			endif;
			
			#------Correct-FEEDBACK-DATA---
			$feed_audio = '';
			if ( ! empty($sortq_feedback_audio)):
				$feed_audio = $sortq_feedback_audio;
			elseif ( ! empty($sortq_feedback_rec_audio)):
				$feed_audio = $sortq_feedback_rec_audio;
			endif;
			
			$feed_video = '';
			if ( ! empty($sortq_feedback_video)):
				$feed_video = $sortq_feedback_video;
			elseif ( ! empty($sortq_feedback_rec_video)):
				$feed_video = $sortq_feedback_rec_video;
			endif;
			
			#------Incorrect-FEEDBACK-DATA---
			$ifeed_audio = '';
			if ( ! empty($sortqi_feedback_audio)):
				$ifeed_audio = $sortqi_feedback_audio;
			elseif ( ! empty($sortqi_feedback_rec_audio)):
				$ifeed_audio = $sortqi_feedback_rec_audio;
			endif;
			
			$ifeed_video = '';
			if ( ! empty($sortqi_feedback_video)):
				$ifeed_video = $sortqi_feedback_video;
			elseif ( ! empty($sortqi_feedback_rec_video)):
				$ifeed_video = $sortqi_feedback_rec_video;
			endif;
						
			$quesData = array('questions'		=> ( ! empty($sortq)) ? addslashes($sortq) : '',
							  'qtype'			=> ( ! empty($question_type)) ? $question_type : '',
					  		  'qaudio'			=> ( ! empty($qaudio)) ? $qaudio : '',
					  		  'qspeech_text'	=> ( ! empty($sortqq_text_to_speech)) ? addslashes($sortqq_text_to_speech) : '',
							  'speech_text'		=> ( ! empty($sortq_text_to_speech)) ? addslashes($sortq_text_to_speech) : '',
							  'audio'			=> ( ! empty($audio)) ? $audio : '',
							  'video'			=> ( ! empty($video)) ? $video : '',
							  'screen'			=> ( ! empty($sortq_rec_screen)) ? $sortq_rec_screen : '',
							  'image'			=> ( ! empty($sortq_img)) ? $sortq_img : '',
							  'document'		=> ( ! empty($sortq_doc)) ? $sortq_doc : '',
							  'ques_val_1'		=> ( ! empty($sort_ques_val_1)) ? $sort_ques_val_1 : '',
							  'ques_val_2'		=> ( ! empty($sort_ques_val_2)) ? $sort_ques_val_2 : '',
							  'ques_val_3'		=> ( ! empty($sort_ques_val_3)) ? $sort_ques_val_3 : '',
							  'ques_val_4'		=> ( ! empty($sort_ques_val_4)) ? $sort_ques_val_4 : '',
							  'ques_val_5'		=> ( ! empty($sort_ques_val_5)) ? $sort_ques_val_5 : '',
							  'ques_val_6'		=> ( ! empty($sort_ques_val_6)) ? $sort_ques_val_6 : '',
							  'critical'		=> ( ! empty($sort_criticalQ)) ? $sort_criticalQ : '',
							  'status' 			=> 1,
							  'user_id'			=> $userId,
							  'cur_date'		=> $curdate,
							  'qid'				=> $ques_id);
			$ques_sql  = "UPDATE `question_tbl` SET `questions`=:questions, `question_type`=:qtype, `qaudio`=:qaudio, `qspeech_text`=:qspeech_text, `speech_text`=:speech_text, `audio`=:audio, `video`=:video, `screen`=:screen, `image`=:image, `document`=:document, `ques_val_1`=:ques_val_1, `ques_val_2`=:ques_val_2, `ques_val_3`=:ques_val_3, `ques_val_4`=:ques_val_4, `ques_val_5`=:ques_val_5, `ques_val_6`=:ques_val_6, `critical`=:critical, `status`=:status, `uid`=:user_id, `cur_date`=:cur_date WHERE question_id=:qid";
			$ques_stmt = $db->prepare($ques_sql);
			$ques_stmt->execute($quesData);
			
			#-----Remove-Old-Feedback-----
			$db->feedback_remove($ques_id);
			
			#--------Correct-Feedback----------
			if(isset($chkCorSORTEndSim) && $chkCorSORTEndSim > 0){
				$mappedQuestionSORTCorrect = '';
			}
			$cfeedData = array('qid'			=> ( ! empty($ques_id)) ? $ques_id : '',
								'nextQid'		=> ( ! empty($mappedQuestionSORTCorrect)) ? $mappedQuestionSORTCorrect : '',
								'EndSim'		=> ( ! empty($chkCorSORTEndSim)) ? $chkCorSORTEndSim : '',
							   'feedback'		=> ( ! empty($sortq_cfeedback)) ? addslashes($sortq_cfeedback) : '',
							   'ftype'			=> 1,
							   'speech_text'	=> ( ! empty($sortq_feedback_text_to_speech)) ? addslashes($sortq_feedback_text_to_speech) : '',
							   'audio'			=> ( ! empty($feed_audio)) ? $feed_audio : '',
							   'video'			=> ( ! empty($feed_video)) ? $feed_video : '',
							   'screen'			=> ( ! empty($sortq_feedback_rec_screen)) ? $sortq_feedback_rec_screen : '',
							   'image'			=> ( ! empty($sortq_feedback_img)) ? $sortq_feedback_img : '',
							   'document'		=> ( ! empty($sortq_feedback_doc)) ? $sortq_feedback_doc : '');
			
			$cfeed_sql = "INSERT INTO `feedback_tbl` (`question_id`, `nextQid`,`EndSim`,`feedback`, `feedback_type`, `feed_speech_text`, `feed_audio`, `feed_video`, `feed_screen`, `feed_image`, `feed_document`) VALUES (:qid, :nextQid,:EndSim,:feedback, :ftype, :speech_text, :audio, :video, :screen, :image, :document)";
			$cfeed_stmt = $db->prepare($cfeed_sql);
			$cfeed_stmt->execute($cfeedData);
			
			#--------Incorrect-Feedback-------
			if(isset($chkInSORTEndSim) && $chkInSORTEndSim > 0){
				$mappedQuestionSORTIncorrect = '';
			}
			$ifeedData = array('qid'			=> ( ! empty($ques_id)) ? $ques_id : '',
								'nextQid'		=> ( ! empty($mappedQuestionSORTIncorrect)) ? $mappedQuestionSORTIncorrect : '',
								'EndSim'		=> ( ! empty($chkInSORTEndSim)) ? $chkInSORTEndSim : '',
							   'feedback'		=> ( ! empty($sortq_ifeedback)) ? addslashes($sortq_ifeedback) : '',
							   'ftype'			=> 2,
							   'speech_text'	=> ( ! empty($sortqi_feedback_text_to_speech)) ? addslashes($sortqi_feedback_text_to_speech) : '',
							   'audio'			=> ( ! empty($ifeed_audio)) ? $ifeed_audio : '',
							   'video'			=> ( ! empty($ifeed_video)) ? $ifeed_video : '',
							   'screen'			=> ( ! empty($sortqi_feedback_rec_screen)) ? $sortqi_feedback_rec_screen : '',
							   'image'			=> ( ! empty($sortqi_feedback_img)) ? $sortqi_feedback_img : '',
							   'document'		=> ( ! empty($sortqi_feedback_doc)) ? $sortqi_feedback_doc : '');
			
			$ifeed_sql = "INSERT INTO `feedback_tbl` (`question_id`,  `nextQid`,`EndSim`,`feedback`, `feedback_type`, `feed_speech_text`, `feed_audio`, `feed_video`, `feed_screen`, `feed_image`, `feed_document`) VALUES (:qid, :nextQid,:EndSim,:feedback, :ftype, :speech_text, :audio, :video, :screen, :image, :document)";
			$ifeed_stmt = $db->prepare($ifeed_sql);
			$ifeed_stmt->execute($ifeedData);
			
			#----------ADD-OPTIONS----------
			if ( ! empty($sortq_sorting_items[0])):
				$sortqi = 0;
				foreach ($sortq_sorting_items as $optionData):
					if ( ! empty($updateSortAnswerid[$sortqi])):
						$ansSql = "UPDATE `answer_tbl` SET `choice_option`=:choice_option, `image`=:image, `status`=:status, `uid`=:user_id, `cur_date`=:cur_date WHERE answer_id=:answer_id";
						$stmt = $db->prepare($ansSql);
						$stmt->execute(array('answer_id'		=> $updateSortAnswerid[$sortqi],
											 'choice_option'	=> (isset($sortq_sorting_items[$sortqi]) && ! empty($sortq_sorting_items[$sortqi])) ? $sortq_sorting_items[$sortqi] : '',
											 'image'			=> (isset($sortq_item_img[$sortqi]) && ! empty($sortq_item_img[$sortqi])) ? $sortq_item_img[$sortqi] : '',
											 'status' 			 => 1,
											 'user_id' 			 => $userId,
											 'cur_date'			 => $curdate));
						
						if ( ! empty($updateSubOptionid[$sortqi])):
							$sortqop = 0;
							foreach ($sortq_drag_items[$sortqi]['option'] as $subOptionData):
								if ( ! empty($updateSubOptionid[$sortqi][$sortqop])):
									$ansOpSql = "UPDATE `sub_options_tbl` SET `sub_option`=:sub_option WHERE sub_options_id=:sub_options_id";
									$opstmt = $db->prepare($ansOpSql);
									$opstmt->execute(array('sub_options_id'	=> $updateSubOptionid[$sortqi][$sortqop],
														   'sub_option'	 	=> ( ! empty($subOptionData)) ? $subOptionData : ''));
								else:
									$opSql = "INSERT INTO `sub_options_tbl` (`answer_id`, `sub_option`) values (:aid, :option)";
									$opstmt = $db->prepare($opSql);
									$opstmt->execute(array('aid'	=> $updateSortAnswerid[$sortqi],
														   'option'	=> ( ! empty($subOptionData)) ? addslashes($subOptionData) : ''));
								endif;
								$sortqop++;
							endforeach;
						endif;
					else:
						$ansSql = "INSERT INTO `answer_tbl` (`question_id`, `choice_option`, `image`, `status`, `uid`, `cur_date`) values (:qid, :choice_option, :image, :status, :user_id, :cur_date)";
						$stmt = $db->prepare($ansSql);
						$stmt->execute(array('qid'				=> $ques_id,
											 'choice_option'	=> ( ! empty($sortq_sorting_items[$sortqi])) ? addslashes($sortq_sorting_items[$sortqi]) : '',
											 'image'			=> (isset($sortq_item_img[$sortqi]) && ! empty($sortq_item_img[$sortqi])) ? $sortq_item_img[$sortqi] : '',
											 'status' 			 => 1,
											 'user_id' 			 => $userId,
											 'cur_date'			 => $curdate));
						$ans_id = $db->lastInsertId();
						if ( ! empty($sortq_drag_items)):
							foreach ($sortq_drag_items[$sortqi]['option'] as $subOptionData):
								$opSql = "INSERT INTO `sub_options_tbl` (`answer_id`, `sub_option`) values (:aid, :option)";
								$opstmt = $db->prepare($opSql);
								$opstmt->execute(array('aid' => $ans_id, 'option' => ( ! empty($subOptionData)) ? addslashes($subOptionData) : ''));
							endforeach;
						endif;
					endif;
					$sortqi++;
				endforeach;	
			endif;
		//-----------END-SORTING----------
	
		//-----------START-MCQ----------
		elseif ($question_type == 4):
			#-------QUESTIONS-DATA---
			$qaudio = '';
			if ( ! empty($mcqq_qaudio)):
				$qaudio = $mcqq_qaudio;
			elseif ( ! empty($mcqq_rec_audio)):
				$qaudio = $mcqq_rec_audio;
			endif;
			
			$audio = '';
			if ( ! empty($mcq_audio)):
				$audio = $mcq_audio;
			elseif ( ! empty($mcq_rec_audio)):
				$audio = $mcq_rec_audio;
			endif;
			
			$video = '';
			if ( ! empty($mcq_video)):
				$video = $mcq_video;
			elseif ( ! empty($mcq_rec_video)):
				$video = $mcq_rec_video;
			endif;
			
			#------Correct-FEEDBACK-DATA---
			$feed_audio = '';
			if ( ! empty($mcq_feedback_audio)):
				$feed_audio = $mcq_feedback_audio;
			elseif ( ! empty($mcq_feedback_rec_audio)):
				$feed_audio = $mcq_feedback_rec_audio;
			endif;
			
			$feed_video = '';
			if ( ! empty($mcq_feedback_video)):
				$feed_video = $mcq_feedback_video;
			elseif ( ! empty($mcq_feedback_rec_video)):
				$feed_video = $mcq_feedback_rec_video;
			endif;
			
			#------Incorrect-FEEDBACK-DATA---
			$ifeed_audio = '';
			if ( ! empty($mcqi_feedback_audio)):
				$ifeed_audio = $mcqi_feedback_audio;
			elseif ( ! empty($mcqi_feedback_rec_audio)):
				$ifeed_audio = $mcqi_feedback_rec_audio;
			endif;
			
			$ifeed_video = '';
			if ( ! empty($mcqi_feedback_video)):
				$ifeed_video = $mcqi_feedback_video;
			elseif ( ! empty($mcqi_feedback_rec_video)):
				$ifeed_video = $mcqi_feedback_rec_video;
			endif;
						
			$quesData = array('questions'		=> ( ! empty($mcq)) ? addslashes($mcq) : '',
							  'qtype'			=> ( ! empty($question_type)) ? $question_type : '',
							  'qaudio'			=> ( ! empty($qaudio)) ? $qaudio : '',
							  'qspeech_text'	=> ( ! empty($mcqq_text_to_speech)) ? addslashes($mcqq_text_to_speech) : '',
							  'speech_text'		=> ( ! empty($mcq_text_to_speech)) ? addslashes($mcq_text_to_speech) : '',
							  'audio'			=> ( ! empty($audio)) ? $audio : '',
							  'video'			=> ( ! empty($video)) ? $video : '',
							  'screen'			=> ( ! empty($mcq_rec_screen)) ? $mcq_rec_screen : '',
							  'image'			=> ( ! empty($mcq_img)) ? $mcq_img : '',
							  'document'		=> ( ! empty($mcq_doc)) ? $mcq_doc : '',
							  'true_option'		=> ( ! empty($mcq_true_option)) ? $mcq_true_option : '',
							  'shuffle'			=> (isset($mcq_shuffle) && ! empty($mcq_shuffle)) ? 1 : 0,
							  'ques_val_1'		=> ( ! empty($mcq_ques_val_1)) ? $mcq_ques_val_1 : '',
							  'ques_val_2'		=> ( ! empty($mcq_ques_val_2)) ? $mcq_ques_val_2 : '',
							  'ques_val_3'		=> ( ! empty($mcq_ques_val_3)) ? $mcq_ques_val_3 : '',
							  'ques_val_4'		=> ( ! empty($mcq_ques_val_4)) ? $mcq_ques_val_4 : '',
							  'ques_val_5'		=> ( ! empty($mcq_ques_val_5)) ? $mcq_ques_val_5 : '',
							  'ques_val_6'		=> ( ! empty($mcq_ques_val_6)) ? $mcq_ques_val_6 : '',
							  'critical'		=> ( ! empty($mcq_criticalQ)) ? $mcq_criticalQ : '',
							  'status' 			=> 1,
							  'user_id'			=> $userId,
							  'cur_date'		=> $curdate,
							  'qid'				=> $ques_id);
			$ques_sql  = "UPDATE `question_tbl` SET `questions`=:questions, `question_type`=:qtype, `qaudio`=:qaudio, `qspeech_text`=:qspeech_text, `speech_text`=:speech_text, `audio`=:audio, `video`=:video, `screen`=:screen, `image`=:image, `document`=:document, `true_options`=:true_option, `shuffle`=:shuffle, `ques_val_1`=:ques_val_1, `ques_val_2`=:ques_val_2, `ques_val_3`=:ques_val_3, `ques_val_4`=:ques_val_4, `ques_val_5`=:ques_val_5, `ques_val_6`=:ques_val_6, `critical`=:critical, `status`=:status, `uid`=:user_id, `cur_date`=:cur_date WHERE question_id=:qid";
			$ques_stmt = $db->prepare($ques_sql);
			$ques_stmt->execute($quesData);
			
			#-----Remove-Old-Feedback-------
			$db->feedback_remove($ques_id);
			
			#--------Correct-Feedback----------
			if(isset($chkCorMCQEndSim) && $chkCorMCQEndSim > 0){
				$mappedQuestionMCQCorrect = '';
			}
			$cfeedData = array('qid'			=> ( ! empty($ques_id)) ? $ques_id : '',
								'nextQid'			=> ( ! empty($mappedQuestionMCQCorrect)) ? $mappedQuestionMCQCorrect : '',
								'EndSim'			=> ( ! empty($chkCorMCQEndSim)) ? $chkCorMCQEndSim : '',
							   'feedback'		=> ( ! empty($mcq_cfeedback)) ? addslashes($mcq_cfeedback) : '',
							   'ftype'			=> 1,
							   'speech_text'	=> ( ! empty($mcq_feedback_text_to_speech)) ? addslashes($mcq_feedback_text_to_speech) : '',
							   'audio'			=> ( ! empty($feed_audio)) ? $feed_audio : '',
							   'video'			=> ( ! empty($feed_video)) ? $feed_video : '',
							   'screen'			=> ( ! empty($mcq_feedback_rec_screen)) ? $mcq_feedback_rec_screen : '',
							   'image'			=> ( ! empty($mcq_feedback_img)) ? $mcq_feedback_img : '',
							   'document'		=> ( ! empty($mcq_feedback_doc)) ? $mcq_feedback_doc : '');
			
			$cfeed_sql = "INSERT INTO `feedback_tbl` (`question_id`, `nextQid`,`EndSim`,`feedback`, `feedback_type`, `feed_speech_text`, `feed_audio`, `feed_video`, `feed_screen`, `feed_image`, `feed_document`) VALUES (:qid,:nextQid, :EndSim,:feedback, :ftype, :speech_text, :audio, :video, :screen, :image, :document)";
			$cfeed_stmt = $db->prepare($cfeed_sql);
			$cfeed_stmt->execute($cfeedData);
			
			#--------Incorrect-Feedback-------
			if(isset($chkInMCQEndSim) && $chkInMCQEndSim > 0){
				$mappedQuestionMCQIncorrect = '';
			}
			$ifeedData = array('qid'			=> ( ! empty($ques_id)) ? $ques_id : '',
								'nextQid'			=> ( ! empty($mappedQuestionMCQIncorrect)) ? $mappedQuestionMCQIncorrect : '',
								'EndSim'			=> ( ! empty($chkInMCQEndSim)) ? $chkInMCQEndSim : '',
							   'feedback'		=> ( ! empty($mcq_ifeedback)) ? addslashes($mcq_ifeedback) : '',
							   'ftype'			=> 2,
							   'speech_text'	=> ( ! empty($mcqi_feedback_text_to_speech)) ? addslashes($mcqi_feedback_text_to_speech) : '',
							   'audio'			=> ( ! empty($ifeed_audio)) ? $ifeed_audio : '',
							   'video'			=> ( ! empty($ifeed_video)) ? $ifeed_video : '',
							   'screen'			=> ( ! empty($mcqi_feedback_rec_screen)) ? $mcqi_feedback_rec_screen : '',
							   'image'			=> ( ! empty($mcqi_feedback_img)) ? $mcqi_feedback_img : '',
							   'document'		=> ( ! empty($mcqi_feedback_doc)) ? $mcqi_feedback_doc : '');
			
			$ifeed_sql  = "INSERT INTO `feedback_tbl` (`question_id`, `nextQid`,`EndSim`,`feedback`, `feedback_type`, `feed_speech_text`, `feed_audio`, `feed_video`, `feed_screen`, `feed_image`, `feed_document`) VALUES (:qid,:nextQid, :EndSim, :feedback, :ftype, :speech_text, :audio, :video, :screen, :image, :document)";
			$ifeed_stmt = $db->prepare($ifeed_sql);
			$ifeed_stmt->execute($ifeedData);
			
			#----------ADD-OPTIONS-------------
			if ( ! empty($mcq_option[0])):
				$mcqi = 0; $mcqtrue = 1;
				foreach ($mcq_option as $answerData):
					if ( ! empty($updateMCQAnswerid[$mcqi])):
						$ansSql = "UPDATE `answer_tbl` SET `choice_option`=:option, `true_option`=:true, `status`=:status, `uid`=:user_id, `cur_date`=:cur_date WHERE answer_id=:aid";
						$stmt = $db->prepare($ansSql);
						$stmt->execute(array('aid'		=> $updateMCQAnswerid[$mcqi],
											 'option'	=> ( ! empty($mcq_option[$mcqi])) ? $mcq_option[$mcqi] : '',
											 'true'		=> ( ! empty($mcq_true_option) && $mcq_true_option == $mcqtrue) ? 1 : '',
											 'status'	=> 1,
											 'user_id' 	=> $userId,
											 'cur_date'	=> $curdate));

					else:
						$ansSql = "INSERT INTO `answer_tbl` (`question_id`, `choice_option`, `true_option`, `status`, `uid`, `cur_date`) values (:qid, :option, :true, :status, :user_id, :cur_date)";
						$stmt = $db->prepare($ansSql);
						$stmt->execute(array('qid'		=> $ques_id,
											 'option'	=> ( ! empty($mcq_option[$mcqi])) ? addslashes($mcq_option[$mcqi]) : '',
											 'true'		=> ( ! empty($mcq_true_option) && $mcq_true_option == $mcqtrue) ? 1 : '',
											 'status'	=> 1,
											 'user_id' 	=> $userId,
											 'cur_date'	=> $curdate));
					endif;
					$mcqi++; $mcqtrue++;
				endforeach;
			endif;
		//-----------END-MCQ----------
	
		//-----------START-MMCQ----------
		elseif ($question_type == 5):
			#-------QUESTIONS-DATA---
			$qaudio = '';
			if ( ! empty($mmcqq_qaudio)):
				$qaudio = $mmcqq_qaudio;
			elseif ( ! empty($mmcqq_rec_audio)):
				$qaudio = $mmcqq_rec_audio;
			endif;

			$audio = '';
			if ( ! empty($mmcq_audio)):
				$audio = $mmcq_audio;
			elseif ( ! empty($mmcq_rec_audio)):
				$audio = $mmcq_rec_audio;
			endif;
			
			$video = '';
			if ( ! empty($mmcq_video)):
				$video = $mmcq_video;
			elseif ( ! empty($mmcq_rec_video)):
				$video = $mmcq_rec_video;
			endif;
			
			#------Correct-FEEDBACK-DATA---
			$feed_audio = '';
			if ( ! empty($mmcq_feedback_audio)):
				$feed_audio = $mmcq_feedback_audio;
			elseif ( ! empty($mmcq_feedback_rec_audio)):
				$feed_audio = $mmcq_feedback_rec_audio;
			endif;
			
			$feed_video = '';
			if ( ! empty($mmcq_feedback_video)):
				$feed_video = $mmcq_feedback_video;
			elseif ( ! empty($mmcq_feedback_rec_video)):
				$feed_video = $mmcq_feedback_rec_video;
			endif;
			
			#------Incorrect-FEEDBACK-DATA---
			$ifeed_audio = '';
			if ( ! empty($mmcqi_feedback_audio)):
				$ifeed_audio = $mmcqi_feedback_audio;
			elseif ( ! empty($mmcqi_feedback_rec_audio)):
				$ifeed_audio = $mmcqi_feedback_rec_audio;
			endif;
			
			$ifeed_video = '';
			if ( ! empty($mmcqi_feedback_video)):
				$ifeed_video = $mmcqi_feedback_video;
			elseif ( ! empty($mmcqi_feedback_rec_video)):
				$ifeed_video = $mmcqi_feedback_rec_video;
			endif;
			
			$quesData = array('questions'		=> ( ! empty($mmcq)) ? addslashes($mmcq) : '',
							  'qtype'			=> ( ! empty($question_type)) ? $question_type : '',
					  		  'qaudio'			=> ( ! empty($qaudio)) ? $qaudio : '',
					  		  'qspeech_text'	=> ( ! empty($mmcqq_text_to_speech)) ? addslashes($mmcqq_text_to_speech) : '',
							  'speech_text'		=> ( ! empty($mmcq_text_to_speech)) ? addslashes($mmcq_text_to_speech) : '',
							  'audio'			=> ( ! empty($audio)) ? $audio : '',
							  'video'			=> ( ! empty($video)) ? $video : '',
							  'screen'			=> ( ! empty($mmcq_rec_screen)) ? $mmcq_rec_screen : '',
							  'image'			=> ( ! empty($mmcq_img)) ? $mmcq_img : '',
							  'document'		=> ( ! empty($mmcq_doc)) ? $mmcq_doc : '',
							  'true_option'		=> ( ! empty($mmcq_true_option)) ? $db->addMultiIds($mmcq_true_option) : '',
							  'shuffle'			=> (isset($mmcq_shuffle) && ! empty($mmcq_shuffle)) ? 1 : 0,
							  'ques_val_1'		=> ( ! empty($mmcq_ques_val_1)) ? $mmcq_ques_val_1 : '',
							  'ques_val_2'		=> ( ! empty($mmcq_ques_val_2)) ? $mmcq_ques_val_2 : '',
							  'ques_val_3'		=> ( ! empty($mmcq_ques_val_3)) ? $mmcq_ques_val_3 : '',
							  'ques_val_4'		=> ( ! empty($mmcq_ques_val_4)) ? $mmcq_ques_val_4 : '',
							  'ques_val_5'		=> ( ! empty($mmcq_ques_val_5)) ? $mmcq_ques_val_5 : '',
							  'ques_val_6'		=> ( ! empty($mmcq_ques_val_6)) ? $mmcq_ques_val_6 : '',
							  'critical'		=> ( ! empty($mmcq_criticalQ)) ? $mmcq_criticalQ : '',
							  'status' 			=> 1,
							  'user_id'			=> $userId,
							  'cur_date'		=> $curdate,
							  'qid'				=> $ques_id);

			$ques_sql  = "UPDATE `question_tbl` SET `questions`=:questions, `question_type`=:qtype, `qaudio`=:qaudio, `qspeech_text`=:qspeech_text, `speech_text`=:speech_text, `audio`=:audio, `video`=:video, `screen`=:screen, `image`=:image, `document`=:document, `true_options`=:true_option, `shuffle`=:shuffle, `ques_val_1`=:ques_val_1, `ques_val_2`=:ques_val_2, `ques_val_3`=:ques_val_3, `ques_val_4`=:ques_val_4, `ques_val_5`=:ques_val_5, `ques_val_6`=:ques_val_6, `critical`=:critical, `status`=:status, `uid`=:user_id, `cur_date`=:cur_date WHERE question_id=:qid";
			$ques_stmt = $db->prepare($ques_sql);
			$ques_stmt->execute($quesData);
			
			#-----Remove-Old-Feedback-------
			$db->feedback_remove($ques_id);
			
			#--------Correct-Feedback----------
			if(isset($chkCorMMCQEndSim) && $chkCorMMCQEndSim > 0){
				$mappedQuestionMMCQCorrect = '';
			}
			$cfeedData = array('qid'			=> ( ! empty($ques_id)) ? $ques_id : '',
								'nextQid'		=> ( ! empty($mappedQuestionMMCQCorrect)) ? addslashes($mappedQuestionMMCQCorrect) : '',
								'EndSim'		=> ( ! empty($chkCorMMCQEndSim)) ? addslashes($chkCorMMCQEndSim) : '',
							   'feedback'		=> ( ! empty($mmcq_cfeedback)) ? addslashes($mmcq_cfeedback) : '',
							   'ftype'			=> 1,
							   'speech_text'	=> ( ! empty($mmcq_feedback_text_to_speech)) ? addslashes($mmcq_feedback_text_to_speech) : '',
							   'audio'			=> ( ! empty($feed_audio)) ? $feed_audio : '',
							   'video'			=> ( ! empty($feed_video)) ? $feed_video : '',
							   'screen'			=> ( ! empty($mmcq_feedback_rec_screen)) ? $mmcq_feedback_rec_screen : '',
							   'image'			=> ( ! empty($mmcq_feedback_img)) ? $mmcq_feedback_img : '',
							   'document'		=> ( ! empty($mmcq_feedback_doc)) ? $mmcq_feedback_doc : '');
			
			$cfeed_sql = "INSERT INTO `feedback_tbl` (`question_id`, `nextQid`,`EndSim`,`feedback`, `feedback_type`, `feed_speech_text`, `feed_audio`, `feed_video`, `feed_screen`, `feed_image`, `feed_document`) VALUES (:qid, :nextQid,:EndSim,:feedback, :ftype, :speech_text, :audio, :video, :screen, :image, :document)";
			$cfeed_stmt = $db->prepare($cfeed_sql);
			$cfeed_stmt->execute($cfeedData);
			
			#--------Incorrect-Feedback-------
			if(isset($chkInMMCQEndSim) && $chkInMMCQEndSim > 0){
				$mappedQuestionMMCQIncorrect = '';
			}
			$ifeedData = array('qid'			=> ( ! empty($ques_id)) ? $ques_id : '',
								'nextQid'		=> ( ! empty($mappedQuestionMMCQIncorrect)) ? addslashes($mappedQuestionMMCQIncorrect) : '',
								'EndSim'		=> ( ! empty($chkInMMCQEndSim)) ? addslashes($chkInMMCQEndSim) : '',
							   'feedback'		=> ( ! empty($mmcq_ifeedback)) ? addslashes($mmcq_ifeedback) : '',
							   'ftype'			=> 2,
							   'speech_text'	=> ( ! empty($mmcqi_feedback_text_to_speech)) ? addslashes($mmcqi_feedback_text_to_speech) : '',
							   'audio'			=> ( ! empty($ifeed_audio)) ? $ifeed_audio : '',
							   'video'			=> ( ! empty($ifeed_video)) ? $ifeed_video : '',
							   'screen'			=> ( ! empty($mmcqi_feedback_rec_screen)) ? $mmcqi_feedback_rec_screen : '',
							   'image'			=> ( ! empty($mmcqi_feedback_img)) ? $mmcqi_feedback_img : '',
							   'document'		=> ( ! empty($mmcqi_feedback_doc)) ? $mmcqi_feedback_doc : '');
			
			$ifeed_sql = "INSERT INTO `feedback_tbl` (`question_id`, `nextQid`,`EndSim`,`feedback`, `feedback_type`, `feed_speech_text`, `feed_audio`, `feed_video`, `feed_screen`, `feed_image`, `feed_document`) VALUES (:qid, :nextQid,:EndSim,:feedback, :ftype, :speech_text, :audio, :video, :screen, :image, :document)";
			$ifeed_stmt = $db->prepare($ifeed_sql);
			$ifeed_stmt->execute($ifeedData);
			
			#----------ADD-OPTIONS-------------
			if ( ! empty($mmcq_option[0])):
				$mmcqi = 0; $mmcqtrue = 1;
				foreach ($mmcq_option as $answerData):
					if ( ! empty($updateMMCQAnswerid[$mmcqi])):
						$ansSql = "UPDATE `answer_tbl` SET `choice_option`=:option, `true_option`=:true, `status`=:status, `uid`=:user_id, `cur_date`=:cur_date WHERE answer_id=:aid";
						$stmt = $db->prepare($ansSql);
						$stmt->execute(array('aid'		=> $updateMMCQAnswerid[$mmcqi],
											 'option'	=> ( ! empty($mmcq_option[$mmcqi])) ? $mmcq_option[$mmcqi] : '',
											 'true'		=> ( ! empty($mmcq_true_option[$mmcqi]) && $mmcq_true_option[$mmcqi] == $mmcqtrue) ? 1 : '',
											 'status'	=> 1,
											 'user_id' 	=> $userId,
											 'cur_date'	=> $curdate));
					else:
						$ansSql = "INSERT INTO `answer_tbl` (`question_id`, `choice_option`, `true_option`, `status`, `uid`, `cur_date`) values (:qid, :option, :true, :status, :user_id, :cur_date)";
						$stmt = $db->prepare($ansSql);
						$stmt->execute(array('qid'		=> $ques_id,
											 'option'	=> ( ! empty($mmcq_option[$mmcqi])) ? addslashes($mmcq_option[$mmcqi]) : '',
											 'true'		=> ( ! empty($mmcq_true_option[$mmcqi]) && $mmcq_true_option[$mmcqi] == $mmcqtrue) ? 1 : '',
											 'status'	=> 1,
											 'user_id' 	=> $userId,
											 'cur_date'	=> $curdate));
					endif;
					$mmcqi++; $mmcqtrue++;
				endforeach;
			endif;
		//-----------END-MMCQ----------
	
		//-----------START-SWIPING----------
		elseif ($question_type == 7):
			#-------QUESTIONS-DATA---
			$qaudio = '';
			if ( ! empty($swipqq_qaudio)):
				$qaudio = $swipqq_qaudio;
			elseif ( ! empty($swipqq_rec_audio)):
				$qaudio = $swipqq_rec_audio;
			endif;

			$audio = '';
			if ( ! empty($swipq_audio)):
				$audio = $swipq_audio;
			elseif ( ! empty($swipq_rec_audio)):
				$audio = $swipq_rec_audio;
			endif;
			
			$video = '';
			if ( ! empty($swipq_video)):
				$video = $swipq_video;
			elseif ( ! empty($swipq_rec_video)):
				$video = $swipq_rec_video;
			endif;
			
			#------Correct-FEEDBACK-DATA---
			$feed_audio = '';
			if ( ! empty($swipq_feedback_audio)):
				$feed_audio = $swipq_feedback_audio;
			elseif ( ! empty($swipq_feedback_rec_audio)):
				$feed_audio = $swipq_feedback_rec_audio;
			endif;
			
			$feed_video = '';
			if ( ! empty($swipq_feedback_video)):
				$feed_video = $swipq_feedback_video;
			elseif ( ! empty($swipq_feedback_rec_video)):
				$feed_video = $swipq_feedback_rec_video;
			endif;
			
			#------Incorrect-FEEDBACK-DATA---
			$ifeed_audio = '';
			if ( ! empty($swipqi_feedback_audio)):
				$ifeed_audio = $swipqi_feedback_audio;
			elseif ( ! empty($swipqi_feedback_rec_audio)):
				$ifeed_audio = $swipqi_feedback_rec_audio;
			endif;
			
			$ifeed_video = '';
			if ( ! empty($swipqi_feedback_video)):
				$ifeed_video = $swipqi_feedback_video;
			elseif ( ! empty($swipqi_feedback_rec_video)):
				$ifeed_video = $swipqi_feedback_rec_video;
			endif;
						
			$quesData = array('questions'		=> ( ! empty($swipq)) ? addslashes($swipq) : '',
							  'qtype'			=> ( ! empty($question_type)) ? $question_type : '',
							  'qaudio'			=> ( ! empty($qaudio)) ? $qaudio : '',
							  'qspeech_text'	=> ( ! empty($swipqq_text_to_speech)) ? addslashes($swipqq_text_to_speech) : '',
							  'speech_text'		=> ( ! empty($swipq_text_to_speech)) ? addslashes($swipq_text_to_speech) : '',
							  'audio'			=> ( ! empty($audio)) ? $audio : '',
							  'video'			=> ( ! empty($video)) ? $video : '',
							  'screen'			=> ( ! empty($swipq_rec_screen)) ? $swipq_rec_screen : '',
							  'image'			=> ( ! empty($swipq_img)) ? $swipq_img : '',
							  'document'		=> ( ! empty($swipq_doc)) ? $swipq_doc : '',
							  'true_option'		=> ( ! empty($swipq_true_option)) ? $swipq_true_option : '',
							  'shuffle'			=> (isset($swipq_shuffle) && ! empty($swipq_shuffle)) ? 1 : 0,
							  'ques_val_1'		=> ( ! empty($swipq_ques_val_1)) ? $swipq_ques_val_1 : '',
							  'ques_val_2'		=> ( ! empty($swipq_ques_val_2)) ? $swipq_ques_val_2 : '',
							  'ques_val_3'		=> ( ! empty($swipq_ques_val_3)) ? $swipq_ques_val_3 : '',
							  'ques_val_4'		=> ( ! empty($swipq_ques_val_4)) ? $swipq_ques_val_4 : '',
							  'ques_val_5'		=> ( ! empty($swipq_ques_val_5)) ? $swipq_ques_val_5 : '',
							  'ques_val_6'		=> ( ! empty($swipq_ques_val_6)) ? $swipq_ques_val_6 : '',
							  'critical'		=> ( ! empty($swip_criticalQ)) ? $swip_criticalQ : '',
							  'status' 			=> 1,
							  'user_id'			=> $userId,
							  'cur_date'		=> $curdate,
							  'qid'				=> $ques_id);

			$ques_sql  = "UPDATE `question_tbl` SET `questions`=:questions, `question_type`=:qtype, `qaudio`=:qaudio, `qspeech_text`=:qspeech_text, `speech_text`=:speech_text, `audio`=:audio, `video`=:video, `screen`=:screen, `image`=:image, `document`=:document, `true_options`=:true_option, `shuffle`=:shuffle, `ques_val_1`=:ques_val_1, `ques_val_2`=:ques_val_2, `ques_val_3`=:ques_val_3, `ques_val_4`=:ques_val_4, `ques_val_5`=:ques_val_5, `ques_val_6`=:ques_val_6, `critical`=:critical, `status`=:status, `uid`=:user_id, `cur_date`=:cur_date WHERE question_id=:qid";
			$ques_stmt = $db->prepare($ques_sql);
			$ques_stmt->execute($quesData);
			
			#-----Remove-Old-Feedback-------
			$db->feedback_remove($ques_id);
			
			#--------Correct-Feedback----------
			if(isset($chkCorSwipeEndSim) && $chkCorSwipeEndSim > 0){
				$mappedQuestionSwipeCorrect = '';
			}
			$cfeedData = array('qid'			=> ( ! empty($ques_id)) ? $ques_id : '',
								'nextQid'		=> ( ! empty($mappedQuestionSwipeCorrect)) ? $mappedQuestionSwipeCorrect : '',
								'EndSim'		=> ( ! empty($chkCorSwipeEndSim)) ? $chkCorSwipeEndSim : '',
							   'feedback'		=> ( ! empty($swipq_cfeedback)) ? addslashes($swipq_cfeedback) : '',
							   'ftype'			=> 1,
							   'speech_text'	=> ( ! empty($swipq_feedback_text_to_speech)) ? addslashes($swipq_feedback_text_to_speech) : '',
							   'audio'			=> ( ! empty($feed_audio)) ? $feed_audio : '',
							   'video'			=> ( ! empty($feed_video)) ? $feed_video : '',
							   'screen'			=> ( ! empty($swipq_feedback_rec_screen)) ? $swipq_feedback_rec_screen : '',
							   'image'			=> ( ! empty($swipq_feedback_img)) ? $swipq_feedback_img : '',
							   'document'		=> ( ! empty($swipq_feedback_doc)) ? $swipq_feedback_doc : '');
			
			$cfeed_sql = "INSERT INTO `feedback_tbl` (`question_id`,`nextQid`,`EndSim`, `feedback`, `feedback_type`, `feed_speech_text`, `feed_audio`, `feed_video`, `feed_screen`, `feed_image`, `feed_document`) VALUES (:qid, :nextQid,:EndSim,:feedback, :ftype, :speech_text, :audio, :video, :screen, :image, :document)";
			$cfeed_stmt = $db->prepare($cfeed_sql);
			$cfeed_stmt->execute($cfeedData);
			
			#--------Incorrect-Feedback-------
			if(isset($chkInSwipeEndSim) && $chkInSwipeEndSim > 0){
				$mappedQuestionSwipeIncorrect = '';
			}
			$ifeedData = array('qid'			=> ( ! empty($ques_id)) ? $ques_id : '',
							   'nextQid'		=> ( ! empty($mappedQuestionSwipeIncorrect)) ? $mappedQuestionSwipeIncorrect : '',
							   'EndSim'			=> ( ! empty($chkInSwipeEndSim)) ? $chkInSwipeEndSim : '',
							   'feedback'		=> ( ! empty($swipq_ifeedback)) ? addslashes($swipq_ifeedback) : '',
							   'ftype'			=> 2,
							   'speech_text'	=> ( ! empty($swipqi_feedback_text_to_speech)) ? addslashes($swipqi_feedback_text_to_speech) : '',
							   'audio'			=> ( ! empty($ifeed_audio)) ? $ifeed_audio : '',
							   'video'			=> ( ! empty($ifeed_video)) ? $ifeed_video : '',
							   'screen'			=> ( ! empty($swipqi_feedback_rec_screen)) ? $swipqi_feedback_rec_screen : '',
							   'image'			=> ( ! empty($swipqi_feedback_img)) ? $swipqi_feedback_img : '',
							   'document'		=> ( ! empty($swipqi_feedback_doc)) ? $swipqi_feedback_doc : '');
			
			$ifeed_sql = "INSERT INTO `feedback_tbl` (`question_id`, `nextQid`,`EndSim`,`feedback`, `feedback_type`, `feed_speech_text`, `feed_audio`, `feed_video`, `feed_screen`, `feed_image`, `feed_document`) VALUES (:qid, :nextQid,:EndSim,:feedback, :ftype, :speech_text, :audio, :video, :screen, :image, :document)";
			$ifeed_stmt = $db->prepare($ifeed_sql);
			$ifeed_stmt->execute($ifeedData);
			
			#----------ADD-OPTIONS-------------
			if ( ! empty($swipq_option[0])):
				$swipqi = 0; $swiptrue = 1;
				foreach ($swipq_option as $answerData):
					if ( ! empty($updateSwipqAnswerid[$swipqi])):
						$ansSql = "UPDATE `answer_tbl` SET `choice_option`=:option, `true_option`=:true, `image`=:image, `status`=:status, `uid`=:user_id, `cur_date`=:cur_date WHERE answer_id=:aid";
						$stmt = $db->prepare($ansSql);
						$stmt->execute(array('aid' 		=> $updateSwipqAnswerid[$swipqi],
											 'option'	=> ( ! empty($swipq_option[$swipqi])) ? $swipq_option[$swipqi] : '',
											 'image'	=> ( ! empty($swipq_item_img[$swipqi])) ? $swipq_item_img[$swipqi] : '',
											 'true'		=> ( ! empty($swipq_true_option) && $swipq_true_option == $swiptrue) ? 1 : '',
											 'status'	=> 1,
											 'user_id' 	=> $userId,
											 'cur_date'	=> $curdate));
					else:
						$ansSql = "INSERT INTO `answer_tbl` (`question_id`, `choice_option`, `true_option`, `image`, `status`, `uid`, `cur_date`) values (:qid, :option, :true, :image, :status, :user_id, :cur_date)";
						$stmt = $db->prepare($ansSql);
						$stmt->execute(array('qid'		=> $ques_id,
											 'option'	=> ( ! empty($swipq_option[$swipqi])) ? addslashes($swipq_option[$swipqi]) : '',
											 'image'	=> ( ! empty($swipq_item_img[$swipqi])) ? $swipq_item_img[$swipqi] : '',
											 'true'		=> ( ! empty($swipq_true_option) && $swipq_true_option == $swiptrue) ? 1 : '',
											 'status'	=> 1,
											 'user_id' 	=> $userId,
											 'cur_date'	=> $curdate));
					endif;
					$swipqi++; $swiptrue++;
				endforeach;
			endif;
		//-----------END-SWIPING----------
		
		//-----------START-DDQ----------
		elseif ($question_type == 8):
			#-------QUESTIONS-DATA---
			$qaudio = '';
			if ( ! empty($ddqq_qaudio)):
				$qaudio = $ddqq_qaudio;
			elseif ( ! empty($ddqq_rec_audio)):
				$qaudio = $ddqq_rec_audio;
			endif;
		
			$audio = '';
			if ( ! empty($ddq_audio)):
				$audio = $ddq_audio;
			elseif ( ! empty($ddq_rec_audio)):
				$audio = $ddq_rec_audio;
			endif;
			
			$video = '';
			if ( ! empty($ddq_video)):
				$video = $ddq_video;
			elseif ( ! empty($ddq_rec_video)):
				$video = $ddq_rec_video;
			endif;
			
			#------Correct-FEEDBACK-DATA---
			$feed_audio = '';
			if ( ! empty($ddq_feedback_audio)):
				$feed_audio = $ddq_feedback_audio;
			elseif ( ! empty($ddq_feedback_rec_audio)):
				$feed_audio = $ddq_feedback_rec_audio;
			endif;
			
			$feed_video = '';
			if ( ! empty($ddq_feedback_video)):
				$feed_video = $ddq_feedback_video;
			elseif ( ! empty($ddq_feedback_rec_video)):
				$feed_video = $ddq_feedback_rec_video;
			endif;
			
			#------Incorrect-FEEDBACK-DATA---
			$ifeed_audio = '';
			if ( ! empty($ddqi_feedback_audio)):
				$ifeed_audio = $ddqi_feedback_audio;
			elseif ( ! empty($ddqi_feedback_rec_audio)):
				$ifeed_audio = $ddqi_feedback_rec_audio;
			endif;
			
			$ifeed_video = '';
			if ( ! empty($ddqi_feedback_video)):
				$ifeed_video = $ddqi_feedback_video;
			elseif ( ! empty($ddqi_feedback_rec_video)):
				$ifeed_video = $ddqi_feedback_rec_video;
			endif;
			
			#--For-Multi-DROP-Options----
			if ($seleted_drag_option == 1):
				$quesData = array('questions'		=> ( ! empty($ddq)) ? addslashes($ddq) : '',
								  'seleted_drag' 	=> ( ! empty($seleted_drag_option)) ? $seleted_drag_option : '',
								  'qtype'			=> ( ! empty($question_type)) ? $question_type : '',
								  'qaudio'			=> ( ! empty($qaudio)) ? $qaudio : '',
								  'qspeech_text'	=> ( ! empty($ddqq_text_to_speech)) ? addslashes($ddqq_text_to_speech) : '',
								  'speech_text'		=> ( ! empty($ddq_text_to_speech)) ? addslashes($ddq_text_to_speech) : '',
								  'audio'			=> ( ! empty($audio)) ? $audio : '',
								  'video'			=> ( ! empty($video)) ? $video : '',
								  'screen'			=> ( ! empty($ddq_rec_screen)) ? $ddq_rec_screen : '',
								  'image'			=> ( ! empty($ddq_img)) ? $ddq_img : '',
								  'document'		=> ( ! empty($ddq_doc)) ? $ddq_doc : '',
								  'shuffle'			=> (isset($ddq_shuffle1) && ! empty($ddq_shuffle1)) ? 1 : 0,
								  'ques_val_1'		=> ( ! empty($ddq_ques_val_1)) ? $ddq_ques_val_1 : '',
								  'ques_val_2'		=> ( ! empty($ddq_ques_val_2)) ? $ddq_ques_val_2 : '',
								  'ques_val_3'		=> ( ! empty($ddq_ques_val_3)) ? $ddq_ques_val_3 : '',
								  'ques_val_4'		=> ( ! empty($ddq_ques_val_4)) ? $ddq_ques_val_4 : '',
								  'ques_val_5'		=> ( ! empty($ddq_ques_val_5)) ? $ddq_ques_val_5 : '',
								  'ques_val_6'		=> ( ! empty($ddq_ques_val_6)) ? $ddq_ques_val_6 : '',
								  'critical'		=> ( ! empty($ddq_criticalQ)) ? $ddq_criticalQ : '',
								  'status' 			=> 1,
								  'user_id'			=> $userId,
								  'cur_date'		=> $curdate,
								  'qid'				=> $ques_id);

				$ques_sql  = "UPDATE `question_tbl` SET `questions`=:questions, `question_type`=:qtype, `seleted_drag_option`=:seleted_drag, `qaudio`=:qaudio, `qspeech_text`=:qspeech_text, `speech_text`=:speech_text, `audio`=:audio, `video`=:video, `screen`=:screen, `image`=:image, `document`=:document, `shuffle`=:shuffle, `ques_val_1`=:ques_val_1, `ques_val_2`=:ques_val_2, `ques_val_3`=:ques_val_3, `ques_val_4`=:ques_val_4, `ques_val_5`=:ques_val_5, `ques_val_6`=:ques_val_6, `critical`=:critical, `status`=:status, `uid`=:user_id, `cur_date`=:cur_date WHERE question_id=:qid";
				$ques_stmt = $db->prepare($ques_sql);
				$ques_stmt->execute($quesData);

				#------OPTIONS------
				if ( ! empty($ddq_drop)):
					$ddqi = 0;
					foreach ($ddq_drop as $answerData):
						if ( ! empty($updateDDqAnswerid[$ddqi])):
							$ansSql = "UPDATE `answer_tbl` SET `choice_option`=:choice, `match_option`=:match, `image`=:image, `image2`=:image2, `status`=:status, `uid`=:user_id, `cur_date`=:cur_date WHERE answer_id=:aid";
							$stmt = $db->prepare($ansSql);
							$stmt->execute(array('aid' 		=> $updateDDqAnswerid[$ddqi],
												 'choice'	=> ( ! empty($ddq_drop[$ddqi])) ? $ddq_drop[$ddqi] : '',
												 'match'	=> ( ! empty($ddq_drag[$ddqi])) ? $ddq_drag[$ddqi] : '',
												 'image'	=> ( ! empty($ddq_item_img[$ddqi])) ? $ddq_item_img[$ddqi] : '',
												 'image2'	=> ( ! empty($ddq_drag_item_img[$ddqi])) ? $ddq_drag_item_img[$ddqi] : '',
												 'status'	=> 1,
												 'user_id' 	=> $userId,
												 'cur_date'	=> $curdate));
						else:
							$ansSql = "INSERT INTO `answer_tbl` (`question_id`, `choice_option`, `match_option`, `image`, `image2`, `status`, `uid`, `cur_date`) values (:qid, :choice, :match, :image, :image2, :status, :user_id, :cur_date)";
							$stmt = $db->prepare($ansSql);
							$stmt->execute(array('qid'		=> $ques_id,
												 'choice'	=> ( ! empty($ddq_drop[$ddqi])) ? addslashes($ddq_drop[$ddqi]) : '',
												 'match'	=> ( ! empty($ddq_drag[$ddqi])) ? addslashes($ddq_drag[$ddqi]) : '',
												 'image'	=> ( ! empty($ddq_item_img[$ddqi])) ? $ddq_item_img[$ddqi] : '',
												 'image2'	=> ( ! empty($ddq_drag_item_img[$ddqi])) ? $ddq_drag_item_img[$ddqi] : '',
												 'status'	=> 1,
												 'user_id' 	=> $userId,
												 'cur_date'	=> $curdate));
						endif;
						$ddqi++;
					endforeach;
				endif;

				#-----Remove-Old-Feedback-------
				$db->feedback_remove($ques_id);

				#--------Correct-Feedback-------
				if (isset($chkCorDNDEndSim) && $chkCorDNDEndSim > 0){
					$mappedQuestionDNDCorrect = '';
				}
				$cfeedData = array('qid'			=> ( ! empty($ques_id)) ? $ques_id : '',
								   'nextQid'		=> ( ! empty($mappedQuestionDNDCorrect)) ? $mappedQuestionDNDCorrect : '',
								   'EndSim'			=> ( ! empty($chkCorDNDEndSim)) ? $chkCorDNDEndSim : '',
								   'feedback'		=> ( ! empty($ddq_cfeedback)) ? addslashes($ddq_cfeedback) : '',
								   'ftype'			=> 1,
								   'speech_text'	=> ( ! empty($ddq_feedback_text_to_speech)) ? addslashes($ddq_feedback_text_to_speech) : '',
								   'audio'			=> ( ! empty($feed_audio)) ? $feed_audio : '',
								   'video'			=> ( ! empty($feed_video)) ? $feed_video : '',
								   'screen'			=> ( ! empty($ddq_feedback_rec_screen)) ? $ddq_feedback_rec_screen : '',
								   'image'			=> ( ! empty($ddq_feedback_img)) ? $ddq_feedback_img : '',
								   'document'		=> ( ! empty($ddq_feedback_doc)) ? $ddq_feedback_doc : '');

				$cfeed_sql = "INSERT INTO `feedback_tbl` (`question_id`, `nextQid`,`EndSim`,`feedback`, `feedback_type`, `feed_speech_text`, `feed_audio`, `feed_video`, `feed_screen`, `feed_image`, `feed_document`) VALUES (:qid, :nextQid,:EndSim,:feedback, :ftype, :speech_text, :audio, :video, :screen, :image, :document)";
				$cfeed_stmt = $db->prepare($cfeed_sql);
				$cfeed_stmt->execute($cfeedData);

				#--------Incorrect-Feedback-------
				if (isset($chkInDNDEndSim) && $chkInDNDEndSim > 0){
					$mappedQuestionDNDIncorrect = '';
				}
				$ifeedData = array('qid'			=> ( ! empty($ques_id)) ? $ques_id : '',
									'nextQid'		=> ( ! empty($mappedQuestionDNDIncorrect)) ? $mappedQuestionDNDIncorrect : '',
									'EndSim'		=> ( ! empty($chkInDNDEndSim)) ? $chkInDNDEndSim : '',
								   'feedback'		=> ( ! empty($ddq_ifeedback)) ? addslashes($ddq_ifeedback) : '',
								   'ftype'			=> 2,
								   'speech_text'	=> ( ! empty($ddqi_feedback_text_to_speech)) ? addslashes($ddqi_feedback_text_to_speech) : '',
								   'audio'			=> ( ! empty($ifeed_audio)) ? $ifeed_audio : '',
								   'video'			=> ( ! empty($ifeed_video)) ? $ifeed_video : '',
								   'screen'			=> ( ! empty($ddqi_feedback_rec_screen)) ? $ddqi_feedback_rec_screen : '',
								   'image'			=> ( ! empty($ddqi_feedback_img)) ? $ddqi_feedback_img : '',
								   'document'		=> ( ! empty($ddqi_feedback_doc)) ? $ddqi_feedback_doc : '');

				$ifeed_sql = "INSERT INTO `feedback_tbl` (`question_id`, `nextQid`,`EndSim`,`feedback`, `feedback_type`, `feed_speech_text`, `feed_audio`, `feed_video`, `feed_screen`, `feed_image`, `feed_document`) VALUES (:qid, :nextQid,:EndSim,:feedback, :ftype, :speech_text, :audio, :video, :screen, :image, :document)";
				$ifeed_stmt = $db->prepare($ifeed_sql);
				$ifeed_stmt->execute($ifeedData);
			elseif ($seleted_drag_option == 2):
				$quesData = array('questions'		=> ( ! empty($ddq)) ? $ddq : '',
								  'seleted_drag' 	=> ( ! empty($seleted_drag_option)) ? $seleted_drag_option : '',
								  'qtype'			=> ( ! empty($question_type)) ? $question_type : '',
								  'qaudio'			=> ( ! empty($qaudio)) ? $qaudio : '',
								  'qspeech_text'	=> ( ! empty($ddqq_text_to_speech)) ? $ddqq_text_to_speech : '',
								  'speech_text'		=> ( ! empty($ddq_text_to_speech)) ? $ddq_text_to_speech : '',
								  'audio'			=> ( ! empty($audio)) ? $audio : '',
								  'video'			=> ( ! empty($video)) ? $video : '',
								  'screen'			=> ( ! empty($ddq_rec_screen)) ? $ddq_rec_screen : '',
								  'image'			=> ( ! empty($ddq_img)) ? $ddq_img : '',
								  'document'		=> ( ! empty($ddq_doc)) ? $ddq_doc : '',
								  'shuffle'			=> (isset($ddq_shuffle2) && ! empty($ddq_shuffle2)) ? 1 : 0,
								  'true_options'	=> ( ! empty($ddq_true_option)) ? $ddq_true_option : '',
								  'ques_val_1'		=> ( ! empty($ddq_ques_val_1)) ? $ddq_ques_val_1 : '',
								  'ques_val_2'		=> ( ! empty($ddq_ques_val_2)) ? $ddq_ques_val_2 : '',
								  'ques_val_3'		=> ( ! empty($ddq_ques_val_3)) ? $ddq_ques_val_3 : '',
								  'ques_val_4'		=> ( ! empty($ddq_ques_val_4)) ? $ddq_ques_val_4 : '',
								  'ques_val_5'		=> ( ! empty($ddq_ques_val_5)) ? $ddq_ques_val_5 : '',
								  'ques_val_6'		=> ( ! empty($ddq_ques_val_6)) ? $ddq_ques_val_6 : '',
								  'critical'		=> ( ! empty($ddq_criticalQ)) ? $ddq_criticalQ : '',
								  'status' 			=> 1,
								  'user_id'			=> $userId,
								  'cur_date'		=> $curdate,
								  'qid'				=> $ques_id);
				$ques_sql  = "UPDATE `question_tbl` SET `questions`=:questions, `question_type`=:qtype, `seleted_drag_option`=:seleted_drag, `qaudio`=:qaudio, `qspeech_text`=:qspeech_text, `speech_text`=:speech_text, `audio`=:audio, `video`=:video, `screen`=:screen, `image`=:image, `document`=:document, `shuffle`=:shuffle, `true_options`=:true_options, `ques_val_1`=:ques_val_1, `ques_val_2`=:ques_val_2, `ques_val_3`=:ques_val_3, `ques_val_4`=:ques_val_4, `ques_val_5`=:ques_val_5, `ques_val_6`=:ques_val_6, `critical`=:critical, `status`=:status, `uid`=:user_id, `cur_date`=:cur_date WHERE question_id=:qid";
				$ques_stmt = $db->prepare($ques_sql);
				$ques_stmt->execute($quesData);
				
				#------OPTION------
				if ( ! empty($ddq2_drop)):
					if ( ! empty($updateDDq2Answerid)):
						#----DROP-TARGET----
						$ansSql = "UPDATE `sub_options_tbl` SET `sub_option`=:option, `image`=:image WHERE answer_id=:aid";
						$stmt = $db->prepare($ansSql);
						$stmt->execute(array('aid' 		=> $updateDDq2Answerid,
											 'option'	=> ( ! empty($ddq2_drop)) ? $ddq2_drop : '',
											 'image' 	=> ( ! empty($ddq2_item_img)) ? $ddq2_item_img : ''));
						#-----DRAG-ITEMS------
						$ddq2iu = 0; $ddq2true = 1;
						foreach ($ddq2_drag as $subAnswerData):
							if ( ! empty($updateDDq2SubAnswerid[$ddq2iu])):
								$getOpId = $updateDDq2SubAnswerid[$ddq2iu];
								$updateAnsSql  = "UPDATE `answer_tbl` SET `choice_option`=:option, `image`=:image, `true_option`=:true, `status`=:status, `uid`=:user_id, `cur_date`=:cur_date WHERE answer_id=:aid";
								$updateAnsstmt = $db->prepare($updateAnsSql);
								$updateAnsstmt->execute(array('aid' 		=> $getOpId,
															  'option'		=> ( ! empty($ddq2_drag[$ddq2iu])) ? $ddq2_drag[$ddq2iu] : '',
															  'image'		=> ( ! empty($ddq2_drag_item_img[$ddq2iu])) ? $ddq2_drag_item_img[$ddq2iu] : '',
															  'true'		=> ( ! empty($ddq_true_option) && $ddq_true_option == $ddq2true) ? 1 : '',
															  'status'		=> 1,
															  'user_id' 	=> $userId,
															  'cur_date'	=> $curdate));
							else:
								$ansSql = "INSERT INTO `answer_tbl` (`question_id`, `choice_option`, `image`, `true_option`, `status`, `uid`, `cur_date`) values (:qid, :option, :image, :true, :status, :user_id, :cur_date)";
								$stmt = $db->prepare($ansSql);
								$stmt->execute(array('qid'		=> $ques_id,
													 'option'	=> ( ! empty($ddq2_drag[$ddq2iu])) ? addslashes($ddq2_drag[$ddq2iu]) : '',
													 'image'	=> ( ! empty($ddq2_drag_item_img[$ddq2iu])) ? $ddq2_drag_item_img[$ddq2iu] : '',
													 'true'		=> ( ! empty($ddq_true_option) && $ddq_true_option == $ddq2true) ? 1 : '',
													 'status'	=> 1,
													 'user_id' 	=> $userId,
													 'cur_date'	=> $curdate));
							endif;
							$ddq2iu++; $ddq2true++;
						endforeach;
					else:
						#----DROP-TARGET----
						$ansSql = "INSERT INTO `sub_options_tbl` (`question_id`, `sub_option`, `image`) values (:qid, :option, :image)";
						$stmt = $db->prepare($ansSql);
						$stmt->execute(array('qid' 		=> $ques_id,
											 'option'	=> ( ! empty($ddq2_drop)) ? addslashes($ddq2_drop) : '',
											 'image' 	=> ( ! empty($ddq2_item_img)) ? $ddq2_item_img : ''));
					
						#-----DRAG-ITEMS------
						$ddq2i = 0; $ddq2itrue = 1;
						foreach ($ddq2_drag as $answerData):
							$ansSql = "INSERT INTO `answer_tbl` (`question_id`, `choice_option`, `image`, `true_option`, `status`, `uid`, `cur_date`) values (:qid, :choice, :image, :true, :status, :user_id, :cur_date)";
							$stmt = $db->prepare($ansSql);
							$stmt->execute(array('qid'		=> $ques_id,
												 'choice'	=> ( ! empty($ddq2_drag[$ddq2i])) ? addslashes($ddq2_drag[$ddq2i]) : '',
												 'image'	=> ( ! empty($ddq2_drag_item_img[$ddq2i])) ? $ddq2_drag_item_img[$ddq2i] : '',
												 'true'		=> ( ! empty($ddq_true_option) && $ddq_true_option == $ddq2itrue) ? 1 : '',
												 'status'	=> 1,
												 'user_id' 	=> $userId,
												 'cur_date'	=> $curdate));
							$ddq2i++; $ddq2itrue++;
						endforeach;
					endif;
				endif;

				#-----Remove-Old-Feedback-------
				$db->feedback_remove($ques_id);

				#--------Correct-Feedback-------
				if(isset($chkCorDNDEndSim) && $chkCorDNDEndSim > 0){
					$mappedQuestionDNDCorrect = '';
				}
				$cfeedData = array('qid'			=> ( ! empty($ques_id)) ? $ques_id : '',
								   'nextQid'		=> ( ! empty($mappedQuestionDNDCorrect)) ? $mappedQuestionDNDCorrect : '',
								   'EndSim'			=> ( ! empty($chkCorDNDEndSim)) ? $chkCorDNDEndSim : '',
								   'feedback'		=> ( ! empty($ddq_cfeedback)) ? addslashes($ddq_cfeedback) : '',
								   'ftype'			=> 1,
								   'speech_text'	=> ( ! empty($ddq_feedback_text_to_speech)) ? addslashes($ddq_feedback_text_to_speech) : '',
								   'audio'			=> ( ! empty($feed_audio)) ? $feed_audio : '',
								   'video'			=> ( ! empty($feed_video)) ? $feed_video : '',
								   'screen'			=> ( ! empty($ddq_feedback_rec_screen)) ? $ddq_feedback_rec_screen : '',
								   'image'			=> ( ! empty($ddq_feedback_img)) ? $ddq_feedback_img : '',
								   'document'		=> ( ! empty($ddq_feedback_doc)) ? $ddq_feedback_doc : '');
				$cfeed_sql  = "INSERT INTO `feedback_tbl` (`question_id`, `nextQid`,`EndSim`,`feedback`, `feedback_type`, `feed_speech_text`, `feed_audio`, `feed_video`, `feed_screen`, `feed_image`, `feed_document`) VALUES (:qid,:nextQid,:EndSim, :feedback, :ftype, :speech_text, :audio, :video, :screen, :image, :document)";
				$cfeed_stmt = $db->prepare($cfeed_sql);
				$cfeed_stmt->execute($cfeedData);

				#--------Incorrect-Feedback-------
				if (isset($chkInDNDEndSim) && $chkInDNDEndSim > 0){
					$mappedQuestionDNDIncorrect = '';
				}
				$ifeedData = array('qid'			=> ( ! empty($ques_id)) ? $ques_id : '',
									'nextQid'		=> ( ! empty($mappedQuestionDNDIncorrect)) ? $mappedQuestionDNDIncorrect : '',
									'EndSim'		=> ( ! empty($chkInDNDEndSim)) ? $chkInDNDEndSim : '',
								   'feedback'		=> ( ! empty($ddq_ifeedback)) ? addslashes($ddq_ifeedback) : '',
								   'ftype'			=> 2,
								   'speech_text'	=> ( ! empty($ddqi_feedback_text_to_speech)) ? addslashes($ddqi_feedback_text_to_speech) : '',
								   'audio'			=> ( ! empty($ifeed_audio)) ? $ifeed_audio : '',
								   'video'			=> ( ! empty($ifeed_video)) ? $ifeed_video : '',
								   'screen'			=> ( ! empty($ddqi_feedback_rec_screen)) ? $ddqi_feedback_rec_screen : '',
								   'image'			=> ( ! empty($ddqi_feedback_img)) ? $ddqi_feedback_img : '',
								   'document'		=> ( ! empty($ddqi_feedback_doc)) ? $ddqi_feedback_doc : '');

				$ifeed_sql  = "INSERT INTO `feedback_tbl` (`question_id`, `nextQid`,`EndSim`,`feedback`, `feedback_type`, `feed_speech_text`, `feed_audio`, `feed_video`, `feed_screen`, `feed_image`, `feed_document`) VALUES (:qid,:nextQid, :EndSim,:feedback, :ftype, :speech_text, :audio, :video, :screen, :image, :document)";
				$ifeed_stmt = $db->prepare($ifeed_sql);
				$ifeed_stmt->execute($ifeedData);
			endif;
		endif;
	endif;
	if ( ! empty($submit_type) && $submit_type == 1):
		$resdata = array('success' => TRUE, 'all_update' => FALSE, 'msg' => 'Simulation details updated successfully.', 'return_url' => 'branching-multiple-template.php?add_data=true&sim_id='.md5($sim_id).'&ques_id='.base64_encode($ques_id));
	else:
		$resdata = array('success' => TRUE, 'all_update' => TRUE, 'msg' => 'All questions and answers updated successfully.', 'return_url' => 'author-simulation.php');
	endif;
	echo json_encode($resdata);
endif;

#-----------------------------------------Add-Update-Linear-Classic-Sim ----------------------------------
if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['update_linear_classic_sim']) && isset($_POST['sim_id'])):
	extract($_POST);
	$error = FALSE;
	if (empty($sim_page_name[0]) || empty($sim_duration) || empty($sim_title) || empty($passing_marks)):
		$resdata = array('success' => FALSE, 'msg' => 'Please fill all the mandatory fields on add Simulation and Simulation details to proceed.');
		$error = TRUE;
		echo json_encode($resdata); exit;
	elseif (isset($competency_name_1) && empty($competency_name_1) || empty($competency_score_1) || empty($weightage_1)):
		$resdata = array('success' => FALSE, 'msg' => 'Please fill all the mandatory fields on Simulation details to proceed.');
		$error = TRUE;
		echo json_encode($resdata); exit;
	elseif ($error == FALSE):
		$sql = "UPDATE scenario_master SET `Scenario_title` = '". $sim_title ."', `duration` = '". $sim_duration ."', `passing_marks` = '". $passing_marks ."' , `sim_cover_img` = '". $scenario_cover_file ."'";
		if (isset($character_type) && $character_type == 1):
			$sql .= ", sim_char_img = '', sim_char_left_img = '', sim_char_right_img = '', char_own_choice = ''";
		elseif (isset($charoption) && $charoption == 1 && isset($scenario_char_file) && ! empty($scenario_char_file)):
			$sql .= ", char_own_choice = '". $scenario_char_file ."', sim_char_img = '', sim_char_left_img = '', sim_char_right_img = ''";
		elseif (isset($charoption) && $charoption == 2 && isset($sim_char_img) && ! empty($sim_char_img)):
			$sql .= ", sim_char_img = '". $sim_char_img ."', sim_char_left_img = '', sim_char_right_img = ''";
		elseif (isset($sim_two_char_img) && count($sim_two_char_img) > 0):
			if (isset($sim_two_char_img[0])):
				$r = explode('|', $sim_two_char_img[0]);
				if (isset($r[1]) && $r[1] == 'R'):
					$sql .= ", sim_char_img = '', char_own_choice = '', sim_char_right_img = '". $r[0] ."'";
				elseif (isset($r[1]) && $r[1] == 'L'):
					$sql .= ", sim_char_img = '', char_own_choice = '', sim_char_left_img = '". $r[0] ."'";
				endif;
			endif;
			if (isset($sim_two_char_img[1])):
				$l = explode('|', $sim_two_char_img[1]);
				if (isset($l[1]) && $l[1] == 'L'):
					$sql .= ", sim_char_img = '', char_own_choice = '', sim_char_left_img = '". $l[0] ."'";
				elseif (isset($l[1]) && $l[1] == 'R'):
					$sql .= ", sim_char_img = '', char_own_choice = '', sim_char_right_img = '". $l[0] ."'";
				endif;
			endif;
		endif;
		#--Add-Web-Object--
		if ( ! empty($web_object)):
			$sql .= ", web_object = '". $web_object ."', web_object_title = '". htmlspecialchars($web_object_title) ."'";
		endif;
		#--Sim-Media-File--
		if ( ! empty($scenario_media_file) && in_array(strtolower(pathinfo($scenario_media_file, PATHINFO_EXTENSION)), $img_valid_exts)):
			$sql .= ", image_url = '". $scenario_media_file ."', video_url = NULL, audio_url = NULL";
		elseif ( ! empty($scenario_media_file) && in_array(strtolower(pathinfo($scenario_media_file, PATHINFO_EXTENSION)), $video_valid_exts)):
			$sql .= ", video_url = '". $scenario_media_file ."', image_url = NULL, audio_url = NULL";
		elseif ( ! empty($scenario_media_file) && in_array(strtolower(pathinfo($scenario_media_file, PATHINFO_EXTENSION)), $audio_valid_exts)):
			$sql .= ", audio_url = '". $scenario_media_file ."', video_url = NULL, image_url = NULL";
		elseif ( ! empty($scenario_media_file) && ! in_array(strtolower(pathinfo($scenario_media_file, PATHINFO_EXTENSION)), $audio_valid_exts) && 
				 ! in_array(strtolower(pathinfo($scenario_media_file, PATHINFO_EXTENSION)), $img_valid_exts) && 
				 ! in_array(strtolower(pathinfo($scenario_media_file, PATHINFO_EXTENSION)), $video_valid_exts)):
			$sql .= ", web_object_file = '". $scenario_media_file ."', video_url = NULL, image_url = NULL, audio_url = NULL";
		endif;
		#--Sim-Backgrounds--
		if (isset($bgoption) && $bgoption == 1 && ! empty($bg_color)):
			$sql .= ", bg_color = '". $bg_color ."', bg_own_choice = '', sim_back_img = ''";
		elseif (isset($bgoption) && $bgoption == 2 && ! empty($scenario_bg_file)):
			$sql .= ", bg_own_choice = '". $scenario_bg_file ."', bg_color = '', sim_back_img = ''";
		elseif (isset($bgoption) && $bgoption == 3 && ! empty($sim_background_img)):
			$sql .= ", sim_back_img = '". $sim_background_img ."', bg_own_choice = '', bg_color = ''";
		endif;
		$sql .= " WHERE scenario_id = '". $sim_id ."'";
		$results = $db->prepare($sql);
		if ($results->execute()):
			#----Competency----
			$delCom	 = "DELETE FROM competency_tbl WHERE scenario_id = '". $sim_id ."'";
			$delComq = $db->prepare($delCom); $delComq->execute();
			$data	 = array('scenario_id'	=> $sim_id,
							 'comp_col_1'	=> (isset($competency_name_1) && ! empty($competency_name_1)) ? addslashes($competency_name_1) : '',
							 'comp_val_1'	=> (isset($competency_score_1) && ! empty($competency_score_1)) ? $competency_score_1 : '',
							 'comp_col_2'	=> (isset($competency_name_2) && ! empty($competency_name_2)) ? addslashes($competency_name_2) : '',
							 'comp_val_2'	=> (isset($competency_score_2) && ! empty($competency_score_2)) ? $competency_score_2 : '',
							 'comp_col_3'	=> (isset($competency_name_3) && ! empty($competency_name_3)) ? addslashes($competency_name_3) : '',
							 'comp_val_3'	=> (isset($competency_score_3) && ! empty($competency_score_3)) ? $competency_score_3 : '',
							 'comp_col_4'	=> (isset($competency_name_4) && ! empty($competency_name_4)) ? addslashes($competency_name_4) : '',
							 'comp_val_4'	=> (isset($competency_score_4) && ! empty($competency_score_4)) ? $competency_score_4 : '',
							 'comp_col_5'	=> (isset($competency_name_5) && ! empty($competency_name_5)) ? addslashes($competency_name_5) : '',
							 'comp_val_5'	=> (isset($competency_score_5) && ! empty($competency_score_5)) ? $competency_score_5 : '',
							 'comp_col_6'	=> (isset($competency_name_6) && ! empty($competency_name_6)) ? addslashes($competency_name_6) : '',
							 'comp_val_6'	=> (isset($competency_score_6) && ! empty($competency_score_6)) ? $competency_score_6 : '',
							 'weightage_1'	=> (isset($weightage_1) && ! empty($weightage_1)) ? $weightage_1 : '',
							 'weightage_2'	=> (isset($weightage_2) && ! empty($weightage_2)) ? $weightage_2 : '',
							 'weightage_3'	=> (isset($weightage_3) && ! empty($weightage_3)) ? $weightage_3 : '',
							 'weightage_4'	=> (isset($weightage_4) && ! empty($weightage_4)) ? $weightage_4 : '',
							 'weightage_5'	=> (isset($weightage_5) && ! empty($weightage_5)) ? $weightage_5 : '',
							 'weightage_6'	=> (isset($weightage_6) && ! empty($weightage_6)) ? $weightage_6 : '',
							 'status' 		=> 1,
							 'user_id' 		=> $userId,
							 'cur_date'		=> $curdate);
			$com_sql  = "INSERT INTO `competency_tbl` (`scenario_id`, `comp_col_1`, `comp_val_1`, `comp_col_2`, `comp_val_2`, `comp_col_3`, `comp_val_3`, `comp_col_4`, `comp_val_4`, `comp_col_5`, `comp_val_5`, `comp_col_6`, `comp_val_6`, `weightage_1`, `weightage_2`, `weightage_3`, `weightage_4`, `weightage_5`, `weightage_6`, `status`, `uid`, `cur_date`) VALUES (:scenario_id, :comp_col_1, :comp_val_1, :comp_col_2, :comp_val_2, :comp_col_3, :comp_val_3, :comp_col_4, :comp_val_4, :comp_col_5, :comp_val_5, :comp_col_6, :comp_val_6, :weightage_1, :weightage_2, :weightage_3, :weightage_4, :weightage_5, :weightage_6, :status, :user_id, :cur_date)";
			$com_stmt = $db->prepare($com_sql);
			$com_stmt->execute($data);
			
			#----PAGES----
			$delPage  = "DELETE FROM sim_pages WHERE scenario_id = '". $sim_id ."'";
			$delPageq = $db->prepare($delPage); $delPageq->execute();
			if ( ! empty($sim_page_name)): $sp = 0;
				foreach ($sim_page_name as $pdata):
					$pageq = "INSERT INTO `sim_pages` (`scenario_id`, `sim_page_name`, `sim_page_desc`, `userid`, `status`, `added_date`) VALUES (:sid, :pname, :pdesc, :user_id, :status, :adate)";
					$page_stmt = $db->prepare($pageq);
					$page_stmt->execute(array('sid'		=> $sim_id,
											  'pname'	=> ( ! empty($pdata)) ? htmlspecialchars($pdata) : '',
											  'pdesc'	=> ( ! empty($sim_page_desc[$sp])) ? $sim_page_desc[$sp] : '',
											  'status' 	=> 1,
											  'user_id'	=> $userId,
											  'adate'	=> $curdate));
				$sp++; endforeach;
			endif;
			
			#----Branding----
			$bandq	  	= "DELETE FROM sim_branding_tbl WHERE scenario_id = '". $sim_id ."'";
			$bandqexe 	= $db->prepare($bandq); $bandqexe->execute();
			$brand_data	= array('sid'				=> $sim_id,
								'bg'				=> ( ! empty($ques_bg_color)) ? $ques_bg_color : '',
								'bg_tr'				=> ( ! empty($ques_bg_transp)) ? $ques_bg_transp : '',
								'obg'				=> ( ! empty($ques_option_bg_color)) ? $ques_option_bg_color : '',
								'obg_tr'			=> ( ! empty($ques_option_bg_transp)) ? $ques_option_bg_transp : '',
								'ohover'			=> ( ! empty($ques_option_hover_color)) ? $ques_option_hover_color : '',
								'oh_tr'				=> ( ! empty($ques_option_hover_transp)) ? $ques_option_hover_transp : '',
								'oselected'			=> ( ! empty($ques_option_select_color)) ? $ques_option_select_color : '',
								'oselected_tr'		=> ( ! empty($ques_option_select_transp)) ? $ques_option_select_transp : '',
								'btn_bg'			=> ( ! empty($btn_bg_color)) ? $btn_bg_color : '',
								'btn_bg_tr'			=> ( ! empty($btn_bg_transp)) ? $btn_bg_transp : '',
								'btn_hover'			=> ( ! empty($btn_hover_color)) ? $btn_hover_color : '',
								'btn_hover_tr'		=> ( ! empty($btn_hover_transp)) ? $btn_hover_transp : '',
								'btn_selected'		=> ( ! empty($btn_select_color)) ? $btn_select_color : '',
								'btn_selected_tr'	=> ( ! empty($btn_select_transp)) ? $btn_select_transp : '',
								'font_type'			=> ( ! empty($font_type)) ? $font_type : '',
								'font_color'		=> ( ! empty($font_color)) ? $font_color : '',
								'font_size'			=> ( ! empty($font_size)) ? $font_size : '',
								'user_id' 			=> $userId,
								'added_date'		=> $curdate);
			$com_sql  = "INSERT INTO `sim_branding_tbl` (`scenario_id`, `ques_bg`, `ques_bg_transparency`, `option_bg`, `option_bg_transparency`, `option_hover`, `option_hover_transparency`, `option_selected`, `option_selected_transparency`, `btn_bg`, `btn_bg_transparency`, `btn_hover`, `btn_hover_transparency`, `btn_selected`, `btn_selected_transparency`, `font_type`, `font_color`, `font_size`, `user_id`, `added_date`) 
						 VALUES (:sid, :bg, :bg_tr, :obg, :obg_tr, :ohover, :oh_tr, :oselected, :oselected_tr, :btn_bg, :btn_bg_tr, :btn_hover, :btn_hover_tr, :btn_selected, :btn_selected_tr, :font_type, :font_color, :font_size, :user_id, :added_date)";
			$com_stmt = $db->prepare($com_sql);
			$com_stmt->execute($brand_data);
		endif;
		
		//-----------START-MCQ----------
		if ($question_type == 4):
			#-------QUESTIONS-DATA---
			$qaudio = '';
			if ( ! empty($mcqq_qaudio)):
				$qaudio = $mcqq_qaudio;
			elseif ( ! empty($mcqq_rec_audio)):
				$qaudio = $mcqq_rec_audio;
			endif;
			
			$audio = '';
			if ( ! empty($mcq_audio)):
				$audio = $mcq_audio;
			elseif ( ! empty($mcq_rec_audio)):
				$audio = $mcq_rec_audio;
			endif;
			
			$video = '';
			if ( ! empty($mcq_video)):
				$video = $mcq_video;
			elseif ( ! empty($mcq_rec_video)):
				$video = $mcq_rec_video;
			endif;
			
			$quesData = array('questions'		=> ( ! empty($mcq)) ? htmlspecialchars($mcq) : '',
							  'qtype'			=> ( ! empty($question_type)) ? $question_type : '',
							  'qaudio'			=> ( ! empty($qaudio)) ? $qaudio : '',
							  'qspeech_text'	=> ( ! empty($mcqq_text_to_speech)) ? htmlspecialchars($mcqq_text_to_speech) : '',
							  'speech_text'		=> ( ! empty($mcq_text_to_speech)) ? htmlspecialchars($mcq_text_to_speech) : '',
							  'audio'			=> ( ! empty($audio)) ? $audio : '',
							  'video'			=> ( ! empty($video)) ? $video : '',
							  'screen'			=> ( ! empty($mcq_rec_screen)) ? $mcq_rec_screen : '',
							  'image'			=> ( ! empty($mcq_img)) ? $mcq_img : '',
							  'document'		=> ( ! empty($mcq_doc)) ? $mcq_doc : '',
							  'true_option'		=> ( ! empty($mcq_true_option)) ? $mcq_true_option : '',
							  'shuffle'			=> (isset($mcq_shuffle) && ! empty($mcq_shuffle)) ? 1 : 0,
							  'ques_val_1'		=> ( ! empty($mcq_ques_val_1)) ? $mcq_ques_val_1 : '',
							  'ques_val_2'		=> ( ! empty($mcq_ques_val_2)) ? $mcq_ques_val_2 : '',
							  'ques_val_3'		=> ( ! empty($mcq_ques_val_3)) ? $mcq_ques_val_3 : '',
							  'ques_val_4'		=> ( ! empty($mcq_ques_val_4)) ? $mcq_ques_val_4 : '',
							  'ques_val_5'		=> ( ! empty($mcq_ques_val_5)) ? $mcq_ques_val_5 : '',
							  'ques_val_6'		=> ( ! empty($mcq_ques_val_6)) ? $mcq_ques_val_6 : '',
							  'critical'		=> ( ! empty($mcq_criticalQ)) ? $mcq_criticalQ : '',
							  'status' 			=> 1,
							  'user_id'			=> $userId,
							  'cur_date'		=> $curdate,
							  'qid'				=> $ques_id);
			$ques_sql  = "UPDATE `question_tbl` SET `questions`=:questions, `question_type`=:qtype, `qaudio`=:qaudio, `qspeech_text`=:qspeech_text, `speech_text`=:speech_text, `audio`=:audio, `video`=:video, `screen`=:screen, `image`=:image, `document`=:document, `true_options`=:true_option, `shuffle`=:shuffle, `ques_val_1`=:ques_val_1, `ques_val_2`=:ques_val_2, `ques_val_3`=:ques_val_3, `ques_val_4`=:ques_val_4, `ques_val_5`=:ques_val_5, `ques_val_6`=:ques_val_6, `critical`=:critical, `status`=:status, `uid`=:user_id, `cur_date`=:cur_date WHERE question_id=:qid";
			$ques_stmt = $db->prepare($ques_sql);
			$ques_stmt->execute($quesData);
			
			#-----Remove-Old-Feedback-------
			$db->feedback_remove($ques_id);
			
			#----------ADD-OPTIONS-------------
			if ( ! empty($mcq_option[0])):
				$mcqi = 0; $mcqtrue = 1;
				foreach ($mcq_option as $answerData):
					if ( ! empty($updateMCQAnswerid[$mcqi])):
						$ans_id = $updateMCQAnswerid[$mcqi];
						$ansSql = "UPDATE `answer_tbl` SET `choice_option`=:option, `ans_val1`=:val1, `ans_val2`=:val2, `ans_val3`=:val3, `ans_val4`=:val4, `ans_val5`=:val5, `ans_val6`=:val6, `true_option`=:true, `status`=:status, `uid`=:user_id, `cur_date`=:cur_date WHERE answer_id=:aid";
						$stmt	= $db->prepare($ansSql);
						$stmt->execute(array('aid'		=> $ans_id,
											 'option'	=> ( ! empty($mcq_option[$mcqi])) ? htmlspecialchars($mcq_option[$mcqi]) : '',
											 'val1'		=> ( ! empty($mcq_ans_val1[$mcqi])) ? $mcq_ans_val1[$mcqi] : '',
											 'val2'		=> ( ! empty($mcq_ans_val2[$mcqi])) ? $mcq_ans_val2[$mcqi] : '',
											 'val3'		=> ( ! empty($mcq_ans_val3[$mcqi])) ? $mcq_ans_val3[$mcqi] : '',
											 'val4'		=> ( ! empty($mcq_ans_val4[$mcqi])) ? $mcq_ans_val4[$mcqi] : '',
											 'val5'		=> ( ! empty($mcq_ans_val5[$mcqi])) ? $mcq_ans_val5[$mcqi] : '',
											 'val6'		=> ( ! empty($mcq_ans_val6[$mcqi])) ? $mcq_ans_val6[$mcqi] : '',
											 'true'		=> ( ! empty($mcq_true_option) && $mcq_true_option == $mcqtrue) ? 1 : '',
											 'status'	=> 1,
											 'user_id' 	=> $userId,
											 'cur_date'	=> $curdate));
					else:
						$ansSql = "INSERT INTO `answer_tbl` (`question_id`, `choice_option`, `ans_val1`, `ans_val2`, `ans_val3`, `ans_val4`, `ans_val5`, `ans_val6`, `true_option`, `status`, `uid`, `cur_date`) values (:qid, :option, :ans_val1, :ans_val2, :ans_val3, :ans_val4, :ans_val5, :ans_val6, :true, :status, :user_id, :cur_date)";
						$stmt	= $db->prepare($ansSql);
						$stmt->execute(array('qid'		=> $ques_id,
											 'option'	=> ( ! empty($mcq_option[$mcqi])) ? htmlspecialchars($mcq_option[$mcqi]) : '',
											 'ans_val1'	=> ( ! empty($mcq_ans_val1[$mcqi])) ? $mcq_ans_val1[$mcqi] : '',
											 'ans_val2'	=> ( ! empty($mcq_ans_val2[$mcqi])) ? $mcq_ans_val2[$mcqi] : '',
											 'ans_val3'	=> ( ! empty($mcq_ans_val3[$mcqi])) ? $mcq_ans_val3[$mcqi] : '',
											 'ans_val4'	=> ( ! empty($mcq_ans_val4[$mcqi])) ? $mcq_ans_val4[$mcqi] : '',
											 'ans_val5'	=> ( ! empty($mcq_ans_val5[$mcqi])) ? $mcq_ans_val5[$mcqi] : '',
											 'ans_val6'	=> ( ! empty($mcq_ans_val6[$mcqi])) ? $mcq_ans_val6[$mcqi] : '',
											 'true'		=> ( ! empty($mcq_true_option) && $mcq_true_option == $mcqtrue) ? 1 : '',
											 'status'	=> 1,
											 'user_id' 	=> $userId,
											 'cur_date'	=> $curdate));
						$ans_id = $db->lastInsertId();
					endif;
					
					#------Correct-FEEDBACK-DATA---
					$feed_audio = '';
					if ( ! empty($mcq_feedback_audio[$mcqi])):
						$feed_audio = $mcq_feedback_audio[$mcqi];
					elseif ( ! empty($mcq_feedback_rec_audio[$mcqi])):
						$feed_audio = $mcq_feedback_rec_audio[$mcqi];
					endif;
					
					$feed_video = '';
					if ( ! empty($mcq_feedback_video[$mcqi])):
						$feed_video = $mcq_feedback_video[$mcqi];
					elseif ( ! empty($mcq_feedback_rec_video[$mcqi])):
						$feed_video = $mcq_feedback_rec_video[$mcqi];
					endif;
					
					#--------Correct-Feedback----------
					$cfeedData = array('qid'			=> ( ! empty($ques_id)) ? $ques_id : '',
									   'aid'			=> ( ! empty($ans_id)) ? $ans_id : '',
									   'feedback'		=> ( ! empty($mcq_cfeedback[$mcqi])) ? htmlspecialchars($mcq_cfeedback[$mcqi]) : '',
									   'ftype'			=> 1,
									   'speech_text'	=> ( ! empty($mcq_feedback_text_to_speech[$mcqi])) ? htmlspecialchars($mcq_feedback_text_to_speech[$mcqi]) : '',
									   'audio'			=> ( ! empty($feed_audio)) ? $feed_audio : '',
									   'video'			=> ( ! empty($feed_video)) ? $feed_video : '',
									   'screen'			=> ( ! empty($mcq_feedback_rec_screen[$mcqi])) ? $mcq_feedback_rec_screen[$mcqi] : '',
									   'image'			=> ( ! empty($mcq_feedback_img[$mcqi])) ? $mcq_feedback_img[$mcqi] : '',
									   'document'		=> ( ! empty($mcq_feedback_doc[$mcqi])) ? $mcq_feedback_doc[$mcqi] : '');
					$cfeed_sql  = "INSERT INTO `feedback_tbl` (`question_id`, `answer_id`, `feedback`, `feedback_type`, `feed_speech_text`, `feed_audio`, `feed_video`, `feed_screen`, `feed_image`, `feed_document`) VALUES (:qid, :aid, :feedback, :ftype, :speech_text, :audio, :video, :screen, :image, :document)";
					$cfeed_stmt = $db->prepare($cfeed_sql);
					$cfeed_stmt->execute($cfeedData);
					$mcqi++; $mcqtrue++;
				endforeach;
			endif;
		//-----------END-MCQ----------

		//-----------START-SWIPING-----
		elseif ($question_type == 7):
			#-------QUESTIONS-DATA---
			$qaudio = '';
			if ( ! empty($swipqq_qaudio)):
				$qaudio = $swipqq_qaudio;
			elseif ( ! empty($swipqq_rec_audio)):
				$qaudio = $swipqq_rec_audio;
			endif;

			$audio = '';
			if ( ! empty($swipq_audio)):
				$audio = $swipq_audio;
			elseif ( ! empty($swipq_rec_audio)):
				$audio = $swipq_rec_audio;
			endif;
			
			$video = '';
			if ( ! empty($swipq_video)):
				$video = $swipq_video;
			elseif ( ! empty($swipq_rec_video)):
				$video = $swipq_rec_video;
			endif;
			
			$quesData = array('questions'		=> ( ! empty($swipq)) ? htmlspecialchars($swipq) : '',
							  'qtype'			=> ( ! empty($question_type)) ? $question_type : '',
							  'qaudio'			=> ( ! empty($qaudio)) ? $qaudio : '',
							  'qspeech_text'	=> ( ! empty($swipqq_text_to_speech)) ? htmlspecialchars($swipqq_text_to_speech) : '',
							  'speech_text'		=> ( ! empty($swipq_text_to_speech)) ? htmlspecialchars($swipq_text_to_speech) : '',
							  'audio'			=> ( ! empty($audio)) ? $audio : '',
							  'video'			=> ( ! empty($video)) ? $video : '',
							  'screen'			=> ( ! empty($swipq_rec_screen)) ? $swipq_rec_screen : '',
							  'image'			=> ( ! empty($swipq_img)) ? $swipq_img : '',
							  'document'		=> ( ! empty($swipq_doc)) ? $swipq_doc : '',
							  'true_option'		=> ( ! empty($swipq_true_option)) ? $swipq_true_option : '',
							  'shuffle'			=> (isset($swipq_shuffle) && ! empty($swipq_shuffle)) ? 1 : 0,
							  'ques_val_1'		=> ( ! empty($swipq_ques_val_1)) ? $swipq_ques_val_1 : '',
							  'ques_val_2'		=> ( ! empty($swipq_ques_val_2)) ? $swipq_ques_val_2 : '',
							  'ques_val_3'		=> ( ! empty($swipq_ques_val_3)) ? $swipq_ques_val_3 : '',
							  'ques_val_4'		=> ( ! empty($swipq_ques_val_4)) ? $swipq_ques_val_4 : '',
							  'ques_val_5'		=> ( ! empty($swipq_ques_val_5)) ? $swipq_ques_val_5 : '',
							  'ques_val_6'		=> ( ! empty($swipq_ques_val_6)) ? $swipq_ques_val_6 : '',
							  'critical'		=> ( ! empty($swip_criticalQ)) ? $swip_criticalQ : '',
							  'status' 			=> 1,
							  'user_id'			=> $userId,
							  'cur_date'		=> $curdate,
							  'qid'				=> $ques_id);
			$ques_sql  = "UPDATE `question_tbl` SET `questions`=:questions, `question_type`=:qtype, `qaudio`=:qaudio, `qspeech_text`=:qspeech_text, `speech_text`=:speech_text, `audio`=:audio, `video`=:video, `screen`=:screen, `image`=:image, `document`=:document, `true_options`=:true_option, `shuffle`=:shuffle, `ques_val_1`=:ques_val_1, `ques_val_2`=:ques_val_2, `ques_val_3`=:ques_val_3, `ques_val_4`=:ques_val_4, `ques_val_5`=:ques_val_5, `ques_val_6`=:ques_val_6, `critical`=:critical, `status`=:status, `uid`=:user_id, `cur_date`=:cur_date WHERE question_id=:qid";
			$ques_stmt = $db->prepare($ques_sql);
			$ques_stmt->execute($quesData);
			
			#-----Remove-Old-Feedback-------
			$db->feedback_remove($ques_id);
			
			#----------ADD-OPTIONS-------------
			if ( ! empty($swipq_option[0])):
				$swipqi = 0; $swiptrue = 1;
				foreach ($swipq_option as $answerData):
					if ( ! empty($updateSwipqAnswerid[$swipqi])):
						$getSwipqAnswerid = $updateSwipqAnswerid[$swipqi];
						$ansSql = "UPDATE `answer_tbl` SET `choice_option`=:option, `image`=:image, `ans_val1`=:val1, `ans_val2`=:val2, `ans_val3`=:val3, `ans_val4`=:val4, `ans_val5`=:val5, `ans_val6`=:val6, `true_option`=:true, `status`=:status, `uid`=:user_id, `cur_date`=:cur_date WHERE answer_id=:aid";
						$stmt   = $db->prepare($ansSql);
						$stmt->execute(array('aid' 		=> $getSwipqAnswerid,
											 'option'	=> ( ! empty($swipq_option[$swipqi])) ? htmlspecialchars($swipq_option[$swipqi]) : '',
											 'image'	=> ( ! empty($swipq_item_img[$swipqi])) ? $swipq_item_img[$swipqi] : '',
											 'val1'		=> ( ! empty($swip_ans_val1[$swipqi])) ? $swip_ans_val1[$swipqi] : '',
											 'val2'		=> ( ! empty($swip_ans_val2[$swipqi])) ? $swip_ans_val2[$swipqi] : '',
											 'val3'		=> ( ! empty($swip_ans_val3[$swipqi])) ? $swip_ans_val3[$swipqi] : '',
											 'val4'		=> ( ! empty($swip_ans_val4[$swipqi])) ? $swip_ans_val4[$swipqi] : '',
											 'val5'		=> ( ! empty($swip_ans_val5[$swipqi])) ? $swip_ans_val5[$swipqi] : '',
											 'val6'		=> ( ! empty($swip_ans_val6[$swipqi])) ? $swip_ans_val6[$swipqi] : '',
											 'true'		=> ( ! empty($swipq_true_option) && $swipq_true_option == $swiptrue) ? 1 : '',
											 'status'	=> 1,
											 'user_id' 	=> $userId,
											 'cur_date'	=> $curdate));
						
					else:
						$ansSql = "INSERT INTO `answer_tbl` (`question_id`, `choice_option`, `image`, `ans_val1`, `ans_val2`, `ans_val3`, `ans_val4`, `ans_val5`, `ans_val6`, `true_option`, `status`, `uid`, `cur_date`) values (:qid, :option, :image, :val1, :val2, :val3, :val4, :val5, :val6, :true, :status, :user_id, :cur_date)";
						$stmt = $db->prepare($ansSql);
						$stmt->execute(array('qid'		=> $ques_id,
											 'option'	=> ( ! empty($swipq_option[$swipqi])) ? htmlspecialchars($swipq_option[$swipqi]) : '',
											 'image'	=> ( ! empty($swipq_item_img[$swipqi])) ? $swipq_item_img[$swipqi] : '',
											 'val1'		=> ( ! empty($swip_ans_val1[$swipqi])) ? $swip_ans_val1[$swipqi] : '',
											 'val2'		=> ( ! empty($swip_ans_val2[$swipqi])) ? $swip_ans_val2[$swipqi] : '',
											 'val3'		=> ( ! empty($swip_ans_val3[$swipqi])) ? $swip_ans_val3[$swipqi] : '',
											 'val4'		=> ( ! empty($swip_ans_val4[$swipqi])) ? $swip_ans_val4[$swipqi] : '',
											 'val5'		=> ( ! empty($swip_ans_val5[$swipqi])) ? $swip_ans_val5[$swipqi] : '',
											 'val6'		=> ( ! empty($swip_ans_val6[$swipqi])) ? $swip_ans_val6[$swipqi] : '',
											 'true'		=> ( ! empty($swipq_true_option) && $swipq_true_option == $swiptrue) ? 1 : '',
											 'status'	=> 1,
											 'user_id' 	=> $userId,
											 'cur_date'	=> $curdate));
						$getSwipqAnswerid =  $db->lastInsertId();
					endif;
					#------FEEDBACK-DATA---
					$feed_audio = '';
					if ( ! empty($swip_feedback_audio[$swipqi])):
						$feed_audio = $swip_feedback_audio[$swipqi];
					elseif ( ! empty($swip_feedback_rec_audio[$swipqi])):
						$feed_audio = $swip_feedback_rec_audio[$swipqi];
					endif;
					
					$feed_video = '';
					if ( ! empty($swip_feedback_video[$swipqi])):
						$feed_video = $swip_feedback_video[$swipqi];
					elseif ( ! empty($swip_feedback_rec_video[$swipqi])):
						$feed_video = $swip_feedback_rec_video[$swipqi];
					endif;
					
					#--------Feedback----------
					$cfeedData = array('qid'			=> $ques_id,
									   'aid'			=> ( ! empty($getSwipqAnswerid)) ? $getSwipqAnswerid : '',
									   'feedback'		=> ( ! empty($swip_cfeedback[$swipqi])) ? htmlspecialchars($swip_cfeedback[$swipqi]) : '',
									   'ftype'			=> 1,
									   'speech_text'	=> ( ! empty($swip_feedback_text_to_speech[$swipqi])) ? htmlspecialchars($swip_feedback_text_to_speech[$swipqi]) : '',
									   'audio'			=> ( ! empty($feed_audio)) ? $feed_audio : '',
									   'video'			=> ( ! empty($feed_video)) ? $feed_video : '',
									   'screen'			=> ( ! empty($swip_feedback_rec_screen[$swipqi])) ? $swip_feedback_rec_screen[$swipqi] : '',
									   'image'			=> ( ! empty($swip_feedback_img[$swipqi])) ? $swip_feedback_img[$swipqi] : '',
									   'document'		=> ( ! empty($swip_feedback_doc[$swipqi])) ? $swip_feedback_doc[$swipqi] : '');
					$cfeed_sql  = "INSERT INTO `feedback_tbl` (`question_id`, `answer_id`, `feedback`, `feedback_type`, `feed_speech_text`, `feed_audio`, `feed_video`, `feed_screen`, `feed_image`, `feed_document`) VALUES (:qid, :aid, :feedback, :ftype, :speech_text, :audio, :video, :screen, :image, :document)";
					$cfeed_stmt = $db->prepare($cfeed_sql);
					$cfeed_stmt->execute($cfeedData);
					$swipqi++; $swiptrue++;
				endforeach;
			endif;
		//-----------END-SWIPING----------
		
		//-----------START-DDQ----------
		elseif ($question_type == 8):
			#-------QUESTIONS-DATA---
			$qaudio = '';
			if ( ! empty($ddqq_qaudio)):
				$qaudio = $ddqq_qaudio;
			elseif ( ! empty($ddqq_rec_audio)):
				$qaudio = $ddqq_rec_audio;
			endif;
		
			$audio = '';
			if ( ! empty($ddq_audio)):
				$audio = $ddq_audio;
			elseif ( ! empty($ddq_rec_audio)):
				$audio = $ddq_rec_audio;
			endif;
			
			$video = '';
			if ( ! empty($ddq_video)):
				$video = $ddq_video;
			elseif ( ! empty($ddq_rec_video)):
				$video = $ddq_rec_video;
			endif;
			
			#---DROP-OPTION---- 
			$quesData = array('questions'		=> ( ! empty($ddq)) ? htmlspecialchars($ddq) : '',
							  'seleted_drag' 	=> 2,
							  'qtype'			=> ( ! empty($question_type)) ? $question_type : '',
							  'qaudio'			=> ( ! empty($qaudio)) ? $qaudio : '',
							  'qspeech_text'	=> ( ! empty($ddqq_text_to_speech)) ? htmlspecialchars($ddqq_text_to_speech) : '',
							  'speech_text'		=> ( ! empty($ddq_text_to_speech)) ? htmlspecialchars($ddq_text_to_speech) : '',
							  'audio'			=> ( ! empty($audio)) ? $audio : '',
							  'video'			=> ( ! empty($video)) ? $video : '',
							  'screen'			=> ( ! empty($ddq_rec_screen)) ? $ddq_rec_screen : '',
							  'image'			=> ( ! empty($ddq_img)) ? $ddq_img : '',
							  'document'		=> ( ! empty($ddq_doc)) ? $ddq_doc : '',
							  'shuffle'			=> (isset($ddq_shuffle2) && ! empty($ddq_shuffle2)) ? 1 : 0,
							  'true_options'	=> ( ! empty($ddq_true_option)) ? $ddq_true_option : '',
							  'ques_val_1'		=> ( ! empty($ddq_ques_val_1)) ? $ddq_ques_val_1 : '',
							  'ques_val_2'		=> ( ! empty($ddq_ques_val_2)) ? $ddq_ques_val_2 : '',
							  'ques_val_3'		=> ( ! empty($ddq_ques_val_3)) ? $ddq_ques_val_3 : '',
							  'ques_val_4'		=> ( ! empty($ddq_ques_val_4)) ? $ddq_ques_val_4 : '',
							  'ques_val_5'		=> ( ! empty($ddq_ques_val_5)) ? $ddq_ques_val_5 : '',
							  'ques_val_6'		=> ( ! empty($ddq_ques_val_6)) ? $ddq_ques_val_6 : '',
							  'critical'		=> ( ! empty($dnd_criticalQ)) ? $dnd_criticalQ : '',
							  'status' 			=> 1,
							  'user_id'			=> $userId,
							  'cur_date'		=> $curdate,
							  'qid'				=> $ques_id);
			$ques_sql  = "UPDATE `question_tbl` SET `questions`=:questions, `question_type`=:qtype, `seleted_drag_option`=:seleted_drag, `qaudio`=:qaudio, `qspeech_text`=:qspeech_text, `speech_text`=:speech_text, `audio`=:audio, `video`=:video, `screen`=:screen, `image`=:image, `document`=:document, `shuffle`=:shuffle, `true_options`=:true_options, `ques_val_1`=:ques_val_1, `ques_val_2`=:ques_val_2, `ques_val_3`=:ques_val_3, `ques_val_4`=:ques_val_4, `ques_val_5`=:ques_val_5, `ques_val_6`=:ques_val_6, `critical`=:critical, `status`=:status, `uid`=:user_id, `cur_date`=:cur_date WHERE question_id=:qid";
			$ques_stmt = $db->prepare($ques_sql);
			$ques_stmt->execute($quesData);
				
			#-----Remove-Old-Feedback------
			$db->feedback_remove($ques_id);
			
			#------DRAG-OPTIONS------
			if ( ! empty($ddq2_drop)):
				if ( ! empty($updateDDq2Answerid)):
					#----DROP-TARGET----
					$ansSql = "UPDATE `sub_options_tbl` SET `sub_option`=:option, `image`=:image WHERE sub_options_id=:sid";
					$stmt	= $db->prepare($ansSql);
					$stmt->execute(array('sid' => $updateDDq2Answerid, 'option' => ( ! empty($ddq2_drop)) ? htmlspecialchars($ddq2_drop) : '', 'image' => ( ! empty($ddq2_item_img)) ? $ddq2_item_img : ''));
					
					#-----DRAG-ITEMS------
					$ddq2iu = 0; $ddq2true = 1;
					foreach ($ddq2_drag as $subAnswerData):
						if ( ! empty($updateDDq2SubAnswerid[$ddq2iu])):
							$getOpId = $updateDDq2SubAnswerid[$ddq2iu];
							$updateAnsSql  = "UPDATE `answer_tbl` SET `choice_option`=:option, `image`=:image, `ans_val1`=:ans_val1, `ans_val2`=:ans_val2, `ans_val3`=:ans_val3, `ans_val4`=:ans_val4, `ans_val5`=:ans_val5, `ans_val6`=:ans_val6, `true_option`=:true, `status`=:status, `uid`=:user_id, `cur_date`=:cur_date WHERE answer_id=:aid";
							$updateAnsstmt = $db->prepare($updateAnsSql);
							$updateAnsstmt->execute(array('aid' 		=> $getOpId,
														  'option'		=> ( ! empty($ddq2_drag[$ddq2iu])) ? htmlspecialchars($ddq2_drag[$ddq2iu]) : '',
														  'image'		=> ( ! empty($ddq2_drag_item_img[$ddq2iu])) ? $ddq2_drag_item_img[$ddq2iu] : '',
														  'ans_val1'	=> ( ! empty($dd2_ans_val1[$ddq2iu])) ? $dd2_ans_val1[$ddq2iu] : '',
														  'ans_val2'	=> ( ! empty($dd2_ans_val2[$ddq2iu])) ? $dd2_ans_val2[$ddq2iu] : '',
														  'ans_val3'	=> ( ! empty($dd2_ans_val3[$ddq2iu])) ? $dd2_ans_val3[$ddq2iu] : '',
														  'ans_val4'	=> ( ! empty($dd2_ans_val4[$ddq2iu])) ? $dd2_ans_val4[$ddq2iu] : '',
														  'ans_val5'	=> ( ! empty($dd2_ans_val5[$ddq2iu])) ? $dd2_ans_val5[$ddq2iu] : '',
														  'ans_val6'	=> ( ! empty($dd2_ans_val6[$ddq2iu])) ? $dd2_ans_val6[$ddq2iu] : '',
														  'true'		=> ( ! empty($ddq_true_option) && $ddq_true_option == $ddq2true) ? 1 : '',
														  'status'		=> 1,
														  'user_id' 	=> $userId,
														  'cur_date'	=> $curdate));
						else:
							$ansSql = "INSERT INTO `answer_tbl` (`question_id`, `choice_option`, `image`, `ans_val1`, `ans_val2`, `ans_val3`, `ans_val4`, `ans_val5`, `ans_val6`, `true_option`, `status`, `uid`, `cur_date`) values (:qid, :option, :image, :ans_val1, :ans_val2, :ans_val3, :ans_val4, :ans_val5, :ans_val6, :true, :status, :user_id, :cur_date)";
							$stmt 	= $db->prepare($ansSql);
							$stmt->execute(array('qid'		=> $ques_id,
												 'option'	=> ( ! empty($ddq2_drag[$ddq2iu])) ? htmlspecialchars($ddq2_drag[$ddq2iu]) : '',
												 'image'	=> ( ! empty($ddq2_drag_item_img[$ddq2iu])) ? $ddq2_drag_item_img[$ddq2iu] : '',
												 'ans_val1'	=> ( ! empty($dd2_ans_val1[$ddq2iu])) ? $dd2_ans_val1[$ddq2iu] : '',
												 'ans_val2'	=> ( ! empty($dd2_ans_val2[$ddq2iu])) ? $dd2_ans_val2[$ddq2iu] : '',
												 'ans_val3'	=> ( ! empty($dd2_ans_val3[$ddq2iu])) ? $dd2_ans_val3[$ddq2iu] : '',
												 'ans_val4'	=> ( ! empty($dd2_ans_val4[$ddq2iu])) ? $dd2_ans_val4[$ddq2iu] : '',
												 'ans_val5'	=> ( ! empty($dd2_ans_val5[$ddq2iu])) ? $dd2_ans_val5[$ddq2iu] : '',
												 'ans_val6'	=> ( ! empty($dd2_ans_val6[$ddq2iu])) ? $dd2_ans_val6[$ddq2iu] : '',
												 'true'		=> ( ! empty($ddq_true_option) && $ddq_true_option == $ddq2true) ? 1 : '',
												 'status'	=> 1,
												 'user_id' 	=> $userId,
												 'cur_date'	=> $curdate));
							$getOpId = $db->lastInsertId();
						endif;
						
						#-----FEEDBACK-DATA---
						$feed_audio = '';
						if ( ! empty($dd2_feedback_audio[$ddq2iu])):
							$feed_audio = $dd2_feedback_audio[$ddq2iu];
						elseif ( ! empty($dd2_feedback_rec_audio[$ddq2iu])):
							$feed_audio = $dd2_feedback_rec_audio[$ddq2iu];
						endif;
						
						$feed_video = '';
						if ( ! empty($dd2_feedback_video[$ddq2iu])):
							$feed_video = $dd2_feedback_video[$ddq2iu];
						elseif ( ! empty($dd2_feedback_rec_video[$ddq2iu])):
							$feed_video = $dd2_feedback_rec_video[$ddq2iu];
						endif;
						
						#--------Feedback----------
						$cfeedData = array('qid'			=> ( ! empty($ques_id)) ? $ques_id : '',
										   'aid'			=> ( ! empty($getOpId)) ? $getOpId : '',
										   'feedback'		=> ( ! empty($dd2_cfeedback[$ddq2iu])) ? htmlspecialchars($dd2_cfeedback[$ddq2iu]) : '',
										   'ftype'			=> 1,
										   'speech_text'	=> ( ! empty($dd2_feedback_text_to_speech[$ddq2iu])) ? htmlspecialchars($dd2_feedback_text_to_speech[$ddq2iu]) : '',
										   'audio'			=> ( ! empty($feed_audio)) ? $feed_audio : '',
										   'video'			=> ( ! empty($feed_video)) ? $feed_video : '',
										   'screen'			=> ( ! empty($dd2_feedback_rec_screen[$ddq2iu])) ? $dd2_feedback_rec_screen[$ddq2iu] : '',
										   'image'			=> ( ! empty($dd2_feedback_img[$ddq2iu])) ? $dd2_feedback_img[$ddq2iu] : '',
										   'document'		=> ( ! empty($dd2_feedback_doc[$ddq2iu])) ? $dd2_feedback_doc[$ddq2iu] : '');
						$cfeed_sql  = "INSERT INTO `feedback_tbl` (`question_id`, `answer_id`, `feedback`, `feedback_type`, `feed_speech_text`, `feed_audio`, `feed_video`, `feed_screen`, `feed_image`, `feed_document`) VALUES (:qid, :aid, :feedback, :ftype, :speech_text, :audio, :video, :screen, :image, :document)";
						$cfeed_stmt = $db->prepare($cfeed_sql);
						$cfeed_stmt->execute($cfeedData);
						$ddq2iu++; $ddq2true++;
					endforeach;
				else:					
					#----DROP-TARGET----
					$ansSql = "INSERT INTO `sub_options_tbl` (`question_id`, `sub_option`, `image`) VALUES (:qid, :option, :image)";
					$stmt 	= $db->prepare($ansSql);
					$stmt->execute(array('qid' => $ques_id, 'option' => ( ! empty($ddq2_drop)) ? htmlspecialchars($ddq2_drop) : '', 'image' => ( ! empty($ddq2_item_img)) ? $ddq2_item_img : ''));
					
					#-----DRAG-ITEMS------
					$ddq2i = 0; $ddq2itrue = 1;
					foreach ($ddq2_drag as $answerData):
						$ansSql = "INSERT INTO `answer_tbl` (`question_id`, `choice_option`, `image`, `ans_val1`, `ans_val2`, `ans_val3`, `ans_val4`, `ans_val5`, `ans_val6`, `true_option`, `status`, `uid`, `cur_date`) VALUES (:qid, :choice, :image, :ans_val1, :ans_val2, :ans_val3, :ans_val4, :ans_val5, :ans_val6, :true, :status, :user_id, :cur_date)";
						$stmt 	= $db->prepare($ansSql);
						$stmt->execute(array('qid'		=> $ques_id,
											 'choice'	=> ( ! empty($ddq2_drag[$ddq2i])) ? htmlspecialchars($ddq2_drag[$ddq2i]) : '',
											 'image'	=> ( ! empty($ddq2_drag_item_img[$ddq2i])) ? $ddq2_drag_item_img[$ddq2i] : '',
											 'ans_val1'	=> ( ! empty($dd2_ans_val1[$ddq2i])) ? $dd2_ans_val1[$ddq2i] : '',
											 'ans_val2'	=> ( ! empty($dd2_ans_val2[$ddq2i])) ? $dd2_ans_val2[$ddq2i] : '',
											 'ans_val3'	=> ( ! empty($dd2_ans_val3[$ddq2i])) ? $dd2_ans_val3[$ddq2i] : '',
											 'ans_val4'	=> ( ! empty($dd2_ans_val4[$ddq2i])) ? $dd2_ans_val4[$ddq2i] : '',
											 'ans_val5'	=> ( ! empty($dd2_ans_val5[$ddq2i])) ? $dd2_ans_val5[$ddq2i] : '',
											 'ans_val6'	=> ( ! empty($dd2_ans_val6[$ddq2i])) ? $dd2_ans_val6[$ddq2i] : '',
											 'true'		=> ( ! empty($ddq_true_option) && $ddq_true_option == $ddq2itrue) ? 1 : '',
											 'status'	=> 1,
											 'user_id' 	=> $userId,
											 'cur_date'	=> $curdate));
						$ans_id = $db->lastInsertId();

						#-----FEEDBACK-DATA---
						$feed_audio = '';
						if ( ! empty($dd2_feedback_audio[$ddq2i])):
							$feed_audio = $dd2_feedback_audio[$ddq2i];
						elseif ( ! empty($dd2_feedback_rec_audio[$ddq2i])):
							$feed_audio = $dd2_feedback_rec_audio[$ddq2i];
						endif;
						
						$feed_video = '';
						if ( ! empty($dd2_feedback_video[$ddq2i])):
							$feed_video = $dd2_feedback_video[$ddq2i];
						elseif ( ! empty($dd2_feedback_rec_video[$ddq2i])):
							$feed_video = $dd2_feedback_rec_video[$ddq2i];
						endif;
						
						#--------Feedback----------
						$cfeedData = array('qid'			=> ( ! empty($ques_id)) ? $ques_id : '',
										   'aid'			=> ( ! empty($ans_id)) ? $ans_id : '',
										   'feedback'		=> ( ! empty($dd2_cfeedback[$ddq2i])) ? htmlspecialchars($dd2_cfeedback[$ddq2i]) : '',
										   'ftype'			=> 1,
										   'speech_text'	=> ( ! empty($dd2_feedback_text_to_speech[$ddq2i])) ? htmlspecialchars($dd2_feedback_text_to_speech[$ddq2i]) : '',
										   'audio'			=> ( ! empty($feed_audio)) ? $feed_audio : '',
										   'video'			=> ( ! empty($feed_video)) ? $feed_video : '',
										   'screen'			=> ( ! empty($dd2_feedback_rec_screen[$ddq2i])) ? $dd2_feedback_rec_screen[$ddq2i] : '',
										   'image'			=> ( ! empty($dd2_feedback_img[$ddq2i])) ? $dd2_feedback_img[$ddq2i] : '',
										   'document'		=> ( ! empty($dd2_feedback_doc[$ddq2i])) ? $dd2_feedback_doc[$ddq2i] : '');
						$cfeed_sql  = "INSERT INTO `feedback_tbl` (`question_id`, `answer_id`, `feedback`, `feedback_type`, `feed_speech_text`, `feed_audio`, `feed_video`, `feed_screen`, `feed_image`, `feed_document`) VALUES (:qid, :aid, :feedback, :ftype, :speech_text, :audio, :video, :screen, :image, :document)";
						$cfeed_stmt = $db->prepare($cfeed_sql);
						$cfeed_stmt->execute($cfeedData);
						$ddq2i++; $ddq2itrue++;
					endforeach;
				endif;
			endif;
		endif;
	endif;
	if ( ! empty($submit_type) && $submit_type == 1):
		$resdata = array('success' => TRUE, 'all_update' => FALSE, 'msg' => 'Simulation details updated successfully.', 'return_url' => 'linear-classic-template.php?add_data=true&sim_id='.md5($sim_id).'&ques_id='.base64_encode($ques_id));
	else:
		$resdata = array('success' => TRUE, 'all_update' => TRUE, 'msg' => 'All questions and answers updated successfully.', 'return_url' => 'author-simulation.php');
	endif;
	echo json_encode($resdata);
endif;

#-----------------------------------------Add-Single-Video-Sim----------------------------------
if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['add_single_video_sim']) && isset($_POST['sim_id'])):
	extract($_POST);
	if (empty($sim_duration) || empty($passing_marks)):
		$resdata = array('success' => FALSE, 'msg' => 'Please enter all required fields.');
	elseif ( ! empty($sim_duration) && ! empty($passing_marks)):
		$sql = "UPDATE scenario_master SET `Scenario_title` = '". $sim_title ."', `duration` = '". $sim_duration ."', `passing_marks` = '". $passing_marks ."'";
		#--Add-Web-Object--
		if ( ! empty($web_object)):
			$sql .= ", web_object = '". $web_object ."', web_object_title = '". htmlspecialchars($web_object_title) ."'";
		endif;
		#--Sim-Media-File--
		if ( ! empty($scenario_media_file) && in_array(strtolower(pathinfo($scenario_media_file, PATHINFO_EXTENSION)), $img_valid_exts)):
			$sql .= ", image_url = '". $scenario_media_file ."', video_url = NULL, audio_url = NULL";
		elseif ( ! empty($scenario_media_file) && in_array(strtolower(pathinfo($scenario_media_file, PATHINFO_EXTENSION)), $video_valid_exts)):
			$sql .= ", video_url = '". $scenario_media_file ."', image_url = NULL, audio_url = NULL";
		elseif ( ! empty($scenario_media_file) && in_array(strtolower(pathinfo($scenario_media_file, PATHINFO_EXTENSION)), $audio_valid_exts)):
			$sql .= ", audio_url = '". $scenario_media_file ."', video_url = NULL, image_url = NULL";
		elseif ( ! empty($scenario_media_file) && ! in_array(strtolower(pathinfo($scenario_media_file, PATHINFO_EXTENSION)), $audio_valid_exts) && 
				! in_array(strtolower(pathinfo($scenario_media_file, PATHINFO_EXTENSION)), $img_valid_exts) && 
				! in_array(strtolower(pathinfo($scenario_media_file, PATHINFO_EXTENSION)), $video_valid_exts)):
			$sql .= ", web_object_file = '". $scenario_media_file ."', video_url = NULL, image_url = NULL, audio_url = NULL";
		endif;
		$sql .= " WHERE scenario_id = '". $sim_id ."'";
		$results = $db->prepare($sql);
		if ($results->execute()):
			#----Competency----
			$delCom  = "DELETE FROM competency_tbl WHERE scenario_id = '". $sim_id ."'";
			$delComq = $db->prepare($delCom); $delComq->execute();
			$data	 = array('scenario_id'	=> $sim_id,
							 'comp_col_1'	=> (isset($competency_name_1) && ! empty($competency_name_1)) ? addslashes($competency_name_1) : '',
							 'comp_val_1'	=> (isset($competency_score_1) && ! empty($competency_score_1)) ? $competency_score_1 : '',
							 'comp_col_2'	=> (isset($competency_name_2) && ! empty($competency_name_2)) ? addslashes($competency_name_2) : '',
							 'comp_val_2'	=> (isset($competency_score_2) && ! empty($competency_score_2)) ? $competency_score_2 : '',
							 'comp_col_3'	=> (isset($competency_name_3) && ! empty($competency_name_3)) ? addslashes($competency_name_3) : '',
							 'comp_val_3'	=> (isset($competency_score_3) && ! empty($competency_score_3)) ? $competency_score_3 : '',
							 'comp_col_4'	=> (isset($competency_name_4) && ! empty($competency_name_4)) ? addslashes($competency_name_4) : '',
							 'comp_val_4'	=> (isset($competency_score_4) && ! empty($competency_score_4)) ? $competency_score_4 : '',
							 'comp_col_5'	=> (isset($competency_name_5) && ! empty($competency_name_5)) ? addslashes($competency_name_5) : '',
							 'comp_val_5'	=> (isset($competency_score_5) && ! empty($competency_score_5)) ? $competency_score_5 : '',
							 'comp_col_6'	=> (isset($competency_name_6) && ! empty($competency_name_6)) ? addslashes($competency_name_6) : '',
							 'comp_val_6'	=> (isset($competency_score_6) && ! empty($competency_score_6)) ? $competency_score_6 : '',
							 'weightage_1'	=> (isset($weightage_1) && ! empty($weightage_1)) ? $weightage_1 : '',
							 'weightage_2'	=> (isset($weightage_2) && ! empty($weightage_2)) ? $weightage_2 : '',
							 'weightage_3'	=> (isset($weightage_3) && ! empty($weightage_3)) ? $weightage_3 : '',
							 'weightage_4'	=> (isset($weightage_4) && ! empty($weightage_4)) ? $weightage_4 : '',
							 'weightage_5'	=> (isset($weightage_5) && ! empty($weightage_5)) ? $weightage_5 : '',
							 'weightage_6'	=> (isset($weightage_6) && ! empty($weightage_6)) ? $weightage_6 : '',
							 'status' 		=> 1,
							 'user_id' 		=> $userId,
							 'cur_date'		=> $curdate);
			$com_sql  = "INSERT INTO `competency_tbl` (`scenario_id`, `comp_col_1`, `comp_val_1`, `comp_col_2`, `comp_val_2`, `comp_col_3`, `comp_val_3`, `comp_col_4`, `comp_val_4`, `comp_col_5`, `comp_val_5`, `comp_col_6`, `comp_val_6`, `weightage_1`, `weightage_2`, `weightage_3`, `weightage_4`, `weightage_5`, `weightage_6`, `status`, `uid`, `cur_date`) VALUES (:scenario_id, :comp_col_1, :comp_val_1, :comp_col_2, :comp_val_2, :comp_col_3, :comp_val_3, :comp_col_4, :comp_val_4, :comp_col_5, :comp_val_5, :comp_col_6, :comp_val_6, :weightage_1, :weightage_2, :weightage_3, :weightage_4, :weightage_5, :weightage_6, :status, :user_id, :cur_date)";
			$com_stmt = $db->prepare($com_sql); $com_stmt->execute($data);

			#----PAGES----
			$delPage  = "DELETE FROM sim_pages WHERE scenario_id = '". $sim_id ."'";
			$delPageq = $db->prepare($delPage); $delPageq->execute();
			if ( ! empty($sim_page_name)): $sp = 0;
				foreach ($sim_page_name as $pdata):
					$pageq = "INSERT INTO `sim_pages` (`scenario_id`, `sim_page_name`, `sim_page_desc`, `userid`, `status`, `added_date`) 
					VALUES (:sid, :pname, :pdesc, :user_id, :status, :adate)";
					$page_stmt = $db->prepare($pageq);
					$page_stmt->execute(array('sid'		=> $sim_id,
											  'pname'	=> ( ! empty($pdata)) ? htmlspecialchars($pdata) : '',
											  'pdesc'	=> ( ! empty($sim_page_desc[$sp])) ? $sim_page_desc[$sp] : '',
											  'status' 	=> 1,
											  'user_id'	=> $userId,
											  'adate'	=> $curdate));
			$sp++; endforeach; endif;

			#----Branding----
			$bandq	    = "DELETE FROM sim_branding_tbl WHERE scenario_id = '". $sim_id ."'";
			$bandqexe   = $db->prepare($bandq); $bandqexe->execute();
			$brand_data = array('sid'				=> $sim_id,
								'bg'				=> ( ! empty($ques_bg_color)) ? $ques_bg_color : '',
								'bg_tr'				=> ( ! empty($ques_bg_transp)) ? $ques_bg_transp : '',
								'obg'				=> ( ! empty($ques_option_bg_color)) ? $ques_option_bg_color : '',
								'obg_tr'			=> ( ! empty($ques_option_bg_transp)) ? $ques_option_bg_transp : '',
								'ohover'			=> ( ! empty($ques_option_hover_color)) ? $ques_option_hover_color : '',
								'oh_tr'				=> ( ! empty($ques_option_hover_transp)) ? $ques_option_hover_transp : '',
								'oselected'			=> ( ! empty($ques_option_select_color)) ? $ques_option_select_color : '',
								'oselected_tr'		=> ( ! empty($ques_option_select_transp)) ? $ques_option_select_transp : '',
								'btn_bg'			=> ( ! empty($btn_bg_color)) ? $btn_bg_color : '',
								'btn_bg_tr'			=> ( ! empty($btn_bg_transp)) ? $btn_bg_transp : '',
								'btn_hover'			=> ( ! empty($btn_hover_color)) ? $btn_hover_color : '',
								'btn_hover_tr'		=> ( ! empty($btn_hover_transp)) ? $btn_hover_transp : '',
								'btn_selected'		=> ( ! empty($btn_select_color)) ? $btn_select_color : '',
								'btn_selected_tr'	=> ( ! empty($btn_select_transp)) ? $btn_select_transp : '',
								'font_type'			=> ( ! empty($font_type)) ? $font_type : '',
								'font_color'		=> ( ! empty($font_color)) ? $font_color : '',
								'font_size'			=> ( ! empty($font_size)) ? $font_size : '',
								'user_id' 			=> $userId,
								'added_date'		=> $curdate);
			$com_sql = "INSERT INTO `sim_branding_tbl` (`scenario_id`, `ques_bg`, `ques_bg_transparency`, `option_bg`, `option_bg_transparency`, `option_hover`, `option_hover_transparency`, `option_selected`, `option_selected_transparency`, `btn_bg`, `btn_bg_transparency`, `btn_hover`, `btn_hover_transparency`, `btn_selected`, `btn_selected_transparency`, `font_type`, `font_color`, `font_size`, `user_id`, `added_date`) 
						VALUES (:sid, :bg, :bg_tr, :obg, :obg_tr, :ohover, :oh_tr, :oselected, :oselected_tr, :btn_bg, :btn_bg_tr, :btn_hover, :btn_hover_tr, :btn_selected, :btn_selected_tr, :font_type, :font_color, :font_size, :user_id, :added_date)";
			$com_stmt = $db->prepare($com_sql); $com_stmt->execute($brand_data);
		endif;
		
		//--------MCQ-Video-Question--------
		if ($question_type == 4):
			if ( ! empty($videoq)):
				$getVdata		= array_merge($videoq);
				$countQues 		= count($getVdata);
				$vi 			= 0;
				$audio 			= '';
				$video 			= '';
				$feed_audio 	= '';
				$feed_video 	= '';
				foreach ($getVdata as $vdata):
					$qaudio = '';
					if ( ! empty($videoq_audio)):
						$qaudio = $videoq_audio[$vi];
					elseif ( ! empty($videoq_rec_audio)):
						$qaudio	= $videoq_rec_audio;
					endif;
					
					$audio = '';
					if ( ! empty($video_audio)):
						$audio = $video_audio[$vi];
					elseif ( ! empty($video_rec_audio)):
						$audio = $video_rec_audio;
					endif;
					
					$video = '';
					if ( ! empty($video_video)):
						$video = $video_video[$vi];
					elseif ( ! empty($video_rec_video)):
						$video = $video_rec_video;
					endif;

					$quesData = array('sim_id'			=> $sim_id,
									  'qType'			=> $question_type,
									  'questions'		=> ( ! empty($videoq[$vi])) ? htmlspecialchars($videoq[$vi]) : '',
									  'qaudio'			=> ( ! empty($qaudio)) ? $qaudio : '',
									  'qspeech_text'	=> ( ! empty($videoqq_text_to_speech[$vi])) ? htmlspecialchars($videoqq_text_to_speech[$vi]) : '',
									  'media_file'		=> ( ! empty($videoq_media_file)) ? $videoq_media_file : '',
									  'cue_point'		=> ( ! empty($cue_point[$vi])) ? $cue_point[$vi] : '',
									  'speech_text'		=> ( ! empty($videoq_text_to_speech[$vi])) ? htmlspecialchars($videoq_text_to_speech[$vi]) : '',
									  'audio'			=> ( ! empty($audio)) ? $audio : '',
									  'video'			=> ( ! empty($video)) ? $video : '',
									  'screen'			=> ( ! empty($videoq_rec_screen[$vi])) ? $videoq_rec_screen[$vi] : '',
									  'image'			=> ( ! empty($videoq_img[$vi])) ? $videoq_img[$vi] : '',
									  'document'		=> ( ! empty($videoq_doc[$vi])) ? $videoq_doc[$vi] : '',
									  'shuffle'			=> (isset($video_shuffle[$vi]) && ! empty($video_shuffle[$vi])) ? 1 : 0,
									  'ques_val_1'		=> ( ! empty($videoq_ques_val_1[$vi])) ? $videoq_ques_val_1[$vi] : '',
									  'ques_val_2'		=> ( ! empty($videoq_ques_val_2[$vi])) ? $videoq_ques_val_2[$vi] : '',
									  'ques_val_3'		=> ( ! empty($videoq_ques_val_3[$vi])) ? $videoq_ques_val_3[$vi] : '',
									  'ques_val_4'		=> ( ! empty($videoq_ques_val_4[$vi])) ? $videoq_ques_val_4[$vi] : '',
									  'ques_val_5'		=> ( ! empty($videoq_ques_val_5[$vi])) ? $videoq_ques_val_5[$vi] : '',
									  'ques_val_6'		=> ( ! empty($videoq_ques_val_6[$vi])) ? $videoq_ques_val_6[$vi] : '',
									  'critical'		=> ( ! empty($video_criticalQ[$vi])) ? $video_criticalQ[$vi] : '',
									  'status' 			=> 1,
									  'user_id'			=> $userId,
									  'cur_date'		=> $curdate);
					//Insert
					$ques_sql = "INSERT INTO `question_tbl` (`scenario_id`, `questions`, `question_type`, `qaudio`, `qspeech_text`, `shuffle`,`videoq_media_file`, `videoq_cue_point`, `speech_text`, `audio`, `video`, `screen`, `image`, `document`, `ques_val_1`, `ques_val_2`, `ques_val_3`, `ques_val_4`, `ques_val_5`,  `ques_val_6`, `critical`, `status`, `uid`, `cur_date`) 
								VALUES (:sim_id, :questions, :qType, :qaudio, :qspeech_text, :shuffle, :media_file, :cue_point, :speech_text, :audio, :video, :screen, :image, :document, :ques_val_1, :ques_val_2, :ques_val_3, :ques_val_4, :ques_val_5, :ques_val_6, :critical, :status, :user_id, :cur_date)";
					$ques_stmt = $db->prepare($ques_sql);
					if ($ques_stmt->execute($quesData)):
						$ques_id = $db->lastInsertId();
						//CHOICE
						$voption = 0; $voptiontrue = 1;
						foreach ($videoq_option[$vi]['option'] as $answerData):
							if ( ! empty($updateAnswerid[$vi][$voption])):
								$answer_id = $updateAnswerid[$vi][$voption];
								$ansSql = "UPDATE `answer_tbl` SET `choice_option`=:option, `ans_val1`=:val1, `ans_val2`=:val2, `ans_val3`=:val3, `ans_val4`=:val4, `ans_val5`=:val5, `ans_val6`=:val6, `true_option`=:true, `status`=:status, `uid`=:user_id, `cur_date`=:cur_date WHERE answer_id=:aid";
								$stmt	= $db->prepare($ansSql);
								$stmt->execute(array(
									'aid' 			=> $updateAnswerid[$vi][$voption],
									'option'		=> ( ! empty($answerData)) ? $answerData : '',
									'val1'			=> ( ! empty($videoq_ans_val1[$vi]['score'][$voption])) ? $videoq_ans_val1[$vi]['score'][$voption] : '',
									'val2'			=> ( ! empty($videoq_ans_val2[$vi]['score'][$voption])) ? $videoq_ans_val2[$vi]['score'][$voption] : '',
									'val3'			=> ( ! empty($videoq_ans_val3[$vi]['score'][$voption])) ? $videoq_ans_val3[$vi]['score'][$voption] : '',
									'val4'			=> ( ! empty($videoq_ans_val4[$vi]['score'][$voption])) ? $videoq_ans_val4[$vi]['score'][$voption] : '',
									'val5'			=> ( ! empty($videoq_ans_val5[$vi]['score'][$voption])) ? $videoq_ans_val5[$vi]['score'][$voption] : '',
									'val6'			=> ( ! empty($videoq_ans_val6[$vi]['score'][$voption])) ? $videoq_ans_val6[$vi]['score'][$voption] : '',
									'true'			=> ( isset($videoq_true_option[$vi][0]) && $videoq_true_option[$vi][0] == $voptiontrue) ? 1 : '',
									'status'		=> 1,
									'user_id' 		=> $userId,
									'cur_date'		=> $curdate));
							else:
								$ansSql = "INSERT INTO `answer_tbl` (`question_id`, `choice_option`, `ans_val1`, `ans_val2`, `ans_val3`, `ans_val4`, `ans_val5`, `ans_val6`, `true_option`, `status`, `uid`, `cur_date`) 
										   VALUES (:qid, :option, :val1, :val2, :val3, :val4, :val5, :val6, :true, :status, :user_id, :cur_date)";
								$stmt	= $db->prepare($ansSql);
								$stmt->execute(array(
										'qid' 				=> $ques_id,
										'option'			=> ( ! empty($answerData)) ? addslashes($answerData) : '',
										'val1'				=> ( ! empty($videoq_ans_val1[$vi]['score'][$voption])) ? $videoq_ans_val1[$vi]['score'][$voption] : '',
										'val2'				=> ( ! empty($videoq_ans_val2[$vi]['score'][$voption])) ? $videoq_ans_val2[$vi]['score'][$voption] : '',
										'val3'				=> ( ! empty($videoq_ans_val3[$vi]['score'][$voption])) ? $videoq_ans_val3[$vi]['score'][$voption] : '',
										'val4'				=> ( ! empty($videoq_ans_val4[$vi]['score'][$voption])) ? $videoq_ans_val4[$vi]['score'][$voption] : '',
										'val5'				=> ( ! empty($videoq_ans_val5[$vi]['score'][$voption])) ? $videoq_ans_val5[$vi]['score'][$voption] : '',
										'val6'				=> ( ! empty($videoq_ans_val6[$vi]['score'][$voption])) ? $videoq_ans_val6[$vi]['score'][$voption] : '',
										'true'				=> ( isset($videoq_true_option[$vi][0]) && $videoq_true_option[$vi][0] == $voptiontrue) ? 1 : '',
										'status'			=> 1,
										'user_id' 			=> $userId,
										'cur_date'			=> $curdate));
								//fetch last inserted id
								$answer_id = $db->lastInsertId();
							endif;
							
							#-----FEEDBACK-DATA---
							if ( ! empty($videoqf_feedback_audio[$vi][$voption])):
								$feed_audio	= $videoqf_feedback_audio[$vi][$voption];
							elseif ( ! empty($videoqf_feedback_rec_audio[$vi][$voption])):
								$feed_audio = $videoqf_feedback_rec_audio[$vi][$voption];
							endif;
							
							if ( ! empty($videoqf_feedback_video[$vi][$voption])):
								$feed_video	= $videoqf_feedback_video[$vi][$voption];
							elseif ( ! empty($videoqf_feedback_rec_video[$vi][$voption])):
								$feed_video= $videoqf_feedback_rec_video[$vi][$voption];
							endif;
							
							#--------Feedback----------
							$cfeedData = array('aid'			=> $answer_id,
											   'qid'			=> $ques_id,
											   'feedback'		=> ( ! empty($videoq_feedback[$vi][$voption])) ? addslashes($videoq_feedback[$vi][$voption]) : '',
											   'ftype'			=> 1,
											   'speech_text'	=> ( ! empty($videoqf_feedback_text_to_speech[$vi][$voption])) ? addslashes($videoqf_feedback_text_to_speech[$vi][$voption]) : '',
											   'feed_audio'		=> ( ! empty($feed_audio)) ? $feed_audio : '',
											   'feed_video'		=> ( ! empty($feed_video)) ? $feed_video : '',
											   'feed_screen'	=> ( ! empty($videoqf_feedback_rec_screen[$vi][$voption])) ? $videoqf_feedback_rec_screen[$vi][$voption] : '',
											   'feed_image'		=> ( ! empty($videoqf_feedback_img[$vi][$voption])) ? $videoqf_feedback_img[$vi][$voption] : '',
											   'feed_document'	=> ( ! empty($videoqf_feedback_doc[$vi][$voption])) ? $videoqf_feedback_doc[$vi][$voption] : '');
							$cfeed_sql  = "INSERT INTO `feedback_tbl` (`answer_id`, `question_id`, `feedback`, `feedback_type`, `feed_speech_text`, `feed_audio`, `feed_video`, `feed_screen`, `feed_image`, `feed_document`) 
							VALUES (:aid, :qid, :feedback, :ftype, :speech_text, :feed_audio, :feed_video, :feed_screen, :feed_image, :feed_document)";
							$cfeed_stmt = $db->prepare($cfeed_sql); $cfeed_stmt->execute($cfeedData);							
						$voption++; $voptiontrue++;
						endforeach;
						if ( ! empty($submit_type) && $submit_type == 1):
							$resdata = array('success' => TRUE, 'all_update' => FALSE, 'msg' => 'Simulation details updated successfully.', 'return_url' => 'update-single-video-template.php?add_data=true&sim_id='.md5($sim_id).'&ques_id='.base64_encode($ques_id));
						else:
							$resdata = array('success' => TRUE, 'all_update' => TRUE, 'msg' => 'All questions and answers updated successfully.', 'return_url' => 'author-simulation.php');
						endif;
					else:
						$resdata = array('success' => FALSE, 'msg' => 'Data not update. try again later.', 'return_url' => 'update-single-video-template.php?add_data=true&sim_id='.md5($sim_id));
					endif;
				$vi++;
				endforeach;
			else:
				if ( ! empty($submit_type) && $submit_type == 1):
					$resdata = array('success' => TRUE, 'all_update' => FALSE, 'msg' => 'Simulation details updated successfully.', 'return_url' => 'single-video-template.php?add_data=true&sim_id='.md5($sim_id));
				else:
					$resdata = array('success' => TRUE, 'all_update' => TRUE, 'msg' => 'All questions and answers updated successfully.', 'return_url' => 'author-simulation.php');
				endif;
			endif;
		endif;
	endif;
	echo json_encode($resdata);
endif;

#-----------------------------------------Update-Single-Video-Sim----------------------------------
if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['update_single_video_sim']) && isset($_POST['sim_id'])):
	extract($_POST);
	if (empty($sim_duration) || empty($passing_marks)):
		$resdata = array('success' => FALSE, 'msg' => 'Please enter all required fields.');
	elseif ( ! empty($sim_duration) && ! empty($passing_marks)):
		$sql = "UPDATE scenario_master SET `Scenario_title` = '". $sim_title ."', `duration` = '". $sim_duration ."', `passing_marks` = '". $passing_marks ."'";
		#--Add-Web-Object--
		if ( ! empty($web_object)):
			$sql .= ", web_object = '". $web_object ."', web_object_title = '". htmlspecialchars($web_object_title) ."'";
		endif;
		#--Sim-Media-File--
		if ( ! empty($scenario_media_file) && in_array(strtolower(pathinfo($scenario_media_file, PATHINFO_EXTENSION)), $img_valid_exts)):
			$sql .= ", image_url = '". $scenario_media_file ."', video_url = NULL, audio_url = NULL";
		elseif ( ! empty($scenario_media_file) && in_array(strtolower(pathinfo($scenario_media_file, PATHINFO_EXTENSION)), $video_valid_exts)):
			$sql .= ", video_url = '". $scenario_media_file ."', image_url = NULL, audio_url = NULL";
		elseif ( ! empty($scenario_media_file) && in_array(strtolower(pathinfo($scenario_media_file, PATHINFO_EXTENSION)), $audio_valid_exts)):
			$sql .= ", audio_url = '". $scenario_media_file ."', video_url = NULL, image_url = NULL";
		elseif ( ! empty($scenario_media_file) && ! in_array(strtolower(pathinfo($scenario_media_file, PATHINFO_EXTENSION)), $audio_valid_exts) && 
				! in_array(strtolower(pathinfo($scenario_media_file, PATHINFO_EXTENSION)), $img_valid_exts) && 
				! in_array(strtolower(pathinfo($scenario_media_file, PATHINFO_EXTENSION)), $video_valid_exts)):
			$sql .= ", web_object_file = '". $scenario_media_file ."', video_url = NULL, image_url = NULL, audio_url = NULL";
		endif;
		$sql .= " WHERE scenario_id = '". $sim_id ."'";
		$results = $db->prepare($sql);
		if ($results->execute()):
			#----Competency----
			$delCom  = "DELETE FROM competency_tbl WHERE scenario_id = '". $sim_id ."'";
			$delComq = $db->prepare($delCom); $delComq->execute();
			$data	 = array('scenario_id'	=> $sim_id,
							 'comp_col_1'	=> (isset($competency_name_1) && ! empty($competency_name_1)) ? addslashes($competency_name_1) : '',
							 'comp_val_1'	=> (isset($competency_score_1) && ! empty($competency_score_1)) ? $competency_score_1 : '',
							 'comp_col_2'	=> (isset($competency_name_2) && ! empty($competency_name_2)) ? addslashes($competency_name_2) : '',
							 'comp_val_2'	=> (isset($competency_score_2) && ! empty($competency_score_2)) ? $competency_score_2 : '',
							 'comp_col_3'	=> (isset($competency_name_3) && ! empty($competency_name_3)) ? addslashes($competency_name_3) : '',
							 'comp_val_3'	=> (isset($competency_score_3) && ! empty($competency_score_3)) ? $competency_score_3 : '',
							 'comp_col_4'	=> (isset($competency_name_4) && ! empty($competency_name_4)) ? addslashes($competency_name_4) : '',
							 'comp_val_4'	=> (isset($competency_score_4) && ! empty($competency_score_4)) ? $competency_score_4 : '',
							 'comp_col_5'	=> (isset($competency_name_5) && ! empty($competency_name_5)) ? addslashes($competency_name_5) : '',
							 'comp_val_5'	=> (isset($competency_score_5) && ! empty($competency_score_5)) ? $competency_score_5 : '',
							 'comp_col_6'	=> (isset($competency_name_6) && ! empty($competency_name_6)) ? addslashes($competency_name_6) : '',
							 'comp_val_6'	=> (isset($competency_score_6) && ! empty($competency_score_6)) ? $competency_score_6 : '',
							 'weightage_1'	=> (isset($weightage_1) && ! empty($weightage_1)) ? $weightage_1 : '',
							 'weightage_2'	=> (isset($weightage_2) && ! empty($weightage_2)) ? $weightage_2 : '',
							 'weightage_3'	=> (isset($weightage_3) && ! empty($weightage_3)) ? $weightage_3 : '',
							 'weightage_4'	=> (isset($weightage_4) && ! empty($weightage_4)) ? $weightage_4 : '',
							 'weightage_5'	=> (isset($weightage_5) && ! empty($weightage_5)) ? $weightage_5 : '',
							 'weightage_6'	=> (isset($weightage_6) && ! empty($weightage_6)) ? $weightage_6 : '',
							 'status' 		=> 1,
							 'user_id' 		=> $userId,
							 'cur_date'		=> $curdate);
			$com_sql  = "INSERT INTO `competency_tbl` (`scenario_id`, `comp_col_1`, `comp_val_1`, `comp_col_2`, `comp_val_2`, `comp_col_3`, `comp_val_3`, `comp_col_4`, `comp_val_4`, `comp_col_5`, `comp_val_5`, `comp_col_6`, `comp_val_6`, `weightage_1`, `weightage_2`, `weightage_3`, `weightage_4`, `weightage_5`, `weightage_6`, `status`, `uid`, `cur_date`) VALUES (:scenario_id, :comp_col_1, :comp_val_1, :comp_col_2, :comp_val_2, :comp_col_3, :comp_val_3, :comp_col_4, :comp_val_4, :comp_col_5, :comp_val_5, :comp_col_6, :comp_val_6, :weightage_1, :weightage_2, :weightage_3, :weightage_4, :weightage_5, :weightage_6, :status, :user_id, :cur_date)";
			$com_stmt = $db->prepare($com_sql); $com_stmt->execute($data);

			#----PAGES----
			$delPage  = "DELETE FROM sim_pages WHERE scenario_id = '". $sim_id ."'";
			$delPageq = $db->prepare($delPage); $delPageq->execute();
			if ( ! empty($sim_page_name)):
				$sp = 0;
				foreach ($sim_page_name as $pdata):
					$pageq = "INSERT INTO `sim_pages` (`scenario_id`, `sim_page_name`, `sim_page_desc`, `userid`, `status`, `added_date`) 
					VALUES (:sid, :pname, :pdesc, :user_id, :status, :adate)";
					$page_stmt = $db->prepare($pageq);
					$page_stmt->execute(array('sid'		=> $sim_id,
											  'pname'	=> ( ! empty($pdata)) ? $pdata : '',
											  'pdesc'	=> ( ! empty($sim_page_desc[$sp])) ? $sim_page_desc[$sp] : '',
											  'status' 	=> 1,
											  'user_id'	=> $userId,
											  'adate'	=> $curdate));
			$sp++; endforeach; endif;

			#----Branding----
			$bandq	    = "DELETE FROM sim_branding_tbl WHERE scenario_id = '". $sim_id ."'";
			$bandqexe   = $db->prepare($bandq); $bandqexe->execute();
			$brand_data = array('sid'				=> $sim_id,
								'bg'				=> ( ! empty($ques_bg_color)) ? $ques_bg_color : '',
								'bg_tr'				=> ( ! empty($ques_bg_transp)) ? $ques_bg_transp : '',
								'obg'				=> ( ! empty($ques_option_bg_color)) ? $ques_option_bg_color : '',
								'obg_tr'			=> ( ! empty($ques_option_bg_transp)) ? $ques_option_bg_transp : '',
								'ohover'			=> ( ! empty($ques_option_hover_color)) ? $ques_option_hover_color : '',
								'oh_tr'				=> ( ! empty($ques_option_hover_transp)) ? $ques_option_hover_transp : '',
								'oselected'			=> ( ! empty($ques_option_select_color)) ? $ques_option_select_color : '',
								'oselected_tr'		=> ( ! empty($ques_option_select_transp)) ? $ques_option_select_transp : '',
								'btn_bg'			=> ( ! empty($btn_bg_color)) ? $btn_bg_color : '',
								'btn_bg_tr'			=> ( ! empty($btn_bg_transp)) ? $btn_bg_transp : '',
								'btn_hover'			=> ( ! empty($btn_hover_color)) ? $btn_hover_color : '',
								'btn_hover_tr'		=> ( ! empty($btn_hover_transp)) ? $btn_hover_transp : '',
								'btn_selected'		=> ( ! empty($btn_select_color)) ? $btn_select_color : '',
								'btn_selected_tr'	=> ( ! empty($btn_select_transp)) ? $btn_select_transp : '',
								'font_type'			=> ( ! empty($font_type)) ? $font_type : '',
								'font_color'		=> ( ! empty($font_color)) ? $font_color : '',
								'font_size'			=> ( ! empty($font_size)) ? $font_size : '',
								'user_id' 			=> $userId,
								'added_date'		=> $curdate);
			$com_sql = "INSERT INTO `sim_branding_tbl` (`scenario_id`, `ques_bg`, `ques_bg_transparency`, `option_bg`, `option_bg_transparency`, `option_hover`, `option_hover_transparency`, `option_selected`, `option_selected_transparency`, `btn_bg`, `btn_bg_transparency`, `btn_hover`, `btn_hover_transparency`, `btn_selected`, `btn_selected_transparency`, `font_type`, `font_color`, `font_size`, `user_id`, `added_date`) 
			VALUES (:sid, :bg, :bg_tr, :obg, :obg_tr, :ohover, :oh_tr, :oselected, :oselected_tr, :btn_bg, :btn_bg_tr, :btn_hover, :btn_hover_tr, :btn_selected, :btn_selected_tr, :font_type, :font_color, :font_size, :user_id, :added_date)";
			$com_stmt = $db->prepare($com_sql); $com_stmt->execute($brand_data);
		endif;
		
		//--------MCQ-Video-Question--------
		if ($question_type == 4):
			if ( ! empty($videoq)):
				$getVdata		= array_merge($videoq);
				$countQues 		= count($getVdata);
				$vi 			= 0;
				$audio 			= '';
				$video 			= '';
				$feed_audio 	= '';
				$feed_video 	= '';
				foreach ($getVdata as $vdata):
					$qaudio = '';
					if ( ! empty($videoq_audio)):
						$qaudio = $videoq_audio[$vi];
					elseif ( ! empty($videoq_rec_audio)):
						$qaudio	= $videoq_rec_audio;
					endif;
					
					$audio = '';
					if ( ! empty($video_audio)):
						$audio = $video_audio[$vi];
					elseif ( ! empty($video_rec_audio)):
						$audio = $video_rec_audio;
					endif;
					
					$video = '';
					if ( ! empty($video_video)):
						$video = $video_video[$vi];
					elseif ( ! empty($video_rec_video)):
						$video = $video_rec_video;
					endif;
					
					if ( ! empty($updateVQuestionId[$vi])):
						$quesData = array('qid'				=> $updateVQuestionId[$vi],
										  'questions'		=> ( ! empty($videoq[$vi])) ? htmlspecialchars($videoq[$vi]) : '',
										  'qaudio'			=> ( ! empty($qaudio)) ? $qaudio : '',
										  'qspeech_text'	=> ( ! empty($videoqq_text_to_speech[$vi])) ? htmlspecialchars($videoqq_text_to_speech[$vi]) : '',
										  'media_file'		=> ( ! empty($videoq_media_file)) ? $videoq_media_file : '',
										  'cue_point'		=> ( ! empty($cue_point[$vi])) ? $cue_point[$vi] : '',
										  'speech_text'		=> ( ! empty($videoq_text_to_speech[$vi])) ? htmlspecialchars($videoq_text_to_speech[$vi]) : '',
										  'audio'			=> ( ! empty($audio)) ? $audio : '',
										  'video'			=> ( ! empty($video)) ? $video : '',
										  'screen'			=> ( ! empty($videoq_rec_screen[$vi])) ? $videoq_rec_screen[$vi] : '',
										  'image'			=> ( ! empty($videoq_img[$vi])) ? $videoq_img[$vi] : '',
										  'document'		=> ( ! empty($videoq_doc[$vi])) ? $videoq_doc[$vi] : '',
										  'shuffle'			=> (isset($video_shuffle[$vi]) && ! empty($video_shuffle[$vi])) ? 1 : 0,
										  'ques_val_1'		=> ( ! empty($videoq_ques_val_1[$vi])) ? $videoq_ques_val_1[$vi] : '',
										  'ques_val_2'		=> ( ! empty($videoq_ques_val_2[$vi])) ? $videoq_ques_val_2[$vi] : '',
										  'ques_val_3'		=> ( ! empty($videoq_ques_val_3[$vi])) ? $videoq_ques_val_3[$vi] : '',
										  'ques_val_4'		=> ( ! empty($videoq_ques_val_4[$vi])) ? $videoq_ques_val_4[$vi] : '',
										  'ques_val_5'		=> ( ! empty($videoq_ques_val_5[$vi])) ? $videoq_ques_val_5[$vi] : '',
										  'ques_val_6'		=> ( ! empty($videoq_ques_val_6[$vi])) ? $videoq_ques_val_6[$vi] : '',
										  'critical'		=> ( ! empty($video_criticalQ[$vi])) ? $video_criticalQ[$vi] : '',
										  'status' 			=> 1,
										  'user_id'			=> $userId,
										  'cur_date'		=> $curdate);
						$ques_sql  = "UPDATE `question_tbl` SET `questions`=:questions, `qaudio`=:qaudio, `qspeech_text`=:qspeech_text, `videoq_media_file`=:media_file, `videoq_cue_point`=:cue_point, `speech_text`=:speech_text, `audio`=:audio, `video`=:video, `screen`=:screen, `image`=:image, `document`=:document, `shuffle`=:shuffle, `ques_val_1`=:ques_val_1, `ques_val_2`=:ques_val_2, `ques_val_3`=:ques_val_3, `ques_val_4`=:ques_val_4, `ques_val_5`=:ques_val_5, `ques_val_6`=:ques_val_6, `critical`=:critical, `status`=:status, `uid`=:user_id, `cur_date`=:cur_date WHERE question_id=:qid";
						$ques_stmt = $db->prepare($ques_sql); $ques_stmt->execute($quesData);
						$ques_id   = $updateVQuestionId[$vi];
						
						#-----Remove-Old-Feedback------
						$db->feedback_remove($ques_id);
						
						if ( ! empty($videoq_option[$vi]['option'])):
							//CHOICE
							$voption = 0; $voptiontrue = 1;
							foreach ($videoq_option[$vi]['option'] as $answerData):
								if ( ! empty($updateAnswerid[$vi][$voption])):
									$answer_id = $updateAnswerid[$vi][$voption];
									$ansSql = "UPDATE `answer_tbl` SET `choice_option`=:option, `ans_val1`=:val1, `ans_val2`=:val2, `ans_val3`=:val3, `ans_val4`=:val4, `ans_val5`=:val5, `ans_val6`=:val6, `true_option`=:true, `status`=:status, `uid`=:user_id, `cur_date`=:cur_date WHERE answer_id=:aid";
									$stmt	= $db->prepare($ansSql);
									$stmt->execute(array(
										'aid' 			=> $updateAnswerid[$vi][$voption],
										'option'		=> ( ! empty($answerData)) ? htmlspecialchars($answerData) : '',
										'val1'			=> ( ! empty($videoq_ans_val1[$vi]['score'][$voption])) ? $videoq_ans_val1[$vi]['score'][$voption] : '',
										'val2'			=> ( ! empty($videoq_ans_val2[$vi]['score'][$voption])) ? $videoq_ans_val2[$vi]['score'][$voption] : '',
										'val3'			=> ( ! empty($videoq_ans_val3[$vi]['score'][$voption])) ? $videoq_ans_val3[$vi]['score'][$voption] : '',
										'val4'			=> ( ! empty($videoq_ans_val4[$vi]['score'][$voption])) ? $videoq_ans_val4[$vi]['score'][$voption] : '',
										'val5'			=> ( ! empty($videoq_ans_val5[$vi]['score'][$voption])) ? $videoq_ans_val5[$vi]['score'][$voption] : '',
										'val6'			=> ( ! empty($videoq_ans_val6[$vi]['score'][$voption])) ? $videoq_ans_val6[$vi]['score'][$voption] : '',
										'true'			=> ( isset($videoq_true_option[$vi][0]) && $videoq_true_option[$vi][0] == $voptiontrue) ? 1 : '',
										'status'		=> 1,
										'user_id' 		=> $userId,
										'cur_date'		=> $curdate));
								else:
									$ansSql = "INSERT INTO `answer_tbl` (`question_id`, `choice_option`, `ans_val1`, `ans_val2`, `ans_val3`, `ans_val4`, `ans_val5`, `ans_val6`, `true_option`, `status`, `uid`, `cur_date`) 
											   VALUES (:qid, :option, :val1, :val2, :val3, :val4, :val5, :val6, :true, :status, :user_id, :cur_date)";
									$stmt	= $db->prepare($ansSql);
									$stmt->execute(array(
											'qid' 				=> $ques_id,
											'option'			=> ( ! empty($answerData)) ? htmlspecialchars($answerData) : '',
											'val1'				=> ( ! empty($videoq_ans_val1[$vi]['score'][$voption])) ? $videoq_ans_val1[$vi]['score'][$voption] : '',
											'val2'				=> ( ! empty($videoq_ans_val2[$vi]['score'][$voption])) ? $videoq_ans_val2[$vi]['score'][$voption] : '',
											'val3'				=> ( ! empty($videoq_ans_val3[$vi]['score'][$voption])) ? $videoq_ans_val3[$vi]['score'][$voption] : '',
											'val4'				=> ( ! empty($videoq_ans_val4[$vi]['score'][$voption])) ? $videoq_ans_val4[$vi]['score'][$voption] : '',
											'val5'				=> ( ! empty($videoq_ans_val5[$vi]['score'][$voption])) ? $videoq_ans_val5[$vi]['score'][$voption] : '',
											'val6'				=> ( ! empty($videoq_ans_val6[$vi]['score'][$voption])) ? $videoq_ans_val6[$vi]['score'][$voption] : '',
											'true'				=> ( isset($videoq_true_option[$vi][0]) && $videoq_true_option[$vi][0] == $voptiontrue) ? 1 : '',
											'status'			=> 1,
											'user_id' 			=> $userId,
											'cur_date'			=> $curdate));
									//fetch last inserted id
									$answer_id = $db->lastInsertId();
								endif;
								
								#-----FEEDBACK-DATA---
								if ( ! empty($videoqf_feedback_audio[$vi][$voption])):
									$feed_audio	= $videoqf_feedback_audio[$vi][$voption];
								elseif ( ! empty($videoqf_feedback_rec_audio[$vi][$voption])):
									$feed_audio = $videoqf_feedback_rec_audio[$vi][$voption];
								endif;
								
								if ( ! empty($videoqf_feedback_video[$vi][$voption])):
									$feed_video	= $videoqf_feedback_video[$vi][$voption];
								elseif ( ! empty($videoqf_feedback_rec_video[$vi][$voption])):
									$feed_video= $videoqf_feedback_rec_video[$vi][$voption];
								endif;
								
								#--------Feedback----------
								$cfeedData = array('aid'			=> $answer_id,
												   'qid'			=> $ques_id,
												   'feedback'		=> ( ! empty($videoq_feedback[$vi][$voption])) ? htmlspecialchars($videoq_feedback[$vi][$voption]) : '',
												   'ftype'			=> 1,
												   'speech_text'	=> ( ! empty($videoqf_feedback_text_to_speech[$vi][$voption])) ? htmlspecialchars($videoqf_feedback_text_to_speech[$vi][$voption]) : '',
												   'feed_audio'		=> ( ! empty($feed_audio)) ? $feed_audio : '',
												   'feed_video'		=> ( ! empty($feed_video)) ? $feed_video : '',
												   'feed_screen'	=> ( ! empty($videoqf_feedback_rec_screen[$vi][$voption])) ? $videoqf_feedback_rec_screen[$vi][$voption] : '',
												   'feed_image'		=> ( ! empty($videoqf_feedback_img[$vi][$voption])) ? $videoqf_feedback_img[$vi][$voption] : '',
												   'feed_document'	=> ( ! empty($videoqf_feedback_doc[$vi][$voption])) ? $videoqf_feedback_doc[$vi][$voption] : '');
								$cfeed_sql  = "INSERT INTO `feedback_tbl` (`answer_id`, `question_id`, `feedback`, `feedback_type`, `feed_speech_text`, `feed_audio`, `feed_video`, `feed_screen`, `feed_image`, `feed_document`) 
											   VALUES (:aid, :qid, :feedback, :ftype, :speech_text, :feed_audio, :feed_video, :feed_screen, :feed_image, :feed_document)";
								$cfeed_stmt = $db->prepare($cfeed_sql); $cfeed_stmt->execute($cfeedData);
							$voption++; $voptiontrue++;
							endforeach;
						endif;
					else:
						if ( ! empty($videoq[$vi])):
							$quesData = array('sim_id'			=> $sim_id,
											'qType'				=> $question_type,
											'questions'			=> ( ! empty($videoq[$vi])) ? htmlspecialchars($videoq[$vi]) : '',
											'qaudio'			=> ( ! empty($qaudio)) ? $qaudio : '',
											'qspeech_text'		=> ( ! empty($videoqq_text_to_speech[$vi])) ? htmlspecialchars($videoqq_text_to_speech[$vi]) : '',
											'media_file'		=> ( ! empty($videoq_media_file)) ? $videoq_media_file : '',
											'cue_point'			=> ( ! empty($cue_point[$vi])) ? $cue_point[$vi] : '',
											'speech_text'		=> ( ! empty($videoq_text_to_speech[$vi])) ? htmlspecialchars($videoq_text_to_speech[$vi]) : '',
											'audio'				=> ( ! empty($audio)) ? $audio : '',
											'video'				=> ( ! empty($video)) ? $video : '',
											'screen'			=> ( ! empty($videoq_rec_screen[$vi])) ? $videoq_rec_screen[$vi] : '',
											'image'				=> ( ! empty($videoq_img[$vi])) ? $videoq_img[$vi] : '',
											'document'			=> ( ! empty($videoq_doc[$vi])) ? $videoq_doc[$vi] : '',
											'shuffle'			=> (isset($video_shuffle[$vi]) && ! empty($video_shuffle[$vi])) ? 1 : 0,
											'ques_val_1'		=> ( ! empty($videoq_ques_val_1[$vi])) ? $videoq_ques_val_1[$vi] : '',
											'ques_val_2'		=> ( ! empty($videoq_ques_val_2[$vi])) ? $videoq_ques_val_2[$vi] : '',
											'ques_val_3'		=> ( ! empty($videoq_ques_val_3[$vi])) ? $videoq_ques_val_3[$vi] : '',
											'ques_val_4'		=> ( ! empty($videoq_ques_val_4[$vi])) ? $videoq_ques_val_4[$vi] : '',
											'ques_val_5'		=> ( ! empty($videoq_ques_val_5[$vi])) ? $videoq_ques_val_5[$vi] : '',
											'ques_val_6'		=> ( ! empty($videoq_ques_val_6[$vi])) ? $videoq_ques_val_6[$vi] : '',
											'critical'			=> ( ! empty($video_criticalQ[$vi])) ? $video_criticalQ[$vi] : '',
											'status' 			=> 1,
											'user_id'			=> $userId,
											'cur_date'			=> $curdate);
							$ques_sql = "INSERT INTO `question_tbl` (`scenario_id`, `question_type`, `questions`,  `qaudio`, `qspeech_text`, `shuffle`,`videoq_media_file`, `videoq_cue_point`, `speech_text`, `audio`, `video`, `screen`, `image`, `document`, `ques_val_1`, `ques_val_2`, `ques_val_3`, `ques_val_4`, `ques_val_5`, `ques_val_6`, `critical`, `status`, `uid`, `cur_date`) 
										 VALUES (:sim_id, :qType, :questions, :qaudio, :qspeech_text, :shuffle, :media_file, :cue_point, :speech_text, :audio, :video, :screen, :image, :document, :ques_val_1, :ques_val_2, :ques_val_3, :ques_val_4, :ques_val_5, :ques_val_6, :critical, :status, :user_id, :cur_date)";
							$ques_stmt = $db->prepare($ques_sql);
							$ques_stmt->execute($quesData);
							$ques_id = $db->lastInsertId();
							
							if ( ! empty($videoq_option[$vi]['option'])):
								//CHOICE
								$voption = 0; $voptiontrue = 1;
								foreach ($videoq_option[$vi]['option'] as $answerData):
									$ansSql = "INSERT INTO `answer_tbl` (`question_id`, `choice_option`, `ans_val1`, `ans_val2`, `ans_val3`, `ans_val4`, `ans_val5`, `ans_val6`, `true_option`, `status`, `uid`, `cur_date`) 
												VALUES (:qid, :option, :val1, :val2, :val3, :val4, :val5, :val6, :true, :status, :user_id, :cur_date)";
										$stmt = $db->prepare($ansSql);
										$stmt->execute(array(
												'qid' 				=> $ques_id,
												'option'			=> ( ! empty($answerData)) ? htmlspecialchars($answerData) : '',
												'val1'				=> ( ! empty($videoq_ans_val1[$vi]['score'][$voption])) ? $videoq_ans_val1[$vi]['score'][$voption] : '',
												'val2'				=> ( ! empty($videoq_ans_val2[$vi]['score'][$voption])) ? $videoq_ans_val2[$vi]['score'][$voption] : '',
												'val3'				=> ( ! empty($videoq_ans_val3[$vi]['score'][$voption])) ? $videoq_ans_val3[$vi]['score'][$voption] : '',
												'val4'				=> ( ! empty($videoq_ans_val4[$vi]['score'][$voption])) ? $videoq_ans_val4[$vi]['score'][$voption] : '',
												'val5'				=> ( ! empty($videoq_ans_val5[$vi]['score'][$voption])) ? $videoq_ans_val5[$vi]['score'][$voption] : '',
												'val6'				=> ( ! empty($videoq_ans_val6[$vi]['score'][$voption])) ? $videoq_ans_val6[$vi]['score'][$voption] : '',
												'true'				=> ( isset($videoq_true_option[$vi][0]) && $videoq_true_option[$vi][0] == $voptiontrue) ? 1 : '',
												'status'			=> 1,
												'user_id' 			=> $userId,
												'cur_date'			=> $curdate));
									//fetch last inserted id
									$answer_id = $db->lastInsertId();
									
									#-----FEEDBACK-DATA---
									if ( ! empty($videoqf_feedback_audio[$vi][$voption])):
										$feed_audio	= $videoqf_feedback_audio[$vi][$voption];
									elseif ( ! empty($videoqf_feedback_rec_audio[$vi][$voption])):
										$feed_audio = $videoqf_feedback_rec_audio[$vi][$voption];
									endif;
									
									if ( ! empty($videoqf_feedback_video[$vi][$voption])):
										$feed_video	= $videoqf_feedback_video[$vi][$voption];
									elseif ( ! empty($videoqf_feedback_rec_video[$vi][$voption])):
										$feed_video= $videoqf_feedback_rec_video[$vi][$voption];
									endif;
									
									#--------Feedback----------
									$cfeedData = array('aid'			=> $answer_id,
													   'qid'			=> $ques_id,
													   'feedback'		=> ( ! empty($videoq_feedback[$vi][$voption])) ? htmlspecialchars($videoq_feedback[$vi][$voption]) : '',
													   'ftype'			=> 1,
													   'speech_text'	=> ( ! empty($videoqf_feedback_text_to_speech[$vi][$voption])) ? htmlspecialchars($videoqf_feedback_text_to_speech[$vi][$voption]) : '',
													   'feed_audio'		=> ( ! empty($feed_audio)) ? $feed_audio : '',
													   'feed_video'		=> ( ! empty($feed_video)) ? $feed_video : '',
													   'feed_screen'	=> ( ! empty($videoqf_feedback_rec_screen[$vi][$voption])) ? $videoqf_feedback_rec_screen[$vi][$voption] : '',
													   'feed_image'		=> ( ! empty($videoqf_feedback_img[$vi][$voption])) ? $videoqf_feedback_img[$vi][$voption] : '',
													   'feed_document'	=> ( ! empty($videoqf_feedback_doc[$vi][$voption])) ? $videoqf_feedback_doc[$vi][$voption] : '');
									$cfeed_sql  = "INSERT INTO `feedback_tbl` (`answer_id`, `question_id`, `feedback`, `feedback_type`, `feed_speech_text`, `feed_audio`, `feed_video`, `feed_screen`, `feed_image`, `feed_document`) 
												   VALUES (:aid, :qid, :feedback, :ftype, :speech_text, :feed_audio, :feed_video, :feed_screen, :feed_image, :feed_document)";
									$cfeed_stmt = $db->prepare($cfeed_sql); $cfeed_stmt->execute($cfeedData);
								$voption++; $voptiontrue++;
								endforeach;
							endif;
						endif;
					endif;
				$vi++;
			endforeach;
			if ( ! empty($submit_type) && $submit_type == 1):
				$resdata = array('success' => TRUE, 'all_update' => FALSE, 'msg' => 'Simulation details updated successfully.', 'return_url' => 'update-single-video-template.php?add_data=true&sim_id='.md5($sim_id).'&ques_id='.base64_encode($ques_id));
			else:
				$resdata = array('success' => TRUE, 'all_update' => TRUE, 'msg' => 'All questions and answers updated successfully.', 'return_url' => 'author-simulation.php');
			endif;
		  endif;
	   endif;
	endif;
	echo json_encode($resdata);
endif;

#-----------------------------------------Add-New-CuePoint--------------------------------------
if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['add_cue_point']) && ! empty($_POST['sim_id'])):
	$sim_id		= ( ! empty($_POST['sim_id'])) ? $_POST['sim_id'] : exit;
	$cue_point	= ( ! empty($_POST['add_cue_point'])) ? $_POST['add_cue_point'] : exit;
	$file		= ( ! empty($_POST['file'])) ? $_POST['file'] : exit;
	$tq			= $db->getTotalQuestionsByScenario(md5($sim_id)) + 1;
	$quesData	= array('sid'		=> $sim_id,
						'questions'	=> 'Question '. $tq,
						'cue_point'	=> $cue_point,
						'file'		=> $file,
						'status' 	=> 1,
						'user_id'	=> $userId,
						'cur_date'	=> $curdate);
	$ques_sql	= "INSERT INTO `question_tbl` (`scenario_id`, `questions`, `videoq_cue_point`, `videoq_media_file`, `status`, `uid`, `cur_date`) VALUES (:sid, :questions, :cue_point, :file, :status, :user_id, :cur_date)";
	$ques_stmt	= $db->prepare($ques_sql);
	if ($ques_stmt->execute($quesData)):
		$ques_id = $db->lastInsertId();
		$resdata = array('success' => TRUE, 'msg' => 'Question created successfully.', 'quesId' => $ques_id);
	else:
		$resdata = array('success' => FALSE, 'msg' => 'Question not created. Please try again later.');
	endif;
	echo json_encode($resdata);
endif;

#---------------------------------------Get-Cue-List-------------------------------------
if ($_SERVER['REQUEST_METHOD'] == 'GET' && isset($_GET['getCueList']) && isset($_GET['sim_id'])):
	$sim_id = (isset($_GET['sim_id'])) ? $_GET['sim_id'] : '';
	if ( ! empty($sim_id)):
		$sql 	= "SELECT question_id, questions FROM question_tbl WHERE MD5(scenario_id) = '". $sim_id ."' ORDER BY question_id";
		$q	 	= $db->prepare($sql); $q->execute();
		$data	= $q->fetchAll();
		if ( ! empty($data)):
			$res = array('success' => TRUE, 'data' => $data);
		else:
			$res = array('success' => FALSE, 'data' => NULL);
		endif;
	else:
		$res = array('success' => FALSE, 'data' => NULL);
	endif;
	echo json_encode($res);
endif;

#-----------------------------------------Update-Linear-Video-Sim - Saumya ----------------------------------
if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['update_linear_video_sim']) && isset($_POST['sim_id'])):
	extract($_POST);
	$error = FALSE;
	if (empty($sim_page_name[0]) || empty($sim_duration) || empty($passing_marks)):
		$resdata = array('success' => FALSE, 'msg' => 'Please fill all the mandatory fields on add Simulation and Simulation details to proceed.');
		$error = TRUE;
		echo json_encode($resdata); exit;
	elseif (isset($competency_name_1) && empty($competency_name_1) || empty($competency_score_1) || empty($weightage_1)):
		$resdata = array('success' => FALSE, 'msg' => 'Please fill all the mandatory fields on Simulation details to proceed.');
		$error = TRUE;
		echo json_encode($resdata); exit;
	elseif ($error == FALSE):
		$sql = "UPDATE scenario_master SET `Scenario_title` = '". $sim_title ."', `duration` = '". $sim_duration ."', `passing_marks` = '". $passing_marks ."', `sim_cover_img` = '". $scenario_cover_file ."'";		
		#--Add-Web-Object--
		if ( ! empty($web_object)):
			$sql .= ", web_object = '". $web_object ."', web_object_title = '". htmlspecialchars($web_object_title) ."'";
		endif;
		#--Sim-Media-File--
		if ( ! empty($scenario_media_file) && in_array(strtolower(pathinfo($scenario_media_file, PATHINFO_EXTENSION)), $img_valid_exts)):
			$sql .= ", image_url = '". $scenario_media_file ."', video_url = NULL, audio_url = NULL";
		elseif ( ! empty($scenario_media_file) && in_array(strtolower(pathinfo($scenario_media_file, PATHINFO_EXTENSION)), $video_valid_exts)):
			$sql .= ", video_url = '". $scenario_media_file ."', image_url = NULL, audio_url = NULL";
		elseif ( ! empty($scenario_media_file) && in_array(strtolower(pathinfo($scenario_media_file, PATHINFO_EXTENSION)), $audio_valid_exts)):
			$sql .= ", audio_url = '". $scenario_media_file ."', video_url = NULL, image_url = NULL";
		elseif ( ! empty($scenario_media_file) && ! in_array(strtolower(pathinfo($scenario_media_file, PATHINFO_EXTENSION)), $audio_valid_exts) && 
				! in_array(strtolower(pathinfo($scenario_media_file, PATHINFO_EXTENSION)), $img_valid_exts) && 
				! in_array(strtolower(pathinfo($scenario_media_file, PATHINFO_EXTENSION)), $video_valid_exts)):
			$sql .= ", web_object_file = '". $scenario_media_file ."', video_url = NULL, image_url = NULL, audio_url = NULL";
		endif;
		$sql .= " WHERE scenario_id = '". $sim_id ."'";
		$results = $db->prepare($sql);
		if ($results->execute()):
			#----Competency----
			$delCom	 = "DELETE FROM competency_tbl WHERE scenario_id = '". $sim_id ."'";
			$delComq = $db->prepare($delCom); $delComq->execute();
			$data	 = array('scenario_id'	=> $sim_id,
							 'comp_col_1'	=> (isset($competency_name_1) && ! empty($competency_name_1)) ? addslashes($competency_name_1) : '',
							 'comp_val_1'	=> (isset($competency_score_1) && ! empty($competency_score_1)) ? $competency_score_1 : '',
							 'comp_col_2'	=> (isset($competency_name_2) && ! empty($competency_name_2)) ? addslashes($competency_name_2) : '',
							 'comp_val_2'	=> (isset($competency_score_2) && ! empty($competency_score_2)) ? $competency_score_2 : '',
							 'comp_col_3'	=> (isset($competency_name_3) && ! empty($competency_name_3)) ? addslashes($competency_name_3) : '',
							 'comp_val_3'	=> (isset($competency_score_3) && ! empty($competency_score_3)) ? $competency_score_3 : '',
							 'comp_col_4'	=> (isset($competency_name_4) && ! empty($competency_name_4)) ? addslashes($competency_name_4) : '',
							 'comp_val_4'	=> (isset($competency_score_4) && ! empty($competency_score_4)) ? $competency_score_4 : '',
							 'comp_col_5'	=> (isset($competency_name_5) && ! empty($competency_name_5)) ? addslashes($competency_name_5) : '',
							 'comp_val_5'	=> (isset($competency_score_5) && ! empty($competency_score_5)) ? $competency_score_5 : '',
							 'comp_col_6'	=> (isset($competency_name_6) && ! empty($competency_name_6)) ? addslashes($competency_name_6) : '',
							 'comp_val_6'	=> (isset($competency_score_6) && ! empty($competency_score_6)) ? $competency_score_6 : '',
							 'weightage_1'	=> (isset($weightage_1) && ! empty($weightage_1)) ? $weightage_1 : '',
							 'weightage_2'	=> (isset($weightage_2) && ! empty($weightage_2)) ? $weightage_2 : '',
							 'weightage_3'	=> (isset($weightage_3) && ! empty($weightage_3)) ? $weightage_3 : '',
							 'weightage_4'	=> (isset($weightage_4) && ! empty($weightage_4)) ? $weightage_4 : '',
							 'weightage_5'	=> (isset($weightage_5) && ! empty($weightage_5)) ? $weightage_5 : '',
							 'weightage_6'	=> (isset($weightage_6) && ! empty($weightage_6)) ? $weightage_6 : '',
							 'status' 		=> 1,
							 'user_id' 		=> $userId,
							 'cur_date'		=> $curdate);
			$com_sql  = "INSERT INTO `competency_tbl` (`scenario_id`, `comp_col_1`, `comp_val_1`, `comp_col_2`, `comp_val_2`, `comp_col_3`, `comp_val_3`, `comp_col_4`, `comp_val_4`, `comp_col_5`, `comp_val_5`, `comp_col_6`, `comp_val_6`, `weightage_1`, `weightage_2`, `weightage_3`, `weightage_4`, `weightage_5`, `weightage_6`, `status`, `uid`, `cur_date`) VALUES (:scenario_id, :comp_col_1, :comp_val_1, :comp_col_2, :comp_val_2, :comp_col_3, :comp_val_3, :comp_col_4, :comp_val_4, :comp_col_5, :comp_val_5, :comp_col_6, :comp_val_6, :weightage_1, :weightage_2, :weightage_3, :weightage_4, :weightage_5, :weightage_6, :status, :user_id, :cur_date)";
			$com_stmt = $db->prepare($com_sql);
			$com_stmt->execute($data);
			
			#----PAGES----
			$delPage  = "DELETE FROM sim_pages WHERE scenario_id = '". $sim_id ."'";
			$delPageq = $db->prepare($delPage); $delPageq->execute();
			if ( ! empty($sim_page_name)):
				$sp = 0;
				foreach ($sim_page_name as $pdata):
					$pageq = "INSERT INTO `sim_pages` (`scenario_id`, `sim_page_name`, `sim_page_desc`, `userid`, `status`, `added_date`) 
					VALUES (:sid, :pname, :pdesc, :user_id, :status, :adate)";
					$page_stmt = $db->prepare($pageq);
					$page_stmt->execute(array('sid'		=> $sim_id,
											  'pname'	=> ( ! empty($pdata)) ? addslashes($pdata) : '',
											  'pdesc'	=> ( ! empty($sim_page_desc[$sp])) ? $sim_page_desc[$sp] : '',
											  'status' 	=> 1,
											  'user_id'	=> $userId,
											  'adate'	=> $curdate));
				$sp++;
				endforeach;
			endif;
			
			#----Branding----
			$bandq	  = "DELETE FROM sim_branding_tbl WHERE scenario_id = '". $sim_id ."'";
			$bandqexe = $db->prepare($bandq); $bandqexe->execute();
			$brand_data = array('sid'				=> $sim_id,
								'bg'				=> ( ! empty($ques_bg_color)) ? $ques_bg_color : '',
								'bg_tr'				=> ( ! empty($ques_bg_transp)) ? $ques_bg_transp : '',
								'obg'				=> ( ! empty($ques_option_bg_color)) ? $ques_option_bg_color : '',
								'obg_tr'			=> ( ! empty($ques_option_bg_transp)) ? $ques_option_bg_transp : '',
								'ohover'			=> ( ! empty($ques_option_hover_color)) ? $ques_option_hover_color : '',
								'oh_tr'				=> ( ! empty($ques_option_hover_transp)) ? $ques_option_hover_transp : '',
								'oselected'			=> ( ! empty($ques_option_select_color)) ? $ques_option_select_color : '',
								'oselected_tr'		=> ( ! empty($ques_option_select_transp)) ? $ques_option_select_transp : '',
								'btn_bg'			=> ( ! empty($btn_bg_color)) ? $btn_bg_color : '',
								'btn_bg_tr'			=> ( ! empty($btn_bg_transp)) ? $btn_bg_transp : '',
								'btn_hover'			=> ( ! empty($btn_hover_color)) ? $btn_hover_color : '',
								'btn_hover_tr'		=> ( ! empty($btn_hover_transp)) ? $btn_hover_transp : '',
								'btn_selected'		=> ( ! empty($btn_select_color)) ? $btn_select_color : '',
								'btn_selected_tr'	=> ( ! empty($btn_select_transp)) ? $btn_select_transp : '',
								'font_type'			=> ( ! empty($font_type)) ? $font_type : '',
								'font_color'		=> ( ! empty($font_color)) ? $font_color : '',
								'font_size'			=> ( ! empty($font_size)) ? $font_size : '',
								'user_id' 			=> $userId,
								'added_date'		=> $curdate);
			$com_sql = "INSERT INTO `sim_branding_tbl` (`scenario_id`, `ques_bg`, `ques_bg_transparency`, `option_bg`, `option_bg_transparency`, `option_hover`, `option_hover_transparency`, `option_selected`, `option_selected_transparency`, `btn_bg`, `btn_bg_transparency`, `btn_hover`, `btn_hover_transparency`, `btn_selected`, `btn_selected_transparency`, `font_type`, `font_color`, `font_size`, `user_id`, `added_date`) 
						VALUES (:sid, :bg, :bg_tr, :obg, :obg_tr, :ohover, :oh_tr, :oselected, :oselected_tr, :btn_bg, :btn_bg_tr, :btn_hover, :btn_hover_tr, :btn_selected, :btn_selected_tr, :font_type, :font_color, :font_size, :user_id, :added_date)";
			$com_stmt = $db->prepare($com_sql);
			$com_stmt->execute($brand_data);
		endif;
		
		//-----------START-MCQ----------
		if ($question_type == 4):
			#-------QUESTIONS-DATA---
			$qaudio = '';
			if ( ! empty($mcqq_qaudio)):
				$qaudio = $mcqq_qaudio;
			elseif ( ! empty($mcqq_rec_audio)):
				$qaudio = $mcqq_rec_audio;
			endif;
			
			$audio = '';
			if ( ! empty($mcq_audio)):
				$audio = $mcq_audio;
			elseif ( ! empty($mcq_rec_audio)):
				$audio = $mcq_rec_audio;
			endif;
			
			$video = '';
			if ( ! empty($mcq_video)):
				$video = $mcq_video;
			elseif ( ! empty($mcq_rec_video)):
				$video = $mcq_rec_video;
			endif;
			
			if(empty($ques_id)){
				$quesData = array('scenario_id'		=> $sim_id,
								  'question_type'	=> $question_type,
								  'questions'		=> ( ! empty($mcq)) ? $mcq : '',
								  'qaudio'			=> ( ! empty($qaudio)) ? $qaudio : '',
								  'qspeech_text'	=> ( ! empty($mcqq_text_to_speech)) ? $mcqq_text_to_speech : '',
								  'speech_text'		=> ( ! empty($mcq_text_to_speech)) ? $mcq_text_to_speech : '',
								  'audio'			=> ( ! empty($audio)) ? $audio : '',
								  'video'			=> ( ! empty($video)) ? $video : '',
								  'screen'			=> ( ! empty($mcq_rec_screen)) ? $mcq_rec_screen : '',
								  'image'			=> ( ! empty($mcq_img)) ? $mcq_img : '',
								  'document'		=> ( ! empty($mcq_doc)) ? $mcq_doc : '',
								  'true_option'		=> ( ! empty($mcq_true_option)) ? $mcq_true_option : '',
								  'media_file'		=> ( ! empty($videoq_media_file)) ? $videoq_media_file : '',
								  'shuffle'			=> (isset($mcq_shuffle) && ! empty($mcq_shuffle)) ? 1 : 0,
								  'ques_val_1'		=> ( ! empty($mcq_ques_val_1)) ? $mcq_ques_val_1 : '',
								  'ques_val_2'		=> ( ! empty($mcq_ques_val_2)) ? $mcq_ques_val_2 : '',
								  'ques_val_3'		=> ( ! empty($mcq_ques_val_3)) ? $mcq_ques_val_3 : '',
								  'ques_val_4'		=> ( ! empty($mcq_ques_val_4)) ? $mcq_ques_val_4 : '',
								  'ques_val_5'		=> ( ! empty($mcq_ques_val_5)) ? $mcq_ques_val_5 : '',
								  'ques_val_6'		=> ( ! empty($mcq_ques_val_6)) ? $mcq_ques_val_6 : '',
								  'critical'		=> (isset($mcq_criticalQ) && ! empty($mcq_criticalQ)) ? 1 : 0,
								  'status' 			=> 1,
								  'user_id'			=> $userId,
								  'cur_date'		=> $curdate);
				//Insert
				$ques_sql = "INSERT INTO `question_tbl` (`scenario_id`,`question_type`,`questions`, `qaudio`, `qspeech_text`, `speech_text`, `audio`, `video`, `screen`, `image`, `document`, `true_options`, `videoq_media_file`, `shuffle`, `ques_val_1`, `ques_val_2`, `ques_val_3`, `ques_val_4`, `ques_val_5`, `ques_val_6`, `critical`, `status`, `uid`, `cur_date`) 
							 VALUES (:scenario_id,:question_type, :questions,:qaudio, :qspeech_text, :speech_text, :audio, :video, :screen, :image, :document, :true_option, :media_file, :shuffle, :ques_val_1, :ques_val_2, :ques_val_3, :ques_val_4, :ques_val_5, :ques_val_6, :critical, :status, :user_id, :cur_date)";
				$ques_stmt = $db->prepare($ques_sql); $ques_stmt->execute($quesData);
				$ques_id = $db->lastInsertId();
			}
			else {
				$quesData = array('questions'		=> ( ! empty($mcq)) ? htmlspecialchars($mcq) : '',
								  'qtype'			=> ( ! empty($question_type)) ? $question_type : '',
								  'qaudio'			=> ( ! empty($qaudio)) ? $qaudio : '',
								  'qspeech_text'	=> ( ! empty($mcqq_text_to_speech)) ? htmlspecialchars($mcqq_text_to_speech) : '',
								  'speech_text'		=> ( ! empty($mcq_text_to_speech)) ? htmlspecialchars($mcq_text_to_speech) : '',
								  'audio'			=> ( ! empty($audio)) ? $audio : '',
								  'video'			=> ( ! empty($video)) ? $video : '',
								  'screen'			=> ( ! empty($mcq_rec_screen)) ? $mcq_rec_screen : '',
								  'image'			=> ( ! empty($mcq_img)) ? $mcq_img : '',
								  'document'		=> ( ! empty($mcq_doc)) ? $mcq_doc : '',
								  'true_option'		=> ( ! empty($mcq_true_option)) ? $mcq_true_option : '',
								  'media_file'		=> ( ! empty($videoq_media_file)) ? $videoq_media_file : '',
								  'shuffle'			=> (isset($mcq_shuffle) && ! empty($mcq_shuffle)) ? 1 : 0,
								  'ques_val_1'		=> ( ! empty($mcq_ques_val_1)) ? $mcq_ques_val_1 : '',
								  'ques_val_2'		=> ( ! empty($mcq_ques_val_2)) ? $mcq_ques_val_2 : '',
								  'ques_val_3'		=> ( ! empty($mcq_ques_val_3)) ? $mcq_ques_val_3 : '',
								  'ques_val_4'		=> ( ! empty($mcq_ques_val_4)) ? $mcq_ques_val_4 : '',
								  'ques_val_5'		=> ( ! empty($mcq_ques_val_5)) ? $mcq_ques_val_5 : '',
								  'ques_val_6'		=> ( ! empty($mcq_ques_val_6)) ? $mcq_ques_val_6 : '',
								  'critical'		=> ( ! empty($mcq_criticalQ)) ? $mcq_criticalQ : '',
								  'status' 			=> 1,
								  'user_id'			=> $userId,
								  'cur_date'		=> $curdate,
								  'qid'				=> $ques_id);
				$ques_sql  = "UPDATE `question_tbl` SET `questions`=:questions, `question_type`=:qtype, `qaudio`=:qaudio, `qspeech_text`=:qspeech_text, `speech_text`=:speech_text, `audio`=:audio, `video`=:video, `screen`=:screen, `image`=:image, `document`=:document, `true_options`=:true_option, `videoq_media_file`=:media_file, `shuffle`=:shuffle, `ques_val_1`=:ques_val_1, `ques_val_2`=:ques_val_2, `ques_val_3`=:ques_val_3, `ques_val_4`=:ques_val_4, `ques_val_5`=:ques_val_5, `ques_val_6`=:ques_val_6, `critical`=:critical, `status`=:status, `uid`=:user_id, `cur_date`=:cur_date WHERE question_id=:qid";
				$ques_stmt = $db->prepare($ques_sql);
				$ques_stmt->execute($quesData);
			}
			#-----Remove-Old-Feedback-------
			$db->feedback_remove($ques_id);
			#----------ADD-OPTIONS-------------
			if ( ! empty($mcq_option[0])):
				$mcqi = 0; $mcqtrue = 1;
				foreach ($mcq_option as $answerData):
					if ( ! empty($updateMCQAnswerid[$mcqi])):
						$ans_id = $updateMCQAnswerid[$mcqi];
						$ansSql = "UPDATE `answer_tbl` SET `choice_option`=:option, `ans_val1`=:val1, `ans_val2`=:val2, `ans_val3`=:val3, `ans_val4`=:val4, `ans_val5`=:val5, `ans_val6`=:val6, `true_option`=:true, `status`=:status, `uid`=:user_id, `cur_date`=:cur_date WHERE answer_id=:aid";
						$stmt	= $db->prepare($ansSql);
						$stmt->execute(array('aid'		=> $ans_id,
											 'option'	=> ( ! empty($mcq_option[$mcqi])) ? htmlspecialchars($mcq_option[$mcqi]) : '',
											 'val1'		=> ( ! empty($mcq_ans_val1[$mcqi])) ? $mcq_ans_val1[$mcqi] : '',
											 'val2'		=> ( ! empty($mcq_ans_val2[$mcqi])) ? $mcq_ans_val2[$mcqi] : '',
											 'val3'		=> ( ! empty($mcq_ans_val3[$mcqi])) ? $mcq_ans_val3[$mcqi] : '',
											 'val4'		=> ( ! empty($mcq_ans_val4[$mcqi])) ? $mcq_ans_val4[$mcqi] : '',
											 'val5'		=> ( ! empty($mcq_ans_val5[$mcqi])) ? $mcq_ans_val5[$mcqi] : '',
											 'val6'		=> ( ! empty($mcq_ans_val6[$mcqi])) ? $mcq_ans_val6[$mcqi] : '',
											 'true'		=> ( ! empty($mcq_true_option) && $mcq_true_option == $mcqtrue) ? 1 : '',
											 'status'	=> 1,
											 'user_id' 	=> $userId,
											 'cur_date'	=> $curdate));
					else:
						$ansSql = "INSERT INTO `answer_tbl` (`question_id`, `choice_option`, `ans_val1`, `ans_val2`, `ans_val3`, `ans_val4`, `ans_val5`, `ans_val6`, `true_option`, `status`, `uid`, `cur_date`) values (:qid, :option, :ans_val1, :ans_val2, :ans_val3, :ans_val4, :ans_val5, :ans_val6, :true, :status, :user_id, :cur_date)";
						$stmt	= $db->prepare($ansSql);
						$stmt->execute(array('qid'		=> $ques_id,
											 'option'	=> ( ! empty($mcq_option[$mcqi])) ? htmlspecialchars($mcq_option[$mcqi]) : '',
											 'ans_val1'	=> ( ! empty($mcq_ans_val1[$mcqi])) ? $mcq_ans_val1[$mcqi] : '',
											 'ans_val2'	=> ( ! empty($mcq_ans_val2[$mcqi])) ? $mcq_ans_val2[$mcqi] : '',
											 'ans_val3'	=> ( ! empty($mcq_ans_val3[$mcqi])) ? $mcq_ans_val3[$mcqi] : '',
											 'ans_val4'	=> ( ! empty($mcq_ans_val4[$mcqi])) ? $mcq_ans_val4[$mcqi] : '',
											 'ans_val5'	=> ( ! empty($mcq_ans_val5[$mcqi])) ? $mcq_ans_val5[$mcqi] : '',
											 'ans_val6'	=> ( ! empty($mcq_ans_val6[$mcqi])) ? $mcq_ans_val6[$mcqi] : '',
											 'true'		=> ( ! empty($mcq_true_option) && $mcq_true_option == $mcqtrue) ? 1 : '',
											 'status'	=> 1,
											 'user_id' 	=> $userId,
											 'cur_date'	=> $curdate));
						$ans_id = $db->lastInsertId();
					endif;
					
					#------Correct-FEEDBACK-DATA---
					$feed_audio = '';
					if ( ! empty($mcq_feedback_audio[$mcqi])):
						$feed_audio = $mcq_feedback_audio[$mcqi];
					elseif ( ! empty($mcq_feedback_rec_audio[$mcqi])):
						$feed_audio = $mcq_feedback_rec_audio[$mcqi];
					endif;
					
					$feed_video = '';
					if ( ! empty($mcq_feedback_video[$mcqi])):
						$feed_video = $mcq_feedback_video[$mcqi];
					elseif ( ! empty($mcq_feedback_rec_video[$mcqi])):
						$feed_video = $mcq_feedback_rec_video[$mcqi];
					endif;
					
					#--------Correct-Feedback----------
					$cfeedData = array('qid'			=> ( ! empty($ques_id)) ? $ques_id : '',
									   'aid'			=> ( ! empty($ans_id)) ? $ans_id : '',
									   'feedback'		=> ( ! empty($mcq_cfeedback[$mcqi])) ? htmlspecialchars($mcq_cfeedback[$mcqi]) : '',
									   'ftype'			=> 1,
									   'speech_text'	=> ( ! empty($mcq_feedback_text_to_speech[$mcqi])) ? htmlspecialchars($mcq_feedback_text_to_speech[$mcqi]) : '',
									   'audio'			=> ( ! empty($feed_audio)) ? $feed_audio : '',
									   'video'			=> ( ! empty($feed_video)) ? $feed_video : '',
									   'screen'			=> ( ! empty($mcq_feedback_rec_screen[$mcqi])) ? $mcq_feedback_rec_screen[$mcqi] : '',
									   'image'			=> ( ! empty($mcq_feedback_img[$mcqi])) ? $mcq_feedback_img[$mcqi] : '',
									   'document'		=> ( ! empty($mcq_feedback_doc[$mcqi])) ? $mcq_feedback_doc[$mcqi] : '');
					$cfeed_sql  = "INSERT INTO `feedback_tbl` (`question_id`, `answer_id`, `feedback`, `feedback_type`, `feed_speech_text`, `feed_audio`, `feed_video`, `feed_screen`, `feed_image`, `feed_document`) VALUES (:qid, :aid, :feedback, :ftype, :speech_text, :audio, :video, :screen, :image, :document)";
					$cfeed_stmt = $db->prepare($cfeed_sql);
					$cfeed_stmt->execute($cfeedData);
					$mcqi++; $mcqtrue++;
				endforeach;
			endif;
		//-----------END-MCQ----------

		//-----------START-SWIPING-----
		elseif ($question_type == 7):
			#-------QUESTIONS-DATA---
			$qaudio = '';
			if ( ! empty($swipqq_qaudio)):
				$qaudio = $swipqq_qaudio;
			elseif ( ! empty($swipqq_rec_audio)):
				$qaudio = $swipqq_rec_audio;
			endif;

			$audio = '';
			if ( ! empty($swipq_audio)):
				$audio = $swipq_audio;
			elseif ( ! empty($swipq_rec_audio)):
				$audio = $swipq_rec_audio;
			endif;
			
			$video = '';
			if ( ! empty($swipq_video)):
				$video = $swipq_video;
			elseif ( ! empty($swipq_rec_video)):
				$video = $swipq_rec_video;
			endif;
			
			$quesData = array('questions'		=> ( ! empty($swipq)) ? htmlspecialchars($swipq) : '',
							  'qtype'			=> ( ! empty($question_type)) ? $question_type : '',
							  'qaudio'			=> ( ! empty($qaudio)) ? $qaudio : '',
							  'qspeech_text'	=> ( ! empty($swipqq_text_to_speech)) ? htmlspecialchars($swipqq_text_to_speech) : '',
							  'speech_text'		=> ( ! empty($swipq_text_to_speech)) ? htmlspecialchars($swipq_text_to_speech) : '',
							  'audio'			=> ( ! empty($audio)) ? $audio : '',
							  'video'			=> ( ! empty($video)) ? $video : '',
							  'screen'			=> ( ! empty($swipq_rec_screen)) ? $swipq_rec_screen : '',
							  'image'			=> ( ! empty($swipq_img)) ? $swipq_img : '',
							  'document'		=> ( ! empty($swipq_doc)) ? $swipq_doc : '',
							  'true_option'		=> ( ! empty($swipq_true_option)) ? $swipq_true_option : '',
							  'media_file'		=> ( ! empty($videoq_media_file)) ? $videoq_media_file : '',
							  'shuffle'			=> (isset($swipq_shuffle) && ! empty($swipq_shuffle)) ? 1 : 0,
							  'ques_val_1'		=> ( ! empty($swipq_ques_val_1)) ? $swipq_ques_val_1 : '',
							  'ques_val_2'		=> ( ! empty($swipq_ques_val_2)) ? $swipq_ques_val_2 : '',
							  'ques_val_3'		=> ( ! empty($swipq_ques_val_3)) ? $swipq_ques_val_3 : '',
							  'ques_val_4'		=> ( ! empty($swipq_ques_val_4)) ? $swipq_ques_val_4 : '',
							  'ques_val_5'		=> ( ! empty($swipq_ques_val_5)) ? $swipq_ques_val_5 : '',
							  'ques_val_6'		=> ( ! empty($swipq_ques_val_6)) ? $swipq_ques_val_6 : '',
							  'critical'		=> ( ! empty($swip_criticalQ)) ? $swip_criticalQ : '',
							  'status' 			=> 1,
							  'user_id'			=> $userId,
							  'cur_date'		=> $curdate,
							  'qid'				=> $ques_id);
			$ques_sql  = "UPDATE `question_tbl` SET `questions`=:questions, `question_type`=:qtype, `qaudio`=:qaudio, `qspeech_text`=:qspeech_text, `speech_text`=:speech_text, `audio`=:audio, `video`=:video, `screen`=:screen, `image`=:image, `document`=:document, `true_options`=:true_option, `videoq_media_file`=:media_file, `shuffle`=:shuffle, `ques_val_1`=:ques_val_1, `ques_val_2`=:ques_val_2, `ques_val_3`=:ques_val_3, `ques_val_4`=:ques_val_4, `ques_val_5`=:ques_val_5, `ques_val_6`=:ques_val_6, `critical`=:critical, `status`=:status, `uid`=:user_id, `cur_date`=:cur_date WHERE question_id=:qid";
			$ques_stmt = $db->prepare($ques_sql);
			$ques_stmt->execute($quesData); 
			
			#-----Remove-Old-Feedback-------
			$db->feedback_remove($ques_id);
			
			#----------ADD-OPTIONS-------------
			if ( ! empty($swipq_option[0])):
				$swipqi = 0; $swiptrue = 1;
				foreach ($swipq_option as $answerData):
					if ( ! empty($updateSwipqAnswerid[$swipqi])):
						$getSwipqAnswerid = $updateSwipqAnswerid[$swipqi];
						$ansSql = "UPDATE `answer_tbl` SET `choice_option`=:option, `image`=:image, `ans_val1`=:val1, `ans_val2`=:val2, `ans_val3`=:val3, `ans_val4`=:val4, `ans_val5`=:val5, `ans_val6`=:val6, `true_option`=:true, `status`=:status, `uid`=:user_id, `cur_date`=:cur_date WHERE answer_id=:aid";
						$stmt   = $db->prepare($ansSql);
						$stmt->execute(array('aid' 		=> $getSwipqAnswerid,
											 'option'	=> ( ! empty($swipq_option[$swipqi])) ? htmlspecialchars($swipq_option[$swipqi]) : '',
											 'image'	=> ( ! empty($swipq_item_img[$swipqi])) ? $swipq_item_img[$swipqi] : '',
											 'val1'		=> ( ! empty($swip_ans_val1[$swipqi])) ? $swip_ans_val1[$swipqi] : '',
											 'val2'		=> ( ! empty($swip_ans_val2[$swipqi])) ? $swip_ans_val2[$swipqi] : '',
											 'val3'		=> ( ! empty($swip_ans_val3[$swipqi])) ? $swip_ans_val3[$swipqi] : '',
											 'val4'		=> ( ! empty($swip_ans_val4[$swipqi])) ? $swip_ans_val4[$swipqi] : '',
											 'val5'		=> ( ! empty($swip_ans_val5[$swipqi])) ? $swip_ans_val5[$swipqi] : '',
											 'val6'		=> ( ! empty($swip_ans_val6[$swipqi])) ? $swip_ans_val6[$swipqi] : '',
											 'true'		=> ( ! empty($swipq_true_option) && $swipq_true_option == $swiptrue) ? 1 : '',
											 'status'	=> 1,
											 'user_id' 	=> $userId,
											 'cur_date'	=> $curdate));
						
					else:
						$ansSql = "INSERT INTO `answer_tbl` (`question_id`, `choice_option`, `image`, `ans_val1`, `ans_val2`, `ans_val3`, `ans_val4`, `ans_val5`, `ans_val6`, `true_option`, `status`, `uid`, `cur_date`) values (:qid, :option, :image, :val1, :val2, :val3, :val4, :val5, :val6, :true, :status, :user_id, :cur_date)";
						$stmt = $db->prepare($ansSql);
						$stmt->execute(array('qid'		=> $ques_id,
											 'option'	=> ( ! empty($swipq_option[$swipqi])) ? htmlspecialchars($swipq_option[$swipqi]) : '',
											 'image'	=> ( ! empty($swipq_item_img[$swipqi])) ? $swipq_item_img[$swipqi] : '',
											 'val1'		=> ( ! empty($swip_ans_val1[$swipqi])) ? $swip_ans_val1[$swipqi] : '',
											 'val2'		=> ( ! empty($swip_ans_val2[$swipqi])) ? $swip_ans_val2[$swipqi] : '',
											 'val3'		=> ( ! empty($swip_ans_val3[$swipqi])) ? $swip_ans_val3[$swipqi] : '',
											 'val4'		=> ( ! empty($swip_ans_val4[$swipqi])) ? $swip_ans_val4[$swipqi] : '',
											 'val5'		=> ( ! empty($swip_ans_val5[$swipqi])) ? $swip_ans_val5[$swipqi] : '',
											 'val6'		=> ( ! empty($swip_ans_val6[$swipqi])) ? $swip_ans_val6[$swipqi] : '',
											 'true'		=> ( ! empty($swipq_true_option) && $swipq_true_option == $swiptrue) ? 1 : '',
											 'status'	=> 1,
											 'user_id' 	=> $userId,
											 'cur_date'	=> $curdate));
						$getSwipqAnswerid =  $db->lastInsertId();
					endif;
					#------FEEDBACK-DATA---
					$feed_audio = '';
					if ( ! empty($swip_feedback_audio[$swipqi])):
						$feed_audio = $swip_feedback_audio[$swipqi];
					elseif ( ! empty($swip_feedback_rec_audio[$swipqi])):
						$feed_audio = $swip_feedback_rec_audio[$swipqi];
					endif;
					
					$feed_video = '';
					if ( ! empty($swip_feedback_video[$swipqi])):
						$feed_video = $swip_feedback_video[$swipqi];
					elseif ( ! empty($swip_feedback_rec_video[$swipqi])):
						$feed_video = $swip_feedback_rec_video[$swipqi];
					endif;
					
					#--------Feedback----------
					$cfeedData = array('qid'			=> $ques_id,
									   'aid'			=> ( ! empty($getSwipqAnswerid)) ? $getSwipqAnswerid : '',
									   'feedback'		=> ( ! empty($swip_cfeedback[$swipqi])) ? htmlspecialchars($swip_cfeedback[$swipqi]) : '',
									   'ftype'			=> 1,
									   'speech_text'	=> ( ! empty($swip_feedback_text_to_speech[$swipqi])) ? htmlspecialchars($swip_feedback_text_to_speech[$swipqi]) : '',
									   'audio'			=> ( ! empty($feed_audio)) ? $feed_audio : '',
									   'video'			=> ( ! empty($feed_video)) ? $feed_video : '',
									   'screen'			=> ( ! empty($swip_feedback_rec_screen[$swipqi])) ? $swip_feedback_rec_screen[$swipqi] : '',
									   'image'			=> ( ! empty($swip_feedback_img[$swipqi])) ? $swip_feedback_img[$swipqi] : '',
									   'document'		=> ( ! empty($swip_feedback_doc[$swipqi])) ? $swip_feedback_doc[$swipqi] : '');
					$cfeed_sql  = "INSERT INTO `feedback_tbl` (`question_id`, `answer_id`, `feedback`, `feedback_type`, `feed_speech_text`, `feed_audio`, `feed_video`, `feed_screen`, `feed_image`, `feed_document`) VALUES (:qid, :aid, :feedback, :ftype, :speech_text, :audio, :video, :screen, :image, :document)";
					$cfeed_stmt = $db->prepare($cfeed_sql);
					$cfeed_stmt->execute($cfeedData);
					$swipqi++; $swiptrue++;
				endforeach;
			endif;
		//-----------END-SWIPING----------
		
		//-----------START-DDQ----------
		elseif ($question_type == 8):
			#-------QUESTIONS-DATA---
			$qaudio = '';
			if ( ! empty($ddqq_qaudio)):
				$qaudio = $ddqq_qaudio;
			elseif ( ! empty($ddqq_rec_audio)):
				$qaudio = $ddqq_rec_audio;
			endif;
		
			$audio = '';
			if ( ! empty($ddq_audio)):
				$audio = $ddq_audio;
			elseif ( ! empty($ddq_rec_audio)):
				$audio = $ddq_rec_audio;
			endif;
			
			$video = '';
			if ( ! empty($ddq_video)):
				$video = $ddq_video;
			elseif ( ! empty($ddq_rec_video)):
				$video = $ddq_rec_video;
			endif;
			
			
			#---DROP-OPTION---- 
			$quesData = array('questions'		=> ( ! empty($ddq)) ? htmlspecialchars($ddq) : '',
							  'seleted_drag' 	=> 2,
							  'qtype'			=> ( ! empty($question_type)) ? $question_type : '',
							  'qaudio'			=> ( ! empty($qaudio)) ? $qaudio : '',
							  'qspeech_text'	=> ( ! empty($ddqq_text_to_speech)) ? htmlspecialchars($ddqq_text_to_speech) : '',
							  'speech_text'		=> ( ! empty($ddq_text_to_speech)) ? htmlspecialchars($ddq_text_to_speech) : '',
							  'audio'			=> ( ! empty($audio)) ? $audio : '',
							  'video'			=> ( ! empty($video)) ? $video : '',
							  'screen'			=> ( ! empty($ddq_rec_screen)) ? $ddq_rec_screen : '',
							  'image'			=> ( ! empty($ddq_img)) ? $ddq_img : '',
							  'document'		=> ( ! empty($ddq_doc)) ? $ddq_doc : '',
							  'media_file'		=> ( ! empty($videoq_media_file)) ? $videoq_media_file : '',
							  'shuffle'			=> (isset($ddq_shuffle2) && ! empty($ddq_shuffle2)) ? 1 : 0,
							  'true_options'	=> ( ! empty($ddq_true_option)) ? $ddq_true_option : '',
							  'ques_val_1'		=> ( ! empty($ddq_ques_val_1)) ? $ddq_ques_val_1 : '',
							  'ques_val_2'		=> ( ! empty($ddq_ques_val_2)) ? $ddq_ques_val_2 : '',
							  'ques_val_3'		=> ( ! empty($ddq_ques_val_3)) ? $ddq_ques_val_3 : '',
							  'ques_val_4'		=> ( ! empty($ddq_ques_val_4)) ? $ddq_ques_val_4 : '',
							  'ques_val_5'		=> ( ! empty($ddq_ques_val_5)) ? $ddq_ques_val_5 : '',
							  'ques_val_6'		=> ( ! empty($ddq_ques_val_6)) ? $ddq_ques_val_6 : '',
							  'critical'		=> ( ! empty($dnd_criticalQ)) ? $dnd_criticalQ : '',
							  'status' 			=> 1,
							  'user_id'			=> $userId,
							  'cur_date'		=> $curdate,
							  'qid'				=> $ques_id);
			$ques_sql  = "UPDATE `question_tbl` SET `questions`=:questions, `question_type`=:qtype, `seleted_drag_option`=:seleted_drag, `qaudio`=:qaudio, `qspeech_text`=:qspeech_text, `speech_text`=:speech_text, `audio`=:audio, `video`=:video, `screen`=:screen, `image`=:image, `document`=:document, `shuffle`=:shuffle, `videoq_media_file`=:media_file, `true_options`=:true_options, `ques_val_1`=:ques_val_1, `ques_val_2`=:ques_val_2, `ques_val_3`=:ques_val_3, `ques_val_4`=:ques_val_4, `ques_val_5`=:ques_val_5, `ques_val_6`=:ques_val_6, `critical`=:critical, `status`=:status, `uid`=:user_id, `cur_date`=:cur_date WHERE question_id=:qid";
			$ques_stmt = $db->prepare($ques_sql);
			$ques_stmt->execute($quesData);
				
			#-----Remove-Old-Feedback------
			$db->feedback_remove($ques_id);
			
			#------DRAG-OPTIONS------
			if ( ! empty($ddq2_drop)):
				if ( ! empty($updateDDq2Answerid)):
					#----DROP-TARGET----
					$ansSql = "UPDATE `sub_options_tbl` SET `sub_option`=:option, `image`=:image WHERE sub_options_id=:sid";
					$stmt	= $db->prepare($ansSql);
					$stmt->execute(array('sid' => $updateDDq2Answerid, 'option' => ( ! empty($ddq2_drop)) ? htmlspecialchars($ddq2_drop) : '', 'image' => ( ! empty($ddq2_item_img)) ? $ddq2_item_img : ''));
					
					#-----DRAG-ITEMS------
					$ddq2iu = 0; $ddq2true = 1;
					foreach ($ddq2_drag as $subAnswerData):
						if ( ! empty($updateDDq2SubAnswerid[$ddq2iu])):
							$getOpId = $updateDDq2SubAnswerid[$ddq2iu];
							$updateAnsSql  = "UPDATE `answer_tbl` SET `choice_option`=:option, `image`=:image, `ans_val1`=:ans_val1, `ans_val2`=:ans_val2, `ans_val3`=:ans_val3, `ans_val4`=:ans_val4, `ans_val5`=:ans_val5, `ans_val6`=:ans_val6, `true_option`=:true, `status`=:status, `uid`=:user_id, `cur_date`=:cur_date WHERE answer_id=:aid";
							$updateAnsstmt = $db->prepare($updateAnsSql);
							$updateAnsstmt->execute(array('aid' 		=> $getOpId,
														  'option'		=> ( ! empty($ddq2_drag[$ddq2iu])) ? htmlspecialchars($ddq2_drag[$ddq2iu]) : '',
														  'image'		=> ( ! empty($ddq2_drag_item_img[$ddq2iu])) ? $ddq2_drag_item_img[$ddq2iu] : '',
														  'ans_val1'	=> ( ! empty($dd2_ans_val1[$ddq2iu])) ? $dd2_ans_val1[$ddq2iu] : '',
														  'ans_val2'	=> ( ! empty($dd2_ans_val2[$ddq2iu])) ? $dd2_ans_val2[$ddq2iu] : '',
														  'ans_val3'	=> ( ! empty($dd2_ans_val3[$ddq2iu])) ? $dd2_ans_val3[$ddq2iu] : '',
														  'ans_val4'	=> ( ! empty($dd2_ans_val4[$ddq2iu])) ? $dd2_ans_val4[$ddq2iu] : '',
														  'ans_val5'	=> ( ! empty($dd2_ans_val5[$ddq2iu])) ? $dd2_ans_val5[$ddq2iu] : '',
														  'ans_val6'	=> ( ! empty($dd2_ans_val6[$ddq2iu])) ? $dd2_ans_val6[$ddq2iu] : '',
														  'true'		=> ( ! empty($ddq_true_option) && $ddq_true_option == $ddq2true) ? 1 : '',
														  'status'		=> 1,
														  'user_id' 	=> $userId,
														  'cur_date'	=> $curdate));
						else:
							$ansSql = "INSERT INTO `answer_tbl` (`question_id`, `choice_option`, `image`, `ans_val1`, `ans_val2`, `ans_val3`, `ans_val4`, `ans_val5`, `ans_val6`, `true_option`, `status`, `uid`, `cur_date`) values (:qid, :option, :image, :ans_val1, :ans_val2, :ans_val3, :ans_val4, :ans_val5, :ans_val6, :true, :status, :user_id, :cur_date)";
							$stmt 	= $db->prepare($ansSql);
							$stmt->execute(array('qid'		=> $ques_id,
												 'option'	=> ( ! empty($ddq2_drag[$ddq2iu])) ? htmlspecialchars($ddq2_drag[$ddq2iu]) : '',
												 'image'	=> ( ! empty($ddq2_drag_item_img[$ddq2iu])) ? $ddq2_drag_item_img[$ddq2iu] : '',
												 'ans_val1'	=> ( ! empty($dd2_ans_val1[$ddq2iu])) ? $dd2_ans_val1[$ddq2iu] : '',
												 'ans_val2'	=> ( ! empty($dd2_ans_val2[$ddq2iu])) ? $dd2_ans_val2[$ddq2iu] : '',
												 'ans_val3'	=> ( ! empty($dd2_ans_val3[$ddq2iu])) ? $dd2_ans_val3[$ddq2iu] : '',
												 'ans_val4'	=> ( ! empty($dd2_ans_val4[$ddq2iu])) ? $dd2_ans_val4[$ddq2iu] : '',
												 'ans_val5'	=> ( ! empty($dd2_ans_val5[$ddq2iu])) ? $dd2_ans_val5[$ddq2iu] : '',
												 'ans_val6'	=> ( ! empty($dd2_ans_val6[$ddq2iu])) ? $dd2_ans_val6[$ddq2iu] : '',
												 'true'		=> ( ! empty($ddq_true_option) && $ddq_true_option == $ddq2true) ? 1 : '',
												 'status'	=> 1,
												 'user_id' 	=> $userId,
												 'cur_date'	=> $curdate));
							$getOpId = $db->lastInsertId();
						endif;
						
						#-----FEEDBACK-DATA---
						$feed_audio = '';
						if ( ! empty($dd2_feedback_audio[$ddq2iu])):
							$feed_audio = $dd2_feedback_audio[$ddq2iu];
						elseif ( ! empty($dd2_feedback_rec_audio[$ddq2iu])):
							$feed_audio = $dd2_feedback_rec_audio[$ddq2iu];
						endif;
						
						$feed_video = '';
						if ( ! empty($dd2_feedback_video[$ddq2iu])):
							$feed_video = $dd2_feedback_video[$ddq2iu];
						elseif ( ! empty($dd2_feedback_rec_video[$ddq2iu])):
							$feed_video = $dd2_feedback_rec_video[$ddq2iu];
						endif;
						
						#--------Feedback----------
						$cfeedData = array('qid'			=> ( ! empty($ques_id)) ? $ques_id : '',
										   'aid'			=> ( ! empty($getOpId)) ? $getOpId : '',
										   'feedback'		=> ( ! empty($dd2_cfeedback[$ddq2iu])) ? htmlspecialchars($dd2_cfeedback[$ddq2iu]) : '',
										   'ftype'			=> 1,
										   'speech_text'	=> ( ! empty($dd2_feedback_text_to_speech[$ddq2iu])) ? htmlspecialchars($dd2_feedback_text_to_speech[$ddq2iu]) : '',
										   'audio'			=> ( ! empty($feed_audio)) ? $feed_audio : '',
										   'video'			=> ( ! empty($feed_video)) ? $feed_video : '',
										   'screen'			=> ( ! empty($dd2_feedback_rec_screen[$ddq2iu])) ? $dd2_feedback_rec_screen[$ddq2iu] : '',
										   'image'			=> ( ! empty($dd2_feedback_img[$ddq2iu])) ? $dd2_feedback_img[$ddq2iu] : '',
										   'document'		=> ( ! empty($dd2_feedback_doc[$ddq2iu])) ? $dd2_feedback_doc[$ddq2iu] : '');
						$cfeed_sql  = "INSERT INTO `feedback_tbl` (`question_id`, `answer_id`, `feedback`, `feedback_type`, `feed_speech_text`, `feed_audio`, `feed_video`, `feed_screen`, `feed_image`, `feed_document`) VALUES (:qid, :aid, :feedback, :ftype, :speech_text, :audio, :video, :screen, :image, :document)";
						$cfeed_stmt = $db->prepare($cfeed_sql);
						$cfeed_stmt->execute($cfeedData);
						$ddq2iu++; $ddq2true++;
					endforeach;
				else:					
					#----DROP-TARGET----
					$ansSql = "INSERT INTO `sub_options_tbl` (`question_id`, `sub_option`, `image`) VALUES (:qid, :option, :image)";
					$stmt 	= $db->prepare($ansSql);
					$stmt->execute(array('qid' => $ques_id, 'option' => ( ! empty($ddq2_drop)) ? htmlspecialchars($ddq2_drop) : '', 'image' => ( ! empty($ddq2_item_img)) ? $ddq2_item_img : ''));
					
					#-----DRAG-ITEMS------
					$ddq2i = 0; $ddq2itrue = 1;
					foreach ($ddq2_drag as $answerData):
						$ansSql = "INSERT INTO `answer_tbl` (`question_id`, `choice_option`, `image`, `ans_val1`, `ans_val2`, `ans_val3`, `ans_val4`, `ans_val5`, `ans_val6`, `true_option`, `status`, `uid`, `cur_date`) VALUES (:qid, :choice, :image, :ans_val1, :ans_val2, :ans_val3, :ans_val4, :ans_val5, :ans_val6, :true, :status, :user_id, :cur_date)";
						$stmt 	= $db->prepare($ansSql);
						$stmt->execute(array('qid'		=> $ques_id,
											 'choice'	=> ( ! empty($ddq2_drag[$ddq2i])) ? htmlspecialchars($ddq2_drag[$ddq2i]) : '',
											 'image'	=> ( ! empty($ddq2_drag_item_img[$ddq2i])) ? $ddq2_drag_item_img[$ddq2i] : '',
											 'ans_val1'	=> ( ! empty($dd2_ans_val1[$ddq2i])) ? $dd2_ans_val1[$ddq2i] : '',
											 'ans_val2'	=> ( ! empty($dd2_ans_val2[$ddq2i])) ? $dd2_ans_val2[$ddq2i] : '',
											 'ans_val3'	=> ( ! empty($dd2_ans_val3[$ddq2i])) ? $dd2_ans_val3[$ddq2i] : '',
											 'ans_val4'	=> ( ! empty($dd2_ans_val4[$ddq2i])) ? $dd2_ans_val4[$ddq2i] : '',
											 'ans_val5'	=> ( ! empty($dd2_ans_val5[$ddq2i])) ? $dd2_ans_val5[$ddq2i] : '',
											 'ans_val6'	=> ( ! empty($dd2_ans_val6[$ddq2i])) ? $dd2_ans_val6[$ddq2i] : '',
											 'true'		=> ( ! empty($ddq_true_option) && $ddq_true_option == $ddq2itrue) ? 1 : '',
											 'status'	=> 1,
											 'user_id' 	=> $userId,
											 'cur_date'	=> $curdate));
						$ans_id = $db->lastInsertId();

						#-----FEEDBACK-DATA---
						$feed_audio = '';
						if ( ! empty($dd2_feedback_audio[$ddq2i])):
							$feed_audio = $dd2_feedback_audio[$ddq2i];
						elseif ( ! empty($dd2_feedback_rec_audio[$ddq2i])):
							$feed_audio = $dd2_feedback_rec_audio[$ddq2i];
						endif;
						
						$feed_video = '';
						if ( ! empty($dd2_feedback_video[$ddq2i])):
							$feed_video = $dd2_feedback_video[$ddq2i];
						elseif ( ! empty($dd2_feedback_rec_video[$ddq2i])):
							$feed_video = $dd2_feedback_rec_video[$ddq2i];
						endif;
						
						#--------Feedback----------
						$cfeedData = array('qid'			=> ( ! empty($ques_id)) ? $ques_id : '',
										   'aid'			=> ( ! empty($ans_id)) ? $ans_id : '',
										   'feedback'		=> ( ! empty($dd2_cfeedback[$ddq2i])) ? htmlspecialchars($dd2_cfeedback[$ddq2i]) : '',
										   'ftype'			=> 1,
										   'speech_text'	=> ( ! empty($dd2_feedback_text_to_speech[$ddq2i])) ? htmlspecialchars($dd2_feedback_text_to_speech[$ddq2i]) : '',
										   'audio'			=> ( ! empty($feed_audio)) ? $feed_audio : '',
										   'video'			=> ( ! empty($feed_video)) ? $feed_video : '',
										   'screen'			=> ( ! empty($dd2_feedback_rec_screen[$ddq2i])) ? $dd2_feedback_rec_screen[$ddq2i] : '',
										   'image'			=> ( ! empty($dd2_feedback_img[$ddq2i])) ? $dd2_feedback_img[$ddq2i] : '',
										   'document'		=> ( ! empty($dd2_feedback_doc[$ddq2i])) ? $dd2_feedback_doc[$ddq2i] : '');
						$cfeed_sql  = "INSERT INTO `feedback_tbl` (`question_id`, `answer_id`, `feedback`, `feedback_type`, `feed_speech_text`, `feed_audio`, `feed_video`, `feed_screen`, `feed_image`, `feed_document`) VALUES (:qid, :aid, :feedback, :ftype, :speech_text, :audio, :video, :screen, :image, :document)";
						$cfeed_stmt = $db->prepare($cfeed_sql);
						$cfeed_stmt->execute($cfeedData);
						$ddq2i++; $ddq2itrue++;
					endforeach;
				endif;
			endif;
		endif;
	endif;
	if ( ! empty($submit_type) && $submit_type == 1):
		$resdata = array('success' => TRUE, 'all_update' => FALSE, 'msg' => 'Simulation details updated successfully.', 'return_url' => 'linear-classic-video-template.php?add_data=true&sim_id='.md5($sim_id).'&ques_id='.base64_encode($ques_id));
	else:
		$resdata = array('success' => TRUE, 'all_update' => TRUE, 'msg' => 'All questions and answers updated successfully.', 'return_url' => 'author-simulation.php');
	endif;
	echo json_encode($resdata);
endif;

#-----------------------------------------Update-Branching Classic Sim - Saumya ----------------------------------
if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['update_branching_classic_sim']) && isset($_POST['sim_id'])):
	extract($_POST);
	$error = FALSE;
	if (empty($sim_page_name[0]) || empty($sim_duration) || empty($sim_title) || empty($passing_marks)):
		$resdata = array('success' => FALSE, 'msg' => 'Please fill all the mandatory fields on add Simulation and Simulation details to proceed.');
		$error = TRUE;
		echo json_encode($resdata); exit;
	elseif (isset($competency_name_1) && empty($competency_name_1) || empty($competency_score_1) || empty($weightage_1)):
		$resdata = array('success' => FALSE, 'msg' => 'Please fill all the mandatory fields on Simulation details to proceed.');
		$error = TRUE;
		echo json_encode($resdata); exit;
	elseif ($error == FALSE):
		$sql = "UPDATE scenario_master SET `Scenario_title` = '". $sim_title ."', `duration` = '". $sim_duration ."', `passing_marks` = '". $passing_marks ."' , `sim_cover_img` = '". $scenario_cover_file ."'";
		if (isset($character_type) && $character_type == 1):
			$sql .= ", sim_char_img = '', sim_char_left_img = '', sim_char_right_img = '', char_own_choice = ''";
		elseif (isset($charoption) && $charoption == 1 && isset($scenario_char_file) && ! empty($scenario_char_file)):
			$sql .= ", char_own_choice = '". $scenario_char_file ."', sim_char_img = '', sim_char_left_img = '', sim_char_right_img = ''";
		elseif (isset($charoption) && $charoption == 2 && isset($sim_char_img) && ! empty($sim_char_img)):
			$sql .= ", sim_char_img = '". $sim_char_img ."', sim_char_left_img = '', sim_char_right_img = ''";
		elseif (isset($sim_two_char_img) && count($sim_two_char_img) > 0):
			if (isset($sim_two_char_img[0])):
				$r = explode('|', $sim_two_char_img[0]);
				if (isset($r[1]) && $r[1] == 'R'):
					$sql .= ", sim_char_img = '', char_own_choice = '', sim_char_right_img = '". $r[0] ."'";
				elseif (isset($r[1]) && $r[1] == 'L'):
					$sql .= ", sim_char_img = '', char_own_choice = '', sim_char_left_img = '". $r[0] ."'";
				endif;
			endif;
			if (isset($sim_two_char_img[1])):
				$l = explode('|', $sim_two_char_img[1]);
				if (isset($l[1]) && $l[1] == 'L'):
					$sql .= ", sim_char_img = '', char_own_choice = '', sim_char_left_img = '". $l[0] ."'";
				elseif (isset($l[1]) && $l[1] == 'R'):
					$sql .= ", sim_char_img = '', char_own_choice = '', sim_char_right_img = '". $l[0] ."'";
				endif;
			endif;
		endif;
		#--Add-Web-Object--
		if ( ! empty($web_object)):
			$sql .= ", web_object = '". $web_object ."', web_object_title = '". htmlspecialchars($web_object_title) ."'";
		endif;
		#--Sim-Media-File--
		if ( ! empty($scenario_media_file) && in_array(strtolower(pathinfo($scenario_media_file, PATHINFO_EXTENSION)), $img_valid_exts)):
			$sql .= ", image_url = '". $scenario_media_file ."', video_url = NULL, audio_url = NULL";
		elseif ( ! empty($scenario_media_file) && in_array(strtolower(pathinfo($scenario_media_file, PATHINFO_EXTENSION)), $video_valid_exts)):
			$sql .= ", video_url = '". $scenario_media_file ."', image_url = NULL, audio_url = NULL";
		elseif ( ! empty($scenario_media_file) && in_array(strtolower(pathinfo($scenario_media_file, PATHINFO_EXTENSION)), $audio_valid_exts)):
			$sql .= ", audio_url = '". $scenario_media_file ."', video_url = NULL, image_url = NULL";
		elseif ( ! empty($scenario_media_file) && ! in_array(strtolower(pathinfo($scenario_media_file, PATHINFO_EXTENSION)), $audio_valid_exts) && 
				 ! in_array(strtolower(pathinfo($scenario_media_file, PATHINFO_EXTENSION)), $img_valid_exts) && 
				 ! in_array(strtolower(pathinfo($scenario_media_file, PATHINFO_EXTENSION)), $video_valid_exts)):
			$sql .= ", web_object_file = '". $scenario_media_file ."', video_url = NULL, image_url = NULL, audio_url = NULL";
		endif;
		#--Sim-Backgrounds--
		if (isset($bgoption) && $bgoption == 1 && ! empty($bg_color)):
			$sql .= ", bg_color = '". $bg_color ."', bg_own_choice = '', sim_back_img = ''";
		elseif (isset($bgoption) && $bgoption == 2 && ! empty($scenario_bg_file)):
			$sql .= ", bg_own_choice = '". $scenario_bg_file ."', bg_color = '', sim_back_img = ''";
		elseif (isset($bgoption) && $bgoption == 3 && ! empty($sim_background_img)):
			$sql .= ", sim_back_img = '". $sim_background_img ."', bg_own_choice = '', bg_color = ''";
		endif;
		$sql .= " WHERE scenario_id = '". $sim_id ."'";
		$results = $db->prepare($sql);
		if ($results->execute()):
			#----Competency----
			$delCom	 = "DELETE FROM competency_tbl WHERE scenario_id = '". $sim_id ."'";
			$delComq = $db->prepare($delCom); $delComq->execute();
			$data	 = array('scenario_id'	=> $sim_id,
							 'comp_col_1'	=> (isset($competency_name_1) && ! empty($competency_name_1)) ? addslashes($competency_name_1) : '',
							 'comp_val_1'	=> (isset($competency_score_1) && ! empty($competency_score_1)) ? $competency_score_1 : '',
							 'comp_col_2'	=> (isset($competency_name_2) && ! empty($competency_name_2)) ? addslashes($competency_name_2) : '',
							 'comp_val_2'	=> (isset($competency_score_2) && ! empty($competency_score_2)) ? $competency_score_2 : '',
							 'comp_col_3'	=> (isset($competency_name_3) && ! empty($competency_name_3)) ? addslashes($competency_name_3) : '',
							 'comp_val_3'	=> (isset($competency_score_3) && ! empty($competency_score_3)) ? $competency_score_3 : '',
							 'comp_col_4'	=> (isset($competency_name_4) && ! empty($competency_name_4)) ? addslashes($competency_name_4) : '',
							 'comp_val_4'	=> (isset($competency_score_4) && ! empty($competency_score_4)) ? $competency_score_4 : '',
							 'comp_col_5'	=> (isset($competency_name_5) && ! empty($competency_name_5)) ? addslashes($competency_name_5) : '',
							 'comp_val_5'	=> (isset($competency_score_5) && ! empty($competency_score_5)) ? $competency_score_5 : '',
							 'comp_col_6'	=> (isset($competency_name_6) && ! empty($competency_name_6)) ? addslashes($competency_name_6) : '',
							 'comp_val_6'	=> (isset($competency_score_6) && ! empty($competency_score_6)) ? $competency_score_6 : '',
							 'weightage_1'	=> (isset($weightage_1) && ! empty($weightage_1)) ? $weightage_1 : '',
							 'weightage_2'	=> (isset($weightage_2) && ! empty($weightage_2)) ? $weightage_2 : '',
							 'weightage_3'	=> (isset($weightage_3) && ! empty($weightage_3)) ? $weightage_3 : '',
							 'weightage_4'	=> (isset($weightage_4) && ! empty($weightage_4)) ? $weightage_4 : '',
							 'weightage_5'	=> (isset($weightage_5) && ! empty($weightage_5)) ? $weightage_5 : '',
							 'weightage_6'	=> (isset($weightage_6) && ! empty($weightage_6)) ? $weightage_6 : '',
							 'status' 		=> 1,
							 'user_id' 		=> $userId,
							 'cur_date'		=> $curdate);
			$com_sql  = "INSERT INTO `competency_tbl` (`scenario_id`, `comp_col_1`, `comp_val_1`, `comp_col_2`, `comp_val_2`, `comp_col_3`, `comp_val_3`, `comp_col_4`, `comp_val_4`, `comp_col_5`, `comp_val_5`, `comp_col_6`, `comp_val_6`, `weightage_1`, `weightage_2`, `weightage_3`, `weightage_4`, `weightage_5`, `weightage_6`, `status`, `uid`, `cur_date`) VALUES (:scenario_id, :comp_col_1, :comp_val_1, :comp_col_2, :comp_val_2, :comp_col_3, :comp_val_3, :comp_col_4, :comp_val_4, :comp_col_5, :comp_val_5, :comp_col_6, :comp_val_6, :weightage_1, :weightage_2, :weightage_3, :weightage_4, :weightage_5, :weightage_6, :status, :user_id, :cur_date)";
			$com_stmt = $db->prepare($com_sql);
			$com_stmt->execute($data);
			
			#----PAGES----
			$delPage  = "DELETE FROM sim_pages WHERE scenario_id = '". $sim_id ."'";
			$delPageq = $db->prepare($delPage); $delPageq->execute();
			if ( ! empty($sim_page_name)): $sp = 0;
				foreach ($sim_page_name as $pdata):
					$pageq = "INSERT INTO `sim_pages` (`scenario_id`, `sim_page_name`, `sim_page_desc`, `userid`, `status`, `added_date`) VALUES (:sid, :pname, :pdesc, :user_id, :status, :adate)";
					$page_stmt = $db->prepare($pageq);
					$page_stmt->execute(array('sid'		=> $sim_id,
											  'pname'	=> ( ! empty($pdata)) ? htmlspecialchars($pdata) : '',
											  'pdesc'	=> ( ! empty($sim_page_desc[$sp])) ? $sim_page_desc[$sp] : '',
											  'status' 	=> 1,
											  'user_id'	=> $userId,
											  'adate'	=> $curdate));
				$sp++; endforeach;
			endif;
			#----Branding----
			$bandq	  	= "DELETE FROM sim_branding_tbl WHERE scenario_id = '". $sim_id ."'";
			$bandqexe 	= $db->prepare($bandq); $bandqexe->execute();
			$brand_data	= array('sid'				=> $sim_id,
								'bg'				=> ( ! empty($ques_bg_color)) ? $ques_bg_color : '',
								'bg_tr'				=> ( ! empty($ques_bg_transp)) ? $ques_bg_transp : '',
								'obg'				=> ( ! empty($ques_option_bg_color)) ? $ques_option_bg_color : '',
								'obg_tr'			=> ( ! empty($ques_option_bg_transp)) ? $ques_option_bg_transp : '',
								'ohover'			=> ( ! empty($ques_option_hover_color)) ? $ques_option_hover_color : '',
								'oh_tr'				=> ( ! empty($ques_option_hover_transp)) ? $ques_option_hover_transp : '',
								'oselected'			=> ( ! empty($ques_option_select_color)) ? $ques_option_select_color : '',
								'oselected_tr'		=> ( ! empty($ques_option_select_transp)) ? $ques_option_select_transp : '',
								'btn_bg'			=> ( ! empty($btn_bg_color)) ? $btn_bg_color : '',
								'btn_bg_tr'			=> ( ! empty($btn_bg_transp)) ? $btn_bg_transp : '',
								'btn_hover'			=> ( ! empty($btn_hover_color)) ? $btn_hover_color : '',
								'btn_hover_tr'		=> ( ! empty($btn_hover_transp)) ? $btn_hover_transp : '',
								'btn_selected'		=> ( ! empty($btn_select_color)) ? $btn_select_color : '',
								'btn_selected_tr'	=> ( ! empty($btn_select_transp)) ? $btn_select_transp : '',
								'font_type'			=> ( ! empty($font_type)) ? $font_type : '',
								'font_color'		=> ( ! empty($font_color)) ? $font_color : '',
								'font_size'			=> ( ! empty($font_size)) ? $font_size : '',
								'user_id' 			=> $userId,
								'added_date'		=> $curdate);
			$com_sql  = "INSERT INTO `sim_branding_tbl` (`scenario_id`, `ques_bg`, `ques_bg_transparency`, `option_bg`, `option_bg_transparency`, `option_hover`, `option_hover_transparency`, `option_selected`, `option_selected_transparency`, `btn_bg`, `btn_bg_transparency`, `btn_hover`, `btn_hover_transparency`, `btn_selected`, `btn_selected_transparency`, `font_type`, `font_color`, `font_size`, `user_id`, `added_date`) 
						 VALUES (:sid, :bg, :bg_tr, :obg, :obg_tr, :ohover, :oh_tr, :oselected, :oselected_tr, :btn_bg, :btn_bg_tr, :btn_hover, :btn_hover_tr, :btn_selected, :btn_selected_tr, :font_type, :font_color, :font_size, :user_id, :added_date)";
			$com_stmt = $db->prepare($com_sql); $com_stmt->execute($brand_data);
		endif;
		//Update MCQ Question
		if ( $question_type == 4):
			#-------QUESTIONS-DATA---
			$qaudio = '';
			if ( ! empty($mcqq_qaudio)):
				$qaudio = $mcqq_qaudio;
			elseif ( ! empty($mcqq_rec_audio)):
				$qaudio = $mcqq_rec_audio;
			endif;
			
			$audio = '';
			if ( ! empty($mcq_audio)):
				$audio = $mcq_audio;
			elseif ( ! empty($mcq_rec_audio)):
				$audio = $mcq_rec_audio;
			endif;
			
			$video = '';
			if ( ! empty($mcq_video)):
				$video = $mcq_video;
			elseif ( ! empty($mcq_rec_video)):
				$video = $mcq_rec_video;
			endif;
			$quesData	= array('questions'		=> ( ! empty($mcq)) ? $mcq : '',
								'qtype'			=> ( ! empty($question_type)) ? $question_type : '',
								'qaudio'		=> ( ! empty($qaudio)) ? $qaudio : '',
								'qspeech_text'	=> ( ! empty($mcqq_text_to_speech)) ? $mcqq_text_to_speech : '',
								'speech_text'	=> ( ! empty($mcq_text_to_speech)) ? $mcq_text_to_speech : '',
								'audio'			=> ( ! empty($audio)) ? $audio : '',
								'video'			=> ( ! empty($video)) ? $video : '',
								'screen'		=> ( ! empty($mcq_rec_screen)) ? $mcq_rec_screen : '',
								'image'			=> ( ! empty($mcq_img)) ? $mcq_img : '',
								'document'		=> ( ! empty($mcq_doc)) ? $mcq_doc : '',
								'true_option'	=> ( ! empty($mcq_true_option)) ? $mcq_true_option : '',
								'shuffle'		=> (isset($mcq_shuffle) && ! empty($mcq_shuffle)) ? 1 : 0,
								'ques_val_1'	=> ( ! empty($mcq_ques_val_1)) ? $mcq_ques_val_1 : '',
								'ques_val_2'	=> ( ! empty($mcq_ques_val_2)) ? $mcq_ques_val_2 : '',
								'ques_val_3'	=> ( ! empty($mcq_ques_val_3)) ? $mcq_ques_val_3 : '',
								'ques_val_4'	=> ( ! empty($mcq_ques_val_4)) ? $mcq_ques_val_4 : '',
								'ques_val_5'	=> ( ! empty($mcq_ques_val_5)) ? $mcq_ques_val_5 : '',
								'ques_val_6'	=> ( ! empty($mcq_ques_val_6)) ? $mcq_ques_val_6 : '',
								'critical'		=> ( ! empty($mcq_criticalQ)) ? $mcq_criticalQ : '', 
								'status' 		=> 1,
								'user_id'		=> $userId,
								'cur_date'		=> $curdate,
								'qid'			=> $ques_id);
			$ques_sql	= "UPDATE `question_tbl` SET `questions`=:questions, `question_type`=:qtype, `qaudio`=:qaudio, `qspeech_text`=:qspeech_text, `speech_text`=:speech_text, `audio`=:audio, `video`=:video, `screen`=:screen, `image`=:image, `document`=:document, `true_options`=:true_option, `shuffle`=:shuffle, `ques_val_1`=:ques_val_1, `ques_val_2`=:ques_val_2, `ques_val_3`=:ques_val_3, `ques_val_4`=:ques_val_4, `ques_val_5`=:ques_val_5, `ques_val_6`=:ques_val_6, `critical`=:critical,`status`=:status, `uid`=:user_id, `cur_date`=:cur_date WHERE question_id=:qid";
			$ques_stmt	= $db->prepare($ques_sql);
			#----------ADD-OPTIONS-------------
			if ($ques_stmt->execute($quesData)):
				#-----Remove-Old-Feedback-------
				$db->feedback_remove($ques_id);
				$mcqi = 0; $mcqtrue = 1;
				if ( ! empty($mcq_option[0])):
					foreach ($mcq_option as $answerData):
						if ( ! empty($updateMCQAnswerid[$mcqi])):
							if (isset($endSimulationMTF) && in_array($mcqtrue, $endSimulationMTF)) {
								$endSim	= 1;
							} else {
								$endSim	= 0;
							}
							if ($endSim > 0){
								$mappedQuestionMtf[$mcqi] = '';
							}
							$ans_id	= $updateMCQAnswerid[$mcqi];
							$ansSql	= "UPDATE `answer_tbl` SET `choice_option`=:option, `End_Sim`=:End_Sim,`nextQid`=:nextQid,`ans_val1`=:val1, `ans_val2`=:val2, `ans_val3`=:val3, `ans_val4`=:val4, `ans_val5`=:val5, `ans_val6`=:val6, `true_option`=:true_option, `status`=:status, `uid`=:user_id, `cur_date`=:cur_date WHERE answer_id=:aid";
							$stmt	= $db->prepare($ansSql);
							$stmt->execute(array('aid'			=> $ans_id,
												 'option'		=> ( ! empty($answerData)) ? $answerData : '',
												 'End_Sim'		=> ( ! empty($endSim)) ? addslashes($endSim) : '',
												 'nextQid'		=> ( ! empty($mappedQuestionMtf[$mcqi])) ? addslashes($mappedQuestionMtf[$mcqi]) : '',
												 'val1'			=> ( ! empty($mcq_ans_val1[$mcqi])) ? $mcq_ans_val1[$mcqi] : '',
												 'val2'			=> ( ! empty($mcq_ans_val2[$mcqi])) ? $mcq_ans_val2[$mcqi] : '',
												 'val3'			=> ( ! empty($mcq_ans_val3[$mcqi])) ? $mcq_ans_val3[$mcqi] : '',
												 'val4'			=> ( ! empty($mcq_ans_val4[$mcqi])) ? $mcq_ans_val4[$mcqi] : '',
												 'val5'			=> ( ! empty($mcq_ans_val5[$mcqi])) ? $mcq_ans_val5[$mcqi] : '',
												 'val6'			=> ( ! empty($mcq_ans_val6[$mcqi])) ? $mcq_ans_val6[$mcqi] : '',
												 'true_option'	=> ( ! empty($mcq_true_option)) ? $mcq_true_option : '',
												 'status'		=> 1,
												 'user_id' 		=> $userId,
												 'cur_date'		=> $curdate));
						else:
							if (isset($endSimulationMTF) && in_array($mcqtrue, $endSimulationMTF)) {
								$endSim	= 1;
							} else {
								$endSim	= 0;
							}
							if ($endSim > 0){
								$mappedQuestionMtf[$mcqi] = '';
							}
							$ansSql	= "INSERT INTO `answer_tbl` (`question_id`, `choice_option`,`End_Sim`, `nextQid`,`ans_val1`, `ans_val2`, `ans_val3`, `ans_val4`, `ans_val5`, `ans_val6`, `true_option`, `status`, `uid`, `cur_date`) values (:qid, :option,:End_Sim,:nextQid, :val1, :val2, :val3, :val4, :val5, :val6, :true_option, :status, :user_id, :cur_date)";
							$stmt = $db->prepare($ansSql);
							$stmt->execute(array('qid'			=> $ques_id,
												'option'		=> ( ! empty($answerData)) ? $answerData : '',
												'End_Sim'		=> ( ! empty($endSim)) ? addslashes($endSim) : '',
												'nextQid'		=> ( ! empty($mappedQuestionMtf[$mcqi])) ? addslashes($mappedQuestionMtf[$mcqi]) : '',
												'val1'			=> ( ! empty($mcq_ans_val1[$mcqi])) ? $mcq_ans_val1[$mcqi] : '',
												'val2'			=> ( ! empty($mcq_ans_val2[$mcqi])) ? $mcq_ans_val2[$mcqi] : '',
												'val3'			=> ( ! empty($mcq_ans_val3[$mcqi])) ? $mcq_ans_val3[$mcqi] : '',
												'val4'			=> ( ! empty($mcq_ans_val4[$mcqi])) ? $mcq_ans_val4[$mcqi] : '',
												'val5'			=> ( ! empty($mcq_ans_val5[$mcqi])) ? $mcq_ans_val5[$mcqi] : '',
												'val6'			=> ( ! empty($mcq_ans_val6[$mcqi])) ? $mcq_ans_val6[$mcqi] : '',
												'true_option'	=> ( ! empty($mcq_true_option)) ? $mcq_true_option : '',
												'status'		=> 1,
												'user_id' 		=> $userId,
												'cur_date'		=> $curdate));
							$ans_id = $db->lastInsertId();
						endif;
						#------Correct-FEEDBACK-DATA---
						$feed_audio = '';
						if ( ! empty($mcq_feedback_audio[$mcqi])):
							$feed_audio = $mcq_feedback_audio[$mcqi];
						elseif ( ! empty($mcq_feedback_rec_audio[$mcqi])):
							$feed_audio = $mcq_feedback_rec_audio[$mcqi];
						endif;				
						$feed_video = '';
						if ( ! empty($mcq_feedback_video[$mcqi])):
							$feed_video = $mcq_feedback_video[$mcqi];
						elseif ( ! empty($mcq_feedback_rec_video[$mcqi])):
							$feed_video = $mcq_feedback_rec_video[$mcqi];
						endif;				
						#--------Correct-Feedback----------
						$cfeedData = array('qid' 			=> ( ! empty($ques_id)) ? $ques_id : '',
										   'aid'			=> ( ! empty($ans_id)) ? $ans_id : '',
										   'feedback'		=> ( ! empty($mcq_cfeedback[$mcqi])) ? addslashes($mcq_cfeedback[$mcqi]) : '',
										   'ftype'			=> 1,
										   'speech_text'	=> ( ! empty($mcq_feedback_text_to_speech[$mcqi])) ? addslashes($mcq_feedback_text_to_speech[$mcqi]) : '',
										   'audio'			=> ( ! empty($feed_audio)) ? $feed_audio : '',
										   'video'			=> ( ! empty($feed_video)) ? $feed_video : '',
										   'screen'			=> ( ! empty($mcq_feedback_rec_screen[$mcqi])) ? $mcq_feedback_rec_screen[$mcqi] : '',
										   'image'			=> ( ! empty($mcq_feedback_img[$mcqi])) ? $mcq_feedback_img[$mcqi] : '',
										   'document'		=> ( ! empty($mcq_feedback_doc[$mcqi])) ? $mcq_feedback_doc[$mcqi] : '');
						$cfeed_sql	= "INSERT INTO `feedback_tbl` (`question_id`, `answer_id`, `feedback`, `feedback_type`, `feed_speech_text`, `feed_audio`, `feed_video`, `feed_screen`, `feed_image`, `feed_document`) VALUES (:qid, :aid, :feedback, :ftype, :speech_text, :audio, :video, :screen, :image, :document)";
						$cfeed_stmt	= $db->prepare($cfeed_sql); $cfeed_stmt->execute($cfeedData);
						$mcqi++; $mcqtrue++;
					endforeach;
				endif;
			endif;
		
		//Update DND Question
		elseif ($question_type == 8):
			#-------QUESTIONS-DATA---
			$qaudio = '';
			if ( ! empty($ddqq_qaudio)):
				$qaudio = $ddqq_qaudio;
			elseif ( ! empty($ddqq_rec_audio)):
				$qaudio = $ddqq_rec_audio;
			endif;
			$audio = '';
			if ( ! empty($ddq_audio)):
				$audio = $ddq_audio;
			elseif ( ! empty($ddq_rec_audio)):
				$audio = $ddq_rec_audio;
			endif;
			$video = '';
			if ( ! empty($ddq_video)):
				$video = $ddq_video;
			elseif ( ! empty($ddq_rec_video)):
				$video = $ddq_rec_video;
			endif;
			#---DROP-OPTION---- 
			$quesData = array('questions'		=> ( ! empty($ddq)) ? addslashes($ddq) : '',
							  'seleted_drag'	=> 2,
							  'qtype'			=> ( ! empty($question_type)) ? $question_type : '',
							  'qaudio'			=> ( ! empty($qaudio)) ? $qaudio : '',
							  'qspeech_text'	=> ( ! empty($ddqq_text_to_speech)) ? addslashes($ddqq_text_to_speech) : '',
							  'speech_text'		=> ( ! empty($ddq_text_to_speech)) ? addslashes($ddq_text_to_speech) : '',
							  'audio'			=> ( ! empty($audio)) ? $audio : '',
							  'video'			=> ( ! empty($video)) ? $video : '',
							  'screen'			=> ( ! empty($ddq_rec_screen)) ? $ddq_rec_screen : '',
							  'image'			=> ( ! empty($ddq_img)) ? $ddq_img : '',
							  'document'		=> ( ! empty($ddq_doc)) ? $ddq_doc : '',
							  'shuffle'			=> (isset($suffleCheck) && ! empty($suffleCheck)) ? 1 : 0,
							  'true_options'	=> ( ! empty($ddq_true_option)) ? $ddq_true_option : '',
							  'ques_val_1'		=> ( ! empty($ddq_ques_val_1)) ? $ddq_ques_val_1 : '',
							  'ques_val_2'		=> ( ! empty($ddq_ques_val_2)) ? $ddq_ques_val_2 : '',
							  'ques_val_3'		=> ( ! empty($ddq_ques_val_3)) ? $ddq_ques_val_3 : '',
							  'ques_val_4'		=> ( ! empty($ddq_ques_val_4)) ? $ddq_ques_val_4 : '',
							  'ques_val_5'		=> ( ! empty($ddq_ques_val_5)) ? $ddq_ques_val_5 : '',
							  'ques_val_6'		=> ( ! empty($ddq_ques_val_6)) ? $ddq_ques_val_6 : '',
							  'critical'		=> ( ! empty($dnd_criticalQ)) ? $dnd_criticalQ : '',
							  'status' 			=> 1,
							  'user_id'			=> $userId,
							  'cur_date'		=> $curdate,
							  'qid'				=> $ques_id);
			$ques_sql  = "UPDATE `question_tbl` SET `questions`=:questions,`question_type`=:qtype, `seleted_drag_option`=:seleted_drag, `qaudio`=:qaudio, `qspeech_text`=:qspeech_text, `speech_text`=:speech_text, `audio`=:audio, `video`=:video, `screen`=:screen, `image`=:image, `document`=:document, `shuffle`=:shuffle, `true_options`=:true_options, `ques_val_1`=:ques_val_1, `ques_val_2`=:ques_val_2, `ques_val_3`=:ques_val_3, `ques_val_4`=:ques_val_4, `ques_val_5`=:ques_val_5, `ques_val_6`=:ques_val_6,`critical`=:critical, `status`=:status, `uid`=:user_id, `cur_date`=:cur_date WHERE question_id=:qid";
			$ques_stmt = $db->prepare($ques_sql);
			if ($ques_stmt->execute($quesData)):
				#-----Remove-Old-Feedback------
				$db->feedback_remove($ques_id);
				#------DRAG-OPTIONS------
				if ( ! empty($ddq2_drop)):
					if ( ! empty($updateDDq2Answerid)):
						#----DROP-TARGET----
						$ansSql = "UPDATE `sub_options_tbl` SET `sub_option`=:option, `image`=:image WHERE answer_id=:aid";
						$stmt = $db->prepare($ansSql);
						$stmt->execute(array('aid' => $updateDDq2Answerid, 'option' => ( ! empty($ddq2_drop)) ? addslashes($ddq2_drop) : '', 'image' => ( ! empty($ddq2_item_img)) ? $ddq2_item_img : ''));
						#-----DRAG-ITEMS------
						$ddq2iu = 0; $ddq2true = 1; $dndOffset = 1; $endSim = '';
						foreach ($ddq2_drag as $subAnswerData):
							if (isset($endSimulationDND) && in_array($dndOffset, $endSimulationDND)) {
								$endSim = 1;
							} else {
								$endSim = 0;
							}
							if ($endSim > 0){
								$mappedQuestionDnD[$ddq2iu] = '';
							}
							if ( ! empty($updateDDq2SubAnswerid[$ddq2iu])):
								$getOpId 		= $updateDDq2SubAnswerid[$ddq2iu];
								$updateAnsSql	= "UPDATE `answer_tbl` SET `choice_option`=:option, `image`=:image, `End_Sim`=:End_Sim,`nextQid`=:nextQid,`ans_val1`=:ans_val1, `ans_val2`=:ans_val2, `ans_val3`=:ans_val3, `ans_val4`=:ans_val4, `ans_val5`=:ans_val5, `ans_val6`=:ans_val6, `true_option`=:true, `status`=:status, `uid`=:user_id, `cur_date`=:cur_date WHERE answer_id=:aid";
								$updateAnsstmt	= $db->prepare($updateAnsSql);
								$updateAnsstmt->execute(array('aid' 		=> $getOpId,
															  'option'		=> ( ! empty($ddq2_drag[$ddq2iu])) ? $ddq2_drag[$ddq2iu] : '',
															  'image'		=> ( ! empty($ddq2_drag_item_img[$ddq2iu])) ? $ddq2_drag_item_img[$ddq2iu] : '',
															  'End_Sim'		=> ( ! empty($endSim)) ? addslashes($endSim) : '',
															  'nextQid'		=> ( ! empty($mappedQuestionDnD[$ddq2iu])) ? addslashes($mappedQuestionDnD[$ddq2iu]) : '',
															  'ans_val1'	=> ( ! empty($dd2_ans_val1[$ddq2iu])) ? $dd2_ans_val1[$ddq2iu] : '',
															  'ans_val2'	=> ( ! empty($dd2_ans_val2[$ddq2iu])) ? $dd2_ans_val2[$ddq2iu] : '',
															  'ans_val3'	=> ( ! empty($dd2_ans_val3[$ddq2iu])) ? $dd2_ans_val3[$ddq2iu] : '',
															  'ans_val4'	=> ( ! empty($dd2_ans_val4[$ddq2iu])) ? $dd2_ans_val4[$ddq2iu] : '',
															  'ans_val5'	=> ( ! empty($dd2_ans_val5[$ddq2iu])) ? $dd2_ans_val5[$ddq2iu] : '',
															  'ans_val6'	=> ( ! empty($dd2_ans_val6[$ddq2iu])) ? $dd2_ans_val6[$ddq2iu] : '',
															  'true'		=> ( ! empty($ddq_true_option) && $ddq_true_option == $ddq2true) ? 1 : '',
															  'status'		=> 1,
															  'user_id' 	=> $userId,
															  'cur_date'	=> $curdate));
							else:
								$ansSql = "INSERT INTO `answer_tbl` (`question_id`, `choice_option`, `End_Sim`,`nextQid`,`image`, `ans_val1`, `ans_val2`, `ans_val3`, `ans_val4`, `ans_val5`, `ans_val6`, `true_option`, `status`, `uid`, `cur_date`) values (:qid, :option, :End_Sim,:nextQid,:image, :ans_val1, :ans_val2, :ans_val3, :ans_val4, :ans_val5, :ans_val6, :true, :status, :user_id, :cur_date)";
								$stmt = $db->prepare($ansSql);
								$stmt->execute(array('qid'		=> $ques_id,
													'option'	=> ( ! empty($ddq2_drag[$ddq2iu])) ? addslashes($ddq2_drag[$ddq2iu]) : '',
													'End_Sim'	=> ( ! empty($endSim)) ? addslashes($endSim) : '',
													'nextQid'	=> ( ! empty($mappedQuestionDnD[$ddq2iu])) ? addslashes($mappedQuestionDnD[$ddq2iu]) : '',
													'image'		=> ( ! empty($ddq2_drag_item_img[$ddq2iu])) ? $ddq2_drag_item_img[$ddq2iu] : '',
													'ans_val1'	=> ( ! empty($dd2_ans_val1[$ddq2iu])) ? $dd2_ans_val1[$ddq2iu] : '',
													'ans_val2'	=> ( ! empty($dd2_ans_val2[$ddq2iu])) ? $dd2_ans_val2[$ddq2iu] : '',
													'ans_val3'	=> ( ! empty($dd2_ans_val3[$ddq2iu])) ? $dd2_ans_val3[$ddq2iu] : '',
													'ans_val4'	=> ( ! empty($dd2_ans_val4[$ddq2iu])) ? $dd2_ans_val4[$ddq2iu] : '',
													'ans_val5'	=> ( ! empty($dd2_ans_val5[$ddq2iu])) ? $dd2_ans_val5[$ddq2iu] : '',
													'ans_val6'	=> ( ! empty($dd2_ans_val6[$ddq2iu])) ? $dd2_ans_val6[$ddq2iu] : '',
													'true'		=> ( ! empty($ddq_true_option) && $ddq_true_option == $ddq2true) ? 1 : '',
													'status'	=> 1,
													'user_id' 	=> $userId,
													'cur_date'	=> $curdate));
								$getOpId = $db->lastInsertId();
							endif;
							#-----FEEDBACK-DATA---
							$feed_audio = '';
							if ( ! empty($dd2_feedback_audio[$ddq2iu])):
								$feed_audio = $dd2_feedback_audio[$ddq2iu];
							elseif ( ! empty($dd2_feedback_rec_audio[$ddq2iu])):
								$feed_audio = $dd2_feedback_rec_audio[$ddq2iu];
							endif;
							$feed_video = '';
							if ( ! empty($dd2_feedback_video[$ddq2iu])):
								$feed_video = $dd2_feedback_video[$ddq2iu];
							elseif ( ! empty($dd2_feedback_rec_video[$ddq2iu])):
								$feed_video = $dd2_feedback_rec_video[$ddq2iu];
							endif;
							#--------Feedback----------
							$cfeedData = array('qid'			=> ( ! empty($ques_id)) ? $ques_id : '',
											   'aid'			=> ( ! empty($getOpId)) ? $getOpId : '',
											   'feedback'		=> ( ! empty($dd2_cfeedback[$ddq2iu])) ? addslashes($dd2_cfeedback[$ddq2iu]) : '',
											   'ftype'			=> 1,
											   'speech_text'	=> ( ! empty($dd2_feedback_text_to_speech[$ddq2iu])) ? addslashes($dd2_feedback_text_to_speech[$ddq2iu]) : '',
											   'audio'			=> ( ! empty($feed_audio)) ? $feed_audio : '',
											   'video'			=> ( ! empty($feed_video)) ? $feed_video : '',
											   'screen'			=> ( ! empty($dd2_feedback_rec_screen[$ddq2iu])) ? $dd2_feedback_rec_screen[$ddq2iu] : '',
											   'image'			=> ( ! empty($dd2_feedback_img[$ddq2iu])) ? $dd2_feedback_img[$ddq2iu] : '',
											   'document'		=> ( ! empty($dd2_feedback_doc[$ddq2iu])) ? $dd2_feedback_doc[$ddq2iu] : '');
							$cfeed_sql  = "INSERT INTO `feedback_tbl` (`question_id`, `answer_id`, `feedback`, `feedback_type`, `feed_speech_text`, `feed_audio`, `feed_video`, `feed_screen`, `feed_image`, `feed_document`) VALUES (:qid, :aid, :feedback, :ftype, :speech_text, :audio, :video, :screen, :image, :document)";
							$cfeed_stmt = $db->prepare($cfeed_sql);
							$cfeed_stmt->execute($cfeedData);
							$ddq2iu++; $ddq2true++; $dndOffset++;
						endforeach;
					else:
						#----DROP-TARGET----
						$ansSql = "INSERT INTO `sub_options_tbl` (`question_id`, `sub_option`, `image`) VALUES (:qid, :option, :image)";
						$stmt = $db->prepare($ansSql);
						$stmt->execute(array('qid' 		=> $ques_id,
											 'option'	=> ( ! empty($ddq2_drop)) ? addslashes($ddq2_drop) : '',
											 'image'	=> ( ! empty($ddq2_item_img)) ? $ddq2_item_img : ''));
						#-----DRAG-ITEMS------
						$ddq2i = 0; $ddq2itrue = 1;
						foreach ($ddq2_drag as $answerData):
							if (isset($endSimulationDND) && in_array($ddq2itrue, $endSimulationDND)) {
								$endSim = 1;
							} else {
								$endSim = 0;
							}
							if ($endSim > 0) {
								$mappedQuestionDnD[$ddq2i] = '';
							}
							$ansSql = "INSERT INTO `answer_tbl` (`question_id`, `choice_option`, `End_Sim`,`nextQid`,`image`, `ans_val1`, `ans_val2`, `ans_val3`, `ans_val4`, `ans_val5`, `ans_val6`, `true_option`, `status`, `uid`, `cur_date`) values (:qid, :choice, :End_Sim,:nextQid,:image, :ans_val1, :ans_val2, :ans_val3, :ans_val4, :ans_val5, :ans_val6, :true, :status, :user_id, :cur_date)";
							$stmt   = $db->prepare($ansSql);
							$stmt->execute(array('qid'		=> $ques_id,
												 'choice'	=> ( ! empty($ddq2_drag[$ddq2i])) ? addslashes($ddq2_drag[$ddq2i]) : '',
												 'End_Sim'	=> ( ! empty($endSim)) ? addslashes($endSim) : '',
												 'nextQid'	=> ( ! empty($mappedQuestionDnD[$ddq2i])) ? addslashes($mappedQuestionDnD[$ddq2i]) : '',
												 'image'	=> ( ! empty($ddq2_drag_item_img[$ddq2i])) ? $ddq2_drag_item_img[$ddq2i] : '',
												 'ans_val1'	=> ( ! empty($dd2_ans_val1[$ddq2i])) ? $dd2_ans_val1[$ddq2i] : '',
												 'ans_val2'	=> ( ! empty($dd2_ans_val2[$ddq2i])) ? $dd2_ans_val2[$ddq2i] : '',
												 'ans_val3'	=> ( ! empty($dd2_ans_val3[$ddq2i])) ? $dd2_ans_val3[$ddq2i] : '',
												 'ans_val4'	=> ( ! empty($dd2_ans_val4[$ddq2i])) ? $dd2_ans_val4[$ddq2i] : '',
												 'ans_val5'	=> ( ! empty($dd2_ans_val5[$ddq2i])) ? $dd2_ans_val5[$ddq2i] : '',
												 'ans_val6'	=> ( ! empty($dd2_ans_val6[$ddq2i])) ? $dd2_ans_val6[$ddq2i] : '',
												 'true'		=> ( ! empty($ddq_true_option) && $ddq_true_option == $ddq2itrue) ? 1 : '',
												 'status'	=> 1,
												 'user_id' 	=> $userId,
												 'cur_date'	=> $curdate));
							$ans_id = $db->lastInsertId();
							
							#-----FEEDBACK-DATA---
							$feed_audio = '';
							if ( ! empty($dd2_feedback_audio[$ddq2i])):
								$feed_audio = $dd2_feedback_audio[$ddq2i];
							elseif ( ! empty($dd2_feedback_rec_audio[$ddq2i])):
								$feed_audio = $dd2_feedback_rec_audio[$ddq2i];
							endif;
							$feed_video = '';
							if ( ! empty($dd2_feedback_video[$ddq2i])):
								$feed_video = $dd2_feedback_video[$ddq2i];
							elseif ( ! empty($dd2_feedback_rec_video[$ddq2i])):
								$feed_video = $dd2_feedback_rec_video[$ddq2i];
							endif;
							#--------Feedback----------
							$cfeedData	= array('qid'			=> ( ! empty($ques_id)) ? $ques_id : '',
												'aid'			=> ( ! empty($ans_id)) ? $ans_id : '',
												'feedback'		=> ( ! empty($dd2_cfeedback[$ddq2i])) ? addslashes($dd2_cfeedback[$ddq2i]) : '',
												'ftype'			=> 1,
												'speech_text'	=> ( ! empty($dd2_feedback_text_to_speech[$ddq2i])) ? addslashes($dd2_feedback_text_to_speech[$ddq2i]) : '',
												'audio'			=> ( ! empty($feed_audio)) ? $feed_audio : '',
												'video'			=> ( ! empty($feed_video)) ? $feed_video : '',
												'screen'		=> ( ! empty($dd2_feedback_rec_screen[$ddq2i])) ? $dd2_feedback_rec_screen[$ddq2i] : '',
												'image'			=> ( ! empty($dd2_feedback_img[$ddq2i])) ? $dd2_feedback_img[$ddq2i] : '',
												'document'		=> ( ! empty($dd2_feedback_doc[$ddq2i])) ? $dd2_feedback_doc[$ddq2i] : '');
							$cfeed_sql	= "INSERT INTO `feedback_tbl` (`question_id`, `answer_id`, `feedback`, `feedback_type`, `feed_speech_text`, `feed_audio`, `feed_video`, `feed_screen`, `feed_image`, `feed_document`) VALUES (:qid, :aid, :feedback, :ftype, :speech_text, :audio, :video, :screen, :image, :document)";
							$cfeed_stmt	= $db->prepare($cfeed_sql); $cfeed_stmt->execute($cfeedData);
							$ddq2i++; $ddq2itrue++;
						endforeach;
					endif;
				endif;
			endif;
		//-----------START-SWIPING-----
		elseif ($question_type == 7):
			#-------QUESTIONS-DATA---
			$qaudio = '';
			if ( ! empty($swipqq_qaudio)):
				$qaudio = $swipqq_qaudio;
			elseif ( ! empty($swipqq_rec_audio)):
				$qaudio = $swipqq_rec_audio;
			endif;
			$audio = '';
			if ( ! empty($swipq_audio)):
				$audio = $swipq_audio;
			elseif ( ! empty($swipq_rec_audio)):
				$audio = $swipq_rec_audio;
			endif;
			$video = '';
			if ( ! empty($swipq_video)):
				$video = $swipq_video;
			elseif ( ! empty($swipq_rec_video)):
				$video = $swipq_rec_video;
			endif;
			$quesData = array('questions'		=> ( ! empty($swipq)) ? addslashes($swipq) : '',
							  'qtype'			=> ( ! empty($question_type)) ? $question_type : '',
							  'qaudio'			=> ( ! empty($qaudio)) ? $qaudio : '',
							  'qspeech_text'	=> ( ! empty($swipqq_text_to_speech)) ? addslashes($swipqq_text_to_speech) : '',
							  'speech_text'		=> ( ! empty($swipq_text_to_speech)) ? addslashes($swipq_text_to_speech) : '',
							  'audio'			=> ( ! empty($audio)) ? $audio : '',
							  'video'			=> ( ! empty($video)) ? $video : '',
							  'screen'			=> ( ! empty($swipq_rec_screen)) ? $swipq_rec_screen : '',
							  'image'			=> ( ! empty($swipq_img)) ? $swipq_img : '',
							  'document'		=> ( ! empty($swipq_doc)) ? $swipq_doc : '',
							  'true_option'		=> ( ! empty($swipq_true_option)) ? $swipq_true_option : '',
							  'shuffle'			=> (isset($swipq_shuffle) && ! empty($swipq_shuffle)) ? 1 : 0,
							  'ques_val_1'		=> ( ! empty($swipq_ques_val_1)) ? $swipq_ques_val_1 : '',
							  'ques_val_2'		=> ( ! empty($swipq_ques_val_2)) ? $swipq_ques_val_2 : '',
							  'ques_val_3'		=> ( ! empty($swipq_ques_val_3)) ? $swipq_ques_val_3 : '',
							  'ques_val_4'		=> ( ! empty($swipq_ques_val_4)) ? $swipq_ques_val_4 : '',
							  'ques_val_5'		=> ( ! empty($swipq_ques_val_5)) ? $swipq_ques_val_5 : '',
							  'ques_val_6'		=> ( ! empty($swipq_ques_val_6)) ? $swipq_ques_val_6 : '',
							  'critical'		=> ( ! empty($swip_criticalQ)) ? $swip_criticalQ : '',
							  'status' 			=> 1,
							  'user_id'			=> $userId,
							  'cur_date'		=> $curdate,
							  'qid'				=> $ques_id);
			$ques_sql	= "UPDATE `question_tbl` SET `questions`=:questions, `question_type`=:qtype, `qaudio`=:qaudio, `qspeech_text`=:qspeech_text, `speech_text`=:speech_text, `audio`=:audio, `video`=:video, `screen`=:screen, `image`=:image, `document`=:document, `true_options`=:true_option,`shuffle`=:shuffle, `ques_val_1`=:ques_val_1, `ques_val_2`=:ques_val_2, `ques_val_3`=:ques_val_3, `ques_val_4`=:ques_val_4, `ques_val_5`=:ques_val_5, `ques_val_6`=:ques_val_6,`critical`=:critical,`status`=:status, `uid`=:user_id, `cur_date`=:cur_date WHERE question_id=:qid";
			$ques_stmt	= $db->prepare($ques_sql);
			if ($ques_stmt->execute($quesData)):
				#-----Remove-Old-Feedback-------
				$db->feedback_remove($ques_id);		
				#----------ADD-OPTIONS----------
				if ( ! empty($swipq_option[0])):
					$swipqi = 0; $swiptrue = 1; $endSimq = '';
					foreach ($swipq_option as $answerData):
						if (isset($endSimulationSwipe) && in_array($swiptrue, $endSimulationSwipe)) {
							$endSim = 1;
						} else {
							$endSim = 0;
						}
						if ($endSim > 0){
							$mappedQuestionSwipe[$swipqi] = '';
						}
						if ( ! empty($updateSwipqAnswerid[$swipqi])):
							$getSwipqAnswerid = $updateSwipqAnswerid[$swipqi];
							$ansSql	= "UPDATE `answer_tbl` SET `choice_option`=:option, `nextQid`=:nextQid,`End_Sim`=:End_Sim,`image`=:image, `ans_val1`=:val1, `ans_val2`=:val2, `ans_val3`=:val3, `ans_val4`=:val4, `ans_val5`=:val5, `ans_val6`=:val6, `true_option`=:true, `status`=:status, `uid`=:user_id, `cur_date`=:cur_date WHERE answer_id=:aid";
							$stmt = $db->prepare($ansSql);
							$stmt->execute(array('aid'		=> $getSwipqAnswerid,
												 'option'	=> ( ! empty($swipq_option[$swipqi])) ? addslashes($swipq_option[$swipqi]) : '',
												 'nextQid'	=> ( ! empty($mappedQuestionSwipe[$swipqi])) ? addslashes($mappedQuestionSwipe[$swipqi]) : '',
												 'End_Sim'	=> $endSim,
												 'image'	=> ( ! empty($swipq_item_img[$swipqi])) ? $swipq_item_img[$swipqi] : '',
												 'val1'		=> ( ! empty($swip_ans_val1[$swipqi])) ? $swip_ans_val1[$swipqi] : '',
												 'val2'		=> ( ! empty($swip_ans_val2[$swipqi])) ? $swip_ans_val2[$swipqi] : '',
												 'val3'		=> ( ! empty($swip_ans_val3[$swipqi])) ? $swip_ans_val3[$swipqi] : '',
												 'val4'		=> ( ! empty($swip_ans_val4[$swipqi])) ? $swip_ans_val4[$swipqi] : '',
												 'val5'		=> ( ! empty($swip_ans_val5[$swipqi])) ? $swip_ans_val5[$swipqi] : '',
												 'val6'		=> ( ! empty($swip_ans_val6[$swipqi])) ? $swip_ans_val6[$swipqi] : '',
												 'true'		=> ( ! empty($swipq_true_option) && $swipq_true_option == $swiptrue) ? 1 : '',
												 'status'	=> 1,
												 'user_id'	=> $userId,
												 'cur_date'	=> $curdate));
						else:
							$ansSql	= "INSERT INTO `answer_tbl` (`question_id`, `choice_option`, `nextQid`,`End_Sim`,`image`, `ans_val1`, `ans_val2`, `ans_val3`, `ans_val4`, `ans_val5`, `ans_val6`, `true_option`, `status`, `uid`, `cur_date`) values (:qid, :option,:nextQid, :End_Sim,:image, :val1, :val2, :val3, :val4, :val5, :val6, :true, :status, :user_id, :cur_date)";
							$stmt	= $db->prepare($ansSql);
							$stmt->execute(array('qid'		=> $ques_id,
												 'option'	=> ( ! empty($swipq_option[$swipqi])) ? addslashes($swipq_option[$swipqi]) : '',
												 'nextQid'	=> ( ! empty($mappedQuestionSwipe[$swipqi])) ? addslashes($mappedQuestionSwipe[$swipqi]) : '',
												 'End_Sim'	=> $endSim,
												 'image'	=> ( ! empty($swipq_item_img[$swipqi])) ? $swipq_item_img[$swipqi] : '',
												 'val1'		=> ( ! empty($swip_ans_val1[$swipqi])) ? $swip_ans_val1[$swipqi] : '',
												 'val2'		=> ( ! empty($swip_ans_val2[$swipqi])) ? $swip_ans_val2[$swipqi] : '',
												 'val3'		=> ( ! empty($swip_ans_val3[$swipqi])) ? $swip_ans_val3[$swipqi] : '',
												 'val4'		=> ( ! empty($swip_ans_val4[$swipqi])) ? $swip_ans_val4[$swipqi] : '',
												 'val5'		=> ( ! empty($swip_ans_val5[$swipqi])) ? $swip_ans_val5[$swipqi] : '',
												 'val6'		=> ( ! empty($swip_ans_val6[$swipqi])) ? $swip_ans_val6[$swipqi] : '',
												 'true'		=> ( ! empty($swipq_true_option) && $swipq_true_option == $swiptrue) ? 1 : '',
												 'status'	=> 1,
												 'user_id' 	=> $userId,
												 'cur_date'	=> $curdate));
							$getSwipqAnswerid =  $db->lastInsertId();
						endif;
						#------FEEDBACK-DATA---
						$feed_audio = '';
						if ( ! empty($swip_feedback_audio[$swipqi])):
							$feed_audio = $swip_feedback_audio[$swipqi];
						elseif ( ! empty($swip_feedback_rec_audio[$swipqi])):
							$feed_audio = $swip_feedback_rec_audio[$swipqi];
						endif;
						$feed_video = '';
						if ( ! empty($swip_feedback_video[$swipqi])):
							$feed_video = $swip_feedback_video[$swipqi];
						elseif ( ! empty($swip_feedback_rec_video[$swipqi])):
							$feed_video = $swip_feedback_rec_video[$swipqi];
						endif;
						#--------Feedback----------
						$cfeedData = array('qid'			=> $ques_id,
										   'aid'			=> ( ! empty($getSwipqAnswerid)) ? $getSwipqAnswerid : '',
										   'feedback'		=> ( ! empty($swip_cfeedback[$swipqi])) ? addslashes($swip_cfeedback[$swipqi]) : '',
										   'ftype'			=> 1,
										   'speech_text'	=> ( ! empty($swip_feedback_text_to_speech[$swipqi])) ? addslashes($swip_feedback_text_to_speech[$swipqi]) : '',
										   'audio'			=> ( ! empty($feed_audio)) ? $feed_audio : '',
										   'video'			=> ( ! empty($feed_video)) ? $feed_video : '',
										   'screen'			=> ( ! empty($swip_feedback_rec_screen[$swipqi])) ? $swip_feedback_rec_screen[$swipqi] : '',
										   'image'			=> ( ! empty($swip_feedback_img[$swipqi])) ? $swip_feedback_img[$swipqi] : '',
										   'document'		=> ( ! empty($swip_feedback_doc[$swipqi])) ? $swip_feedback_doc[$swipqi] : '');
						$cfeed_sql  = "INSERT INTO `feedback_tbl` (`question_id`, `answer_id`, `feedback`, `feedback_type`, `feed_speech_text`, `feed_audio`, `feed_video`, `feed_screen`, `feed_image`, `feed_document`) VALUES (:qid, :aid, :feedback, :ftype, :speech_text, :audio, :video, :screen, :image, :document)";
						$cfeed_stmt = $db->prepare($cfeed_sql); $cfeed_stmt->execute($cfeedData);
						$swipqi++; $swiptrue++;
					endforeach;
				endif;
			endif;
		endif;
	endif;
	//-----------END-SWIPING----------
	if ( ! empty($submit_type) && $submit_type == 1):
		$resdata = array('success' => TRUE, 'all_update' => FALSE, 'msg' => 'Simulation details updated successfully.', 'return_url' => 'branching-classic-template.php?add_data=true&sim_id='.md5($sim_id).'&ques_id='.base64_encode($ques_id));
	else:
		$resdata = array('success' => TRUE, 'all_update' => TRUE, 'msg' => 'All questions and answers updated successfully.', 'return_url' => 'author-simulation.php');
	endif;
	echo json_encode($resdata);
endif;

#-----------------------------------------Add-Branching-Video-Classic-Sim - Saumya----------------------------------
if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['add_branching_video_classic_sim']) && isset($_POST['sim_id'])):
	extract($_POST);
	if (empty($sim_duration) || empty($passing_marks)):
		$resdata 					= array('success' => FALSE, 'msg' => 'Please enter all required fields.');
	elseif ( ! empty($sim_duration) && ! empty($passing_marks)):
		$sql 						= "UPDATE scenario_master SET duration = '". $sim_duration ."', passing_marks = '". $passing_marks ."'";		
		#--Sim-Media-File--
		if ( ! empty($scenario_media_file) && in_array(strtolower(pathinfo($scenario_media_file, PATHINFO_EXTENSION)), $img_valid_exts)):
			$sql 					.= ", image_url = '". $scenario_media_file ."', video_url = NULL, audio_url = NULL";
		elseif ( ! empty($scenario_media_file) && in_array(strtolower(pathinfo($scenario_media_file, PATHINFO_EXTENSION)), $video_valid_exts)):
			$sql 					.= ", video_url = '". $scenario_media_file ."', image_url = NULL, audio_url = NULL";
		elseif ( ! empty($scenario_media_file) && in_array(strtolower(pathinfo($scenario_media_file, PATHINFO_EXTENSION)), $audio_valid_exts)):
			$sql 					.= ", audio_url = '". $scenario_media_file ."', video_url = NULL, image_url = NULL";
		endif;
		#--Sim-Backgrounds--
		if (isset($bgoption) && $bgoption == 1 && ! empty($bg_color)):
			$sql 					.= ", bg_color = '". $bg_color ."', bg_own_choice = '', sim_back_img = ''";
		elseif (isset($bgoption) && $bgoption == 2 && ! empty($scenario_bg_file)):
			$sql 					.= ", bg_own_choice = '". $scenario_bg_file ."', bg_color = '', sim_back_img = ''";
		elseif (isset($bgoption) && $bgoption == 3 && ! empty($sim_background_img)):
			$sql 					.= ", sim_back_img = '". $sim_background_img ."', bg_own_choice = '', bg_color = ''";
		endif;
		$sql 						.= " WHERE scenario_id = '". $sim_id ."'";
		$results 					= $db->prepare($sql);
		if ($results->execute()):
			#----Competency----
			$delCom	 				= "DELETE FROM competency_tbl WHERE scenario_id = '". $sim_id ."'";
			$delComq 				= $db->prepare($delCom); $delComq->execute();
			$data	 				= array(
										'scenario_id'	=> $sim_id,
										'comp_col_1'	=> (isset($competency_name_1) && ! empty($competency_name_1)) ? $competency_name_1 : '',
										'comp_val_1'	=> (isset($competency_score_1) && ! empty($competency_score_1)) ? $competency_score_1 : '',
										'comp_col_2'	=> (isset($competency_name_2) && ! empty($competency_name_2)) ? $competency_name_2 : '',
										'comp_val_2'	=> (isset($competency_score_2) && ! empty($competency_score_2)) ? $competency_score_2 : '',
										'comp_col_3'	=> (isset($competency_name_3) && ! empty($competency_name_3)) ? $competency_name_3 : '',
										'comp_val_3'	=> (isset($competency_score_3) && ! empty($competency_score_3)) ? $competency_score_3 : '',
										'comp_col_4'	=> (isset($competency_name_4) && ! empty($competency_name_4)) ? $competency_name_4 : '',
										'comp_val_4'	=> (isset($competency_score_4) && ! empty($competency_score_4)) ? $competency_score_4 : '',
										'comp_col_5'	=> (isset($competency_name_5) && ! empty($competency_name_5)) ? $competency_name_5 : '',
										'comp_val_5'	=> (isset($competency_score_5) && ! empty($competency_score_5)) ? $competency_score_5 : '',
										'comp_col_6'	=> (isset($competency_name_6) && ! empty($competency_name_6)) ? $competency_name_6 : '',
										'comp_val_6'	=> (isset($competency_score_6) && ! empty($competency_score_6)) ? $competency_score_6 : '',
										'weightage_1'	=> (isset($weightage_1) && ! empty($weightage_1)) ? $weightage_1 : '',
										'weightage_2'	=> (isset($weightage_2) && ! empty($weightage_2)) ? $weightage_2 : '',
										'weightage_3'	=> (isset($weightage_3) && ! empty($weightage_3)) ? $weightage_3 : '',
										'weightage_4'	=> (isset($weightage_4) && ! empty($weightage_4)) ? $weightage_4 : '',
										'weightage_5'	=> (isset($weightage_5) && ! empty($weightage_5)) ? $weightage_5 : '',
										'weightage_6'	=> (isset($weightage_6) && ! empty($weightage_6)) ? $weightage_6 : '',
										'status' 		=> 1,
										'user_id' 		=> $userId,
										'cur_date'		=> $curdate);
			$com_sql 				= "INSERT INTO `competency_tbl` (`scenario_id`, `comp_col_1`, `comp_val_1`, `comp_col_2`, `comp_val_2`, `comp_col_3`, `comp_val_3`, `comp_col_4`, `comp_val_4`, `comp_col_5`, `comp_val_5`, `comp_col_6`, `comp_val_6`, `weightage_1`, `weightage_2`, `weightage_3`, `weightage_4`, `weightage_5`, `weightage_6`, `status`, `uid`, `cur_date`) VALUES (:scenario_id, :comp_col_1, :comp_val_1, :comp_col_2, :comp_val_2, :comp_col_3, :comp_val_3, :comp_col_4, :comp_val_4, :comp_col_5, :comp_val_5, :comp_col_6, :comp_val_6, :weightage_1, :weightage_2, :weightage_3, :weightage_4, :weightage_5, :weightage_6, :status, :user_id, :cur_date)";
			$com_stmt 				= $db->prepare($com_sql);
			$com_stmt->execute($data);
			#----PAGES----
			$delPage 				= "DELETE FROM sim_pages WHERE scenario_id = '". $sim_id ."'";
			$delPageq 				= $db->prepare($delPage); $delPageq->execute();
			if ( ! empty($sim_page_name)):
				$sp 				= 0;
				foreach ($sim_page_name as $pdata):
					$pageq 			= "INSERT INTO `sim_pages` (`scenario_id`, `sim_page_name`, `sim_page_desc`, `userid`, `status`, `added_date`) 
					VALUES (:sid, :pname, :pdesc, :user_id, :status, :adate)";
					$page_stmt 		= $db->prepare($pageq);
					$page_stmt->execute(array(
						'sid'		=> $sim_id,
						'pname'		=> ( ! empty($pdata)) ? $pdata : '',
						'pdesc'		=> ( ! empty($sim_page_desc[$sp])) ? $sim_page_desc[$sp] : '',
						'status' 	=> 1,
						'user_id'	=> $userId,
						'adate'		=> $curdate
					));
					$sp++;
				endforeach;
			endif;
			#----Branding----
			$bandq	  				= "DELETE FROM sim_branding_tbl WHERE scenario_id = '". $sim_id ."'";
			$bandqexe 				= $db->prepare($bandq); $bandqexe->execute();
			$brand_data 			= array(
										'sid'				=> $sim_id,
										'bg'				=> ( ! empty($ques_bg_color)) ? $ques_bg_color : '',
										'bg_tr'				=> ( ! empty($ques_bg_transp)) ? $ques_bg_transp : '',
										'obg'				=> ( ! empty($ques_option_bg_color)) ? $ques_option_bg_color : '',
										'obg_tr'			=> ( ! empty($ques_option_bg_transp)) ? $ques_option_bg_transp : '',
										'ohover'			=> ( ! empty($ques_option_hover_color)) ? $ques_option_hover_color : '',
										'oh_tr'				=> ( ! empty($ques_option_hover_transp)) ? $ques_option_hover_transp : '',
										'oselected'			=> ( ! empty($ques_option_select_color)) ? $ques_option_select_color : '',
										'oselected_tr'		=> ( ! empty($ques_option_select_transp)) ? $ques_option_select_transp : '',
										'btn_bg'			=> ( ! empty($btn_bg_color)) ? $btn_bg_color : '',
										'btn_bg_tr'			=> ( ! empty($btn_bg_transp)) ? $btn_bg_transp : '',
										'btn_hover'			=> ( ! empty($btn_hover_color)) ? $btn_hover_color : '',
										'btn_hover_tr'		=> ( ! empty($btn_hover_transp)) ? $btn_hover_transp : '',
										'btn_selected'		=> ( ! empty($btn_select_color)) ? $btn_select_color : '',
										'btn_selected_tr'	=> ( ! empty($btn_select_transp)) ? $btn_select_transp : '',
										'font_type'			=> ( ! empty($font_type)) ? $font_type : '',
										'font_color'		=> ( ! empty($font_color)) ? $font_color : '',
										'font_size'			=> ( ! empty($font_size)) ? $font_size : '',
										'user_id' 			=> $userId,
										'added_date'		=> $curdate);
			$com_sql 				= "INSERT INTO `sim_branding_tbl` (`scenario_id`, `ques_bg`, `ques_bg_transparency`, `option_bg`, `option_bg_transparency`, `option_hover`, `option_hover_transparency`, `option_selected`, `option_selected_transparency`, `btn_bg`, `btn_bg_transparency`, `btn_hover`, `btn_hover_transparency`, `btn_selected`, `btn_selected_transparency`, `font_type`, `font_color`, `font_size`, `user_id`, `added_date`) 
			VALUES (:sid, :bg, :bg_tr, :obg, :obg_tr, :ohover, :oh_tr, :oselected, :oselected_tr, :btn_bg, :btn_bg_tr, :btn_hover, :btn_hover_tr, :btn_selected, :btn_selected_tr, :font_type, :font_color, :font_size, :user_id, :added_date)";
			$com_stmt 				= $db->prepare($com_sql);
			$com_stmt->execute($brand_data);
		endif;
	endif;
	if($singleOrMultiple == 'multipleVideo'){
		if ( $question_type == 4){//MCQ
			$audio 							= '';
			$video 							= '';
			$feed_audio 					= '';
			$feed_video 					= '';
			#-------QUESTIONS-DATA---
			$qaudio 						= '';
			if ( ! empty($videoqq_audio[0])):
				$qaudio 					= $videoqq_audio[0];
			elseif ( ! empty($videoqq_rec_audio[0])):
				$qaudio 					= $videoqq_rec_audio[0];
			endif;			
			if ( ! empty($videoq_audio[0])):
				$audio 						= $videoq_audio[0];
			elseif ( ! empty($videoq_rec_audio[0])):
				$audio 						= $videoq_rec_audio[0];
			endif;		
			if ( ! empty($videoq_video[0])):
				$video 						= $videoq_video[0];
			elseif ( ! empty($videoq_rec_video[0])):
				$video 						= $videoq_rec_video[0];
			endif;
			$quesData 						= array( 
				'scenarioId'				=> $sim_id,
				'questionType'				=> $question_type,
				'questions'					=> ( ! empty($videoq)) ? $videoq : '',
				'qaudio'					=> ( ! empty($qaudio)) ? $qaudio : '',
				'qspeech_text'				=> ( ! empty($videoqq_text_to_speech[0])) ? $videoqq_text_to_speech[0] : '',
				'speech_text'				=> ( ! empty($videoq_text_to_speech[0])) ? $videoq_text_to_speech[0] : '',
				'audio'						=> ( ! empty($audio)) ? $audio : '',
				'video'						=> ( ! empty($video)) ? $video : '',
				'screen'					=> ( ! empty($videoq_rec_screen[0])) ? $videoq_rec_screen[0] : '',
				'image'						=> ( ! empty($videoq_img[0])) ? $videoq_img[0] : '',
				'document'					=> ( ! empty($videoq_doc[0])) ? $videoq_doc[0] : '',
				'true_option'				=> ( ! empty($videoq_true_option[1][0])) ? $videoq_true_option[1][0] : '',
				'media_file'				=> ( ! empty($videoq_media_file)) ? $videoq_media_file : '',				
				'shuffle'					=> ( ! empty($video_shuffle[0])) ? $video_shuffle[0] : '',
				'ques_val_1'				=> ( ! empty($videoq_ques_val_1)) ? $videoq_ques_val_1 : '',
				'ques_val_2'				=> ( ! empty($videoq_ques_val_2)) ? $videoq_ques_val_2 : '',
				'ques_val_3'				=> ( ! empty($videoq_ques_val_3)) ? $videoq_ques_val_3: '',
				'ques_val_4'				=> ( ! empty($videoq_ques_val_4)) ? $videoq_ques_val_4 : '',
				'ques_val_5'				=> ( ! empty($videoq_ques_val_5)) ? $videoq_ques_val_5 : '',
				'ques_val_6'				=> ( ! empty($videoq_ques_val_6)) ? $videoq_ques_val_6 : '',
				'critical'					=> ( ! empty($mcq_criticalQ)) ? $mcq_criticalQ : '', 
				'status' 					=> 1,
				'user_id'					=> $userId,
				'cur_date'					=> $curdate 
			);
			$ques_sql 						= "INSERT INTO `question_tbl` (`scenario_id`,`question_type`, `questions`, `qaudio`,`qspeech_text`, `speech_text`,`audio`, `video`, `screen`, `image`, `document`, `true_options`, `videoq_media_file`, `shuffle`,`ques_val_1`, `ques_val_2`, `ques_val_3`, `ques_val_4`, `ques_val_5`, `ques_val_6`,`critical`, `status`, `uid`, `cur_date`) 
			VALUES (:scenarioId, :questionType, :questions, :qaudio, :qspeech_text, :speech_text, :audio, :video, :screen, :image, :document, :true_option, :media_file, :shuffle, :ques_val_1, :ques_val_2, :ques_val_3, :ques_val_4, :ques_val_5, :ques_val_6, :critical,:status, :user_id, :cur_date)";
			$ques_stmt 						= $db->prepare($ques_sql);
			if ($ques_stmt->execute($quesData)){
				$ques_id 					= $db->lastInsertId();
				$voption 					= 0;
				foreach ($videoq_option[1]['option'] as $answerData):					
					//echo $endSim;die();			
					$ansSql 		= "INSERT INTO `answer_tbl` (`question_id`, `choice_option`, `ans_val1`, `ans_val2`, `ans_val3`, `ans_val4`, `ans_val5`, `ans_val6`, `true_option`,`status`, `uid`, `cur_date`) 
					values (:qid, :option, :val1, :val2, :val3, :val4, :val5, :val6, :true_option,:status, :user_id, :cur_date)";
					$stmt 			= $db->prepare($ansSql);
					$stmt->execute(array(
						'qid' 		=> $ques_id,
						'option'	=> ( ! empty($answerData)) ? $answerData : '',
						'val1'		=> ( ! empty($videoq_ans_val1[1]['score'][$voption])) ? $videoq_ans_val1[1]['score'][$voption] : '',
						'val2'		=> ( ! empty($videoq_ans_val2[1]['score'][$voption])) ? $videoq_ans_val2[1]['score'][$voption] : '',
						'val3'		=> ( ! empty($videoq_ans_val3[1]['score'][$voption])) ? $videoq_ans_val3[1]['score'][$voption] : '',
						'val4'		=> ( ! empty($videoq_ans_val4[1]['score'][$voption])) ? $videoq_ans_val4[1]['score'][$voption] : '',
						'val5'		=> ( ! empty($videoq_ans_val5[1]['score'][$voption])) ? $videoq_ans_val5[1]['score'][$voption] : '',
						'val6'		=> ( ! empty($videoq_ans_val6[1]['score'][$voption])) ? $videoq_ans_val6[1]['score'][$voption] : '',
						'true_option'	=> ( ! empty($videoq_true_option[1][$voption])) ? $videoq_true_option[1][$voption] : '',
						'status'	=> 1,
						'user_id' 	=> $userId,
						'cur_date'	=> $curdate
					));
					$getOptionAnswerid 		=  $db->lastInsertId();
					//Add Dummy Question for the option
					$tq 					= $db->getTotalQuestionsByScenario(md5($sim_id)) + 1;
					$quesData 				= array(
												'sid'					=> $sim_id,
												'option_id'				=> $getOptionAnswerid,
												'ref_question_id'		=> $ques_id,
												'questions'				=> 'Question '. $tq,
												'status' 				=> 1,
												'user_id'				=> $userId,
												'cur_date'				=> $curdate
											);
					$ques_sql 				= "INSERT INTO `question_tbl` (`scenario_id`, `option_id`,`ref_question_id`,`questions`, `status`, `uid`, `cur_date`) VALUES (:sid, :option_id,:ref_question_id,:questions, :status, :user_id, :cur_date)";
					$ques_stmt 				= $db->prepare($ques_sql);
					$ques_stmt->execute($quesData);					
					#-----ADD FEEDBACK-DATA---
					if ( ! empty($videoqf_feedback_audio[0])):
						$feed_audio = $videoqf_feedback_audio[0];
					elseif ( ! empty($videoqf_feedback_rec_audio[0])):
						$feed_audio = $videoqf_feedback_rec_audio[0];
					endif;					
					if ( ! empty($videoqf_feedback_video[0])):
						$feed_video = $videoqf_feedback_video[0];
					elseif ( ! empty($videoqf_feedback_rec_video[0])):
						$feed_video = $videoqf_feedback_rec_video[0];
					endif;
					#--------Feedback----------
					$cfeedData = array(
						'qid'				=> $ques_id,
						'answer_id'			=> $getOptionAnswerid,
						'feedback'			=> ( ! empty($mcq_cfeedback[$voption])) ? $mcq_cfeedback[$voption] : '',
						'ftype'				=> 1,
						'speech_text'		=> ( ! empty($videoqf_feedback_text_to_speech[0])) ? $videoqf_feedback_text_to_speech[0] : '',
						'feed_audio'		=> ( ! empty($feed_audio)) ? $feed_audio : '',
						'feed_video'		=> ( ! empty($feed_video)) ? $feed_video : '',
						'feed_screen'		=> ( ! empty($videoqf_feedback_rec_screen[0])) ? $videoqf_feedback_rec_screen[0] : '',
						'feed_image'		=> ( ! empty($videoqf_feedback_img[0])) ? $videoqf_feedback_img[0] : '',
						'feed_document'		=> ( ! empty($videoqf_feedback_doc[0])) ? $videoqf_feedback_doc[0] : ''
					);
					$cfeed_sql 				= "INSERT INTO `feedback_tbl` (`question_id`, `answer_id`,`feedback`, `feedback_type`, `feed_speech_text`, `feed_audio`, `feed_video`, `feed_screen`, `feed_image`, `feed_document`) 
					VALUES (:qid, :answer_id,:feedback, :ftype, :speech_text, :feed_audio, :feed_video, :feed_screen, :feed_image, :feed_document)";
					$cfeed_stmt 			= $db->prepare($cfeed_sql);
					$cfeed_stmt->execute($cfeedData);
					$voption++;
				endforeach;	
			}
		}
		if ($question_type == 8){//DND
			#-------QUESTIONS-DATA---
			$qaudio 						= '';
			if ( ! empty($ddqq_qaudio)):
				$qaudio 					= $ddqq_qaudio;
			elseif ( ! empty($ddqq_rec_audio)):
				$qaudio 					= $ddqq_rec_audio;
			endif;	
			$audio 							= '';
			if ( ! empty($ddq_audio)):
				$audio 						= $ddq_audio;
			elseif ( ! empty($ddq_rec_audio)):
				$audio 						= $ddq_rec_audio;
			endif;		
			$video 							= '';
			if ( ! empty($ddq_video)):
				$video 						= $ddq_video;
			elseif ( ! empty($ddq_rec_video)):
				$video 						= $ddq_rec_video;
			endif;
			$DDqquesData 						= array(
				'scenarioId'				=> $sim_id,
				'questionType'				=> $question_type,
				'questions'					=> ( ! empty($ddq)) ? addslashes($ddq) : '',
				'qaudio'					=> ( ! empty($qaudio)) ? $qaudio : '',
				'audio'						=> ( ! empty($audio)) ? $audio : '',
				'video'						=> ( ! empty($video)) ? $video : '',
				'screen'					=> ( ! empty($ddq_rec_screen)) ? $ddq_rec_screen : '',
				'image'						=> ( ! empty($ddq_img)) ? $ddq_img : '',
				'document'					=> ( ! empty($ddq_doc)) ? $ddq_doc : '',
				'true_options'				=> ( ! empty($ddq_true_option)) ? $ddq_true_option : '',
				'media_file'				=> ( ! empty($videoq_media_file)) ? $videoq_media_file : '',
				'shuffle'					=> (isset($ddq_shuffle2) && ! empty($ddq_shuffle2)) ? 1 : 0,
				'ques_val_1'				=> ( ! empty($ddq_ques_val_1)) ? $ddq_ques_val_1 : '',
				'ques_val_2'				=> ( ! empty($ddq_ques_val_2)) ? $ddq_ques_val_2 : '',
				'ques_val_3'				=> ( ! empty($ddq_ques_val_3)) ? $ddq_ques_val_3 : '',
				'ques_val_4'				=> ( ! empty($ddq_ques_val_4)) ? $ddq_ques_val_4 : '',
				'ques_val_5'				=> ( ! empty($ddq_ques_val_5)) ? $ddq_ques_val_5 : '',
				'ques_val_6'				=> ( ! empty($ddq_ques_val_6)) ? $ddq_ques_val_6 : '',	
				'critical'					=> ( ! empty($dnd_criticalQ)) ? $dnd_criticalQ : '',		
				'status' 					=> 1,
				'user_id'					=> $userId,
				'cur_date'					=> $curdate
			);						
			$DDqques_sql 						= "INSERT INTO `question_tbl` (`scenario_id`,`question_type`, `questions`, `qaudio`, `audio`, `video`, `screen`, `image`, `document`, `true_options`, `videoq_media_file`, `shuffle`,`ques_val_1`, `ques_val_2`, `ques_val_3`, `ques_val_4`, `ques_val_5`, `ques_val_6`,`critical`, `status`, `uid`, `cur_date`) 
			VALUES (:scenarioId, :questionType, :questions, :qaudio, :audio, :video, :screen, :image, :document, :true_options, :media_file, :shuffle, :ques_val_1, :ques_val_2, :ques_val_3, :ques_val_4, :ques_val_5, :ques_val_6, :critical,:status, :user_id, :cur_date)";
			$DDqques_stmt 						= $db->prepare($DDqques_sql);
			if ($DDqques_stmt->execute($DDqquesData)){
				$ques_id 					= $db->lastInsertId();				
				#-----Remove-Old-Feedback------
				$db->feedback_remove($ques_id);				
				#------DRAG-OPTIONS------
				if ( ! empty($ddq2_drop)):						
					#----DROP-TARGET----
					$ansSql 				= "INSERT INTO `sub_options_tbl` (`question_id`, `sub_option`, `image`) values (:qid, :option, :image)";
					$stmt 					= $db->prepare($ansSql);
					$stmt->execute(array('qid' => $ques_id, 'option' => ( ! empty($ddq2_drop)) ? addslashes($ddq2_drop) : '', 'image' => ( ! empty($ddq2_item_img)) ? $ddq2_item_img : ''));
					#-----DRAG-ITEMS------
					$ddq2i 					= 0; 
					$ddq2itrue 				= 1;
					foreach ($ddq2_drag as $answerData):
						$ansSql 			= "INSERT INTO `answer_tbl` (`question_id`, `choice_option`, `image`, `ans_val1`, `ans_val2`, `ans_val3`, `ans_val4`, `ans_val5`, `ans_val6`, `true_option`, `status`, `uid`, `cur_date`) values (:qid, :choice, :image, :ans_val1, :ans_val2, :ans_val3, :ans_val4, :ans_val5, :ans_val6, :true, :status, :user_id, :cur_date)";
						$stmt 				= $db->prepare($ansSql);
						$stmt->execute(array(
							'qid'			=> $ques_id,
							'choice'		=> ( ! empty($ddq2_drag[$ddq2i])) ? addslashes($ddq2_drag[$ddq2i]) : '',
							'image'			=> ( ! empty($ddq2_drag_item_img[$ddq2i])) ? $ddq2_drag_item_img[$ddq2i] : '',
							'ans_val1'		=> ( ! empty($dd2_ans_val1[$ddq2i])) ? $dd2_ans_val1[$ddq2i] : '',
							'ans_val2'		=> ( ! empty($dd2_ans_val2[$ddq2i])) ? $dd2_ans_val2[$ddq2i] : '',
							'ans_val3'		=> ( ! empty($dd2_ans_val3[$ddq2i])) ? $dd2_ans_val3[$ddq2i] : '',
							'ans_val4'		=> ( ! empty($dd2_ans_val4[$ddq2i])) ? $dd2_ans_val4[$ddq2i] : '',
							'ans_val5'		=> ( ! empty($dd2_ans_val5[$ddq2i])) ? $dd2_ans_val5[$ddq2i] : '',
							'ans_val6'		=> ( ! empty($dd2_ans_val6[$ddq2i])) ? $dd2_ans_val6[$ddq2i] : '',
							'true'			=> ( ! empty($ddq_true_option) && $ddq_true_option == $ddq2itrue) ? 1 : '',
							'status'		=> 1,
							'user_id' 		=> $userId,
							'cur_date'		=> $curdate
						));
						$ans_id 			= $db->lastInsertId();	
						//Add Dummy Question for the option
						$tq 				= $db->getTotalQuestionsByScenario(md5($sim_id)) + 1;
						$quesData 			= array(
													'sid'					=> $sim_id,
													'option_id'				=> $ans_id,
													'ref_question_id'		=> $ques_id,
													'questions'				=> 'Question '. $tq,
													'status' 				=> 1,
													'user_id'				=> $userId,
													'cur_date'				=> $curdate
												);
						$ques_sql 			= "INSERT INTO `question_tbl` (`scenario_id`, `option_id`,`ref_question_id`,`questions`, `status`, `uid`, `cur_date`) VALUES (:sid, :option_id,:ref_question_id,:questions, :status, :user_id, :cur_date)";
						$ques_stmt 			= $db->prepare($ques_sql);
						$ques_stmt->execute($quesData);
						#-----FEEDBACK-DATA---
						$feed_audio 		= '';
						if ( ! empty($dd2_feedback_audio[$ddq2i])):
							$feed_audio 	= $dd2_feedback_audio[$ddq2i];
						elseif ( ! empty($dd2_feedback_rec_audio[$ddq2i])):
							$feed_audio	 	= $dd2_feedback_rec_audio[$ddq2i];
						endif;						
						$feed_video 		= '';
						if ( ! empty($dd2_feedback_video[$ddq2i])):
							$feed_video 	= $dd2_feedback_video[$ddq2i];
						elseif ( ! empty($dd2_feedback_rec_video[$ddq2i])):
							$feed_video 	= $dd2_feedback_rec_video[$ddq2i];
						endif;							
						#--------Feedback----------
						$cfeedData 			= array(
							'qid'			=> ( ! empty($ques_id)) ? $ques_id : '',
							'aid'			=> ( ! empty($ans_id)) ? $ans_id : '',
							'feedback'		=> ( ! empty($dd2_cfeedback[$ddq2i])) ? addslashes($dd2_cfeedback[$ddq2i]) : '',
							'ftype'			=> 1,
							'speech_text'	=> ( ! empty($dd2_feedback_text_to_speech[$ddq2i])) ? addslashes($dd2_feedback_text_to_speech[$ddq2i]) : '',
							'audio'			=> ( ! empty($feed_audio)) ? $feed_audio : '',
							'video'			=> ( ! empty($feed_video)) ? $feed_video : '',
							'screen'		=> ( ! empty($dd2_feedback_rec_screen[$ddq2i])) ? $dd2_feedback_rec_screen[$ddq2i] : '',
							'image'			=> ( ! empty($dd2_feedback_img[$ddq2i])) ? $dd2_feedback_img[$ddq2i] : '',
							'document'		=> ( ! empty($dd2_feedback_doc[$ddq2i])) ? $dd2_feedback_doc[$ddq2i] : ''
						);
						$cfeed_sql 			= "INSERT INTO `feedback_tbl` (`question_id`, `answer_id`, `feedback`, `feedback_type`, `feed_speech_text`, `feed_audio`, `feed_video`, `feed_screen`, `feed_image`, `feed_document`) VALUES (:qid, :aid, :feedback, :ftype, :speech_text, :audio, :video, :screen, :image, :document)";
						$cfeed_stmt 		= $db->prepare($cfeed_sql);
						$cfeed_stmt->execute($cfeedData);
						$ddq2i++; 
						$ddq2itrue++;
					endforeach;
				endif;
			}
		}
		if ($question_type == 7){//Swipe			
			#-------QUESTIONS-DATA---
			$qaudio 					= '';
			if ( ! empty($swipqq_qaudio)):
				$qaudio 				= $swipqq_qaudio;
			elseif ( ! empty($swipqq_rec_audio)):
				$qaudio 				= $swipqq_rec_audio;
			endif;
			$audio 						= '';
			if ( ! empty($swipq_audio)):
				$audio 					= $swipq_audio;
			elseif ( ! empty($swipq_rec_audio)):
				$audio 					= $swipq_rec_audio;
			endif;			
			$video 						= '';
			if ( ! empty($swipq_video)):
				$video 					= $swipq_video;
			elseif ( ! empty($swipq_rec_video)):
				$video 					= $swipq_rec_video;
			endif;			
			$quesData 					= array(
				'scenarioId'			=> $sim_id,
				'qtype'					=> ( ! empty($question_type)) ? $question_type : '',
				'questions'				=> ( ! empty($swipq)) ? addslashes($swipq) : '',				
				'qaudio'				=> ( ! empty($qaudio)) ? $qaudio : '',
				'qspeech_text'			=> ( ! empty($swipqq_text_to_speech)) ? addslashes($swipqq_text_to_speech) : '',
				'speech_text'			=> ( ! empty($swipq_text_to_speech)) ? addslashes($swipq_text_to_speech) : '',
				'audio'					=> ( ! empty($audio)) ? $audio : '',
				'video'					=> ( ! empty($video)) ? $video : '',
				'screen'				=> ( ! empty($swipq_rec_screen)) ? $swipq_rec_screen : '',
				'image'					=> ( ! empty($swipq_img)) ? $swipq_img : '',
				'document'				=> ( ! empty($swipq_doc)) ? $swipq_doc : '',
				'true_option'			=> ( ! empty($swipq_true_option)) ? $swipq_true_option : '',
				'media_file'			=> ( ! empty($videoq_media_file)) ? $videoq_media_file : '',
				'shuffle'				=> (isset($swipq_shuffle) && ! empty($swipq_shuffle)) ? 1 : 0,
				'ques_val_1'			=> ( ! empty($swipq_ques_val_1)) ? $swipq_ques_val_1 : '',
				'ques_val_2'			=> ( ! empty($swipq_ques_val_2)) ? $swipq_ques_val_2 : '',
				'ques_val_3'			=> ( ! empty($swipq_ques_val_3)) ? $swipq_ques_val_3 : '',
				'ques_val_4'			=> ( ! empty($swipq_ques_val_4)) ? $swipq_ques_val_4 : '',
				'ques_val_5'			=> ( ! empty($swipq_ques_val_5)) ? $swipq_ques_val_5 : '',
				'ques_val_6'			=> ( ! empty($swipq_ques_val_6)) ? $swipq_ques_val_6 : '',
				'critical'				=> ( ! empty($swip_criticalQ)) ? $swip_criticalQ : '',
				'status' 				=> 1,
				'user_id'				=> $userId,
				'cur_date'				=> $curdate
			);
			$ques_sql 					= "INSERT INTO `question_tbl` (`scenario_id`,`question_type`, `questions`, `qaudio`,`qspeech_text`, `speech_text`,`audio`, `video`, `screen`, `image`, `document`, `true_options`, `videoq_media_file`, `shuffle`,`ques_val_1`, `ques_val_2`, `ques_val_3`, `ques_val_4`, `ques_val_5`, `ques_val_6`, `critical`,`status`, `uid`, `cur_date`) 
			VALUES (:scenarioId, :qtype, :questions, :qaudio, :qspeech_text, :speech_text, :audio, :video, :screen, :image, :document, :true_option, :media_file, :shuffle, :ques_val_1, :ques_val_2, :ques_val_3, :ques_val_4, :ques_val_5, :ques_val_6,:critical, :status, :user_id, :cur_date)";
			$ques_stmt 					= $db->prepare($ques_sql);
			if ($ques_stmt->execute($quesData)){
				$ques_id 				= $db->lastInsertId();			
				#-----Remove-Old-Feedback-------
				$db->feedback_remove($ques_id);				
				#----------ADD-OPTIONS-------------
				if ( ! empty($swipq_option[0])):
					$swipqi 			= 0; 
					$swiptrue 			= 1;
					foreach ($swipq_option as $answerData):						
						$ansSql 		= "INSERT INTO `answer_tbl` (`question_id`, `choice_option`, `image`, `ans_val1`, `ans_val2`, `ans_val3`, `ans_val4`, `ans_val5`, `ans_val6`, `true_option`, `status`, `uid`, `cur_date`) values (:qid, :option, :image, :val1, :val2, :val3, :val4, :val5, :val6, :true, :status, :user_id, :cur_date)";
						$stmt 			= $db->prepare($ansSql);
						$stmt->execute(array('qid'		=> $ques_id,
											 'option'	=> ( ! empty($swipq_option[$swipqi])) ? addslashes($swipq_option[$swipqi]) : '',
											 'image'	=> ( ! empty($swipq_item_img[$swipqi])) ? $swipq_item_img[$swipqi] : '',
											 'val1'		=> ( ! empty($swip_ans_val1[$swipqi])) ? $swip_ans_val1[$swipqi] : '',
											 'val2'		=> ( ! empty($swip_ans_val2[$swipqi])) ? $swip_ans_val2[$swipqi] : '',
											 'val3'		=> ( ! empty($swip_ans_val3[$swipqi])) ? $swip_ans_val3[$swipqi] : '',
											 'val4'		=> ( ! empty($swip_ans_val4[$swipqi])) ? $swip_ans_val4[$swipqi] : '',
											 'val5'		=> ( ! empty($swip_ans_val5[$swipqi])) ? $swip_ans_val5[$swipqi] : '',
											 'val6'		=> ( ! empty($swip_ans_val6[$swipqi])) ? $swip_ans_val6[$swipqi] : '',
											 'true'		=> ( ! empty($swipq_true_option) && $swipq_true_option == $swiptrue) ? 1 : '',
											 'status'	=> 1,
											 'user_id' 	=> $userId,
											 'cur_date'	=> $curdate));
						$getSwipqAnswerid =  $db->lastInsertId();
						//Add Dummy Question for the option
						$tq			= $db->getTotalQuestionsByScenario(md5($sim_id)) + 1;
						$quesData	= array('sid'				=> $sim_id,
											'option_id'			=> $getSwipqAnswerid,
											'ref_question_id'	=> $ques_id,
											'questions'			=> 'Question '. $tq,
											'status' 			=> 1,
											'user_id'			=> $userId,
											'cur_date'			=> $curdate);
						$ques_sql	= "INSERT INTO `question_tbl` (`scenario_id`, `option_id`,`ref_question_id`,`questions`, `status`, `uid`, `cur_date`) VALUES (:sid, :option_id,:ref_question_id,:questions, :status, :user_id, :cur_date)";
						$ques_stmt	= $db->prepare($ques_sql);
						$ques_stmt->execute($quesData);
						#------FEEDBACK-DATA---
						$feed_audio	= '';
						if ( ! empty($swip_feedback_audio[$swipqi])):
							$feed_audio	= $swip_feedback_audio[$swipqi];
						elseif ( ! empty($swip_feedback_rec_audio[$swipqi])):
							$feed_audio	= $swip_feedback_rec_audio[$swipqi];
						endif;						
						$feed_video	= '';
						if ( ! empty($swip_feedback_video[$swipqi])):
							$feed_video	= $swip_feedback_video[$swipqi];
						elseif ( ! empty($swip_feedback_rec_video[$swipqi])):
							$feed_video	= $swip_feedback_rec_video[$swipqi];
						endif;
						#--------Feedback----------
						$cfeedData	= array('qid'			=> $ques_id,
											'aid'			=> ( ! empty($getSwipqAnswerid)) ? $getSwipqAnswerid : '',
											'feedback'		=> ( ! empty($swip_cfeedback[$swipqi])) ? addslashes($swip_cfeedback[$swipqi]) : '',
											'ftype'			=> 1,
											'speech_text'	=> ( ! empty($swip_feedback_text_to_speech[$swipqi])) ? addslashes($swip_feedback_text_to_speech[$swipqi]) : '',
											'audio'			=> ( ! empty($feed_audio)) ? $feed_audio : '',
											'video'			=> ( ! empty($feed_video)) ? $feed_video : '',
											'screen'		=> ( ! empty($swip_feedback_rec_screen[$swipqi])) ? $swip_feedback_rec_screen[$swipqi] : '',
											'image'			=> ( ! empty($swip_feedback_img[$swipqi])) ? $swip_feedback_img[$swipqi] : '',
											'document'		=> ( ! empty($swip_feedback_doc[$swipqi])) ? $swip_feedback_doc[$swipqi] : '');
						$cfeed_sql	= "INSERT INTO `feedback_tbl` (`question_id`, `answer_id`, `feedback`, `feedback_type`, `feed_speech_text`, `feed_audio`, `feed_video`, `feed_screen`, `feed_image`, `feed_document`) VALUES (:qid, :aid, :feedback, :ftype, :speech_text, :audio, :video, :screen, :image, :document)";
						$cfeed_stmt	= $db->prepare($cfeed_sql);
						$cfeed_stmt->execute($cfeedData);
						$swipqi++; $swiptrue++;
					endforeach;
				endif;
			}
		}
	}
	if ( empty($resdata)):
		if ( ! empty($submit_type) && $submit_type == 1):
			$resdata = array('success' => TRUE, 'all_update' => FALSE, 'msg' => 'Simulation details updated successfully.', 'return_url' => 'branching-video-classic-template.php?add_data=true&sim_id='.md5($sim_id).'&temp_type='.$temp_type.'&ques_id='.base64_encode($ques_id));
		else:
			$resdata = array('success' => TRUE, 'all_update' => TRUE, 'msg' => 'All questions and answers updated successfully.', 'return_url' => 'author-simulation.php');
		endif;
	endif;
	echo json_encode($resdata);
endif;

#-----------------------------------------Update-Branching Video Classic Sim - Saumya ----------------------------------
if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['update_branching_video_classic_sim']) && isset($_POST['sim_id'])):
	extract($_POST);
	$error = FALSE;
	if (empty($sim_page_name[0]) || empty($sim_duration) || empty($sim_title) || empty($passing_marks)):
		$resdata = array('success' => FALSE, 'msg' => 'Please fill all the mandatory fields on add Simulation and Simulation details to proceed.');
		$error = TRUE;
		echo json_encode($resdata); exit;
	elseif (isset($competency_name_1) && empty($competency_name_1) || empty($competency_score_1) || empty($weightage_1)):
		$resdata = array('success' => FALSE, 'msg' => 'Please fill all the mandatory fields on Simulation details to proceed.');
		$error = TRUE;
		echo json_encode($resdata); exit;
	elseif ($error == FALSE):
		$sql = "UPDATE scenario_master SET `Scenario_title` = '". $sim_title ."', `duration` = '". $sim_duration ."', `passing_marks` = '". $passing_marks ."' , `sim_cover_img` = '". $scenario_cover_file ."'";
		#--Add-Web-Object--
		if ( ! empty($web_object)):
			$sql .= ", web_object = '". $web_object ."', web_object_title = '". $web_object_title ."'";
		endif;
		#--Sim-Media-File--
		if ( ! empty($scenario_media_file) && in_array(strtolower(pathinfo($scenario_media_file, PATHINFO_EXTENSION)), $img_valid_exts)):
			$sql .= ", image_url = '". $scenario_media_file ."', video_url = NULL, audio_url = NULL";
		elseif ( ! empty($scenario_media_file) && in_array(strtolower(pathinfo($scenario_media_file, PATHINFO_EXTENSION)), $video_valid_exts)):
			$sql .= ", video_url = '". $scenario_media_file ."', image_url = NULL, audio_url = NULL";
		elseif ( ! empty($scenario_media_file) && in_array(strtolower(pathinfo($scenario_media_file, PATHINFO_EXTENSION)), $audio_valid_exts)):
			$sql .= ", audio_url = '". $scenario_media_file ."', video_url = NULL, image_url = NULL";
		elseif ( ! empty($scenario_media_file) && ! in_array(strtolower(pathinfo($scenario_media_file, PATHINFO_EXTENSION)), $audio_valid_exts) && 
				 ! in_array(strtolower(pathinfo($scenario_media_file, PATHINFO_EXTENSION)), $img_valid_exts) && 
				 ! in_array(strtolower(pathinfo($scenario_media_file, PATHINFO_EXTENSION)), $video_valid_exts)):
			$sql .= ", web_object_file = '". $scenario_media_file ."', video_url = NULL, image_url = NULL, audio_url = NULL";
		endif;
		$sql .= " WHERE scenario_id = '". $sim_id ."'";
		$results = $db->prepare($sql);
		if ($results->execute()):
			#----Competency----
			$delCom	 = "DELETE FROM competency_tbl WHERE scenario_id = '". $sim_id ."'";
			$delComq = $db->prepare($delCom); $delComq->execute();
			$data	 = array('scenario_id'	=> $sim_id,
							 'comp_col_1'	=> (isset($competency_name_1) && ! empty($competency_name_1)) ? addslashes($competency_name_1) : '',
							 'comp_val_1'	=> (isset($competency_score_1) && ! empty($competency_score_1)) ? $competency_score_1 : '',
							 'comp_col_2'	=> (isset($competency_name_2) && ! empty($competency_name_2)) ? addslashes($competency_name_2) : '',
							 'comp_val_2'	=> (isset($competency_score_2) && ! empty($competency_score_2)) ? $competency_score_2 : '',
							 'comp_col_3'	=> (isset($competency_name_3) && ! empty($competency_name_3)) ? addslashes($competency_name_3) : '',
							 'comp_val_3'	=> (isset($competency_score_3) && ! empty($competency_score_3)) ? $competency_score_3 : '',
							 'comp_col_4'	=> (isset($competency_name_4) && ! empty($competency_name_4)) ? addslashes($competency_name_4) : '',
							 'comp_val_4'	=> (isset($competency_score_4) && ! empty($competency_score_4)) ? $competency_score_4 : '',
							 'comp_col_5'	=> (isset($competency_name_5) && ! empty($competency_name_5)) ? addslashes($competency_name_5) : '',
							 'comp_val_5'	=> (isset($competency_score_5) && ! empty($competency_score_5)) ? $competency_score_5 : '',
							 'comp_col_6'	=> (isset($competency_name_6) && ! empty($competency_name_6)) ? addslashes($competency_name_6) : '',
							 'comp_val_6'	=> (isset($competency_score_6) && ! empty($competency_score_6)) ? $competency_score_6 : '',
							 'weightage_1'	=> (isset($weightage_1) && ! empty($weightage_1)) ? $weightage_1 : '',
							 'weightage_2'	=> (isset($weightage_2) && ! empty($weightage_2)) ? $weightage_2 : '',
							 'weightage_3'	=> (isset($weightage_3) && ! empty($weightage_3)) ? $weightage_3 : '',
							 'weightage_4'	=> (isset($weightage_4) && ! empty($weightage_4)) ? $weightage_4 : '',
							 'weightage_5'	=> (isset($weightage_5) && ! empty($weightage_5)) ? $weightage_5 : '',
							 'weightage_6'	=> (isset($weightage_6) && ! empty($weightage_6)) ? $weightage_6 : '',
							 'status' 		=> 1,
							 'user_id' 		=> $userId,
							 'cur_date'		=> $curdate);
			$com_sql  = "INSERT INTO `competency_tbl` (`scenario_id`, `comp_col_1`, `comp_val_1`, `comp_col_2`, `comp_val_2`, `comp_col_3`, `comp_val_3`, `comp_col_4`, `comp_val_4`, `comp_col_5`, `comp_val_5`, `comp_col_6`, `comp_val_6`, `weightage_1`, `weightage_2`, `weightage_3`, `weightage_4`, `weightage_5`, `weightage_6`, `status`, `uid`, `cur_date`) VALUES (:scenario_id, :comp_col_1, :comp_val_1, :comp_col_2, :comp_val_2, :comp_col_3, :comp_val_3, :comp_col_4, :comp_val_4, :comp_col_5, :comp_val_5, :comp_col_6, :comp_val_6, :weightage_1, :weightage_2, :weightage_3, :weightage_4, :weightage_5, :weightage_6, :status, :user_id, :cur_date)";
			$com_stmt = $db->prepare($com_sql);
			$com_stmt->execute($data);
			
			#----PAGES----
			$delPage  = "DELETE FROM sim_pages WHERE scenario_id = '". $sim_id ."'";
			$delPageq = $db->prepare($delPage); $delPageq->execute();
			if ( ! empty($sim_page_name)): $sp = 0;
				foreach ($sim_page_name as $pdata):
					$pageq = "INSERT INTO `sim_pages` (`scenario_id`, `sim_page_name`, `sim_page_desc`, `userid`, `status`, `added_date`) VALUES (:sid, :pname, :pdesc, :user_id, :status, :adate)";
					$page_stmt = $db->prepare($pageq);
					$page_stmt->execute(array('sid'		=> $sim_id,
											  'pname'	=> ( ! empty($pdata)) ? htmlspecialchars($pdata) : '',
											  'pdesc'	=> ( ! empty($sim_page_desc[$sp])) ? $sim_page_desc[$sp] : '',
											  'status' 	=> 1,
											  'user_id'	=> $userId,
											  'adate'	=> $curdate));
				$sp++; endforeach;
			endif;
			
			#----Branding----
			$bandq	  	= "DELETE FROM sim_branding_tbl WHERE scenario_id = '". $sim_id ."'";
			$bandqexe 	= $db->prepare($bandq); $bandqexe->execute();
			$brand_data	= array('sid'				=> $sim_id,
								'bg'				=> ( ! empty($ques_bg_color)) ? $ques_bg_color : '',
								'bg_tr'				=> ( ! empty($ques_bg_transp)) ? $ques_bg_transp : '',
								'obg'				=> ( ! empty($ques_option_bg_color)) ? $ques_option_bg_color : '',
								'obg_tr'			=> ( ! empty($ques_option_bg_transp)) ? $ques_option_bg_transp : '',
								'ohover'			=> ( ! empty($ques_option_hover_color)) ? $ques_option_hover_color : '',
								'oh_tr'				=> ( ! empty($ques_option_hover_transp)) ? $ques_option_hover_transp : '',
								'oselected'			=> ( ! empty($ques_option_select_color)) ? $ques_option_select_color : '',
								'oselected_tr'		=> ( ! empty($ques_option_select_transp)) ? $ques_option_select_transp : '',
								'btn_bg'			=> ( ! empty($btn_bg_color)) ? $btn_bg_color : '',
								'btn_bg_tr'			=> ( ! empty($btn_bg_transp)) ? $btn_bg_transp : '',
								'btn_hover'			=> ( ! empty($btn_hover_color)) ? $btn_hover_color : '',
								'btn_hover_tr'		=> ( ! empty($btn_hover_transp)) ? $btn_hover_transp : '',
								'btn_selected'		=> ( ! empty($btn_select_color)) ? $btn_select_color : '',
								'btn_selected_tr'	=> ( ! empty($btn_select_transp)) ? $btn_select_transp : '',
								'font_type'			=> ( ! empty($font_type)) ? $font_type : '',
								'font_color'		=> ( ! empty($font_color)) ? $font_color : '',
								'font_size'			=> ( ! empty($font_size)) ? $font_size : '',
								'user_id' 			=> $userId,
								'added_date'		=> $curdate);
			$com_sql  = "INSERT INTO `sim_branding_tbl` (`scenario_id`, `ques_bg`, `ques_bg_transparency`, `option_bg`, `option_bg_transparency`, `option_hover`, `option_hover_transparency`, `option_selected`, `option_selected_transparency`, `btn_bg`, `btn_bg_transparency`, `btn_hover`, `btn_hover_transparency`, `btn_selected`, `btn_selected_transparency`, `font_type`, `font_color`, `font_size`, `user_id`, `added_date`) 
						 VALUES (:sid, :bg, :bg_tr, :obg, :obg_tr, :ohover, :oh_tr, :oselected, :oselected_tr, :btn_bg, :btn_bg_tr, :btn_hover, :btn_hover_tr, :btn_selected, :btn_selected_tr, :font_type, :font_color, :font_size, :user_id, :added_date)";
			$com_stmt = $db->prepare($com_sql);
			$com_stmt->execute($brand_data);
		endif;

		//Add Update MCQ Question
		if ( $question_type == 4):
			#-------QUESTIONS-DATA---
			$qaudio = '';
			if ( ! empty($mcqq_qaudio)):
				$qaudio = $mcqq_qaudio;
			elseif ( ! empty($mcqq_rec_audio)):
				$qaudio = $mcqq_rec_audio;
			endif;			
			$audio = '';
			if ( ! empty($mcq_audio)):
				$audio = $mcq_audio;
			elseif ( ! empty($mcq_rec_audio)):
				$audio = $mcq_rec_audio;
			endif;			
			$video = '';
			if ( ! empty($mcq_video)):
				$video = $mcq_video;
			elseif ( ! empty($mcq_rec_video)):
				$video = $mcq_rec_video;
			endif;			
			$quesData	= array('questions'		=> ( ! empty($mcq)) ? $mcq : '',
								'qtype'			=> ( ! empty($question_type)) ? $question_type : '',
								'media_file'	=> ( ! empty($videoq_media_file)) ? $videoq_media_file : '',
								'qaudio'		=> ( ! empty($qaudio)) ? $qaudio : '',
								'qspeech_text'	=> ( ! empty($mcqq_text_to_speech)) ? $mcqq_text_to_speech : '',
								'speech_text'	=> ( ! empty($mcq_text_to_speech)) ? $mcq_text_to_speech : '',
								'audio'			=> ( ! empty($audio)) ? $audio : '',
								'video'			=> ( ! empty($video)) ? $video : '',
								'screen'		=> ( ! empty($mcq_rec_screen)) ? $mcq_rec_screen : '',
								'image'			=> ( ! empty($mcq_img)) ? $mcq_img : '',
								'document'		=> ( ! empty($mcq_doc)) ? $mcq_doc : '',
								'true_option'	=> ( ! empty($videoq_true_option[1][0])) ? $videoq_true_option[1][0] : '',
								'shuffle'		=> (isset($mcq_shuffle) && ! empty($mcq_shuffle)) ? 1 : 0,
								'ques_val_1'	=> ( ! empty($mcq_ques_val_1)) ? $mcq_ques_val_1 : '',
								'ques_val_2'	=> ( ! empty($mcq_ques_val_2)) ? $mcq_ques_val_2 : '',
								'ques_val_3'	=> ( ! empty($mcq_ques_val_3)) ? $mcq_ques_val_3 : '',
								'ques_val_4'	=> ( ! empty($mcq_ques_val_4)) ? $mcq_ques_val_4 : '',
								'ques_val_5'	=> ( ! empty($mcq_ques_val_5)) ? $mcq_ques_val_5 : '',
								'ques_val_6'	=> ( ! empty($mcq_ques_val_6)) ? $mcq_ques_val_6 : '',
								'critical'		=> ( ! empty($mcq_criticalQ)) ? $mcq_criticalQ : '',
								'status' 		=> 1,
								'user_id'		=> $userId,
								'cur_date'		=> $curdate,
								'qid'			=> $ques_id);
			$ques_sql	= "UPDATE `question_tbl` SET `questions`=:questions, `question_type`=:qtype, `videoq_media_file`=:media_file,`qaudio`=:qaudio, `qspeech_text`=:qspeech_text, `speech_text`=:speech_text, `audio`=:audio, `video`=:video, `screen`=:screen, `image`=:image, `document`=:document, `true_options`=:true_option, `shuffle`=:shuffle, `ques_val_1`=:ques_val_1, `ques_val_2`=:ques_val_2, `ques_val_3`=:ques_val_3, `ques_val_4`=:ques_val_4, `ques_val_5`=:ques_val_5, `ques_val_6`=:ques_val_6, `critical`=:critical,`status`=:status, `uid`=:user_id, `cur_date`=:cur_date WHERE question_id=:qid";
			$ques_stmt	= $db->prepare($ques_sql);
			#----------ADD-OPTIONS-------------
			if ($ques_stmt->execute($quesData)):
				#-----Remove-Old-Feedback-------
				$db->feedback_remove($ques_id);
				if ( ! empty($mcq_option[0])):
					$mcqi = 0; $mcqtrue = 1;
					foreach ($mcq_option as $answerData):
						if ( ! empty($updateMCQAnswerid[$mcqi])):
							if ( ! empty($endSimulationMTF)) {
								if (in_array($mcqtrue, $endSimulationMTF)) {
									$endSim = 1;
								} else {
									$endSim = 0;
								}
								if(isset($endSim) && $endSim > 0){
									$mappedQuestionMtf[$mcqi] = '';
								}
							}
							$ans_id	= $updateMCQAnswerid[$mcqi];
							$ansSql	= "UPDATE `answer_tbl` SET `choice_option`=:option, `End_Sim`=:End_Sim,`nextQid`=:nextQid,`ans_val1`=:val1, `ans_val2`=:val2, `ans_val3`=:val3, `ans_val4`=:val4, `ans_val5`=:val5, `ans_val6`=:val6, `true_option`=:true_option, `status`=:status, `uid`=:user_id, `cur_date`=:cur_date WHERE answer_id=:aid";
							$stmt	= $db->prepare($ansSql);
							$stmt->execute(array('aid'				=> $ans_id,
												'option'			=> ( ! empty($mcq_option[$mcqi])) ? $mcq_option[$mcqi] : '',
												'End_Sim'			=> ( ! empty($endSim)) ? addslashes($endSim) : '',
												'nextQid'			=> ( ! empty($mappedQuestionMtf[$mcqi])) ? addslashes($mappedQuestionMtf[$mcqi]) : '',
												'val1'				=> ( ! empty($mcq_ans_val1[$mcqi])) ? $mcq_ans_val1[$mcqi] : '',
												'val2'				=> ( ! empty($mcq_ans_val2[$mcqi])) ? $mcq_ans_val2[$mcqi] : '',
												'val3'				=> ( ! empty($mcq_ans_val3[$mcqi])) ? $mcq_ans_val3[$mcqi] : '',
												'val4'				=> ( ! empty($mcq_ans_val4[$mcqi])) ? $mcq_ans_val4[$mcqi] : '',
												'val5'				=> ( ! empty($mcq_ans_val5[$mcqi])) ? $mcq_ans_val5[$mcqi] : '',
												'val6'				=> ( ! empty($mcq_ans_val6[$mcqi])) ? $mcq_ans_val6[$mcqi] : '',
												'true_option'		=> ( ! empty($mcq_true_option) && $mcq_true_option == $mcqtrue) ? 1 : '',
												'status'			=> 1,
												'user_id' 			=> $userId,
												'cur_date'			=> $curdate));

							//delete question if endsim
							/*if(empty($cue_point[0])):
								if($endSim == 1):
									$delQues	 			= "DELETE FROM question_tbl WHERE option_id = '". $ans_id ."'";
									$delQuesq 				= $db->prepare($delQues); 
									$delQuesq->execute();
								else:
									//check if dummy question is there
									$checkQues	 			= "SELECT * FROM question_tbl WHERE option_id = '". $ans_id ."' and ref_question_id='".$ques_id."'";
									$checkQuesq 				= $db->prepare($checkQues); 
									$checkQuesq->execute();
									$checkQResult 					= $checkQuesq->fetchAll();
									
									if(empty($checkQResult[0])){
										//Add Dummy Question
										$tq 					= $db->getTotalQuestionsByScenario(md5($sim_id)) + 1;
										$quesData 				= array(
																	'sid'					=> $sim_id,
																	'option_id'				=> $updateMCQAnswerid[$mcqi],
																	'ref_question_id'		=> $ques_id,
																	'questions'				=> 'Question '. $tq,
																	'status' 				=> 1,
																	'user_id'				=> $userId,
																	'cur_date'				=> $curdate
																);
										$ques_sql 				= "INSERT INTO `question_tbl` (`scenario_id`, `option_id`,`ref_question_id`,`questions`, `status`, `uid`, `cur_date`) VALUES (:sid, :option_id,:ref_question_id,:questions, :status, :user_id, :cur_date)";
										$ques_stmt 				= $db->prepare($ques_sql);
										$ques_stmt->execute($quesData);
									}										
								endif;
							endif;*/
						else:
							if ( ! empty($endSimulationMTF)){
								if (in_array($mcqtrue, $endSimulationMTF)) {
									$endSim = 1;
								} else {
									$endSim = 0;
								}
								if (isset($endSim) && $endSim > 0){
									$mappedQuestionMtf[$mcqi] = '';
								}
							}
							$ansSql	= "INSERT INTO `answer_tbl` (`question_id`, `choice_option`,`End_Sim`, `nextQid`,`ans_val1`, `ans_val2`, `ans_val3`, `ans_val4`, `ans_val5`, `ans_val6`, `true_option`, `status`, `uid`, `cur_date`) values (:qid, :option, :End_Sim,:nextQid,:val1, :val2, :val3, :val4, :val5, :val6, :true_option, :status, :user_id, :cur_date)";
							$stmt	= $db->prepare($ansSql);
							$stmt->execute(array('qid'			=> $ques_id,
												 'option'		=> ( ! empty($answerData)) ? $answerData : '',
												 'End_Sim'		=> ( ! empty($endSim)) ? addslashes($endSim) : '',
												 'nextQid'		=> ( ! empty($mappedQuestionMtf[$mcqi])) ? addslashes($mappedQuestionMtf[$mcqi]) : '',
												 'val1'			=> ( ! empty($mcq_ans_val1[$mcqi])) ? $mcq_ans_val1[$mcqi] : '',
												 'val2'			=> ( ! empty($mcq_ans_val2[$mcqi])) ? $mcq_ans_val2[$mcqi] : '',
												 'val3'			=> ( ! empty($mcq_ans_val3[$mcqi])) ? $mcq_ans_val3[$mcqi] : '',
												 'val4'			=> ( ! empty($mcq_ans_val4[$mcqi])) ? $mcq_ans_val4[$mcqi] : '',
												 'val5'			=> ( ! empty($mcq_ans_val5[$mcqi])) ? $mcq_ans_val5[$mcqi] : '',
												 'val6'			=> ( ! empty($mcq_ans_val6[$mcqi])) ? $mcq_ans_val6[$mcqi] : '',
												 'true_option'	=> ( ! empty($mcq_true_option) && $mcq_true_option == $mcqtrue) ? 1 : '',
												 'status'		=> 1,
												 'user_id' 		=> $userId,
												 'cur_date'		=> $curdate));
							$ans_id = $db->lastInsertId();
						endif;
						#------Correct-FEEDBACK-DATA---
						$feed_audio = '';
						if ( ! empty($mcq_feedback_audio[$mcqi])):
							$feed_audio = $mcq_feedback_audio[$mcqi];
						elseif ( ! empty($mcq_feedback_rec_audio[$mcqi])):
							$feed_audio = $mcq_feedback_rec_audio[$mcqi];
						endif;						
						$feed_video = '';
						if ( ! empty($mcq_feedback_video[$mcqi])):
							$feed_video = $mcq_feedback_video[$mcqi];
						elseif ( ! empty($mcq_feedback_rec_video[$mcqi])):
							$feed_video = $mcq_feedback_rec_video[$mcqi];
						endif;
						#--------Correct-Feedback----------
						$cfeedData = array('qid'			=> ( ! empty($ques_id)) ? $ques_id : '',
										   'aid'			=> ( ! empty($ans_id)) ? $ans_id : '',
										   'feedback'		=> ( ! empty($mcq_cfeedback[$mcqi])) ? htmlspecialchars($mcq_cfeedback[$mcqi]) : '',
										   'ftype'			=> 1,
										   'speech_text'	=> ( ! empty($mcq_feedback_text_to_speech[$mcqi])) ? htmlspecialchars($mcq_feedback_text_to_speech[$mcqi]) : '',
										   'audio'			=> ( ! empty($feed_audio)) ? $feed_audio : '',
										   'video'			=> ( ! empty($feed_video)) ? $feed_video : '',
										   'screen'			=> ( ! empty($mcq_feedback_rec_screen[$mcqi])) ? $mcq_feedback_rec_screen[$mcqi] : '',
										   'image'			=> ( ! empty($mcq_feedback_img[$mcqi])) ? $mcq_feedback_img[$mcqi] : '',
										   'document'		=> ( ! empty($mcq_feedback_doc[$mcqi])) ? $mcq_feedback_doc[$mcqi] : '');
						$cfeed_sql  = "INSERT INTO `feedback_tbl` (`question_id`, `answer_id`, `feedback`, `feedback_type`, `feed_speech_text`, `feed_audio`, `feed_video`, `feed_screen`, `feed_image`, `feed_document`) VALUES (:qid, :aid, :feedback, :ftype, :speech_text, :audio, :video, :screen, :image, :document)";
						$cfeed_stmt = $db->prepare($cfeed_sql);
						$cfeed_stmt->execute($cfeedData);
						$mcqi++; $mcqtrue++;
					endforeach;
				endif;
			endif;
		endif;
		//Add update DND question
		if ($question_type == 8):
			#-------QUESTIONS-DATA---
			$qaudio = '';
			if ( ! empty($ddqq_qaudio)):
				$qaudio = $ddqq_qaudio;
			elseif ( ! empty($ddqq_rec_audio)):
				$qaudio = $ddqq_rec_audio;
			endif;		
			$audio = '';
			if ( ! empty($ddq_audio)):
				$audio = $ddq_audio;
			elseif ( ! empty($ddq_rec_audio)):
				$audio = $ddq_rec_audio;
			endif;			
			$video = '';
			if ( ! empty($ddq_video)):
				$video = $ddq_video;
			elseif ( ! empty($ddq_rec_video)):
				$video = $ddq_rec_video;
			endif;				
			$quesData 						= array(
				'questions'					=> ( ! empty($ddq)) ? addslashes($ddq) : '',
				'seleted_drag' 				=> 2,
				'qtype'						=> ( ! empty($question_type)) ? $question_type : '',
				'media_file'				=> ( ! empty($videoq_media_file)) ? $videoq_media_file : '',
				'qaudio'					=> ( ! empty($qaudio)) ? $qaudio : '',
				'qspeech_text'				=> ( ! empty($ddqq_text_to_speech)) ? addslashes($ddqq_text_to_speech) : '',
				'speech_text'				=> ( ! empty($ddq_text_to_speech)) ? addslashes($ddq_text_to_speech) : '',
				'audio'						=> ( ! empty($audio)) ? $audio : '',
				'video'						=> ( ! empty($video)) ? $video : '',
				'screen'					=> ( ! empty($ddq_rec_screen)) ? $ddq_rec_screen : '',
				'image'						=> ( ! empty($ddq_img)) ? $ddq_img : '',
				'document'					=> ( ! empty($ddq_doc)) ? $ddq_doc : '',
				'shuffle'					=> (isset($ddq_shuffle2) && ! empty($ddq_shuffle2)) ? 1 : 0,
				'true_options'				=> ( ! empty($ddq_true_option)) ? $ddq_true_option : '',
				'ques_val_1'				=> ( ! empty($ddq_ques_val_1)) ? $ddq_ques_val_1 : '',
				'ques_val_2'				=> ( ! empty($ddq_ques_val_2)) ? $ddq_ques_val_2 : '',
				'ques_val_3'				=> ( ! empty($ddq_ques_val_3)) ? $ddq_ques_val_3 : '',
				'ques_val_4'				=> ( ! empty($ddq_ques_val_4)) ? $ddq_ques_val_4 : '',
				'ques_val_5'				=> ( ! empty($ddq_ques_val_5)) ? $ddq_ques_val_5 : '',
				'ques_val_6'				=> ( ! empty($ddq_ques_val_6)) ? $ddq_ques_val_6 : '',
				'critical'					=> ( ! empty($dnd_criticalQ)) ? $dnd_criticalQ : '',
				'status' 					=> 1,
				'user_id'					=> $userId,
				'cur_date'					=> $curdate,
				'qid'						=> $ques_id);
			$ques_sql 						= "UPDATE `question_tbl` SET `questions`=:questions,`videoq_media_file`=:media_file, `question_type`=:qtype, `seleted_drag_option`=:seleted_drag, `qaudio`=:qaudio, `qspeech_text`=:qspeech_text, `speech_text`=:speech_text, `audio`=:audio, `video`=:video, `screen`=:screen, `image`=:image, `document`=:document, `shuffle`=:shuffle, `true_options`=:true_options, `ques_val_1`=:ques_val_1, `ques_val_2`=:ques_val_2, `ques_val_3`=:ques_val_3, `ques_val_4`=:ques_val_4, `ques_val_5`=:ques_val_5, `ques_val_6`=:ques_val_6, `critical`=:critical,`status`=:status, `uid`=:user_id, `cur_date`=:cur_date WHERE question_id=:qid";
			$ques_stmt 						= $db->prepare($ques_sql);			
			#---DROP-OPTION---- 			
			if ($ques_stmt->execute($quesData)){
				#-----Remove-Old-Feedback------
				$db->feedback_remove($ques_id);		
				#------DRAG-OPTIONS------
				if ( ! empty($ddq2_drop)):
					if ( ! empty($updateDDq2Answerid)):				
						#----DROP-TARGET----
						$ansSql 							= "UPDATE `sub_options_tbl` SET `sub_option`=:option, `image`=:image WHERE answer_id=:aid";
						$stmt 								= $db->prepare($ansSql);
						$stmt->execute(array('aid' => $updateDDq2Answerid, 'option' => ( ! empty($ddq2_drop)) ? addslashes($ddq2_drop) : '', 'image' => ( ! empty($ddq2_item_img)) ? $ddq2_item_img : ''));
						#-----DRAG-ITEMS------
						$ddq2iu 							= 0; 
						$ddq2true 							= 1;
						$dndOffset							= 1;	
						$endSim 							= '';			
						foreach ($ddq2_drag as $subAnswerData):
							if(!empty($endSimulationDND)){
								if (in_array($dndOffset, $endSimulationDND)) {
									$endSim 				= 1;
								} else {
									$endSim 				= 0;
								}
							} 
							if(isset($endSim) && $endSim > 0){
								$mappedQuestionDnD[$ddq2iu] = '';
							}
							if ( ! empty($updateDDq2SubAnswerid[$ddq2iu])):
								$getOpId 					= $updateDDq2SubAnswerid[$ddq2iu];
								$updateAnsSql  				= "UPDATE `answer_tbl` SET `choice_option`=:option, `image`=:image, `End_Sim`=:End_Sim,`nextQid`=:nextQid,`ans_val1`=:ans_val1, `ans_val2`=:ans_val2, `ans_val3`=:ans_val3, `ans_val4`=:ans_val4, `ans_val5`=:ans_val5, `ans_val6`=:ans_val6, `true_option`=:true, `status`=:status, `uid`=:user_id, `cur_date`=:cur_date WHERE answer_id=:aid";
								$updateAnsstmt 				= $db->prepare($updateAnsSql);
								$updateAnsstmt->execute(array(
									'aid' 					=> $getOpId,
									'option'				=> ( ! empty($ddq2_drag[$ddq2iu])) ? $ddq2_drag[$ddq2iu] : '',
									'image'					=> ( ! empty($ddq2_drag_item_img[$ddq2iu])) ? $ddq2_drag_item_img[$ddq2iu] : '',
									'End_Sim'				=> ( ! empty($endSim)) ? addslashes($endSim) : '',
									'nextQid'				=> ( ! empty($mappedQuestionDnD[$ddq2iu])) ? addslashes($mappedQuestionDnD[$ddq2iu]) : '',
									'ans_val1'				=> ( ! empty($dd2_ans_val1[$ddq2iu])) ? $dd2_ans_val1[$ddq2iu] : '',
									'ans_val2'				=> ( ! empty($dd2_ans_val2[$ddq2iu])) ? $dd2_ans_val2[$ddq2iu] : '',
									'ans_val3'				=> ( ! empty($dd2_ans_val3[$ddq2iu])) ? $dd2_ans_val3[$ddq2iu] : '',
									'ans_val4'				=> ( ! empty($dd2_ans_val4[$ddq2iu])) ? $dd2_ans_val4[$ddq2iu] : '',
									'ans_val5'				=> ( ! empty($dd2_ans_val5[$ddq2iu])) ? $dd2_ans_val5[$ddq2iu] : '',
									'ans_val6'				=> ( ! empty($dd2_ans_val6[$ddq2iu])) ? $dd2_ans_val6[$ddq2iu] : '',
									'true'					=> ( ! empty($ddq_true_option) && $ddq_true_option == $ddq2true) ? 1 : '',
									'status'				=> 1,
									'user_id' 				=> $userId,
									'cur_date'				=> $curdate
								));				
							else:
								$ansSql = "INSERT INTO `answer_tbl` (`question_id`, `choice_option`, `End_Sim`,`nextQid`,`image`, `ans_val1`, `ans_val2`, `ans_val3`, `ans_val4`, `ans_val5`, `ans_val6`, `true_option`, `status`, `uid`, `cur_date`) values (:qid, :option, :End_Sim,:nextQid,:image, :ans_val1, :ans_val2, :ans_val3, :ans_val4, :ans_val5, :ans_val6, :true, :status, :user_id, :cur_date)";
								$stmt = $db->prepare($ansSql);
								$stmt->execute(array('qid'		=> $ques_id,
													'option'	=> ( ! empty($ddq2_drag[$ddq2iu])) ? addslashes($ddq2_drag[$ddq2iu]) : '',
													'End_Sim'	=> ( ! empty($endSim)) ? addslashes($endSim) : '',
													'nextQid'	=> ( ! empty($mappedQuestionDnD[$ddq2iu])) ? addslashes($mappedQuestionDnD[$ddq2iu]) : '',
													'image'		=> ( ! empty($ddq2_drag_item_img[$ddq2iu])) ? $ddq2_drag_item_img[$ddq2iu] : '',
													'ans_val1'	=> ( ! empty($dd2_ans_val1[$ddq2iu])) ? $dd2_ans_val1[$ddq2iu] : '',
													'ans_val2'	=> ( ! empty($dd2_ans_val2[$ddq2iu])) ? $dd2_ans_val2[$ddq2iu] : '',
													'ans_val3'	=> ( ! empty($dd2_ans_val3[$ddq2iu])) ? $dd2_ans_val3[$ddq2iu] : '',
													'ans_val4'	=> ( ! empty($dd2_ans_val4[$ddq2iu])) ? $dd2_ans_val4[$ddq2iu] : '',
													'ans_val5'	=> ( ! empty($dd2_ans_val5[$ddq2iu])) ? $dd2_ans_val5[$ddq2iu] : '',
													'ans_val6'	=> ( ! empty($dd2_ans_val6[$ddq2iu])) ? $dd2_ans_val6[$ddq2iu] : '',
													'true'		=> ( ! empty($ddq_true_option) && $ddq_true_option == $ddq2true) ? 1 : '',
													'status'	=> 1,
													'user_id' 	=> $userId,
													'cur_date'	=> $curdate));
								$getOpId = $db->lastInsertId();
							endif;							
							#-----FEEDBACK-DATA---
							$feed_audio = '';
							if ( ! empty($dd2_feedback_audio[$ddq2iu])):
								$feed_audio = $dd2_feedback_audio[$ddq2iu];
							elseif ( ! empty($dd2_feedback_rec_audio[$ddq2iu])):
								$feed_audio = $dd2_feedback_rec_audio[$ddq2iu];
							endif;							
							$feed_video = '';
							if ( ! empty($dd2_feedback_video[$ddq2iu])):
								$feed_video = $dd2_feedback_video[$ddq2iu];
							elseif ( ! empty($dd2_feedback_rec_video[$ddq2iu])):
								$feed_video = $dd2_feedback_rec_video[$ddq2iu];
							endif;							
							#--------Feedback----------
							$cfeedData = array('qid'			=> ( ! empty($ques_id)) ? $ques_id : '',
											   'aid'			=> ( ! empty($getOpId)) ? $getOpId : '',
											   'feedback'		=> ( ! empty($dd2_cfeedback[$ddq2iu])) ? addslashes($dd2_cfeedback[$ddq2iu]) : '',
											   'ftype'			=> 1,
											   'speech_text'	=> ( ! empty($dd2_feedback_text_to_speech[$ddq2iu])) ? addslashes($dd2_feedback_text_to_speech[$ddq2iu]) : '',
											   'audio'			=> ( ! empty($feed_audio)) ? $feed_audio : '',
											   'video'			=> ( ! empty($feed_video)) ? $feed_video : '',
											   'screen'			=> ( ! empty($dd2_feedback_rec_screen[$ddq2iu])) ? $dd2_feedback_rec_screen[$ddq2iu] : '',
											   'image'			=> ( ! empty($dd2_feedback_img[$ddq2iu])) ? $dd2_feedback_img[$ddq2iu] : '',
											   'document'		=> ( ! empty($dd2_feedback_doc[$ddq2iu])) ? $dd2_feedback_doc[$ddq2iu] : '');
							$cfeed_sql = "INSERT INTO `feedback_tbl` (`question_id`, `answer_id`, `feedback`, `feedback_type`, `feed_speech_text`, `feed_audio`, `feed_video`, `feed_screen`, `feed_image`, `feed_document`) VALUES (:qid, :aid, :feedback, :ftype, :speech_text, :audio, :video, :screen, :image, :document)";
							$cfeed_stmt = $db->prepare($cfeed_sql);
							$cfeed_stmt->execute($cfeedData);
							$ddq2iu++; $ddq2true++; $dndOffset++;
						endforeach;
					else:					
						#----DROP-TARGET----
						$ansSql = "INSERT INTO `sub_options_tbl` (`question_id`, `sub_option`, `image`) values (:qid, :option, :image)";
						$stmt = $db->prepare($ansSql);
						$stmt->execute(array('qid' => $ques_id, 'option' => ( ! empty($ddq2_drop)) ? addslashes($ddq2_drop) : '', 'image' => ( ! empty($ddq2_item_img)) ? $ddq2_item_img : ''));
						#-----DRAG-ITEMS------
						$ddq2i = 0; $ddq2itrue = 1;
						foreach ($ddq2_drag as $answerData):
							if ( ! empty($endSimulationDND)){
								if (in_array($ddq2itrue, $endSimulationDND)) {
									$endSim	= 1;
								} else {
									$endSim	= 0;
								}
							}
							if (isset($endSim) && $endSim > 0){
								$mappedQuestionDnD[$ddq2i] = '';
							}
							$ansSql = "INSERT INTO `answer_tbl` (`question_id`, `choice_option`,`End_Sim`, `nextQid`,`image`, `ans_val1`, `ans_val2`, `ans_val3`, `ans_val4`, `ans_val5`, `ans_val6`, `true_option`, `status`, `uid`, `cur_date`) values (:qid, :choice, :End_Sim,:nextQid,:image, :ans_val1, :ans_val2, :ans_val3, :ans_val4, :ans_val5, :ans_val6, :true, :status, :user_id, :cur_date)";
							$stmt = $db->prepare($ansSql);
							$stmt->execute(array('qid'		=> $ques_id,
												'choice'	=> ( ! empty($ddq2_drag[$ddq2i])) ? addslashes($ddq2_drag[$ddq2i]) : '',
												'End_Sim'	=> ( ! empty($endSim)) ? addslashes($endSim) : '',
												'nextQid'	=> ( ! empty($mappedQuestionDnD[$ddq2i])) ? addslashes($mappedQuestionDnD[$ddq2i]) : '',
												'image'		=> ( ! empty($ddq2_drag_item_img[$ddq2i])) ? $ddq2_drag_item_img[$ddq2i] : '',
												'ans_val1'	=> ( ! empty($dd2_ans_val1[$ddq2i])) ? $dd2_ans_val1[$ddq2i] : '',
												'ans_val2'	=> ( ! empty($dd2_ans_val2[$ddq2i])) ? $dd2_ans_val2[$ddq2i] : '',
												'ans_val3'	=> ( ! empty($dd2_ans_val3[$ddq2i])) ? $dd2_ans_val3[$ddq2i] : '',
												'ans_val4'	=> ( ! empty($dd2_ans_val4[$ddq2i])) ? $dd2_ans_val4[$ddq2i] : '',
												'ans_val5'	=> ( ! empty($dd2_ans_val5[$ddq2i])) ? $dd2_ans_val5[$ddq2i] : '',
												'ans_val6'	=> ( ! empty($dd2_ans_val6[$ddq2i])) ? $dd2_ans_val6[$ddq2i] : '',
												'true'		=> ( ! empty($ddq_true_option) && $ddq_true_option == $ddq2itrue) ? 1 : '',
												'status'	=> 1,
												'user_id' 	=> $userId,
												'cur_date'	=> $curdate));							
							$ans_id = $db->lastInsertId();
							#-----FEEDBACK-DATA---
							$feed_audio = '';
							if ( ! empty($dd2_feedback_audio[$ddq2i])):
								$feed_audio = $dd2_feedback_audio[$ddq2i];
							elseif ( ! empty($dd2_feedback_rec_audio[$ddq2i])):
								$feed_audio = $dd2_feedback_rec_audio[$ddq2i];
							endif;
							$feed_video = '';
							if ( ! empty($dd2_feedback_video[$ddq2i])):
								$feed_video = $dd2_feedback_video[$ddq2i];
							elseif ( ! empty($dd2_feedback_rec_video[$ddq2i])):
								$feed_video = $dd2_feedback_rec_video[$ddq2i];
							endif;
							#--------Feedback----------
							$cfeedData = array('qid'			=> ( ! empty($ques_id)) ? $ques_id : '',
											   'aid'			=> ( ! empty($ans_id)) ? $ans_id : '',
											   'feedback'		=> ( ! empty($dd2_cfeedback[$ddq2i])) ? addslashes($dd2_cfeedback[$ddq2i]) : '',
											   'ftype'			=> 1,
											   'speech_text'	=> ( ! empty($dd2_feedback_text_to_speech[$ddq2i])) ? addslashes($dd2_feedback_text_to_speech[$ddq2i]) : '',
											   'audio'			=> ( ! empty($feed_audio)) ? $feed_audio : '',
											   'video'			=> ( ! empty($feed_video)) ? $feed_video : '',
											   'screen'			=> ( ! empty($dd2_feedback_rec_screen[$ddq2i])) ? $dd2_feedback_rec_screen[$ddq2i] : '',
											   'image'			=> ( ! empty($dd2_feedback_img[$ddq2i])) ? $dd2_feedback_img[$ddq2i] : '',
											   'document'		=> ( ! empty($dd2_feedback_doc[$ddq2i])) ? $dd2_feedback_doc[$ddq2i] : '');
							$cfeed_sql = "INSERT INTO `feedback_tbl` (`question_id`, `answer_id`, `feedback`, `feedback_type`, `feed_speech_text`, `feed_audio`, `feed_video`, `feed_screen`, `feed_image`, `feed_document`) VALUES (:qid, :aid, :feedback, :ftype, :speech_text, :audio, :video, :screen, :image, :document)";
							$cfeed_stmt = $db->prepare($cfeed_sql);
							$cfeed_stmt->execute($cfeedData);
							$ddq2i++; $ddq2itrue++;
						endforeach;
					endif;
				endif;
			}
		endif;
		//Add Update Swipe Question
		if ($question_type == 7):
			#-------QUESTIONS-DATA---
			$qaudio = '';
			if ( ! empty($swipqq_qaudio)):
				$qaudio = $swipqq_qaudio;
			elseif ( ! empty($swipqq_rec_audio)):
				$qaudio = $swipqq_rec_audio;
			endif;

			$audio = '';
			if ( ! empty($swipq_audio)):
				$audio = $swipq_audio;
			elseif ( ! empty($swipq_rec_audio)):
				$audio = $swipq_rec_audio;
			endif;
			
			$video = '';
			if ( ! empty($swipq_video)):
				$video = $swipq_video;
			elseif ( ! empty($swipq_rec_video)):
				$video = $swipq_rec_video;
			endif;
			$quesData 					= array(
				'questions'				=> ( ! empty($swipq)) ? addslashes($swipq) : '',
				'qtype'					=> ( ! empty($question_type)) ? $question_type : '',
				'qaudio'				=> ( ! empty($qaudio)) ? $qaudio : '',
				'qspeech_text'			=> ( ! empty($swipqq_text_to_speech)) ? addslashes($swipqq_text_to_speech) : '',
				'speech_text'			=> ( ! empty($swipq_text_to_speech)) ? addslashes($swipq_text_to_speech) : '',
				'audio'					=> ( ! empty($audio)) ? $audio : '',
				'video'					=> ( ! empty($video)) ? $video : '',
				'screen'				=> ( ! empty($swipq_rec_screen)) ? $swipq_rec_screen : '',
				'image'					=> ( ! empty($swipq_img)) ? $swipq_img : '',
				'document'				=> ( ! empty($swipq_doc)) ? $swipq_doc : '',
				'true_option'			=> ( ! empty($swipq_true_option)) ? $swipq_true_option : '',
				'videoq_media_file'		=> ( ! empty($videoq_media_file)) ? $videoq_media_file : '',
				'shuffle'				=> (isset($swipq_shuffle) && ! empty($swipq_shuffle)) ? 1 : 0,
				'ques_val_1'			=> ( ! empty($swipq_ques_val_1)) ? $swipq_ques_val_1 : '',
				'ques_val_2'			=> ( ! empty($swipq_ques_val_2)) ? $swipq_ques_val_2 : '',
				'ques_val_3'			=> ( ! empty($swipq_ques_val_3)) ? $swipq_ques_val_3 : '',
				'ques_val_4'			=> ( ! empty($swipq_ques_val_4)) ? $swipq_ques_val_4 : '',
				'ques_val_5'			=> ( ! empty($swipq_ques_val_5)) ? $swipq_ques_val_5 : '',
				'ques_val_6'			=> ( ! empty($swipq_ques_val_6)) ? $swipq_ques_val_6 : '',
				'critical'				=> ( ! empty($swip_criticalQ)) ? $swip_criticalQ : '',
				'status' 				=> 1,
				'user_id'				=> $userId,
				'cur_date'				=> $curdate,
				'qid'					=> $ques_id
			);
			$ques_sql  					= "UPDATE `question_tbl` SET `questions`=:questions, `question_type`=:qtype, `qaudio`=:qaudio, `qspeech_text`=:qspeech_text, `speech_text`=:speech_text, `audio`=:audio, `video`=:video, `screen`=:screen, `image`=:image, `document`=:document, `true_options`=:true_option, `videoq_media_file`=:videoq_media_file,`shuffle`=:shuffle, `ques_val_1`=:ques_val_1, `ques_val_2`=:ques_val_2, `ques_val_3`=:ques_val_3, `ques_val_4`=:ques_val_4, `ques_val_5`=:ques_val_5, `ques_val_6`=:ques_val_6,`critical`=:critical,`status`=:status, `uid`=:user_id, `cur_date`=:cur_date WHERE question_id=:qid";
			$ques_stmt 					= $db->prepare($ques_sql);						
			if ($ques_stmt->execute($quesData)):	
				#-----Remove-Old-Feedback-------
				$db->feedback_remove($ques_id);		
				#----------ADD-OPTIONS-------------
				if ( ! empty($swipq_option[0])):
					$swipqi 							= 0; 
					$swiptrue 							= 1;//mtfOffset
					$endSimq							= ''; //blank variable
					foreach ($swipq_option as $answerData):
						if(!empty($endSimulationSwipe)){
							if (in_array($swiptrue, $endSimulationSwipe)) {
								$endSim 				= 1;
							} else {
								$endSim 				= 0;
							}
						}
						if(isset($endSim) && $endSim > 0){
							$mappedQuestionSwipe[$swipqi] = '';
						}
						if ( ! empty($updateSwipqAnswerid[$swipqi])):					 
							$getSwipqAnswerid 			= $updateSwipqAnswerid[$swipqi];
							$ansSql 					= "UPDATE `answer_tbl` SET `choice_option`=:option, `nextQid`=:nextQid,`End_Sim`=:End_Sim,`image`=:image, `ans_val1`=:val1, `ans_val2`=:val2, `ans_val3`=:val3, `ans_val4`=:val4, `ans_val5`=:val5, `ans_val6`=:val6, `true_option`=:true, `status`=:status, `uid`=:user_id, `cur_date`=:cur_date WHERE answer_id=:aid";
							$stmt 						= $db->prepare($ansSql);
							$stmt->execute(array(
								'aid' 					=> $getSwipqAnswerid,
								'option'				=> ( ! empty($swipq_option[$swipqi])) ? addslashes($swipq_option[$swipqi]) : '',
								'nextQid'				=> ( ! empty($mappedQuestionSwipe[$swipqi])) ? addslashes($mappedQuestionSwipe[$swipqi]) : '',
								'End_Sim'				=> ( ! empty($endSim)) ? addslashes($endSim) : '',
								'image'					=> ( ! empty($swipq_item_img[$swipqi])) ? $swipq_item_img[$swipqi] : '',
								'val1'					=> ( ! empty($swip_ans_val1[$swipqi])) ? $swip_ans_val1[$swipqi] : '',
								'val2'					=> ( ! empty($swip_ans_val2[$swipqi])) ? $swip_ans_val2[$swipqi] : '',
								'val3'					=> ( ! empty($swip_ans_val3[$swipqi])) ? $swip_ans_val3[$swipqi] : '',
								'val4'					=> ( ! empty($swip_ans_val4[$swipqi])) ? $swip_ans_val4[$swipqi] : '',
								'val5'					=> ( ! empty($swip_ans_val5[$swipqi])) ? $swip_ans_val5[$swipqi] : '',
								'val6'					=> ( ! empty($swip_ans_val6[$swipqi])) ? $swip_ans_val6[$swipqi] : '',
								'true'					=> ( ! empty($swipq_true_option) && $swipq_true_option == $swiptrue) ? 1 : '',
								'status'				=> 1,
								'user_id' 				=> $userId,
								'cur_date'				=> $curdate
							));		
						else:
							$ansSql 					= "INSERT INTO `answer_tbl` (`question_id`, `choice_option`,`nextQid`,`End_Sim`, `image`, `ans_val1`, `ans_val2`, `ans_val3`, `ans_val4`, `ans_val5`, `ans_val6`, `true_option`, `status`, `uid`, `cur_date`) values (:qid, :option,:nextQid, :End_Sim,:image, :val1, :val2, :val3, :val4, :val5, :val6, :true, :status, :user_id, :cur_date)";
							$stmt 						= $db->prepare($ansSql);
							$stmt->execute(array(
								'qid'				=> $ques_id,
								'option'			=> ( ! empty($swipq_option[$swipqi])) ? addslashes($swipq_option[$swipqi]) : '',
								'nextQid'				=> ( ! empty($mappedQuestionSwipe[$swipqi])) ? addslashes($mappedQuestionSwipe[$swipqi]) : '',
								'End_Sim'				=> ( ! empty($endSim)) ? addslashes($endSim) : '',
								'image'				=> ( ! empty($swipq_item_img[$swipqi])) ? $swipq_item_img[$swipqi] : '',
								'val1'				=> ( ! empty($swip_ans_val1[$swipqi])) ? $swip_ans_val1[$swipqi] : '',
								'val2'				=> ( ! empty($swip_ans_val2[$swipqi])) ? $swip_ans_val2[$swipqi] : '',
								'val3'				=> ( ! empty($swip_ans_val3[$swipqi])) ? $swip_ans_val3[$swipqi] : '',
								'val4'				=> ( ! empty($swip_ans_val4[$swipqi])) ? $swip_ans_val4[$swipqi] : '',
								'val5'				=> ( ! empty($swip_ans_val5[$swipqi])) ? $swip_ans_val5[$swipqi] : '',
								'val6'				=> ( ! empty($swip_ans_val6[$swipqi])) ? $swip_ans_val6[$swipqi] : '',
								'true'				=> ( ! empty($swipq_true_option) && $swipq_true_option == $swiptrue) ? 1 : '',
								'status'			=> 1,
								'user_id' 			=> $userId,
								'cur_date'			=> $curdate
							));
							$getSwipqAnswerid 			=  $db->lastInsertId();
						endif;
						#------FEEDBACK-DATA---
						$feed_audio 				= '';
						if ( ! empty($swip_feedback_audio[$swipqi])):
							$feed_audio 			= $swip_feedback_audio[$swipqi];
						elseif ( ! empty($swip_feedback_rec_audio[$swipqi])):
							$feed_audio 			= $swip_feedback_rec_audio[$swipqi];
						endif;				
						$feed_video 				= '';
						if ( ! empty($swip_feedback_video[$swipqi])):
							$feed_video 			= $swip_feedback_video[$swipqi];
						elseif ( ! empty($swip_feedback_rec_video[$swipqi])):
							$feed_video 			= $swip_feedback_rec_video[$swipqi];
						endif;				
						#--------Feedback----------
						$cfeedData 					= array(
							'qid'					=> $ques_id,
							'aid'					=> ( ! empty($getSwipqAnswerid)) ? $getSwipqAnswerid : '',
							'feedback'				=> ( ! empty($swip_cfeedback[$swipqi])) ? addslashes($swip_cfeedback[$swipqi]) : '',
							'ftype'					=> 1,
							'speech_text'			=> ( ! empty($swip_feedback_text_to_speech[$swipqi])) ? addslashes($swip_feedback_text_to_speech[$swipqi]) : '',
							'audio'					=> ( ! empty($feed_audio)) ? $feed_audio : '',
							'video'					=> ( ! empty($feed_video)) ? $feed_video : '',
							'screen'				=> ( ! empty($swip_feedback_rec_screen[$swipqi])) ? $swip_feedback_rec_screen[$swipqi] : '',
							'image'					=> ( ! empty($swip_feedback_img[$swipqi])) ? $swip_feedback_img[$swipqi] : '',
							'document'				=> ( ! empty($swip_feedback_doc[$swipqi])) ? $swip_feedback_doc[$swipqi] : ''
						);
						$cfeed_sql 					= "INSERT INTO `feedback_tbl` (`question_id`, `answer_id`, `feedback`, `feedback_type`, `feed_speech_text`, `feed_audio`, `feed_video`, `feed_screen`, `feed_image`, `feed_document`) VALUES (:qid, :aid, :feedback, :ftype, :speech_text, :audio, :video, :screen, :image, :document)";
						$cfeed_stmt 				= $db->prepare($cfeed_sql);
						$cfeed_stmt->execute($cfeedData);
						$swipqi++; 
						$swiptrue++;
					endforeach;
				endif;
			endif;
		endif;
	endif;
	if ( empty($resdata)):
		if ( ! empty($submit_type) && $submit_type == 1):
			$resdata = array('success' => TRUE, 'all_update' => FALSE, 'msg' => 'Simulation details updated successfully.', 'return_url' => 'branching-video-classic-template.php?add_data=true&sim_id='.md5($sim_id).'&ques_id='.base64_encode($ques_id));
		else:
			$resdata = array('success' => TRUE, 'all_update' => TRUE, 'msg' => 'All questions and answers updated successfully.', 'return_url' => 'author-simulation.php');
		endif;
	endif;
	echo json_encode($resdata);
endif;

#-----------------------------------------Add-Branching-Video-multiple-Sim - Saumya----------------------------------
if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['add_branching_video_multiple_sim']) && isset($_POST['sim_id'])):
	extract($_POST);
	if (empty($sim_duration) || empty($passing_marks)):
		$resdata = array('success' => FALSE, 'msg' => 'Please enter all required fields.');
	elseif ( ! empty($sim_duration) && ! empty($passing_marks)):
		$sql = "UPDATE scenario_master SET duration = '". $sim_duration ."', passing_marks = '". $passing_marks ."'";
		if ($character_type == 1):
			$sql .= ", sim_char_img = '', sim_char_left_img = '', sim_char_right_img = '', char_own_choice = ''";
		elseif ($charoption == 1 && isset($scenario_char_file) && ! empty($scenario_char_file)):
			$sql .= ", char_own_choice = '". $scenario_char_file ."', sim_char_img = '', sim_char_left_img = '', sim_char_right_img = ''";
		elseif ($charoption == 2 && isset($sim_char_img) && ! empty($sim_char_img)):
			$sql .= ", sim_char_img = '". $sim_char_img ."', sim_char_left_img = '', sim_char_right_img = ''";
		elseif (isset($sim_two_char_img) && count($sim_two_char_img) > 0):
			if (isset($sim_two_char_img[0])):
				$r = explode('|', $sim_two_char_img[0]);
				if (isset($r[1]) && $r[1] == 'R'):
					$sql .= ", sim_char_img = '', char_own_choice = '', sim_char_right_img = '". $r[0] ."'";
				elseif (isset($r[1]) && $r[1] == 'L'):
					$sql .= ", sim_char_img = '', char_own_choice = '', sim_char_left_img = '". $r[0] ."'";
				endif;
			endif;
			if (isset($sim_two_char_img[1])):
				$l = explode('|', $sim_two_char_img[1]);
				if (isset($l[1]) && $l[1] == 'L'):
					$sql .= ", sim_char_img = '', char_own_choice = '', sim_char_left_img = '". $l[0] ."'";
				elseif (isset($l[1]) && $l[1] == 'R'):
					$sql .= ", sim_char_img = '', char_own_choice = '', sim_char_right_img = '". $l[0] ."'";
				endif;
			endif;
		endif;
		#--Sim-Media-File--
		if ( ! empty($scenario_media_file) && in_array(strtolower(pathinfo($scenario_media_file, PATHINFO_EXTENSION)), $img_valid_exts)):
			$sql .= ", image_url = '". $scenario_media_file ."', video_url = NULL, audio_url = NULL";
		elseif ( ! empty($scenario_media_file) && in_array(strtolower(pathinfo($scenario_media_file, PATHINFO_EXTENSION)), $video_valid_exts)):
			$sql .= ", video_url = '". $scenario_media_file ."', image_url = NULL, audio_url = NULL";
		elseif ( ! empty($scenario_media_file) && in_array(strtolower(pathinfo($scenario_media_file, PATHINFO_EXTENSION)), $audio_valid_exts)):
			$sql .= ", audio_url = '". $scenario_media_file ."', video_url = NULL, image_url = NULL";
		endif;
		#--Sim-Backgrounds--
		if ($bgoption == 1 && ! empty($bg_color)):
			$sql .= ", bg_color = '". $bg_color ."', bg_own_choice = '', sim_back_img = ''";
		elseif ($bgoption == 2 && ! empty($scenario_bg_file)):
			$sql .= ", bg_own_choice = '". $scenario_bg_file ."', bg_color = '', sim_back_img = ''";
		elseif ($bgoption == 3 && ! empty($sim_background_img)):
			$sql .= ", sim_back_img = '". $sim_background_img ."', bg_own_choice = '', bg_color = ''";
		endif;
		$sql .= " WHERE scenario_id = '". $sim_id ."'";
		$results = $db->prepare($sql);
		if ($results->execute()):
			#----Competency----
			$delCom	 = "DELETE FROM competency_tbl WHERE scenario_id = '". $sim_id ."'";
			$delComq = $db->prepare($delCom); $delComq->execute();
			$data	 = array('scenario_id'	=> $sim_id,
							 'comp_col_1'	=> (isset($competency_name_1) && ! empty($competency_name_1)) ? addslashes($competency_name_1) : '',
							 'comp_val_1'	=> (isset($competency_score_1) && ! empty($competency_score_1)) ? addslashes($competency_score_1) : '',
							 'comp_col_2'	=> (isset($competency_name_2) && ! empty($competency_name_2)) ? addslashes($competency_name_2) : '',
							 'comp_val_2'	=> (isset($competency_score_2) && ! empty($competency_score_2)) ? $competency_score_2 : '',
							 'comp_col_3'	=> (isset($competency_name_3) && ! empty($competency_name_3)) ? addslashes($competency_name_3) : '',
							 'comp_val_3'	=> (isset($competency_score_3) && ! empty($competency_score_3)) ? $competency_score_3 : '',
							 'comp_col_4'	=> (isset($competency_name_4) && ! empty($competency_name_4)) ? addslashes($competency_name_4) : '',
							 'comp_val_4'	=> (isset($competency_score_4) && ! empty($competency_score_4)) ? $competency_score_4 : '',
							 'comp_col_5'	=> (isset($competency_name_5) && ! empty($competency_name_5)) ? addslashes($competency_name_5) : '',
							 'comp_val_5'	=> (isset($competency_score_5) && ! empty($competency_score_5)) ? $competency_score_5 : '',
							 'comp_col_6'	=> (isset($competency_name_6) && ! empty($competency_name_6)) ? addslashes($competency_name_6) : '',
							 'comp_val_6'	=> (isset($competency_score_6) && ! empty($competency_score_6)) ? $competency_score_6 : '',
							 'weightage_1'	=> (isset($weightage_1) && ! empty($weightage_1)) ? $weightage_1 : '',
							 'weightage_2'	=> (isset($weightage_2) && ! empty($weightage_2)) ? $weightage_2 : '',
							 'weightage_3'	=> (isset($weightage_3) && ! empty($weightage_3)) ? $weightage_3 : '',
							 'weightage_4'	=> (isset($weightage_4) && ! empty($weightage_4)) ? $weightage_4 : '',
							 'weightage_5'	=> (isset($weightage_5) && ! empty($weightage_5)) ? $weightage_5 : '',
							 'weightage_6'	=> (isset($weightage_6) && ! empty($weightage_6)) ? $weightage_6 : '',
							 'status' 		=> 1,
							 'user_id' 		=> $userId,
							 'cur_date'		=> $curdate);
			$com_sql = "INSERT INTO `competency_tbl` (`scenario_id`, `comp_col_1`, `comp_val_1`, `comp_col_2`, `comp_val_2`, `comp_col_3`, `comp_val_3`, `comp_col_4`, `comp_val_4`, `comp_col_5`, `comp_val_5`, `comp_col_6`, `comp_val_6`, `weightage_1`, `weightage_2`, `weightage_3`, `weightage_4`, `weightage_5`, `weightage_6`, `status`, `uid`, `cur_date`) VALUES (:scenario_id, :comp_col_1, :comp_val_1, :comp_col_2, :comp_val_2, :comp_col_3, :comp_val_3, :comp_col_4, :comp_val_4, :comp_col_5, :comp_val_5, :comp_col_6, :comp_val_6, :weightage_1, :weightage_2, :weightage_3, :weightage_4, :weightage_5, :weightage_6, :status, :user_id, :cur_date)";
			$com_stmt = $db->prepare($com_sql);
			$com_stmt->execute($data);
			
			#----PAGES----
			$delPage = "DELETE FROM sim_pages WHERE scenario_id = '". $sim_id ."'";
			$delPageq = $db->prepare($delPage); $delPageq->execute();
			if ( ! empty($sim_page_name)):
				$sp = 0;
				foreach ($sim_page_name as $pdata):
					$pageq = "INSERT INTO `sim_pages` (`scenario_id`, `sim_page_name`, `sim_page_desc`, `userid`, `status`, `added_date`) 
					VALUES (:sid, :pname, :pdesc, :user_id, :status, :adate)";
					$page_stmt = $db->prepare($pageq);
					$page_stmt->execute(array('sid'		=> $sim_id,
											  'pname'	=> ( ! empty($pdata)) ? addslashes($pdata) : '',
											  'pdesc'	=> ( ! empty($sim_page_desc[$sp])) ? $sim_page_desc[$sp] : '',
											  'status' 	=> 1,
											  'user_id'	=> $userId,
											  'adate'	=> $curdate));
				$sp++;
				endforeach;
			endif;
			
			#----Branding----
			$bandq	  = "DELETE FROM sim_branding_tbl WHERE scenario_id = '". $sim_id ."'";
			$bandqexe = $db->prepare($bandq); $bandqexe->execute();
			$brand_data = array('sid'				=> $sim_id,
								'bg'				=> ( ! empty($ques_bg_color)) ? $ques_bg_color : '',
								'bg_tr'				=> ( ! empty($ques_bg_transp)) ? $ques_bg_transp : '',
								'obg'				=> ( ! empty($ques_option_bg_color)) ? $ques_option_bg_color : '',
								'obg_tr'			=> ( ! empty($ques_option_bg_transp)) ? $ques_option_bg_transp : '',
								'ohover'			=> ( ! empty($ques_option_hover_color)) ? $ques_option_hover_color : '',
								'oh_tr'				=> ( ! empty($ques_option_hover_transp)) ? $ques_option_hover_transp : '',
								'oselected'			=> ( ! empty($ques_option_select_color)) ? $ques_option_select_color : '',
								'oselected_tr'		=> ( ! empty($ques_option_select_transp)) ? $ques_option_select_transp : '',
								'btn_bg'			=> ( ! empty($btn_bg_color)) ? $btn_bg_color : '',
								'btn_bg_tr'			=> ( ! empty($btn_bg_transp)) ? $btn_bg_transp : '',
								'btn_hover'			=> ( ! empty($btn_hover_color)) ? $btn_hover_color : '',
								'btn_hover_tr'		=> ( ! empty($btn_hover_transp)) ? $btn_hover_transp : '',
								'btn_selected'		=> ( ! empty($btn_select_color)) ? $btn_select_color : '',
								'btn_selected_tr'	=> ( ! empty($btn_select_transp)) ? $btn_select_transp : '',
								'font_type'			=> ( ! empty($font_type)) ? $font_type : '',
								'font_color'		=> ( ! empty($font_color)) ? $font_color : '',
								'font_size'			=> ( ! empty($font_size)) ? $font_size : '',
								'user_id' 			=> $userId,
								'added_date'		=> $curdate);
			$com_sql = "INSERT INTO `sim_branding_tbl` (`scenario_id`, `ques_bg`, `ques_bg_transparency`, `option_bg`, `option_bg_transparency`, `option_hover`, `option_hover_transparency`, `option_selected`, `option_selected_transparency`, `btn_bg`, `btn_bg_transparency`, `btn_hover`, `btn_hover_transparency`, `btn_selected`, `btn_selected_transparency`, `font_type`, `font_color`, `font_size`, `user_id`, `added_date`) 
			VALUES (:sid, :bg, :bg_tr, :obg, :obg_tr, :ohover, :oh_tr, :oselected, :oselected_tr, :btn_bg, :btn_bg_tr, :btn_hover, :btn_hover_tr, :btn_selected, :btn_selected_tr, :font_type, :font_color, :font_size, :user_id, :added_date)";
			$com_stmt = $db->prepare($com_sql);
			$com_stmt->execute($brand_data);
		endif;
	endif;
	if($singleOrMultiple == 'multipleVideo'){
		//Add MTF Question
		if ( $question_type == 1):
			#-------QUESTIONS-DATA---
			$qaudio = '';
			if ( ! empty($mtfq_qaudio)):
				$qaudio = $mtfq_qaudio;
			elseif ( ! empty($mtfq_rec_audio)):
				$qaudio = $mtfq_rec_audio;
			endif;
			
			$audio = '';
			if ( ! empty($mtf_qaudio)):
				$audio = $mtf_qaudio;
			elseif ( ! empty($mtf_rec_audio)):
				$audio = $mtf_rec_audio;
			endif;
			
			$video = '';
			if ( ! empty($mtf_video)):
				$video = $mtf_video;
			elseif ( ! empty($mtf_rec_video)):
				$video = $mtf_rec_video;
			endif;
			
			#------Correct-FEEDBACK-DATA---
			$feed_audio = '';
			if ( ! empty($mtf_feedback_audio)):
				$feed_audio = $mtf_feedback_audio;
			elseif ( ! empty($mtf_feedback_rec_audio)):
				$feed_audio = $mtf_feedback_rec_audio;
			endif;
			
			$feed_video = '';
			if ( ! empty($mtf_feedback_video)):
				$feed_video = $mtf_feedback_video;
			elseif ( ! empty($mtf_feedback_rec_video)):
				$feed_video = $mtf_feedback_rec_video;
			endif;
			
			#------Incorrect-FEEDBACK-DATA---
			$ifeed_audio = '';
			if ( ! empty($mtfi_feedback_audio)):
				$ifeed_audio = $mtfi_feedback_audio;
			elseif ( ! empty($mtfi_feedback_rec_audio)):
				$ifeed_audio = $mtfi_feedback_rec_audio;
			endif;
			
			$ifeed_video = '';
			if ( ! empty($mtfi_feedback_video)):
				$ifeed_video = $mtfi_feedback_video;
			elseif ( ! empty($mtfi_feedback_rec_video)):
				$ifeed_video = $mtfi_feedback_rec_video;
			endif;
			
			$quesData = array(
							'scenarioId'		=> $sim_id,
							'questions'			=> ( ! empty($mtfq)) ? addslashes($mtfq) : '',
							'videoq_media_file'	=> ( ! empty($videoq_media_file)) ? addslashes($videoq_media_file) : '',
							'qtype'				=> ( ! empty($question_type)) ? $question_type : '',
							'qaudio'			=> ( ! empty($qaudio)) ? $qaudio : '',
							'qspeech_text'		=> ( ! empty($mtfqq_text_to_speech)) ? addslashes($mtfqq_text_to_speech) : '',
							'speech_text'		=> ( ! empty($mtfq_text_to_speech)) ? addslashes($mtfq_text_to_speech) : '',
							'audio'				=> ( ! empty($audio)) ? $audio : '',
							'video'				=> ( ! empty($video)) ? $video : '',
							'screen'			=> ( ! empty($mtf_rec_screen)) ? $mtf_rec_screen : '',
							'image'				=> ( ! empty($mtf_img)) ? $mtf_img : '',
							'document'			=> ( ! empty($mtf_doc)) ? $mtf_doc : '',
							'ques_val_1'		=> ( ! empty($ques_val_1)) ? $ques_val_1 : '',
							'ques_val_2'		=> ( ! empty($ques_val_2)) ? $ques_val_2 : '',
							'ques_val_3'		=> ( ! empty($ques_val_3)) ? $ques_val_3 : '',
							'ques_val_4'		=> ( ! empty($ques_val_4)) ? $ques_val_4 : '',
							'ques_val_5'		=> ( ! empty($ques_val_5)) ? $ques_val_5 : '',
							'ques_val_6'		=> ( ! empty($ques_val_6)) ? $ques_val_6 : '',
							'critical'			=> ( ! empty($mtf_criticalQ)) ? $mtf_criticalQ : '',
							'status' 			=> 1,
							'user_id'			=> $userId,
							'cur_date'			=> $curdate);
			$ques_sql = "INSERT INTO `question_tbl` (`scenario_id`,`questions`,`videoq_media_file`,`question_type`, `qaudio`, `qspeech_text`,`speech_text`, `audio`,`video`, `screen`, `image`, `document`, `ques_val_1`, `ques_val_2`, `ques_val_3`, `ques_val_4`, `ques_val_5`, `ques_val_6`, `critical`,`status`, `uid`, `cur_date`) 
			VALUES (:scenarioId,:questions,:videoq_media_file, :qtype, :qaudio, :qspeech_text, :speech_text, :audio, :video, :screen, :image, :document, :ques_val_1, :ques_val_2, :ques_val_3, :ques_val_4, :ques_val_5, :ques_val_6,:critical, :status, :user_id, :cur_date)";
			$ques_stmt = $db->prepare($ques_sql);
			if ($ques_stmt->execute($quesData)){

				$ques_id 					= $db->lastInsertId();
				#-----Remove-Old-Feedback-------
				$db->feedback_remove($ques_id);
				
				#--------Correct-Feedback----------
				$cfeedData = array('qid'			=> ( ! empty($ques_id)) ? $ques_id : '',
									'nextQid'			=> ( ! empty($mappedQuestionMtfCorrect)) ? $mappedQuestionMtfCorrect : '',
									'EndSim'		=> (isset($chkCorMTFEndSim) && ! empty($chkCorMTFEndSim)) ? 1 : 0,
								'feedback'		=> ( ! empty($mtf_cfeedback)) ? addslashes($mtf_cfeedback) : '',
								'ftype'			=> 1,
								'speech_text'	=> ( ! empty($mtf_feedback_text_to_speech)) ? addslashes($mtf_feedback_text_to_speech) : '',
								'audio'			=> ( ! empty($feed_audio)) ? $feed_audio : '',
								'video'			=> ( ! empty($feed_video)) ? $feed_video : '',
								'screen'			=> ( ! empty($mtf_feedback_rec_screen)) ? $mtf_feedback_rec_screen : '',
								'image'			=> ( ! empty($mtf_feedback_img)) ? $mtf_feedback_img : '',
								'document'		=> ( ! empty($mtf_feedback_doc)) ? $mtf_feedback_doc : '');
				
				$cfeed_sql = "INSERT INTO `feedback_tbl` (`question_id`,`nextQid`,`EndSim`, `feedback`, `feedback_type`, `feed_speech_text`, `feed_audio`, `feed_video`, `feed_screen`, `feed_image`, `feed_document`) VALUES (:qid, :nextQid,:EndSim,:feedback, :ftype, :speech_text, :audio, :video, :screen, :image, :document)";
				$cfeed_stmt = $db->prepare($cfeed_sql);
				$cfeed_stmt->execute($cfeedData);
				
				#--------Incorrect-Feedback-------
				$ifeedData = array('qid'			=> ( ! empty($ques_id)) ? $ques_id : '',
									'nextQid'			=> ( ! empty($mappedQuestionMtfIncorrect)) ? $mappedQuestionMtfIncorrect : '',
									'EndSim'		=> (isset($chkInMTFEndSim) && ! empty($chkInMTFEndSim)) ? 1 : 0,
								'feedback'		=> ( ! empty($mtf_ifeedback)) ? addslashes($mtf_ifeedback) : '',
								'ftype'			=> 2,
								'speech_text'	=> ( ! empty($mtfi_feedback_text_to_speech)) ? addslashes($mtfi_feedback_text_to_speech) : '',
								'audio'			=> ( ! empty($ifeed_audio)) ? $ifeed_audio : '',
								'video'			=> ( ! empty($ifeed_video)) ? $ifeed_video : '',
								'screen'			=> ( ! empty($mtfi_feedback_rec_screen)) ? $mtfi_feedback_rec_screen : '',
								'image'			=> ( ! empty($mtfi_feedback_img)) ? $mtfi_feedback_img : '',
								'document'		=> ( ! empty($mtfi_feedback_doc)) ? $mtfi_feedback_doc : '');
				
				$ifeed_sql = "INSERT INTO `feedback_tbl` (`question_id`, `nextQid`,`EndSim`,`feedback`, `feedback_type`, `feed_speech_text`, `feed_audio`, `feed_video`, `feed_screen`, `feed_image`, `feed_document`) VALUES (:qid, :nextQid,:EndSim,:feedback, :ftype, :speech_text, :audio, :video, :screen, :image, :document)";
				$ifeed_stmt = $db->prepare($ifeed_sql);
				$ifeed_stmt->execute($ifeedData);
				
				#----------Shuffle-OFF--------------
				if ( ! empty($mtf_choice[0]) && ! empty($mtf_choice_order[0]) && ! empty($mtf_match_order[0]) && ! empty($mtf_match[0]) && ! empty($ques_id)):
					$soff = 0;
					foreach ($mtf_choice as $answerData):
						if ( ! empty($updateAnswerid[$soff])):
							$ansSql = "UPDATE `answer_tbl` SET `choice_option`=:choice_option, `choice_order_no`=:choice_order_no, `match_order_no`=:match_order_no, `match_option`=:match_option, `status`=:status, `uid`=:user_id, `cur_date`=:cur_date WHERE answer_id=:answer_id";
							$stmt = $db->prepare($ansSql);
							$stmt->execute(array('answer_id' 		=> $updateAnswerid[$soff],
												'choice_option'	=> ( ! empty($mtf_choice[$soff])) ? $mtf_choice[$soff] : '',
												'choice_order_no'	=> ( ! empty($mtf_choice_order[$soff])) ? $mtf_choice_order[$soff] : '',
												'match_order_no'	=> ( ! empty($mtf_match_order[$soff])) ? $mtf_match_order[$soff] : '',
												'match_option'		=> ( ! empty($mtf_match[$soff])) ? $mtf_match[$soff] : '',
												'status' 			=> 1,
												'user_id' 			=> $userId,
												'cur_date'			=> $curdate));
						else:
							$ansSql = "INSERT INTO `answer_tbl` (`question_id`, `choice_option`, `choice_order_no`, `match_order_no`, `match_option`, `status`, `uid`, `cur_date`) 
							values (:qid, :choice_option, :choice_order_no, :match_order_no, :match_option, :status, :user_id, :cur_date)";
							$stmt = $db->prepare($ansSql);
							$stmt->execute(array('qid'				=> $ques_id,
												'choice_option'	=> ( ! empty($mtf_choice[$soff])) ? addslashes($mtf_choice[$soff]) : '',
												'choice_order_no'	=> ( ! empty($mtf_choice_order[$soff])) ? $mtf_choice_order[$soff] : '',
												'match_order_no'	=> ( ! empty($mtf_match_order[$soff])) ? $mtf_match_order[$soff] : '',
												'match_option'		=> ( ! empty($mtf_match[$soff])) ? $mtf_match[$soff] : '',
												'status' 			=> 1,
												'user_id' 			=> $userId,
												'cur_date'			=> $curdate));
						endif;
					$soff++;
					endforeach;
				endif;
				
				#----------Shuffle-ON--------------
				if ( ! empty($mtf_choice2[0]) && ! empty($mtf_match2[0]) && empty($mtf_match_order[0]) && empty($mtf_match[0])):
					$sON = 0;
					foreach ($mtf_choice2 as $answerData):
						if ( ! empty($updateAnswerid[$sON])):
							$ansSql = "UPDATE `answer_tbl` SET `choice_option`=:choice_option, `match_option`=:match_option, `status`=:status, `uid`=:user_id, `cur_date`=:cur_date WHERE answer_id=:answer_id";
							$stmt = $db->prepare($ansSql);
							$stmt->execute(array('answer_id' 		=> $updateAnswerid[$sON],
												'choice_option'	=> ( ! empty($mtf_choice2[$sON])) ? $mtf_choice2[$sON] : '',
												'match_option'		=> ( ! empty($mtf_match2[$sON])) ? $mtf_match2[$sON] : '',
												'status' 			=> 1,
												'user_id' 			=> $userId,
												'cur_date'			=> $curdate));
						else:
							$ansSql = "INSERT INTO `answer_tbl` (`question_id`, `choice_option`, `match_option`, `status`, `uid`, `cur_date`) 
							values (:qid, :choice_option, :match_option, :status, :user_id, :cur_date)";
							$stmt = $db->prepare($ansSql);
							$stmt->execute(array('qid'				=> $ques_id,
												'choice_option'	=> ( ! empty($mtf_choice2[$sON])) ? addslashes($mtf_choice2[$sON]) : '',
												'match_option'		=> ( ! empty($mtf_match2[$sON])) ? addslashes($mtf_match2[$sON]) : '',
												'status' 			=> 1,
												'user_id' 			=> $userId,
												'cur_date'			=> $curdate));
						endif;
						$sON++;
					endforeach;
				endif;
			}			
		endif;

		//add SEQUENCE Question
		if ($question_type == 2):
			#-------QUESTIONS-DATA---
			$qaudio = '';
			if ( ! empty($sqq_qaudio)):
				$qaudio = $sqq_qaudio;
			elseif ( ! empty($sqq_rec_audio)):
				$qaudio = $sqq_rec_audio;
			endif;
			
			$audio = '';
			if ( ! empty($sq_qaudio)):
				$audio = $sq_qaudio;
			elseif ( ! empty($sq_rec_audio)):
				$audio = $sq_rec_audio;
			endif;
			
			$video = '';
			if ( ! empty($sq_video)):
				$video = $sq_video;
			elseif ( ! empty($sq_rec_video)):
				$video = $sq_rec_video;
			endif;
			
			#------Correct-FEEDBACK-DATA---
			$feed_audio = '';
			if ( ! empty($sq_feedback_audio)):
				$feed_audio = $sq_feedback_audio;
			elseif ( ! empty($sq_feedback_rec_audio)):
				$feed_audio = $sq_feedback_rec_audio;
			endif;
			
			$feed_video = '';
			if ( ! empty($sq_feedback_video)):
				$feed_video = $sq_feedback_video;
			elseif ( ! empty($sq_feedback_rec_video)):
				$feed_video = $sq_feedback_rec_video;
			endif;
			
			#------Incorrect-FEEDBACK-DATA---
			$ifeed_audio = '';
			if ( ! empty($sqi_feedback_audio)):
				$ifeed_audio = $sqi_feedback_audio;
			elseif ( ! empty($sqi_feedback_rec_audio)):
				$ifeed_audio = $sqi_feedback_rec_audio;
			endif;
			
			$ifeed_video = '';
			if ( ! empty($sqi_feedback_video)):
				$ifeed_video = $sqi_feedback_video;
			elseif ( ! empty($sqi_feedback_rec_video)):
				$ifeed_video = $sqi_feedback_rec_video;
			endif;
			
			$quesData = array(
				'scenarioId'		=> $sim_id,
				'questions'			=> ( ! empty($sq)) ? addslashes($sq) : '',
				'videoq_media_file'	=> ( ! empty($videoq_media_file)) ? addslashes($videoq_media_file) : '',
				'qtype'				=> ( ! empty($question_type)) ? $question_type : '',
				'qaudio'			=> ( ! empty($qaudio)) ? $qaudio : '',
				'qspeech_text'		=> ( ! empty($sqq_text_to_speech)) ? addslashes($sqq_text_to_speech) : '',
				'speech_text'		=> ( ! empty($sq_text_to_speech)) ? addslashes($sq_text_to_speech) : '',
				'audio'				=> ( ! empty($audio)) ? $audio : '',
				'video'				=> ( ! empty($video)) ? $video : '',
				'screen'			=> ( ! empty($sq_rec_screen)) ? $sq_rec_screen : '',
				'image'				=> ( ! empty($sq_img)) ? $sq_img : '',
				'document'			=> ( ! empty($sq_doc)) ? $sq_doc : '',
				'ques_val_1'		=> ( ! empty($ques_val_1)) ? $ques_val_1 : '',
				'ques_val_2'		=> ( ! empty($ques_val_2)) ? $ques_val_2 : '',
				'ques_val_3'		=> ( ! empty($ques_val_3)) ? $ques_val_3 : '',
				'ques_val_4'		=> ( ! empty($ques_val_4)) ? $ques_val_4 : '',
				'ques_val_5'		=> ( ! empty($ques_val_5)) ? $ques_val_5 : '',
				'ques_val_6'		=> ( ! empty($ques_val_6)) ? $ques_val_6 : '',
				'critical'		=> ( ! empty($sq_criticalQ)) ? $sq_criticalQ : '',
				'status' 			=> 1,
				'user_id'			=> $userId,
				'cur_date'			=> $curdate);
			$ques_sql = "INSERT INTO `question_tbl` (`scenario_id`,`questions`,`videoq_media_file`,`question_type`, `qaudio`, `qspeech_text`,`speech_text`, `audio`,`video`, `screen`, `image`, `document`, `ques_val_1`, `ques_val_2`, `ques_val_3`, `ques_val_4`, `ques_val_5`, `ques_val_6`, `critical`,`status`, `uid`, `cur_date`) 
			VALUES (:scenarioId,:questions,:videoq_media_file, :qtype, :qaudio, :qspeech_text, :speech_text, :audio, :video, :screen, :image, :document, :ques_val_1, :ques_val_2, :ques_val_3, :ques_val_4, :ques_val_5, :ques_val_6, :critical,:status, :user_id, :cur_date)";
			$ques_stmt = $db->prepare($ques_sql);

			if ($ques_stmt->execute($quesData)){
				$ques_id 					= $db->lastInsertId();
			
				#-----Remove-Old-Feedback-------
				$db->feedback_remove($ques_id);
			
				#--------Correct-Feedback----------
				$cfeedData = array('qid'			=> ( ! empty($ques_id)) ? $ques_id : '',
									'nextQid'		=> ( ! empty($mappedQuestionSEQCorrect)) ? $mappedQuestionSEQCorrect : '',
									'EndSim'		=> (isset($chkCorSWQEndSim) && ! empty($chkCorSWQEndSim)) ? 1 : 0,
								'feedback'		=> ( ! empty($sq_feedback)) ? addslashes($sq_feedback) : '',
								'ftype'			=> 1,
								'speech_text'	=> ( ! empty($sq_feedback_text_to_speech)) ? addslashes($sq_feedback_text_to_speech) : '',
								'audio'			=> ( ! empty($feed_audio)) ? $feed_audio : '',
								'video'			=> ( ! empty($feed_video)) ? $feed_video : '',
								'screen'			=> ( ! empty($sq_feedback_rec_screen)) ? $sq_feedback_rec_screen : '',
								'image'			=> ( ! empty($sq_feedback_img)) ? $sq_feedback_img : '',
								'document'		=> ( ! empty($sq_feedback_doc)) ? $sq_feedback_doc : '');
				
				$cfeed_sql = "INSERT INTO `feedback_tbl` (`question_id`, `nextQid`,`EndSim`,`feedback`, `feedback_type`, `feed_speech_text`, `feed_audio`, `feed_video`, `feed_screen`, `feed_image`, `feed_document`) VALUES (:qid, :nextQid,:EndSim,:feedback, :ftype, :speech_text, :audio, :video, :screen, :image, :document)";
				$cfeed_stmt = $db->prepare($cfeed_sql);
				$cfeed_stmt->execute($cfeedData);
			
				#--------Incorrect-Feedback-------
				$ifeedData = array('qid'			=> ( ! empty($ques_id)) ? $ques_id : '',
									'nextQid'		=> ( ! empty($mappedQuestionSEQIncorrect)) ? $mappedQuestionSEQIncorrect : '',
									'EndSim'		=> (isset($chkInSWQEndSim) && ! empty($chkInSWQEndSim)) ? 1 : 0,
								'feedback'		=> ( ! empty($sq_ifeedback)) ? addslashes($sq_ifeedback) : '',
								'ftype'			=> 2,
								'speech_text'	=> ( ! empty($sqi_feedback_text_to_speech)) ? addslashes($sqi_feedback_text_to_speech) : '',
								'audio'			=> ( ! empty($ifeed_audio)) ? $ifeed_audio : '',
								'video'			=> ( ! empty($ifeed_video)) ? $ifeed_video : '',
								'screen'			=> ( ! empty($sqi_feedback_rec_screen)) ? $sqi_feedback_rec_screen : '',
								'image'			=> ( ! empty($sqi_feedback_img)) ? $sqi_feedback_img : '',
								'document'		=> ( ! empty($sqi_feedback_doc)) ? $sqi_feedback_doc : '');
				
				$ifeed_sql = "INSERT INTO `feedback_tbl` (`question_id`, `nextQid`,`EndSim`,`feedback`, `feedback_type`, `feed_speech_text`, `feed_audio`, `feed_video`, `feed_screen`, `feed_image`, `feed_document`) VALUES (:qid, :nextQid,:EndSim,:feedback, :ftype, :speech_text, :audio, :video, :screen, :image, :document)";
				$ifeed_stmt = $db->prepare($ifeed_sql);
				$ifeed_stmt->execute($ifeedData);
			
				#----------ADD-OPTIONS-------------
				if ( ! empty($sq_ans[0])):
					$sqi = 0; $sqi_order = 1;
					foreach ($sq_ans as $answerData):
						if ( ! empty($updateSQAnswerid[$sqi])):
							$ansSql = "UPDATE `answer_tbl` SET `choice_option`=:choice_option, `match_order_no`=:order_no, `status`=:status, `uid`=:user_id, `cur_date`=:cur_date WHERE answer_id=:answer_id";
							$stmt = $db->prepare($ansSql);
							$stmt->execute(array('answer_id' 		=> $updateSQAnswerid[$sqi],
												'choice_option'	=> ( ! empty($sq_ans[$sqi])) ? $sq_ans[$sqi] : '',
												'order_no'			=> $sqi_order,
												'status' 			=> 1,
												'user_id' 			=> $userId,
												'cur_date'			=> $curdate));
						else:
							$ansSql = "INSERT INTO `answer_tbl` (`question_id`, `choice_option`, `match_order_no`, `status`, `uid`, `cur_date`) 
							values (:qid, :choice_option, :order_no, :status, :user_id, :cur_date)";
							$stmt = $db->prepare($ansSql);
							$stmt->execute(array('qid'				=> $ques_id,
												'choice_option'	=> ( ! empty($sq_ans[$sqi])) ? addslashes($sq_ans[$sqi]) : '',
												'order_no'			=> $sqi_order,
												'status' 			=> 1,
												'user_id' 			=> $userId,
												'cur_date'			=> $curdate));
						endif;
						$sqi++; $sqi_order++;
					endforeach;
				endif;
			}
		endif;

		//Add Sorting question
		if ($question_type == 3):
			#-------QUESTIONS-DATA---
			$qaudio = '';
			if ( ! empty($sortqq_qaudio)):
				$qaudio = $sortqq_qaudio;
			elseif ( ! empty($sortqq_rec_audio)):
				$qaudio = $sortqq_rec_audio;
			endif;
			
			$audio = '';
			if ( ! empty($sortq_qaudio)):
				$audio = $sortq_qaudio;
			elseif ( ! empty($sortq_rec_audio)):
				$audio = $sortq_rec_audio;
			endif;
			
			$video = '';
			if ( ! empty($sortq_video)):
				$video = $sortq_video;
			elseif ( ! empty($sortq_rec_video)):
				$video = $sortq_rec_video;
			endif;
			
			#------Correct-FEEDBACK-DATA---
			$feed_audio = '';
			if ( ! empty($sortq_feedback_audio)):
				$feed_audio = $sortq_feedback_audio;
			elseif ( ! empty($sortq_feedback_rec_audio)):
				$feed_audio = $sortq_feedback_rec_audio;
			endif;
			
			$feed_video = '';
			if ( ! empty($sortq_feedback_video)):
				$feed_video = $sortq_feedback_video;
			elseif ( ! empty($sortq_feedback_rec_video)):
				$feed_video = $sortq_feedback_rec_video;
			endif;
			
			#------Incorrect-FEEDBACK-DATA---
			$ifeed_audio = '';
			if ( ! empty($sortqi_feedback_audio)):
				$ifeed_audio = $sortqi_feedback_audio;
			elseif ( ! empty($sortqi_feedback_rec_audio)):
				$ifeed_audio = $sortqi_feedback_rec_audio;
			endif;
			
			$ifeed_video = '';
			if ( ! empty($sortqi_feedback_video)):
				$ifeed_video = $sortqi_feedback_video;
			elseif ( ! empty($sortqi_feedback_rec_video)):
				$ifeed_video = $sortqi_feedback_rec_video;
			endif;
						
			$quesData = array(
				'scenarioId'		=> $sim_id,
				'questions'			=> ( ! empty($sortq)) ? addslashes($sortq) : '',
				'videoq_media_file'	=> ( ! empty($videoq_media_file)) ? addslashes($videoq_media_file) : '',
				'qtype'				=> ( ! empty($question_type)) ? $question_type : '',
				'qaudio'			=> ( ! empty($qaudio)) ? $qaudio : '',
				'qspeech_text'		=> ( ! empty($sortqq_text_to_speech)) ? addslashes($sortqq_text_to_speech) : '',
				'speech_text'		=> ( ! empty($sortq_text_to_speech)) ? addslashes($sortq_text_to_speech) : '',
				'audio'				=> ( ! empty($audio)) ? $audio : '',
				'video'				=> ( ! empty($video)) ? $video : '',
				'screen'			=> ( ! empty($sortq_rec_screen)) ? $sortq_rec_screen : '',
				'image'				=> ( ! empty($sortq_img)) ? $sortq_img : '',
				'document'			=> ( ! empty($sortq_doc)) ? $sortq_doc : '',
				'ques_val_1'		=> ( ! empty($ques_val_1)) ? $ques_val_1 : '',
				'ques_val_2'		=> ( ! empty($ques_val_2)) ? $ques_val_2 : '',
				'ques_val_3'		=> ( ! empty($ques_val_3)) ? $ques_val_3 : '',
				'ques_val_4'		=> ( ! empty($ques_val_4)) ? $ques_val_4 : '',
				'ques_val_5'		=> ( ! empty($ques_val_5)) ? $ques_val_5 : '',
				'ques_val_6'		=> ( ! empty($ques_val_6)) ? $ques_val_6 : '',
				'critical'		=> ( ! empty($sort_criticalQ)) ? $sort_criticalQ : '',
				'status' 			=> 1,
				'user_id'			=> $userId,
				'cur_date'			=> $curdate);
			$ques_sql = "INSERT INTO `question_tbl` (`scenario_id`,`questions`,`videoq_media_file`,`question_type`, `qaudio`, `qspeech_text`,`speech_text`, `audio`,`video`, `screen`, `image`, `document`, `ques_val_1`, `ques_val_2`, `ques_val_3`, `ques_val_4`, `ques_val_5`, `ques_val_6`, `critical`,`status`, `uid`, `cur_date`) 
			VALUES (:scenarioId,:questions,:videoq_media_file, :qtype, :qaudio, :qspeech_text, :speech_text, :audio, :video, :screen, :image, :document, :ques_val_1, :ques_val_2, :ques_val_3, :ques_val_4, :ques_val_5, :ques_val_6,:critical, :status, :user_id, :cur_date)";
			$ques_stmt = $db->prepare($ques_sql);

			if ($ques_stmt->execute($quesData)){
				$ques_id 					= $db->lastInsertId();
							
				#-----Remove-Old-Feedback-------
				$db->feedback_remove($ques_id);
			
				#--------Correct-Feedback----------
				$cfeedData = array('qid'			=> ( ! empty($ques_id)) ? $ques_id : '',
									'nextQid'		=> ( ! empty($mappedQuestionSORTCorrect)) ? $mappedQuestionSORTCorrect : '',
									'EndSim'		=> (isset($chkCorSORTEndSim) && ! empty($chkCorSORTEndSim)) ? 1 : 0,
								'feedback'		=> ( ! empty($sortq_cfeedback)) ? addslashes($sortq_cfeedback) : '',
								'ftype'			=> 1,
								'speech_text'	=> ( ! empty($sortq_feedback_text_to_speech)) ? addslashes($sortq_feedback_text_to_speech) : '',
								'audio'			=> ( ! empty($feed_audio)) ? $feed_audio : '',
								'video'			=> ( ! empty($feed_video)) ? $feed_video : '',
								'screen'			=> ( ! empty($sortq_feedback_rec_screen)) ? $sortq_feedback_rec_screen : '',
								'image'			=> ( ! empty($sortq_feedback_img)) ? $sortq_feedback_img : '',
								'document'		=> ( ! empty($sortq_feedback_doc)) ? $sortq_feedback_doc : '');
				
				$cfeed_sql = "INSERT INTO `feedback_tbl` (`question_id`, `nextQid`,`EndSim`,`feedback`, `feedback_type`, `feed_speech_text`, `feed_audio`, `feed_video`, `feed_screen`, `feed_image`, `feed_document`) VALUES (:qid, :nextQid,:EndSim,:feedback, :ftype, :speech_text, :audio, :video, :screen, :image, :document)";
				$cfeed_stmt = $db->prepare($cfeed_sql);
				$cfeed_stmt->execute($cfeedData);
			
				#--------Incorrect-Feedback-------
				$ifeedData = array('qid'			=> ( ! empty($ques_id)) ? $ques_id : '',
									'nextQid'		=> ( ! empty($mappedQuestionSORTIncorrect)) ? $mappedQuestionSORTIncorrect : '',
									'EndSim'		=> (isset($chkInSORTEndSim) && ! empty($chkInSORTEndSim)) ? 1 : 0,
								'feedback'		=> ( ! empty($sortq_ifeedback)) ? addslashes($sortq_ifeedback) : '',
								'ftype'			=> 2,
								'speech_text'	=> ( ! empty($sortqi_feedback_text_to_speech)) ? addslashes($sortqi_feedback_text_to_speech) : '',
								'audio'			=> ( ! empty($ifeed_audio)) ? $ifeed_audio : '',
								'video'			=> ( ! empty($ifeed_video)) ? $ifeed_video : '',
								'screen'			=> ( ! empty($sortqi_feedback_rec_screen)) ? $sortqi_feedback_rec_screen : '',
								'image'			=> ( ! empty($sortqi_feedback_img)) ? $sortqi_feedback_img : '',
								'document'		=> ( ! empty($sortqi_feedback_doc)) ? $sortqi_feedback_doc : '');
				
				$ifeed_sql = "INSERT INTO `feedback_tbl` (`question_id`,`nextQid`, `EndSim`,`feedback`, `feedback_type`, `feed_speech_text`, `feed_audio`, `feed_video`, `feed_screen`, `feed_image`, `feed_document`) VALUES (:qid,:nextQid,:EndSim, :feedback, :ftype, :speech_text, :audio, :video, :screen, :image, :document)";
				$ifeed_stmt = $db->prepare($ifeed_sql);
				$ifeed_stmt->execute($ifeedData);
			
				#----------ADD-OPTIONS----------
				if ( ! empty($sortq_sorting_items[0])):
					$sortqi = 0;
					foreach ($sortq_sorting_items as $optionData):
						if ( ! empty($updateSortAnswerid[$sortqi])):
							$ansSql = "UPDATE `answer_tbl` SET `choice_option`=:choice_option, `image`=:image, `status`=:status, `uid`=:user_id, `cur_date`=:cur_date WHERE answer_id=:answer_id";
							$stmt = $db->prepare($ansSql);
							$stmt->execute(array('answer_id'		=> $updateSortAnswerid[$sortqi],
												'choice_option'	=> (isset($sortq_sorting_items[$sortqi]) && ! empty($sortq_sorting_items[$sortqi])) ? $sortq_sorting_items[$sortqi] : '',
												'image'			=> (isset($sortq_item_img[$sortqi]) && ! empty($sortq_item_img[$sortqi])) ? $sortq_item_img[$sortqi] : '',
												'status' 			 => 1,
												'user_id' 			 => $userId,
												'cur_date'			 => $curdate));
							
							if ( ! empty($updateSubOptionid[$sortqi])):
								$sortqop = 0;
								foreach ($sortq_drag_items[$sortqi]['option'] as $subOptionData):
									if ( ! empty($updateSubOptionid[$sortqi][$sortqop])):
										$ansOpSql = "UPDATE `sub_options_tbl` SET `sub_option`=:sub_option WHERE sub_options_id=:sub_options_id";
										$opstmt = $db->prepare($ansOpSql);
										$opstmt->execute(array('sub_options_id'	=> $updateSubOptionid[$sortqi][$sortqop],
															'sub_option'	 	=> ( ! empty($subOptionData)) ? $subOptionData : ''));
									else:
										$opSql = "INSERT INTO `sub_options_tbl` (`answer_id`, `sub_option`) values (:aid, :option)";
										$opstmt = $db->prepare($opSql);
										$opstmt->execute(array('aid'	=> $updateSortAnswerid[$sortqi],
															'option'	=> ( ! empty($subOptionData)) ? addslashes($subOptionData) : ''));
									endif;
									$sortqop++;
								endforeach;
							endif;
						else:
							$ansSql = "INSERT INTO `answer_tbl` (`question_id`, `choice_option`, `image`, `status`, `uid`, `cur_date`) values (:qid, :choice_option, :image, :status, :user_id, :cur_date)";
							$stmt = $db->prepare($ansSql);
							$stmt->execute(array('qid'				=> $ques_id,
												'choice_option'	=> ( ! empty($sortq_sorting_items[$sortqi])) ? addslashes($sortq_sorting_items[$sortqi]) : '',
												'image'			=> (isset($sortq_item_img[$sortqi]) && ! empty($sortq_item_img[$sortqi])) ? $sortq_item_img[$sortqi] : '',
												'status' 			 => 1,
												'user_id' 			 => $userId,
												'cur_date'			 => $curdate));
							$ans_id = $db->lastInsertId();
							if ( ! empty($sortq_drag_items)):
								foreach ($sortq_drag_items[$sortqi]['option'] as $subOptionData):
									$opSql = "INSERT INTO `sub_options_tbl` (`answer_id`, `sub_option`) values (:aid, :option)";
									$opstmt = $db->prepare($opSql);
									$opstmt->execute(array('aid'	=> $ans_id,
														'option'	=> ( ! empty($subOptionData)) ? addslashes($subOptionData) : ''));
								endforeach;
							endif;
						endif;
						$sortqi++;
					endforeach;	
				endif;
			}
		endif;
		//end sorting

		//Add DND Question
		if ($question_type == 8):
			#-------QUESTIONS-DATA---
			$qaudio = '';
			if ( ! empty($ddqq_qaudio)):
				$qaudio = $ddqq_qaudio;
			elseif ( ! empty($ddqq_rec_audio)):
				$qaudio = $ddqq_rec_audio;
			endif;
		
			$audio = '';
			if ( ! empty($ddq_audio)):
				$audio = $ddq_audio;
			elseif ( ! empty($ddq_rec_audio)):
				$audio = $ddq_rec_audio;
			endif;
			
			$video = '';
			if ( ! empty($ddq_video)):
				$video = $ddq_video;
			elseif ( ! empty($ddq_rec_video)):
				$video = $ddq_rec_video;
			endif;
			
			#------Correct-FEEDBACK-DATA---
			$feed_audio = '';
			if ( ! empty($ddq_feedback_audio)):
				$feed_audio = $ddq_feedback_audio;
			elseif ( ! empty($ddq_feedback_rec_audio)):
				$feed_audio = $ddq_feedback_rec_audio;
			endif;
			
			$feed_video = '';
			if ( ! empty($ddq_feedback_video)):
				$feed_video = $ddq_feedback_video;
			elseif ( ! empty($ddq_feedback_rec_video)):
				$feed_video = $ddq_feedback_rec_video;
			endif;
			
			#------Incorrect-FEEDBACK-DATA---
			$ifeed_audio = '';
			if ( ! empty($ddqi_feedback_audio)):
				$ifeed_audio = $ddqi_feedback_audio;
			elseif ( ! empty($ddqi_feedback_rec_audio)):
				$ifeed_audio = $ddqi_feedback_rec_audio;
			endif;
			
			$ifeed_video = '';
			if ( ! empty($ddqi_feedback_video)):
				$ifeed_video = $ddqi_feedback_video;
			elseif ( ! empty($ddqi_feedback_rec_video)):
				$ifeed_video = $ddqi_feedback_rec_video;
			endif;
			
			#--For-Multi-DROP-Options----
			if ($seleted_drag_option == 1):
				$quesData = array(
								'scenario_id'		=> ( ! empty($sim_id)) ? addslashes($sim_id) : '',
								'questions'			=> ( ! empty($ddq)) ? addslashes($ddq) : '',
								'videoq_media_file'	=> ( ! empty($videoq_media_file)) ? addslashes($videoq_media_file) : '',
								'seleted_drag_option' 		=> ( ! empty($seleted_drag_option)) ? $seleted_drag_option : '',
								'qtype'				=> ( ! empty($question_type)) ? $question_type : '',
								'qaudio'			=> ( ! empty($qaudio)) ? $qaudio : '',
								'qspeech_text'		=> ( ! empty($ddqq_text_to_speech)) ? addslashes($ddqq_text_to_speech) : '',
								'speech_text'		=> ( ! empty($ddq_text_to_speech)) ? addslashes($ddq_text_to_speech) : '',
								'audio'				=> ( ! empty($audio)) ? $audio : '',
								'video'				=> ( ! empty($video)) ? $video : '',
								'screen'			=> ( ! empty($ddq_rec_screen)) ? $ddq_rec_screen : '',
								'image'				=> ( ! empty($ddq_img)) ? $ddq_img : '',
								'document'			=> ( ! empty($ddq_doc)) ? $ddq_doc : '',
								'shuffle'			=> (isset($ddq_shuffle1) && ! empty($ddq_shuffle1)) ? 1 : 0,
								'ques_val_1'		=> ( ! empty($ddq_ques_val_1)) ? $ddq_ques_val_1 : '',
								'ques_val_2'		=> ( ! empty($ddq_ques_val_2)) ? $ddq_ques_val_2 : '',
								'ques_val_3'		=> ( ! empty($ddq_ques_val_3)) ? $ddq_ques_val_3 : '',
								'ques_val_4'		=> ( ! empty($ddq_ques_val_4)) ? $ddq_ques_val_4 : '',
								'ques_val_5'		=> ( ! empty($ddq_ques_val_5)) ? $ddq_ques_val_5 : '',
								'ques_val_6'		=> ( ! empty($ddq_ques_val_6)) ? $ddq_ques_val_6 : '',
								'critical'		=> ( ! empty($ddq_criticalQ)) ? $ddq_criticalQ : '',
								'status' 			=> 1,
								'user_id'			=> $userId,
								'cur_date'			=> $curdate,
								'qid'				=> $ques_id);
				$ques_sql = "INSERT INTO `question_tbl` (`scenario_id`,`questions`,`videoq_media_file`,`seleted_drag_option`,`question_type`, `qaudio`, `qspeech_text`,`speech_text`, `audio`,`video`, `screen`, `image`, `document`, `ques_val_1`, `ques_val_2`, `ques_val_3`, `ques_val_4`, `ques_val_5`, `ques_val_6`, `critical`,`status`, `uid`, `cur_date`) 
				VALUES (:scenarioId,:questions,:videoq_media_file,:seleted_drag_option, :qtype, :qaudio, :qspeech_text, :speech_text, :audio, :video, :screen, :image, :document, :ques_val_1, :ques_val_2, :ques_val_3, :ques_val_4, :ques_val_5, :ques_val_6, :critical,:status, :user_id, :cur_date)";
				$ques_stmt = $db->prepare($ques_sql);

				if ($ques_stmt->execute($quesData)):
					$ques_id 					= $db->lastInsertId();
					#------OPTIONS------
					if ( ! empty($ddq_drop)):
						$ddqi = 0;
						foreach ($ddq_drop as $answerData):
							if ( ! empty($updateDDqAnswerid[$ddqi])):
								$ansSql = "UPDATE `answer_tbl` SET `choice_option`=:choice, `match_option`=:match, `image`=:image, `image2`=:image2, `status`=:status, `uid`=:user_id, `cur_date`=:cur_date WHERE answer_id=:aid";
								$stmt = $db->prepare($ansSql);
								$stmt->execute(array('aid' 		=> $updateDDqAnswerid[$ddqi],
													'choice'	=> ( ! empty($ddq_drop[$ddqi])) ? $ddq_drop[$ddqi] : '',
													'match'	=> ( ! empty($ddq_drag[$ddqi])) ? $ddq_drag[$ddqi] : '',
													'image'	=> ( ! empty($ddq_item_img[$ddqi])) ? $ddq_item_img[$ddqi] : '',
													'image2'	=> ( ! empty($ddq_drag_item_img[$ddqi])) ? $ddq_drag_item_img[$ddqi] : '',
													'status'	=> 1,
													'user_id' 	=> $userId,
													'cur_date'	=> $curdate));
							else:
								$ansSql = "INSERT INTO `answer_tbl` (`question_id`, `choice_option`, `match_option`, `image`, `image2`, `status`, `uid`, `cur_date`) values (:qid, :choice, :match, :image, :image2, :status, :user_id, :cur_date)";
								$stmt = $db->prepare($ansSql);
								$stmt->execute(array('qid'		=> $ques_id,
													'choice'	=> ( ! empty($ddq_drop[$ddqi])) ? addslashes($ddq_drop[$ddqi]) : '',
													'match'	=> ( ! empty($ddq_drag[$ddqi])) ? addslashes($ddq_drag[$ddqi]) : '',
													'image'	=> ( ! empty($ddq_item_img[$ddqi])) ? $ddq_item_img[$ddqi] : '',
													'image2'	=> ( ! empty($ddq_drag_item_img[$ddqi])) ? $ddq_drag_item_img[$ddqi] : '',
													'status'	=> 1,
													'user_id' 	=> $userId,
													'cur_date'	=> $curdate));
							endif;
							$ddqi++;
						endforeach;
					endif;

					#-----Remove-Old-Feedback-------
					$db->feedback_remove($ques_id);

					#--------Correct-Feedback-------
					$cfeedData = array('qid'			=> ( ! empty($ques_id)) ? $ques_id : '',
										'nextQid'		=> ( ! empty($mappedQuestionDNDCorrect)) ? $mappedQuestionDNDCorrect : '',
										'EndSim'		=> (isset($chkCorDNDEndSim) && ! empty($chkCorDNDEndSim)) ? 1 : 0,
									'feedback'		=> ( ! empty($ddq_cfeedback)) ? addslashes($ddq_cfeedback) : '',
									'ftype'			=> 1,
									'speech_text'	=> ( ! empty($ddq_feedback_text_to_speech)) ? addslashes($ddq_feedback_text_to_speech) : '',
									'audio'			=> ( ! empty($feed_audio)) ? $feed_audio : '',
									'video'			=> ( ! empty($feed_video)) ? $feed_video : '',
									'screen'			=> ( ! empty($ddq_feedback_rec_screen)) ? $ddq_feedback_rec_screen : '',
									'image'			=> ( ! empty($ddq_feedback_img)) ? $ddq_feedback_img : '',
									'document'		=> ( ! empty($ddq_feedback_doc)) ? $ddq_feedback_doc : '');

					$cfeed_sql = "INSERT INTO `feedback_tbl` (`question_id`, `nextQid`,`EndSim`,`feedback`, `feedback_type`, `feed_speech_text`, `feed_audio`, `feed_video`, `feed_screen`, `feed_image`, `feed_document`) VALUES (:qid, :nextQid,:EndSim,:feedback, :ftype, :speech_text, :audio, :video, :screen, :image, :document)";
					$cfeed_stmt = $db->prepare($cfeed_sql);
					$cfeed_stmt->execute($cfeedData);

					#--------Incorrect-Feedback-------
					$ifeedData = array('qid'			=> ( ! empty($ques_id)) ? $ques_id : '',
										'nextQid'		=> ( ! empty($mappedQuestionDNDIncorrect)) ? $mappedQuestionDNDIncorrect : '',
										'EndSim'		=> (isset($chkInDNDEndSim) && ! empty($chkInDNDEndSim)) ? 1 : 0,
									'feedback'		=> ( ! empty($ddq_ifeedback)) ? addslashes($ddq_ifeedback) : '',
									'ftype'			=> 2,
									'speech_text'	=> ( ! empty($ddqi_feedback_text_to_speech)) ? addslashes($ddqi_feedback_text_to_speech) : '',
									'audio'			=> ( ! empty($ifeed_audio)) ? $ifeed_audio : '',
									'video'			=> ( ! empty($ifeed_video)) ? $ifeed_video : '',
									'screen'			=> ( ! empty($ddqi_feedback_rec_screen)) ? $ddqi_feedback_rec_screen : '',
									'image'			=> ( ! empty($ddqi_feedback_img)) ? $ddqi_feedback_img : '',
									'document'		=> ( ! empty($ddqi_feedback_doc)) ? $ddqi_feedback_doc : '');

					$ifeed_sql = "INSERT INTO `feedback_tbl` (`question_id`,`nextQid`, `EndSim`,`feedback`, `feedback_type`, `feed_speech_text`, `feed_audio`, `feed_video`, `feed_screen`, `feed_image`, `feed_document`) VALUES (:qid, :nextQid,:EndSim,:feedback, :ftype, :speech_text, :audio, :video, :screen, :image, :document)";
					$ifeed_stmt = $db->prepare($ifeed_sql);
					$ifeed_stmt->execute($ifeedData);
				endif;
			elseif ($seleted_drag_option == 2):
				$quesData = array('scenario_id'		=> ( ! empty($sim_id)) ? addslashes($sim_id) : '',
								'questions'			=> ( ! empty($ddq)) ? addslashes($ddq) : '',
								'videoq_media_file'	=> ( ! empty($videoq_media_file)) ? addslashes($videoq_media_file) : '',
								'seleted_drag_option' 		=> ( ! empty($seleted_drag_option)) ? $seleted_drag_option : '',
								'qtype'			=> ( ! empty($question_type)) ? $question_type : '',
								'qaudio'			=> ( ! empty($qaudio)) ? $qaudio : '',
								'qspeech_text'	=> ( ! empty($ddqq_text_to_speech)) ? $ddqq_text_to_speech : '',
								'speech_text'		=> ( ! empty($ddq_text_to_speech)) ? $ddq_text_to_speech : '',
								'audio'			=> ( ! empty($audio)) ? $audio : '',
								'video'			=> ( ! empty($video)) ? $video : '',
								'screen'			=> ( ! empty($ddq_rec_screen)) ? $ddq_rec_screen : '',
								'image'			=> ( ! empty($ddq_img)) ? $ddq_img : '',
								'document'		=> ( ! empty($ddq_doc)) ? $ddq_doc : '',
								'shuffle'			=> (isset($ddq_shuffle2) && ! empty($ddq_shuffle2)) ? 1 : 0,
								'true_options'	=> ( ! empty($ddq_true_option)) ? $ddq_true_option : '',
								'ques_val_1'		=> ( ! empty($ddq_ques_val_1)) ? $ddq_ques_val_1 : '',
								'ques_val_2'		=> ( ! empty($ddq_ques_val_2)) ? $ddq_ques_val_2 : '',
								'ques_val_3'		=> ( ! empty($ddq_ques_val_3)) ? $ddq_ques_val_3 : '',
								'ques_val_4'		=> ( ! empty($ddq_ques_val_4)) ? $ddq_ques_val_4 : '',
								'ques_val_5'		=> ( ! empty($ddq_ques_val_5)) ? $ddq_ques_val_5 : '',
								'ques_val_6'		=> ( ! empty($ddq_ques_val_6)) ? $ddq_ques_val_6 : '',
								'status' 			=> 1,
								'user_id'			=> $userId,
								'cur_date'		=> $curdate,
								'qid'				=> $ques_id);
				$ques_sql = "INSERT INTO `question_tbl` (`scenario_id`,`questions`,`videoq_media_file`,`seleted_drag_option`,`question_type`, `qaudio`, `qspeech_text`,`speech_text`, `audio`,`video`, `screen`, `image`, `document`, `ques_val_1`, `ques_val_2`, `ques_val_3`, `ques_val_4`, `ques_val_5`, `ques_val_6`, `status`, `uid`, `cur_date`) 
				VALUES (:scenarioId,:questions,:videoq_media_file,:seleted_drag_option, :qtype, :qaudio, :qspeech_text, :speech_text, :audio, :video, :screen, :image, :document, :ques_val_1, :ques_val_2, :ques_val_3, :ques_val_4, :ques_val_5, :ques_val_6, :status, :user_id, :cur_date)";
				$ques_stmt = $db->prepare($ques_sql);

				if ($ques_stmt->execute($quesData)):
					$ques_id 					= $db->lastInsertId();
				
					#------OPTION------
					if ( ! empty($ddq2_drop)):
						if ( ! empty($updateDDq2Answerid)):
							#----DROP-TARGET----
							$ansSql = "UPDATE `sub_options_tbl` SET `sub_option`=:option, `image`=:image WHERE answer_id=:aid";
							$stmt = $db->prepare($ansSql);
							$stmt->execute(array('aid' 		=> $updateDDq2Answerid,
												'option'	=> ( ! empty($ddq2_drop)) ? $ddq2_drop : '',
												'image' 	=> ( ! empty($ddq2_item_img)) ? $ddq2_item_img : ''));
							#-----DRAG-ITEMS------
							$ddq2iu = 0; $ddq2true = 1;
							foreach ($ddq2_drag as $subAnswerData):
								if ( ! empty($updateDDq2SubAnswerid[$ddq2iu])):
									$getOpId = $updateDDq2SubAnswerid[$ddq2iu];
									$updateAnsSql  = "UPDATE `answer_tbl` SET `choice_option`=:option, `image`=:image, `true_option`=:true, `status`=:status, `uid`=:user_id, `cur_date`=:cur_date WHERE answer_id=:aid";
									$updateAnsstmt = $db->prepare($updateAnsSql);
									$updateAnsstmt->execute(array('aid' 		=> $getOpId,
																'option'		=> ( ! empty($ddq2_drag[$ddq2iu])) ? $ddq2_drag[$ddq2iu] : '',
																'image'		=> ( ! empty($ddq2_drag_item_img[$ddq2iu])) ? $ddq2_drag_item_img[$ddq2iu] : '',
																'true'		=> ( ! empty($ddq_true_option) && $ddq_true_option == $ddq2true) ? 1 : '',
																'status'		=> 1,
																'user_id' 	=> $userId,
																'cur_date'	=> $curdate));
								else:
									$ansSql = "INSERT INTO `answer_tbl` (`question_id`, `choice_option`, `image`, `true_option`, `status`, `uid`, `cur_date`) values (:qid, :option, :image, :true, :status, :user_id, :cur_date)";
									$stmt = $db->prepare($ansSql);
									$stmt->execute(array('qid'		=> $ques_id,
														'option'	=> ( ! empty($ddq2_drag[$ddq2iu])) ? addslashes($ddq2_drag[$ddq2iu]) : '',
														'image'	=> ( ! empty($ddq2_drag_item_img[$ddq2iu])) ? $ddq2_drag_item_img[$ddq2iu] : '',
														'true'		=> ( ! empty($ddq_true_option) && $ddq_true_option == $ddq2true) ? 1 : '',
														'status'	=> 1,
														'user_id' 	=> $userId,
														'cur_date'	=> $curdate));
								endif;
								$ddq2iu++; $ddq2true++;
							endforeach;
						else:
							#----DROP-TARGET----
							$ansSql = "INSERT INTO `sub_options_tbl` (`question_id`, `sub_option`, `image`) values (:qid, :option, :image)";
							$stmt = $db->prepare($ansSql);
							$stmt->execute(array('qid' 		=> $ques_id,
												'option'	=> ( ! empty($ddq2_drop)) ? addslashes($ddq2_drop) : '',
												'image' 	=> ( ! empty($ddq2_item_img)) ? $ddq2_item_img : ''));
						
							#-----DRAG-ITEMS------
							$ddq2i = 0; $ddq2itrue = 1;
							foreach ($ddq2_drag as $answerData):
								$ansSql = "INSERT INTO `answer_tbl` (`question_id`, `choice_option`, `image`, `true_option`, `status`, `uid`, `cur_date`) values (:qid, :choice, :image, :true, :status, :user_id, :cur_date)";
								$stmt = $db->prepare($ansSql);
								$stmt->execute(array('qid'		=> $ques_id,
													'choice'	=> ( ! empty($ddq2_drag[$ddq2i])) ? addslashes($ddq2_drag[$ddq2i]) : '',
													'image'	=> ( ! empty($ddq2_drag_item_img[$ddq2i])) ? $ddq2_drag_item_img[$ddq2i] : '',
													'true'		=> ( ! empty($ddq_true_option) && $ddq_true_option == $ddq2itrue) ? 1 : '',
													'status'	=> 1,
													'user_id' 	=> $userId,
													'cur_date'	=> $curdate));
								$ddq2i++; $ddq2itrue++;
							endforeach;
						endif;
					endif;

					#-----Remove-Old-Feedback-------
					$db->feedback_remove($ques_id);

					#--------Correct-Feedback-------
					$cfeedData = array('qid'			=> ( ! empty($ques_id)) ? $ques_id : '',
										'nextQid'		=> ( ! empty($mappedQuestionDNDCorrect)) ? $mappedQuestionDNDCorrect : '',
										'EndSim'		=> (isset($chkCorDNDEndSim) && ! empty($chkCorDNDEndSim)) ? 1 : 0,
									'feedback'		=> ( ! empty($ddq_cfeedback)) ? addslashes($ddq_cfeedback) : '',
									'ftype'			=> 1,
									'speech_text'	=> ( ! empty($ddq_feedback_text_to_speech)) ? addslashes($ddq_feedback_text_to_speech) : '',
									'audio'			=> ( ! empty($feed_audio)) ? $feed_audio : '',
									'video'			=> ( ! empty($feed_video)) ? $feed_video : '',
									'screen'			=> ( ! empty($ddq_feedback_rec_screen)) ? $ddq_feedback_rec_screen : '',
									'image'			=> ( ! empty($ddq_feedback_img)) ? $ddq_feedback_img : '',
									'document'		=> ( ! empty($ddq_feedback_doc)) ? $ddq_feedback_doc : '');

					$cfeed_sql = "INSERT INTO `feedback_tbl` (`question_id`,`nextQid`, `EndSim`,`feedback`, `feedback_type`, `feed_speech_text`, `feed_audio`, `feed_video`, `feed_screen`, `feed_image`, `feed_document`) VALUES (:qid,:nextQid, :EndSim,:feedback, :ftype, :speech_text, :audio, :video, :screen, :image, :document)";
					$cfeed_stmt = $db->prepare($cfeed_sql);
					$cfeed_stmt->execute($cfeedData);

					#--------Incorrect-Feedback-------
					$ifeedData = array('qid'			=> ( ! empty($ques_id)) ? $ques_id : '',
										'nextQid'		=> ( ! empty($mappedQuestionDNDIncorrect)) ? $mappedQuestionDNDIncorrect : '',
										'EndSim'		=> (isset($chkInDNDEndSim) && ! empty($chkInDNDEndSim)) ? 1 : 0,
									'feedback'		=> ( ! empty($ddq_ifeedback)) ? addslashes($ddq_ifeedback) : '',
									'ftype'			=> 2,
									'speech_text'	=> ( ! empty($ddqi_feedback_text_to_speech)) ? addslashes($ddqi_feedback_text_to_speech) : '',
									'audio'			=> ( ! empty($ifeed_audio)) ? $ifeed_audio : '',
									'video'			=> ( ! empty($ifeed_video)) ? $ifeed_video : '',
									'screen'			=> ( ! empty($ddqi_feedback_rec_screen)) ? $ddqi_feedback_rec_screen : '',
									'image'			=> ( ! empty($ddqi_feedback_img)) ? $ddqi_feedback_img : '',
									'document'		=> ( ! empty($ddqi_feedback_doc)) ? $ddqi_feedback_doc : '');

					$ifeed_sql = "INSERT INTO `feedback_tbl` (`question_id`,`nextQid`, `EndSim`,`feedback`, `feedback_type`, `feed_speech_text`, `feed_audio`, `feed_video`, `feed_screen`, `feed_image`, `feed_document`) VALUES (:qid, :nextQid,:EndSim,:feedback, :ftype, :speech_text, :audio, :video, :screen, :image, :document)";
					$ifeed_stmt = $db->prepare($ifeed_sql);
					$ifeed_stmt->execute($ifeedData);
				endif;
			endif;
		endif;

		//Update MCQ Question
		if($question_type == 4):
			#-------QUESTIONS-DATA---
			$qaudio = '';
			if ( ! empty($mcqq_qaudio)):
				$qaudio = $mcqq_qaudio;
			elseif ( ! empty($mcqq_rec_audio)):
				$qaudio = $mcqq_rec_audio;
			endif;
			
			$audio = '';
			if ( ! empty($mcq_audio)):
				$audio = $mcq_audio;
			elseif ( ! empty($mcq_rec_audio)):
				$audio = $mcq_rec_audio;
			endif;
			
			$video = '';
			if ( ! empty($mcq_video)):
				$video = $mcq_video;
			elseif ( ! empty($mcq_rec_video)):
				$video = $mcq_rec_video;
			endif;
			
			#------Correct-FEEDBACK-DATA---
			$feed_audio = '';
			if ( ! empty($mcq_feedback_audio)):
				$feed_audio = $mcq_feedback_audio;
			elseif ( ! empty($mcq_feedback_rec_audio)):
				$feed_audio = $mcq_feedback_rec_audio;
			endif;
			
			$feed_video = '';
			if ( ! empty($mcq_feedback_video)):
				$feed_video = $mcq_feedback_video;
			elseif ( ! empty($mcq_feedback_rec_video)):
				$feed_video = $mcq_feedback_rec_video;
			endif;
			
			#------Incorrect-FEEDBACK-DATA---
			$ifeed_audio = '';
			if ( ! empty($mcqi_feedback_audio)):
				$ifeed_audio = $mcqi_feedback_audio;
			elseif ( ! empty($mcqi_feedback_rec_audio)):
				$ifeed_audio = $mcqi_feedback_rec_audio;
			endif;
			
			$ifeed_video = '';
			if ( ! empty($mcqi_feedback_video)):
				$ifeed_video = $mcqi_feedback_video;
			elseif ( ! empty($mcqi_feedback_rec_video)):
				$ifeed_video = $mcqi_feedback_rec_video;
			endif;
						
			$quesData = array('questions'		=> ( ! empty($mcq)) ? addslashes($mcq) : '',
							'qtype'			=> ( ! empty($question_type)) ? $question_type : '',
							'qaudio'			=> ( ! empty($qaudio)) ? $qaudio : '',
							'qspeech_text'	=> ( ! empty($mcqq_text_to_speech)) ? addslashes($mcqq_text_to_speech) : '',
							'speech_text'		=> ( ! empty($mcq_text_to_speech)) ? addslashes($mcq_text_to_speech) : '',
							'audio'			=> ( ! empty($audio)) ? $audio : '',
							'video'			=> ( ! empty($video)) ? $video : '',
							'screen'			=> ( ! empty($mcq_rec_screen)) ? $mcq_rec_screen : '',
							'image'			=> ( ! empty($mcq_img)) ? $mcq_img : '',
							'document'		=> ( ! empty($mcq_doc)) ? $mcq_doc : '',
							'true_option'		=> ( ! empty($mcq_true_option)) ? $mcq_true_option : '',
							'shuffle'			=> (isset($mcq_shuffle) && ! empty($mcq_shuffle)) ? 1 : 0,
							'ques_val_1'		=> ( ! empty($mcq_ques_val_1)) ? $mcq_ques_val_1 : '',
							'ques_val_2'		=> ( ! empty($mcq_ques_val_2)) ? $mcq_ques_val_2 : '',
							'ques_val_3'		=> ( ! empty($mcq_ques_val_3)) ? $mcq_ques_val_3 : '',
							'ques_val_4'		=> ( ! empty($mcq_ques_val_4)) ? $mcq_ques_val_4 : '',
							'ques_val_5'		=> ( ! empty($mcq_ques_val_5)) ? $mcq_ques_val_5 : '',
							'ques_val_6'		=> ( ! empty($mcq_ques_val_6)) ? $mcq_ques_val_6 : '',
							'critical'		=> ( ! empty($mcq_criticalQ)) ? $mcq_criticalQ : '',
							'status' 			=> 1,
							'user_id'			=> $userId,
							'cur_date'		=> $curdate,
							'qid'				=> $ques_id);
			$ques_sql = "UPDATE `question_tbl` SET `questions`=:questions, `question_type`=:qtype, `qaudio`=:qaudio, `qspeech_text`=:qspeech_text, `speech_text`=:speech_text, `audio`=:audio, `video`=:video, `screen`=:screen, `image`=:image, `document`=:document, `true_options`=:true_option, `shuffle`=:shuffle, `ques_val_1`=:ques_val_1, `ques_val_2`=:ques_val_2, `ques_val_3`=:ques_val_3, `ques_val_4`=:ques_val_4, `ques_val_5`=:ques_val_5, `ques_val_6`=:ques_val_6,`critical`=:critical, `status`=:status, `uid`=:user_id, `cur_date`=:cur_date WHERE question_id=:qid";
			$ques_stmt = $db->prepare($ques_sql);
			$ques_stmt->execute($quesData);
			
			#-----Remove-Old-Feedback-------
			$db->feedback_remove($ques_id);
			
			#--------Correct-Feedback----------
			$cfeedData = array('qid'			=> ( ! empty($ques_id)) ? $ques_id : '',
								'nextQid'		=> ( ! empty($mappedQuestionMCQCorrect)) ? $mappedQuestionMCQCorrect : '',
								'EndSim'		=> (isset($chkCorMCQEndSim) && ! empty($chkCorMCQEndSim)) ? 1 : 0,
							'feedback'		=> ( ! empty($mcq_cfeedback)) ? addslashes($mcq_cfeedback) : '',
							'ftype'			=> 1,
							'speech_text'	=> ( ! empty($mcq_feedback_text_to_speech)) ? addslashes($mcq_feedback_text_to_speech) : '',
							'audio'			=> ( ! empty($feed_audio)) ? $feed_audio : '',
							'video'			=> ( ! empty($feed_video)) ? $feed_video : '',
							'screen'			=> ( ! empty($mcq_feedback_rec_screen)) ? $mcq_feedback_rec_screen : '',
							'image'			=> ( ! empty($mcq_feedback_img)) ? $mcq_feedback_img : '',
							'document'		=> ( ! empty($mcq_feedback_doc)) ? $mcq_feedback_doc : '');
			
			$cfeed_sql = "INSERT INTO `feedback_tbl` (`question_id`, `nextQid`,`EndSim`,`feedback`, `feedback_type`, `feed_speech_text`, `feed_audio`, `feed_video`, `feed_screen`, `feed_image`, `feed_document`) VALUES (:qid, :nextQid,:EndSim,:feedback, :ftype, :speech_text, :audio, :video, :screen, :image, :document)";
			$cfeed_stmt = $db->prepare($cfeed_sql);
			$cfeed_stmt->execute($cfeedData);
			
			#--------Incorrect-Feedback-------
			$ifeedData = array('qid'			=> ( ! empty($ques_id)) ? $ques_id : '',
								'nextQid'		=> ( ! empty($mappedQuestionMCQIncorrect)) ? $mappedQuestionMCQIncorrect : '',
								'EndSim'		=> (isset($chkInMCQEndSim) && ! empty($chkInMCQEndSim)) ? 1 : 0,
							'feedback'		=> ( ! empty($mcq_ifeedback)) ? addslashes($mcq_ifeedback) : '',
							'ftype'			=> 2,
							'speech_text'	=> ( ! empty($mcqi_feedback_text_to_speech)) ? addslashes($mcqi_feedback_text_to_speech) : '',
							'audio'			=> ( ! empty($ifeed_audio)) ? $ifeed_audio : '',
							'video'			=> ( ! empty($ifeed_video)) ? $ifeed_video : '',
							'screen'			=> ( ! empty($mcqi_feedback_rec_screen)) ? $mcqi_feedback_rec_screen : '',
							'image'			=> ( ! empty($mcqi_feedback_img)) ? $mcqi_feedback_img : '',
							'document'		=> ( ! empty($mcqi_feedback_doc)) ? $mcqi_feedback_doc : '');
			
			$ifeed_sql = "INSERT INTO `feedback_tbl` (`question_id`,`nextQid`, `EndSim`,`feedback`, `feedback_type`, `feed_speech_text`, `feed_audio`, `feed_video`, `feed_screen`, `feed_image`, `feed_document`) VALUES (:qid, :nextQid,:EndSim,:feedback, :ftype, :speech_text, :audio, :video, :screen, :image, :document)";
			$ifeed_stmt = $db->prepare($ifeed_sql);
			$ifeed_stmt->execute($ifeedData);
			
			#----------ADD-OPTIONS-------------
			if ( ! empty($mcq_option[0])):
				$mcqi = 0; $mcqtrue = 1;
				foreach ($mcq_option as $answerData):
					if ( ! empty($updateMCQAnswerid[$mcqi])):
						$ansSql = "UPDATE `answer_tbl` SET `choice_option`=:option, `true_option`=:true, `status`=:status, `uid`=:user_id, `cur_date`=:cur_date WHERE answer_id=:aid";
						$stmt = $db->prepare($ansSql);
						$stmt->execute(array('aid'		=> $updateMCQAnswerid[$mcqi],
											'option'	=> ( ! empty($mcq_option[$mcqi])) ? $mcq_option[$mcqi] : '',
											'true'		=> ( ! empty($mcq_true_option) && $mcq_true_option == $mcqtrue) ? 1 : '',
											'status'	=> 1,
											'user_id' 	=> $userId,
											'cur_date'	=> $curdate));

					else:
						$ansSql = "INSERT INTO `answer_tbl` (`question_id`, `choice_option`, `true_option`, `status`, `uid`, `cur_date`) values (:qid, :option, :true, :status, :user_id, :cur_date)";
						$stmt = $db->prepare($ansSql);
						$stmt->execute(array('qid'		=> $ques_id,
											'option'	=> ( ! empty($mcq_option[$mcqi])) ? addslashes($mcq_option[$mcqi]) : '',
											'true'		=> ( ! empty($mcq_true_option) && $mcq_true_option == $mcqtrue) ? 1 : '',
											'status'	=> 1,
											'user_id' 	=> $userId,
											'cur_date'	=> $curdate));
					endif;
					$mcqi++; $mcqtrue++;
				endforeach;
			endif;
		endif;

		//update MMCQ
		if($question_type == 5):
			#-------QUESTIONS-DATA---
			$qaudio = '';
			if ( ! empty($mmcqq_qaudio)):
				$qaudio = $mmcqq_qaudio;
			elseif ( ! empty($mmcqq_rec_audio)):
				$qaudio = $mmcqq_rec_audio;
			endif;

			$audio = '';
			if ( ! empty($mmcq_audio)):
				$audio = $mmcq_audio;
			elseif ( ! empty($mmcq_rec_audio)):
				$audio = $mmcq_rec_audio;
			endif;
			
			$video = '';
			if ( ! empty($mmcq_video)):
				$video = $mmcq_video;
			elseif ( ! empty($mmcq_rec_video)):
				$video = $mmcq_rec_video;
			endif;
			
			#------Correct-FEEDBACK-DATA---
			$feed_audio = '';
			if ( ! empty($mmcq_feedback_audio)):
				$feed_audio = $mmcq_feedback_audio;
			elseif ( ! empty($mmcq_feedback_rec_audio)):
				$feed_audio = $mmcq_feedback_rec_audio;
			endif;
			
			$feed_video = '';
			if ( ! empty($mmcq_feedback_video)):
				$feed_video = $mmcq_feedback_video;
			elseif ( ! empty($mmcq_feedback_rec_video)):
				$feed_video = $mmcq_feedback_rec_video;
			endif;
			
			#------Incorrect-FEEDBACK-DATA---
			$ifeed_audio = '';
			if ( ! empty($mmcqi_feedback_audio)):
				$ifeed_audio = $mmcqi_feedback_audio;
			elseif ( ! empty($mmcqi_feedback_rec_audio)):
				$ifeed_audio = $mmcqi_feedback_rec_audio;
			endif;
			
			$ifeed_video = '';
			if ( ! empty($mmcqi_feedback_video)):
				$ifeed_video = $mmcqi_feedback_video;
			elseif ( ! empty($mmcqi_feedback_rec_video)):
				$ifeed_video = $mmcqi_feedback_rec_video;
			endif;
			
			$quesData = array('questions'		=> ( ! empty($mmcq)) ? addslashes($mmcq) : '',
							'qtype'			=> ( ! empty($question_type)) ? $question_type : '',
								'qaudio'			=> ( ! empty($qaudio)) ? $qaudio : '',
								'qspeech_text'	=> ( ! empty($mmcqq_text_to_speech)) ? addslashes($mmcqq_text_to_speech) : '',
							'speech_text'		=> ( ! empty($mmcq_text_to_speech)) ? addslashes($mmcq_text_to_speech) : '',
							'audio'			=> ( ! empty($audio)) ? $audio : '',
							'video'			=> ( ! empty($video)) ? $video : '',
							'screen'			=> ( ! empty($mmcq_rec_screen)) ? $mmcq_rec_screen : '',
							'image'			=> ( ! empty($mmcq_img)) ? $mmcq_img : '',
							'document'		=> ( ! empty($mmcq_doc)) ? $mmcq_doc : '',
							'true_option'		=> ( ! empty($mmcq_true_option)) ? $db->addMultiIds($mmcq_true_option) : '',
							'shuffle'			=> (isset($mmcq_shuffle) && ! empty($mmcq_shuffle)) ? 1 : 0,
							'ques_val_1'		=> ( ! empty($mmcq_ques_val_1)) ? $mmcq_ques_val_1 : '',
							'ques_val_2'		=> ( ! empty($mmcq_ques_val_2)) ? $mmcq_ques_val_2 : '',
							'ques_val_3'		=> ( ! empty($mmcq_ques_val_3)) ? $mmcq_ques_val_3 : '',
							'ques_val_4'		=> ( ! empty($mmcq_ques_val_4)) ? $mmcq_ques_val_4 : '',
							'ques_val_5'		=> ( ! empty($mmcq_ques_val_5)) ? $mmcq_ques_val_5 : '',
							'ques_val_6'		=> ( ! empty($mmcq_ques_val_6)) ? $mmcq_ques_val_6 : '',
							'status' 			=> 1,
							'user_id'			=> $userId,
							'cur_date'		=> $curdate,
							'qid'				=> $ques_id);
			$ques_sql = "UPDATE `question_tbl` SET `questions`=:questions, `question_type`=:qtype, `qaudio`=:qaudio, `qspeech_text`=:qspeech_text, `speech_text`=:speech_text, `audio`=:audio, `video`=:video, `screen`=:screen, `image`=:image, `document`=:document, `true_options`=:true_option, `shuffle`=:shuffle, `ques_val_1`=:ques_val_1, `ques_val_2`=:ques_val_2, `ques_val_3`=:ques_val_3, `ques_val_4`=:ques_val_4, `ques_val_5`=:ques_val_5, `ques_val_6`=:ques_val_6, `status`=:status, `uid`=:user_id, `cur_date`=:cur_date WHERE question_id=:qid";
			$ques_stmt = $db->prepare($ques_sql);
			$ques_stmt->execute($quesData);
			
			#-----Remove-Old-Feedback-------
			$db->feedback_remove($ques_id);
			
			#--------Correct-Feedback----------
			$cfeedData = array('qid'			=> ( ! empty($ques_id)) ? $ques_id : '',
								'nextQid'		=> ( ! empty($mappedQuestionMMCQCorrect)) ? $mappedQuestionMMCQCorrect : '',
								'EndSim'		=> (isset($chkCorMMCQEndSim) && ! empty($chkCorMMCQEndSim)) ? 1 : 0,
							'feedback'		=> ( ! empty($mmcq_cfeedback)) ? addslashes($mmcq_cfeedback) : '',
							'ftype'			=> 1,
							'speech_text'	=> ( ! empty($mmcq_feedback_text_to_speech)) ? addslashes($mmcq_feedback_text_to_speech) : '',
							'audio'			=> ( ! empty($feed_audio)) ? $feed_audio : '',
							'video'			=> ( ! empty($feed_video)) ? $feed_video : '',
							'screen'			=> ( ! empty($mmcq_feedback_rec_screen)) ? $mmcq_feedback_rec_screen : '',
							'image'			=> ( ! empty($mmcq_feedback_img)) ? $mmcq_feedback_img : '',
							'document'		=> ( ! empty($mmcq_feedback_doc)) ? $mmcq_feedback_doc : '');
			
			$cfeed_sql = "INSERT INTO `feedback_tbl` (`question_id`, `nextQid`,`EndSim`,`feedback`, `feedback_type`, `feed_speech_text`, `feed_audio`, `feed_video`, `feed_screen`, `feed_image`, `feed_document`) VALUES (:qid, :nextQid,:EndSim,:feedback, :ftype, :speech_text, :audio, :video, :screen, :image, :document)";
			$cfeed_stmt = $db->prepare($cfeed_sql);
			$cfeed_stmt->execute($cfeedData);
			
			#--------Incorrect-Feedback-------
			$ifeedData = array('qid'			=> ( ! empty($ques_id)) ? $ques_id : '',
								'nextQid'		=> ( ! empty($mappedQuestionMMCQIncorrect)) ? $mappedQuestionMMCQIncorrect : '',
								'EndSim'		=> (isset($chkInMMCQEndSim) && ! empty($chkInMMCQEndSim)) ? 1 : 0,
							'feedback'		=> ( ! empty($mmcq_ifeedback)) ? addslashes($mmcq_ifeedback) : '',
							'ftype'			=> 2,
							'speech_text'	=> ( ! empty($mmcqi_feedback_text_to_speech)) ? addslashes($mmcqi_feedback_text_to_speech) : '',
							'audio'			=> ( ! empty($ifeed_audio)) ? $ifeed_audio : '',
							'video'			=> ( ! empty($ifeed_video)) ? $ifeed_video : '',
							'screen'			=> ( ! empty($mmcqi_feedback_rec_screen)) ? $mmcqi_feedback_rec_screen : '',
							'image'			=> ( ! empty($mmcqi_feedback_img)) ? $mmcqi_feedback_img : '',
							'document'		=> ( ! empty($mmcqi_feedback_doc)) ? $mmcqi_feedback_doc : '');
			
			$ifeed_sql = "INSERT INTO `feedback_tbl` (`question_id`,`nextQid`, `EndSim`,`feedback`, `feedback_type`, `feed_speech_text`, `feed_audio`, `feed_video`, `feed_screen`, `feed_image`, `feed_document`) VALUES (:qid, :nextQid,:EndSim,:feedback, :ftype, :speech_text, :audio, :video, :screen, :image, :document)";
			$ifeed_stmt = $db->prepare($ifeed_sql);
			$ifeed_stmt->execute($ifeedData);
			
			#----------ADD-OPTIONS-------------
			if ( ! empty($mmcq_option[0])):
				$mmcqi = 0; $mmcqtrue = 1;
				foreach ($mmcq_option as $answerData):
					if ( ! empty($updateMMCQAnswerid[$mmcqi])):
						$ansSql = "UPDATE `answer_tbl` SET `choice_option`=:option, `true_option`=:true, `status`=:status, `uid`=:user_id, `cur_date`=:cur_date WHERE answer_id=:aid";
						$stmt = $db->prepare($ansSql);
						$stmt->execute(array('aid'		=> $updateMMCQAnswerid[$mmcqi],
											'option'	=> ( ! empty($mmcq_option[$mmcqi])) ? $mmcq_option[$mmcqi] : '',
											'true'		=> ( ! empty($mmcq_true_option[$mmcqi]) && $mmcq_true_option[$mmcqi] == $mmcqtrue) ? 1 : '',
											'status'	=> 1,
											'user_id' 	=> $userId,
											'cur_date'	=> $curdate));
					else:
						$ansSql = "INSERT INTO `answer_tbl` (`question_id`, `choice_option`, `true_option`, `status`, `uid`, `cur_date`) values (:qid, :option, :true, :status, :user_id, :cur_date)";
						$stmt = $db->prepare($ansSql);
						$stmt->execute(array('qid'		=> $ques_id,
											'option'	=> ( ! empty($mmcq_option[$mmcqi])) ? addslashes($mmcq_option[$mmcqi]) : '',
											'true'		=> ( ! empty($mmcq_true_option[$mmcqi]) && $mmcq_true_option[$mmcqi] == $mmcqtrue) ? 1 : '',
											'status'	=> 1,
											'user_id' 	=> $userId,
											'cur_date'	=> $curdate));
					endif;
					$mmcqi++; $mmcqtrue++;
				endforeach;
			endif;
		endif;

		//update SWIPE
		if($question_type == 7):
			#-------QUESTIONS-DATA---
			$qaudio = '';
			if ( ! empty($swipqq_qaudio)):
				$qaudio = $swipqq_qaudio;
			elseif ( ! empty($swipqq_rec_audio)):
				$qaudio = $swipqq_rec_audio;
			endif;

			$audio = '';
			if ( ! empty($swipq_audio)):
				$audio = $swipq_audio;
			elseif ( ! empty($swipq_rec_audio)):
				$audio = $swipq_rec_audio;
			endif;
			
			$video = '';
			if ( ! empty($swipq_video)):
				$video = $swipq_video;
			elseif ( ! empty($swipq_rec_video)):
				$video = $swipq_rec_video;
			endif;
			
			#------Correct-FEEDBACK-DATA---
			$feed_audio = '';
			if ( ! empty($swipq_feedback_audio)):
				$feed_audio = $swipq_feedback_audio;
			elseif ( ! empty($swipq_feedback_rec_audio)):
				$feed_audio = $swipq_feedback_rec_audio;
			endif;
			
			$feed_video = '';
			if ( ! empty($swipq_feedback_video)):
				$feed_video = $swipq_feedback_video;
			elseif ( ! empty($swipq_feedback_rec_video)):
				$feed_video = $swipq_feedback_rec_video;
			endif;
			
			#------Incorrect-FEEDBACK-DATA---
			$ifeed_audio = '';
			if ( ! empty($swipqi_feedback_audio)):
				$ifeed_audio = $swipqi_feedback_audio;
			elseif ( ! empty($swipqi_feedback_rec_audio)):
				$ifeed_audio = $swipqi_feedback_rec_audio;
			endif;
			
			$ifeed_video = '';
			if ( ! empty($swipqi_feedback_video)):
				$ifeed_video = $swipqi_feedback_video;
			elseif ( ! empty($swipqi_feedback_rec_video)):
				$ifeed_video = $swipqi_feedback_rec_video;
			endif;
						
			$quesData = array('questions'		=> ( ! empty($swipq)) ? addslashes($swipq) : '',
							'qtype'			=> ( ! empty($question_type)) ? $question_type : '',
							'qaudio'			=> ( ! empty($qaudio)) ? $qaudio : '',
							'qspeech_text'	=> ( ! empty($swipqq_text_to_speech)) ? addslashes($swipqq_text_to_speech) : '',
							'speech_text'		=> ( ! empty($swipq_text_to_speech)) ? addslashes($swipq_text_to_speech) : '',
							'audio'			=> ( ! empty($audio)) ? $audio : '',
							'video'			=> ( ! empty($video)) ? $video : '',
							'screen'			=> ( ! empty($swipq_rec_screen)) ? $swipq_rec_screen : '',
							'image'			=> ( ! empty($swipq_img)) ? $swipq_img : '',
							'document'		=> ( ! empty($swipq_doc)) ? $swipq_doc : '',
							'true_option'		=> ( ! empty($swipq_true_option)) ? $swipq_true_option : '',
							'shuffle'			=> (isset($swipq_shuffle) && ! empty($swipq_shuffle)) ? 1 : 0,
							'ques_val_1'		=> ( ! empty($swipq_ques_val_1)) ? $swipq_ques_val_1 : '',
							'ques_val_2'		=> ( ! empty($swipq_ques_val_2)) ? $swipq_ques_val_2 : '',
							'ques_val_3'		=> ( ! empty($swipq_ques_val_3)) ? $swipq_ques_val_3 : '',
							'ques_val_4'		=> ( ! empty($swipq_ques_val_4)) ? $swipq_ques_val_4 : '',
							'ques_val_5'		=> ( ! empty($swipq_ques_val_5)) ? $swipq_ques_val_5 : '',
							'ques_val_6'		=> ( ! empty($swipq_ques_val_6)) ? $swipq_ques_val_6 : '',
							'status' 			=> 1,
							'user_id'			=> $userId,
							'cur_date'		=> $curdate,
							'qid'				=> $ques_id);
			$ques_sql = "UPDATE `question_tbl` SET `questions`=:questions, `question_type`=:qtype, `qaudio`=:qaudio, `qspeech_text`=:qspeech_text, `speech_text`=:speech_text, `audio`=:audio, `video`=:video, `screen`=:screen, `image`=:image, `document`=:document, `true_options`=:true_option, `shuffle`=:shuffle, `ques_val_1`=:ques_val_1, `ques_val_2`=:ques_val_2, `ques_val_3`=:ques_val_3, `ques_val_4`=:ques_val_4, `ques_val_5`=:ques_val_5, `ques_val_6`=:ques_val_6, `status`=:status, `uid`=:user_id, `cur_date`=:cur_date WHERE question_id=:qid";
			$ques_stmt = $db->prepare($ques_sql);
			$ques_stmt->execute($quesData);
			
			#-----Remove-Old-Feedback-------
			$db->feedback_remove($ques_id);
			
			#--------Correct-Feedback----------
			$cfeedData = array('qid'			=> ( ! empty($ques_id)) ? $ques_id : '',
								'nextQid'		=> ( ! empty($mappedQuestionSwipeCorrect)) ? $mappedQuestionSwipeCorrect : '',
								'EndSim'		=> (isset($chkCorSwipeEndSim) && ! empty($chkCorSwipeEndSim)) ? 1 : 0,
							'feedback'		=> ( ! empty($swipq_cfeedback)) ? addslashes($swipq_cfeedback) : '',
							'ftype'			=> 1,
							'speech_text'	=> ( ! empty($swipq_feedback_text_to_speech)) ? addslashes($swipq_feedback_text_to_speech) : '',
							'audio'			=> ( ! empty($feed_audio)) ? $feed_audio : '',
							'video'			=> ( ! empty($feed_video)) ? $feed_video : '',
							'screen'			=> ( ! empty($swipq_feedback_rec_screen)) ? $swipq_feedback_rec_screen : '',
							'image'			=> ( ! empty($swipq_feedback_img)) ? $swipq_feedback_img : '',
							'document'		=> ( ! empty($swipq_feedback_doc)) ? $swipq_feedback_doc : '');
			
			$cfeed_sql = "INSERT INTO `feedback_tbl` (`question_id`, `nextQid`,`EndSim`,`feedback`, `feedback_type`, `feed_speech_text`, `feed_audio`, `feed_video`, `feed_screen`, `feed_image`, `feed_document`) VALUES (:qid, :nextQid,:EndSim,:feedback, :ftype, :speech_text, :audio, :video, :screen, :image, :document)";
			$cfeed_stmt = $db->prepare($cfeed_sql);
			$cfeed_stmt->execute($cfeedData);
			
			#--------Incorrect-Feedback-------
			$ifeedData = array('qid'			=> ( ! empty($ques_id)) ? $ques_id : '',
								'nextQid'		=> ( ! empty($mappedQuestionSwipeIncorrect)) ? $mappedQuestionSwipeIncorrect : '',
								'EndSim'		=> (isset($chkInSwipeEndSim) && ! empty($chkInSwipeEndSim)) ? 1 : 0,
							'feedback'		=> ( ! empty($swipq_ifeedback)) ? addslashes($swipq_ifeedback) : '',
							'ftype'			=> 2,
							'speech_text'	=> ( ! empty($swipqi_feedback_text_to_speech)) ? addslashes($swipqi_feedback_text_to_speech) : '',
							'audio'			=> ( ! empty($ifeed_audio)) ? $ifeed_audio : '',
							'video'			=> ( ! empty($ifeed_video)) ? $ifeed_video : '',
							'screen'			=> ( ! empty($swipqi_feedback_rec_screen)) ? $swipqi_feedback_rec_screen : '',
							'image'			=> ( ! empty($swipqi_feedback_img)) ? $swipqi_feedback_img : '',
							'document'		=> ( ! empty($swipqi_feedback_doc)) ? $swipqi_feedback_doc : '');
			
			$ifeed_sql = "INSERT INTO `feedback_tbl` (`question_id`, `nextQid`,`EndSim`,`feedback`, `feedback_type`, `feed_speech_text`, `feed_audio`, `feed_video`, `feed_screen`, `feed_image`, `feed_document`) VALUES (:qid, :nextQid,:EndSim,:feedback, :ftype, :speech_text, :audio, :video, :screen, :image, :document)";
			$ifeed_stmt = $db->prepare($ifeed_sql);
			$ifeed_stmt->execute($ifeedData);
			
			#----------ADD-OPTIONS-------------
			if ( ! empty($swipq_option[0])):
				$swipqi = 0; $swiptrue = 1;
				foreach ($swipq_option as $answerData):
					if ( ! empty($updateSwipqAnswerid[$swipqi])):
						$ansSql = "UPDATE `answer_tbl` SET `choice_option`=:option, `true_option`=:true, `image`=:image, `status`=:status, `uid`=:user_id, `cur_date`=:cur_date WHERE answer_id=:aid";
						$stmt = $db->prepare($ansSql);
						$stmt->execute(array('aid' 		=> $updateSwipqAnswerid[$swipqi],
											'option'	=> ( ! empty($swipq_option[$swipqi])) ? $swipq_option[$swipqi] : '',
											'image'	=> ( ! empty($swipq_item_img[$swipqi])) ? $swipq_item_img[$swipqi] : '',
											'true'		=> ( ! empty($swipq_true_option) && $swipq_true_option == $swiptrue) ? 1 : '',
											'status'	=> 1,
											'user_id' 	=> $userId,
											'cur_date'	=> $curdate));
					else:
						$ansSql = "INSERT INTO `answer_tbl` (`question_id`, `choice_option`, `true_option`, `image`, `status`, `uid`, `cur_date`) values (:qid, :option, :true, :image, :status, :user_id, :cur_date)";
						$stmt = $db->prepare($ansSql);
						$stmt->execute(array('qid'		=> $ques_id,
											'option'	=> ( ! empty($swipq_option[$swipqi])) ? addslashes($swipq_option[$swipqi]) : '',
											'image'	=> ( ! empty($swipq_item_img[$swipqi])) ? $swipq_item_img[$swipqi] : '',
											'true'		=> ( ! empty($swipq_true_option) && $swipq_true_option == $swiptrue) ? 1 : '',
											'status'	=> 1,
											'user_id' 	=> $userId,
											'cur_date'	=> $curdate));
					endif;
					$swipqi++; $swiptrue++;
				endforeach;
			endif;
		endif;
	}
	if ( ! empty($submit_type) && $submit_type == 1):
		$resdata = array('success' => TRUE, 'all_update' => FALSE, 'msg' => 'Simulation details updated successfully.', 'return_url' => 'branching-video-multiple-template.php?add_data=true&sim_id='.md5($sim_id).'&temp_type='.$temp_type);
	else:
		$resdata = array('success' => TRUE, 'all_update' => TRUE, 'msg' => 'All questions and answers updated successfully.', 'return_url' => 'author-simulation.php');
	endif;
	echo json_encode($resdata);
endif;

#-----------------------------------------Update-Branching Video multiple Sim - Saumya ----------------------------------
if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['update_branching_video_multiple_sim']) && isset($_POST['sim_id'])):
	extract($_POST);
	$error = FALSE;
	if (empty($sim_page_name[0]) || empty($sim_duration) || empty($sim_title) || empty($passing_marks)):
		$resdata = array('success' => FALSE, 'msg' => 'Please fill all the mandatory fields on add Simulation and Simulation details to proceed.');
		$error = TRUE;
		echo json_encode($resdata); exit;
	elseif (isset($competency_name_1) && empty($competency_name_1) || empty($competency_score_1) || empty($weightage_1)):
		$resdata = array('success' => FALSE, 'msg' => 'Please fill all the mandatory fields on Simulation details to proceed.');
		$error = TRUE;
		echo json_encode($resdata); exit;
	elseif ($error == FALSE):
		$sql = "UPDATE scenario_master SET `Scenario_title` = '". $sim_title ."', `duration` = '". $sim_duration ."', `passing_marks` = '". $passing_marks ."', `sim_cover_img` = '". $scenario_cover_file ."'";
		#--Add-Web-Object--
		if ( ! empty($web_object)):
			$sql .= ", web_object = '". $web_object ."', web_object_title = '". htmlspecialchars($web_object_title) ."'";
		endif;
		#--Sim-Media-File--
		if ( ! empty($scenario_media_file) && in_array(strtolower(pathinfo($scenario_media_file, PATHINFO_EXTENSION)), $img_valid_exts)):
			$sql .= ", image_url = '". $scenario_media_file ."', video_url = NULL, audio_url = NULL";
		elseif ( ! empty($scenario_media_file) && in_array(strtolower(pathinfo($scenario_media_file, PATHINFO_EXTENSION)), $video_valid_exts)):
			$sql .= ", video_url = '". $scenario_media_file ."', image_url = NULL, audio_url = NULL";
		elseif ( ! empty($scenario_media_file) && in_array(strtolower(pathinfo($scenario_media_file, PATHINFO_EXTENSION)), $audio_valid_exts)):
			$sql .= ", audio_url = '". $scenario_media_file ."', video_url = NULL, image_url = NULL";
		elseif ( ! empty($scenario_media_file) && ! in_array(strtolower(pathinfo($scenario_media_file, PATHINFO_EXTENSION)), $audio_valid_exts) && 
				! in_array(strtolower(pathinfo($scenario_media_file, PATHINFO_EXTENSION)), $img_valid_exts) && 
				! in_array(strtolower(pathinfo($scenario_media_file, PATHINFO_EXTENSION)), $video_valid_exts)):
			$sql .= ", web_object_file = '". $scenario_media_file ."', video_url = NULL, image_url = NULL, audio_url = NULL";
		endif;
		$sql .= " WHERE scenario_id = '". $sim_id ."'";
		$results = $db->prepare($sql);
		if ($results->execute()):
			#----Competency----
			$delCom	 = "DELETE FROM competency_tbl WHERE scenario_id = '". $sim_id ."'";
			$delComq = $db->prepare($delCom); $delComq->execute();
			$data	 = array('scenario_id'	=> $sim_id,
							 'comp_col_1'	=> (isset($competency_name_1) && ! empty($competency_name_1)) ? addslashes($competency_name_1) : '',
							 'comp_val_1'	=> (isset($competency_score_1) && ! empty($competency_score_1)) ? addslashes($competency_score_1) : '',
							 'comp_col_2'	=> (isset($competency_name_2) && ! empty($competency_name_2)) ? addslashes($competency_name_2) : '',
							 'comp_val_2'	=> (isset($competency_score_2) && ! empty($competency_score_2)) ? $competency_score_2 : '',
							 'comp_col_3'	=> (isset($competency_name_3) && ! empty($competency_name_3)) ? addslashes($competency_name_3) : '',
							 'comp_val_3'	=> (isset($competency_score_3) && ! empty($competency_score_3)) ? $competency_score_3 : '',
							 'comp_col_4'	=> (isset($competency_name_4) && ! empty($competency_name_4)) ? addslashes($competency_name_4) : '',
							 'comp_val_4'	=> (isset($competency_score_4) && ! empty($competency_score_4)) ? $competency_score_4 : '',
							 'comp_col_5'	=> (isset($competency_name_5) && ! empty($competency_name_5)) ? addslashes($competency_name_5) : '',
							 'comp_val_5'	=> (isset($competency_score_5) && ! empty($competency_score_5)) ? $competency_score_5 : '',
							 'comp_col_6'	=> (isset($competency_name_6) && ! empty($competency_name_6)) ? addslashes($competency_name_6) : '',
							 'comp_val_6'	=> (isset($competency_score_6) && ! empty($competency_score_6)) ? $competency_score_6 : '',
							 'weightage_1'	=> (isset($weightage_1) && ! empty($weightage_1)) ? $weightage_1 : '',
							 'weightage_2'	=> (isset($weightage_2) && ! empty($weightage_2)) ? $weightage_2 : '',
							 'weightage_3'	=> (isset($weightage_3) && ! empty($weightage_3)) ? $weightage_3 : '',
							 'weightage_4'	=> (isset($weightage_4) && ! empty($weightage_4)) ? $weightage_4 : '',
							 'weightage_5'	=> (isset($weightage_5) && ! empty($weightage_5)) ? $weightage_5 : '',
							 'weightage_6'	=> (isset($weightage_6) && ! empty($weightage_6)) ? $weightage_6 : '',
							 'status' 		=> 1,
							 'user_id' 		=> $userId,
							 'cur_date'		=> $curdate);
			$com_sql  = "INSERT INTO `competency_tbl` (`scenario_id`, `comp_col_1`, `comp_val_1`, `comp_col_2`, `comp_val_2`, `comp_col_3`, `comp_val_3`, `comp_col_4`, `comp_val_4`, `comp_col_5`, `comp_val_5`, `comp_col_6`, `comp_val_6`, `weightage_1`, `weightage_2`, `weightage_3`, `weightage_4`, `weightage_5`, `weightage_6`, `status`, `uid`, `cur_date`) VALUES (:scenario_id, :comp_col_1, :comp_val_1, :comp_col_2, :comp_val_2, :comp_col_3, :comp_val_3, :comp_col_4, :comp_val_4, :comp_col_5, :comp_val_5, :comp_col_6, :comp_val_6, :weightage_1, :weightage_2, :weightage_3, :weightage_4, :weightage_5, :weightage_6, :status, :user_id, :cur_date)";
			$com_stmt = $db->prepare($com_sql);
			$com_stmt->execute($data);
			
			#----PAGES----
			$delPage  = "DELETE FROM sim_pages WHERE scenario_id = '". $sim_id ."'";
			$delPageq = $db->prepare($delPage); $delPageq->execute();
			if ( ! empty($sim_page_name)): $sp = 0;
				foreach ($sim_page_name as $pdata):
					$pageq 		= "INSERT INTO `sim_pages` (`scenario_id`, `sim_page_name`, `sim_page_desc`, `userid`, `status`, `added_date`) VALUES (:sid, :pname, :pdesc, :user_id, :status, :adate)";
					$page_stmt	= $db->prepare($pageq);
					$page_stmt->execute(array('sid'		=> $sim_id,
											  'pname'	=> ( ! empty($pdata)) ? htmlspecialchars($pdata) : '',
											  'pdesc'	=> ( ! empty($sim_page_desc[$sp])) ? $sim_page_desc[$sp] : '',
											  'status' 	=> 1,
											  'user_id'	=> $userId,
											  'adate'	=> $curdate));
				$sp++; endforeach;
			endif;
			
			#----Branding----
			$bandq	  	= "DELETE FROM sim_branding_tbl WHERE scenario_id = '". $sim_id ."'";
			$bandqexe 	= $db->prepare($bandq); $bandqexe->execute();
			$brand_data = array('sid'				=> $sim_id,
								'bg'				=> ( ! empty($ques_bg_color)) ? $ques_bg_color : '',
								'bg_tr'				=> ( ! empty($ques_bg_transp)) ? $ques_bg_transp : '',
								'obg'				=> ( ! empty($ques_option_bg_color)) ? $ques_option_bg_color : '',
								'obg_tr'			=> ( ! empty($ques_option_bg_transp)) ? $ques_option_bg_transp : '',
								'ohover'			=> ( ! empty($ques_option_hover_color)) ? $ques_option_hover_color : '',
								'oh_tr'				=> ( ! empty($ques_option_hover_transp)) ? $ques_option_hover_transp : '',
								'oselected'			=> ( ! empty($ques_option_select_color)) ? $ques_option_select_color : '',
								'oselected_tr'		=> ( ! empty($ques_option_select_transp)) ? $ques_option_select_transp : '',
								'btn_bg'			=> ( ! empty($btn_bg_color)) ? $btn_bg_color : '',
								'btn_bg_tr'			=> ( ! empty($btn_bg_transp)) ? $btn_bg_transp : '',
								'btn_hover'			=> ( ! empty($btn_hover_color)) ? $btn_hover_color : '',
								'btn_hover_tr'		=> ( ! empty($btn_hover_transp)) ? $btn_hover_transp : '',
								'btn_selected'		=> ( ! empty($btn_select_color)) ? $btn_select_color : '',
								'btn_selected_tr'	=> ( ! empty($btn_select_transp)) ? $btn_select_transp : '',
								'font_type'			=> ( ! empty($font_type)) ? $font_type : '',
								'font_color'		=> ( ! empty($font_color)) ? $font_color : '',
								'font_size'			=> ( ! empty($font_size)) ? $font_size : '',
								'user_id' 			=> $userId,
								'added_date'		=> $curdate);
			$com_sql  = "INSERT INTO `sim_branding_tbl` (`scenario_id`, `ques_bg`, `ques_bg_transparency`, `option_bg`, `option_bg_transparency`, `option_hover`, `option_hover_transparency`, `option_selected`, `option_selected_transparency`, `btn_bg`, `btn_bg_transparency`, `btn_hover`, `btn_hover_transparency`, `btn_selected`, `btn_selected_transparency`, `font_type`, `font_color`, `font_size`, `user_id`, `added_date`) 
						 VALUES (:sid, :bg, :bg_tr, :obg, :obg_tr, :ohover, :oh_tr, :oselected, :oselected_tr, :btn_bg, :btn_bg_tr, :btn_hover, :btn_hover_tr, :btn_selected, :btn_selected_tr, :font_type, :font_color, :font_size, :user_id, :added_date)";
			$com_stmt = $db->prepare($com_sql); $com_stmt->execute($brand_data);
		endif;
		//------START-MATCH-THE-FOLLOWING-----Add & Update
		if ($question_type == 1):
			#-------QUESTIONS-DATA---
			$qaudio = '';
			if ( ! empty($mtfq_qaudio)):
				$qaudio = $mtfq_qaudio;
			elseif ( ! empty($mtfq_rec_audio)):
				$qaudio = $mtfq_rec_audio;
			endif;
			
			$audio = '';
			if ( ! empty($mtf_qaudio)):
				$audio = $mtf_qaudio;
			elseif ( ! empty($mtf_rec_audio)):
				$audio = $mtf_rec_audio;
			endif;
			
			$video = '';
			if ( ! empty($mtf_video)):
				$video = $mtf_video;
			elseif ( ! empty($mtf_rec_video)):
				$video = $mtf_rec_video;
			endif;
			
			#------Correct-FEEDBACK-DATA---
			$feed_audio = '';
			if ( ! empty($mtf_feedback_audio)):
				$feed_audio = $mtf_feedback_audio;
			elseif ( ! empty($mtf_feedback_rec_audio)):
				$feed_audio = $mtf_feedback_rec_audio;
			endif;
			
			$feed_video = '';
			if ( ! empty($mtf_feedback_video)):
				$feed_video = $mtf_feedback_video;
			elseif ( ! empty($mtf_feedback_rec_video)):
				$feed_video = $mtf_feedback_rec_video;
			endif;
			
			#------Incorrect-FEEDBACK-DATA---
			$ifeed_audio = '';
			if ( ! empty($mtfi_feedback_audio)):
				$ifeed_audio = $mtfi_feedback_audio;
			elseif ( ! empty($mtfi_feedback_rec_audio)):
				$ifeed_audio = $mtfi_feedback_rec_audio;
			endif;
			
			$ifeed_video = '';
			if ( ! empty($mtfi_feedback_video)):
				$ifeed_video = $mtfi_feedback_video;
			elseif ( ! empty($mtfi_feedback_rec_video)):
				$ifeed_video = $mtfi_feedback_rec_video;
			endif;			
			$quesData = array('questions'		=> ( ! empty($mtfq)) ? htmlspecialchars($mtfq) : '',
							  'qtype'			=> ( ! empty($question_type)) ? $question_type : '',
							  'media_file'		=> ( ! empty($videoq_media_file)) ? $videoq_media_file : '',
							  'qaudio'			=> ( ! empty($qaudio)) ? $qaudio : '',
							  'qspeech_text'	=> ( ! empty($mtfqq_text_to_speech)) ? htmlspecialchars($mtfqq_text_to_speech) : '',
							  'speech_text'		=> ( ! empty($mtfq_text_to_speech)) ? htmlspecialchars($mtfq_text_to_speech) : '',
							  'audio'			=> ( ! empty($audio)) ? $audio : '',
							  'video'			=> ( ! empty($video)) ? $video : '',
							  'screen'			=> ( ! empty($mtf_rec_screen)) ? $mtf_rec_screen : '',
							  'image'			=> ( ! empty($mtf_img)) ? $mtf_img : '',
							  'document'		=> ( ! empty($mtf_doc)) ? $mtf_doc : '',
							  'ques_val_1'		=> ( ! empty($ques_val_1)) ? $ques_val_1 : '',
							  'ques_val_2'		=> ( ! empty($ques_val_2)) ? $ques_val_2 : '',
							  'ques_val_3'		=> ( ! empty($ques_val_3)) ? $ques_val_3 : '',
							  'ques_val_4'		=> ( ! empty($ques_val_4)) ? $ques_val_4 : '',
							  'ques_val_5'		=> ( ! empty($ques_val_5)) ? $ques_val_5 : '',
							  'ques_val_6'		=> ( ! empty($ques_val_6)) ? $ques_val_6 : '',
							  'critical'		=> ( ! empty($mtf_criticalQ)) ? $mtf_criticalQ : '',
							  'status' 			=> 1,
							  'user_id'			=> $userId,
							  'cur_date'		=> $curdate,
							  'qid'				=> $ques_id);
			$ques_sql  = "UPDATE `question_tbl` SET `questions`=:questions, `question_type`=:qtype, `qaudio`=:qaudio, `qspeech_text`=:qspeech_text, `speech_text`=:speech_text, `audio`=:audio, `video`=:video, `screen`=:screen, `image`=:image, `document`=:document, `videoq_media_file`=:media_file,`ques_val_1`=:ques_val_1, `ques_val_2`=:ques_val_2, `ques_val_3`=:ques_val_3, `ques_val_4`=:ques_val_4, `ques_val_5`=:ques_val_5, `ques_val_6`=:ques_val_6, `critical`=:critical, `status`=:status, `uid`=:user_id, `cur_date`=:cur_date WHERE question_id=:qid";
			$ques_stmt = $db->prepare($ques_sql);
			if ($ques_stmt->execute($quesData)):
				#-----Remove-Old-Feedback-------
				$db->feedback_remove($ques_id);

				if (isset($chkCorMTFEndSim) && $chkCorMTFEndSim > 0){
					$mappedQuestionMtfCorrect = '';
				}
				
				#--------Correct-Feedback----------
				$cfeedData = array( 'qid'			=> ( ! empty($ques_id)) ? $ques_id : '',
									'nextQid'		=> ( ! empty($mappedQuestionMtfCorrect)) ? $mappedQuestionMtfCorrect : '',
									'EndSim'		=> (isset($chkCorMTFEndSim) && ! empty($chkCorMTFEndSim)) ? 1 : 0,
									'feedback'		=> ( ! empty($mtf_cfeedback)) ? htmlspecialchars($mtf_cfeedback) : '',
									'ftype'			=> 1,
									'speech_text'	=> ( ! empty($mtf_feedback_text_to_speech)) ? htmlspecialchars($mtf_feedback_text_to_speech) : '',
									'audio'			=> ( ! empty($feed_audio)) ? $feed_audio : '',
									'video'			=> ( ! empty($feed_video)) ? $feed_video : '',
									'screen'		=> ( ! empty($mtf_feedback_rec_screen)) ? $mtf_feedback_rec_screen : '',
									'image'			=> ( ! empty($mtf_feedback_img)) ? $mtf_feedback_img : '',
									'document'		=> ( ! empty($mtf_feedback_doc)) ? $mtf_feedback_doc : '');
				$cfeed_sql  = "INSERT INTO `feedback_tbl` (`question_id`, `nextQid`,`EndSim`,`feedback`, `feedback_type`, `feed_speech_text`, `feed_audio`, `feed_video`, `feed_screen`, `feed_image`, `feed_document`) VALUES (:qid, :nextQid,:EndSim,:feedback, :ftype, :speech_text, :audio, :video, :screen, :image, :document)";
				$cfeed_stmt = $db->prepare($cfeed_sql);
				$cfeed_stmt->execute($cfeedData);

				if (isset($chkInMTFEndSim) && $chkInMTFEndSim > 0){
					$mappedQuestionMtfIncorrect = '';
				}

				#--------Incorrect-Feedback-------
				$ifeedData = array( 'qid'			=> ( ! empty($ques_id)) ? $ques_id : '',
									'nextQid'		=> ( ! empty($mappedQuestionMtfIncorrect)) ? $mappedQuestionMtfIncorrect : '',
									'EndSim'		=> (isset($chkInMTFEndSim) && ! empty($chkInMTFEndSim)) ? 1 : 0,
									'feedback'		=> ( ! empty($mtf_ifeedback)) ? htmlspecialchars($mtf_ifeedback) : '',
									'ftype'			=> 2,
									'speech_text'	=> ( ! empty($mtfi_feedback_text_to_speech)) ? htmlspecialchars($mtfi_feedback_text_to_speech) : '',
									'audio'			=> ( ! empty($ifeed_audio)) ? $ifeed_audio : '',
									'video'			=> ( ! empty($ifeed_video)) ? $ifeed_video : '',
									'screen'		=> ( ! empty($mtfi_feedback_rec_screen)) ? $mtfi_feedback_rec_screen : '',
									'image'			=> ( ! empty($mtfi_feedback_img)) ? $mtfi_feedback_img : '',
									'document'		=> ( ! empty($mtfi_feedback_doc)) ? $mtfi_feedback_doc : '');
				$ifeed_sql  = "INSERT INTO `feedback_tbl` (`question_id`, `nextQid`,`EndSim`,`feedback`, `feedback_type`, `feed_speech_text`, `feed_audio`, `feed_video`, `feed_screen`, `feed_image`, `feed_document`) VALUES (:qid, :nextQid,:EndSim,:feedback, :ftype, :speech_text, :audio, :video, :screen, :image, :document)";
				$ifeed_stmt = $db->prepare($ifeed_sql);
				$ifeed_stmt->execute($ifeedData);
				
				#----------Shuffle-OFF--------------
				if ( ! empty($mtf_choice[0]) && ! empty($mtf_choice_order[0]) && ! empty($mtf_match_order[0]) && ! empty($mtf_match[0]) && ! empty($ques_id)):
					$soff = 0;
					foreach ($mtf_choice as $answerData):
						if ( ! empty($updateAnswerid[$soff])):
							$ansSql = "UPDATE `answer_tbl` SET `choice_option`=:choice_option, `choice_order_no`=:choice_order_no, `match_order_no`=:match_order_no, `match_option`=:match_option, `status`=:status, `uid`=:user_id, `cur_date`=:cur_date WHERE answer_id=:answer_id";
							$stmt 	= $db->prepare($ansSql);
							$stmt->execute(array('answer_id' 		=> $updateAnswerid[$soff],
												'choice_option'		=> ( ! empty($mtf_choice[$soff])) ? htmlspecialchars($mtf_choice[$soff]) : '',
												'choice_order_no'	=> ( ! empty($mtf_choice_order[$soff])) ? $mtf_choice_order[$soff] : '',
												'match_order_no'	=> ( ! empty($mtf_match_order[$soff])) ? $mtf_match_order[$soff] : '',
												'match_option'		=> ( ! empty($mtf_match[$soff])) ? htmlspecialchars($mtf_match[$soff]) : '',
												'status' 			=> 1,
												'user_id' 			=> $userId,
												'cur_date'			=> $curdate));
						else:
							$ansSql = "INSERT INTO `answer_tbl` (`question_id`, `choice_option`, `choice_order_no`, `match_order_no`, `match_option`, `status`, `uid`, `cur_date`) 
									   VALUES (:qid, :choice_option, :choice_order_no, :match_order_no, :match_option, :status, :user_id, :cur_date)";
							$stmt = $db->prepare($ansSql);
							$stmt->execute(array('qid'				=> $ques_id,
												 'choice_option'	=> ( ! empty($mtf_choice[$soff])) ? htmlspecialchars($mtf_choice[$soff]) : '',
												 'choice_order_no'	=> ( ! empty($mtf_choice_order[$soff])) ? $mtf_choice_order[$soff] : '',
												 'match_order_no'	=> ( ! empty($mtf_match_order[$soff])) ? $mtf_match_order[$soff] : '',
												 'match_option'		=> ( ! empty($mtf_match[$soff])) ? htmlspecialchars($mtf_match[$soff]) : '',
												 'status' 			=> 1,
												 'user_id' 			=> $userId,
												 'cur_date'			=> $curdate));
						endif;
					$soff++;
					endforeach;
				endif;
				
				#----------Shuffle-ON--------------
				if ( ! empty($mtf_choice2[0]) && ! empty($mtf_match2[0]) && empty($mtf_match_order[0]) && empty($mtf_match[0])):
					$sON = 0;
					foreach ($mtf_choice2 as $answerData):
						if ( ! empty($updateAnswerid[$sON])):
							$ansSql = "UPDATE `answer_tbl` SET `choice_option`=:choice_option, `match_option`=:match_option, `status`=:status, `uid`=:user_id, `cur_date`=:cur_date WHERE answer_id=:answer_id";
							$stmt 	= $db->prepare($ansSql);
							$stmt->execute(array('answer_id' 		=> $updateAnswerid[$sON],
												'choice_option'	=> ( ! empty($mtf_choice2[$sON])) ? htmlspecialchars($mtf_choice2[$sON]) : '',
												'match_option'		=> ( ! empty($mtf_match2[$sON])) ? htmlspecialchars($mtf_match2[$sON]) : '',
												'status' 			=> 1,
												'user_id' 			=> $userId,
												'cur_date'			=> $curdate));
						else:
							$ansSql = "INSERT INTO `answer_tbl` (`question_id`, `choice_option`, `match_option`, `status`, `uid`, `cur_date`) 
									VALUES (:qid, :choice_option, :match_option, :status, :user_id, :cur_date)";
							$stmt = $db->prepare($ansSql);
							$stmt->execute(array('qid'				=> $ques_id,
												'choice_option'	=> ( ! empty($mtf_choice2[$sON])) ? htmlspecialchars($mtf_choice2[$sON]) : '',
												'match_option'		=> ( ! empty($mtf_match2[$sON])) ? htmlspecialchars($mtf_match2[$sON]) : '',
												'status' 			=> 1,
												'user_id' 			=> $userId,
												'cur_date'			=> $curdate));
						endif;
						$sON++;
					endforeach;
				endif;
			endif;
		//------END-MATCH-THE-FOLLOWING------
		
		//-----------START-SEQUENCE--------
		elseif ($question_type == 2):
			#-------QUESTIONS-DATA---
			$qaudio = '';
			if ( ! empty($sqq_qaudio)):
				$qaudio = $sqq_qaudio;
			elseif ( ! empty($sqq_rec_audio)):
				$qaudio = $sqq_rec_audio;
			endif;
			
			$audio = '';
			if ( ! empty($sq_qaudio)):
				$audio = $sq_qaudio;
			elseif ( ! empty($sq_rec_audio)):
				$audio = $sq_rec_audio;
			endif;
			
			$video = '';
			if ( ! empty($sq_video)):
				$video = $sq_video;
			elseif ( ! empty($sq_rec_video)):
				$video = $sq_rec_video;
			endif;
			
			#------Correct-FEEDBACK-DATA---
			$feed_audio = '';
			if ( ! empty($sq_feedback_audio)):
				$feed_audio = $sq_feedback_audio;
			elseif ( ! empty($sq_feedback_rec_audio)):
				$feed_audio = $sq_feedback_rec_audio;
			endif;
			
			$feed_video = '';
			if ( ! empty($sq_feedback_video)):
				$feed_video = $sq_feedback_video;
			elseif ( ! empty($sq_feedback_rec_video)):
				$feed_video = $sq_feedback_rec_video;
			endif;
			
			#------Incorrect-FEEDBACK-DATA---
			$ifeed_audio = '';
			if ( ! empty($sqi_feedback_audio)):
				$ifeed_audio = $sqi_feedback_audio;
			elseif ( ! empty($sqi_feedback_rec_audio)):
				$ifeed_audio = $sqi_feedback_rec_audio;
			endif;
			
			$ifeed_video = '';
			if ( ! empty($sqi_feedback_video)):
				$ifeed_video = $sqi_feedback_video;
			elseif ( ! empty($sqi_feedback_rec_video)):
				$ifeed_video = $sqi_feedback_rec_video;
			endif;			
			$quesData =   array('questions'			=> ( ! empty($sq)) ? htmlspecialchars($sq) : '',
								'videoq_media_file'	=> ( ! empty($videoq_media_file)) ? addslashes($videoq_media_file) : '',
								'qtype'				=> ( ! empty($question_type)) ? $question_type : '',
								'qaudio'			=> ( ! empty($qaudio)) ? $qaudio : '',
								'qspeech_text'		=> ( ! empty($sqq_text_to_speech)) ? htmlspecialchars($sqq_text_to_speech) : '',
								'speech_text'		=> ( ! empty($sq_text_to_speech)) ? htmlspecialchars($sq_text_to_speech) : '',
								'audio'				=> ( ! empty($audio)) ? $audio : '',
								'video'				=> ( ! empty($video)) ? $video : '',
								'screen'			=> ( ! empty($sq_rec_screen)) ? $sq_rec_screen : '',
								'image'				=> ( ! empty($sq_img)) ? $sq_img : '',
								'document'			=> ( ! empty($sq_doc)) ? $sq_doc : '',
								'shuffle'			=> (isset($sq_shuffle) && ! empty($sq_shuffle)) ? 1 : 0,
								'ques_val_1'		=> ( ! empty($sq_ques_val_1)) ? $sq_ques_val_1 : '',
								'ques_val_2'		=> ( ! empty($sq_ques_val_2)) ? $sq_ques_val_2 : '',
								'ques_val_3'		=> ( ! empty($sq_ques_val_3)) ? $sq_ques_val_3 : '',
								'ques_val_4'		=> ( ! empty($sq_ques_val_4)) ? $sq_ques_val_4 : '',
								'ques_val_5'		=> ( ! empty($sq_ques_val_5)) ? $sq_ques_val_5 : '',
								'ques_val_6'		=> ( ! empty($sq_ques_val_6)) ? $sq_ques_val_6 : '',
								'critical'			=> ( ! empty($sq_criticalQ)) ? $sq_criticalQ : '',
								'status' 			=> 1,
								'user_id'			=> $userId,
								'cur_date'			=> $curdate,
								'qid'				=> $ques_id);
			$ques_sql  = "UPDATE `question_tbl` SET `questions`=:questions, `videoq_media_file`=:videoq_media_file,`question_type`=:qtype, `qaudio`=:qaudio, `qspeech_text`=:qspeech_text, `speech_text`=:speech_text, `audio`=:audio, `video`=:video, `screen`=:screen, `image`=:image, `document`=:document, `shuffle`=:shuffle, `ques_val_1`=:ques_val_1, `ques_val_2`=:ques_val_2, `ques_val_3`=:ques_val_3, `ques_val_4`=:ques_val_4, `ques_val_5`=:ques_val_5, `ques_val_6`=:ques_val_6, `critical`=:critical, `status`=:status, `uid`=:user_id, `cur_date`=:cur_date WHERE question_id=:qid";
			$ques_stmt = $db->prepare($ques_sql);
					
			if ($ques_stmt->execute($quesData)):
				#-----Remove-Old-Feedback-------
				$db->feedback_remove($ques_id);
				
				if (isset($chkCorSWQEndSim) && $chkCorSWQEndSim > 0){
					$mappedQuestionSEQCorrect = '';
				}
				#--------Correct-Feedback----------
				$cfeedData = array( 'qid'			=> ( ! empty($ques_id)) ? $ques_id : '',
									'nextQid'		=> ( ! empty($mappedQuestionSEQCorrect)) ? $mappedQuestionSEQCorrect : '',
									'EndSim'		=> (isset($chkCorSWQEndSim) && ! empty($chkCorSWQEndSim)) ? 1 : 0,
									'feedback'		=> ( ! empty($sq_feedback)) ? htmlspecialchars($sq_feedback) : '',
									'ftype'			=> 1,
									'speech_text'	=> ( ! empty($sq_feedback_text_to_speech)) ? htmlspecialchars($sq_feedback_text_to_speech) : '',
									'audio'			=> ( ! empty($feed_audio)) ? $feed_audio : '',
									'video'			=> ( ! empty($feed_video)) ? $feed_video : '',
									'screen'		=> ( ! empty($sq_feedback_rec_screen)) ? $sq_feedback_rec_screen : '',
									'image'			=> ( ! empty($sq_feedback_img)) ? $sq_feedback_img : '',
									'document'		=> ( ! empty($sq_feedback_doc)) ? $sq_feedback_doc : '');
				$cfeed_sql  = "INSERT INTO `feedback_tbl` (`question_id`, `nextQid`,`EndSim`,`feedback`, `feedback_type`, `feed_speech_text`, `feed_audio`, `feed_video`, `feed_screen`, `feed_image`, `feed_document`) VALUES (:qid, :nextQid,:EndSim,:feedback, :ftype, :speech_text, :audio, :video, :screen, :image, :document)";
				$cfeed_stmt = $db->prepare($cfeed_sql);
				$cfeed_stmt->execute($cfeedData);

				if (isset($chkInSWQEndSim) && $chkInSWQEndSim > 0) {
					$mappedQuestionSEQIncorrect = '';
				}

				#--------Incorrect-Feedback-------
				$ifeedData = array('qid'			=> ( ! empty($ques_id)) ? $ques_id : '',
								   'nextQid'		=> ( ! empty($mappedQuestionSEQIncorrect)) ? $mappedQuestionSEQIncorrect : '',
								   'EndSim'			=> (isset($chkInSWQEndSim) && ! empty($chkInSWQEndSim)) ? 1 : 0,
								   'feedback'		=> ( ! empty($sq_ifeedback)) ? htmlspecialchars($sq_ifeedback) : '',
								   'ftype'			=> 2,
								   'speech_text'	=> ( ! empty($sqi_feedback_text_to_speech)) ? htmlspecialchars($sqi_feedback_text_to_speech) : '',
								   'audio'			=> ( ! empty($ifeed_audio)) ? $ifeed_audio : '',
								   'video'			=> ( ! empty($ifeed_video)) ? $ifeed_video : '',
								   'screen'			=> ( ! empty($sqi_feedback_rec_screen)) ? $sqi_feedback_rec_screen : '',
								   'image'			=> ( ! empty($sqi_feedback_img)) ? $sqi_feedback_img : '',
								   'document'		=> ( ! empty($sqi_feedback_doc)) ? $sqi_feedback_doc : '');
				$ifeed_sql  = "INSERT INTO `feedback_tbl` (`question_id`, `nextQid`,`EndSim`,`feedback`, `feedback_type`, `feed_speech_text`, `feed_audio`, `feed_video`, `feed_screen`, `feed_image`, `feed_document`) VALUES (:qid, :nextQid,:EndSim,:feedback, :ftype, :speech_text, :audio, :video, :screen, :image, :document)";
				$ifeed_stmt = $db->prepare($ifeed_sql);
				$ifeed_stmt->execute($ifeedData);
				/* ADD-OPTIONS */
				if ( ! empty($sq_ans[0])):
					$sqi = 0; $sqi_order = 1;
					foreach ($sq_ans as $answerData):
						if ( ! empty($updateSQAnswerid[$sqi])):
							$ansSql = "UPDATE `answer_tbl` SET `choice_option`=:choice_option, `match_order_no`=:order_no, `status`=:status, `uid`=:user_id, `cur_date`=:cur_date WHERE answer_id=:answer_id";
							$stmt	= $db->prepare($ansSql);
							$stmt->execute(array('answer_id' 		=> $updateSQAnswerid[$sqi],
												'choice_option'		=> ( ! empty($sq_ans[$sqi])) ? htmlspecialchars($sq_ans[$sqi]) : '',
												'order_no'			=> $sqi_order,
												'status' 			=> 1,
												'user_id' 			=> $userId,
												'cur_date'			=> $curdate));
						else:
							$ansSql = "INSERT INTO `answer_tbl` (`question_id`, `choice_option`, `match_order_no`, `status`, `uid`, `cur_date`) 
									VALUES (:qid, :choice_option, :order_no, :status, :user_id, :cur_date)";
							$stmt = $db->prepare($ansSql);
							$stmt->execute(array('qid'				=> $ques_id,
												'choice_option'		=> ( ! empty($sq_ans[$sqi])) ? htmlspecialchars($sq_ans[$sqi]) : '',
												'order_no'			=> $sqi_order,
												'status' 			=> 1,
												'user_id' 			=> $userId,
												'cur_date'			=> $curdate));
						endif;
						$sqi++; $sqi_order++;
					endforeach;
				endif;
			endif;
		//-----------END-SEQUENCE----------
	
		//-----------START-SORTING----------
		elseif ($question_type == 3):
			#-------QUESTIONS-DATA---
			$qaudio = '';
			if ( ! empty($sortqq_qaudio)):
				$qaudio = $sortqq_qaudio;
			elseif ( ! empty($sortqq_rec_audio)):
				$qaudio = $sortqq_rec_audio;
			endif;
			
			$audio = '';
			if ( ! empty($sortq_qaudio)):
				$audio = $sortq_qaudio;
			elseif ( ! empty($sortq_rec_audio)):
				$audio = $sortq_rec_audio;
			endif;
			
			$video = '';
			if ( ! empty($sortq_video)):
				$video = $sortq_video;
			elseif ( ! empty($sortq_rec_video)):
				$video = $sortq_rec_video;
			endif;
			
			#------Correct-FEEDBACK-DATA---
			$feed_audio = '';
			if ( ! empty($sortq_feedback_audio)):
				$feed_audio = $sortq_feedback_audio;
			elseif ( ! empty($sortq_feedback_rec_audio)):
				$feed_audio = $sortq_feedback_rec_audio;
			endif;
			
			$feed_video = '';
			if ( ! empty($sortq_feedback_video)):
				$feed_video = $sortq_feedback_video;
			elseif ( ! empty($sortq_feedback_rec_video)):
				$feed_video = $sortq_feedback_rec_video;
			endif;
			
			#------Incorrect-FEEDBACK-DATA---
			$ifeed_audio = '';
			if ( ! empty($sortqi_feedback_audio)):
				$ifeed_audio = $sortqi_feedback_audio;
			elseif ( ! empty($sortqi_feedback_rec_audio)):
				$ifeed_audio = $sortqi_feedback_rec_audio;
			endif;
			
			$ifeed_video = '';
			if ( ! empty($sortqi_feedback_video)):
				$ifeed_video = $sortqi_feedback_video;
			elseif ( ! empty($sortqi_feedback_rec_video)):
				$ifeed_video = $sortqi_feedback_rec_video;
			endif;			
			$quesData = array('questions'			=> ( ! empty($sortq)) ? htmlspecialchars($sortq) : '',
							  'videoq_media_file'	=> ( ! empty($videoq_media_file)) ? addslashes($videoq_media_file) : '',
							  'qtype'				=> ( ! empty($question_type)) ? $question_type : '',
							  'qaudio'				=> ( ! empty($qaudio)) ? $qaudio : '',
							  'qspeech_text'		=> ( ! empty($sortqq_text_to_speech)) ? htmlspecialchars($sortqq_text_to_speech) : '',
							  'speech_text'			=> ( ! empty($sortq_text_to_speech)) ? htmlspecialchars($sortq_text_to_speech) : '',
							  'audio'				=> ( ! empty($audio)) ? $audio : '',
							  'video'				=> ( ! empty($video)) ? $video : '',
							  'screen'				=> ( ! empty($sortq_rec_screen)) ? $sortq_rec_screen : '',
							  'image'				=> ( ! empty($sortq_img)) ? $sortq_img : '',
							  'document'			=> ( ! empty($sortq_doc)) ? $sortq_doc : '',
							  'shuffle'				=> (isset($sort_shuffle) && ! empty($sort_shuffle)) ? 1 : 0,
							  'ques_val_1'			=> ( ! empty($sort_ques_val_1)) ? $sort_ques_val_1 : '',
							  'ques_val_2'			=> ( ! empty($sort_ques_val_2)) ? $sort_ques_val_2 : '',
							  'ques_val_3'			=> ( ! empty($sort_ques_val_3)) ? $sort_ques_val_3 : '',
							  'ques_val_4'			=> ( ! empty($sort_ques_val_4)) ? $sort_ques_val_4 : '',
							  'ques_val_5'			=> ( ! empty($sort_ques_val_5)) ? $sort_ques_val_5 : '',
							  'ques_val_6'			=> ( ! empty($sort_ques_val_6)) ? $sort_ques_val_6 : '',
							  'critical'			=> ( ! empty($sort_criticalQ)) ? $sort_criticalQ : '',
							  'status' 				=> 1,
							  'user_id'				=> $userId,
							  'cur_date'			=> $curdate,
							  'qid'					=> $ques_id);
			$ques_sql  = "UPDATE `question_tbl` SET `questions`=:questions, `videoq_media_file`=:videoq_media_file,`question_type`=:qtype, `qaudio`=:qaudio, `qspeech_text`=:qspeech_text, `speech_text`=:speech_text, `audio`=:audio, `video`=:video, `screen`=:screen, `image`=:image, `document`=:document, `shuffle`=:shuffle, `ques_val_1`=:ques_val_1, `ques_val_2`=:ques_val_2, `ques_val_3`=:ques_val_3, `ques_val_4`=:ques_val_4, `ques_val_5`=:ques_val_5, `ques_val_6`=:ques_val_6, `critical`=:critical, `status`=:status, `uid`=:user_id, `cur_date`=:cur_date WHERE question_id=:qid";
			$ques_stmt = $db->prepare($ques_sql);
			if ($ques_stmt->execute($quesData)):
				#-----Remove-Old-Feedback-----
				$db->feedback_remove($ques_id);

				if (isset($chkCorSORTEndSim) && $chkCorSORTEndSim > 0){
					$mappedQuestionSORTCorrect = '';
				}

				#--------Correct-Feedback----------
				$cfeedData = array('qid'			=> ( ! empty($ques_id)) ? $ques_id : '',
								   'nextQid'		=> ( ! empty($mappedQuestionSORTCorrect)) ? $mappedQuestionSORTCorrect : '',
								   'EndSim'			=> (isset($chkCorSORTEndSim) && ! empty($chkCorSORTEndSim)) ? 1 : 0,
								   'feedback'		=> ( ! empty($sortq_cfeedback)) ? htmlspecialchars($sortq_cfeedback) : '',
								   'ftype'			=> 1,
								   'speech_text'	=> ( ! empty($sortq_feedback_text_to_speech)) ? htmlspecialchars($sortq_feedback_text_to_speech) : '',
								   'audio'			=> ( ! empty($feed_audio)) ? $feed_audio : '',
								   'video'			=> ( ! empty($feed_video)) ? $feed_video : '',
								   'screen'			=> ( ! empty($sortq_feedback_rec_screen)) ? $sortq_feedback_rec_screen : '',
								   'image'			=> ( ! empty($sortq_feedback_img)) ? $sortq_feedback_img : '',
								   'document'		=> ( ! empty($sortq_feedback_doc)) ? $sortq_feedback_doc : '');
				$cfeed_sql  = "INSERT INTO `feedback_tbl` (`question_id`, `nextQid`,`EndSim`,`feedback`, `feedback_type`, `feed_speech_text`, `feed_audio`, `feed_video`, `feed_screen`, `feed_image`, `feed_document`) VALUES (:qid, :nextQid,:EndSim,:feedback, :ftype, :speech_text, :audio, :video, :screen, :image, :document)";
				$cfeed_stmt = $db->prepare($cfeed_sql);
				$cfeed_stmt->execute($cfeedData);

				if (isset($chkInSORTEndSim) && $chkInSORTEndSim > 0){
					$mappedQuestionSORTIncorrect = '';
				}

				#--------Incorrect-Feedback-------
				$ifeedData = array('qid'			=> ( ! empty($ques_id)) ? $ques_id : '',
								   'nextQid'		=> ( ! empty($mappedQuestionSORTIncorrect)) ? $mappedQuestionSORTIncorrect : '',
								   'EndSim'			=> (isset($chkInSORTEndSim) && ! empty($chkInSORTEndSim)) ? 1 : 0,
								   'feedback'		=> ( ! empty($sortq_ifeedback)) ? htmlspecialchars($sortq_ifeedback) : '',
								   'ftype'			=> 2,
								   'speech_text'	=> ( ! empty($sortqi_feedback_text_to_speech)) ? htmlspecialchars($sortqi_feedback_text_to_speech) : '',
								   'audio'			=> ( ! empty($ifeed_audio)) ? $ifeed_audio : '',
								   'video'			=> ( ! empty($ifeed_video)) ? $ifeed_video : '',
								   'screen'			=> ( ! empty($sortqi_feedback_rec_screen)) ? $sortqi_feedback_rec_screen : '',
								   'image'			=> ( ! empty($sortqi_feedback_img)) ? $sortqi_feedback_img : '',
								   'document'		=> ( ! empty($sortqi_feedback_doc)) ? $sortqi_feedback_doc : '');
				$ifeed_sql  = "INSERT INTO `feedback_tbl` (`question_id`, `nextQid`,`EndSim`,`feedback`, `feedback_type`, `feed_speech_text`, `feed_audio`, `feed_video`, `feed_screen`, `feed_image`, `feed_document`) VALUES (:qid, :nextQid,:EndSim,:feedback, :ftype, :speech_text, :audio, :video, :screen, :image, :document)";
				$ifeed_stmt = $db->prepare($ifeed_sql);
				$ifeed_stmt->execute($ifeedData);				
				#----------ADD-OPTIONS----------
				$sortqi = 0;
				foreach ($sortq_sorting_items as $optionData):
					if ( ! empty($updateSortAnswerid[$sortqi])):
						$ansSql = "UPDATE `answer_tbl` SET `choice_option`=:choice_option, `image`=:image, `status`=:status, `uid`=:user_id, `cur_date`=:cur_date WHERE answer_id=:answer_id";
						$stmt	= $db->prepare($ansSql);
						$stmt->execute(array('answer_id'		=> $updateSortAnswerid[$sortqi],
											'choice_option'	=> (isset($sortq_sorting_items[$sortqi]) && ! empty($sortq_sorting_items[$sortqi])) ? htmlspecialchars($sortq_sorting_items[$sortqi]) : '',
											'image'			=> (isset($sortq_item_img[$sortqi]) && ! empty($sortq_item_img[$sortqi])) ? $sortq_item_img[$sortqi] : '',
											'status' 			=> 1,
											'user_id' 			=> $userId,
											'cur_date'			=> $curdate));
						if ( ! empty($updateSubOptionid[$sortqi])):
							$sortqop = 0;
							foreach ($sortq_drag_items[$sortqi]['option'] as $subOptionData):
								if ( ! empty($updateSubOptionid[$sortqi][$sortqop])):
									$ansOpSql = "UPDATE `sub_options_tbl` SET `sub_option`=:sub_option WHERE sub_options_id=:sub_options_id";
									$opstmt	  = $db->prepare($ansOpSql);
									$opstmt->execute(array('sub_options_id'	=> $updateSubOptionid[$sortqi][$sortqop],
														   'sub_option'	 	=> ( ! empty($subOptionData)) ? htmlspecialchars($subOptionData) : ''));
								else:
									$opSql	= "INSERT INTO `sub_options_tbl` (`answer_id`, `sub_option`) VALUES (:aid, :option)";
									$opstmt = $db->prepare($opSql);
									$opstmt->execute(array('aid'	=> $updateSortAnswerid[$sortqi],
														   'option'	=> ( ! empty($subOptionData)) ? htmlspecialchars($subOptionData) : ''));
								endif;
								$sortqop++;
							endforeach;
						endif;
					else:
						$ansSql = "INSERT INTO `answer_tbl` (`question_id`, `choice_option`, `image`, `status`, `uid`, `cur_date`) VALUES (:qid, :option, :image, :status, :user_id, :cur_date)";
						$stmt	= $db->prepare($ansSql);
						$stmt->execute(array('qid'		=> $ques_id,
											'option'	=> ( ! empty($sortq_sorting_items[$sortqi])) ? htmlspecialchars($sortq_sorting_items[$sortqi]) : '',
											'image'		=> (isset($sortq_item_img[$sortqi]) && ! empty($sortq_item_img[$sortqi])) ? $sortq_item_img[$sortqi] : '',
											'status' 	=> 1,
											'user_id' 	=> $userId,
											'cur_date'	=> $curdate));
						$ans_id = $db->lastInsertId();
						if ( ! empty($sortq_drag_items)):
							foreach ($sortq_drag_items[$sortqi]['option'] as $subOptionData):
								$opSql  = "INSERT INTO `sub_options_tbl` (`answer_id`, `sub_option`) VALUES (:aid, :option)";
								$opstmt = $db->prepare($opSql);
								$opstmt->execute(array('aid' => $ans_id,'option' => ( ! empty($subOptionData)) ? htmlspecialchars($subOptionData) : ''));
							endforeach;
						endif;
					endif;
					$sortqi++;
				endforeach;
			endif;
		//-----------END-SORTING----------
	
		//-----------START-MCQ----------
		elseif ($question_type == 4):
			#-------QUESTIONS-DATA---
			$qaudio = '';
			if ( ! empty($mcqq_qaudio)):
				$qaudio = $mcqq_qaudio;
			elseif ( ! empty($mcqq_rec_audio)):
				$qaudio = $mcqq_rec_audio;
			endif;
			
			$audio = '';
			if ( ! empty($mcq_audio)):
				$audio = $mcq_audio;
			elseif ( ! empty($mcq_rec_audio)):
				$audio = $mcq_rec_audio;
			endif;
			
			$video = '';
			if ( ! empty($mcq_video)):
				$video = $mcq_video;
			elseif ( ! empty($mcq_rec_video)):
				$video = $mcq_rec_video;
			endif;
			
			#------Correct-FEEDBACK-DATA---
			$feed_audio = '';
			if ( ! empty($mcq_feedback_audio)):
				$feed_audio = $mcq_feedback_audio;
			elseif ( ! empty($mcq_feedback_rec_audio)):
				$feed_audio = $mcq_feedback_rec_audio;
			endif;
			
			$feed_video = '';
			if ( ! empty($mcq_feedback_video)):
				$feed_video = $mcq_feedback_video;
			elseif ( ! empty($mcq_feedback_rec_video)):
				$feed_video = $mcq_feedback_rec_video;
			endif;
			
			#------Incorrect-FEEDBACK-DATA---
			$ifeed_audio = '';
			if ( ! empty($mcqi_feedback_audio)):
				$ifeed_audio = $mcqi_feedback_audio;
			elseif ( ! empty($mcqi_feedback_rec_audio)):
				$ifeed_audio = $mcqi_feedback_rec_audio;
			endif;
			
			$ifeed_video = '';
			if ( ! empty($mcqi_feedback_video)):
				$ifeed_video = $mcqi_feedback_video;
			elseif ( ! empty($mcqi_feedback_rec_video)):
				$ifeed_video = $mcqi_feedback_rec_video;
			endif;
			$quesData = array('questions'		=> ( ! empty($mcq)) ? htmlspecialchars($mcq) : '',
							  'qtype'			=> ( ! empty($question_type)) ? $question_type : '',
							  'media_file'		=> ( ! empty($videoq_media_file)) ? $videoq_media_file : '',
							  'qaudio'			=> ( ! empty($qaudio)) ? $qaudio : '',
							  'qspeech_text'	=> ( ! empty($mcqq_text_to_speech)) ? htmlspecialchars($mcqq_text_to_speech) : '',
							  'speech_text'		=> ( ! empty($mcq_text_to_speech)) ? htmlspecialchars($mcq_text_to_speech) : '',
							  'audio'			=> ( ! empty($audio)) ? $audio : '',
							  'video'			=> ( ! empty($video)) ? $video : '',
							  'screen'			=> ( ! empty($mcq_rec_screen)) ? $mcq_rec_screen : '',
							  'image'			=> ( ! empty($mcq_img)) ? $mcq_img : '',
							  'document'		=> ( ! empty($mcq_doc)) ? $mcq_doc : '',
							  'true_option'		=> ( ! empty($mcq_true_option)) ? $mcq_true_option : '',
							  'shuffle'			=> (isset($mcq_shuffle) && ! empty($mcq_shuffle)) ? 1 : 0,
							  'ques_val_1'		=> ( ! empty($mcq_ques_val_1)) ? $mcq_ques_val_1 : '',
							  'ques_val_2'		=> ( ! empty($mcq_ques_val_2)) ? $mcq_ques_val_2 : '',
							  'ques_val_3'		=> ( ! empty($mcq_ques_val_3)) ? $mcq_ques_val_3 : '',
							  'ques_val_4'		=> ( ! empty($mcq_ques_val_4)) ? $mcq_ques_val_4 : '',
							  'ques_val_5'		=> ( ! empty($mcq_ques_val_5)) ? $mcq_ques_val_5 : '',
							  'ques_val_6'		=> ( ! empty($mcq_ques_val_6)) ? $mcq_ques_val_6 : '',
							  'critical'		=> ( ! empty($mcq_criticalQ)) ? $mcq_criticalQ : '',
							  'status' 			=> 1,
							  'user_id'			=> $userId,
							  'cur_date'		=> $curdate,
							  'qid'				=> $ques_id);
			$ques_sql  = "UPDATE `question_tbl` SET `questions`=:questions, `question_type`=:qtype, `qaudio`=:qaudio, `qspeech_text`=:qspeech_text, `speech_text`=:speech_text, `audio`=:audio, `video`=:video, `screen`=:screen, `image`=:image, `document`=:document, `videoq_media_file`=:media_file,`true_options`=:true_option, `shuffle`=:shuffle, `ques_val_1`=:ques_val_1, `ques_val_2`=:ques_val_2, `ques_val_3`=:ques_val_3, `ques_val_4`=:ques_val_4, `ques_val_5`=:ques_val_5, `ques_val_6`=:ques_val_6, `critical`=:critical, `status`=:status, `uid`=:user_id, `cur_date`=:cur_date WHERE question_id=:qid";
			$ques_stmt = $db->prepare($ques_sql);
			$ques_stmt->execute($quesData);
			
			#-----Remove-Old-Feedback-------
			$db->feedback_remove($ques_id);
			
			if (isset($chkCorMCQEndSim) && $chkCorMCQEndSim > 0){
				$mappedQuestionMCQCorrect = '';
			}

			#--------Correct-Feedback----------
			$cfeedData = array('qid'			=> ( ! empty($ques_id)) ? $ques_id : '',
							   'nextQid'		=> ( ! empty($mappedQuestionMCQCorrect)) ? $mappedQuestionMCQCorrect : '',
							   'EndSim'			=> (isset($chkCorMCQEndSim) && ! empty($chkCorMCQEndSim)) ? 1 : 0,
							   'feedback'		=> ( ! empty($mcq_cfeedback)) ? htmlspecialchars($mcq_cfeedback) : '',
							   'ftype'			=> 1,
							   'speech_text'	=> ( ! empty($mcq_feedback_text_to_speech)) ? htmlspecialchars($mcq_feedback_text_to_speech) : '',
							   'audio'			=> ( ! empty($feed_audio)) ? $feed_audio : '',
							   'video'			=> ( ! empty($feed_video)) ? $feed_video : '',
							   'screen'			=> ( ! empty($mcq_feedback_rec_screen)) ? $mcq_feedback_rec_screen : '',
							   'image'			=> ( ! empty($mcq_feedback_img)) ? $mcq_feedback_img : '',
							   'document'		=> ( ! empty($mcq_feedback_doc)) ? $mcq_feedback_doc : '');
			$cfeed_sql = "INSERT INTO `feedback_tbl` (`question_id`, `nextQid`,`EndSim`,`feedback`, `feedback_type`, `feed_speech_text`, `feed_audio`, `feed_video`, `feed_screen`, `feed_image`, `feed_document`) VALUES (:qid, :nextQid,:EndSim,:feedback, :ftype, :speech_text, :audio, :video, :screen, :image, :document)";
			$cfeed_stmt = $db->prepare($cfeed_sql);
			$cfeed_stmt->execute($cfeedData);

			if (isset($chkInMCQEndSim) && $chkInMCQEndSim > 0){
				$mappedQuestionMCQIncorrect = '';
			}

			#--------Incorrect-Feedback-------
			$ifeedData = array('qid'			=> ( ! empty($ques_id)) ? $ques_id : '',
							   'nextQid'		=> ( ! empty($mappedQuestionMCQIncorrect)) ? $mappedQuestionMCQIncorrect : '',
							   'EndSim'			=> (isset($chkInMCQEndSim) && ! empty($chkInMCQEndSim)) ? 1 : 0,
							   'feedback'		=> ( ! empty($mcq_ifeedback)) ? htmlspecialchars($mcq_ifeedback) : '',
							   'ftype'			=> 2,
							   'speech_text'	=> ( ! empty($mcqi_feedback_text_to_speech)) ? htmlspecialchars($mcqi_feedback_text_to_speech) : '',
							   'audio'			=> ( ! empty($ifeed_audio)) ? $ifeed_audio : '',
							   'video'			=> ( ! empty($ifeed_video)) ? $ifeed_video : '',
							   'screen'			=> ( ! empty($mcqi_feedback_rec_screen)) ? $mcqi_feedback_rec_screen : '',
							   'image'			=> ( ! empty($mcqi_feedback_img)) ? $mcqi_feedback_img : '',
							   'document'		=> ( ! empty($mcqi_feedback_doc)) ? $mcqi_feedback_doc : '');			
			$ifeed_sql  = "INSERT INTO `feedback_tbl` (`question_id`, `nextQid`,`EndSim`,`feedback`, `feedback_type`, `feed_speech_text`, `feed_audio`, `feed_video`, `feed_screen`, `feed_image`, `feed_document`) VALUES (:qid, :nextQid,:EndSim,:feedback, :ftype, :speech_text, :audio, :video, :screen, :image, :document)";
			$ifeed_stmt = $db->prepare($ifeed_sql);
			$ifeed_stmt->execute($ifeedData);
			#----------ADD-OPTIONS--------
			if ( ! empty($mcq_option[0])):
				$mcqi = 0; $mcqtrue = 1;
				foreach ($mcq_option as $answerData):
					if ( ! empty($updateMCQAnswerid[$mcqi])):
						$ansSql = "UPDATE `answer_tbl` SET `choice_option`=:option, `true_option`=:true, `status`=:status, `uid`=:user_id, `cur_date`=:cur_date WHERE answer_id=:aid";
						$stmt = $db->prepare($ansSql);
						$stmt->execute(array('aid'		=> $updateMCQAnswerid[$mcqi],
											 'option'	=> ( ! empty($mcq_option[$mcqi])) ? htmlspecialchars($mcq_option[$mcqi]) : '',
											 'true'		=> ( ! empty($mcq_true_option) && $mcq_true_option == $mcqtrue) ? 1 : '',
											 'status'	=> 1,
											 'user_id' 	=> $userId,
											 'cur_date'	=> $curdate));

					else:
						$ansSql = "INSERT INTO `answer_tbl` (`question_id`, `choice_option`, `true_option`, `status`, `uid`, `cur_date`) VALUES (:qid, :option, :true, :status, :user_id, :cur_date)";
						$stmt = $db->prepare($ansSql);
						$stmt->execute(array('qid'		=> $ques_id,
											 'option'	=> ( ! empty($mcq_option[$mcqi])) ? htmlspecialchars($mcq_option[$mcqi]) : '',
											 'true'		=> ( ! empty($mcq_true_option) && $mcq_true_option == $mcqtrue) ? 1 : '',
											 'status'	=> 1,
											 'user_id' 	=> $userId,
											 'cur_date'	=> $curdate));
					endif;
					$mcqi++; $mcqtrue++;
				endforeach;
			endif;
		//-----------END-MCQ----------
	
		//-----------START-MMCQ----------
		elseif ($question_type == 5):
			#-------QUESTIONS-DATA---
			$qaudio = '';
			if ( ! empty($mmcqq_qaudio)):
				$qaudio = $mmcqq_qaudio;
			elseif ( ! empty($mmcqq_rec_audio)):
				$qaudio = $mmcqq_rec_audio;
			endif;

			$audio = '';
			if ( ! empty($mmcq_audio)):
				$audio = $mmcq_audio;
			elseif ( ! empty($mmcq_rec_audio)):
				$audio = $mmcq_rec_audio;
			endif;
			
			$video = '';
			if ( ! empty($mmcq_video)):
				$video = $mmcq_video;
			elseif ( ! empty($mmcq_rec_video)):
				$video = $mmcq_rec_video;
			endif;
			
			#------Correct-FEEDBACK-DATA---
			$feed_audio = '';
			if ( ! empty($mmcq_feedback_audio)):
				$feed_audio = $mmcq_feedback_audio;
			elseif ( ! empty($mmcq_feedback_rec_audio)):
				$feed_audio = $mmcq_feedback_rec_audio;
			endif;
			
			$feed_video = '';
			if ( ! empty($mmcq_feedback_video)):
				$feed_video = $mmcq_feedback_video;
			elseif ( ! empty($mmcq_feedback_rec_video)):
				$feed_video = $mmcq_feedback_rec_video;
			endif;
			
			#------Incorrect-FEEDBACK-DATA---
			$ifeed_audio = '';
			if ( ! empty($mmcqi_feedback_audio)):
				$ifeed_audio = $mmcqi_feedback_audio;
			elseif ( ! empty($mmcqi_feedback_rec_audio)):
				$ifeed_audio = $mmcqi_feedback_rec_audio;
			endif;
			
			$ifeed_video = '';
			if ( ! empty($mmcqi_feedback_video)):
				$ifeed_video = $mmcqi_feedback_video;
			elseif ( ! empty($mmcqi_feedback_rec_video)):
				$ifeed_video = $mmcqi_feedback_rec_video;
			endif;
			
			$quesData = array('questions'		=> ( ! empty($mmcq)) ? htmlspecialchars($mmcq) : '',
							  'qtype'			=> ( ! empty($question_type)) ? $question_type : '',
							  'media_file'		=> ( ! empty($videoq_media_file)) ? $videoq_media_file : '',
					  		  'qaudio'			=> ( ! empty($qaudio)) ? $qaudio : '',
					  		  'qspeech_text'	=> ( ! empty($mmcqq_text_to_speech)) ? htmlspecialchars($mmcqq_text_to_speech) : '',
							  'speech_text'		=> ( ! empty($mmcq_text_to_speech)) ? htmlspecialchars($mmcq_text_to_speech) : '',
							  'audio'			=> ( ! empty($audio)) ? $audio : '',
							  'video'			=> ( ! empty($video)) ? $video : '',
							  'screen'			=> ( ! empty($mmcq_rec_screen)) ? $mmcq_rec_screen : '',
							  'image'			=> ( ! empty($mmcq_img)) ? $mmcq_img : '',
							  'document'		=> ( ! empty($mmcq_doc)) ? $mmcq_doc : '',
							  'true_option'		=> ( ! empty($mmcq_true_option)) ? $db->addMultiIds($mmcq_true_option) : '',
							  'shuffle'			=> (isset($mmcq_shuffle) && ! empty($mmcq_shuffle)) ? 1 : 0,
							  'ques_val_1'		=> ( ! empty($mmcq_ques_val_1)) ? $mmcq_ques_val_1 : '',
							  'ques_val_2'		=> ( ! empty($mmcq_ques_val_2)) ? $mmcq_ques_val_2 : '',
							  'ques_val_3'		=> ( ! empty($mmcq_ques_val_3)) ? $mmcq_ques_val_3 : '',
							  'ques_val_4'		=> ( ! empty($mmcq_ques_val_4)) ? $mmcq_ques_val_4 : '',
							  'ques_val_5'		=> ( ! empty($mmcq_ques_val_5)) ? $mmcq_ques_val_5 : '',
							  'ques_val_6'		=> ( ! empty($mmcq_ques_val_6)) ? $mmcq_ques_val_6 : '',
							  'critical'		=> ( ! empty($mmcq_criticalQ)) ? $mmcq_criticalQ : '',
							  'status' 			=> 1,
							  'user_id'			=> $userId,
							  'cur_date'		=> $curdate,
							  'qid'				=> $ques_id);
			$ques_sql  = "UPDATE `question_tbl` SET `questions`=:questions, `question_type`=:qtype, `qaudio`=:qaudio, `qspeech_text`=:qspeech_text, `speech_text`=:speech_text, `audio`=:audio, `video`=:video, `screen`=:screen, `image`=:image, `document`=:document,`videoq_media_file`=:media_file, `true_options`=:true_option, `shuffle`=:shuffle, `ques_val_1`=:ques_val_1, `ques_val_2`=:ques_val_2, `ques_val_3`=:ques_val_3, `ques_val_4`=:ques_val_4, `ques_val_5`=:ques_val_5, `ques_val_6`=:ques_val_6, `critical`=:critical, `status`=:status, `uid`=:user_id, `cur_date`=:cur_date WHERE question_id=:qid";
			$ques_stmt = $db->prepare($ques_sql);
			$ques_stmt->execute($quesData);
			
			#-----Remove-Old-Feedback-------
			$db->feedback_remove($ques_id);

			if (isset($chkCorMMCQEndSim) && $chkCorMMCQEndSim > 0){
				$mappedQuestionMMCQCorrect = '';
			}
			
			#--------Correct-Feedback----------
			$cfeedData = array('qid'			=> ( ! empty($ques_id)) ? $ques_id : '',
							   'nextQid'		=> ( ! empty($mappedQuestionMMCQCorrect)) ? $mappedQuestionMMCQCorrect : '',
							   'EndSim'			=> (isset($chkCorMMCQEndSim) && ! empty($chkCorMMCQEndSim)) ? 1 : 0,
							   'feedback'		=> ( ! empty($mmcq_cfeedback)) ? htmlspecialchars($mmcq_cfeedback) : '',
							   'ftype'			=> 1,
							   'speech_text'	=> ( ! empty($mmcq_feedback_text_to_speech)) ? htmlspecialchars($mmcq_feedback_text_to_speech) : '',
							   'audio'			=> ( ! empty($feed_audio)) ? $feed_audio : '',
							   'video'			=> ( ! empty($feed_video)) ? $feed_video : '',
							   'screen'			=> ( ! empty($mmcq_feedback_rec_screen)) ? $mmcq_feedback_rec_screen : '',
							   'image'			=> ( ! empty($mmcq_feedback_img)) ? $mmcq_feedback_img : '',
							   'document'		=> ( ! empty($mmcq_feedback_doc)) ? $mmcq_feedback_doc : '');			
			$cfeed_sql = "INSERT INTO `feedback_tbl` (`question_id`, `nextQid`,`EndSim`,`feedback`, `feedback_type`, `feed_speech_text`, `feed_audio`, `feed_video`, `feed_screen`, `feed_image`, `feed_document`) VALUES (:qid, :nextQid,:EndSim,:feedback, :ftype, :speech_text, :audio, :video, :screen, :image, :document)";
			$cfeed_stmt = $db->prepare($cfeed_sql);
			$cfeed_stmt->execute($cfeedData);

			if (isset($chkInMMCQEndSim) && $chkInMMCQEndSim > 0){
				$mappedQuestionMMCQIncorrect = '';
			}

			#--------Incorrect-Feedback-------
			$ifeedData = array('qid'			=> ( ! empty($ques_id)) ? $ques_id : '',
							   'nextQid'		=> ( ! empty($mappedQuestionMMCQIncorrect)) ? $mappedQuestionMMCQIncorrect : '',
							   'EndSim'			=> (isset($chkInMMCQEndSim) && ! empty($chkInMMCQEndSim)) ? 1 : 0,
							   'feedback'		=> ( ! empty($mmcq_ifeedback)) ? htmlspecialchars($mmcq_ifeedback) : '',
							   'ftype'			=> 2,
							   'speech_text'	=> ( ! empty($mmcqi_feedback_text_to_speech)) ? htmlspecialchars($mmcqi_feedback_text_to_speech) : '',
							   'audio'			=> ( ! empty($ifeed_audio)) ? $ifeed_audio : '',
							   'video'			=> ( ! empty($ifeed_video)) ? $ifeed_video : '',
							   'screen'			=> ( ! empty($mmcqi_feedback_rec_screen)) ? $mmcqi_feedback_rec_screen : '',
							   'image'			=> ( ! empty($mmcqi_feedback_img)) ? $mmcqi_feedback_img : '',
							   'document'		=> ( ! empty($mmcqi_feedback_doc)) ? $mmcqi_feedback_doc : '');
			$ifeed_sql = "INSERT INTO `feedback_tbl` (`question_id`, `nextQid`,`EndSim`,`feedback`, `feedback_type`, `feed_speech_text`, `feed_audio`, `feed_video`, `feed_screen`, `feed_image`, `feed_document`) VALUES (:qid, :nextQid,:EndSim,:feedback, :ftype, :speech_text, :audio, :video, :screen, :image, :document)";
			$ifeed_stmt = $db->prepare($ifeed_sql);
			$ifeed_stmt->execute($ifeedData);
			
			#----------ADD-OPTIONS-------------
			if ( ! empty($mmcq_option[0])):
				$mmcqi = 0; $mmcqtrue = 1;
				foreach ($mmcq_option as $answerData):
					if ( ! empty($updateMMCQAnswerid[$mmcqi])):
						$ansSql = "UPDATE `answer_tbl` SET `choice_option`=:option, `true_option`=:true, `status`=:status, `uid`=:user_id, `cur_date`=:cur_date WHERE answer_id=:aid";
						$stmt = $db->prepare($ansSql);
						$stmt->execute(array('aid'		=> $updateMMCQAnswerid[$mmcqi],
											 'option'	=> ( ! empty($mmcq_option[$mmcqi])) ? htmlspecialchars($mmcq_option[$mmcqi]) : '',
											 'true'		=> ( ! empty($mmcq_true_option[$mmcqi]) && $mmcq_true_option[$mmcqi] == $mmcqtrue) ? 1 : '',
											 'status'	=> 1,
											 'user_id' 	=> $userId,
											 'cur_date'	=> $curdate));
					else:
						$ansSql = "INSERT INTO `answer_tbl` (`question_id`, `choice_option`, `true_option`, `status`, `uid`, `cur_date`) values (:qid, :option, :true, :status, :user_id, :cur_date)";
						$stmt = $db->prepare($ansSql);
						$stmt->execute(array('qid'		=> $ques_id,
											 'option'	=> ( ! empty($mmcq_option[$mmcqi])) ? htmlspecialchars($mmcq_option[$mmcqi]) : '',
											 'true'		=> ( ! empty($mmcq_true_option[$mmcqi]) && $mmcq_true_option[$mmcqi] == $mmcqtrue) ? 1 : '',
											 'status'	=> 1,
											 'user_id' 	=> $userId,
											 'cur_date'	=> $curdate));
					endif;
					$mmcqi++; $mmcqtrue++;
				endforeach;
			endif;
		//-----------END-MMCQ----------
	
		//-----------START-SWIPING----------
		elseif ($question_type == 7):
			#-------QUESTIONS-DATA---
			$qaudio = '';
			if ( ! empty($swipqq_qaudio)):
				$qaudio = $swipqq_qaudio;
			elseif ( ! empty($swipqq_rec_audio)):
				$qaudio = $swipqq_rec_audio;
			endif;

			$audio = '';
			if ( ! empty($swipq_audio)):
				$audio = $swipq_audio;
			elseif ( ! empty($swipq_rec_audio)):
				$audio = $swipq_rec_audio;
			endif;
			
			$video = '';
			if ( ! empty($swipq_video)):
				$video = $swipq_video;
			elseif ( ! empty($swipq_rec_video)):
				$video = $swipq_rec_video;
			endif;
			
			#------Correct-FEEDBACK-DATA---
			$feed_audio = '';
			if ( ! empty($swipq_feedback_audio)):
				$feed_audio = $swipq_feedback_audio;
			elseif ( ! empty($swipq_feedback_rec_audio)):
				$feed_audio = $swipq_feedback_rec_audio;
			endif;
			
			$feed_video = '';
			if ( ! empty($swipq_feedback_video)):
				$feed_video = $swipq_feedback_video;
			elseif ( ! empty($swipq_feedback_rec_video)):
				$feed_video = $swipq_feedback_rec_video;
			endif;
			
			#------Incorrect-FEEDBACK-DATA---
			$ifeed_audio = '';
			if ( ! empty($swipqi_feedback_audio)):
				$ifeed_audio = $swipqi_feedback_audio;
			elseif ( ! empty($swipqi_feedback_rec_audio)):
				$ifeed_audio = $swipqi_feedback_rec_audio;
			endif;
			
			$ifeed_video = '';
			if ( ! empty($swipqi_feedback_video)):
				$ifeed_video = $swipqi_feedback_video;
			elseif ( ! empty($swipqi_feedback_rec_video)):
				$ifeed_video = $swipqi_feedback_rec_video;
			endif;
						
			$quesData = array('questions'		=> ( ! empty($swipq)) ? htmlspecialchars($swipq) : '',
							  'qtype'			=> ( ! empty($question_type)) ? $question_type : '',
							  'media_file'		=> ( ! empty($videoq_media_file)) ? $videoq_media_file : '',
							  'qaudio'			=> ( ! empty($qaudio)) ? $qaudio : '',
							  'qspeech_text'	=> ( ! empty($swipqq_text_to_speech)) ? htmlspecialchars($swipqq_text_to_speech) : '',
							  'speech_text'		=> ( ! empty($swipq_text_to_speech)) ? htmlspecialchars($swipq_text_to_speech) : '',
							  'audio'			=> ( ! empty($audio)) ? $audio : '',
							  'video'			=> ( ! empty($video)) ? $video : '',
							  'screen'			=> ( ! empty($swipq_rec_screen)) ? $swipq_rec_screen : '',
							  'image'			=> ( ! empty($swipq_img)) ? $swipq_img : '',
							  'document'		=> ( ! empty($swipq_doc)) ? $swipq_doc : '',
							  'true_option'		=> ( ! empty($swipq_true_option)) ? $swipq_true_option : '',
							  'shuffle'			=> (isset($swipq_shuffle) && ! empty($swipq_shuffle)) ? 1 : 0,
							  'ques_val_1'		=> ( ! empty($swipq_ques_val_1)) ? $swipq_ques_val_1 : '',
							  'ques_val_2'		=> ( ! empty($swipq_ques_val_2)) ? $swipq_ques_val_2 : '',
							  'ques_val_3'		=> ( ! empty($swipq_ques_val_3)) ? $swipq_ques_val_3 : '',
							  'ques_val_4'		=> ( ! empty($swipq_ques_val_4)) ? $swipq_ques_val_4 : '',
							  'ques_val_5'		=> ( ! empty($swipq_ques_val_5)) ? $swipq_ques_val_5 : '',
							  'ques_val_6'		=> ( ! empty($swipq_ques_val_6)) ? $swipq_ques_val_6 : '',
							  'critical'		=> ( ! empty($swip_criticalQ)) ? $swip_criticalQ : '',
							  'status' 			=> 1,
							  'user_id'			=> $userId,
							  'cur_date'		=> $curdate,
							  'qid'				=> $ques_id);
			$ques_sql  = "UPDATE `question_tbl` SET `questions`=:questions, `question_type`=:qtype, `qaudio`=:qaudio, `qspeech_text`=:qspeech_text, `speech_text`=:speech_text, `audio`=:audio, `video`=:video, `screen`=:screen, `image`=:image, `document`=:document, `videoq_media_file`=:media_file,`true_options`=:true_option, `shuffle`=:shuffle, `ques_val_1`=:ques_val_1, `ques_val_2`=:ques_val_2, `ques_val_3`=:ques_val_3, `ques_val_4`=:ques_val_4, `ques_val_5`=:ques_val_5, `ques_val_6`=:ques_val_6, `critical`=:critical, `status`=:status, `uid`=:user_id, `cur_date`=:cur_date WHERE question_id=:qid";
			$ques_stmt = $db->prepare($ques_sql);
			$ques_stmt->execute($quesData);
			
			#-----Remove-Old-Feedback-------
			$db->feedback_remove($ques_id);

			if (isset($chkCorSwipeEndSim) && $chkCorSwipeEndSim > 0){
				$mappedQuestionSwipeCorrect = '';
			}

			#--------Correct-Feedback----------
			$cfeedData = array('qid'			=> ( ! empty($ques_id)) ? $ques_id : '',
							   'nextQid'		=> ( ! empty($mappedQuestionSwipeCorrect)) ? $mappedQuestionSwipeCorrect : '',
							   'EndSim'			=> (isset($chkCorSwipeEndSim) && ! empty($chkCorSwipeEndSim)) ? 1 : 0,
							   'feedback'		=> ( ! empty($swipq_cfeedback)) ? htmlspecialchars($swipq_cfeedback) : '',
							   'ftype'			=> 1,
							   'speech_text'	=> ( ! empty($swipq_feedback_text_to_speech)) ? htmlspecialchars($swipq_feedback_text_to_speech) : '',
							   'audio'			=> ( ! empty($feed_audio)) ? $feed_audio : '',
							   'video'			=> ( ! empty($feed_video)) ? $feed_video : '',
							   'screen'			=> ( ! empty($swipq_feedback_rec_screen)) ? $swipq_feedback_rec_screen : '',
							   'image'			=> ( ! empty($swipq_feedback_img)) ? $swipq_feedback_img : '',
							   'document'		=> ( ! empty($swipq_feedback_doc)) ? $swipq_feedback_doc : '');
			$cfeed_sql  = "INSERT INTO `feedback_tbl` (`question_id`,`nextQid`,`EndSim`, `feedback`, `feedback_type`, `feed_speech_text`, `feed_audio`, `feed_video`, `feed_screen`, `feed_image`, `feed_document`) VALUES (:qid, :nextQid,:EndSim,:feedback, :ftype, :speech_text, :audio, :video, :screen, :image, :document)";
			$cfeed_stmt = $db->prepare($cfeed_sql);
			$cfeed_stmt->execute($cfeedData);
			
			if (isset($chkInSwipeEndSim) && $chkInSwipeEndSim > 0){
				$mappedQuestionSwipeIncorrect = '';
			}

			#--------Incorrect-Feedback-------
			$ifeedData = array('qid'			=> ( ! empty($ques_id)) ? $ques_id : '',
							   'nextQid'		=> ( ! empty($mappedQuestionSwipeIncorrect)) ? $mappedQuestionSwipeIncorrect : '',
							   'EndSim'			=> (isset($chkInSwipeEndSim) && ! empty($chkInSwipeEndSim)) ? 1 : 0,
							   'feedback'		=> ( ! empty($swipq_ifeedback)) ? htmlspecialchars($swipq_ifeedback) : '',
							   'ftype'			=> 2,
							   'speech_text'	=> ( ! empty($swipqi_feedback_text_to_speech)) ? htmlspecialchars($swipqi_feedback_text_to_speech) : '',
							   'audio'			=> ( ! empty($ifeed_audio)) ? $ifeed_audio : '',
							   'video'			=> ( ! empty($ifeed_video)) ? $ifeed_video : '',
							   'screen'			=> ( ! empty($swipqi_feedback_rec_screen)) ? $swipqi_feedback_rec_screen : '',
							   'image'			=> ( ! empty($swipqi_feedback_img)) ? $swipqi_feedback_img : '',
							   'document'		=> ( ! empty($swipqi_feedback_doc)) ? $swipqi_feedback_doc : '');			
			$ifeed_sql  = "INSERT INTO `feedback_tbl` (`question_id`, `nextQid`,`EndSim`,`feedback`, `feedback_type`, `feed_speech_text`, `feed_audio`, `feed_video`, `feed_screen`, `feed_image`, `feed_document`) VALUES (:qid, :nextQid,:EndSim,:feedback, :ftype, :speech_text, :audio, :video, :screen, :image, :document)";
			$ifeed_stmt = $db->prepare($ifeed_sql);
			$ifeed_stmt->execute($ifeedData);
			
			#----------ADD-OPTIONS-------------
			$swipqi = 0; $swiptrue = 1;
			foreach ($swipq_option as $answerData):
				if ( ! empty($updateSwipqAnswerid[$swipqi])):
					$ansSql = "UPDATE `answer_tbl` SET `choice_option`=:option, `true_option`=:true, `image`=:image, `status`=:status, `uid`=:user_id, `cur_date`=:cur_date WHERE answer_id=:aid";
					$stmt	= $db->prepare($ansSql);
					$stmt->execute(array('aid' 		=> $updateSwipqAnswerid[$swipqi],
										 'option'	=> ( ! empty($swipq_option[$swipqi])) ? htmlspecialchars($swipq_option[$swipqi]) : '',
										 'image'	=> ( ! empty($swipq_item_img[$swipqi])) ? $swipq_item_img[$swipqi] : '',
										 'true'		=> ( ! empty($swipq_true_option) && $swipq_true_option == $swiptrue) ? 1 : '',
										 'status'	=> 1,
										 'user_id' 	=> $userId,
										 'cur_date'	=> $curdate));
				else:
					$ansSql = "INSERT INTO `answer_tbl` (`question_id`, `choice_option`, `true_option`, `image`, `status`, `uid`, `cur_date`) VALUES (:qid, :option, :true, :image, :status, :user_id, :cur_date)";
					$stmt = $db->prepare($ansSql);
					$stmt->execute(array('qid'		=> $ques_id,
										 'option'	=> ( ! empty($swipq_option[$swipqi])) ? htmlspecialchars($swipq_option[$swipqi]) : '',
										 'image'	=> ( ! empty($swipq_item_img[$swipqi])) ? $swipq_item_img[$swipqi] : '',
										 'true'		=> ( ! empty($swipq_true_option) && $swipq_true_option == $swiptrue) ? 1 : '',
										 'status'	=> 1,
										 'user_id' 	=> $userId,
										 'cur_date'	=> $curdate));
				endif;
				$swipqi++; $swiptrue++;
			endforeach;
		//-----------END-SWIPING----------
		
		//-----------START-DDQ----------
		elseif ($question_type == 8):
			#-------QUESTIONS-DATA---
			$qaudio = '';
			if ( ! empty($ddqq_qaudio)):
				$qaudio = $ddqq_qaudio;
			elseif ( ! empty($ddqq_rec_audio)):
				$qaudio = $ddqq_rec_audio;
			endif;
		
			$audio = '';
			if ( ! empty($ddq_audio)):
				$audio = $ddq_audio;
			elseif ( ! empty($ddq_rec_audio)):
				$audio = $ddq_rec_audio;
			endif;
			
			$video = '';
			if ( ! empty($ddq_video)):
				$video = $ddq_video;
			elseif ( ! empty($ddq_rec_video)):
				$video = $ddq_rec_video;
			endif;
			
			#------Correct-FEEDBACK-DATA---
			$feed_audio = '';
			if ( ! empty($ddq_feedback_audio)):
				$feed_audio = $ddq_feedback_audio;
			elseif ( ! empty($ddq_feedback_rec_audio)):
				$feed_audio = $ddq_feedback_rec_audio;
			endif;
			
			$feed_video = '';
			if ( ! empty($ddq_feedback_video)):
				$feed_video = $ddq_feedback_video;
			elseif ( ! empty($ddq_feedback_rec_video)):
				$feed_video = $ddq_feedback_rec_video;
			endif;
			
			#------Incorrect-FEEDBACK-DATA---
			$ifeed_audio = '';
			if ( ! empty($ddqi_feedback_audio)):
				$ifeed_audio = $ddqi_feedback_audio;
			elseif ( ! empty($ddqi_feedback_rec_audio)):
				$ifeed_audio = $ddqi_feedback_rec_audio;
			endif;
			
			$ifeed_video = '';
			if ( ! empty($ddqi_feedback_video)):
				$ifeed_video = $ddqi_feedback_video;
			elseif ( ! empty($ddqi_feedback_rec_video)):
				$ifeed_video = $ddqi_feedback_rec_video;
			endif;
			
			#--For-Multi-DROP-Options----
			if ($seleted_drag_option == 1):				
				$quesData = array('questions'			=> ( ! empty($ddq)) ? htmlspecialchars($ddq) : '',
								  'videoq_media_file'	=> ( ! empty($videoq_media_file)) ? addslashes($videoq_media_file) : '',
								  'seleted_drag' 		=> ( ! empty($seleted_drag_option)) ? $seleted_drag_option : '',
								  'qtype'				=> ( ! empty($question_type)) ? $question_type : '',
								  'qaudio'				=> ( ! empty($qaudio)) ? $qaudio : '',
								  'qspeech_text'		=> ( ! empty($ddqq_text_to_speech)) ? htmlspecialchars($ddqq_text_to_speech) : '',
								  'speech_text'			=> ( ! empty($ddq_text_to_speech)) ? htmlspecialchars($ddq_text_to_speech) : '',
								  'audio'				=> ( ! empty($audio)) ? $audio : '',
								  'video'				=> ( ! empty($video)) ? $video : '',
								  'screen'				=> ( ! empty($ddq_rec_screen)) ? $ddq_rec_screen : '',
								  'image'				=> ( ! empty($ddq_img)) ? $ddq_img : '',
								  'document'			=> ( ! empty($ddq_doc)) ? $ddq_doc : '',
								  'shuffle'				=> (isset($ddq_shuffle1) && ! empty($ddq_shuffle1)) ? 1 : 0,
								  'ques_val_1'			=> ( ! empty($ddq_ques_val_1)) ? $ddq_ques_val_1 : '',
								  'ques_val_2'			=> ( ! empty($ddq_ques_val_2)) ? $ddq_ques_val_2 : '',
								  'ques_val_3'			=> ( ! empty($ddq_ques_val_3)) ? $ddq_ques_val_3 : '',
								  'ques_val_4'			=> ( ! empty($ddq_ques_val_4)) ? $ddq_ques_val_4 : '',
								  'ques_val_5'			=> ( ! empty($ddq_ques_val_5)) ? $ddq_ques_val_5 : '',
								  'ques_val_6'			=> ( ! empty($ddq_ques_val_6)) ? $ddq_ques_val_6 : '',
								  'critical'			=> ( ! empty($ddq_criticalQ)) ? $ddq_criticalQ : '',
								  'status' 				=> 1,
								  'user_id'				=> $userId,
								  'cur_date'			=> $curdate,
								  'qid'					=> $ques_id);
				$ques_sql  = "UPDATE `question_tbl` SET `questions`=:questions, `videoq_media_file`=:videoq_media_file,`question_type`=:qtype, `seleted_drag_option`=:seleted_drag, `qaudio`=:qaudio, `qspeech_text`=:qspeech_text, `speech_text`=:speech_text, `audio`=:audio, `video`=:video, `screen`=:screen, `image`=:image, `document`=:document, `shuffle`=:shuffle, `ques_val_1`=:ques_val_1, `ques_val_2`=:ques_val_2, `ques_val_3`=:ques_val_3, `ques_val_4`=:ques_val_4, `ques_val_5`=:ques_val_5, `ques_val_6`=:ques_val_6, `critical`=:critical, `status`=:status, `uid`=:user_id, `cur_date`=:cur_date WHERE question_id=:qid";
				$ques_stmt = $db->prepare($ques_sql);
				if ($ques_stmt->execute($quesData)):
					#------OPTIONS------
					$ddqi = 0;
					foreach ($ddq_drop as $answerData):
						if ( ! empty($updateDDqAnswerid[$ddqi])):
							$ansSql = "UPDATE `answer_tbl` SET `choice_option`=:choice, `match_option`=:match, `image`=:image, `image2`=:image2, `status`=:status, `uid`=:user_id, `cur_date`=:cur_date WHERE answer_id=:aid";
							$stmt = $db->prepare($ansSql);
							$stmt->execute(array('aid' 		=> $updateDDqAnswerid[$ddqi],
												'choice'	=> ( ! empty($ddq_drop[$ddqi])) ? htmlspecialchars($ddq_drop[$ddqi]) : '',
												'match'	=> ( ! empty($ddq_drag[$ddqi])) ? htmlspecialchars($ddq_drag[$ddqi]) : '',
												'image'	=> ( ! empty($ddq_item_img[$ddqi])) ? $ddq_item_img[$ddqi] : '',
												'image2'	=> ( ! empty($ddq_drag_item_img[$ddqi])) ? $ddq_drag_item_img[$ddqi] : '',
												'status'	=> 1,
												'user_id' 	=> $userId,
												'cur_date'	=> $curdate));
						else:
							$ansSql = "INSERT INTO `answer_tbl` (`question_id`, `choice_option`, `match_option`, `image`, `image2`, `status`, `uid`, `cur_date`) VALUES (:qid, :choice, :match, :image, :image2, :status, :user_id, :cur_date)";
							$stmt = $db->prepare($ansSql);
							$stmt->execute(array('qid'		=> $ques_id,
												'choice'	=> ( ! empty($ddq_drop[$ddqi])) ? htmlspecialchars($ddq_drop[$ddqi]) : '',
												'match'	=> ( ! empty($ddq_drag[$ddqi])) ? htmlspecialchars($ddq_drag[$ddqi]) : '',
												'image'	=> ( ! empty($ddq_item_img[$ddqi])) ? $ddq_item_img[$ddqi] : '',
												'image2'	=> ( ! empty($ddq_drag_item_img[$ddqi])) ? $ddq_drag_item_img[$ddqi] : '',
												'status'	=> 1,
												'user_id' 	=> $userId,
												'cur_date'	=> $curdate));
						endif;
						$ddqi++;
					endforeach;
					
					#-----Remove-Old-Feedback-------
					$db->feedback_remove($ques_id);

					if (isset($chkCorDNDEndSim) && $chkCorDNDEndSim > 0){
						$mappedQuestionDNDCorrect = '';
					}

					#--------Correct-Feedback-------
					$cfeedData = array('qid'			=> ( ! empty($ques_id)) ? $ques_id : '',
									   'nextQid'		=> ( ! empty($mappedQuestionDNDCorrect)) ? $mappedQuestionDNDCorrect : '',
									   'EndSim'			=> (isset($chkCorDNDEndSim) && ! empty($chkCorDNDEndSim)) ? 1 : 0,
									   'feedback'		=> ( ! empty($ddq_cfeedback)) ? htmlspecialchars($ddq_cfeedback) : '',
									   'ftype'			=> 1,
									   'speech_text'	=> ( ! empty($ddq_feedback_text_to_speech)) ? htmlspecialchars($ddq_feedback_text_to_speech) : '',
									   'audio'			=> ( ! empty($feed_audio)) ? $feed_audio : '',
									   'video'			=> ( ! empty($feed_video)) ? $feed_video : '',
									   'screen'			=> ( ! empty($ddq_feedback_rec_screen)) ? $ddq_feedback_rec_screen : '',
									   'image'			=> ( ! empty($ddq_feedback_img)) ? $ddq_feedback_img : '',
									   'document'		=> ( ! empty($ddq_feedback_doc)) ? $ddq_feedback_doc : '');
					$cfeed_sql  = "INSERT INTO `feedback_tbl` (`question_id`, `nextQid`,`EndSim`,`feedback`, `feedback_type`, `feed_speech_text`, `feed_audio`, `feed_video`, `feed_screen`, `feed_image`, `feed_document`) VALUES (:qid, :nextQid,:EndSim,:feedback, :ftype, :speech_text, :audio, :video, :screen, :image, :document)";
					$cfeed_stmt = $db->prepare($cfeed_sql);
					$cfeed_stmt->execute($cfeedData);

					if (isset($chkInDNDEndSim) && $chkInDNDEndSim > 0){
						$mappedQuestionDNDIncorrect = '';
					}

					#--------Incorrect-Feedback-------
					$ifeedData = array('qid'			=> ( ! empty($ques_id)) ? $ques_id : '',
										'nextQid'		=> ( ! empty($mappedQuestionDNDIncorrect)) ? $mappedQuestionDNDIncorrect : '',
										'EndSim'		=> (isset($chkInDNDEndSim) && ! empty($chkInDNDEndSim)) ? 1 : 0,
										'feedback'		=> ( ! empty($ddq_ifeedback)) ? htmlspecialchars($ddq_ifeedback) : '',
										'ftype'			=> 2,
										'speech_text'	=> ( ! empty($ddqi_feedback_text_to_speech)) ? htmlspecialchars($ddqi_feedback_text_to_speech) : '',
										'audio'			=> ( ! empty($ifeed_audio)) ? $ifeed_audio : '',
										'video'			=> ( ! empty($ifeed_video)) ? $ifeed_video : '',
										'screen'		=> ( ! empty($ddqi_feedback_rec_screen)) ? $ddqi_feedback_rec_screen : '',
										'image'			=> ( ! empty($ddqi_feedback_img)) ? $ddqi_feedback_img : '',
										'document'		=> ( ! empty($ddqi_feedback_doc)) ? $ddqi_feedback_doc : '');
					$ifeed_sql  = "INSERT INTO `feedback_tbl` (`question_id`, `nextQid`,`EndSim`,`feedback`, `feedback_type`, `feed_speech_text`, `feed_audio`, `feed_video`, `feed_screen`, `feed_image`, `feed_document`) VALUES (:qid,:nextQid,:EndSim, :feedback, :ftype, :speech_text, :audio, :video, :screen, :image, :document)";
					$ifeed_stmt = $db->prepare($ifeed_sql);
					$ifeed_stmt->execute($ifeedData);
				endif;
			elseif ($seleted_drag_option == 2):
				$quesData =   array('questions'			=> ( ! empty($ddq)) ? htmlspecialchars($ddq) : '',
									'videoq_media_file'	=> ( ! empty($videoq_media_file)) ? addslashes($videoq_media_file) : '',
									'seleted_drag' 		=> ( ! empty($seleted_drag_option)) ? $seleted_drag_option : '',
									'qtype'				=> ( ! empty($question_type)) ? $question_type : '',
									'qaudio'			=> ( ! empty($qaudio)) ? $qaudio : '',
									'qspeech_text'		=> ( ! empty($ddqq_text_to_speech)) ? $ddqq_text_to_speech : '',
									'speech_text'		=> ( ! empty($ddq_text_to_speech)) ? $ddq_text_to_speech : '',
									'audio'				=> ( ! empty($audio)) ? $audio : '',
									'video'				=> ( ! empty($video)) ? $video : '',
									'screen'			=> ( ! empty($ddq_rec_screen)) ? $ddq_rec_screen : '',
									'image'				=> ( ! empty($ddq_img)) ? $ddq_img : '',
									'document'			=> ( ! empty($ddq_doc)) ? $ddq_doc : '',
									'shuffle'			=> (isset($ddq_shuffle2) && ! empty($ddq_shuffle2)) ? 1 : 0,
									'true_options'		=> ( ! empty($ddq_true_option)) ? $ddq_true_option : '',
									'ques_val_1'		=> ( ! empty($ddq_ques_val_1)) ? $ddq_ques_val_1 : '',
									'ques_val_2'		=> ( ! empty($ddq_ques_val_2)) ? $ddq_ques_val_2 : '',
									'ques_val_3'		=> ( ! empty($ddq_ques_val_3)) ? $ddq_ques_val_3 : '',
									'ques_val_4'		=> ( ! empty($ddq_ques_val_4)) ? $ddq_ques_val_4 : '',
									'ques_val_5'		=> ( ! empty($ddq_ques_val_5)) ? $ddq_ques_val_5 : '',
									'ques_val_6'		=> ( ! empty($ddq_ques_val_6)) ? $ddq_ques_val_6 : '',
									'critical'			=> ( ! empty($ddq_criticalQ)) ? $ddq_criticalQ : '',
									'status' 			=> 1,
									'user_id'			=> $userId,
									'cur_date'			=> $curdate,
									'qid'				=> $ques_id);
				$ques_sql  = "UPDATE `question_tbl` SET `questions`=:questions,`videoq_media_file`=:videoq_media_file, `question_type`=:qtype, `seleted_drag_option`=:seleted_drag, `qaudio`=:qaudio, `qspeech_text`=:qspeech_text, `speech_text`=:speech_text, `audio`=:audio, `video`=:video, `screen`=:screen, `image`=:image, `document`=:document, `shuffle`=:shuffle, `true_options`=:true_options, `ques_val_1`=:ques_val_1, `ques_val_2`=:ques_val_2, `ques_val_3`=:ques_val_3, `ques_val_4`=:ques_val_4, `ques_val_5`=:ques_val_5, `ques_val_6`=:ques_val_6, `critical`=:critical, `status`=:status, `uid`=:user_id, `cur_date`=:cur_date WHERE question_id=:qid";
				$ques_stmt = $db->prepare($ques_sql);
				$ques_stmt->execute($quesData);
				#------OPTION------
				if ( ! empty($updateDDq2Answerid)):
					#----DROP-TARGET----
					$ansSql = "UPDATE `sub_options_tbl` SET `sub_option`=:option, `image`=:image WHERE sub_options_id=:sid";
					$stmt = $db->prepare($ansSql);
					$stmt->execute(array('sid' 		=> $updateDDq2Answerid,
										 'option'	=> ( ! empty($ddq2_drop)) ? htmlspecialchars($ddq2_drop) : '',
										 'image' 	=> ( ! empty($ddq2_item_img)) ? $ddq2_item_img : ''));
					#-----DRAG-ITEMS------
					$ddq2iu = 0; $ddq2true = 1;
					foreach ($ddq2_drag as $subAnswerData):
						if ( ! empty($updateDDq2SubAnswerid[$ddq2iu])):
							$getOpId	   = $updateDDq2SubAnswerid[$ddq2iu];
							$updateAnsSql  = "UPDATE `answer_tbl` SET `choice_option`=:option, `image`=:image, `true_option`=:true, `status`=:status, `uid`=:user_id, `cur_date`=:cur_date WHERE answer_id=:aid";
							$updateAnsstmt = $db->prepare($updateAnsSql);
							$updateAnsstmt->execute(array('aid' 		=> $getOpId,
														  'option'		=> ( ! empty($ddq2_drag[$ddq2iu])) ? htmlspecialchars($ddq2_drag[$ddq2iu]) : '',
														  'image'		=> ( ! empty($ddq2_drag_item_img[$ddq2iu])) ? $ddq2_drag_item_img[$ddq2iu] : '',
														  'true'		=> ( ! empty($ddq_true_option) && $ddq_true_option == $ddq2true) ? 1 : '',
														  'status'		=> 1,
														  'user_id' 	=> $userId,
														  'cur_date'	=> $curdate));
						else:
							$ansSql = "INSERT INTO `answer_tbl` (`question_id`, `choice_option`, `image`, `true_option`, `status`, `uid`, `cur_date`) VALUES (:qid, :option, :image, :true, :status, :user_id, :cur_date)";
							$stmt 	= $db->prepare($ansSql);
							$stmt->execute(array('qid'		=> $ques_id,
												 'option'	=> ( ! empty($ddq2_drag[$ddq2iu])) ? htmlspecialchars($ddq2_drag[$ddq2iu]) : '',
												 'image'	=> ( ! empty($ddq2_drag_item_img[$ddq2iu])) ? $ddq2_drag_item_img[$ddq2iu] : '',
												 'true'		=> ( ! empty($ddq_true_option) && $ddq_true_option == $ddq2true) ? 1 : '',
												 'status'	=> 1,
												 'user_id' 	=> $userId,
												 'cur_date'	=> $curdate));
						endif;
						$ddq2iu++; $ddq2true++;
					endforeach;
				else:
					#----DROP-TARGET----
					$ansSql = "INSERT INTO `sub_options_tbl` (`question_id`, `sub_option`, `image`) VALUES (:qid, :option, :image)";
					$stmt   = $db->prepare($ansSql);
					$stmt->execute(array('qid' 		=> $ques_id,
										 'option'	=> ( ! empty($ddq2_drop)) ? htmlspecialchars($ddq2_drop) : '',
										 'image' 	=> ( ! empty($ddq2_item_img)) ? $ddq2_item_img : ''));
					#-----DRAG-ITEMS------
					$ddq2i = 0; $ddq2itrue = 1;
					foreach ($ddq2_drag as $answerData):
						$ansSql = "INSERT INTO `answer_tbl` (`question_id`, `choice_option`, `image`, `true_option`, `status`, `uid`, `cur_date`) VALUES (:qid, :choice, :image, :true, :status, :user_id, :cur_date)";
						$stmt = $db->prepare($ansSql);
						$stmt->execute(array('qid'		=> $ques_id,
											 'choice'	=> ( ! empty($ddq2_drag[$ddq2i])) ? htmlspecialchars($ddq2_drag[$ddq2i]) : '',
											 'image'	=> ( ! empty($ddq2_drag_item_img[$ddq2i])) ? $ddq2_drag_item_img[$ddq2i] : '',
											 'true'		=> ( ! empty($ddq_true_option) && $ddq_true_option == $ddq2itrue) ? 1 : '',
											 'status'	=> 1,
											 'user_id' 	=> $userId,
											 'cur_date'	=> $curdate));
						$ddq2i++; $ddq2itrue++;
					endforeach;
				endif;

				#-----Remove-Old-Feedback-------
				$db->feedback_remove($ques_id);

				#--------Correct-Feedback-------
				$cfeedData = array('qid'			=> ( ! empty($ques_id)) ? $ques_id : '',
								   'nextQid'		=> ( ! empty($mappedQuestionDNDCorrect)) ? $mappedQuestionDNDCorrect : '',
								   'EndSim'		=> (isset($chkCorDNDEndSim) && ! empty($chkCorDNDEndSim)) ? 1 : 0,
								   'feedback'		=> ( ! empty($ddq_cfeedback)) ? htmlspecialchars($ddq_cfeedback) : '',
								   'ftype'			=> 1,
								   'speech_text'	=> ( ! empty($ddq_feedback_text_to_speech)) ? htmlspecialchars($ddq_feedback_text_to_speech) : '',
								   'audio'			=> ( ! empty($feed_audio)) ? $feed_audio : '',
								   'video'			=> ( ! empty($feed_video)) ? $feed_video : '',
								   'screen'			=> ( ! empty($ddq_feedback_rec_screen)) ? $ddq_feedback_rec_screen : '',
								   'image'			=> ( ! empty($ddq_feedback_img)) ? $ddq_feedback_img : '',
								   'document'		=> ( ! empty($ddq_feedback_doc)) ? $ddq_feedback_doc : '');
				$cfeed_sql  = "INSERT INTO `feedback_tbl` (`question_id`, `nextQid`, `EndSim`,`feedback`, `feedback_type`, `feed_speech_text`, `feed_audio`, `feed_video`, `feed_screen`, `feed_image`, `feed_document`) VALUES (:qid, :nextQid, :EndSim,:feedback, :ftype, :speech_text, :audio, :video, :screen, :image, :document)";
				$cfeed_stmt = $db->prepare($cfeed_sql);
				$cfeed_stmt->execute($cfeedData);

				#--------Incorrect-Feedback-------
				$ifeedData = array('qid'			=> ( ! empty($ques_id)) ? $ques_id : '',
								   'nextQid'		=> ( ! empty($mappedQuestionDNDIncorrect)) ? $mappedQuestionDNDIncorrect : '',
								   'EndSim'			=> (isset($chkInDNDEndSim) && ! empty($chkInDNDEndSim)) ? 1 : 0,
								   'feedback'		=> ( ! empty($ddq_ifeedback)) ? htmlspecialchars($ddq_ifeedback) : '',
								   'ftype'			=> 2,
								   'speech_text'	=> ( ! empty($ddqi_feedback_text_to_speech)) ? htmlspecialchars($ddqi_feedback_text_to_speech) : '',
								   'audio'			=> ( ! empty($ifeed_audio)) ? $ifeed_audio : '',
								   'video'			=> ( ! empty($ifeed_video)) ? $ifeed_video : '',
								   'screen'			=> ( ! empty($ddqi_feedback_rec_screen)) ? $ddqi_feedback_rec_screen : '',
								   'image'			=> ( ! empty($ddqi_feedback_img)) ? $ddqi_feedback_img : '',
								   'document'		=> ( ! empty($ddqi_feedback_doc)) ? $ddqi_feedback_doc : '');
				$ifeed_sql  = "INSERT INTO `feedback_tbl` (`question_id`, `nextQid`, `EndSim`,`feedback`, `feedback_type`, `feed_speech_text`, `feed_audio`, `feed_video`, `feed_screen`, `feed_image`, `feed_document`) VALUES (:qid, :nextQid, :EndSim,:feedback, :ftype, :speech_text, :audio, :video, :screen, :image, :document)";
				$ifeed_stmt = $db->prepare($ifeed_sql);
				$ifeed_stmt->execute($ifeedData);
			endif;
		endif;
	endif;
	if ( ! empty($submit_type) && $submit_type == 1):
		$resdata = array('success' => TRUE, 'all_update' => FALSE, 'msg' => 'Simulation details updated successfully.', 'return_url' => 'branching-video-multiple-template.php?add_data=true&sim_id='.md5($sim_id).'&ques_id='.base64_encode($ques_id));
	else:
		$resdata = array('success' => TRUE, 'all_update' => TRUE, 'msg' => 'All questions and answers updated successfully.', 'return_url' => 'author-simulation.php');
	endif;
	echo json_encode($resdata);
endif;

#-----------------------------------------Add-Update-Open-Response-Sim ----------------------------------
if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['update_open_response_sim']) && isset($_POST['sim_id'])):
	extract($_POST);
	$error = FALSE;
	if (empty($sim_page_name[0]) || empty($sim_duration) || empty($sim_title) || empty($passing_marks)):
		$resdata = array('success' => FALSE, 'msg' => 'Please fill all the mandatory fields on add Simulation and Simulation details to proceed.');
		$error = TRUE;
		echo json_encode($resdata); exit;
	elseif (isset($competency_name_1) && empty($competency_name_1) || empty($competency_score_1) || empty($weightage_1)):
		$resdata = array('success' => FALSE, 'msg' => 'Please fill all the mandatory fields on Simulation details to proceed.');
		$error = TRUE;
		echo json_encode($resdata); exit;
	elseif ($error == FALSE):
		$sql = "UPDATE scenario_master SET `Scenario_title` = '". $sim_title ."', `duration` = '". $sim_duration ."', `passing_marks` = '". $passing_marks ."', `sim_cover_img` = '". $scenario_cover_file ."'";
		if (isset($character_type) && $character_type == 1):
			$sql .= ", sim_char_img = '', sim_char_left_img = '', sim_char_right_img = '', char_own_choice = ''";
		elseif (isset($charoption) && $charoption == 1 && isset($scenario_char_file) && ! empty($scenario_char_file)):
			$sql .= ", char_own_choice = '". $scenario_char_file ."', sim_char_img = '', sim_char_left_img = '', sim_char_right_img = ''";
		elseif (isset($charoption) && $charoption == 2 && isset($sim_char_img) && ! empty($sim_char_img)):
			$sql .= ", sim_char_img = '". $sim_char_img ."', sim_char_left_img = '', sim_char_right_img = ''";
		elseif (isset($sim_two_char_img) && count($sim_two_char_img) > 0):
			if (isset($sim_two_char_img[0])):
				$r = explode('|', $sim_two_char_img[0]);
				if (isset($r[1]) && $r[1] == 'R'):
					$sql .= ", sim_char_img = '', char_own_choice = '', sim_char_right_img = '". $r[0] ."'";
				elseif (isset($r[1]) && $r[1] == 'L'):
					$sql .= ", sim_char_img = '', char_own_choice = '', sim_char_left_img = '". $r[0] ."'";
				endif;
			endif;
			if (isset($sim_two_char_img[1])):
				$l = explode('|', $sim_two_char_img[1]);
				if (isset($l[1]) && $l[1] == 'L'):
					$sql .= ", sim_char_img = '', char_own_choice = '', sim_char_left_img = '". $l[0] ."'";
				elseif (isset($l[1]) && $l[1] == 'R'):
					$sql .= ", sim_char_img = '', char_own_choice = '', sim_char_right_img = '". $l[0] ."'";
				endif;
			endif;
		endif;
		#--Add-Web-Object--
		if ( ! empty($web_object)):
			$sql .= ", web_object = '". $web_object ."', web_object_title = '". htmlspecialchars($web_object_title) ."'";
		endif;
		#--Sim-Media-File--
		if ( ! empty($scenario_media_file) && in_array(strtolower(pathinfo($scenario_media_file, PATHINFO_EXTENSION)), $img_valid_exts)):
			$sql .= ", image_url = '". $scenario_media_file ."', video_url = NULL, audio_url = NULL";
		elseif ( ! empty($scenario_media_file) && in_array(strtolower(pathinfo($scenario_media_file, PATHINFO_EXTENSION)), $video_valid_exts)):
			$sql .= ", video_url = '". $scenario_media_file ."', image_url = NULL, audio_url = NULL";
		elseif ( ! empty($scenario_media_file) && in_array(strtolower(pathinfo($scenario_media_file, PATHINFO_EXTENSION)), $audio_valid_exts)):
			$sql .= ", audio_url = '". $scenario_media_file ."', video_url = NULL, image_url = NULL";
		elseif ( ! empty($scenario_media_file) && ! in_array(strtolower(pathinfo($scenario_media_file, PATHINFO_EXTENSION)), $audio_valid_exts) && 
				! in_array(strtolower(pathinfo($scenario_media_file, PATHINFO_EXTENSION)), $img_valid_exts) && 
				! in_array(strtolower(pathinfo($scenario_media_file, PATHINFO_EXTENSION)), $video_valid_exts)):
			$sql .= ", web_object_file = '". $scenario_media_file ."', video_url = NULL, image_url = NULL, audio_url = NULL";
		endif;
		#--Sim-Backgrounds--
		if (isset($bgoption) && $bgoption == 1 && ! empty($bg_color)):
			$sql .= ", bg_color = '". $bg_color ."', bg_own_choice = '', sim_back_img = ''";
		elseif (isset($bgoption) && $bgoption == 2 && ! empty($scenario_bg_file)):
			$sql .= ", bg_own_choice = '". $scenario_bg_file ."', bg_color = '', sim_back_img = ''";
		elseif (isset($bgoption) && $bgoption == 3 && ! empty($sim_background_img)):
			$sql .= ", sim_back_img = '". $sim_background_img ."', bg_own_choice = '', bg_color = ''";
		endif;
		$sql .= " WHERE scenario_id = '". $sim_id ."'";
		$results = $db->prepare($sql);
		if ($results->execute()):
			#----Competency----
			$delCom	 = "DELETE FROM competency_tbl WHERE scenario_id = '". $sim_id ."'";
			$delComq = $db->prepare($delCom); $delComq->execute();
			$data	 = array('scenario_id'	=> $sim_id,
							 'comp_col_1'	=> (isset($competency_name_1) && ! empty($competency_name_1)) ? addslashes($competency_name_1) : '',
							 'comp_val_1'	=> (isset($competency_score_1) && ! empty($competency_score_1)) ? $competency_score_1 : '',
							 'comp_col_2'	=> (isset($competency_name_2) && ! empty($competency_name_2)) ? addslashes($competency_name_2) : '',
							 'comp_val_2'	=> (isset($competency_score_2) && ! empty($competency_score_2)) ? $competency_score_2 : '',
							 'comp_col_3'	=> (isset($competency_name_3) && ! empty($competency_name_3)) ? addslashes($competency_name_3) : '',
							 'comp_val_3'	=> (isset($competency_score_3) && ! empty($competency_score_3)) ? $competency_score_3 : '',
							 'comp_col_4'	=> (isset($competency_name_4) && ! empty($competency_name_4)) ? addslashes($competency_name_4) : '',
							 'comp_val_4'	=> (isset($competency_score_4) && ! empty($competency_score_4)) ? $competency_score_4 : '',
							 'comp_col_5'	=> (isset($competency_name_5) && ! empty($competency_name_5)) ? addslashes($competency_name_5) : '',
							 'comp_val_5'	=> (isset($competency_score_5) && ! empty($competency_score_5)) ? $competency_score_5 : '',
							 'comp_col_6'	=> (isset($competency_name_6) && ! empty($competency_name_6)) ? addslashes($competency_name_6) : '',
							 'comp_val_6'	=> (isset($competency_score_6) && ! empty($competency_score_6)) ? $competency_score_6 : '',
							 'weightage_1'	=> (isset($weightage_1) && ! empty($weightage_1)) ? $weightage_1 : '',
							 'weightage_2'	=> (isset($weightage_2) && ! empty($weightage_2)) ? $weightage_2 : '',
							 'weightage_3'	=> (isset($weightage_3) && ! empty($weightage_3)) ? $weightage_3 : '',
							 'weightage_4'	=> (isset($weightage_4) && ! empty($weightage_4)) ? $weightage_4 : '',
							 'weightage_5'	=> (isset($weightage_5) && ! empty($weightage_5)) ? $weightage_5 : '',
							 'weightage_6'	=> (isset($weightage_6) && ! empty($weightage_6)) ? $weightage_6 : '',
							 'status' 		=> 1,
							 'user_id' 		=> $userId,
							 'cur_date'		=> $curdate);
			$com_sql  = "INSERT INTO `competency_tbl` (`scenario_id`, `comp_col_1`, `comp_val_1`, `comp_col_2`, `comp_val_2`, `comp_col_3`, `comp_val_3`, `comp_col_4`, `comp_val_4`, `comp_col_5`, `comp_val_5`, `comp_col_6`, `comp_val_6`, `weightage_1`, `weightage_2`, `weightage_3`, `weightage_4`, `weightage_5`, `weightage_6`, `status`, `uid`, `cur_date`) VALUES (:scenario_id, :comp_col_1, :comp_val_1, :comp_col_2, :comp_val_2, :comp_col_3, :comp_val_3, :comp_col_4, :comp_val_4, :comp_col_5, :comp_val_5, :comp_col_6, :comp_val_6, :weightage_1, :weightage_2, :weightage_3, :weightage_4, :weightage_5, :weightage_6, :status, :user_id, :cur_date)";
			$com_stmt = $db->prepare($com_sql);
			$com_stmt->execute($data);
			
			#----PAGES----
			$delPage  = "DELETE FROM sim_pages WHERE scenario_id = '". $sim_id ."'";
			$delPageq = $db->prepare($delPage); $delPageq->execute();
			if ( ! empty($sim_page_name)):
				$sp = 0;
				foreach ($sim_page_name as $pdata):
					$pageq = "INSERT INTO `sim_pages` (`scenario_id`, `sim_page_name`, `sim_page_desc`, `userid`, `status`, `added_date`) 
					VALUES (:sid, :pname, :pdesc, :user_id, :status, :adate)";
					$page_stmt = $db->prepare($pageq);
					$page_stmt->execute(array('sid'		=> $sim_id,
											  'pname'	=> ( ! empty($pdata)) ? htmlspecialchars($pdata) : '',
											  'pdesc'	=> ( ! empty($sim_page_desc[$sp])) ? $sim_page_desc[$sp] : '',
											  'status' 	=> 1,
											  'user_id'	=> $userId,
											  'adate'	=> $curdate));
				$sp++;
				endforeach;
			endif;
			
			#----Branding----
			$bandq	  	= "DELETE FROM sim_branding_tbl WHERE scenario_id = '". $sim_id ."'";
			$bandqexe	= $db->prepare($bandq); $bandqexe->execute();
			$brand_data = array('sid'				=> $sim_id,
								'bg'				=> ( ! empty($ques_bg_color)) ? $ques_bg_color : '',
								'bg_tr'				=> ( ! empty($ques_bg_transp)) ? $ques_bg_transp : '',
								'obg'				=> ( ! empty($ques_option_bg_color)) ? $ques_option_bg_color : '',
								'obg_tr'			=> ( ! empty($ques_option_bg_transp)) ? $ques_option_bg_transp : '',
								'ohover'			=> ( ! empty($ques_option_hover_color)) ? $ques_option_hover_color : '',
								'oh_tr'				=> ( ! empty($ques_option_hover_transp)) ? $ques_option_hover_transp : '',
								'oselected'			=> ( ! empty($ques_option_select_color)) ? $ques_option_select_color : '',
								'oselected_tr'		=> ( ! empty($ques_option_select_transp)) ? $ques_option_select_transp : '',
								'btn_bg'			=> ( ! empty($btn_bg_color)) ? $btn_bg_color : '',
								'btn_bg_tr'			=> ( ! empty($btn_bg_transp)) ? $btn_bg_transp : '',
								'btn_hover'			=> ( ! empty($btn_hover_color)) ? $btn_hover_color : '',
								'btn_hover_tr'		=> ( ! empty($btn_hover_transp)) ? $btn_hover_transp : '',
								'btn_selected'		=> ( ! empty($btn_select_color)) ? $btn_select_color : '',
								'btn_selected_tr'	=> ( ! empty($btn_select_transp)) ? $btn_select_transp : '',
								'font_type'			=> ( ! empty($font_type)) ? $font_type : '',
								'font_color'		=> ( ! empty($font_color)) ? $font_color : '',
								'font_size'			=> ( ! empty($font_size)) ? $font_size : '',
								'user_id' 			=> $userId,
								'added_date'		=> $curdate);
			$com_sql = "INSERT INTO `sim_branding_tbl` (`scenario_id`, `ques_bg`, `ques_bg_transparency`, `option_bg`, `option_bg_transparency`, `option_hover`, `option_hover_transparency`, `option_selected`, `option_selected_transparency`, `btn_bg`, `btn_bg_transparency`, `btn_hover`, `btn_hover_transparency`, `btn_selected`, `btn_selected_transparency`, `font_type`, `font_color`, `font_size`, `user_id`, `added_date`) 
						VALUES (:sid, :bg, :bg_tr, :obg, :obg_tr, :ohover, :oh_tr, :oselected, :oselected_tr, :btn_bg, :btn_bg_tr, :btn_hover, :btn_hover_tr, :btn_selected, :btn_selected_tr, :font_type, :font_color, :font_size, :user_id, :added_date)";
			$com_stmt = $db->prepare($com_sql);
			$com_stmt->execute($brand_data);
		endif;
		
		//-------START-OPEN-RESPONSE-------
		if ($question_type == 9):
			#-------QUESTIONS-DATA---
			$qaudio = '';
			if ( ! empty($mcqq_qaudio)):
				$qaudio = $mcqq_qaudio;
			elseif ( ! empty($mcqq_rec_audio)):
				$qaudio = $mcqq_rec_audio;
			endif;
			
			$audio = '';
			if ( ! empty($mcq_audio)):
				$audio = $mcq_audio;
			elseif ( ! empty($mcq_rec_audio)):
				$audio = $mcq_rec_audio;
			endif;
			
			$video = '';
			if ( ! empty($mcq_video)):
				$video = $mcq_video;
			elseif ( ! empty($mcq_rec_video)):
				$video = $mcq_rec_video;
			endif;
			
			$quesData = array('questions'		=> ( ! empty($mcq)) ? htmlspecialchars($mcq) : '',
							  'qtype'			=> ( ! empty($question_type)) ? $question_type : '',
							  'qaudio'			=> ( ! empty($qaudio)) ? $qaudio : '',
							  'qspeech_text'	=> ( ! empty($mcqq_text_to_speech)) ? htmlspecialchars($mcqq_text_to_speech) : '',
							  'speech_text'		=> ( ! empty($mcq_text_to_speech)) ? htmlspecialchars($mcq_text_to_speech) : '',
							  'audio'			=> ( ! empty($audio)) ? $audio : '',
							  'video'			=> ( ! empty($video)) ? $video : '',
							  'screen'			=> ( ! empty($mcq_rec_screen)) ? $mcq_rec_screen : '',
							  'image'			=> ( ! empty($mcq_img)) ? $mcq_img : '',
							  'document'		=> ( ! empty($mcq_doc)) ? $mcq_doc : '',
							  'ques_val_1'		=> ( ! empty($mcq_ques_val_1)) ? $mcq_ques_val_1 : '',
							  'ques_val_2'		=> ( ! empty($mcq_ques_val_2)) ? $mcq_ques_val_2 : '',
							  'ques_val_3'		=> ( ! empty($mcq_ques_val_3)) ? $mcq_ques_val_3 : '',
							  'ques_val_4'		=> ( ! empty($mcq_ques_val_4)) ? $mcq_ques_val_4 : '',
							  'ques_val_5'		=> ( ! empty($mcq_ques_val_5)) ? $mcq_ques_val_5 : '',
							  'ques_val_6'		=> ( ! empty($mcq_ques_val_6)) ? $mcq_ques_val_6 : '',
							  'critical'		=> ( ! empty($mcq_criticalQ)) ? $mcq_criticalQ : '',
							  'status' 			=> 1,
							  'user_id'			=> $userId,
							  'cur_date'		=> $curdate,
							  'qid'				=> $ques_id);
			$ques_sql  = "UPDATE `question_tbl` SET `questions`=:questions, `question_type`=:qtype, `qaudio`=:qaudio, `qspeech_text`=:qspeech_text, `speech_text`=:speech_text, `audio`=:audio, `video`=:video, `screen`=:screen, `image`=:image, `document`=:document, `ques_val_1`=:ques_val_1, `ques_val_2`=:ques_val_2, `ques_val_3`=:ques_val_3, `ques_val_4`=:ques_val_4, `ques_val_5`=:ques_val_5, `ques_val_6`=:ques_val_6, `critical`=:critical, `status`=:status, `uid`=:user_id, `cur_date`=:cur_date WHERE question_id=:qid";
			$ques_stmt = $db->prepare($ques_sql);
			$ques_stmt->execute($quesData);
		endif;
		//-----------END-OPEN-RESPONSE-------
	endif;
	if ( ! empty($submit_type) && $submit_type == 1):
		$resdata = array('success' => TRUE, 'all_update' => FALSE, 'msg' => 'Simulation details updated successfully.', 'return_url' => 'open-response-template.php?add_data=true&sim_id='.md5($sim_id).'&ques_id='.base64_encode($ques_id));
	else:
		$resdata = array('success' => TRUE, 'all_update' => TRUE, 'msg' => 'All questions and answers updated successfully.', 'return_url' => 'author-simulation.php');
	endif;
	echo json_encode($resdata);
endif;

#-----------------------------------------Add-Score-Open-Response-Sim ----------------------------------
if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['add_score_open_response_sim']) && isset($_POST['sim_id'])):
	extract($_POST);
	if (empty($learner)):
		$resdata = array('success' => FALSE, 'msg' => 'Please select learner.');
	elseif ( ! empty($learner)):
		if (isset($uid) && ! empty($uid)):
		 	$attUID = $uid;
		else:
			$attData = $db->getLastAttemptOpenResp($sim_id, $learner);
			$attUID = ( ! empty($attData)) ? $attData['uid'] : '';
		endif;
		if ( empty($attUID)):
			$resdata = array('success' => FALSE, 'msg' => 'Learner data not found.');
			echo json_encode($resdata); exit;
		endif;
		if ( ! empty($open_res_id)):
			$postData = array('ans_val1'	=> ( ! empty($mcq_ans_val1)) ? $mcq_ans_val1 : '',
							  'ans_val2'	=> ( ! empty($mcq_ans_val2)) ? $mcq_ans_val2 : '',
							  'ans_val3'	=> ( ! empty($mcq_ans_val3)) ? $mcq_ans_val3 : '',
							  'ans_val4'	=> ( ! empty($mcq_ans_val4)) ? $mcq_ans_val4 : '',
							  'ans_val5'	=> ( ! empty($mcq_ans_val5)) ? $mcq_ans_val5 : '',
							  'ans_val6'	=> ( ! empty($mcq_ans_val6)) ? $mcq_ans_val6 : '',
							  'tts_data'	=> ( ! empty($tts_data_1)) ? htmlspecialchars($tts_data_1) : '',
							  'file_name'	=> ( ! empty($file_name_1)) ? $file_name_1 : '',
							  'resid'		=> $open_res_id);
			$sql  = "UPDATE `open_res_tbl` SET `ans_val1`=:ans_val1, `ans_val2`=:ans_val2, `ans_val3`=:ans_val3, `ans_val4`=:ans_val4, `ans_val5`=:ans_val5, `ans_val6`=:ans_val6, `tts_data_feedback`=:tts_data, `file_name_feedback`=:file_name WHERE open_res_id=:resid";
			$stmt = $db->prepare($sql);
			$stmt->execute($postData);
			if ( ! empty($submit_type) && $submit_type == 1):
				$resdata = array('success' => TRUE, 'all_update' => FALSE, 'msg' => 'Score added successfully.', 'return_url' => 'report-open-response-template.php?add_data=true&sim_id='. md5($sim_id) .'&user_id='. base64_encode($learner) .'&uid='. $attUID .'&ques_id='. base64_encode($ques_id));
			else:
				$resdata = array('success' => TRUE, 'all_update' => TRUE, 'msg' => 'Simulation updated successfully.', 'return_url' => 'login.php');
			endif;
			echo json_encode($resdata); exit;
		endif;
		if (empty($open_res_id) && ! empty($submit_type) && $submit_type == 1):
			$resdata = array('success' => TRUE, 'all_update' => FALSE, 'msg' => 'Processing....', 'return_url' => 'report-open-response-template.php?add_data=true&sim_id='. md5($sim_id) .'&user_id='. base64_encode($learner) .'&uid='. $attUID .'&ques_id='. base64_encode($ques_id));
		else:
			$resdata = array('success' => TRUE, 'all_update' => TRUE, 'msg' => 'Simulation updated successfully.', 'return_url' => 'login.php');
		endif;
	endif;
	echo json_encode($resdata); exit;
endif;

#------------------------------------------------Update Sim Stage----------------------------------------
if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['scenario_stage_update']) && ! empty($_POST['scenario_id'])):
	$stage	= (isset($_POST['scenario_status'])) ? $_POST['scenario_status'] : '';
	$sql	= "UPDATE scenario_master SET status = '". $stage ."' WHERE MD5(scenario_id) = '". $_POST['scenario_id'] ."'";
    $results = $db->prepare($sql);
	if ($results->execute()):
		$resdata = array('success' => TRUE, 'msg' => 'Simulation stage update successfully.');
	else:
		$resdata = array('success' => FALSE, 'msg' => 'Simulation stage not update. try again later.');
	endif;
	echo json_encode($resdata);
endif;

#--------------------------------------Update Sim Char Background----------------------------------------
if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['add_char_background']) && ! empty($_POST['add_char_background']) && isset($_FILES)):
	$ad_img = (isset($_FILES['file_image'])) ? $_FILES['file_image'] : '';
	$handle = new Upload($ad_img);
	$error  = FALSE;
	if ( ! empty($ad_img) && ! in_array(strtolower(pathinfo($ad_img['name'], PATHINFO_EXTENSION)), $img_valid_exts)):
		$resdata = array('success' => FALSE, 'msg' => 'Please select valid file. accept only jpeg, jpg, png, svg');
		$error   = TRUE;
	elseif ($error == FALSE && ! empty($ad_img) && $ad_img['size'] > 2097152):
		$resdata = array('success' => FALSE, 'msg' => 'File size must be less than 2 MB.');
		$error	 = TRUE;
	elseif ($error == FALSE && $handle->uploaded):
		if ($_POST['type'] == 1):
		$handle->image_resize		= TRUE;
		$handle->image_x        	= 1920;
		$handle->image_y        	= 1080;
		endif;
		$handle->file_safe_name 	= TRUE;
		$handle->file_overwrite 	= FALSE;
		$handle->file_auto_rename	= TRUE;
		$handle->Process($char_bg_path);
		if ($handle->processed) {
			$img_name = $handle->file_dst_name;
		}
		$handle->Clean();
		$data = array('img_name'	=> ( ! empty($img_name)) ? $img_name : '',
					  'des'			=> ( ! empty($_POST['description'])) ? $_POST['description'] : '',
					  'type'		=> $_POST['type'],
					  'uid'			=> $userId,
					  'status'		=> 1,
					  'add_date'	=> $curdate);
		$q = "INSERT INTO `upload_tbl` (`img_name`, `des`, `type`, `uid`, `status`, `add_date`) VALUES (:img_name, :des, :type, :uid, :status, :add_date)";
		$stmt = $db->prepare($q);
		if ($stmt->execute($data)):
			$resdata = array('success' => TRUE, 'msg' => 'File upload successfully.');
		else:
			$resdata = array('success' => TRUE, 'msg' => 'File not upload. Please try again later.');
		endif;
	else:
		$resdata = array('success' => FALSE, 'msg' => 'Oops. something went wrong please try again.?');
	endif;
	echo json_encode($resdata);
endif;

#-------------------------------------------Delete Upload File----------------------------------------
if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['delete']) && ! empty($_POST['upload_id']) && ! empty($_POST['file'])):
	extract($_POST);
	$q = $db->prepare("DELETE FROM upload_tbl WHERE upload_id = ?");
	if ($q->execute(array($upload_id))):
		@unlink($char_bg_path . $file);
		$resdata = array('success' => TRUE, 'msg' => 'File deleted successfully.');
	else:
		$resdata = array('success' => TRUE, 'msg' => 'File not deleted. Please try again later.');
	endif;
	echo json_encode($resdata);
endif;

#------------------------------------------------Update-Profile-Page----------------------------------------
if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['update_profile']) && ! empty($_POST['update_profile_id'])):
	extract($_POST);
	$img 	= '';
	$error	= FALSE;
	if (empty($fname) || empty($email) || empty($lname) || empty($organization) || empty($department)):
		$resdata = array('success' => FALSE, 'msg' => 'Please enter all required fields.');
		$error = TRUE;
	elseif ($error == FALSE):
		if ( ! empty($proImgName)):
			$img = $proImgName;
			@unlink($uploadProfilePath . $old_profile_img);
			@unlink($uploadProfilePath . $old_profile_img1);
			@unlink($uploadProfilePath . $old_profile_img2);
		elseif ( empty($proImgName) && ! empty($old_profile_img)):
			$img = $old_profile_img;
			$proImgName1 = $old_profile_img1;
			$proImgName2 = $old_profile_img2;
		endif;
		$sql = "UPDATE users SET fname = '". $fname ."', lname = '". $lname ."', email = '". $email ."', mob = '". $mob ."', location = '". $location ."', company = '". $organization ."', department = '". $department ."', webpage = '". $webpage ."', facebook = '". $facebook ."', twitter = '". $twitter ."', linkedIn = '". $linkedIn ."', img = '". $img ."', img_thumb_50 = '". $proImgName1 ."', img_thumb_125 = '". $proImgName2 ."', modified_date = CURDATE() WHERE id = '". $update_profile_id ."'";
    	$results = $db->prepare($sql);
		if ($results->execute()):
			$resdata = array('success' => TRUE, 'msg' => 'Profile update successfully.', 'return_url' => 'login.php');
		else:
			$resdata = array('success' => FALSE, 'msg' => 'Profile not update. try again later.');
		endif;
	endif;
	echo json_encode($resdata);
endif;

#---------------------------------------------Update-User----------------------------------------
if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['update_user_profile']) && ! empty($_POST['update_user_id'])):
	extract($_POST);
	$error = FALSE;
	if (empty($edit_role) || empty($username)):
		$resdata = array('success' => FALSE, 'msg' => 'Please enter user name and role.');
		$error 	 = TRUE;
	elseif ( ! empty($npwd) && ! empty($rpwd) && md5($npwd) != md5($rpwd)):
		$resdata = array('success' => FALSE, 'msg' => 'Password and confirm password not match.');
		$error   = TRUE;
	elseif ($error == FALSE):
		$sql = "UPDATE users SET fname = '". $fname ."', lname = '". $lname ."', email = '". $email ."', mob = '". $mob ."', location = '". $location ."', company = '". $organization ."', department = '". $department ."', role = '". $edit_role ."', status = '". $status ."' ".((isset($npwd) && ! empty($npwd)) ? ", password = '". md5($npwd) ."'" : " ").", modified_date = '". date('Y-m-d') ."' WHERE id = '". $update_user_id ."'";
    	$results = $db->prepare($sql);
		if ($results->execute()):
			$resdata = array('success' => TRUE, 'msg' => 'Profile update successfully.', 'return_url' => NULL);
		else:
			$resdata = array('success' => FALSE, 'msg' => 'Profile not update. try again later.');
		endif;
	endif;
	echo json_encode($resdata);
endif;

#----------------------------User-Registration------------------------
if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['user_regis'])):
	$image	= new Securimage();
	$error = FALSE;
	extract($_POST);	
	if (empty($username) || empty($password_1) || empty($password_2) || empty($email) || empty($captcha_code)):
		$resdata = array('success' => FALSE, 'msg' => 'Please enter all required fields.');
		$error = TRUE;
	elseif ( ! empty($username) && $db->checkUserNameExist(trim($username)) == TRUE):
		$resdata = array('success' => FALSE, 'msg' => 'This username already exists. try another.');
		$error = TRUE;
	elseif ( ! empty($password_1) && ! empty($password_2) && md5($password_1) != md5($password_2)):
		$resdata = array('success' => FALSE, 'msg' => 'Password and confirm password not match.');
		$error = TRUE;
	elseif ( ! empty($email) && $db->checkEmail($email) == FALSE):
		$resdata = array('success' => FALSE, 'msg' => 'Please enter valid email-id.');
		$error = TRUE;
	elseif ($error == FALSE && $image->check($captcha_code) == TRUE):
		$userData = array('uname'	=> trim($username),
						  'email'	=> $email,
						  'pwd'		=> md5($password_1),
						  'status'	=> NULL,
						  'estatus'	=> 1,
						  'pwd_date'=> date('Y-m-d'),
						  'date'	=> $curdate);
		$sql = "INSERT INTO `users` (`username`, `email`, `password`, `status`, `email_status`, `last_change_pwd`, `date`) 
				VALUES (:uname, :email, :pwd, :status, :estatus, :pwd_date, :date)";
		$results = $db->prepare($sql);
		if ($results->execute($userData)):
			$getTempData = $db->getEmailTemplate(NULL, 'new_user');
			$template	 = $getTempData['tmp_des'];
			$subject	 = $getTempData['subject'];
			$values	   	 = array('full_name'	=> $username,
								 'name'			=> $username,
								 'pwd'			=> $password_1,
								 'logo'			=> TEMPLATE_HEADER_LOGO,
								 'url'			=> LOGIN_URL,
								 'cmail'		=> CONTACT_EMAIL,
								 'tname'		=> FTITLE,
								 'subject'		=> $subject);
			sendMail($email, $template, $values, $subject);
			$resdata = array('success' => TRUE, 'msg' => 'User registered successfully.', 'return_url' => 'index.php');
		else:
			$resdata = array('success' => TRUE, 'msg' => 'User not registered please. try again later.');
		endif;
	else:
		$resdata = array('success' => FALSE, 'msg' => 'Incorrect captcha please try again.');
	endif;
	echo json_encode($resdata);
endif;

#----------------------------Forget password----------------------------
if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['Forget_pwd'])):
	extract($_POST);
	$error = FALSE;
	if (empty($username)):
		$resdata = array('success' => FALSE, 'msg' => 'Username cannot be blank.');
		$error = TRUE;
	elseif ( ! empty($username) && $db->checkUserNameExist(trim($username)) == FALSE):
		$resdata = array('success' => FALSE, 'msg' => 'Username does not exist in the system.');
		$error = TRUE;
	elseif ($error == FALSE):
		$getTempData = $db->getEmailTemplate(NULL, 'reset-password');
		$template	 = $getTempData['tmp_des'];
		$subject	 = $getTempData['subject'];
		$revData	 = $db->checkUserNameExist(trim($username), TRUE);
		if ( ! empty($revData['email']) && $db->checkEmail($revData['email']) == TRUE):
			$npwd 	  	 = 'ES-'. $db->get_random_string(6);
			$updatePwd	 = $db->updatePassword($npwd, $revData['id'], $revData['role']);
			$values	   	 = array('name'		=> $revData['username'],
								 'pwd'		=> $npwd,
								 'logo'		=> TEMPLATE_HEADER_LOGO,
								 'url'		=> LOGIN_URL,
								 'cmail'	=> CONTACT_EMAIL,
								 'tname'	=> FTITLE,
								 'subject'	=> $subject);
			if (sendMail($revData['email'], $template, $values, $subject)):
				$resdata = array('success' => TRUE, 'msg' => 'Your new password has been sent to your email address.', 'return_url' => 'login.php');
			else:
				$resdata = array('success' => FALSE, 'msg' => 'Your Password not changed!. try again later.');
			endif;
		else:
			$resdata = array('success' => FALSE, 'msg' => 'Your Password not changed!. try again later.');
		endif;
	else:
		$resdata = array('success' => FALSE, 'msg' => 'Oops. something went wrong please try again.?');
	endif;
	echo json_encode($resdata);
endif;

//----------------------------------------Change-Password-------------------------------------------------
if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['change_pwd']) && isset($_POST['npwd']) && isset($_POST['rpwd'])):
	extract($_POST);
	$error = FALSE;
	if ( ! empty($npwd) && ! empty($rpwd) && md5($npwd) != md5($rpwd)):
		$resdata = array('success' => FALSE, 'msg' => 'Password and confirm password not match.');
		$error = TRUE;
	elseif ($error == FALSE && ! empty($userId)):
		$sql = "UPDATE users SET `password` = '". md5($npwd) ."', `last_change_pwd` = '". date('Y-m-d') ."' WHERE id = '". $userId ."'";
		$results = $db->prepare($sql);
		if ($results->execute()):
			session_destroy(); //clears everything for the current user
			unset($_SESSION['username']);
			unset($_SESSION['role']);
			unset($_SESSION['userId']);
			$resdata = array('success' => TRUE, 'msg' => 'Password has been successfully changed. Please login again.', 'return_url' => 'login');
		else:
			$resdata = array('success' => FALSE, 'msg' => 'Password not changed. Please try again.');
		endif;
	endif;
	echo json_encode($resdata);
endif;

#---------------------------------------------Delete-Option-------------------------------
if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['delete_answer']) && ! empty($_POST['ans_id'])):
	$q = $db->prepare("DELETE FROM answer_tbl WHERE answer_id = '". $_POST['ans_id'] ."'");
	if ($q->execute()):
		$resdata = array('success' => TRUE, 'msg' => 'Option deleted successfully.');
	else:
		$resdata = array('success' => FALSE, 'msg' => 'Option not deleted. Please try again later.');
	endif;
	echo json_encode($resdata);
endif;

#-----------------------------------------Delete-Sorting-Option-------------------------------
if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['delete_sorting_answer']) && ! empty($_POST['ans_id'])):
	$sql = "DELETE t1.*, t2.* FROM answer_tbl AS t1 LEFT JOIN sub_options_tbl AS t2 ON t1.answer_id = t2.answer_id WHERE t1.answer_id = '". $_POST['ans_id'] ."'";
	$exe = $db->prepare($sql);
	if ($exe->execute()):
		$resdata = array('success' => TRUE, 'msg' => 'Option deleted successfully.');
	else:
		$resdata = array('success' => FALSE, 'msg' => 'Option not deleted. Please try again later.');
	endif;
	echo json_encode($resdata);
endif;

#-------------------------------------Delete-Sorting-Sub-Answer-------------------------------
if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['delete_sorting_sub_answer']) && ! empty($_POST['ans_id'])):
	$q = $db->prepare("DELETE FROM sub_options_tbl WHERE sub_options_id = '". $_POST['ans_id'] ."'");
	if ($q->execute()):
		$resdata = array('success' => TRUE, 'msg' => 'Option deleted successfully.');
	else:
		$resdata = array('success' => FALSE, 'msg' => 'Option not deleted. Please try again later.');
	endif;
	echo json_encode($resdata);
endif;

#---------------------------------------Delete-classic-temp-Option-------------------------------
if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['delete_classic_answer']) && ! empty($_POST['ans_id'])):
	$sql = "DELETE t1.*, t2.* FROM answer_tbl AS t1 LEFT JOIN feedback_tbl AS t2 ON t1.answer_id = t2.answer_id WHERE t1.answer_id = '". $_POST['ans_id'] ."'";
	$exe = $db->prepare($sql);
	if ($exe->execute()):
		$resdata = array('success' => TRUE, 'msg' => 'Option deleted successfully.');
	else:
		$resdata = array('success' => FALSE, 'msg' => 'Option not deleted. Please try again later.');
	endif;
	echo json_encode($resdata);
endif;

#--------------------------------------------Add-Comment--------------------------------------------
if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['comment_add']) && ! empty($_POST['comment'])):
	extract($_POST);
	$data = array('comment_id'	=> (isset($comment_id)) ? $comment_id : '',
				  'simid'		=> $scenario_id,
				  'user_id'		=> ( ! empty($userId)) ? $userId : $uid,
				  'comment'		=> strip_tags($comment),
				  'type' 		=> $chat_type,
				  'cdate'		=> $curdate);
	$sql = "INSERT INTO `comments_tbl` (`parent_comment_id`, `scenario_id`, `user_id`, `comment`, `type`, `cdate`) VALUES (:comment_id, :simid, :user_id, :comment, :type, :cdate)";
	$stmt = $db->prepare($sql);
	if ($stmt->execute($data)) echo TRUE;
endif;

#--------------------------------------------Show-Comment--------------------------------------------
if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['simID']) && isset($_POST['getComments']) && isset($_POST['getType'])):
	$simID	= intval($_POST['simID']);
	$typeID	= intval($_POST['getType']);
	$statement = $db->prepare("SELECT c.comment_id, c.parent_comment_id, c.scenario_id, c.user_id, c.comment, c.type, DATE_FORMAT(c.cdate, '%d-%m-%y, %h:%i %p') AS datetime, u.username, u.fname, u.lname, u.img_thumb_50 as img FROM comments_tbl c LEFT JOIN users u ON c.user_id = u.id WHERE c.scenario_id = '". $simID ."' AND c.type = '". $typeID ."' ORDER BY c.parent_comment_id asc, c.comment_id asc");
	$statement->execute();
	$record_set = array();
	if ($statement->rowCount() > 0):
		while ($row = $statement->fetch(PDO::FETCH_ASSOC)):
			array_push($record_set, 
				array('comment_id'			=> $row['comment_id'],
					  'parent_comment_id'	=> $row['parent_comment_id'],
					  'scenario_id' 		=> $row['scenario_id'],
					  'user_id' 			=> $row['user_id'],
					  'comment' 			=> $row['comment'],
					  'type' 				=> $row['type'],
					  'datetime' 			=> $row['datetime'],
					  'username' 			=> ($row['username'] == $user) ? 'You' : $row['username'],
					  'fname' 				=> ($row['username'] == $user) ? 'You' : $row['fname'],
					  'lname' 				=> $row['lname'],
					  'img' 				=> $row['img']));
		endwhile;
	endif;
	echo json_encode(array('curUser' => "You", 'cdate' => date('d-m-y'), 'resData' => $record_set));
endif;

#------------------------------------Delete-Comment------------------------
if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['remove_chat']) && ! empty($_POST['commentID'])):
	extract($_POST);
	if ( ! empty($commentID) && empty($parentCommentID)):
		$sql  = "DELETE FROM comments_tbl WHERE comment_id = '". $commentID ."'";
		$sqlp = "DELETE FROM comments_tbl WHERE parent_comment_id = '". $commentID ."'";
		$p = $db->prepare($sqlp); $p->execute();
	elseif ( ! empty($commentID) && ! empty($parentCommentID)):
		$sql = "DELETE FROM comments_tbl WHERE comment_id = '". $commentID ."' AND parent_comment_id = '". $parentCommentID ."'";
	endif;
	$q = $db->prepare($sql);
	if ($q->execute()):
		$resdata = array('success' => TRUE, 'msg' => 'Comment deleted successfully.');
	else:
		$resdata = array('success' => FALSE, 'msg' => 'Comment not deleted. Please try again later.');
	endif;
	echo json_encode($resdata);
endif;

#---------------------------------NOTIFY-AUTHOR-OR-REVIEWER-------------------------------------------
if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['notify']) && ! empty($_POST['notify_sim_id'])):
	extract($_POST);
	$error = FALSE;
	if (empty($notify_sim_id)):
		$resdata = array('success' => FALSE, 'msg' => 'Simulation id cannot be blank.');
		$error = TRUE;
	elseif ($error == FALSE):
		$columns  = ($type == 'author') ? 'assign_author' : 'assign_reviewer';
		$updateId = ($type == 'author') ? 18 : 17;
		$q = $db->prepare("SELECT Scenario_title, $columns FROM scenario_master WHERE scenario_id = '". $notify_sim_id ."'"); $q->execute();
		if ($q->rowCount() > 0):
			$data = $q->fetch(PDO::FETCH_ASSOC);
			if ( ! empty($data[$columns])):
				$assign_id	= $data[$columns];
				$getUsers	= $db->getUsersColumns($assign_id, 'id, username, fname, lname, email');
				if (count($getUsers) > 0):
					#----Update-Stage----
					$db->updateStatus($updateId, md5($notify_sim_id));
					#----SEND-MAIL----
					$temp_type	 = ($type == 'author') ? 'notify_author' : 'Reviewer';
					$tempData	 = $db->getEmailTemplate(NULL, $temp_type);
					$template	 = $tempData['tmp_des'];
					$subject	 = $tempData['subject'];
					foreach ($getUsers as $udata):
						if ( ! empty($udata['email']) && $db->checkEmail($udata['email']) == TRUE):
							$values = array('name'		=> ( ! empty($udata['fname'])) ? $udata['fname'].' '.$udata['lname'] : $udata['username'],
											'simname'	=> $data['Scenario_title'],
											'logo'		=> TEMPLATE_HEADER_LOGO,
											'url'		=> base64_decode($url),
											'cmail'		=> CONTACT_EMAIL,
											'tname'		=> FTITLE,
											'subject'	=> $subject);
							sendMail($udata['email'], $template, $values, $subject);
						endif;
					endforeach;
					$resdata = array('success' => TRUE, 'msg' => 'Notified Successfully.');
				endif;
			else:
				$resdata = array('success' => FALSE, 'msg' => 'User not found.');
			endif;
		endif;
	else:
		$resdata = array('success' => FALSE, 'msg' => 'Oops. something went wrong please try again.?');
	endif;
	echo json_encode($resdata);
endif;

#------------------------------------Delete Multiple Users------------------------
if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['multiple_user_type']) && ! empty($_POST['id'])):
	extract($_POST);
	if ($multiple_user_type == 1 && ! empty($id)):
		$getuid = $db->addMultiIds($id);
		$total_user = count($id);
		$sql  = "DELETE t1.*, t2.*, t3.*, t4.*, t5 ";
		$sql .= "FROM users t1 
				 LEFT JOIN scenario_attempt_tbl t2 ON t1.id = t2.userid 
				 LEFT JOIN score_tbl t3 ON t2.uid = t3.uid 
				 LEFT JOIN open_res_tbl t4 ON t2.uid = t4.uid 
				 LEFT JOIN multi_score_tbl t5 ON t2.uid = t5.uid 
				 WHERE t1.id IN (". $getuid .")";
		$q = $db->prepare($sql);
		if ($q->execute()):
			$resdata = array('success' => TRUE, 'msg' => "$total_user User with related to all data deleted successfully!");
		else:
			$resdata = array('success' => FALSE, 'msg' => 'User not deleted. Please try again later!');
		endif;
	endif;
	echo json_encode($resdata);
endif;

#------------------------------------Send Mail Multiple Users------------------------
if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['send_mail_Multiple_Users']) && ! empty($_POST['id'])):
	extract($_POST);
	$getuid  	= $db->addMultiIds($id);
	$columns 	= "id, username, concat(fname, ' ', lname) as fullname, email";
	$userData	= $db->getUsersColumns($getuid, $columns);
	foreach ($userData as $udata):
		if ( ! empty($udata['email']) && $db->checkEmail($udata['email']) == TRUE):
			$npwd = 'ES#'. $db->get_random_string(8);
			$sql = "UPDATE users SET `password` = '". md5($npwd) ."', `email_status` = '1' WHERE id = '". $udata['id'] ."'";
			$q = $db->prepare($sql); $q->execute();
			$getTempData = $db->getEmailTemplate(NULL, 'new_user');
			$template	 = $getTempData['tmp_des'];
			$subject	 = $getTempData['subject'];
			$values	   	 = array('full_name'	=> $udata['fullname'],
								 'name'			=> $udata['username'],
								 'pwd'			=> $npwd,
								 'logo'			=> TEMPLATE_HEADER_LOGO,
								 'url'			=> LOGIN_URL,
								 'cmail'		=> CONTACT_EMAIL,
								 'tname'		=> FTITLE,
								 'subject'		=> $subject);
			sendMail($udata['email'], $template, $values, $subject);
			$npwd = '';
		endif;
	endforeach;
	if ($q):
		$resdata = array('success' => TRUE, 'msg' => "New account mail sent successfully.");
	else:
		$resdata = array('success' => FALSE, 'msg' => 'New account mail not sent. Please try again later.');
	endif;
	echo json_encode($resdata);
endif;

#------------------------------------Send Mail Group learner------------------------
if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['send_group_learner_mail']) && ! empty($_POST['group_id'])):
	extract($_POST);
	$gdata	 = $db->getGroup($group_id);
	$learner = $gdata['learner'];
	if ( ! empty($learner)):
		$columns  = "id, username, concat(fname, ' ', lname) as fullname, email";
		$userData = $db->getUsersColumns($learner, $columns);
		foreach ($userData as $udata):
			if ( ! empty($udata['email']) && $db->checkEmail($udata['email']) == TRUE):
				$npwd = 'ES#'. $db->get_random_string(8);
				$sql = "UPDATE users SET `password` = '". md5($npwd) ."', `email_status` = '1' WHERE id = '". $udata['id'] ."'";
				$q = $db->prepare($sql); $q->execute();
				$getTempData = $db->getEmailTemplate(NULL, 'new_user');
				$template	 = $getTempData['tmp_des'];
				$subject	 = $getTempData['subject'];
				$values	   	 = array('full_name'	=> $udata['fullname'],
									 'name'			=> $udata['username'],
									 'pwd'			=> $npwd,
									 'logo'			=> TEMPLATE_HEADER_LOGO,
									 'url'			=> LOGIN_URL,
									 'cmail'		=> CONTACT_EMAIL,
									 'tname'		=> FTITLE,
									 'subject'		=> $subject);
				sendMail($udata['email'], $template, $values, $subject);
				$npwd = '';
			endif;
		endforeach;
		if ($q):
			$resdata = array('success' => TRUE, 'msg' => 'New account mail sent successfully.');
		else:
			$resdata = array('success' => FALSE, 'msg' => 'New account mail not sent. Please try again later.');
		endif;
	else:
		$resdata = array('success' => FALSE, 'msg' => 'User not found. Please try again later.');
	endif;
	echo json_encode($resdata);
endif;

#-----------------------------------------Add-New-Questions--------------------------------------
if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['add_Question']) && ! empty($_POST['sim_id'])):
	$sim_id = ( ! empty($_POST['sim_id'])) ? $_POST['sim_id'] : exit;
	$tq = $db->getTotalQuestionsByScenario(md5($sim_id)) + 1;
	$quesData = array('sid'			=> $sim_id,
					  'questions'	=> 'Question '. $tq,
					  'status' 		=> 1,
					  'user_id'		=> $userId,
					  'cur_date'	=> $curdate);
	$ques_sql = "INSERT INTO `question_tbl` (`scenario_id`, `questions`, `status`, `uid`, `cur_date`) VALUES (:sid, :questions, :status, :user_id, :cur_date)";
	$ques_stmt = $db->prepare($ques_sql);
	if ($ques_stmt->execute($quesData)):
		$resdata = array('success' => TRUE, 'msg' => 'Question created successfully.');
	else:
		$resdata = array('success' => FALSE, 'msg' => 'Question not created. Please try again later.');
	endif;
	echo json_encode($resdata);
endif;

#---------------------------------------Get-Questions-List-------------------------------------
if ($_SERVER['REQUEST_METHOD'] == 'GET' && isset($_GET['getQuesList']) && isset($_GET['sim_id'])):
	$sim_id = (isset($_GET['sim_id'])) ? $_GET['sim_id'] : '';
	if ( ! empty($sim_id)):
		$data = $db->getQuestionList($sim_id);
		if ( ! empty($data)):
			$res = array('success' => TRUE, 'data' => $data);
		else:
			$res = array('success' => FALSE, 'data' => NULL);
		endif;
	else:
		$res = array('success' => FALSE, 'data' => NULL);
	endif;
	echo json_encode($res);
endif;

#----------------------------Get-Video-Temp-Cue-Point-List-------------------------------------
if ($_SERVER['REQUEST_METHOD'] == 'GET' && isset($_GET['getCuePointList']) && isset($_GET['sim_id'])):
	$sim_id = (isset($_GET['sim_id'])) ? $_GET['sim_id'] : '';
	if ( ! empty($sim_id)):
		$data = $db->getCuePointList($sim_id);
		if ( ! empty($data)):
			$res = array('success' => TRUE, 'data' => $data);
		else:
			$res = array('success' => FALSE, 'data' => NULL);
		endif;
	else:
		$res = array('success' => FALSE, 'data' => NULL);
	endif;
	echo json_encode($res);
endif;

#---------------------------------------Clone-Questions-and-Answers----------------------------
if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['clone_Question']) && isset($_POST['ques_id'])):
	$ques_id = (isset($_POST['ques_id'])) ? $_POST['ques_id'] : '';
	if(is_array($ques_id)):
		if (count($ques_id) > 1):
			$res = array('success' => FALSE, 'msg' => 'Please select only one question.');
			echo json_encode($res); exit;
		endif;
	elseif ( ! empty($ques_id)):
		#----Question-Data----
		$ques 			= $db->getQuestionsByQuesId(md5($ques_id))[0];
		$sim_id			= $ques['scenario_id'];
		$question_id	= $ques['question_id'];
		$question_type	= $ques['question_type'];
		#----SIM Data----
		$sim_data		= $db->getScenario($sim_id);
		$sim_ques_type	= $sim_data['sim_ques_type'];
		$sim_temp_type	= $sim_data['sim_temp_type'];
		$random_st_len	= 10;
		$clone_qqaudio  = $clone_qaudio = $clone_qvideo = $clone_qscreen = $clone_qimage = $clone_qdoc = $clone_videoq_file = '';
		#----copy-question-audio----
		if ( ! empty($ques['qaudio']) && file_exists($root_path . $ques['qaudio'])):
			$clone_qqaudio = 'copy-' . $db->get_random_string($random_st_len) . $ques['qaudio'];
			@copy($uploadpath . $ques['qaudio'], $uploadpath . $clone_qqaudio);
		endif;
		
		#----copy-question--audio----
		if ( ! empty($ques['audio']) && file_exists($root_path . $ques['audio'])):
			$clone_qaudio = 'copy-' . $db->get_random_string($random_st_len) . $ques['audio'];
			@copy($uploadpath . $ques['audio'], $uploadpath . $clone_audio);
		endif;

		#----copy-question--video----
		if ( ! empty($ques['video']) && file_exists($root_path . $ques['video'])):
			$clone_qvideo = 'copy-' . $db->get_random_string($random_st_len) . $ques['video'];
			@copy($uploadpath . $ques['video'], $uploadpath . $clone_qvideo);
		endif;

		#----copy-question--screen----
		if ( ! empty($ques['screen']) && file_exists($root_path . $ques['screen'])):
			$clone_qscreen = 'copy-' . $db->get_random_string($random_st_len) . $ques['screen'];
			@copy($uploadpath . $ques['screen'], $uploadpath . $clone_qscreen);
		endif;

		#----copy-question--image----
		if ( ! empty($ques['image']) && file_exists($root_path . $ques['image'])):
			$clone_qimage = 'copy-' . $db->get_random_string($random_st_len) . $ques['image'];
			@copy($uploadpath . $ques['image'], $uploadpath . $clone_qimage);
		endif;

		#----copy-question--document----
		if ( ! empty($ques['document']) && file_exists($root_path . $ques['document'])):
			$clone_qdoc = 'copy-' . $db->get_random_string($random_st_len) . $ques['document'];
			@copy($uploadpath . $ques['document'], $uploadpath . $clone_qdoc);
		endif;

		#----copy-video-sim-question----
		if ( ! empty($ques['videoq_media_file']) && file_exists($root_path . $ques['videoq_media_file'])):
			$clone_videoq_file = 'copy-' . $db->get_random_string($random_st_len) . $ques['videoq_media_file'];
			@copy($uploadpath . $ques['videoq_media_file'], $uploadpath . $clone_videoq_file);
		endif;

		$quesData = array('sid'					=> $sim_id,
						  'qtype'				=> $question_type,
						  'questions'			=> $ques['questions'],
						  'qaudio'				=> ( ! empty($clone_qqaudio)) ? $clone_qqaudio : '',
						  'qspeech_text'		=> $ques['qspeech_text'],
						  'speech_text'			=> $ques['speech_text'],
						  'audio'				=> ( ! empty($clone_qaudio)) ? $clone_qaudio : '',
						  'video'				=> ( ! empty($clone_qvideo)) ? $clone_qvideo : '',
						  'screen'				=> ( ! empty($clone_qscreen)) ? $clone_qscreen : '',
						  'image'				=> ( ! empty($clone_qimage)) ? $clone_qimage : '',
						  'document'			=> ( ! empty($clone_qdoc)) ? $clone_qdoc : '',
						  'true_options'		=> $ques['true_options'],
						  'videoq_media_file'	=> ( ! empty($clone_videoq_file)) ? $clone_videoq_file : '',
						  'videoq_cue_point'	=> $ques['videoq_cue_point'],
						  'seleted_drag_option'	=> $ques['seleted_drag_option'],
						  'qorder'				=> $ques['qorder'],
						  'shuffle'				=> $ques['shuffle'],
						  'ques_val_1'			=> $ques['ques_val_1'],
						  'ques_val_2'			=> $ques['ques_val_2'],
						  'ques_val_3'			=> $ques['ques_val_3'],
						  'ques_val_4'			=> $ques['ques_val_4'],
						  'ques_val_5'			=> $ques['ques_val_5'],
						  'ques_val_6'			=> $ques['ques_val_6'],
						  'critical'			=> $ques['critical'],
						  'status' 				=> 1,
						  'user_id'				=> $userId,
						  'cur_date'			=> $curdate);
		$ques_sql = "INSERT INTO `question_tbl` (`scenario_id`, `question_type`, `questions`, `qaudio`, `qspeech_text`, `speech_text`, `audio`, `video`, `screen`, `image`, `document`, `true_options`, `videoq_media_file`, `videoq_cue_point`, `seleted_drag_option`, `qorder`, `shuffle`, `ques_val_1`, `ques_val_2`, `ques_val_3`, `ques_val_4`, `ques_val_5`, `ques_val_6`, `critical`, `status`, `uid`, `cur_date`) 
					 VALUES (:sid, :qtype, :questions, :qaudio, :qspeech_text, :speech_text, :audio, :video, :screen, :image, :document, :true_options, :videoq_media_file, :videoq_cue_point, :seleted_drag_option, :qorder, :shuffle, :ques_val_1, :ques_val_2, :ques_val_3, :ques_val_4, :ques_val_5, :ques_val_6, :critical, :status, :user_id, :cur_date)";
		$ques_stmt = $db->prepare($ques_sql); $ques_stmt->execute($quesData);

		#-----Get-Last-Ques-Id----
		$last_ques_id = $db->lastInsertId();

		#----COPY-MULTIPLE-TEMP-FEEDBACK-DATA----
		if ($sim_temp_type == 2):
			#--------CORRECT-FEEDBACK----------
			$cfeed_data = $db->getFeedback($question_id, NULL, NULL, 1);
			$clone_cfaudio  = $clone_cfvideo = $clone_cfscreen = $clone_cfimage = $clone_cfdoc = '';
			#----copy-audio----
			if ( ! empty($cfeed_data['feed_audio']) && file_exists($root_path . $cfeed_data['feed_audio'])):
				$clone_cfaudio = 'copy-' . $db->get_random_string($random_st_len) . $cfeed_data['feed_audio'];
				@copy($uploadpath . $cfeed_data['feed_audio'], $uploadpath . $clone_cfaudio);
			endif;

			#----copy-video----
			if ( ! empty($cfeed_data['video']) && file_exists($root_path . $cfeed_data['video'])):
				$clone_cfvideo = 'copy-' . $db->get_random_string($random_st_len) . $cfeed_data['video'];
				@copy($uploadpath . $cfeed_data['video'], $uploadpath . $clone_cfvideo);
			endif;

			#----copy-screen----
			if ( ! empty($cfeed_data['screen']) && file_exists($root_path . $cfeed_data['screen'])):
				$clone_cfscreen = 'copy-' . $db->get_random_string($random_st_len) . $cfeed_data['screen'];
				@copy($uploadpath . $cfeed_data['screen'], $uploadpath . $clone_cfscreen);
			endif;

			#----copy-image----
			if ( ! empty($cfeed_data['image']) && file_exists($root_path . $cfeed_data['image'])):
				$clone_cfimage = 'copy-' . $db->get_random_string($random_st_len) . $cfeed_data['image'];
				@copy($uploadpath . $cfeed_data['image'], $uploadpath . $clone_cfimage);
			endif;

			#----copy-document----
			if ( ! empty($cfeed_data['document']) && file_exists($root_path . $cfeed_data['document'])):
				$clone_cfdoc = 'copy-' . $db->get_random_string($random_st_len) . $cfeed_data['document'];
				@copy($uploadpath . $cfeed_data['document'], $uploadpath . $clone_cfdoc);
			endif;
			
			$cfeedData = array('qid'			=> $last_ques_id,
							   'feedback'		=> ( ! empty($cfeed_data['feedback'])) ? $cfeed_data['feedback'] : '',
							   'ftype'			=> 1,
							   'speech_text'	=> ( ! empty($cfeed_data['feed_speech_text'])) ? $cfeed_data['feed_speech_text'] : '',
							   'audio'			=> ( ! empty($clone_cfaudio)) ? $clone_cfaudio : '',
							   'video'			=> ( ! empty($clone_cfvideo)) ? $clone_cfvideo : '',
							   'screen'			=> ( ! empty($clone_cfscreen)) ? $clone_cfscreen : '',
							   'image'			=> ( ! empty($clone_cfimage)) ? $clone_cfimage : '',
							   'document'		=> ( ! empty($clone_cfdoc)) ? $clone_cfdoc : '');
			$cfeed_sql  = "INSERT INTO `feedback_tbl` (`question_id`, `feedback`, `feedback_type`, `feed_speech_text`, `feed_audio`, `feed_video`, `feed_screen`, `feed_image`, `feed_document`) 
						   VALUES (:qid, :feedback, :ftype, :speech_text, :audio, :video, :screen, :image, :document)";
			$cfeed_stmt = $db->prepare($cfeed_sql); $cfeed_stmt->execute($cfeedData);
			
			#--------INCORRECT-FEEDBACK-------
			$ifeed_data = $db->getFeedback($question_id, NULL, NULL, 2);
			$clone_ifaudio  = $clone_ifvideo = $clone_ifscreen = $clone_ifimage = $clone_ifdoc = '';
			#----copy-audio----
			if ( ! empty($ifeed_data['feed_audio']) && file_exists($root_path . $ifeed_data['feed_audio'])):
				$clone_ifaudio = 'copy-' . $db->get_random_string($random_st_len) . $ifeed_data['feed_audio'];
				@copy($uploadpath . $ifeed_data['feed_audio'], $uploadpath . $clone_ifaudio);
			endif;

			#----copy-video----
			if ( ! empty($ifeed_data['video']) && file_exists($root_path . $ifeed_data['video'])):
				$clone_ifvideo = 'copy-' . $db->get_random_string($random_st_len) . $ifeed_data['video'];
				@copy($uploadpath . $ifeed_data['video'], $uploadpath . $clone_ifvideo);
			endif;

			#----copy-screen----
			if ( ! empty($ifeed_data['screen']) && file_exists($root_path . $ifeed_data['screen'])):
				$clone_ifscreen = 'copy-' . $db->get_random_string($random_st_len) . $ifeed_data['screen'];
				@copy($uploadpath . $ifeed_data['screen'], $uploadpath . $clone_ifscreen);
			endif;

			#----copy-image----
			if ( ! empty($ifeed_data['image']) && file_exists($root_path . $ifeed_data['image'])):
				$clone_ifimage = 'copy-' . $db->get_random_string($random_st_len) . $ifeed_data['image'];
				@copy($uploadpath . $ifeed_data['image'], $uploadpath . $clone_ifimage);
			endif;

			#----copy-document----
			if ( ! empty($ifeed_data['document']) && file_exists($root_path . $ifeed_data['document'])):
				$clone_ifdoc = 'copy-' . $db->get_random_string($random_st_len) . $ifeed_data['document'];
				@copy($uploadpath . $ifeed_data['document'], $uploadpath . $clone_ifdoc);
			endif;

			$ifeedData = array('qid'			=> $last_ques_id,
							   'feedback'		=> ( ! empty($ifeed_data['feedback'])) ? $ifeed_data['feedback'] : '',
							   'ftype'			=> 2,
							   'speech_text'	=> ( ! empty($ifeed_data['feed_speech_text'])) ? $ifeed_data['feed_speech_text'] : '',
							   'audio'			=> ( ! empty($clone_ifaudio)) ? $clone_ifaudio : '',
							   'video'			=> ( ! empty($clone_ifvideo)) ? $clone_ifvideo : '',
							   'screen'			=> ( ! empty($clone_ifscreen)) ? $clone_ifscreen : '',
							   'image'			=> ( ! empty($clone_ifimage)) ? $clone_ifimage : '',
							   'document'		=> ( ! empty($clone_ifdoc)) ? $clone_ifdoc : '');
			$ifeed_sql  = "INSERT INTO `feedback_tbl` (`question_id`, `feedback`, `feedback_type`, `feed_speech_text`, `feed_audio`, `feed_video`, `feed_screen`, `feed_image`, `feed_document`) 
						   VALUES (:qid, :feedback, :ftype, :speech_text, :audio, :video, :screen, :image, :document)";
			$ifeed_stmt = $db->prepare($ifeed_sql);
			$ifeed_stmt->execute($ifeedData);
		endif;
		
		#----COPY-ANSWER-DATA----
		$answer_data = $db->getAnswersByQuestionId(md5($ques['question_id']));
		if ( ! empty($answer_data)):
			foreach ($answer_data as $key => $ans):
				$answer_id = $ans['answer_id'];
				$clone_ans_img  = $clone_ans_img2 = '';

				if ( ! empty($ans['image']) && file_exists($root_path . $ans['image'])):
					$clone_ans_img = 'copy-' . $db->get_random_string($random_st_len) . $ans['image'];
					@copy($uploadpath . $ans['image'], $uploadpath . $clone_ans_img);
				endif;

				if ( ! empty($ans['image2']) && file_exists($root_path . $ans['image2'])):
					$clone_ans_img2 = 'copy-' . $db->get_random_string($random_st_len) . $ans['image2'];
					@copy($uploadpath . $ans['image2'], $uploadpath . $clone_ans_img2);
				endif;
				
				$ansSql	= "INSERT INTO `answer_tbl` (`question_id`, `choice_option`, `choice_order_no`, `match_order_no`, `match_option`, `match_sorting_item`, `nextQid`, `End_Sim`, `image`, `image2`, `ans_val1`, `ans_val2`, `ans_val3`, `ans_val4`, `ans_val5`, `ans_val6`, `true_option`, `status`, `uid`, `cur_date`) 
						   VALUES (:qid, :co, :con, :mon, :mo, :msi, :nqid, :end_sim, :img, :img2, :val1, :val2, :val3, :val4, :val5, :val6, :true, :status, :uid, :date)";
				$stmt	= $db->prepare($ansSql);
				$stmt->execute(array('qid'		=> $last_ques_id,
									 'co'		=> ( ! empty($ans['choice_option'])) ? $ans['choice_option'] : '',
									 'con'		=> ( ! empty($ans['choice_order_no'])) ? $ans['choice_order_no'] : '',
									 'mon'		=> ( ! empty($ans['match_order_no'])) ? $ans['match_order_no'] : '',
									 'mo'		=> ( ! empty($ans['match_option'])) ? $ans['match_option'] : '',
									 'msi'		=> ( ! empty($ans['match_sorting_item'])) ? $ans['match_sorting_item'] : '',
									 'nqid'		=> ( ! empty($ans['nextQid'])) ? $ans['nextQid'] : '',
									 'end_sim'	=> ( ! empty($ans['End_Sim'])) ? $ans['End_Sim'] : '',
									 'img'		=> ( ! empty($clone_ans_img)) ? $clone_ans_img : '',
									 'img2'		=> ( ! empty($clone_ans_img2)) ? $clone_ans_img2 : '',
									 'val1'		=> ( ! empty($ans['ans_val1'])) ? $ans['ans_val1'] : '',
									 'val2'		=> ( ! empty($ans['ans_val2'])) ? $ans['ans_val2'] : '',
									 'val3'		=> ( ! empty($ans['ans_val3'])) ? $ans['ans_val3'] : '',
									 'val4'		=> ( ! empty($ans['ans_val4'])) ? $ans['ans_val4'] : '',
									 'val5'		=> ( ! empty($ans['ans_val5'])) ? $ans['ans_val5'] : '',
									 'val6'		=> ( ! empty($ans['ans_val6'])) ? $ans['ans_val6'] : '',
									 'true'		=> ( ! empty($ans['true_option'])) ? $ans['true_option'] : '',
									 'status' 	=> 1,
									 'uid' 		=> $userId,
									 'date'		=> $curdate));
				#-----Get-Last-Ans-Id----
				$last_ans_id = $db->lastInsertId();

				#----COPY-SUBOPTION----
				$subOption = $db->getSubOptions($answer_id);
				if ( ! empty($subOption)):
					foreach ($subOption as $key => $sub):
						$clone_sub_img = '';
						if ( ! empty($sub['image']) && file_exists($root_path . $sub['image'])):
							$clone_sub_img = 'copy-' . $db->get_random_string($random_st_len) . $sub['image'];
							@copy($uploadpath . $ans['image'], $uploadpath . $clone_sub_img);
						endif;
						$opSql	= "INSERT INTO `sub_options_tbl` (`answer_id`, `sub_option`, `image`) values (:aid, :option, :img)";
						$opstmt = $db->prepare($opSql);
						$opstmt->execute(array('aid'	=> $last_ans_id,
											   'option'	=> ( ! empty($sub['sub_option'])) ? $sub['sub_option'] : '',
											   'img'	=> ( ! empty($clone_sub_img)) ? $clone_sub_img : ''));
					endforeach;
				endif;
				
				#----COPY-CLASSIC-TEMP-FEEDBACK-DATA----
				if ($sim_temp_type == 2 || $sim_temp_type == 8):
					
					#--------CORRECT-FEEDBACK----------
					$cfeed_data = $db->getFeedback($question_id, $answer_id, NULL, 1);
					$clone_cfaudio  = $clone_cfvideo = $clone_cfscreen = $clone_cfimage = $clone_cfdoc = '';
					#----copy-audio----
					if ( ! empty($cfeed_data['feed_audio']) && file_exists($root_path . $cfeed_data['feed_audio'])):
						$clone_cfaudio = 'copy-' . $db->get_random_string($random_st_len) . $cfeed_data['feed_audio'];
						@copy($uploadpath . $cfeed_data['feed_audio'], $uploadpath . $clone_cfaudio);
					endif;

					#----copy-video----
					if ( ! empty($cfeed_data['video']) && file_exists($root_path . $cfeed_data['video'])):
						$clone_cfvideo = 'copy-' . $db->get_random_string($random_st_len) . $cfeed_data['video'];
						@copy($uploadpath . $cfeed_data['video'], $uploadpath . $clone_cfvideo);
					endif;

					#----copy-screen----
					if ( ! empty($cfeed_data['screen']) && file_exists($root_path . $cfeed_data['screen'])):
						$clone_cfscreen = 'copy-' . $db->get_random_string($random_st_len) . $cfeed_data['screen'];
						@copy($uploadpath . $cfeed_data['screen'], $uploadpath . $clone_cfscreen);
					endif;

					#----copy-image----
					if ( ! empty($cfeed_data['image']) && file_exists($root_path . $cfeed_data['image'])):
						$clone_cfimage = 'copy-' . $db->get_random_string($random_st_len) . $cfeed_data['image'];
						@copy($uploadpath . $cfeed_data['image'], $uploadpath . $clone_cfimage);
					endif;

					#----copy-document----
					if ( ! empty($cfeed_data['document']) && file_exists($root_path . $cfeed_data['document'])):
						$clone_cfdoc = 'copy-' . $db->get_random_string($random_st_len) . $cfeed_data['document'];
						@copy($uploadpath . $cfeed_data['document'], $uploadpath . $clone_cfdoc);
					endif;
					
					$cfeedData = array('qid'			=> $last_ques_id,
									   'aid'			=> $last_ans_id,
									   'feedback'		=> ( ! empty($cfeed_data['feedback'])) ? $cfeed_data['feedback'] : '',
									   'ftype'			=> 1,
									   'speech_text'	=> ( ! empty($cfeed_data['feed_speech_text'])) ? $cfeed_data['feed_speech_text'] : '',
									   'audio'			=> ( ! empty($clone_cfaudio)) ? $clone_cfaudio : '',
									   'video'			=> ( ! empty($clone_cfvideo)) ? $clone_cfvideo : '',
									   'screen'			=> ( ! empty($clone_cfscreen)) ? $clone_cfscreen : '',
									   'image'			=> ( ! empty($clone_cfimage)) ? $clone_cfimage : '',
									   'document'		=> ( ! empty($clone_cfdoc)) ? $clone_cfdoc : '');
					$cfeed_sql  = "INSERT INTO `feedback_tbl` (`question_id`, `answer_id`, `feedback`, `feedback_type`, `feed_speech_text`, `feed_audio`, `feed_video`, `feed_screen`, `feed_image`, `feed_document`) 
								   VALUES (:qid, :aid, :feedback, :ftype, :speech_text, :audio, :video, :screen, :image, :document)";
					$cfeed_stmt = $db->prepare($cfeed_sql); $cfeed_stmt->execute($cfeedData);
				endif;
			endforeach;
		endif;
		$res = array('success' => TRUE, 'msg' => 'Question copied successfully.');
	else:
		$res = array('success' => FALSE, 'msg' => 'Question not copied. Please try again later!');
	endif;
	echo json_encode($res);
endif;

#---------------------------------------Clone-Simulation-----------------------------
if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['clone_sim']) &&  ! empty($_POST['sim_id'])):
	$sim_id 		= $_POST['sim_id'];
	$sim_data		= $db->getScenario($sim_id);
	$sim_comp		= $db->getScenarioCompetency($sim_id);
	$sim_page		= $db->getSimPage($sim_data['scenario_id']);
	$sim_brand		= $db->getSimBranding($sim_data['scenario_id']);
	$sim_ques_type	= $sim_data['sim_ques_type'];
	$sim_temp_type	= $sim_data['sim_temp_type'];
	$random_st_len	= 10;
	
	#----COPY-SIMULATION-DATA----

	#----copy-Sim-Image----
	if ( ! empty($sim_data['image_url']) && file_exists($root_path . $sim_data['image_url'])):
		$clone_img = 'copy-' . $db->get_random_string($random_st_len) . $sim_data['image_url'];
		@copy($uploadpath . $sim_data['image_url'], $uploadpath . $clone_img);
	endif;

	#----copy-Sim-Video----
	if ( ! empty($sim_data['video_url']) && file_exists($root_path . $sim_data['video_url'])):
		$clone_video = 'copy-' . $db->get_random_string($random_st_len) . $sim_data['video_url'];
		@copy($uploadpath . $sim_data['video_url'], $uploadpath . $clone_video);
	endif;

	#----copy-Sim-Audio----
	if ( ! empty($sim_data['audio_url']) && file_exists($root_path . $sim_data['audio_url'])):
		$clone_audio = 'copy-' . $db->get_random_string($random_st_len) . $sim_data['audio_url'];
		@copy($uploadpath . $sim_data['audio_url'], $uploadpath . $clone_audio);
	endif;

	#----copy-Own-Char---
	if ( ! empty($sim_data['char_own_choice']) && file_exists($root_path . $sim_data['char_own_choice'])):
		$clone_own_choice = 'copy-' . $db->get_random_string($random_st_len) . $sim_data['char_own_choice'];
		@copy($uploadpath . $sim_data['char_own_choice'], $uploadpath . $clone_own_choice);
	endif;
	
	#----copy-Sim-Cover---
	if ( ! empty($sim_data['sim_cover_img']) && file_exists($root_path . $sim_data['sim_cover_img'])):
		$clone_cover_img = 'copy-' . $db->get_random_string($random_st_len) . $sim_data['sim_cover_img'];
		@copy($uploadpath . $sim_data['sim_cover_img'], $uploadpath . $clone_cover_img);
	endif;

	#----copy-Sim-Cover---
	if ( ! empty($sim_data['bg_own_choice']) && file_exists($root_path . $sim_data['bg_own_choice'])):
		$clone_bg_own_img = 'copy-' . $db->get_random_string($random_st_len) . $sim_data['bg_own_choice'];
		@copy($uploadpath . $sim_data['bg_own_choice'], $uploadpath . $clone_bg_own_img);
	endif;
	
	$sim_copy_data = array('title'					=> ( ! empty($sim_data['Scenario_title'])) ? 'Copy-'. $sim_data['Scenario_title'] : '',
						   'sim_type'				=> ( ! empty($sim_data['scenario_type'])) ? $sim_data['scenario_type'] : '',
						   'ques_type' 				=> ( ! empty($sim_data['sim_ques_type'])) ? $sim_data['sim_ques_type'] : '',
						   'temp_type' 				=> ( ! empty($sim_data['sim_temp_type'])) ? $sim_data['sim_temp_type'] : '',
						   'auth_sim_type' 			=> ( ! empty($sim_data['author_scenario_type'])) ? $sim_data['author_scenario_type'] : '',
						   'category' 				=> ( ! empty($sim_data['scenario_category'])) ? $sim_data['scenario_category'] : '',
						   'duration' 				=> ( ! empty($sim_data['duration'])) ? $sim_data['duration'] : '',
						   'passing_marks' 			=> ( ! empty($sim_data['passing_marks'])) ? $sim_data['passing_marks'] : '',
						   'image_url' 				=> ( ! empty($clone_img)) ? $clone_img : '',
						   'video_url' 				=> ( ! empty($clone_video)) ? $clone_video : '',
						   'audio_url' 				=> ( ! empty($clone_audio)) ? $clone_audio : '',
						   'assign_start_date' 		=> ( ! empty($sim_data['assign_start_date'])) ? $sim_data['assign_start_date'] : '',
						   'assign_end_date' 		=> ( ! empty($sim_data['assign_end_date'])) ? $sim_data['assign_end_date'] : '',
						   'assign_author' 			=> ( ! empty($sim_data['assign_author'])) ? $sim_data['assign_author'] : '',
						   'assign_reviewer' 		=> ( ! empty($sim_data['assign_reviewer'])) ? $sim_data['assign_reviewer'] : '',
						   'start_date' 			=> ( ! empty($sim_data['start_date'])) ? $sim_data['start_date'] : '',
						   'end_date' 				=> ( ! empty($sim_data['end_date'])) ? $sim_data['end_date'] : '',
						   'bg_color' 				=> ( ! empty($sim_data['bg_color'])) ? $sim_data['bg_color'] : '',
						   'bg_own_choice' 			=> ( ! empty($clone_bg_own_img)) ? $clone_bg_own_img : '',
						   'sim_back_img' 			=> ( ! empty($sim_data['sim_back_img'])) ? $sim_data['sim_back_img'] : '',
						   'sim_char_img' 			=> ( ! empty($sim_data['sim_char_img'])) ? $sim_data['sim_char_img'] : '',
						   'sim_char_left_img' 		=> ( ! empty($sim_data['sim_char_left_img'])) ? $sim_data['sim_char_left_img'] : '',
						   'sim_char_right_img'		=> ( ! empty($sim_data['sim_char_right_img'])) ? $sim_data['sim_char_right_img'] : '',
						   'char_own_choice' 		=> ( ! empty($clone_own_choice)) ? $clone_own_choice : '',
						   'sim_cover_img' 			=> ( ! empty($clone_cover_img)) ? $clone_cover_img : '',
						   'status' 				=> 2,
						   'modified_by' 			=> $userId,
						   'modified_on'			=> $curdate,
						   'created_by' 			=> $userId,
						   'created_on' 			=> $curdate);
	$sql  = "INSERT INTO `scenario_master` (`Scenario_title`, `scenario_type`, `sim_ques_type`, `sim_temp_type`, `author_scenario_type`, `scenario_category`, `duration`, `passing_marks`, `image_url`, `video_url`, `audio_url`, `assign_start_date`, `assign_end_date`, `assign_author`, `assign_reviewer`, `start_date`, `end_date`, `bg_color`, `bg_own_choice`, `sim_back_img`, `sim_char_img`, `sim_char_left_img`, `sim_char_right_img`, `char_own_choice`, `sim_cover_img`, `status`, `modified_by`, `modified_on`, `created_by`, `created_on`) 
			VALUES (:title, :sim_type, :ques_type, :temp_type, :auth_sim_type, :category, :duration, :passing_marks, :image_url, :video_url, :audio_url, :assign_start_date, :assign_end_date, :assign_author, :assign_reviewer, :start_date, :end_date, :bg_color, :bg_own_choice, :sim_back_img, :sim_char_img, :sim_char_left_img, :sim_char_right_img, :char_own_choice, :sim_cover_img, :status, :modified_by, :modified_on, :created_by, :created_on)";
	$stmt = $db->prepare($sql);
	if ($stmt->execute($sim_copy_data)):
		
		#--Get-Last-SIM-Id--
		$last_sim_id = $db->lastInsertId();
		
		#----COPY-COMPETENCY----
		if ( ! empty($sim_comp)):
			$comp_data	 = array('scenario_id'	=> $last_sim_id,
								'comp_col_1'	=> ( ! empty($sim_comp['comp_col_1'])) ? $sim_comp['comp_col_1'] : '',
								'comp_val_1'	=> ( ! empty($sim_comp['comp_val_1'])) ? $sim_comp['comp_val_1'] : '',
								'comp_col_2'	=> ( ! empty($sim_comp['comp_col_2'])) ? $sim_comp['comp_col_2'] : '',
								'comp_val_2'	=> ( ! empty($sim_comp['comp_val_2'])) ? $sim_comp['comp_val_2'] : '',
								'comp_col_3'	=> ( ! empty($sim_comp['comp_col_3'])) ? $sim_comp['comp_col_3'] : '',
								'comp_val_3'	=> ( ! empty($sim_comp['comp_val_3'])) ? $sim_comp['comp_val_3'] : '',
								'comp_col_4'	=> ( ! empty($sim_comp['comp_col_4'])) ? $sim_comp['comp_col_4'] : '',
								'comp_val_4'	=> ( ! empty($sim_comp['comp_val_4'])) ? $sim_comp['comp_val_4'] : '',
								'comp_col_5'	=> ( ! empty($sim_comp['comp_col_5'])) ? $sim_comp['comp_col_5'] : '',
								'comp_val_5'	=> ( ! empty($sim_comp['comp_val_5'])) ? $sim_comp['comp_val_5'] : '',
								'comp_col_6'	=> ( ! empty($sim_comp['comp_col_6'])) ? $sim_comp['comp_col_6'] : '',
								'comp_val_6'	=> ( ! empty($sim_comp['comp_val_6'])) ? $sim_comp['comp_val_6'] : '',
								'weightage_1'	=> ( ! empty($sim_comp['weightage_1'])) ? $sim_comp['weightage_1'] : '',
								'weightage_2'	=> ( ! empty($sim_comp['weightage_2'])) ? $sim_comp['weightage_2'] : '',
								'weightage_3'	=> ( ! empty($sim_comp['weightage_3'])) ? $sim_comp['weightage_3'] : '',
								'weightage_4'	=> ( ! empty($sim_comp['weightage_4'])) ? $sim_comp['weightage_4'] : '',
								'weightage_5'	=> ( ! empty($sim_comp['weightage_5'])) ? $sim_comp['weightage_5'] : '',
								'weightage_6'	=> ( ! empty($sim_comp['weightage_6'])) ? $sim_comp['weightage_6'] : '',
								'status' 		=> 1,
								'user_id' 		=> $userId,
								'cur_date'		=> $curdate);
			$com_sql  = "INSERT INTO `competency_tbl` (`scenario_id`, `comp_col_1`, `comp_val_1`, `comp_col_2`, `comp_val_2`, `comp_col_3`, `comp_val_3`, `comp_col_4`, `comp_val_4`, `comp_col_5`, `comp_val_5`, `comp_col_6`, `comp_val_6`, `weightage_1`, `weightage_2`, `weightage_3`, `weightage_4`, `weightage_5`, `weightage_6`, `status`, `uid`, `cur_date`) 
						VALUES (:scenario_id, :comp_col_1, :comp_val_1, :comp_col_2, :comp_val_2, :comp_col_3, :comp_val_3, :comp_col_4, :comp_val_4, :comp_col_5, :comp_val_5, :comp_col_6, :comp_val_6, :weightage_1, :weightage_2, :weightage_3, :weightage_4, :weightage_5, :weightage_6, :status, :user_id, :cur_date)";
			$com_stmt = $db->prepare($com_sql); $com_stmt->execute($comp_data);
		endif;

		#---COPY-PAGES----
		if ( ! empty($sim_page)): $sp = 0;
			foreach ($sim_page as $pdata):
				$pageq 		= "INSERT INTO `sim_pages` (`scenario_id`, `sim_page_name`, `sim_page_desc`, `userid`, `status`, `added_date`) VALUES (:sid, :pname, :pdesc, :user_id, :status, :adate)";
				$page_stmt	= $db->prepare($pageq);
				$page_stmt->execute(array('sid'		=> $last_sim_id,
										  'pname'	=> ( ! empty($pdata['sim_page_name'])) ? $pdata['sim_page_name'] : '',
										  'pdesc'	=> ( ! empty($pdata['sim_page_desc'])) ? $pdata['sim_page_desc'] : '',
										  'status' 	=> 1,
										  'user_id'	=> $userId,
										  'adate'	=> $curdate));
			$sp++; endforeach;
		endif;

		#---COPY-BRANDING----
		if ( ! empty($sim_brand)):
			$brand_data = array('sid'				=> $last_sim_id,
								'bg'				=> ( ! empty($sim_brand['ques_bg'])) ? $sim_brand['ques_bg'] : '',
								'bg_tr'				=> ( ! empty($sim_brand['ques_bg_transparency'])) ? $sim_brand['ques_bg_transparency'] : '',
								'obg'				=> ( ! empty($sim_brand['option_bg'])) ? $sim_brand['option_bg'] : '',
								'obg_tr'			=> ( ! empty($sim_brand['option_bg_transparency'])) ? $sim_brand['option_bg_transparency'] : '',
								'ohover'			=> ( ! empty($sim_brand['option_hover'])) ? $sim_brand['option_hover'] : '',
								'oh_tr'				=> ( ! empty($sim_brand['option_hover_transparency'])) ? $sim_brand['option_hover_transparency'] : '',
								'oselected'			=> ( ! empty($sim_brand['option_selected'])) ? $sim_brand['option_selected'] : '',
								'oselected_tr'		=> ( ! empty($sim_brand['option_selected_transparency'])) ? $sim_brand['option_selected_transparency'] : '',
								'btn_bg'			=> ( ! empty($sim_brand['btn_bg'])) ? $sim_brand['btn_bg'] : '',
								'btn_bg_tr'			=> ( ! empty($sim_brand['btn_bg_transparency'])) ? $sim_brand['btn_bg_transparency'] : '',
								'btn_hover'			=> ( ! empty($sim_brand['btn_hover'])) ? $sim_brand['btn_hover'] : '',
								'btn_hover_tr'		=> ( ! empty($sim_brand['btn_hover_transparency'])) ? $sim_brand['btn_hover_transparency'] : '',
								'btn_selected'		=> ( ! empty($sim_brand['btn_selected'])) ? $sim_brand['btn_selected'] : '',
								'btn_selected_tr'	=> ( ! empty($sim_brand['btn_selected_transparency'])) ? $sim_brand['btn_selected_transparency'] : '',
								'font_type'			=> ( ! empty($sim_brand['font_type'])) ? $sim_brand['font_type'] : '',
								'font_color'		=> ( ! empty($sim_brand['font_color'])) ? $sim_brand['font_color'] : '',
								'font_size'			=> ( ! empty($sim_brand['font_size'])) ? $sim_brand['font_size'] : '',
								'user_id' 			=> $userId,
								'added_date'		=> $curdate);
			$com_sql  = "INSERT INTO `sim_branding_tbl` (`scenario_id`, `ques_bg`, `ques_bg_transparency`, `option_bg`, `option_bg_transparency`, `option_hover`, `option_hover_transparency`, `option_selected`, `option_selected_transparency`, `btn_bg`, `btn_bg_transparency`, `btn_hover`, `btn_hover_transparency`, `btn_selected`, `btn_selected_transparency`, `font_type`, `font_color`, `font_size`, `user_id`, `added_date`) 
						VALUES (:sid, :bg, :bg_tr, :obg, :obg_tr, :ohover, :oh_tr, :oselected, :oselected_tr, :btn_bg, :btn_bg_tr, :btn_hover, :btn_hover_tr, :btn_selected, :btn_selected_tr, :font_type, :font_color, :font_size, :user_id, :added_date)";
			$com_stmt = $db->prepare($com_sql); $com_stmt->execute($brand_data);
		endif;
		
		#----COPY-QUESTION-DATA----
		$ques_data = $db->getQuestionsByScenario($sim_id);
		if ( ! empty($ques_data)):
			foreach ($ques_data as $ques):
				$question_id	= $ques['question_id'];
				$question_type	= $ques['question_type'];
				$clone_qqaudio	= $clone_qaudio = $clone_qvideo = $clone_qscreen = $clone_qimage = $clone_qdoc = $clone_videoq_file = '';
				#----copy-question-audio----
				if ( ! empty($ques['qaudio']) && file_exists($root_path . $ques['qaudio'])):
					$clone_qqaudio = 'copy-' . $db->get_random_string($random_st_len) . $ques['qaudio'];
					@copy($uploadpath . $ques['qaudio'], $uploadpath . $clone_qqaudio);
				endif;
				
				#----copy-question--audio----
				if ( ! empty($ques['audio']) && file_exists($root_path . $ques['audio'])):
					$clone_qaudio = 'copy-' . $db->get_random_string($random_st_len) . $ques['audio'];
					@copy($uploadpath . $ques['audio'], $uploadpath . $clone_audio);
				endif;

				#----copy-question--video----
				if ( ! empty($ques['video']) && file_exists($root_path . $ques['video'])):
					$clone_qvideo = 'copy-' . $db->get_random_string($random_st_len) . $ques['video'];
					@copy($uploadpath . $ques['video'], $uploadpath . $clone_qvideo);
				endif;

				#----copy-question--screen----
				if ( ! empty($ques['screen']) && file_exists($root_path . $ques['screen'])):
					$clone_qscreen = 'copy-' . $db->get_random_string($random_st_len) . $ques['screen'];
					@copy($uploadpath . $ques['screen'], $uploadpath . $clone_qscreen);
				endif;

				#----copy-question--image----
				if ( ! empty($ques['image']) && file_exists($root_path . $ques['image'])):
					$clone_qimage = 'copy-' . $db->get_random_string($random_st_len) . $ques['image'];
					@copy($uploadpath . $ques['image'], $uploadpath . $clone_qimage);
				endif;

				#----copy-question--document----
				if ( ! empty($ques['document']) && file_exists($root_path . $ques['document'])):
					$clone_qdoc = 'copy-' . $db->get_random_string($random_st_len) . $ques['document'];
					@copy($uploadpath . $ques['document'], $uploadpath . $clone_qdoc);
				endif;

				#----copy-video-sim-question----
				if ( ! empty($ques['videoq_media_file']) && file_exists($root_path . $ques['videoq_media_file'])):
					$clone_videoq_file = 'copy-' . $db->get_random_string($random_st_len) . $ques['videoq_media_file'];
					@copy($uploadpath . $ques['videoq_media_file'], $uploadpath . $clone_videoq_file);
				endif;

				$quesData = array('sid'					=> $last_sim_id,
								  'qtype'				=> $ques['question_type'],
								  'questions'			=> $ques['questions'],
								  'qaudio'				=> ( ! empty($clone_qqaudio)) ? $clone_qqaudio : '',
								  'qspeech_text'		=> $ques['qspeech_text'],
								  'speech_text'			=> $ques['speech_text'],
								  'audio'				=> ( ! empty($clone_qaudio)) ? $clone_qaudio : '',
								  'video'				=> ( ! empty($clone_qvideo)) ? $clone_qvideo : '',
								  'screen'				=> ( ! empty($clone_qscreen)) ? $clone_qscreen : '',
								  'image'				=> ( ! empty($clone_qimage)) ? $clone_qimage : '',
								  'document'			=> ( ! empty($clone_qdoc)) ? $clone_qdoc : '',
								  'true_options'		=> $ques['true_options'],
								  'videoq_media_file'	=> ( ! empty($clone_videoq_file)) ? $clone_videoq_file : '',
								  'videoq_cue_point'	=> $ques['videoq_cue_point'],
								  'seleted_drag_option'	=> $ques['seleted_drag_option'],
								  'qorder'				=> $ques['qorder'],
								  'shuffle'				=> $ques['shuffle'],
								  'ques_val_1'			=> $ques['ques_val_1'],
								  'ques_val_2'			=> $ques['ques_val_2'],
								  'ques_val_3'			=> $ques['ques_val_3'],
								  'ques_val_4'			=> $ques['ques_val_4'],
								  'ques_val_5'			=> $ques['ques_val_5'],
								  'ques_val_6'			=> $ques['ques_val_6'],
								  'critical'			=> $ques['critical'],
								  'status' 				=> 1,
								  'user_id'				=> $userId,
								  'cur_date'			=> $curdate);
				$ques_sql = "INSERT INTO `question_tbl` (`scenario_id`, `question_type`, `questions`, `qaudio`, `qspeech_text`, `speech_text`, `audio`, `video`, `screen`, `image`, `document`, `true_options`, `videoq_media_file`, `videoq_cue_point`, `seleted_drag_option`, `qorder`, `shuffle`, `ques_val_1`, `ques_val_2`, `ques_val_3`, `ques_val_4`, `ques_val_5`, `ques_val_6`, `critical`, `status`, `uid`, `cur_date`) 
							VALUES (:sid, :qtype, :questions, :qaudio, :qspeech_text, :speech_text, :audio, :video, :screen, :image, :document, :true_options, :videoq_media_file, :videoq_cue_point, :seleted_drag_option, :qorder, :shuffle, :ques_val_1, :ques_val_2, :ques_val_3, :ques_val_4, :ques_val_5, :ques_val_6, :critical, :status, :user_id, :cur_date)";
				$ques_stmt = $db->prepare($ques_sql); $ques_stmt->execute($quesData);

				#-----Get-Last-Ques-Id----
				$last_ques_id = $db->lastInsertId();

				#----COPY--MULTIPLE-TEMP-FEEDBACK-DATA----
				if ($sim_temp_type == 2):
					
					#--------CORRECT-FEEDBACK----------
					$cfeed_data = $db->getFeedback($question_id, NULL, NULL, 1);
					
					$clone_cfaudio	= $clone_cfvideo = $clone_cfscreen = $clone_cfimage = $clone_cfdoc = '';

					#----copy-audio----
					if ( ! empty($cfeed_data['feed_audio']) && file_exists($root_path . $cfeed_data['feed_audio'])):
						$clone_cfaudio = 'copy-' . $db->get_random_string($random_st_len) . $cfeed_data['feed_audio'];
						@copy($uploadpath . $cfeed_data['feed_audio'], $uploadpath . $clone_cfaudio);
					endif;

					#----copy-video----
					if ( ! empty($cfeed_data['video']) && file_exists($root_path . $cfeed_data['video'])):
						$clone_cfvideo = 'copy-' . $db->get_random_string($random_st_len) . $cfeed_data['video'];
						@copy($uploadpath . $cfeed_data['video'], $uploadpath . $clone_cfvideo);
					endif;

					#----copy-screen----
					if ( ! empty($cfeed_data['screen']) && file_exists($root_path . $cfeed_data['screen'])):
						$clone_cfscreen = 'copy-' . $db->get_random_string($random_st_len) . $cfeed_data['screen'];
						@copy($uploadpath . $cfeed_data['screen'], $uploadpath . $clone_cfscreen);
					endif;

					#----copy-image----
					if ( ! empty($cfeed_data['image']) && file_exists($root_path . $cfeed_data['image'])):
						$clone_cfimage = 'copy-' . $db->get_random_string($random_st_len) . $cfeed_data['image'];
						@copy($uploadpath . $cfeed_data['image'], $uploadpath . $clone_cfimage);
					endif;

					#----copy-document----
					if ( ! empty($cfeed_data['document']) && file_exists($root_path . $cfeed_data['document'])):
						$clone_cfdoc = 'copy-' . $db->get_random_string($random_st_len) . $cfeed_data['document'];
						@copy($uploadpath . $cfeed_data['document'], $uploadpath . $clone_cfdoc);
					endif;
					
					$cfeedData = array('qid'			=> $last_ques_id,
									   'feedback'		=> ( ! empty($cfeed_data['feedback'])) ? $cfeed_data['feedback'] : '',
									   'ftype'			=> 1,
									   'speech_text'	=> ( ! empty($cfeed_data['feed_speech_text'])) ? $cfeed_data['feed_speech_text'] : '',
									   'audio'			=> ( ! empty($clone_cfaudio)) ? $clone_cfaudio : '',
									   'video'			=> ( ! empty($clone_cfvideo)) ? $clone_cfvideo : '',
									   'screen'			=> ( ! empty($clone_cfscreen)) ? $clone_cfscreen : '',
									   'image'			=> ( ! empty($clone_cfimage)) ? $clone_cfimage : '',
									   'document'		=> ( ! empty($clone_cfdoc)) ? $clone_cfdoc : '');
					$cfeed_sql  = "INSERT INTO `feedback_tbl` (`question_id`, `feedback`, `feedback_type`, `feed_speech_text`, `feed_audio`, `feed_video`, `feed_screen`, `feed_image`, `feed_document`) 
									VALUES (:qid, :feedback, :ftype, :speech_text, :audio, :video, :screen, :image, :document)";
					$cfeed_stmt = $db->prepare($cfeed_sql); $cfeed_stmt->execute($cfeedData);
					
					#--------INCORRECT-FEEDBACK-------
					$ifeed_data = $db->getFeedback($question_id, NULL, NULL, 2);
					$clone_ifaudio = $clone_ifvideo = $clone_ifscreen = $clone_ifimage = $clone_ifdoc = '';
					#----copy-audio----
					if ( ! empty($ifeed_data['feed_audio']) && file_exists($root_path . $ifeed_data['feed_audio'])):
						$clone_ifaudio = 'copy-' . $db->get_random_string($random_st_len) . $ifeed_data['feed_audio'];
						@copy($uploadpath . $ifeed_data['feed_audio'], $uploadpath . $clone_ifaudio);
					endif;

					#----copy-video----
					if ( ! empty($ifeed_data['video']) && file_exists($root_path . $ifeed_data['video'])):
						$clone_ifvideo = 'copy-' . $db->get_random_string($random_st_len) . $ifeed_data['video'];
						@copy($uploadpath . $ifeed_data['video'], $uploadpath . $clone_ifvideo);
					endif;

					#----copy-screen----
					if ( ! empty($ifeed_data['screen']) && file_exists($root_path . $ifeed_data['screen'])):
						$clone_ifscreen = 'copy-' . $db->get_random_string($random_st_len) . $ifeed_data['screen'];
						@copy($uploadpath . $ifeed_data['screen'], $uploadpath . $clone_ifscreen);
					endif;

					#----copy-image----
					if ( ! empty($ifeed_data['image']) && file_exists($root_path . $ifeed_data['image'])):
						$clone_ifimage = 'copy-' . $db->get_random_string($random_st_len) . $ifeed_data['image'];
						@copy($uploadpath . $ifeed_data['image'], $uploadpath . $clone_ifimage);
					endif;

					#----copy-document----
					if ( ! empty($ifeed_data['document']) && file_exists($root_path . $ifeed_data['document'])):
						$clone_ifdoc = 'copy-' . $db->get_random_string($random_st_len) . $ifeed_data['document'];
						@copy($uploadpath . $ifeed_data['document'], $uploadpath . $clone_ifdoc);
					endif;

					$ifeedData = array('qid'			=> $last_ques_id,
									   'feedback'		=> ( ! empty($ifeed_data['feedback'])) ? $ifeed_data['feedback'] : '',
									   'ftype'			=> 2,
									   'speech_text'	=> ( ! empty($ifeed_data['feed_speech_text'])) ? $ifeed_data['feed_speech_text'] : '',
									   'audio'			=> ( ! empty($clone_ifaudio)) ? $clone_ifaudio : '',
									   'video'			=> ( ! empty($clone_ifvideo)) ? $clone_ifvideo : '',
									   'screen'			=> ( ! empty($clone_ifscreen)) ? $clone_ifscreen : '',
									   'image'			=> ( ! empty($clone_ifimage)) ? $clone_ifimage : '',
									   'document'		=> ( ! empty($clone_ifdoc)) ? $clone_ifdoc : '');
					$ifeed_sql  = "INSERT INTO `feedback_tbl` (`question_id`, `feedback`, `feedback_type`, `feed_speech_text`, `feed_audio`, `feed_video`, `feed_screen`, `feed_image`, `feed_document`) 
									VALUES (:qid, :feedback, :ftype, :speech_text, :audio, :video, :screen, :image, :document)";
					$ifeed_stmt = $db->prepare($ifeed_sql);
					$ifeed_stmt->execute($ifeedData);
				endif;
				
				#----COPY-ANSWER-DATA----
				$answer_data  = $db->getAnswersByQuestionId(md5($ques['question_id']));
				if ( ! empty($answer_data)):
					foreach ($answer_data as $key => $ans):
						$answer_id = $ans['answer_id'];
						$clone_ans_img	= $clone_ans_img2 = '';
						if ( ! empty($ans['image']) && file_exists($root_path . $ans['image'])):
							$clone_ans_img = 'copy-' . $db->get_random_string($random_st_len) . $ans['image'];
							@copy($uploadpath . $ans['image'], $uploadpath . $clone_ans_img);
						endif;
						if ( ! empty($ans['image2']) && file_exists($root_path . $ans['image2'])):
							$clone_ans_img2 = 'copy-' . $db->get_random_string($random_st_len) . $ans['image2'];
							@copy($uploadpath . $ans['image2'], $uploadpath . $clone_ans_img2);
						endif;
						$ansSql	= "INSERT INTO `answer_tbl` (`question_id`, `choice_option`, `choice_order_no`, `match_order_no`, `match_option`, `match_sorting_item`, `nextQid`, `End_Sim`, `image`, `image2`, `ans_val1`, `ans_val2`, `ans_val3`, `ans_val4`, `ans_val5`, `ans_val6`, `true_option`, `status`, `uid`, `cur_date`) 
									VALUES (:qid, :co, :con, :mon, :mo, :msi, :nqid, :end_sim, :img, :img2, :val1, :val2, :val3, :val4, :val5, :val6, :true, :status, :uid, :date)";
						$stmt	= $db->prepare($ansSql);
						$stmt->execute(array('qid'		=> $last_ques_id,
											 'co'		=> ( ! empty($ans['choice_option'])) ? $ans['choice_option'] : '',
											 'con'		=> ( ! empty($ans['choice_order_no'])) ? $ans['choice_order_no'] : '',
											 'mon'		=> ( ! empty($ans['match_order_no'])) ? $ans['match_order_no'] : '',
											 'mo'		=> ( ! empty($ans['match_option'])) ? $ans['match_option'] : '',
											 'msi'		=> ( ! empty($ans['match_sorting_item'])) ? $ans['match_sorting_item'] : '',
											 'nqid'		=> ( ! empty($ans['nextQid'])) ? $ans['nextQid'] : '',
											 'end_sim'	=> ( ! empty($ans['End_Sim'])) ? $ans['End_Sim'] : '',
											 'img'		=> ( ! empty($clone_ans_img)) ? $clone_ans_img : '',
											 'img2'		=> ( ! empty($clone_ans_img2)) ? $clone_ans_img2 : '',
											 'val1'		=> ( ! empty($ans['ans_val1'])) ? $ans['ans_val1'] : '',
											 'val2'		=> ( ! empty($ans['ans_val2'])) ? $ans['ans_val2'] : '',
											 'val3'		=> ( ! empty($ans['ans_val3'])) ? $ans['ans_val3'] : '',
											 'val4'		=> ( ! empty($ans['ans_val4'])) ? $ans['ans_val4'] : '',
											 'val5'		=> ( ! empty($ans['ans_val5'])) ? $ans['ans_val5'] : '',
											 'val6'		=> ( ! empty($ans['ans_val6'])) ? $ans['ans_val6'] : '',
											 'true'		=> ( ! empty($ans['true_option'])) ? $ans['true_option'] : '',
											 'status' 	=> 1,
											 'uid' 		=> $userId,
											 'date'		=> $curdate));
						#-----Get-Last-Ans-Id----
						$last_ans_id = $db->lastInsertId();

						#----COPY-SUBOPTION----
						$subOption	 = $db->getSubOptions($answer_id);
						if ( ! empty($subOption)):
							foreach ($subOption as $key => $sub):
								$clone_sub_img = '';
								if ( ! empty($sub['image']) && file_exists($root_path . $sub['image'])):
									$clone_sub_img = 'copy-' . $db->get_random_string($random_st_len) . $sub['image'];
									@copy($uploadpath . $ans['image'], $uploadpath . $clone_sub_img);
								endif;
								$opSql	= "INSERT INTO `sub_options_tbl` (`answer_id`, `sub_option`, `image`) values (:aid, :option, :img)";
								$opstmt = $db->prepare($opSql);
								$opstmt->execute(array('aid'	=> $last_ans_id,
													   'option'	=> ( ! empty($sub['sub_option'])) ? $sub['sub_option'] : '',
													   'img'	=> ( ! empty($clone_sub_img)) ? $clone_sub_img : ''));
							endforeach;
						endif;
						
						#----COPY-CLASSIC-TEMP-FEEDBACK-DATA----
						if ($sim_temp_type == 2 || $sim_temp_type == 8):
							
							#--------CORRECT-FEEDBACK----------
							$cfeed_data = $db->getFeedback($question_id, $answer_id, NULL, 1);
							$clone_cfaudio = $clone_cfvideo = $clone_cfscreen = $clone_cfimage = $clone_cfdoc = '';

							#----copy-audio----
							if ( ! empty($cfeed_data['feed_audio']) && file_exists($root_path . $cfeed_data['feed_audio'])):
								$clone_cfaudio = 'copy-' . $db->get_random_string($random_st_len) . $cfeed_data['feed_audio'];
								@copy($uploadpath . $cfeed_data['feed_audio'], $uploadpath . $clone_cfaudio);
							endif;

							#----copy-video----
							if ( ! empty($cfeed_data['video']) && file_exists($root_path . $cfeed_data['video'])):
								$clone_cfvideo = 'copy-' . $db->get_random_string($random_st_len) . $cfeed_data['video'];
								@copy($uploadpath . $cfeed_data['video'], $uploadpath . $clone_cfvideo);
							endif;

							#----copy-screen----
							if ( ! empty($cfeed_data['screen']) && file_exists($root_path . $cfeed_data['screen'])):
								$clone_cfscreen = 'copy-' . $db->get_random_string($random_st_len) . $cfeed_data['screen'];
								@copy($uploadpath . $cfeed_data['screen'], $uploadpath . $clone_cfscreen);
							endif;

							#----copy-image----
							if ( ! empty($cfeed_data['image']) && file_exists($root_path . $cfeed_data['image'])):
								$clone_cfimage = 'copy-' . $db->get_random_string($random_st_len) . $cfeed_data['image'];
								@copy($uploadpath . $cfeed_data['image'], $uploadpath . $clone_cfimage);
							endif;

							#----copy-document----
							if ( ! empty($cfeed_data['document']) && file_exists($root_path . $cfeed_data['document'])):
								$clone_cfdoc = 'copy-' . $db->get_random_string($random_st_len) . $cfeed_data['document'];
								@copy($uploadpath . $cfeed_data['document'], $uploadpath . $clone_cfdoc);
							endif;
							
							$cfeedData = array('qid'			=> $last_ques_id,
											   'aid'			=> $last_ans_id,
											   'feedback'		=> ( ! empty($cfeed_data['feedback'])) ? $cfeed_data['feedback'] : '',
											   'ftype'			=> 1,
											   'speech_text'	=> ( ! empty($cfeed_data['feed_speech_text'])) ? $cfeed_data['feed_speech_text'] : '',
											   'audio'			=> ( ! empty($clone_cfaudio)) ? $clone_cfaudio : '',
											   'video'			=> ( ! empty($clone_cfvideo)) ? $clone_cfvideo : '',
											   'screen'			=> ( ! empty($clone_cfscreen)) ? $clone_cfscreen : '',
											   'image'			=> ( ! empty($clone_cfimage)) ? $clone_cfimage : '',
											   'document'		=> ( ! empty($clone_cfdoc)) ? $clone_cfdoc : '');
							$cfeed_sql  = "INSERT INTO `feedback_tbl` (`question_id`, `answer_id`, `feedback`, `feedback_type`, `feed_speech_text`, `feed_audio`, `feed_video`, `feed_screen`, `feed_image`, `feed_document`) 
											VALUES (:qid, :aid, :feedback, :ftype, :speech_text, :audio, :video, :screen, :image, :document)";
							$cfeed_stmt = $db->prepare($cfeed_sql); $cfeed_stmt->execute($cfeedData);
						endif;
					endforeach;
				endif;
			endforeach;
		endif;
		$res = array('success' => TRUE, 'msg' => 'Simulation copied successfully.');
	else:
		$res = array('success' => FALSE, 'msg' => 'Simulation not copied. Please try again later!');
	endif;
	echo json_encode($res);
endif;

#------------------------------------------Delete-Questions---------------------------------------
if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['del_Question']) && isset($_POST['ques_id'])):
	$ques_id = (isset($_POST['ques_id'])) ? $_POST['ques_id'] : '';
	if ( ! empty($ques_id)):
		$sql  = "DELETE t1.*, t2.*, t3.* ";
		$sql .= "FROM question_tbl t1 
				 LEFT JOIN answer_tbl t2 ON t1.question_id = t2.question_id 
				 LEFT JOIN sub_options_tbl t3 ON t1.question_id = t3.question_id 
				 WHERE t1.question_id IN (". $ques_id .")";
		$q = $db->prepare($sql);
		if ($q->execute()):
			$res = array('success' => TRUE, 'msg' => 'Question deleted successfully!');
		else:
			$res = array('success' => FALSE, 'msg' => 'Question not deleted. Please try again later!');
		endif;
	else:
		$res = array('success' => FALSE, 'msg' => 'Please select at-least one Question');
	endif;
	echo json_encode($res);
endif;

#---------------------------Reset-Learner-Data----------------------------
if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['reset_sim_data'])):
	extract($_POST);
	if (empty($reset_category) || empty($reset_sim_id) || empty($reset_user_id)):
		$resdata = array('success' => TRUE, 'msg' => "Please select all required fields.");
	elseif ( ! empty($reset_category) && ! empty($reset_sim_id) && ! empty($reset_user_id)):
		$get_reset_user_id = $db->addMultiIds($reset_user_id);
		$sql  = "DELETE t1.*, t2.*, t3.*, t4.* FROM ";
		$sql .= "scenario_attempt_tbl AS t1 LEFT JOIN 
				 score_tbl AS t2 ON t1.uid = t2.uid 
				 LEFT JOIN multi_score_tbl AS t3 ON t1.uid = t3.uid 
				 LEFT JOIN open_res_tbl AS t4 ON t1.uid = t4.uid 
				 WHERE t1.scenario_id = '". $reset_sim_id ."' AND t1.userid IN ($get_reset_user_id)";
		$q = $db->prepare($sql);
		if ($q->execute()):
			$resdata = array('success' => TRUE, 'msg' => 'Data reset successfully!');
		else:
			$resdata = array('success' => FALSE, 'msg' => 'Data not reset. Please try again later!');
		endif;
	endif;
	echo json_encode($resdata);
endif;

#-------------------------Set-Questions-List-Order------------------
if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['qorder'])):
	extract($_POST);
	if ($question):
		foreach ($question as $k => $v):
			$order	= $k + 1;
			$sql	= "UPDATE question_tbl SET qorder = '". $order ."' WHERE question_id = '". $v ."' AND MD5(scenario_id) = '". $sim_id ."'";
			$qstmt	= $db->prepare($sql);
			$res 	= $qstmt->execute();
		endforeach;
		if ($res):
			$resdata = array('success' => TRUE, 'msg' => 'Order update successfully!');
		else:
			$resdata = array('success' => FALSE, 'msg' => 'Order not update. Please try again later!');
		endif;
	else:
		$resdata = array('success' => FALSE, 'msg' => 'Order not update. Please try again later!');
	endif;
	echo json_encode($resdata);
endif;

#-----------------------------Add-CMS-Page-Data--------------------------
if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['add_page_data'])):
	extract($_POST);
	$data = array('img'		=> ( ! empty($bg_file)) ? $bg_file : '',
				  'content'	=> ( ! empty($page_des)) ? $page_des : '',
				  'status' 	=> 1,
				  'cdate'	=> $curdate,
				  'pid'		=> $cms_page_id);
	$sql  = "UPDATE `cms_page_tbl` SET `bg_img`=:img, `page_content`=:content, `status`=:status, `added_date_time`=:cdate WHERE cms_page_id=:pid";
	$stmt = $db->prepare($sql);
	if ($stmt->execute($data)):
		$resdata = array('success' => TRUE, 'msg' => 'Page update successfully!', 'return_url' => 'home-page-management.php');
	else:
		$resdata = array('success' => FALSE, 'msg' => 'Page not update. Please try again later!');
	endif;
	echo json_encode($resdata);
endif;

#-----------------------------Get-Learner-SIM--------------------------
if ($_SERVER['REQUEST_METHOD'] == 'GET' && isset($_GET['getLearnerSim'])):
	$sid = $db->getAssignSimData($userId);
	if ( ! empty($sid)):
		$sql = "SELECT scenario_id, Scenario_title FROM scenario_master WHERE scenario_id IN ($sid) AND scenario_id IN (SELECT scenario_id FROM scenario_attempt_tbl WHERE userid = '". $userId ."')";
		$exe = $db->prepare($sql); $exe->execute();
		if ($exe->rowCount() > 0):
			foreach ($exe->fetchAll(PDO::FETCH_ASSOC) as $row):
				$data[] = array('value' => $row['scenario_id'], 'label' => $row['Scenario_title'], 'title' => $row['Scenario_title']);
			endforeach;
			$res = array('success' => TRUE, 'data' => $data);
		else:
			$res = array('success' => FALSE, 'data' => NULL);
		endif;
	else:
		$res = array('success' => FALSE, 'data' => NULL);
	endif;
	echo json_encode($res);
endif;

#-----------------------Check-Competency-Score--------------------------
if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['checkScore'])):
	extract($_POST);
	$sid 		= md5($sim_id);
	$competency = $db->getScenarioCompetency($sid);
	$question	= $db->getScenarioQuestion($sid);
	$res		= [];
	$comp_col	= '';
	$c1 = ( ! empty($competency['comp_val_1'])) ? $competency['comp_val_1'] : 0;
	$q1	= ( ! empty($question['ques_val_1'])) ? $question['ques_val_1'] : 0;
	$c2 = ( ! empty($competency['comp_val_2'])) ? $competency['comp_val_2'] : 0;
	$q2	= ( ! empty($question['ques_val_2'])) ? $question['ques_val_2'] : 0;
	$c3 = ( ! empty($competency['comp_val_3'])) ? $competency['comp_val_3'] : 0;
	$q3	= ( ! empty($question['ques_val_3'])) ? $question['ques_val_3'] : 0;
	$c4 = ( ! empty($competency['comp_val_4'])) ? $competency['comp_val_4'] : 0;
	$q4	= ( ! empty($question['ques_val_4'])) ? $question['ques_val_4'] : 0;
	$c5 = ( ! empty($competency['comp_val_5'])) ? $competency['comp_val_5'] : 0;
	$q5	= ( ! empty($question['ques_val_5'])) ? $question['ques_val_5'] : 0;
	$c6 = ( ! empty($competency['comp_val_6'])) ? $competency['comp_val_6'] : 0;
	$q6	= ( ! empty($question['ques_val_6'])) ? $question['ques_val_6'] : 0;
	if ( ! empty($c1) && ! empty($q1) && $q1 > $c1):
		$comp_col = $competency['comp_col_1'];
	elseif ( ! empty($c2) && ! empty($q2) && $q2 > $c2):
		$comp_col = $competency['comp_col_2'];
	elseif ( ! empty($c3) && ! empty($q3) && $q3 > $c3):
		$comp_col = $competency['comp_col_3'];
	elseif ( ! empty($c4) && ! empty($q4) && $q4 > $c4):
		$comp_col = $competency['comp_col_4'];
	elseif ( ! empty($c5) && ! empty($q5) && $q5 > $c5):
		$comp_col = $competency['comp_col_5'];
	elseif ( ! empty($c6) && ! empty($q6) && $q6 > $c6):
		$comp_col = $competency['comp_col_6'];
	endif;
	$msg = ( ! empty($comp_col)) ? ['success' => TRUE, 'msg' => 'Assigned '. $comp_col .' score is more than remaining competency score. Please check and correct.'] : ['success' => FALSE];
	echo json_encode($msg);
endif;

ob_flush();
$db->closeConnection();
unset($db);
