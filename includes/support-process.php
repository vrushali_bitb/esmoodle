<?php 
ob_start();
session_start();
/******************* Class ********************/
require_once dirname(dirname(__FILE__)) . '/config/db.class.php';
require_once dirname(dirname(__FILE__)) . '/config/src/class.upload.php';
require_once dirname(__FILE__) . '/mailer/Send_Mail.php';

/******************* DBConnection ********************/
$db 				= new DBConnection();
$user				= (isset($_SESSION['username'])) ? $_SESSION['username'] : '';
$role				= (isset($_SESSION['role'])) ? $_SESSION['role'] : '';
$userId				= (isset($_SESSION['userId'])) ? $_SESSION['userId'] : '';
$domain 			= (isset($_SESSION['domain_name'])) ? $_SESSION['domain_name'] : $_SERVER['HTTP_HOST'];
$valid_exts			= array('jpeg', 'jpg', 'png', 'pdf');
$video_valid_exts	= array('flv', 'mp4');
$audio_valid_exts	= array('mp3', 'MP3');
$curdate			= date('Y-m-d H:i:s a');

/******************* Folder Path ********************/
$uploadpath 		= dirname(dirname(__FILE__)) . '/scenario/attachment/'.$domain.'/';

/******************* Create folder if not exist ********************/
if ( ! is_dir($uploadpath)) mkdir($uploadpath, 0777, TRUE);

/*********** Attachment Files ************/
if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_FILES) && isset($_POST['support_attachment']) && ! isset($_POST['support'])):
	$file = (isset($_FILES['file'])) ? $_FILES['file'] : '';
	$error  = FALSE;
	if ( empty($file['name'])):
		$resdata = array('success' => FALSE, 'msg' => 'Please select file.');
		$error = TRUE;
	elseif ($error == FALSE && ! empty($file) && ! in_array(strtolower(pathinfo($file['name'], PATHINFO_EXTENSION)), $valid_exts)):
		$resdata = array('success' => FALSE, 'msg' => 'Please select valid file. accept only jpeg, jpg, png, pdf');
		$error = TRUE;
	elseif ($error == FALSE && ! empty($file) && $file['size'] > 1048576):
		$resdata = array('success' => FALSE, 'msg' => 'File size must be less than 1 MB.');
		$error = TRUE;
	elseif ($error == FALSE):
		$handle = new Upload($file);
		if ($handle->uploaded) {
			$handle->file_safe_name 	= TRUE;
			$handle->file_overwrite 	= FALSE;
			$handle->file_auto_rename	= TRUE;
			$handle->Process($uploadpath);
			if ($handle->processed) {
				$file_dst_name = $handle->file_dst_name;
			}
			$handle->Clean();
		}
	$resdata = ( ! empty($file_dst_name)) ? array('success' => TRUE, 'file_name' => $file_dst_name, 'msg' => 'Attachment upload successfully.', 'path' => $db->getBaseUrl('../scenario/attachment/'.$domain.'/')) : array('success' => FALSE, 'msg' => 'Attachment not upload. try again later.');
	else:
		$resdata = array('success' => FALSE, 'msg' => 'Oops. something went wrong please try again.?');
	endif;
	echo json_encode($resdata);
endif;

/*********** Send-Support-Mail ***********/
if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['support'])):
	extract($_POST);
	$error = FALSE;
	if (empty($support_subject)):
		$resdata = array('success' => FALSE, 'msg' => 'Subject cannot be blank.');
		$error = TRUE;
	elseif (empty($support_email_des)):
		$resdata = array('success' => FALSE, 'msg' => 'Description cannot be blank.');
		$error = TRUE;
	elseif ($error == FALSE):
		$email 		= ($role == 'admin') ? MAIN_SUPPORT_MAIL : CLIENT_SUPPORT_MAIL;
		$attachment	= ( ! empty($support_attachment)) ? $uploadpath. $support_attachment : '';
		if ($role == 'admin'):
			$template = $support_email_des;
			$subject  = $support_subject;
			$values   = array('logo'	=> TEMPLATE_HEADER_LOGO,
							  'url'		=> LOGIN_URL,
							  'cmail'	=> CONTACT_EMAIL,
							  'tname'	=> FTITLE,
							  'subject'	=> $subject);
		else:
			$getTempData = $db->getEmailTemplate(NULL, 'support');
			$revData	 = $db->getAssigneeName($userId);
			$template	 = $getTempData['tmp_des'];
			$subject	 = ( ! empty($support_subject)) ? $support_subject : $getTempData['subject'];
			$values 	 = array('name'		=> $revData['username'],
								 'email'	=> ( ! empty($revData['email'])) ? $revData['email'] : '',
								 'role'		=> ( ! empty($revData['role'])) ? $revData['role'] : '',
								 'msg'		=> ( ! empty($support_email_des)) ? $support_email_des : '',
								 'logo'		=> TEMPLATE_HEADER_LOGO,
								 'url'		=> LOGIN_URL,
								 'cmail'	=> CONTACT_EMAIL,
								 'tname'	=> FTITLE,
								 'subject'	=> $subject);
		endif;
		if (sendSupportMail($email, $template, $values, $subject, $attachment)):
			$data = array('subject'		=> ( ! empty($subject)) ? $subject : '',
						  'des'			=> ( ! empty($support_email_des)) ? $support_email_des : '',
						  'attachment'	=> ( ! empty($support_attachment)) ? $support_attachment : '',
						  'status'		=> 1,
						  'uid'			=> $userId,
						  'datetime'	=> $curdate);
			$sql = "INSERT INTO `support_tbl` (`subject`, `des`, `attachment`, `status`, `user_id`, `datetime`) 
			VALUES (:subject, :des, :attachment, :status, :uid, :datetime)";
			$stmt = $db->prepare($sql);
			$stmt->execute($data);			
			$resdata = array('success' => TRUE, 'msg' => 'Support mail sent.');
		else:
			$resdata = array('success' => FALSE, 'msg' => 'Support mail sent!. try again later.');
		endif;
	else:
		$resdata = array('success' => FALSE, 'msg' => 'Oops. something went wrong please try again.?');
	endif;
	echo json_encode($resdata);
endif;

/*********** Update Status ***********/
if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['update_Support_Status']) && ! empty($_POST['support_id'])):
	extract($_POST);
	$sql = "UPDATE support_tbl SET status = '". $supportStatus ."' WHERE support_id = '". $support_id ."'";
    $results = $db->prepare($sql);
	if ($results->execute()):
		$resdata = array('success' => TRUE, 'msg' => 'Status successfully updated.');
	else:
		$resdata = array('success' => FALSE, 'msg' => 'Status not updated. try again later.');
	endif;
	echo json_encode($resdata);
endif;

ob_flush();
