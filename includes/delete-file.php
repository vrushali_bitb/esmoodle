<?php 
ob_start(); 
session_start();
$user  	= (isset($_SESSION['username'])) ? $_SESSION['username'] : '';
$role   = (isset($_SESSION['role'])) ? $_SESSION['role'] : '';
$userId	= (isset($_SESSION['userId'])) ? $_SESSION['userId'] : '';
$domain = (isset($_SESSION['domain_name'])) ? $_SESSION['domain_name'] : $_SERVER['HTTP_HOST'];

/******************* Folder Path ********************/
$uploadpath 		= dirname(dirname(__FILE__)) . '/scenario/upload/'.$domain.'/';
$type_icon_path		= dirname(dirname(__FILE__)) . '/img/type_icon/';
$group_icon_path	= dirname(dirname(__FILE__)) . '/img/group_icon/';
$client_logo_path	= dirname(dirname(__FILE__)) . '/img/logo/';
$attachment_path 	= dirname(dirname(__FILE__)) . '/scenario/attachment/'.$domain.'/';

if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['delete']) && isset($_POST['file'])):
	$file = (isset($_POST['file'])) ? $_POST['file'] : '';
	if ( ! empty($file) && @unlink($uploadpath . $file)):
		$res = array('success' => TRUE, 'msg' => 'File deleted successfully. Please Save & continue');
	else:
		$res = array('success' => FALSE, 'msg' => "Error: File doesn't exist.");
	endif;
	echo json_encode($res);
endif;

if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['type_icon'])):
	$file = (isset($_POST['type_icon'])) ? $_POST['type_icon'] : '';
	if ( ! empty($file) && @unlink($type_icon_path . $file)):
		$res = array('success' => TRUE, 'msg' => 'Icon deleted successfully.');
	else:
		$res = array('success' => FALSE, 'msg' => "Error: File doesn't exist.");
	endif;
	echo json_encode($res);
endif;

if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['group_icon'])):
	$file = (isset($_POST['group_icon'])) ? $_POST['group_icon'] : '';
	if ( ! empty($file) && @unlink($group_icon_path . $file)):
		$res = array('success' => TRUE, 'msg' => 'Icon deleted successfully.');
	else:
		$res = array('success' => FALSE, 'msg' => "Error: File doesn't exist.");
	endif;
	echo json_encode($res);
endif;

if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['scenario_media_file'])):
	$file = (isset($_POST['scenario_media_file'])) ? $_POST['scenario_media_file'] : '';
	if ( ! empty($file) && @unlink($uploadpath . $file)):
		$res = array('success' => TRUE, 'msg' => 'File deleted successfully.');
	else:
		$res = array('success' => FALSE, 'msg' => "Error: File doesn't exist.");
	endif;
	echo json_encode($res);
endif;

if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['client_logo_file'])):
	$file = (isset($_POST['client_logo_file'])) ? $_POST['client_logo_file'] : '';
	if ( ! empty($file) && @unlink($client_logo_path . $file)):
		$res = array('success' => TRUE, 'msg' => 'Logo deleted successfully.');
	else:
		$res = array('success' => FALSE, 'msg' => "Error: File doesn't exist.");
	endif;
	echo json_encode($res);
endif;

if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['rec_audio'])):
	$file = (isset($_POST['rec_audio'])) ? $_POST['rec_audio'] : '';
	if ( ! empty($file) && @unlink($uploadpath . $file)):
		$res = array('success' => TRUE, 'msg' => 'Recording deleted successfully. Please Save & continue');
	else:
		$res = array('success' => FALSE, 'msg' => "Error: File doesn't exist.");
	endif;
	echo json_encode($res);
endif;

if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['rec_video'])):
	$file = (isset($_POST['rec_video'])) ? $_POST['rec_video'] : '';
	if ( ! empty($file) && @unlink($uploadpath . $file)):
		$res = array('success' => TRUE, 'msg' => 'Recording deleted successfully. Please Save & continue');
	else:
		$res = array('success' => FALSE, 'msg' => "Error: File doesn't exist.");
	endif;
	echo json_encode($res);
endif;

if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['rec_screen'])):
	$file = (isset($_POST['rec_screen'])) ? $_POST['rec_screen'] : '';
	if ( ! empty($file) && @unlink($uploadpath . $file)):
		$res = array('success' => TRUE, 'msg' => 'Recording deleted successfully. Please Save & continue');
	else:
		$res = array('success' => FALSE, 'msg' => "Error: File doesn't exist.");
	endif;
	echo json_encode($res);
endif;

if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['delete_assets'])):
	$path = (isset($_POST['assets_path'])) ? $_POST['assets_path'] : ''; 
	$file = (isset($_POST['file'])) ? $_POST['file'] : '';
	if ( ! empty($file) && @unlink($path . $file)):
		$res = array('success' => TRUE, 'msg' => 'File deleted successfully. Please Save & continue');
	else:
		$res = array('success' => FALSE, 'msg' => "Error: File doesn't exist.");
	endif;
	echo json_encode($res);
endif;

if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['attachment_file'])):
	$file = (isset($_POST['attachment_file'])) ? $_POST['attachment_file'] : '';
	if ( ! empty($file) && @unlink($attachment_path . $file)):
		$res = array('success' => TRUE, 'msg' => 'File deleted successfully.');
	else:
		$res = array('success' => FALSE, 'msg' => "Error: File doesn't exist.");
	endif;
	echo json_encode($res);
endif;

if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['wb_file'])):
	$file = (isset($_POST['wb_file'])) ? $_POST['wb_file'] : '';
	$webf = $uploadpath . $file;
	function rrmdir($dir) {
		if (is_dir($dir)) {
			$objects = scandir($dir);
			foreach ($objects as $object) {
			  if ($object != "." && $object != "..") {
				if (filetype($dir."/".$object) == "dir") 
				   rrmdir($dir."/".$object);
				else unlink($dir."/".$object);
			  }
			}
			reset($objects);
			rmdir($dir);
			return TRUE;
		  }
	}
	if ( ! empty($file) && rrmdir($webf)):
		$res = array('success' => TRUE, 'msg' => 'Web Object file deleted successfully.');
	else:
		$res = array('success' => FALSE, 'msg' => "Error: Web Object file doesn't exist.");
	endif;
	echo json_encode($res);
endif;
