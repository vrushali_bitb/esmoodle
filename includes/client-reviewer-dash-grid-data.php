<?php 
session_start();

/******************* Class ********************/
require_once dirname(dirname(__FILE__)) . '/config/db.class.php';

/******************* DBConnection ********************/
$db 		= new DBConnection();
$user		= (isset($_SESSION['username'])) ? $_SESSION['username'] : '';
$role		= (isset($_SESSION['role'])) ? $_SESSION['role'] : '';
$userId		= (isset($_SESSION['userId'])) ? $_SESSION['userId'] : '';
$cid		= (isset($_SESSION['client_id'])) ? $_SESSION['client_id'] : exit('Oops. something went wrong please try again');

//-------------------------Get-Auth-Default-Data--------------------
if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['getReviewerDashData'])):
	$date		= ( ! empty($_POST['date'])) ? explode(' - ', $_POST['date']) : exit('error');
	$sdate		= $date[0];
	$edate		= $date[1];
	$limit		= '0, 5';
	$resdata	= $review_sim = $ready_publish_sim = $publish_sim = $ready_review_sim = [];

	/* IN REVIEW */
	$review_sql = "SELECT scenario_id, Scenario_title AS label FROM scenario_master WHERE status = 14 AND FIND_IN_SET($userId, assign_reviewer) AND DATE(modified_on) BETWEEN '". $sdate ."' AND '". $edate ."'";
	$review_res = $db->prepare($review_sql); $review_res->execute();
	foreach ($review_res->fetchAll(PDO::FETCH_ASSOC) as $row):
		$review_sim[] = ['y' => 1, 'name' => ucwords($row['label'])];
	endforeach;
	
	/* READY TO PUBLISH */
	$ready_sql = "SELECT scenario_id, Scenario_title AS label FROM scenario_master WHERE status = 5 AND FIND_IN_SET($userId, assign_reviewer) AND DATE(modified_on) BETWEEN '". $sdate ."' AND '". $edate ."'";
	$ready_res = $db->prepare($ready_sql); $ready_res->execute();
	foreach ($ready_res->fetchAll(PDO::FETCH_ASSOC) as $row):
		$ready_publish_sim[] = ['y' => 1, 'name' => ucwords($row['label'])];
	endforeach;

	/* PUBLISH */
	$publish_sql = "SELECT scenario_id, Scenario_title AS label FROM scenario_master WHERE status = 8 AND FIND_IN_SET($userId, assign_reviewer) AND DATE(modified_on) BETWEEN '". $sdate ."' AND '". $edate ."'";
	$publish_res = $db->prepare($publish_sql); $publish_res->execute();
	foreach ($publish_res->fetchAll(PDO::FETCH_ASSOC) as $row):
		$publish_sim[] = ['y' => 1, 'name' => ucwords($row['label'])];
	endforeach;

	/* Ready to Review */
	$ready_rew_sql = "SELECT scenario_id, Scenario_title AS label FROM scenario_master WHERE status = 7 AND FIND_IN_SET($userId, assign_reviewer) AND DATE(modified_on) BETWEEN '". $sdate ."' AND '". $edate ."'";
	$ready_rew_res = $db->prepare($ready_rew_sql); $ready_rew_res->execute();
	foreach ($ready_rew_res->fetchAll(PDO::FETCH_ASSOC) as $row):
		$ready_review_sim[] = ['y' => 1, 'name' => ucwords($row['label'])];
	endforeach;

	$resdata = ['success' => TRUE, 
				'data'	  => ['review_sim'			=> $review_sim,
							  'ready_publish_sim'	=> $ready_publish_sim,
							  'publish_sim'			=> $publish_sim,
							  'ready_review_sim'	=> $ready_review_sim
							  ]
				];
	echo json_encode($resdata, JSON_NUMERIC_CHECK);
endif;

$db->closeConnection();
