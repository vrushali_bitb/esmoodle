<?php 
ini_set('max_execution_time', 0); // 0 = Unlimited
session_start();

/******************* Class ********************/
require_once dirname(dirname(__FILE__)) . '/config/db.class.php';
require_once dirname(dirname(__FILE__)) . '/config/PHPExcel.php';
require_once dirname(dirname(__FILE__)) . '/config/PHPExcel/IOFactory.php';

/******************* DBConnection ********************/
$db 		= new DBConnection();
$user		= (isset($_SESSION['username'])) ? $_SESSION['username'] : '';
$role		= (isset($_SESSION['role'])) ? $_SESSION['role'] : '';
$userId = (isset($_SESSION['userId'])) ? $_SESSION['userId'] : '';
$cid		= (isset($_SESSION['client_id'])) ? $_SESSION['client_id'] : exit('Oops. something went wrong please try again');

/* Group Performance: Admin */
if ($_SERVER['REQUEST_METHOD'] == 'POST' && $_POST['export_type'] == 1):
  extract($_POST);
  if (empty($fdate) && empty($tdate)):
    $_SESSION['msg'] = 'Please select start or end date.!';
    header('location: ../group-performance-report.php');
    exit;
  elseif (empty($scenario)):
      $_SESSION['msg'] = 'Please select Simulation.';
      header('location: ../group-performance-report.php');
      exit;
  elseif ( ! empty($fdate) && ! empty($tdate)):
    $earlier  = new DateTime($fdate);
    $later    = new DateTime($tdate);
    $abs_diff = $later->diff($earlier)->format("%a");
    if ($abs_diff > 30):
      $_SESSION['msg'] = 'please select between one month.!';
      header('location: ../group-performance-report.php');
      exit;
    endif;
  endif;
  
  $limit            =  2;
  $get_filter_type  = '';
  $show_filter_data = '';
  $get_sim_id       = $db->addMultiIds($scenario);
  if ($filter_type == 1):
    #----System-Wise-------
    $get_filter_type = 'Systemwide';
    $sql  = "SELECT GROUP_CONCAT(DISTINCT IF(group_id = '', null, group_id)) AS gid, GROUP_CONCAT(DISTINCT IF(learner_id = '', null, learner_id)) AS lid FROM assignment_tbl WHERE scenario_id IN ($get_sim_id)";
    $exc  = $db->prepare($sql); $exc->execute();
    $row  = $exc->fetch(PDO::FETCH_ASSOC);
    $gid  = ( ! empty($row['gid'])) ? rtrim($row['gid'], ',') : '';
    $lid  = ( ! empty($row['lid'])) ? rtrim($row['lid'], ',') : '';
    foreach ($db->getLearnerByGroup($gid) as $gdata):
      $getlearnerId[] = $gdata['learner'];
    endforeach;
    if ( ! empty($tdate) && ! empty($fdate)):
      $get_learner_id = $db->addMultiIds($getlearnerId);
      $date_sql = "SELECT GROUP_CONCAT(userid) AS learner FROM scenario_attempt_tbl WHERE scenario_id IN ($get_sim_id) AND userid IN ($get_learner_id) AND DATE(attempt_date) BETWEEN '". $fdate ."' AND '". $tdate ."'";
      $date_exc = $db->prepare($date_sql); $date_exc->execute();
      $date_row = $date_exc->fetch(PDO::FETCH_ASSOC);
      $get_learner = $date_row['learner'];
    elseif ( ! empty($tdate) && empty($fdate)):
      $get_learner_id = $db->addMultiIds($getlearnerId);
      $date_sql = "SELECT GROUP_CONCAT(userid) AS learner FROM scenario_attempt_tbl WHERE scenario_id IN ($get_sim_id) AND userid IN ($get_learner_id) AND DATE(attempt_date) = '". $tdate ."'";
      $date_exc = $db->prepare($date_sql); $date_exc->execute();
      $date_row = $date_exc->fetch(PDO::FETCH_ASSOC);
      $get_learner = $date_row['learner'];
    elseif ( ! empty($fdate) && empty($tdate)):
      $get_learner_id = $db->addMultiIds($getlearnerId);
      $date_sql = "SELECT GROUP_CONCAT(userid) AS learner FROM scenario_attempt_tbl WHERE scenario_id IN ($get_sim_id) AND userid IN ($get_learner_id) AND DATE(attempt_date) = '". $fdate ."'";
      $date_exc = $db->prepare($date_sql); $date_exc->execute();
      $date_row = $date_exc->fetch(PDO::FETCH_ASSOC);
      $get_learner = $date_row['learner'];
    else:
      $get_learner = $db->addMultiIds($getlearnerId);
    endif;
  elseif ($filter_type == 2):
    #----Educator-------
    $get_filter_type  = 'Educator';
    $show_filter_data = $db->addMultiIds($get_educator);
    if ( ! empty($tdate) && ! empty($fdate)):
      $get_learner_id = $db->addMultiIds($educator);
      $date_sql = "SELECT GROUP_CONCAT(userid) AS learner FROM scenario_attempt_tbl WHERE scenario_id IN ($get_sim_id) AND userid IN ($get_learner_id) AND DATE(attempt_date) BETWEEN '". $fdate ."' AND '". $tdate ."'";
      $date_exc = $db->prepare($date_sql); $date_exc->execute();
      $date_row = $date_exc->fetch(PDO::FETCH_ASSOC);
      $get_learner = $date_row['learner'];
    elseif ( ! empty($tdate) && empty($fdate)):
      $get_learner_id = $db->addMultiIds($educator);
      $date_sql = "SELECT GROUP_CONCAT(userid) AS learner FROM scenario_attempt_tbl WHERE scenario_id IN ($get_sim_id) AND userid IN ($get_learner_id) AND DATE(attempt_date) = '". $tdate ."'";
      $date_exc = $db->prepare($date_sql); $date_exc->execute();
      $date_row = $date_exc->fetch(PDO::FETCH_ASSOC);
      $get_learner = $date_row['learner'];
    elseif ( ! empty($fdate) && empty($tdate)):
      $get_learner_id = $db->addMultiIds($educator);
      $date_sql = "SELECT GROUP_CONCAT(userid) AS learner FROM scenario_attempt_tbl WHERE scenario_id IN ($get_sim_id) AND userid IN ($get_learner_id) AND DATE(attempt_date) = '". $fdate ."'";
      $date_exc = $db->prepare($date_sql); $date_exc->execute();
      $date_row = $date_exc->fetch(PDO::FETCH_ASSOC);
      $get_learner = $date_row['learner'];
    else:
      $get_learner = $db->addMultiIds($educator);
    endif;
  elseif ($filter_type == 7):
      #----Group-------
      $get_filter_type  = 'Group';
      if ( ! empty($tdate) && ! empty($fdate)):
        $get_learner_id = $db->addMultiIds($group);
        $date_sql = "SELECT GROUP_CONCAT(userid) AS learner FROM scenario_attempt_tbl WHERE scenario_id IN ($get_sim_id) AND userid IN ($get_learner_id) AND DATE(attempt_date) BETWEEN '". $fdate ."' AND '". $tdate ."'";
        $date_exc = $db->prepare($date_sql); $date_exc->execute();
        $date_row = $date_exc->fetch(PDO::FETCH_ASSOC);
        $get_learner = $date_row['learner'];
      elseif ( ! empty($tdate) && empty($fdate)):
        $get_learner_id = $db->addMultiIds($group);
        $date_sql = "SELECT GROUP_CONCAT(userid) AS learner FROM scenario_attempt_tbl WHERE scenario_id IN ($get_sim_id) AND userid IN ($get_learner_id) AND DATE(attempt_date) = '". $tdate ."'";
        $date_exc = $db->prepare($date_sql); $date_exc->execute();
        $date_row = $date_exc->fetch(PDO::FETCH_ASSOC);
        $get_learner = $date_row['learner'];
      elseif ( ! empty($fdate) && empty($tdate)):
        $get_learner_id = $db->addMultiIds($group);
        $date_sql = "SELECT GROUP_CONCAT(userid) AS learner FROM scenario_attempt_tbl WHERE scenario_id IN ($get_sim_id) AND userid IN ($get_learner_id) AND DATE(attempt_date) = '". $fdate ."'";
        $date_exc = $db->prepare($date_sql); $date_exc->execute();
        $date_row = $date_exc->fetch(PDO::FETCH_ASSOC);
        $get_learner = $date_row['learner'];
      else:
        $get_learner = $db->addMultiIds($group);
      endif;
  elseif ($filter_type == 3):
    #----Learner------
    $get_filter_type  = 'Learner';
    $get_learner      = $db->addMultiIds($learner);
  elseif ($filter_type == 4 || $filter_type == 5 || $filter_type == 6):
    #----Unit------
    if ($filter_type == 4):
      $get_filter_type  = 'Unit';
      $get_filter_data  = implode("','", $unit);
      $show_filter_data = $db->addMultiIds($unit);
      $column           = 'department';
    elseif ($filter_type == 5):
      #----Location------
      $get_filter_type  = 'Location';
      $get_filter_data  = implode("','", $location);
      $show_filter_data = $db->addMultiIds($location);
      $column           = 'location';
    elseif ($filter_type == 6):
      #----Designation-----
      $get_filter_type  = 'Designation';
      $get_filter_data  = implode("','", $designation);
      $show_filter_data = $db->addMultiIds($designation);
      $column           = 'designation';
    endif;
    $sql = "SELECT GROUP_CONCAT(DISTINCT IF(group_id = '', null, group_id)) AS gid, GROUP_CONCAT(DISTINCT IF(learner_id = '', null, learner_id)) AS lid FROM assignment_tbl WHERE scenario_id IN ($get_sim_id)";
    $exc = $db->prepare($sql); $exc->execute();
    $row = $exc->fetch(PDO::FETCH_ASSOC);
    $gid = ( ! empty($row['gid'])) ? rtrim($row['gid'], ',') : '';
    $lid = ( ! empty($row['lid'])) ? rtrim($row['lid'], ',') : '';
    foreach ($db->getLearnerByGroup($gid) as $gdata) :
      $getlearnerId[] = $gdata['learner'];
    endforeach;
    $get_unit_learner   = $db->addMultiIds($getlearnerId);
    $cond               = "$column IN ('$get_filter_data')";
    $get_other_learner  = $db->GetUsersByFilter($get_unit_learner, 'id', $cond);
    if ( ! empty($tdate) && ! empty($fdate)):
      $get_learner_id = $db->addMultiIds($get_other_learner);
      $date_sql = "SELECT GROUP_CONCAT(userid) AS learner FROM scenario_attempt_tbl WHERE scenario_id IN ($get_sim_id) AND userid IN ($get_learner_id) AND DATE(attempt_date) BETWEEN '". $fdate ."' AND '". $tdate ."'";
      $date_exc = $db->prepare($date_sql); $date_exc->execute();
      $date_row = $date_exc->fetch(PDO::FETCH_ASSOC);
      $get_learner = $date_row['learner'];
    elseif ( ! empty($tdate) && empty($fdate)):
      $get_learner_id = $db->addMultiIds($get_other_learner);
      $date_sql = "SELECT GROUP_CONCAT(userid) AS learner FROM scenario_attempt_tbl WHERE scenario_id IN ($get_sim_id) AND userid IN ($get_learner_id) AND DATE(attempt_date) = '". $tdate ."'";
      $date_exc = $db->prepare($date_sql); $date_exc->execute();
      $date_row = $date_exc->fetch(PDO::FETCH_ASSOC);
      $get_learner = $date_row['learner'];
    elseif ( ! empty($fdate) && empty($tdate)):
      $get_learner_id = $db->addMultiIds($get_other_learner);
      $date_sql = "SELECT GROUP_CONCAT(userid) AS learner FROM scenario_attempt_tbl WHERE scenario_id IN ($get_sim_id) AND userid IN ($get_learner_id) AND DATE(attempt_date) = '". $fdate ."'";
      $date_exc = $db->prepare($date_sql); $date_exc->execute();
      $date_row = $date_exc->fetch(PDO::FETCH_ASSOC);
      $get_learner = $date_row['learner'];
    else:
      $get_learner = $db->addMultiIds($get_other_learner);
    endif;
  endif;
  $get_all_learner      = explode(',', $get_learner);
  $get_filter_learner   = array_unique($get_all_learner);
  $get_total_learner    = count($get_filter_learner);
  $get_all_learners     = $db->addMultiIds($get_filter_learner);

  /* Generate Excel */
  $fileName = '';
  if ( ! empty($fdate) && ! empty($tdate)):
    $fileName = 'Group-Performance-Report-from-'. $fdate .'-to-'. $tdate;
  elseif ( ! empty($fdate) && empty($tdate)):
    $fileName = 'Group-Performance-Report-from-'. $fdate;
  elseif ( ! empty($tdate) && empty($fdate)):
    $fileName = 'Group-Performance-Report-'. $fdate;
  elseif (empty($fdate) && empty($tdate)):
    $fileName = 'Group-Performance-Report';
  endif;
  $title = 'Group Performance Report';
    
  $objPHPExcel = new PHPExcel();

  $styleHead   = array('font'  => array('bold'  => true,
                                        'size'  => 16,
                                        'color' => array('rgb' => '2f5497'),
                                      ),
                      'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER)
                  );

  $styleSubHead = array('font'  => array('bold'  => true,
                        'size'  => 12
                      ),
                      'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER)
                    );

  $styleArray  = array('font'  => array('bold'  => true,
                                        'size'  => 12,
                                        'color' => array('rgb' => '2f5497'),
                                      ),
                      'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER)
                  );
  
  $styleRRResult = array('font'  => array('bold'  => true,
                                        'size'  => 12,
                                        'color' => array('rgb' => 'FF0000'),
                                      ),
                        'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER)
                      );
  $stylePResult  = array('font'  => array('bold'  => true,
                                        'size'  => 12,
                                        'color' => array('rgb' => '008000'),
                                      ),
                      'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER)
                      );
                    
  $objPHPExcel->getProperties()->setCreator("EasySIM ®")
                ->setLastModifiedBy("EasySIM ®")
                ->setTitle($fileName)
                ->setSubject($title)
                ->setDescription($title ." document for Office XLSX, generated by EasySIM ®.")
                ->setKeywords($title ." for Office")
                ->setCategory($title);

  function getNextCell($cell, $cnt){
      for ($i = 0; $i < $cnt - 1; $i++):
        $cell++;
      endfor;
      return $cell;
    }
      
  if ($get_total_learner > 0):
      $headers = ['#', 'Full Name', 'User Name', 'Company', 'Unit', 'Location', 'Designation'];
      $attempt = ['Result', 'Score', 'Time-Stamp', 'Time Taken'];
      $column  = "id, CONCAT(fname, ' ', lname) AS fullname, username, company, department, location, designation";
      $noofattempt = 2;
      
      $sheet = $objPHPExcel->getActiveSheet();
      $sheet->setCellValue('A1', $title);
      $sheet->mergeCells('A1:I1');
      $sheet->getStyle('A1')->applyFromArray($styleHead);
      
      foreach ($scenario as $sim):
          for ($a = 0; $a < $noofattempt; $a++):
            $headers = array_merge($headers, $attempt);
          endfor;
      endforeach;

      $i = 4; $cell = 'A';
      foreach ($headers as $k => $v):
          $celln = $cell++ . $i;
          $objPHPExcel->getActiveSheet()->setCellValue($celln, $v);
          $objPHPExcel->getActiveSheet()->getStyle($celln)->applyFromArray($styleSubHead);
      endforeach;
      
      $userData = $db->getUsersColumns($get_all_learners, $column);
      
      $lcid = 5; $lsn = 1; $ldata = [];
      foreach ($userData as $cnt => $udata):
        $learner_id = $udata['id'];
        $tmp = [$cnt + 1,
                $udata['fullname'],
                $udata['username'],
                $udata['company'],
                $udata['department'],
                $udata['location'],
                $udata['designation']];
        foreach ($scenario as $sid):
          $report = $db->learnerReport($sid, $learner_id, $limit, $fdate, $tdate);
          $rtemp  = [];
          for ($rt = 0; $rt < $limit; $rt++):
            if (isset($report[$rt])):
              $rtemp = array_merge($rtemp, array_values($report[$rt]));
            else:
              $rtemp = array_merge($rtemp, array_fill(0, count($attempt), ''));
            endif;
          endfor;
          $tmp = array_merge(array_values($tmp), $rtemp);
        endforeach;
        $ldata[] = $tmp;
        $lsn++; $lcid++;
      endforeach;
      
      foreach ($ldata as $k1 => $v1):
        $i = 5 + $k1; $cell = 'A';
        foreach ($v1 as $k2 => $v2):
            $celln = $cell++ . $i;
            $objPHPExcel->getActiveSheet()->setCellValue($celln, $v2);
            if ($v2 == 'RR'):
              $objPHPExcel->getActiveSheet()->getStyle($celln)->applyFromArray($styleRRResult);
            elseif ($v2 == 'P'):
              $objPHPExcel->getActiveSheet()->getStyle($celln)->applyFromArray($stylePResult);
            endif;
        endforeach;
      endforeach;
      
      $l = 'H';
      foreach ($scenario as $sid):
          $sim = $db->getSim('Scenario_title', $sid)[0];
          $i   = 2;
          $s   = getNextCell($l, count($attempt) * $noofattempt);
          $objPHPExcel->getActiveSheet()->setCellValue($l.$i, $sim['Scenario_title']);
          $sheet->mergeCells($l.$i.":".$s.$i);
          $objPHPExcel->getActiveSheet()->getStyle($l.$i)->applyFromArray($styleArray);
          
          $i = 3; $l1 = $l;
          for ($a = 0; $a < $noofattempt; $a++):
              $l1 = getNextCell($l1, (count($attempt) * $a) + 1);
              $s1 = getNextCell($l1, count($attempt));
              $objPHPExcel->getActiveSheet()->setCellValue($l1.$i, 'A'. ($a + 1));
              $sheet->mergeCells($l1.$i.":".$s1.$i);
              $objPHPExcel->getActiveSheet()->getStyle($l1.$i)->applyFromArray($styleArray);
          endfor;
          for ($x = 0; $x < count($attempt) * $noofattempt; $x++):
              $l++; $s++;
          endfor;
      endforeach;

      $objPHPExcel->getActiveSheet()->setTitle($title);
      $objPHPExcel->getActiveSheet()->getTabColor()->setARGB(PHPExcel_Style_Color::COLOR_RED);
      $objPHPExcel->createSheet();
      $fName = $fileName.'.xlsx';
      // Redirect output to a client’s web browser (Excel2007)
      header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
      header('Content-Disposition: attachment;filename="'. $fName .'"');
      header('Cache-Control: max-age=0');
      // If you're serving to IE 9, then the following may be needed
      header('Cache-Control: max-age=1');	
      // If you're serving to IE over SSL, then the following may be needed
      header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
      header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
      header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
      header ('Pragma: public'); // HTTP/1.0	
      $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
      $objWriter->save('php://output');
  endif;
endif;

/* Group Performance: Educator */
if ($_SERVER['REQUEST_METHOD'] == 'POST' && $_POST['export_type'] == 2):
  extract($_POST);
  if (empty($fdate) && empty($tdate)):
    $_SESSION['msg'] = 'Please select start or end date.!';
    header('location: ../group-performance-educator.php');
    exit;
  elseif (empty($scenario)):
      $_SESSION['msg'] = 'Please select Simulation.';
      header('location: ../group-performance-educator.php');
      exit;
  elseif ( ! empty($fdate) && ! empty($tdate)):
    $earlier  = new DateTime($fdate);
    $later    = new DateTime($tdate);
    $abs_diff = $later->diff($earlier)->format("%a");
    if ($abs_diff > 30):
      $_SESSION['msg'] = 'please select between one month.!';
      header('location: ../group-performance-educator.php');
      exit;
    endif;
  endif;
  
  $limit            =  2;
  $get_filter_type  = '';
  $show_filter_data = '';
  $get_sim_id       = $db->addMultiIds($scenario);
  if ($filter_type == 1):
    #----System-Wise-------
    $get_filter_type = 'Systemwide';
    $sql  = "SELECT GROUP_CONCAT(DISTINCT IF(group_id = '', null, group_id)) AS gid, GROUP_CONCAT(DISTINCT IF(learner_id = '', null, learner_id)) AS lid FROM assignment_tbl WHERE scenario_id IN ($get_sim_id)";
    $exc  = $db->prepare($sql); $exc->execute();
    $row  = $exc->fetch(PDO::FETCH_ASSOC);
    $gid  = ( ! empty($row['gid'])) ? rtrim($row['gid'], ',') : '';
    $lid  = ( ! empty($row['lid'])) ? rtrim($row['lid'], ',') : '';
    foreach ($db->getLearnerByGroup($gid, $userId) as $gdata):
      $getlearnerId[] = $gdata['learner'];
    endforeach;
    if ( ! empty($tdate) && ! empty($fdate)):
      $get_learner_id = $db->addMultiIds($getlearnerId);
      $date_sql = "SELECT GROUP_CONCAT(userid) AS learner FROM scenario_attempt_tbl WHERE scenario_id IN ($get_sim_id) AND userid IN ($get_learner_id) AND DATE(attempt_date) BETWEEN '". $fdate ."' AND '". $tdate ."'";
      $date_exc = $db->prepare($date_sql); $date_exc->execute();
      $date_row = $date_exc->fetch(PDO::FETCH_ASSOC);
      $get_learner = $date_row['learner'];
    elseif ( ! empty($tdate) && empty($fdate)):
      $get_learner_id = $db->addMultiIds($getlearnerId);
      $date_sql = "SELECT GROUP_CONCAT(userid) AS learner FROM scenario_attempt_tbl WHERE scenario_id IN ($get_sim_id) AND userid IN ($get_learner_id) AND DATE(attempt_date) = '". $tdate ."'";
      $date_exc = $db->prepare($date_sql); $date_exc->execute();
      $date_row = $date_exc->fetch(PDO::FETCH_ASSOC);
      $get_learner = $date_row['learner'];
    elseif ( ! empty($fdate) && empty($tdate)):
      $get_learner_id = $db->addMultiIds($getlearnerId);
      $date_sql = "SELECT GROUP_CONCAT(userid) AS learner FROM scenario_attempt_tbl WHERE scenario_id IN ($get_sim_id) AND userid IN ($get_learner_id) AND DATE(attempt_date) = '". $fdate ."'";
      $date_exc = $db->prepare($date_sql); $date_exc->execute();
      $date_row = $date_exc->fetch(PDO::FETCH_ASSOC);
      $get_learner = $date_row['learner'];
    else:
      $get_learner = $db->addMultiIds($getlearnerId);
    endif;
  elseif ($filter_type == 2):
      #----Group-------
      $get_filter_type  = 'Group';
      if ( ! empty($tdate) && ! empty($fdate)):
        $get_learner_id = $db->addMultiIds($group);
        $date_sql = "SELECT GROUP_CONCAT(userid) AS learner FROM scenario_attempt_tbl WHERE scenario_id IN ($get_sim_id) AND userid IN ($get_learner_id) AND DATE(attempt_date) BETWEEN '". $fdate ."' AND '". $tdate ."'";
        $date_exc = $db->prepare($date_sql); $date_exc->execute();
        $date_row = $date_exc->fetch(PDO::FETCH_ASSOC);
        $get_learner = $date_row['learner'];
      elseif ( ! empty($tdate) && empty($fdate)):
        $get_learner_id = $db->addMultiIds($group);
        $date_sql = "SELECT GROUP_CONCAT(userid) AS learner FROM scenario_attempt_tbl WHERE scenario_id IN ($get_sim_id) AND userid IN ($get_learner_id) AND DATE(attempt_date) = '". $tdate ."'";
        $date_exc = $db->prepare($date_sql); $date_exc->execute();
        $date_row = $date_exc->fetch(PDO::FETCH_ASSOC);
        $get_learner = $date_row['learner'];
      elseif ( ! empty($fdate) && empty($tdate)):
        $get_learner_id = $db->addMultiIds($group);
        $date_sql = "SELECT GROUP_CONCAT(userid) AS learner FROM scenario_attempt_tbl WHERE scenario_id IN ($get_sim_id) AND userid IN ($get_learner_id) AND DATE(attempt_date) = '". $fdate ."'";
        $date_exc = $db->prepare($date_sql); $date_exc->execute();
        $date_row = $date_exc->fetch(PDO::FETCH_ASSOC);
        $get_learner = $date_row['learner'];
      else:
        $get_learner = $db->addMultiIds($group);
      endif;
  elseif ($filter_type == 3):
    #----Learner------
    $get_filter_type  = 'Learner';
    $get_learner      = $db->addMultiIds($learner);
  elseif ($filter_type == 4 || $filter_type == 5 || $filter_type == 6):
    #----Unit------
    if ($filter_type == 4):
      $get_filter_type  = 'Unit';
      $get_filter_data  = implode("','", $unit);
      $show_filter_data = $db->addMultiIds($unit);
      $column           = 'department';
    elseif ($filter_type == 5):
      #----Location------
      $get_filter_type  = 'Location';
      $get_filter_data  = implode("','", $location);
      $show_filter_data = $db->addMultiIds($location);
      $column           = 'location';
    elseif ($filter_type == 6):
      #----Designation-----
      $get_filter_type  = 'Designation';
      $get_filter_data  = implode("','", $designation);
      $show_filter_data = $db->addMultiIds($designation);
      $column           = 'designation';
    endif;
    $sql = "SELECT GROUP_CONCAT(DISTINCT IF(group_id = '', null, group_id)) AS gid, GROUP_CONCAT(DISTINCT IF(learner_id = '', null, learner_id)) AS lid FROM assignment_tbl WHERE scenario_id IN ($get_sim_id)";
    $exc = $db->prepare($sql); $exc->execute();
    $row = $exc->fetch(PDO::FETCH_ASSOC);
    $gid = ( ! empty($row['gid'])) ? rtrim($row['gid'], ',') : '';
    $lid = ( ! empty($row['lid'])) ? rtrim($row['lid'], ',') : '';
    foreach ($db->getLearnerByGroup($gid, $userId) as $gdata) :
      $getlearnerId[] = $gdata['learner'];
    endforeach;
    $get_unit_learner   = $db->addMultiIds($getlearnerId);
    $cond               = "$column IN ('$get_filter_data')";
    $get_other_learner  = $db->GetUsersByFilter($get_unit_learner, 'id', $cond);
    if ( ! empty($tdate) && ! empty($fdate)):
      $get_learner_id = $db->addMultiIds($get_other_learner);
      $date_sql = "SELECT GROUP_CONCAT(userid) AS learner FROM scenario_attempt_tbl WHERE scenario_id IN ($get_sim_id) AND userid IN ($get_learner_id) AND DATE(attempt_date) BETWEEN '". $fdate ."' AND '". $tdate ."'";
      $date_exc = $db->prepare($date_sql); $date_exc->execute();
      $date_row = $date_exc->fetch(PDO::FETCH_ASSOC);
      $get_learner = $date_row['learner'];
    elseif ( ! empty($tdate) && empty($fdate)):
      $get_learner_id = $db->addMultiIds($get_other_learner);
      $date_sql = "SELECT GROUP_CONCAT(userid) AS learner FROM scenario_attempt_tbl WHERE scenario_id IN ($get_sim_id) AND userid IN ($get_learner_id) AND DATE(attempt_date) = '". $tdate ."'";
      $date_exc = $db->prepare($date_sql); $date_exc->execute();
      $date_row = $date_exc->fetch(PDO::FETCH_ASSOC);
      $get_learner = $date_row['learner'];
    elseif ( ! empty($fdate) && empty($tdate)):
      $get_learner_id = $db->addMultiIds($get_other_learner);
      $date_sql = "SELECT GROUP_CONCAT(userid) AS learner FROM scenario_attempt_tbl WHERE scenario_id IN ($get_sim_id) AND userid IN ($get_learner_id) AND DATE(attempt_date) = '". $fdate ."'";
      $date_exc = $db->prepare($date_sql); $date_exc->execute();
      $date_row = $date_exc->fetch(PDO::FETCH_ASSOC);
      $get_learner = $date_row['learner'];
    else:
      $get_learner = $db->addMultiIds($get_other_learner);
    endif;
  endif;
  $get_all_learner      = explode(',', $get_learner);
  $get_filter_learner   = array_unique($get_all_learner);
  $get_total_learner    = count($get_filter_learner);
  $get_all_learners     = $db->addMultiIds($get_filter_learner);

  /* Generate Excel */
  $fileName = '';
  if ( ! empty($fdate) && ! empty($tdate)):
    $fileName = 'Group-Performance-Report-from-'. $fdate .'-to-'. $tdate;
  elseif ( ! empty($fdate) && empty($tdate)):
    $fileName = 'Group-Performance-Report-from-'. $fdate;
  elseif ( ! empty($tdate) && empty($fdate)):
    $fileName = 'Group-Performance-Report-'. $fdate;
  elseif (empty($fdate) && empty($tdate)):
    $fileName = 'Group-Performance-Report';
  endif;
  $title = 'Group Performance Report';
    
  $objPHPExcel = new PHPExcel();

  $styleHead   = array('font'  => array('bold'  => true,
                                        'size'  => 16,
                                        'color' => array('rgb' => '2f5497'),
                                      ),
                      'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER)
                  );

  $styleSubHead = array('font'  => array('bold'  => true,
                        'size'  => 12
                      ),
                      'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER)
                    );

  $styleArray  = array('font'  => array('bold'  => true,
                                        'size'  => 12,
                                        'color' => array('rgb' => '2f5497'),
                                      ),
                      'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER)
                  );
  
  $styleRRResult = array('font'  => array('bold'  => true,
                                        'size'  => 12,
                                        'color' => array('rgb' => 'FF0000'),
                                      ),
                        'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER)
                      );
  $stylePResult  = array('font'  => array('bold'  => true,
                                        'size'  => 12,
                                        'color' => array('rgb' => '008000'),
                                      ),
                      'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER)
                      );
                    
  $objPHPExcel->getProperties()->setCreator("EasySIM ®")
                ->setLastModifiedBy("EasySIM ®")
                ->setTitle($fileName)
                ->setSubject($title)
                ->setDescription($title ." document for Office XLSX, generated by EasySIM ®.")
                ->setKeywords($title ." for Office")
                ->setCategory($title);

  function getNextCell($cell, $cnt){
      for ($i = 0; $i < $cnt - 1; $i++):
        $cell++;
      endfor;
      return $cell;
    }
      
  if ($get_total_learner > 0):
      $headers = ['#', 'Full Name', 'User Name', 'Company', 'Unit', 'Location', 'Designation'];
      $attempt = ['Result', 'Score', 'Time-Stamp', 'Time Taken'];
      $column  = "id, CONCAT(fname, ' ', lname) AS fullname, username, company, department, location, designation";
      $noofattempt = 2;
      
      $sheet = $objPHPExcel->getActiveSheet();
      $sheet->setCellValue('A1', $title);
      $sheet->mergeCells('A1:I1');
      $sheet->getStyle('A1')->applyFromArray($styleHead);
      
      foreach ($scenario as $sim):
          for ($a = 0; $a < $noofattempt; $a++):
            $headers = array_merge($headers, $attempt);
          endfor;
      endforeach;

      $i = 4; $cell = 'A';
      foreach ($headers as $k => $v):
          $celln = $cell++ . $i;
          $objPHPExcel->getActiveSheet()->setCellValue($celln, $v);
          $objPHPExcel->getActiveSheet()->getStyle($celln)->applyFromArray($styleSubHead);
      endforeach;
      
      $userData = $db->getUsersColumns($get_all_learners, $column);
      
      $lcid = 5; $lsn = 1; $ldata = [];
      foreach ($userData as $cnt => $udata):
        $learner_id = $udata['id'];
        $tmp = [$cnt + 1,
                $udata['fullname'],
                $udata['username'],
                $udata['company'],
                $udata['department'],
                $udata['location'],
                $udata['designation']];
        foreach ($scenario as $sid):
          $report = $db->learnerReport($sid, $learner_id, $limit, $fdate, $tdate);
          $rtemp  = [];
          for ($rt = 0; $rt < $limit; $rt++):
            if (isset($report[$rt])):
              $rtemp = array_merge($rtemp, array_values($report[$rt]));
            else:
              $rtemp = array_merge($rtemp, array_fill(0, count($attempt), ''));
            endif;
          endfor;
          $tmp = array_merge(array_values($tmp), $rtemp);
        endforeach;
        $ldata[] = $tmp;
        $lsn++; $lcid++;
      endforeach;
      
      foreach ($ldata as $k1 => $v1):
        $i = 5 + $k1; $cell = 'A';
        foreach ($v1 as $k2 => $v2):
            $celln = $cell++ . $i;
            $objPHPExcel->getActiveSheet()->setCellValue($celln, $v2);
            if ($v2 == 'RR'):
              $objPHPExcel->getActiveSheet()->getStyle($celln)->applyFromArray($styleRRResult);
            elseif ($v2 == 'P'):
              $objPHPExcel->getActiveSheet()->getStyle($celln)->applyFromArray($stylePResult);
            endif;
        endforeach;
      endforeach;
      
      $l = 'H';
      foreach ($scenario as $sid):
          $sim = $db->getSim('Scenario_title', $sid)[0];
          $i   = 2;
          $s   = getNextCell($l, count($attempt) * $noofattempt);
          $objPHPExcel->getActiveSheet()->setCellValue($l.$i, $sim['Scenario_title']);
          $sheet->mergeCells($l.$i.":".$s.$i);
          $objPHPExcel->getActiveSheet()->getStyle($l.$i)->applyFromArray($styleArray);
          
          $i = 3; $l1 = $l;
          for ($a = 0; $a < $noofattempt; $a++):
              $l1 = getNextCell($l1, (count($attempt) * $a) + 1);
              $s1 = getNextCell($l1, count($attempt));
              $objPHPExcel->getActiveSheet()->setCellValue($l1.$i, 'A'. ($a + 1));
              $sheet->mergeCells($l1.$i.":".$s1.$i);
              $objPHPExcel->getActiveSheet()->getStyle($l1.$i)->applyFromArray($styleArray);
          endfor;
          for ($x = 0; $x < count($attempt) * $noofattempt; $x++):
              $l++; $s++;
          endfor;
      endforeach;

      $objPHPExcel->getActiveSheet()->setTitle($title);
      $objPHPExcel->getActiveSheet()->getTabColor()->setARGB(PHPExcel_Style_Color::COLOR_RED);
      $objPHPExcel->createSheet();
      $fName = $fileName.'.xlsx';
      // Redirect output to a client’s web browser (Excel2007)
      header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
      header('Content-Disposition: attachment;filename="'. $fName .'"');
      header('Cache-Control: max-age=0');
      // If you're serving to IE 9, then the following may be needed
      header('Cache-Control: max-age=1');	
      // If you're serving to IE over SSL, then the following may be needed
      header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
      header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
      header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
      header ('Pragma: public'); // HTTP/1.0	
      $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
      $objWriter->save('php://output');
  endif;
endif;
