<?php 

function SanitizeForSQL($str)
{
    if( function_exists( "mysql_real_escape_string" ) )
    {
        $ret_str = mysql_real_escape_string( $str );
    }
    else
    {
        $ret_str = addslashes( $str );
    }
        return $ret_str;
}

/**
 * Encrypt and decrypt
 * 
 * @author Nazmul Ahsan <n.mukto@gmail.com>
 * @link http://nazmulahsan.me/simple-two-way-function-encrypt-decrypt-string/
 *
 * @param string $string string to be encrypted/decrypted
 * @param string $action what to do with this? e for encrypt, d for decrypt
 */
function encryptString( $string, $action = 'e' ) {
    // you may change these values to your own
    $secret_key = 'PASS';
    $secret_iv = 'KSES';

    $output = false;
    $encrypt_method = "AES-256-CBC";
    $key = hash( 'sha256', $secret_key );
    $iv = substr( hash( 'sha256', $secret_iv ), 0, 16 );

    if( $action == 'e' ) {
        $output = base64_encode( openssl_encrypt( $string, $encrypt_method, $key, 0, $iv ) );
    }
    else if( $action == 'd' ){
        $output = openssl_decrypt( base64_decode( $string ), $encrypt_method, $key, 0, $iv );
    }
    return $output;
}
