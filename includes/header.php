<!doctype html>
<!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]><html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--><html class="no-js" lang="en"> <!--<![endif]-->
<head>
	<title>EasySim</title>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" type="image/x-icon" href="favicon.ico" />
    <!-- Chrome, Firefox OS and Opera -->
    <meta name="theme-color" content="#2c3545">
    <!-- Windows Phone -->
    <meta name="msapplication-navbutton-color" content="#2c3545">
    <!-- iOS Safari -->
    <meta name="apple-mobile-web-app-status-bar-style" content="#2c3545">
    <link type="text/css" rel="stylesheet" href="content/css/bootstrap.min.css" />
    <link type="text/css" rel="stylesheet" href="content/css/bootstrap-responsive.css" />
    <link type="text/css" rel="stylesheet" href="content/css/bootstrap-editable.css" />
    <link type="text/css" rel="stylesheet" href="content/css/bootstrap-toggle.css" />
    <link type="text/css" rel="stylesheet" href="content/css/font-awesome.min.css" />
	<link type="text/css" rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link type="text/css" rel="stylesheet" href="content/css/style.css?v=<?php echo time() ?>" />
    <link type="text/css" rel="stylesheet" href="content/css/responsive.css?v=<?php echo time() ?>" />
    <link type="text/css" rel="stylesheet" href="content/css/component.css?v=<?php echo time() ?>" />
    <link type="text/css" rel="stylesheet" href="content/css/jquery.modalLink-1.0.0.css" />
    <script type="text/javascript" src="content/js/jquery-3.3.1.min.js"></script>
    <script type="text/javascript" src="content/js/bootstrap.3.3.7.min.js"></script>
    <script type="text/javascript" src="content/js/sweetalert.min.js"></script>
    <script type="text/javascript" src="content/js/loadingoverlay.min.js"></script>
    <link rel="stylesheet" href="content/css/owlcarousel/owl.carousel.min.css" />
    <link rel="stylesheet" href="content/css/owlcarousel/owl.theme.default.css" />
    <link rel="stylesheet" href="content/css/animate.css" />
    <link rel="stylesheet" href="content/css/qustemplate.css?v=<?php echo time() ?>" />
    <link rel="stylesheet" href="content/css/questionresponsive.css?v=<?php echo time() ?>" />
    <link rel="stylesheet" href="content/css/draganddrop.css?v=<?php echo time() ?>" />
    <link rel="stylesheet" href="content/css/template-updated.css?v=<?php echo time() ?>" />
    <link rel="stylesheet" href="videojs/node_modules/video.js/dist/video-js.min.css" />
    <link rel="stylesheet" href="videojs/node_modules/video.js/dist/themes/forest/index.css" />
    <script type="text/javascript" src="videojs/node_modules/video.js/dist/video.min.js"></script>
    <link rel="stylesheet" href="content/fancybox/jquery.fancybox.min.css" />
    <script type="text/javascript" src="content/fancybox/jquery.fancybox.min.js"></script>
    <script type="text/javascript" src="content/js/jquery.simple.timer.js"></script>
    <script type="text/javascript" src="content/js/draganddrop.js"></script>
    <?php if(isset($_SESSION['autologin'])){ ?>
        <link rel="stylesheet" href="content/css/iframe.css" />
    <?php } ?>

</head>
<body class="">