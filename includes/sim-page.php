<?php 
$simHtml	= $scorm_head;
$simHtml   .= '
<style type="text/css">
	.text-hint-banner.webframeobj{
		text-align: center;
		width: 100%;
	}
	.nav-tabs {
		border-bottom: none;
	}
	.owl-item {
		text-align: center;
	}
	.owl-item li {
		list-style: none;
	}
	ul, ol {
		padding: revert;
		margin: revert;
		list-style: unset !important;
	}
	.owl-stage {
		margin: 0 auto;
	}
	.owl-theme .owl-nav{
		margin-top:0;
	}
	.owl-carousel .owl-item {
		padding: 10px;
	}
	button.owl-prev {
		position: absolute;
		left: -80px;
		top: 37%;
		transform: translateY(-50%);
		background-color: #049805!important;
		width: 20px;
		height: 20px;
		border-radius: 100%!important;
		color: #fff!important;
	}				
	button.owl-next {
		position: absolute;
		right: -80px;
		top: 37%;
		transform: translateY(-50%);
		background-color: #049805!important;
		width: 20px;
		height: 20px;
		border-radius: 100%!important;
		color: #fff!important;
	}			
	.admin-dashboard.Ev2 .nav {
		padding-top: 5px;
		padding: 0 95px;
	}
	.owl-dots {
		display: none;
	}			
	/*------------------phone-------------*/
	@media (min-width:320px)   and (max-width: 767px) {
		button.owl-prev {
			left: -20px !important;
		}
		.admin-dashboard.Ev2 .nav {
			padding: 0 25px !important;
		}
	
		button.owl-next {
			right: -20px !important;
		}
	}
</style>';
$simHtml   .= '
<div class="admin-dashboard Ev2 launchSce launchScev2">
	<div class="container-fuild linearassestBG"><h2 class="launchhead">'. $simData['Scenario_title'] .'</h2>
	<div class="text-hint-banner">';
	if (empty($simData['video_url']) && ! empty($simData['audio_url'])):
		$simHtml .= '<audio src="'. $path . $simData['audio_url'] .'" controls="controls" class="img-responsive img-fluid"></audio>';
	elseif ( ! empty($simData['video_url'])):
		$simHtml .= '<video width="100%" height="auto" controls muted><source src="'. $path . $simData['video_url'] .'" class="img-responsive img-fluid"></video>';
	elseif ( ! empty($simData['image_url'])):
		$simHtml .= '<img src="'. $path . $simData['image_url'] .'" class="img-responsive img-fluid"/>';
	elseif ( ! empty($simData['web_object_file'])):
		$wofn = '';
		if (file_exists($path . $simData['web_object_file'] . '/story.html')):
			$wofn = 'story.html';
		elseif (file_exists($path . $simData['web_object_file'] . '/index.html')):
			$wofn = 'index.html';
		elseif (file_exists($path . $simData['web_object_file'] . '/a001index.html')):
			$wofn = 'a001index.html';
		endif;
		$simHtml .= '<iframe src="'. $path . $simData['web_object_file'] . '/' . $wofn .'" width="1000px" height="600px" frameborder="0"></iframe>';
		$simHtml .=	'<script type="text/javascript">$(".text-hint-banner").addClass("webframeobj");</script>';
	endif;
$simHtml .= '</div>
	<ul class="nav nav-tabs">
		<div class="owl_1 owl-carousel">';
		if ($simPage): $l = 0;
			foreach ($simPage as $plink):
$simHtml .=	'<div class="item">';
				if ($l == 0):
					$at = 'class="active"';
				else:
					$at = '';
				endif;
		$simHtml .=	'<li '. $at .'><a data-toggle="tab" href="#'. $plink['sim_page_id'] .'">'. $plink['sim_page_name'] .'</a></li>';
$simHtml .=	'</div>';
		$l++; endforeach; endif;
$simHtml .= '</div></ul></div>';
$simHtml .= '<div class="tab-content">';
if ($simPage): $p = 0;
	foreach ($simPage as $pdata):
		if ($p == 0):
			$pa = 'in active';
		else:
			$pa = '';
		endif;
$simHtml .= '<div id="'. $pdata['sim_page_id'] .'" class="tab-pane fade '. $pa .'"><div class="Lrn-Banner linearlunch1">'. $pdata['sim_page_desc'] .'</div></div>';
$p++; endforeach; endif;
$simHtml .= '</div></div>';
$simHtml .= '<div class="footergray Launchbtn linnerlaunch"><a href="launch.html"><button class="btn btn-primary">LAUNCH SIMULATION</button></a></div>';
$simHtml .= '<script src="'. $db->getBaseUrl('content/js/owlcarousel/owl.carousel.min.js') .'"></script>';
$simHtml .= '
<script type="text/javascript">
	$(".owl_1").owlCarousel({
		loop:false,
		mouseDrag: false,
		margin:20,
		responsiveClass:true,
		autoplayHoverPause:true,
		autoplay:false,
		slideSpeed: 400,
		paginationSpeed: 400,
		autoplayTimeout:3000,
		responsive:{
			0:{
				items:2,
				nav:true,
				loop:false
			},
			600:{
				items:3,
				nav:true,
				loop:false
			},
			1000:{
				items:6,
				nav:true,
				loop:false
			}
		}
	});
	$(document) .ready(function(){
		var li =  $(".owl-item li");
		$(".owl-item li").click(function(){
			li.removeClass("active");
		});
	});
</script>';
