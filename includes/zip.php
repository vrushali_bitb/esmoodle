<?php 
#-------------------------------ZIP------------------------------
if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['web_obj_publish'])):
	$HtmlCode	= ob_get_contents();
	$HtmlCode	.= '<style type="text/css">.scorm_publishs { display:none !important; }</style>';
	$scorm		= 'zip';
	$sim_name	= $_POST['sim_name'];
	$scorm_path = $scorm .'/'. $sim_name. '-Web-Object-'. date("M-j-Y-g-i-s-a");
	
	if ( ! is_dir($scorm)) mkdir($scorm);
	if ( ! is_dir($scorm_path)) mkdir($scorm_path);

	$sfh = fopen($scorm_path . '/index.html', 'w');
	fwrite($sfh, $simHtml);
	fclose($sfh);
	
	$fh = fopen($scorm_path . '/launch.html', 'w');
	fwrite($fh, $HtmlCode);
	fclose($fh);

	function delete_directory($dirname) {
		if (is_dir($dirname))
			$dir_handle = opendir($dirname);
			if ( ! $dir_handle) return FALSE;
				while ($file = readdir($dir_handle)):
					if ($file != "." && $file != ".."):
						if ( ! is_dir($dirname."/".$file))
							unlink($dirname."/".$file);
						else
							delete_directory($dirname.'/'.$file);
						endif;
					endwhile;
			closedir($dir_handle);
			rmdir($dirname);
			return TRUE;
		}
	
	require_once 'includes/zip-backup.php';
	$zipname = $scorm_path .'.zip';
	zipData($scorm_path, $zipname);
	delete_directory($scorm_path);
endif;
