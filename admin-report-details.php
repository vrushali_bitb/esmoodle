<?php
session_start();
if (isset($_SESSION['username'])) {
    $user   = $_SESSION['username'];
    $role   = $_SESSION['role'];
    $userid = $_SESSION['userId'];
} else {
    header('location: index.php');
}
require_once 'config/db.class.php';
require_once 'includes/learners_menu.php';
require_once 'config/pagination.php';
$db         = new DBConnection();
$type_path  = 'img/type_icon/';
$base_url   = $db->getBaseUrl($db->getFile() . '?');
$type       = (isset($_GET['type'])) ? $_GET['type'] : '';
$order      = (isset($_GET['orderby'])) ? $_GET['orderby'] : '';
?>
<div class="bottomheader">
    <div class="container-fluid">
        <ul class="breadcrumb">
            <?php echo $db->breadcrumbs(); ?>
        </ul>
    </div>
</div>
<div class="Group_managment">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="usermanage_main">
                    <div class="buttonRight">
                        <button type="button" class="btn btn-primary back_btn" name="cancel" onclick="window.location = 'admin-report.php'">Back</button>
                    </div>
                    <div class="table-box">
                        <div class="tableheader clearfix">
                            <div class="tableheader_left">Assigned Simulation</div>
                            <div class="tableheader_right">
                                <div class="Searchbox">
                                    <form method="post">
                                        <input type="serch" class="serchtext" name="searchkey" required>
                                        <div class="input-group-append serchBTN">
                                            <button class="btn btn-Transprate" type="submit"><i class="fa fa-search" aria-hidden="true"></i></button>
                                        </div>
                                    </form>
                                </div>
                                <div class="btn-group btn-shortBY">
                                    <button type="button" class="btn btn-Transprate"><span class="sort">Sort by</span></button>
                                    <button type="button" class="btn btn-Transprate dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img src="img/down_arrow_white.png"></button>
                                    <div class="dropdown-menu shortBYMenu">
                                        <a class="dropdown-item" href="<?php echo $base_url . $db->build_url(array('view' => 'true', 'type' => $type)); ?>">Default Sorting</a>
                                        <a class="dropdown-item" href="<?php echo $base_url . $db->build_url(array('view' => 'true', 'type' => $type, 'orderby' => 'type')); ?>">Scenario Type</a>
                                        <a class="dropdown-item" href="<?php echo $base_url . $db->build_url(array('view' => 'true', 'type' => $type, 'orderby' => 'status')); ?>">Scenario Status</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="usermanage-form1">
                    <div class="table-responsive">
						<?php 
							#----------Serch----------------
							$searchText = '';
							if (isset($_GET['searchkey']) && ! empty($_GET['searchkey'])):
								extract($_GET);
								$searchText = " AND ( c.category LIKE '%$searchkey%' OR s.Scenario_title LIKE '%$searchkey%' )";
							endif;
							#----------Sort by----------------
							if (isset($_GET['orderby']) && $_GET['orderby'] == 'type'):
								$orderby = (( ! empty($searchText)) ? $searchText : " ")." ORDER BY s.author_scenario_type";
							elseif (isset($_GET['orderby']) && $_GET['orderby'] == 'status'):
								$orderby = (( ! empty($searchText)) ? $searchText : " ")." ORDER BY s.status";
							else:
								$orderby = (( ! empty($searchText)) ? $searchText : " ")." ORDER BY s.scenario_id DESC";
							endif;
							
							$type = (isset($_GET['type'])) ? $_GET['type'] : '';
							
							if ($type == 1):
								#-------ASSIGNED-LEARNERS-------
								$sql = "SELECT DISTINCT s.Scenario_title, GROUP_CONCAT(DISTINCT a.group_id SEPARATOR ',') as groupid , s.scenario_id, s.assign_start_date, s.assign_end_date, s.assign_reviewer, s.scenario_type, s.scenario_category, s.duration, s.status , s.created_on, s.sim_ques_type, s.assign_group,  s.update_sim, c.category, st.status_name, t.type_name, t.type_icon FROM 
										scenario_master s LEFT JOIN 
										assignment_tbl a ON s.scenario_id = a.scenario_id INNER JOIN 
										group_tbl g LEFT JOIN 
										category_tbl c ON s.scenario_category = c.cat_id LEFT JOIN
										scenario_status st ON st.sid = s.status LEFT JOIN 
										scenario_type t ON t.st_id = s.author_scenario_type WHERE 
										s.assign_author != '' AND s.author_scenario_type != '0' AND a.group_id IS NOT NULL GROUP BY a.scenario_id $orderby";
							elseif ($type == 2):
								#-------TOTAL-SIMULATION-AUTHORED-------
								$sql = "SELECT DISTINCT s.scenario_id, s.Scenario_title, s.assign_start_date, s.assign_end_date, s.assign_reviewer, s.scenario_type, s.scenario_category, s.duration, s.status , s.created_on, s.sim_ques_type, s.assign_group,  s.update_sim, c.category, st.status_name, t.type_name, t.type_icon FROM 
										scenario_master s INNER JOIN 
										group_tbl g LEFT JOIN 
										category_tbl c ON c.cat_id = s.scenario_category LEFT JOIN 
										scenario_status st ON st.sid = s.status LEFT JOIN 
										scenario_type t ON t.st_id = s.author_scenario_type $orderby";
							elseif ($type == 3):
								#-------PENDING-SIMULATION-------
								$sql = "SELECT DISTINCT s.scenario_id, s.Scenario_title, s.assign_start_date, s.assign_end_date, s.assign_reviewer, s.scenario_type, s.scenario_category, s.duration, s.status , s.created_on, s.sim_ques_type, s.assign_group,  s.update_sim, c.category, st.status_name, t.type_name, t.type_icon FROM 
										scenario_master s INNER JOIN 
										group_tbl g LEFT JOIN 
										category_tbl c ON c.cat_id = s.scenario_category LEFT JOIN 
										scenario_status st ON st.sid = s.status LEFT JOIN 
										scenario_type t ON t.st_id = s.author_scenario_type WHERE 
										s.scenario_id NOT IN (SELECT scenario_id FROM question_tbl) $orderby";
							elseif ($type == 4):
								#-------NEW-SIMULATION-------
								$sql = "SELECT DISTINCT s.scenario_id, s.Scenario_title, s.assign_start_date, s.assign_end_date, s.assign_reviewer, s.scenario_type, s.scenario_category, s.duration, s.status , s.created_on, s.sim_ques_type, s.assign_group,  s.update_sim, c.category, st.status_name, t.type_name, t.type_icon FROM 
										scenario_master s INNER JOIN 
										group_tbl g LEFT JOIN 
										category_tbl c ON c.cat_id = s.scenario_category LEFT JOIN 
										scenario_status st ON st.sid = s.status LEFT JOIN 
										scenario_type t ON t.st_id = s.author_scenario_type WHERE 
										s.author_scenario_type = '0' AND s.sim_ques_type = '0' $orderby";
							elseif ($type == 5):
								#-------REVIEWED-SIMULATION-------
								$sql = "SELECT DISTINCT s.scenario_id, s.Scenario_title, s.assign_start_date, s.assign_end_date, s.assign_reviewer, s.scenario_type, s.scenario_category, s.duration, s.status , s.created_on, s.sim_ques_type, s.assign_group,  s.update_sim, c.category, st.status_name, t.type_name, t.type_icon FROM 
										scenario_master s INNER JOIN 
										group_tbl g LEFT JOIN 
										category_tbl c ON c.cat_id = s.scenario_category LEFT JOIN 
										scenario_status st ON st.sid = s.status LEFT JOIN 
										scenario_type t ON t.st_id = s.author_scenario_type WHERE 
										s.status = '14' $orderby";
							elseif ($type == 6):
								#-------REVIEW-IN-PROCESS-------
								$sql = "SELECT DISTINCT s.scenario_id, s.Scenario_title, s.assign_start_date, s.assign_end_date, s.assign_reviewer, s.scenario_type, s.scenario_category, s.duration, s.status , s.created_on, s.sim_ques_type, s.assign_group,  s.update_sim, c.category, st.status_name, t.type_name, t.type_icon FROM 
										scenario_master s INNER JOIN 
										group_tbl g LEFT JOIN 
										category_tbl c ON c.cat_id = s.scenario_category LEFT JOIN 
										scenario_status st ON st.sid = s.status LEFT JOIN 
										scenario_type t ON t.st_id = s.author_scenario_type WHERE 
										s.status = '14' $orderby";
							elseif ($type == 7):
								#-------REVIEW-FIXED-------
								$sql = "SELECT DISTINCT s.scenario_id, s.Scenario_title, s.assign_start_date, s.assign_end_date, s.assign_reviewer, s.scenario_type, s.scenario_category, s.duration, s.status , s.created_on, s.sim_ques_type, s.assign_group,  s.update_sim, c.category, st.status_name, t.type_name, t.type_icon FROM 
										scenario_master s INNER JOIN 
										group_tbl g LEFT JOIN 
										category_tbl c ON c.cat_id = s.scenario_category LEFT JOIN 
										scenario_status st ON st.sid = s.status LEFT JOIN 
										scenario_type t ON t.st_id = s.author_scenario_type WHERE 
										s.status = '15' $orderby";
							elseif ($type == 8):
								#-------READY-FOR-PUBLISHED-------
								$sql = "SELECT DISTINCT s.scenario_id, s.Scenario_title, s.assign_start_date, s.assign_end_date, s.assign_reviewer, s.scenario_type, s.scenario_category, s.duration, s.status , s.created_on, s.sim_ques_type, s.assign_group,  s.update_sim, c.category, st.status_name, t.type_name, t.type_icon FROM 
										scenario_master s INNER JOIN 
										group_tbl g LEFT JOIN 
										category_tbl c ON c.cat_id = s.scenario_category LEFT JOIN 
										scenario_status st ON st.sid = s.status LEFT JOIN 
										scenario_type t ON t.st_id = s.author_scenario_type WHERE 
										s.status = '5' $orderby";
							elseif ($type == 9):
								#-------PUBLISH-------
								$sql = "SELECT DISTINCT s.scenario_id, s.Scenario_title, s.assign_start_date, s.assign_end_date, s.assign_reviewer, s.scenario_type, s.scenario_category, s.duration, s.status , s.created_on, s.sim_ques_type, s.assign_group,  s.update_sim, c.category, st.status_name, t.type_name, t.type_icon FROM 
										scenario_master s INNER JOIN 
										group_tbl g LEFT JOIN 
										category_tbl c ON c.cat_id = s.scenario_category LEFT JOIN 
										scenario_status st ON st.sid = s.status LEFT JOIN 
										scenario_type t ON t.st_id = s.author_scenario_type WHERE 
										s.status = '8' $orderby";
							endif;
							$q 				= $db->prepare($sql); $q->execute();
                            $rowCount		= $q->rowCount();
                            $count			= ($sql) ? $rowCount : 0;
                            $page   		= (isset($_REQUEST['page']) && ! empty($_REQUEST['page'])) ? $_REQUEST['page'] : 1;
                            $show  			= 100;
                            $start 			= ($page - 1) * $show;
                            $npagination	= getPaginationString($count, $show, $page, 1, basename(__FILE__). '?view=true&type='. $type, '&page=');
                            $sql			= $sql ." LIMIT $start, $show";
							$results 		= $db->prepare($sql); ?>
                            <table class="table table-striped">
                                <thead class="Theader">
                                    <tr>
                                        <th>Category Name</th>
                                        <th>Simulation Title</th>
                                        <th>Creation Date</th>
                                        <th>Start Date</th>
                                        <th>End Date</th>
                                        <th>Reviewer Name</th>
                                        <?php if ($type == 1): ?>
                                        <th>Learners Name</th>
										<?php endif; ?>
                                        <th>Total Questions</th>
                                        <th>Duration in min</th>
                                        <th>Simulation Type</th>
                                        <th>Status</th>                                        
                                    </tr>
                                </thead>
                                <tbody>
                                <?php if ($results->execute() && $rowCount > 0):
                                    foreach ($results->fetchAll(PDO::FETCH_ASSOC) as $nrow): ?>
                                    <tr>
                                        <td><?php echo $nrow['category']; ?></td>
                                        <td title="<?php echo $nrow['Scenario_title']; ?>"><?php echo $db->truncateText($nrow['Scenario_title'], 40); ?></td>
                                        <td><?php echo $db->dateFormat($nrow['created_on'], 'd-M-y'); ?></td>
                                        <td><?php echo ($nrow['assign_start_date'] != '0000-00-00') ? $db->dateFormat($nrow['assign_start_date'], 'd-M-y') : '--' ?></td>
                                        <td><?php echo ($nrow['assign_end_date'] != '0000-00-00') ? $db->dateFormat($nrow['assign_end_date'], 'd-M-y') : '--' ?></td>
                                        <td><?php if ( ! empty($nrow['assign_reviewer'])): ?>
											<a data-toggle="popover" title="View Reviewer" href="<?php echo $db->getBaseUrl('includes/ajax.php?getTooltipData=true&ids='.$nrow['assign_reviewer']); ?>" onclick="return false"><i class="fa fa-info-circle fa-2x" aria-hidden="true"></i></a>
											<?php endif; ?>
										</td>
                                        <?php 
										if ($type == 1):
											$l = 0;
											$data[] 	 = $db->getGroupColumns($nrow['groupid'], 'learner')[0]['col'];
											$getdata[] 	 = implode(',', array_unique(explode(',', $db->addMultiIds($data))));
											$get_learner = $getdata[$l];
											$l++;
										  endif; ?>
                                         <?php if ( ! empty($get_learner)): ?>
                                        <td><a data-toggle="popover" title="View Learners" href="<?php echo $db->getBaseUrl('includes/ajax.php?getTooltipData=true&get_learner=true&learnerId='.$get_learner); ?>" onclick="return false"><i class="fa fa-info-circle fa-2x" aria-hidden="true"></i></a></a></td>
                                        <?php endif; ?>
                                        <td><?php echo $db->getTotalQuestionsByScenario(md5($nrow['scenario_id'])); ?></td>
                                        <td><?php echo ( ! empty($nrow['duration'])) ? $nrow['duration'] : 0; ?></td>
                                        <td><img src="<?php echo $type_path . $nrow['type_icon'] ?>" class="type_icon" title="<?php echo $nrow['type_name'] ?>" /></td>
                                        <td><strong><?php echo $nrow['status_name']; ?></strong></td>
                                    </tr>
                                    <?php endforeach; else: ?>
                                    <tr><td colspan="11" style="text-align:center"><strong>No data available in database.</strong></td></tr>
                                    <?php endif; ?>
                                </tbody>
                            </table>
                        </div>
                        <div class="pull-right">
                            <nav aria-label="navigation"><?php echo ( ! empty($npagination)) ? $npagination : '&nbsp;'; ?></nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<style>
    a.disabled {
        color: currentColor;
        cursor: not-allowed;
        opacity: 0.2;
        text-decoration: none;
    }

    .table>tbody>tr>td,
    .table>tbody>tr>th,
    .table>tfoot>tr>td,
    .table>tfoot>tr>th,
    .table>thead>tr>td,
    .table>thead>tr>th {
        padding: 10px;
    }
</style>
<script src="content/js/tooltip.js"></script>
<?php
require_once 'includes/footer.php';
