<?php 
session_start();
if (isset($_SESSION['username'])) {
    $user   	= $_SESSION['username'];
    $role   	= $_SESSION['role'];
    $userid 	= $_SESSION['userId'];
	$client_id	= $_SESSION['client_id'];
	$domain		= (isset($_SESSION['domain_name'])) ? $_SESSION['domain_name'] : $_SERVER['HTTP_HOST'];
}
else {
    header('location: index.php');
}
require_once 'config/db.class.php';
require_once 'includes/learners_menu.php';
$db 		= new DBConnection();
$sim_id 	= (isset($_GET['createsim']) && ! empty($_GET['sim_id'])) ? $_GET['sim_id'] : '';
$data   	= $db->getScenario($sim_id);
$sim_type	= $db->getMultiScenarioType($data['scenario_type']);
$permission = $db->getClientPermission($client_id);
$icon_path	= 'img/type_icon/';
$uploadpath	= 'scenario/upload/'.$domain.'/'; ?>
<link rel="stylesheet" href="content/css/qustemplate.css" />
<link rel="stylesheet" href="content/css/questionresponsive.css" />
<link rel="stylesheet" href="content/css/template-updated.css" />
<div class="bottomheader">
    <div class="container-fluid">
        <ul class="breadcrumb">
			<?php echo $db->breadcrumbs(); ?>
         </ul>
     </div>
</div>
<div class="edit-sim-banner">
    <div class="container-fluid">
    	<form class="form-inline" method="post" name="template_form" id="template_form">
        <input type="hidden" name="create_sim_template" value="1" /> 
        <input type="hidden" name="sim_id" value="<?php echo $data['scenario_id'] ?>" />
    	<div class="row">
			<div class="edit-sim-box">
				<div class="user_text editSim">Simulation Name</div>	
				<div class="form-group">
					<input type="text" name="sim_title" placeholder="Simulation Name" value="<?php echo $data['Scenario_title'] ?>" class="form-control" required="required" />
					<img class="editsimimg" src="img/list/edit.svg">
				</div>
			</div>
			<div class="edit-sim-box">
				<div class="user_text editSim">Add Cover Image</div>
			    <div class="addsce-img">
					<div class="imgpath">
						<div class="form-group">
							<input type="file" name="file" id="file" class="form-control file">
							<input type="text" class="form-control controls" disabled placeholder="Select File" />
							<input type="hidden" name="scenario_cover_file" id="scenario_cover_file" value="" />
                            <input type="hidden" name="scenario_media_file_type" id="scenario_media_file_type" value="image" />
						</div>
					</div>
					<div class="browsebtn">
						<span id="splashImg"></span> &nbsp;&nbsp;
                        <span id="ImgDelete" style="display:none">
                        	<a href="javascript:void(0);" data-scenario-media-file="" class="delete_scenario_media_file" title="Delete File"><i class="fa fa-times" aria-hidden="true"></i></a>
                        </span>
					</div>
					<div class="browsebtn"><button class="browse btn btn-primary" type="button">Browse & Select</button></div>
                    <div class="browsebtn"><button type="button" name="upload" id="upload" class="btn btn-primary">Uplaod</button></div>
				</div>
			</div>	
			<div class="edit-sim-box">
				<div class="user_text editSim">Scenario Type</div>	
				<div class="edit-sim-radio">
				<?php foreach ($sim_type as $type): ?>
				<div class="Radio-box MCQRadio">
					<label class="radiostyle">
						<input type="radio" class="sim_type" name="sim_type" value="<?php echo $type['st_id'] ?>" />
						<span class="radiomark"></span>
						<span class="text-R"><?php echo $type['type_name'] ?>
						<img src="<?php echo $icon_path . $type['type_icon'] ?>" class="type_icon" title="<?php echo $type['type_name'] ?>" /></span>
					</label>
				</div>
				<?php endforeach; ?>
				</div>
			</div>
			<?php $sim_temp = ( ! empty($permission['auth_templates'])) ? explode(',', $permission['auth_templates']) : []; ?>
			<div class="edit-sim-box">
				<div class="user_text editSim">Classic Simulation</div>
				<?php if (in_array(1, $sim_temp) || in_array(2, $sim_temp)): ?>
				<div class="linersim">
					<div class="Radio-box Selectsim">
						<label class="radiostyle">
                            <input type="radio" name="sim_ques_type" class="sim_ques_type" value="1" />
                            <span class="radiomark white respwhite"></span>
                            <span class="text-R">Linear Simulation</span>
                        </label>
					</div>
					<div class="edit-sim-radio Linerm">
						<?php if (in_array(1, $sim_temp)): ?>
						<div class="Radio-box">
							<label class="radiostyle">
								<input type="radio" name="sim_tmp" value="1" class="linear_tmp tmp_type" />
								<span class="radiomark"></span>
								<span class="text-R">Classic Template</span>
							</label>
							<span class="smallT">
								<p class="question-type">(MCQ, Drag and Drop, Swiping)</p>
								<p class="question-type">(Multiple Feedback)</p>
							</span>
						</div>
						<?php endif; 
						if (in_array(2, $sim_temp)): ?>
						<div class="Radio-box">
							<label class="radiostyle">
								<input type="radio" name="sim_tmp" value="2" class="linear_tmp tmp_type" />
								<span class="radiomark"></span>
								<span class="text-R">Multiple Template</span>
							</label>
							<span class="smallT">
								<p class="question-type">(MTF, Sequence, Sorting, MMCQ, MCQ, Drag and Drop, Swiping)</p>
								<p class="question-type">(Single Feedback)</p>
							</span>
						</div>
						<?php endif; ?>
					</div>
				</div>
				<?php endif; 
				if (in_array(4, $sim_temp) || in_array(5, $sim_temp)): ?>
				<div class="linersim linersimBotom">
					<div class="Radio-box Selectsim">
						<label class="radiostyle">
							<input type="radio" name="sim_ques_type" class="sim_ques_type" value="2" />
							<span class="radiomark white respwhite"></span>
							<span class="text-R">Branching Simulation</span>
						</label>
					</div>
					<div class="edit-sim-radio Linerm">
					<?php if (in_array(4, $sim_temp)): ?>
						<div class="Radio-box">
							<label class="radiostyle">
								<input type="radio" name="sim_tmp" value="4" class="branch_tmp tmp_type" />
								<span class="radiomark"></span>
								<span class="text-R">Classic Template</span>
							</label>
							<span class="smallT">
								<p class="question-type">(MCQ, Drag and Drop,Swiping)</p>
								<p class="question-type">(Multiple Feedback)</p>
							</span>
						</div>
						<?php endif; 
						if (in_array(5, $sim_temp)): ?>
						<div class="Radio-box">
							<label class="radiostyle">
								<input type="radio" name="sim_tmp" value="5" class="branch_tmp tmp_type" />
								<span class="radiomark"></span>
								<span class="text-R">Multiple Template</span>
							</label>
							<span class="smallT">
								<p class="question-type">(MTF, Sorting, MMCQ, MCQ, Drag and Drop, Swiping)</p>
								<p class="question-type">(Single Feedback)</p>
							</span>
						</div>
						<?php endif; ?>
					</div>
				</div>
				<?php endif; 
				if (in_array(7, $sim_temp)): ?>
				<div class="user_text editSim">Open Response Simulation</div>
                <div class="linersim linersimlast">
                    <div class="Radio-box Selectsim">
                        <label class="radiostyle">
                            <input type="radio" name="sim_ques_type" class="sim_ques_type" value="3" />
                            <input type="hidden" name="open_sim_tmp" value="7" />
                            <span class="radiomark white respwhite"></span>
                            <span class="text-R">Open Response</span>
                        </label>
                    </div>
            	</div>
				<?php endif; 
				if (in_array(8, $sim_temp)): ?>
				<div class="user_text editSim">Video Simulation</div>
				<div class="linersim">
                    <div class="Radio-box Selectsim">
                        <label class="radiostyle">
                            <input type="radio" name="sim_ques_type" class="sim_ques_type" value="4" />
                            <input type="hidden" name="single_vsim_tmp" value="8" />
                            <span class="radiomark white respwhite"></span>
                            <span class="text-R">Single Video Simulation</span>
                        </label>
                    </div>
            	</div>
				<?php endif; 
				if (in_array(6, $sim_temp) || in_array(9, $sim_temp)): ?>
				<div class="linersim">
					<div class="Radio-box Selectsim">
						<label class="radiostyle">
                            <input type="radio" name="sim_ques_type" class="sim_ques_type" value="5" />
                            <span class="radiomark white respwhite"></span>
                            <span class="text-R">Linear Video Simulation</span>
                        </label>
					</div>
					<div class="edit-sim-radio Linerm">
					<?php if (in_array(6, $sim_temp)): ?>
						<div class="Radio-box">
							<label class="radiostyle">
								<input type="radio" name="sim_tmp" value="6" class="linear_tmp tmp_type" />
								<span class="radiomark"></span>
								<span class="text-R">Classic Template</span>
							</label>
							<span class="smallT">
								<p class="question-type">(MCQ, Drag and Drop, Swiping)</p>
								<p class="question-type">(Multiple Feedback)</p>
							</span>
						</div>
						<?php endif; 
						if (in_array(9, $sim_temp)): ?>
						<div class="Radio-box">
							<label class="radiostyle">
								<input type="radio" name="sim_tmp" value="9" class="linear_tmp tmp_type" />
								<span class="radiomark"></span>
								<span class="text-R">Multiple Template</span>
							</label>
							<span class="smallT">
								<p class="question-type">(MTF, Sequence, Sorting, MMCQ, MCQ, Drag and Drop, Swiping)</p>
								<p class="question-type">(Single Feedback)</p>
							</span>
						</div>
						<?php endif; ?>
					</div>
				</div>
				<?php endif; 
				if (in_array(10, $sim_temp) || in_array(11, $sim_temp)): ?>
				<div class="linersim linersimBotom">
					<div class="Radio-box Selectsim">
						<label class="radiostyle">
                        	<input type="radio" name="sim_ques_type" class="sim_ques_type" value="6" />
							<span class="radiomark white respwhite"></span>
							<span class="text-R">Branching Video Simulation</span>
						</label>
					</div>
					<div class="edit-sim-radio Linerm">
					<?php if (in_array(10, $sim_temp)): ?>
						<div class="Radio-box">
							<label class="radiostyle">
                            	<input type="radio" name="sim_tmp" value="10" class="branch_tmp tmp_type" />
								<span class="radiomark"></span>
								<span class="text-R">Classic Template</span>
							</label>
							<span class="smallT">
								<p class="question-type">(MCQ, Drag and Drop,Swiping)</p>
								<p class="question-type">(Multiple Feedback)</p>
							</span>
						</div>
						<?php endif; 
						if (in_array(11, $sim_temp)): ?>
						<div class="Radio-box">
							<label class="radiostyle">
								<input type="radio" name="sim_tmp" value="11" class="branch_tmp tmp_type" />
								<span class="radiomark"></span>
								<span class="text-R">Multiple Template</span>
							</label>
							<span class="smallT">
								<p class="question-type">(MTF, Sorting, MMCQ, MCQ, Drag and Drop, Swiping)</p>
								<p class="question-type">(Single Feedback)</p>
							</span>
						</div>
						<?php endif; ?>
					</div>
				</div>
				<?php endif; ?>
			</div>
		</div>
		<div class="browsebtncreate">
			<button type="submit" class="btn btn-primary" id="create_sim_template">Create Simulation</button>
		</div>
      </form>
   </div>
</div>
<script type="text/javascript">
$('.sim_type').on('change', function() {
	$('.sim_type').not(this).prop('checked', false);
});

$('.tmp_type').on('change', function() {
	var qtype = $('.sim_ques_type:checked').val();
	if (qtype == 1) {
		$('.branch_tmp').prop('checked', false);
	}
	else if (qtype == 2) {
		$('.linear_tmp').prop('checked', false);
	}
});

$('.sim_ques_type').on('change', function() {
	$('.branch_tmp,.linear_tmp').prop('checked', false);
});

$('#upload').click(function() {
	$(this).attr('disabled', 'disabled').html('<i class="fa fa-refresh fa-spin fa-fw"></i><span class="sr-only">Loading...</span> Please wait....');
	var file_data = $('#file').prop('files')[0];
	var form_data = new FormData();
	form_data.append('file', file_data);
	form_data.append('scenario_cover_file', true);
	var ftype   = file_data.type;
	var type	= $('#scenario_media_file_type').val();
	var ext 	= ftype.split('/')[0];
	if (ext == type) {
		$.ajax({
			url: "includes/upload.php",
			type: "POST",
			data: form_data,
			contentType: false,
			cache: false,
			processData:false,
			success: function(resdata) {
				var res = $.parseJSON(resdata);
				if (res.success == true) {
					var path = '<?php echo $uploadpath; ?>';
					$('.sim_cover_preview').attr("src", path + res.file_name);
					$('#ImgDelete').show();
					$('.delete_scenario_media_file').attr('data-scenario-media-file', res.file_name);
					$('#scenario_cover_file').val(res.file_name);
					$('#upload').html('Upload').removeAttr('disabled', 'disabled');
					swal(res.msg, { buttons: false, timer: 1000, });
				}
				else if (res.success == false) {
					swal("Error", res.msg, "error");
					$('#scenario_cover_file').val('');
					$('#splashImg').hide('slow');
					$('#upload').html('Upload').removeAttr('disabled', 'disabled');
				}
			},error: function() {
				$('#splashImg').hide('slow');
				swal("Error", 'Oops. something went wrong please try again.?', "error");
			}
		});
	} else { 
		swal("Warning", 'Please select valid file. accept only '+ type +' file', "warning");
		$('#upload').html('Upload').removeAttr('disabled', 'disabled');
	}
});

$('.delete_scenario_media_file').click(function() {
	var file_name   = $(this).attr("data-scenario-media-file");
	var dataString	= 'delete='+ true + '&scenario_media_file='+ file_name;
	if (file_name) {
		swal({
			title: "Are you sure?",
			text: "Delete this file.",
			icon: "warning",
			buttons: [true, 'Delete'],
			dangerMode: true }).then((willDelete) => { if (willDelete) {
				$('#splashImg').show().html(' <img src="scenario/img/loader.gif"> Please wait....');
				$.ajax({
					url: "includes/delete-file.php",
					type: "POST",
					data: dataString,
					cache: false,
					success: function(resdata) {
						var res = $.parseJSON(resdata);
						if (res.success == false) {
							swal("Error", res.msg, "error");
							$('#splashImg').hide('slow');
						}
						else if (res.success == true) {
							$('#splashImg, #ImgDelete').hide('slow');
							$('#scenario_cover_file').val('');
							$('.sim_cover_preview').attr("src", "img/addbackgroundimage.png");
							swal(res.msg, { buttons: false, timer: 1000, });
						}
					},error: function() {
						swal("Error", 'Oops, something went wrong. Please try again later.?', "error");
					}
				});
			} else { swal("Your file is safe.!", { buttons: false, timer: 1000, }); }
		});
	}
});
</script>
<?php 
require_once 'includes/footer.php';
