<?php 
session_start();
if(isset($_SESSION['username'])) {
    $user 	= $_SESSION['username'];
	$role 	= $_SESSION['role'];
	$userid = $_SESSION['userId'];
}
else {
    header('location: index.php');
}
require_once 'includes/db.php';
require_once 'config/db.class.php';
require_once 'includes/formkey.class.php';
require_once 'includes/learners_menu.php';
require_once 'config/pagination.php';

$formKey = new formKey();
$db = new DBConnection();

if ( isset($_GET['delete']) && ! empty($_GET['status_id'])):
	$id	 = base64_decode($_GET['status_id']);
	$sql = "DELETE FROM scenario_status WHERE sid = ?";
    $q   = $db->prepare($sql);
	if ($q->execute(array($id))):
		$delete = 'Scenario status delete successfully.!';
	endif;
endif;

$sqlr       = "SELECT * FROM scenario_status ORDER BY sid DESC";
$q          = $db->prepare($sqlr); $q->execute();
$rowCount   = $q->rowCount();
$count      = ($sqlr) ? $rowCount : 0;
$page       = (isset($_REQUEST['page']) && ! empty($_REQUEST['page'])) ? $_REQUEST['page'] : 1;
$show       = 15;
$start      = ($page - 1) * $show;
$pagination = getPaginationString($count, $show, $page, 1, basename(__FILE__), '?page=');
$sql        = "SELECT * FROM scenario_status ORDER BY sid DESC LIMIT $start, $show";
$results    = $db->prepare($sql);
$msg        = (isset($_GET['msg'])) ? $_GET['msg'] : ''; ?>
<div class="bottomheader">
    <div class="container">
        <ul class="breadcrumb">
			<?php echo $db->breadcrumbs(); ?>
         </ul>
     </div>
</div>
<div class="Group_managment">
    <div class="container">
		<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
			<div class="panel panelCaollapse">
				<?php if ( ! empty($msg) && isset($_GET['add']) == 'true'): ?>
                	<div class="alert alert-success alert-dismissable">Stage added successfully.!</div>
                <?php elseif ( ! empty($msg) && isset($_GET['exist']) == 'true'): ?>
                	<div class="alert alert-danger alert-dismissable">Stage already exists. please try another one.!</div>
                <?php endif; ?>
                <?php if ( ! empty($delete)): ?>
                	<div class="alert alert-success alert-dismissable"><?php echo $delete; ?></div>
                <?php endif; ?>
				<div class="panel-heading" role="tab" id="headingOne">
					<h4 class="panel-title">
						<a  class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
							Create Stage
						</a>
					</h4>
				</div>
                <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
					<div class="panel-body">
                        <form enctype="multipart/form" action="operations/manage_scenario.php" name="frmscenarioModal" method="POST">
                            <?php echo $formKey->outputKey(); ?>
                            <input type="hidden" name="frmscenario" id="frmscenario" value="<?php echo encryptString('modalScenarioStatus', 'e'); ?>">
                            <div class="usermanage-form">
                            	<div class="row">
                                	<div class="col-sm-6">
                                    	<div class="form-group">
                                        	<input type="text" class="form-control" name="status_name" id="status_name" placeholder="Stage Name" required="required">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12 ">
                                        <div class="form-group">
                                        	<textarea name="status_description" id="status_description" placeholder="Stage Description" class="form-control"></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="usermanage-add clearfix"><input type="submit" class="btn btn-primary pull-right" value="Add Stage"></div>
                        </form>
					</div>
				</div>
			</div>	
		</div>
        <div class="usermanage_main">
            <div class="table-box">
                <div class="tableheader clearfix">
                   <div class="tableheader_left">List of Users</div>
                   <div class="tableheader_right">
                       <div class="Searchbox"><input type="text" class="serchtext"></div>
                       <div class="sorttext">  <span class="sort">Sort by</span><img  src="img/down_arrow_white.png"></div>
                   </div>
                </div>                
            </div>
            <div class="usermanage-form1">
               <div class="table-responsive">        
                    <table class="table table-striped">
                        <thead class="Theader">
                            <tr>
                                <th>#</th>
                                <th>Stage Name</th>
                                <th>Description</th>
                                <th>Created Date</th>
                                <th>Edit</th>                                                                     
                            </tr>
                         </thead>
                        <tbody>
                        <?php if ($results->execute() && $rowCount > 0):
                            $i = 1;
                            foreach ($results->fetchAll(PDO::FETCH_ASSOC) as $row): ?>
                        <tr>
                            <td><?php echo $i ?></td>
                            <td><?php echo $row['status_name'] ?></td>
                            <td><?php echo $row['status_des'] ?></td>
                            <td><?php echo $db->dateFormat($row['status_date'], 'd-M-y'); ?></td>
                            <td><a class="edit" title="Edit" data-scenario-status-id="<?php echo md5($row['sid']) ?>" href="javascript:void(0);"><img src="img/icons/edit.png"></a></td>
                        </tr>
                        <?php $i++; endforeach; else: ?>
                        <tr><td colspan="7" style="text-align:center"><strong>No data available in database.</strong></td></tr>
                        <?php endif; ?>
                       </tbody>
                    </table>
                </div>
                <div class="pull-right"><nav aria-label="navigation"><?php echo ( ! empty($pagination) ) ? $pagination : '&nbsp;'; ?></nav></div>
            </div>
          </div>
    </div>
</div>

<div id="load_popup_modal_show" class="modal fade" tabindex="-1"></div>

<script type="text/javascript">
	$('.edit').click(function () {
		var id = $(this).attr('data-scenario-status-id');
		if (id != '') {
			$.LoadingOverlay("show");
			var $modal = $('#load_popup_modal_show');
			$modal.load('edit-scenario-status-modal.php', {'data-id': id }, function(res) {
				if (res != '') {
					$.LoadingOverlay("hide");
					$modal.modal('show');
				}
				else {
					$.LoadingOverlay("hide");
					swal("Error", 'Oops. something went wrong please try again.?', "error");
				}
			});
		}
	});
</script>
<?php 
require_once 'includes/footer.php';
