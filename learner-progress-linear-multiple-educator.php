<?php 
session_start();
if (isset($_SESSION['username'])):
	$user 	= $_SESSION['username'];
	$role 	= $_SESSION['role'];
	$userid	= $_SESSION['userId'];
	$domain = (isset($_SESSION['domain_name'])) ? $_SESSION['domain_name'] : $_SERVER['HTTP_HOST'];
else:
    header('location: index.php');
endif;
require_once 'config/db.class.php';
require_once 'includes/learners_menu.php';
$db				= new DBConnection();
$sid			= (isset($_GET['sid'])) ? $_GET['sid'] : die('Error');
$uid			= (isset($_GET['uid'])) ? $_GET['uid'] : die('Error');
$lid			= (isset($_GET['lid'])) ? $_GET['lid'] : die('Error');
$scenario		= $db->getScenario($sid);
$sim_type 		= $scenario['sim_ques_type'];
$scenario_id	= $scenario['scenario_id'];
$attempt_uid	= $uid;
$icon_path		= 'img/list/';

#-----------COMPLETE-INCOMPLETE-----------------------------
$getTotalQuestions	 = $db->getTotalQuestionsByScenario($sid);
$getTotalAttemptQues = $db->getTotalAttemptQuesMulti($scenario_id, $lid);

#------------Progress-Time----------------------------------
$getTotalTime = $db->getLastTotalTimeMulti($scenario_id, $lid, $attempt_uid);

#--------------TOTAL PASS/FAIL------------------------------
$sql = "SELECT * FROM competency_tbl WHERE scenario_id = '". $scenario_id ."'";
$q	 = $db->prepare($sql); $q->execute();
$row = $q->fetch(PDO::FETCH_ASSOC);

#----Questions-Score----
$sqlQSc = "SELECT SUM(ques_val_1) AS com_qes_score_1, SUM(ques_val_2) AS com_qes_score_2, SUM(ques_val_3) AS com_qes_score_3, SUM(ques_val_4) AS com_qes_score_4, SUM(ques_val_5) AS com_qes_score_5, SUM(ques_val_6) AS com_qes_score_6 FROM 
		   question_tbl WHERE scenario_id = '". $scenario_id ."'";
$qsc = $db->prepare($sqlQSc); $qsc->execute();
foreach ($qsc->fetchAll(PDO::FETCH_ASSOC) as $QscRow):
	$qdata[] = $QscRow;
endforeach;

#----Correct-Questions-Score----
$sqlSc = "SELECT SUM(q.ques_val_1) AS com_score_1, SUM(q.ques_val_2) AS com_score_2, SUM(q.ques_val_3) AS com_score_3, SUM(q.ques_val_4) AS com_score_4, SUM(q.ques_val_5) AS com_score_5, SUM(q.ques_val_6) AS com_score_6 FROM 
		  question_tbl q LEFT JOIN 
		  multi_score_tbl m ON q.question_id = m.question_id WHERE 
		  m.status = 1 AND m.scenario_id = '". $scenario_id ."' AND m.uid = '". $attempt_uid ."'";
$sc = $db->prepare($sqlSc); $sc->execute();
foreach ($sc->fetchAll(PDO::FETCH_ASSOC) as $scRow):
	$wedata[] = $scRow;
endforeach;

$getweightage_1 = ( ! empty($qdata[0]['com_qes_score_1']) && ! empty($wedata[0]['com_score_1'])) ? $qdata[0]['com_qes_score_1'] / $wedata[0]['com_score_1'] : '';
$getweightage_2 = ( ! empty($qdata[0]['com_qes_score_2']) && ! empty($wedata[0]['com_score_2'])) ? $qdata[0]['com_qes_score_2'] / $wedata[0]['com_score_2'] : '';
$getweightage_3 = ( ! empty($qdata[0]['com_qes_score_3']) && ! empty($wedata[0]['com_score_3'])) ? $qdata[0]['com_qes_score_3'] / $wedata[0]['com_score_3'] : '';
$getweightage_4 = ( ! empty($qdata[0]['com_qes_score_4']) && ! empty($wedata[0]['com_score_4'])) ? $qdata[0]['com_qes_score_4'] / $wedata[0]['com_score_4'] : '';
$getweightage_5 = ( ! empty($qdata[0]['com_qes_score_5']) && ! empty($wedata[0]['com_score_5'])) ? $qdata[0]['com_qes_score_5'] / $wedata[0]['com_score_5'] : '';
$getweightage_6 = ( ! empty($qdata[0]['com_qes_score_6']) && ! empty($wedata[0]['com_score_6'])) ? $qdata[0]['com_qes_score_6'] / $wedata[0]['com_score_6'] : '';

#--------------------DECISION REVIEW----------------------
$getQuestions = $db->getQuestionsScoreByMultiSim($sid, $attempt_uid);
$q = 0; $dataPoints = []; $ansDataPoints = [];
foreach ($getQuestions as $qData):
	$dataPoints[$q]['y'] 	 	= $qData['totalQScore'];
	$dataPoints[$q]['label'] 	= stripslashes($qData['questions']);
	$ansDataPoints[$q]['y'] 	= ( ! empty($qData['status'])) ? $qData['totalQScore'] : 0;
	$ansDataPoints[$q]['label'] = stripslashes($qData['questions']);
	$q++;
endforeach;

#------------MISSED-DECISION-REVIEW--------------
$missedDecisionReview = $db->getMissedDecisionReviewMulti($sid, $attempt_uid);
$miss = 0; $missqdataPoints = []; $missAnsdataPoints = [];
foreach ($missedDecisionReview as $missqData):
	$missqdataPoints[$miss]['y'] 	   = $missqData['totalQScore'];
	$missqdataPoints[$miss]['label']   = stripslashes($missqData['questions']);
	$missAnsdataPoints[$miss]['y'] 	   = 0;
	$missAnsdataPoints[$miss]['label'] = stripslashes($missqData['questions']);
	$miss++;
endforeach;

#-----------CORRECT-DECISION-REVIEW--------------
$correctDecisionReview = $db->getCorrectDecisionReviewMulti($sid, $attempt_uid);
$cor = 0; $corqdataPoints = []; $corAnsdataPoints = [];
foreach ($correctDecisionReview as $corqData):
	$corqdataPoints[$cor]['y'] 	  	 = $corqData['totalQScore'];
	$corqdataPoints[$cor]['label']   = stripslashes($corqData['questions']);
	$corAnsdataPoints[$cor]['y']	 = $corqData['totalQScore'];
	$corAnsdataPoints[$cor]['label'] = stripslashes($corqData['questions']);
	$cor++;
endforeach;

#---------INDIVIUAL COMPETENCY BASED SCORE----------------
$sqlScombase = "SELECT comp_col_1, comp_col_2, comp_col_3, comp_col_4, comp_col_5, comp_col_6, comp_val_1, comp_val_2, comp_val_3, comp_val_4, comp_val_5, comp_val_6, 
				(SUM(comp_val_1) + SUM(comp_val_2) + SUM(comp_val_3) + SUM(comp_val_4) + SUM(comp_val_5) + SUM(comp_val_6)) AS totalCompval FROM 
				competency_tbl WHERE scenario_id = '". $scenario_id ."'";
$qcombaseexc = $db->prepare($sqlScombase); $qcombaseexc->execute();
foreach ($qcombaseexc->fetchAll(PDO::FETCH_ASSOC) as $combaseRow):
	$totalCompval[] = $combaseRow['totalCompval'];
	$competencyDataPoints[] = array_filter(array_map('array_filter', array(
		array("y" => $combaseRow['comp_val_1'], "label" => $combaseRow['comp_col_1']),
	    array("y" => $combaseRow['comp_val_2'], "label" => $combaseRow['comp_col_2']),
	    array("y" => $combaseRow['comp_val_3'], "label" => $combaseRow['comp_col_3']),
	    array("y" => $combaseRow['comp_val_4'], "label" => $combaseRow['comp_col_4']),
	    array("y" => $combaseRow['comp_val_5'], "label" => $combaseRow['comp_col_5']),
	    array("y" => $combaseRow['comp_val_6'], "label" => $combaseRow['comp_col_6']))));
endforeach;

$sqlScombase1 = "SELECT c.comp_col_1, c.comp_col_2, c.comp_col_3, c.comp_col_4, c.comp_col_5, c.comp_col_6, 
				 SUM(q.ques_val_1) AS score1, SUM(q.ques_val_2) AS score2, SUM(q.ques_val_3) AS score3, SUM(q.ques_val_4) AS score4, SUM(q.ques_val_5) AS score5, SUM(q.ques_val_6) AS score6, 
				 (SUM(q.ques_val_1) + SUM(q.ques_val_2) + SUM(q.ques_val_3) + SUM(q.ques_val_4) + SUM(q.ques_val_5) + SUM(q.ques_val_6)) AS totalAnsval FROM 
				 multi_score_tbl sc LEFT JOIN 
				 question_tbl q ON sc.question_id = q.question_id LEFT JOIN 
				 competency_tbl c ON sc.scenario_id = c.scenario_id WHERE 
				 sc.status = 1 AND sc.scenario_id = '". $scenario_id ."' AND sc.uid = '". $attempt_uid ."'";
$qcombaseexc1 = $db->prepare($sqlScombase1); $qcombaseexc1->execute();
foreach ($qcombaseexc1->fetchAll(PDO::FETCH_ASSOC) as $combaseRow1):
	$totalAnsval[] = $combaseRow1['totalAnsval'];
	$achievedDataPoints[] = array_filter(array_map('array_filter', array(
		array("y" => $combaseRow1['score1'], "label" => $combaseRow1['comp_col_1']),
		array("y" => $combaseRow1['score2'], "label" => $combaseRow1['comp_col_2']),
		array("y" => $combaseRow1['score3'], "label" => $combaseRow1['comp_col_3']),
		array("y" => $combaseRow1['score4'], "label" => $combaseRow1['comp_col_4']),
		array("y" => $combaseRow1['score5'], "label" => $combaseRow1['comp_col_5']),
		array("y" => $combaseRow1['score6'], "label" => $combaseRow1['comp_col_6']))));
endforeach;

#-----------------JOB READINESS REPORT----------------------
$dataPointweightage1 = array(array("y" => ( ! empty($getweightage_1) && ! empty($row['comp_col_1'])) ? round($row['weightage_1'] / $getweightage_1) : 0, "label" => ( ! empty($getweightage_1) && ! empty($row['comp_col_1'])) ? stripslashes($row['comp_col_1']) : ''),
							 array("y" => ( ! empty($getweightage_2) && ! empty($row['comp_col_2'])) ? round($row['weightage_2'] / $getweightage_2) : 0, "label" => ( ! empty($getweightage_2) && ! empty($row['comp_col_2'])) ? stripslashes($row['comp_col_2']) : ''),
							 array("y" => ( ! empty($getweightage_3) && ! empty($row['comp_col_3'])) ? round($row['weightage_3'] / $getweightage_3) : 0, "label" => ( ! empty($getweightage_3) && ! empty($row['comp_col_3'])) ? stripslashes($row['comp_col_3']) : ''),
							 array("y" => ( ! empty($getweightage_4) && ! empty($row['comp_col_4'])) ? round($row['weightage_4'] / $getweightage_4) : 0, "label" => ( ! empty($getweightage_4) && ! empty($row['comp_col_4'])) ? stripslashes($row['comp_col_4']) : ''),
							 array("y" => ( ! empty($getweightage_5) && ! empty($row['comp_col_5'])) ? round($row['weightage_5'] / $getweightage_5) : 0, "label" => ( ! empty($getweightage_5) && ! empty($row['comp_col_5'])) ? stripslashes($row['comp_col_5']) : ''),
							 array("y" => ( ! empty($getweightage_6) && ! empty($row['comp_col_6'])) ? round($row['weightage_6'] / $getweightage_6) : 0, "label" => ( ! empty($getweightage_6) && ! empty($row['comp_col_6'])) ? stripslashes($row['comp_col_6']) : ''));

$dataPoints3 = array_values(array_filter(array_map('array_filter', $dataPointweightage1)));

$dataPointweightage2 = array(array("y" => $row['weightage_1'], "label" => ( ! empty($row['weightage_1']) && ! empty($row['comp_col_1'])) ? stripslashes($row['comp_col_1']) : ''),
							 array("y" => $row['weightage_2'], "label" => ( ! empty($row['weightage_2']) && ! empty($row['comp_col_2'])) ? stripslashes($row['comp_col_2']) : ''),
							 array("y" => $row['weightage_3'], "label" => ( ! empty($row['weightage_3']) && ! empty($row['comp_col_3'])) ? stripslashes($row['comp_col_3']) : ''),
							 array("y" => $row['weightage_4'], "label" => ( ! empty($row['weightage_4']) && ! empty($row['comp_col_4'])) ? stripslashes($row['comp_col_4']) : ''),
							 array("y" => $row['weightage_5'], "label" => ( ! empty($row['weightage_5']) && ! empty($row['comp_col_5'])) ? stripslashes($row['comp_col_5']) : ''),
							 array("y" => $row['weightage_6'], "label" => ( ! empty($row['weightage_6']) && ! empty($row['comp_col_6'])) ? stripslashes($row['comp_col_6']) : ''));

$dataPoints4 = array_values(array_filter(array_map('array_filter', $dataPointweightage2)));

#-----------CONSOLIDATED COMPETENCIES SCORE---------------
$consoldataPoints = [];
$sql_consol_com = "SELECT sc.status, q.questions, q.ques_val_1, q.ques_val_2, q.ques_val_3, q.ques_val_4, q.ques_val_5, q.ques_val_6, c.comp_col_1, c.comp_col_2, c.comp_col_3, c.comp_col_4, c.comp_col_5, c.comp_col_6 FROM 
				   multi_score_tbl sc LEFT JOIN 
				   question_tbl q ON sc.question_id = q.question_id LEFT JOIN 
				   competency_tbl c ON sc.scenario_id = c.scenario_id WHERE 
				   sc.scenario_id = '". $scenario_id ."' AND sc.uid = '". $attempt_uid ."'";
$sql_consol_exc = $db->prepare($sql_consol_com); $sql_consol_exc->execute();
foreach ($sql_consol_exc->fetchAll(PDO::FETCH_ASSOC) as $consolRow):
	$consoldataPoints[$consolRow['questions']] = array_filter(array_map('array_filter', 
		array(array("y" => ( ! empty($consolRow['status'])) ? $consolRow['ques_val_1'] : 0, "label" => stripslashes($consolRow['comp_col_1']), 'indexLabel' => stripslashes($consolRow['ques_val_1'])),
			  array("y" => ( ! empty($consolRow['status'])) ? $consolRow['ques_val_2'] : 0, "label" => stripslashes($consolRow['comp_col_2']), 'indexLabel' => stripslashes($consolRow['ques_val_2'])),
			  array("y" => ( ! empty($consolRow['status'])) ? $consolRow['ques_val_3'] : 0, "label" => stripslashes($consolRow['comp_col_3']), 'indexLabel' => stripslashes($consolRow['ques_val_3'])),
			  array("y" => ( ! empty($consolRow['status'])) ? $consolRow['ques_val_4'] : 0, "label" => stripslashes($consolRow['comp_col_4']), 'indexLabel' => stripslashes($consolRow['ques_val_4'])),
			  array("y" => ( ! empty($consolRow['status'])) ? $consolRow['ques_val_5'] : 0, "label" => stripslashes($consolRow['comp_col_5']), 'indexLabel' => stripslashes($consolRow['ques_val_5'])),
			  array("y" => ( ! empty($consolRow['status'])) ? $consolRow['ques_val_6'] : 0, "label" => stripslashes($consolRow['comp_col_6']), 'indexLabel' => stripslashes($consolRow['ques_val_6'])))));
endforeach;

$attemptsql  = "SELECT uid, scenario_id, userid, DATE_FORMAT(attempt_date, '%l:%i %p %b %e, %Y') AS atdate FROM scenario_attempt_tbl WHERE scenario_id = '". $scenario_id ."' AND userid = '". $lid ."'";
$attemptqexe = $db->prepare($attemptsql); $attemptqexe->execute();
if ($attemptqexe->rowCount() > 0):
	foreach ($attemptqexe->fetchAll(PDO::FETCH_ASSOC) as $attemptrow):
		$sqlSc1 = "SELECT (c.comp_val_1 + c.comp_val_2 + c.comp_val_3 + c.comp_val_4 + c.comp_val_5 + c.comp_val_6) AS totalCompval, 
				   (SUM(q.ques_val_1) + SUM(q.ques_val_2) + SUM(q.ques_val_3) + SUM(q.ques_val_4) + SUM(q.ques_val_5) + SUM(q.ques_val_6)) AS totalAnsval 
				   FROM multi_score_tbl sc LEFT JOIN 
				   question_tbl q ON sc.question_id = q.question_id LEFT JOIN 
				   competency_tbl c ON sc.scenario_id = c.scenario_id WHERE sc.status = 1 AND sc.scenario_id = '". $attemptrow['scenario_id'] ."' AND sc.uid = '". $attemptrow['uid'] ."'";
		$scg = $db->prepare($sqlSc1); $scg->execute();
		$resdata[$attemptrow['atdate']] = 0;
		foreach ($scg->fetchAll(PDO::FETCH_ASSOC) as $scgRow):
			$resdata[$attemptrow['atdate']] = array($scgRow['totalAnsval'], $scgRow['totalCompval']);
		endforeach;
	endforeach;
endif;

$attempt_data = [];
if ( ! empty($resdata)):
	foreach ($resdata as $k => $v):
		$attempt_data[] = array('y' => ( ! empty($v[0])) ? $v[0] : 0, 'label' => $k, 'yValueFormatString' => "# '/".$v[1]."'");
	endforeach;
endif; ?>
<link rel="stylesheet" media="screen" href="content/css/report_page.css" />
<div class="bottomheader">
    <div class="container">
		<div class="row">
			<ul class="breadcrumb">
				<?php #echo $db->breadcrumbs(); ?>
			</ul>
		 </div>
     </div>
</div>
<div class="bottomheader" style="margin-top:0px">
	<div class="container">
		<div class="row">
			<div class="col-sm-10 S_R_text">
				<h3 style="margin-top: 10px;">Simulations Report</h3>
			</div>
			<div class="col-sm-2 S_R-icon">
                <a id="print" onclick="getPDF('#print_data')" title="Download Report"><img class="svg" src="<?php echo $icon_path; ?>download_report.svg" /></a>
			</div>
		</div>
	</div>
    <div id="print_data">
    	<div class="lernar-dashboard learner-progress-Banner learner_dashboard">
        	<div class="container">
            	<div class="row">
					<?php $user_data = $db->getUsersColumns($lid, 'id, username, fname, lname, company, department, location')[0]; ?>
                    <div class="col-sm-12">
                        <h3 class="adminGraphtitle"><?php echo $scenario['Scenario_title'] ?></h3><br/>
                        <strong>User ID:</strong> <?php echo $user_data['username'] ?><strong>, Full Name: </strong><?php echo $user_data['fname'] . ' ' . $user_data['lname'] ?><strong>, Company: </strong><?php echo $user_data['company'] ?><strong>, Department: </strong><?php echo $user_data['department'] ?><strong>, Location: </strong><?php echo $user_data['location'] ?><strong>
                    </div>
                    <div class="col-sm-6">
                        <div class="report_page comp-incomp-topbanner report_page_main Authoreport-box">
                            <div class="comp-incomp-header col-sm-12">
                                <h3>DECISION PROMPTNESS</h3>
                            </div>
                            <div>
                                <div class="col-sm-12">
                                    <div class="score">
                                        <div class="col-sm-6">
                                            <span class="Score-Y"><?php echo ( ! empty($sid)) ? gmdate('H:i:s', ($scenario['duration']) * 60) : '00' ?></span>
                                            <p>Allocated time</p>
                                        </div>
                                        <div class="col-sm-6">
                                            <span class="Score-B"><?php echo ( ! empty($sid)) ? $getTotalTime['timeSum'] : '00'; ?></span>
                                            <p>Taken Time</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="report_page comp-incomp-topbanner report_page_main Authoreport-box">
                            <div class="comp-incomp-header col-sm-12">
                                <h3>SIMULATION STATUS</h3>
                            </div>
                            <div class="comp-incomp-banner">
                                <div class="col-sm-12">
                                    <div class="comp-incomp text-center">
                                    <h1><?php if ($getTotalAttemptQues > 0 && $getTotalAttemptQues == $getTotalQuestions): ?>
										<span class="pass-text">COMPLETE</span>
									<?php else: ?>
										<span class="fail-text">INCOMPLETE</span>
									<?php endif; ?></h1>
                                    </div>
                                </div>
                            </div>
                            <div>
                                <div class="col-sm-12">
                                    <div class="score">
                                        <div class="col-sm-6">
                                            <span class="Score-Y"><?php echo ( ! empty($sid)) ? $getTotalQuestions : 0; ?></span>
                                            <p>Total Question</p>
                                        </div>
                                        <div class="col-sm-6">
                                            <span class="Score-B"><?php echo ( ! empty($sid)) ? $getTotalAttemptQues : 0; ?></span>
                                            <p>Question Attempted</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> 
                    </div>
                    <div class="col-sm-6">
                        <div class="report_page comp-incomp-topbanner report_page_main Authoreport-box">
                            <div class="comp-incomp-header col-sm-12">
                                <h3>SIMULATION RESULT</h3>
                            </div>
                            <div class="comp-incomp-banner">
                                <div class="col-sm-12">
                                    <div class="comp-incomp text-center">
                                    <?php 
									$passing_marks	= ($scenario['passing_marks'] > 0) ? $scenario['passing_marks'] : 0;
									$achieved_score	= $db->get_percentage($totalCompval[0], $totalAnsval[0]); ?>
                                    <h1><?php 
									if ($db->checkCriticalMultiTemp($scenario['scenario_id'], $attempt_uid) == TRUE || $db->checkCriticalMultiTemp($scenario['scenario_id'], $attempt_uid) == 2):
										if ($achieved_score > 0 && $achieved_score >= $passing_marks): ?>
											<img class="svg" src="<?php echo $icon_path ?>happy.png" />
											<span class="pass-text">PASS</span>
										<?php else: ?>
											<span class="fail-text">REMEDIATION REQUIRED</span>
										<?php endif; else: ?>
											<span class="fail-text">REMEDIATION REQUIRED</span>
											<span class="critical-text">You answered one or more critical competency questions incorrectly.</span>
										<?php endif; ?></h1>
                                    </div>
                                 </div>
                            </div>
                            <div>
								<div class="col-sm-12">
                                    <div class="score">
                                        <div class="col-sm-3">
                                            <span class="Score-Y"><?php echo ($sim_type == 2) ? $getTotalQscore : $totalCompval[0]; ?></span>
                                            <p>Total Score</p>
                                        </div>
                                        <div class="col-sm-3">
                                            <span class="Score-Y"><?php echo $totalAnsval[0]; ?></span>
                                            <p>Score Achieved</p>
                                        </div>
                                        <div class="col-sm-3">
                                            <span class="Score-B"><?php echo $passing_marks ?>%</span>
                                            <p>Passing Score(%)</p>
                                        </div>
                                        <div class="col-sm-3">
                                            <span class="Score-B"><?php echo $achieved_score ?>%</span>
                                            <p>Score(%) Achieved</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> 
                    </div>
                    <div class="col-sm-6">
                        <div class="comp-incomp-topbanner">
                            <div class="comp-incomp-header">
                                <h3>JOB READINESS</h3>
                            </div>
                            <div class="row report_page T-alinment">
                                <div class="score">
                                    <div class="col-sm-6">
                                        <span class="Score-Y"><?php echo ( ! empty($sid)) ? array_sum(array_column($dataPoints4, 'y')) : '00'; ?></span>
                                        <p>Total Weightage Score</p>
                                    </div>
                                    <div class="col-sm-6">
                                        <span class="Score-B"><?php echo ( ! empty($sid)) ?  array_sum(array_column($dataPoints3, 'y')) : 0; ?></span>
                                        <p>Total Weightage Achieved</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div id="Job_Read_Score" style="height: 240px; width: 100%;"></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="comp-incomp-topbanner">
                        	<div class="col-sm-3 form-group form-select pull-right">
                                    <select class="form-control" id="decision_review">
                                        <option value="1" selected="selected">Default</option>
                                        <option value="2">Missed</option>
                                        <option value="3">Correct</option>
                                    </select>
                                </div>
                            <div class="comp-incomp-header">
                                <h3>DECISION REVIEW</h3>
                            </div>
                            <div class="comp-incomp-banner">
                                <div id="Sim_Score" style="height: 350px; width: 100%;"></div>
                                <div id="Missed_Decision_review" style="height: 350px; width: 100%; display:none;"></div>
                                <div id="Correct_Decision_review" style="height: 350px; width: 100%; display:none;"></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="comp-incomp-topbanner">
                            <div class="comp-incomp-header">
                                <h3>MISSED QUESTIONS</h3>
                            </div>
                            <div class="comp-incomp-banner LRtableRes">
                            	<div class="table-responsive">
                                	<table class="table table-striped">
                                    	<thead class="Theader">
                                        	<tr>
                                            	<th>Question</th>
                                                <th>Score</th>
                                            </tr>
                                    	</thead>
                                    	<tbody>
                                        <?php if ( ! empty($missqdataPoints)): foreach ($missqdataPoints as $missqdata): ?>
                                        <tr>
                                        	<td><?php echo $missqdata['label'] ?></td>
                                            <td><?php echo $missqdata['y'] ?></td>
                                        </tr>
                                        <?php endforeach; endif; ?>
                                    	</tbody>
                                	</table>
                            	</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="comp-incomp-topbanner">
                            <div class="comp-incomp-header">
                                <h3>MY COMPETENCIES</h3>
                            </div>
                            <div class="comp-incomp-banner">
                                <div id="Indu_Comp_Score" style="height: 350px; width: 100%;"></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="comp-incomp-topbanner">
                            <div class="comp-incomp-header">
                                <h3>CONSOLIDATED COMPETENCIES SCORE</h3>
                            </div>
                            <div class="comp-incomp-banner">
                                <div id="Console_Score" style="height: 350px; width: 100%;"></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="comp-incomp-topbanner">
                            <div class="comp-incomp-header">
                                <h3>PERFORMANCE IN EACH ATTEMPT</h3>
                            </div>
                            <div class="comp-incomp-banner">
                                <div id="attempt_data" style="height: 350px; width: 100%;"></div>
                            </div>
                        </div>
                    </div>
           		</div>
        	</div>
    	</div>
	</div>
</div>
<style type="text/css">
@media screen and (-ms-high-contrast: active), (-ms-high-contrast: none) {
	p.circleChart_text { left: 350%; }
}
p.circleChart_text {
    font-size: 25px !important;
}
.lernar-dashboard-boxes {
	padding: 10px;
}
.lernar-dashboard-boxes span{
	display:table-cell;
	font-size:40px;
	vertical-align:middle;
}
.lernar-dashboard-boxes p{
	padding-left:10px;
}
@media(max-width: 1450px) and (min-width: 1024px){
	.lernar-dashboard-boxes p {
		padding-right:0px;
	}
}
.wordwrap { 
   white-space: pre-wrap;
   white-space: -moz-pre-wrap;
   white-space: -pre-wrap;
   white-space: -o-pre-wrap;
   word-wrap: break-word;
}
canvas.canvasjs-chart-canvas {
    padding-top: 0px;
}
</style>
<script src="content/js/canvasjs.min.js"></script>
<script type="text/javascript">
$('#decision_review').on('change', function(){
	var type = $(this).val();
	if (type == 1) {
		$('#Sim_Score').show();
		$('#Missed_Decision_review,#Correct_Decision_review').hide();
	}
	else if (type == 2) {
		$('#Missed_Decision_review').show();
		$('#Sim_Score,#Correct_Decision_review').hide();
	}
	else if (type == 3) {
		$('#Correct_Decision_review').show();
		$('#Sim_Score,#Missed_Decision_review').hide();
	}
});

function percentage(partialValue, totalValue) {
   var data = (100 * partialValue) / totalValue;
   return data.toFixed();
}

window.onload = function () {
	var Sim_Score = new CanvasJS.Chart("Sim_Score", {
		backgroundColor: "",
		animationEnabled: true,
		theme: "light2", //"light1", "light2", "dark1", "dark2"
		title: { text: "" },
		axisX: { gridThickness: 0, tickLength: 0, lineThickness: 0, labelFontColor: "white", labelFormatter: function() { return " "; } },
		axisY: { gridThickness: 0, tickLength: 0, lineThickness: 0, labelFormatter: function(){ return " "; } },
		toolTip: { content: "<div style='max-width:200px;' class='wordwrap'>{label}</div>" },
		data: [
				{
					type: "column",
					name: "Points Earned",
					indexLabel: "{y}",
					showInLegend: true,
					dataPoints: <?php echo json_encode($ansDataPoints, JSON_NUMERIC_CHECK); ?>},
				{
					type: "column",
					name: "Maximum Score",
					indexLabel: "{y}",
					showInLegend: true,
					dataPoints: <?php echo json_encode($dataPoints, JSON_NUMERIC_CHECK); ?>}
				]});
		Sim_Score.render();
		
	var missed_Sim_Score = new CanvasJS.Chart("Missed_Decision_review", {
		backgroundColor: "",
		animationEnabled: true,
		theme: "light2", //"light1", "light2", "dark1", "dark2"
		title: { text: "" },
		axisX: { gridThickness: 0, tickLength: 0, lineThickness: 0, labelFontColor: "white", labelFormatter: function() { return " "; } },
		axisY: { gridThickness: 0, tickLength: 0, lineThickness: 0, labelFormatter: function() { return " "; } },
		toolTip: { content: "<div style='max-width:200px;' class='wordwrap'>{label}</div>" },
		height:350,
		data: [
				{
					type: "column",
					name: "Achieved Score",
					indexLabel: "{y}",
					showInLegend: true,
					dataPoints: <?php echo json_encode($missAnsdataPoints, JSON_NUMERIC_CHECK); ?>},
				{
					type: "column",
					name: "Maximum Score",
					indexLabel: "{y}",
					showInLegend: true,
					dataPoints: <?php echo json_encode($missqdataPoints, JSON_NUMERIC_CHECK); ?>}
				]});
		missed_Sim_Score.render();
		
	var correct_Sim_Score = new CanvasJS.Chart("Correct_Decision_review", {
		backgroundColor: "",
		animationEnabled: true,
		theme: "light2", //"light1", "light2", "dark1", "dark2"
		title: { text: "" },
		axisX: { gridThickness: 0, tickLength: 0, lineThickness: 0, labelFontColor: "white", labelFormatter: function() { return " "; } },
		axisY: { gridThickness: 0, tickLength: 0, lineThickness: 0, labelFormatter: function() { return " "; } },
		toolTip: { content: "<div style='max-width:200px;' class='wordwrap'>{label}</div>" },
		height:350,
		data: [
				{
					type: "column",
					name: "Achieved Score",
					indexLabel: "{y}",
					showInLegend: true,
					dataPoints: <?php echo json_encode($corAnsdataPoints, JSON_NUMERIC_CHECK); ?>},
				{
					type: "column",
					name: "Maximum Score",
					indexLabel: "{y}",
					showInLegend: true,
					dataPoints: <?php echo json_encode($corqdataPoints, JSON_NUMERIC_CHECK); ?>}
				]});
		correct_Sim_Score.render();

	var Indu_Comp_Score = new CanvasJS.Chart("Indu_Comp_Score", {
		backgroundColor: "",
		animationEnabled: true,
		title: { text: "" },
		axisX: { gridThickness: 0, tickLength: 0, lineThickness: 0, labelAngle: 0 },
		axisY: { gridThickness: 0, tickLength: 0, lineThickness: 0, labelFormatter: function(){ return " "; }},
		toolTip: { shared: true, content: toolTipFormatters },
		data: [
				{
					type: "stackedBar",
					name: "Score Achieved",
					indexLabel: "{y}",
					showInLegend: true,
					dataPoints: <?php echo json_encode($achievedDataPoints[0], JSON_NUMERIC_CHECK); ?>},
				{
					type: "stackedBar",
					name: "Competency Score",
					indexLabel: "{y}",
					showInLegend: true,
					dataPoints: <?php echo json_encode($competencyDataPoints[0], JSON_NUMERIC_CHECK); ?>}
				]});
		Indu_Comp_Score.render();
		
		function toolTipFormatters(e) {
			var str = "";
			var sa = 0;
			var ca = 0;
			var str3;
			var str2;
			for (var i = 0; i < e.entries.length; i++){
				var str1 = "<span style= \"color:"+e.entries[i].dataSeries.color + "\" class=\"wordwrap\">" + e.entries[i].dataSeries.name + "</span>: <strong>"+  e.entries[i].dataPoint.y + "<br/>" ;
				if (i != 0) {
					var ni = i - 1;
					sa = e.entries[ni].dataPoint.y;
				}
				else { sa = e.entries[i].dataPoint.y; }				
				ca = e.entries[i].dataPoint.y;
				str = str.concat(str1);
			}
			str2 = "<strong>" + e.entries[0].dataPoint.label + "</strong> <br/>";
			str3 = "<span style = \"color:Tomato\">Percentage Achieved: </span><strong>"+ percentage(sa, ca) +"%</strong><br/>";
			return (str2.concat(str)).concat(str3);
		}
		
		var Consol_chart = new CanvasJS.Chart("Console_Score", {
			backgroundColor: "",
			animationEnabled: true,
			title: { text: "" },
			axisX: { gridThickness: 0, tickLength: 0, lineThickness: 0, labelAngle: 0 },
			axisY: { gridThickness: 0, tickLength: 0, lineThickness: 0, labelFormatter: function(){ return " "; }},
			legend: { cursor:"pointer", itemclick : toggleDataSeries },
			toolTip: { shared: true, content: toolTipFormatter },
			data: [<?php foreach ($consoldataPoints as $consoleques => $consolquesPoints): ?>{
				type: "line",
				name: "<?php echo addslashes($consoleques); ?>",
				showInLegend: false,
				dataPoints: [<?php foreach ($consolquesPoints as $consolquesPointsV):?>{label:'<?php echo $consolquesPointsV['label']; ?>', y: <?php echo ( ! empty($consolquesPointsV['y'])) ? $consolquesPointsV['y'] : 0; ?>, yValueFormatString: <?php echo ( ! empty($consolquesPointsV['indexLabel'])) ? $consolquesPointsV['indexLabel'] : 0; ?>},<?php endforeach; ?>]
			},<?php endforeach; ?>]});
		Consol_chart.render();

		function toolTipFormatter(e) {
			var str = "";
			var total = 0;
			var allTotal = 0;
			var str3;
			var str2 ;
			for (var i = 0; i < e.entries.length; i++){
				var str1 = "<span style= \"color:"+e.entries[i].dataSeries.color + "\" class=\"wordwrap\">" + e.entries[i].dataSeries.name + "</span>: <strong>"+  e.entries[i].dataPoint.y + " / "+  e.entries[i].dataPoint.yValueFormatString + "</strong> <br/>" ;
				total = e.entries[i].dataPoint.y + total;
				allTotal = e.entries[i].dataPoint.yValueFormatString + allTotal;
				str = str.concat(str1);
			}
			str2 = "<strong>" + e.entries[0].dataPoint.label + "</strong> <br/>";
			str3 = "<span style = \"color:Tomato\">Total Achieved: </span><strong>" + total + "/ " + allTotal + " ("+ percentage(total, allTotal) +"%)</strong><br/>";
			return (str2.concat(str)).concat(str3);
		}
		
		function toggleDataSeries(e) {
			if (typeof (e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
				e.dataSeries.visible = false;
			}
			else {
				e.dataSeries.visible = true;
			}
			Consol_chart.render();
		}
		
	var job_chart = new CanvasJS.Chart("Job_Read_Score", {
		backgroundColor: "",
		animationEnabled: true,
		theme: "light2", // "light1", "light2", "dark1", "dark2"
		title: { text: "" },
		legend: { cursor: "pointer", verticalAlign: "center", horizontalAlign: "right", itemclick: toggleDataSeries},
		axisX: { gridThickness: 0, tickLength: 0, lineThickness: 0, labelAngle: 0 },
		axisY: { gridThickness: 0, tickLength: 0, lineThickness: 0, labelFormatter: function() { return " "; }},
		data: [
				{
					type: "column",
					name: "Weightage Achieved",
					indexLabel: "{y}",
					showInLegend: true,
					dataPoints: <?php echo json_encode($dataPoints3, JSON_NUMERIC_CHECK); ?>},
				{
					type: "column",
					name: "Weightage Score",
					indexLabel: "{y}",
					showInLegend: true,
					dataPoints: <?php echo json_encode($dataPoints4, JSON_NUMERIC_CHECK); ?>}
				]});
		job_chart.render();

		function toggleDataSeries(e){
			if (typeof(e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
				e.dataSeries.visible = false;
			}
			else {
				e.dataSeries.visible = true;
			}
			job_chart.render();
		}
	
	var attempt_data = new CanvasJS.Chart("attempt_data", {
		backgroundColor: "",
		animationEnabled: true,
		theme: "light2", // "light1", "light2", "dark1", "dark2"
		title: { text: "" },
		axisX: { gridThickness: 0, tickLength: 0, lineThickness: 0, labelFontColor: "white", labelFormatter: function(){ return " "; } },
		axisY: { gridThickness: 0, tickLength: 0, lineThickness: 0, labelFormatter: function(){ return " "; } },
		data: [
				{
					type: "column",
					indexLabel: "{y}",
					indexLabelPlacement: "outside",
					indexLabelFontColor: "black",
					indexLabelFontSize:10,
					dataPoints: <?php echo json_encode($attempt_data, JSON_NUMERIC_CHECK); ?>}
				]});
		attempt_data.render();
}
</script>
<script type="text/javascript" src="content/js/polyfill.min.js"></script>
<script type="text/javascript" src="content/js/jspdf.min.js"></script>
<script type="text/javascript" src="content/js/html2canvas.js"></script>
<script type="text/javascript">
	function getPDF(id){
		var HTML_Width = $(id).width();
		var HTML_Height = $(id).height();
		var top_left_margin = 15;
		var PDF_Width = HTML_Width+(top_left_margin*2);
		var PDF_Height = (PDF_Width*1.5)+(top_left_margin*2);
		var canvas_image_width = HTML_Width;
		var canvas_image_height = HTML_Height;		
		var totalPDFPages = Math.ceil(HTML_Height/PDF_Height)-1;
		html2canvas($(id)[0],{allowTaint:true}).then(function(canvas) {
			var imgData = canvas.toDataURL("image/jpeg", 1.0);
			var pdf = new jsPDF('p', 'pt',  [PDF_Width, PDF_Height]);
		    pdf.addImage(imgData, 'JPG', top_left_margin, top_left_margin,canvas_image_width,canvas_image_height);
			for (var i = 1; i <= totalPDFPages; i++) {
				pdf.addPage(PDF_Width, PDF_Height);
				pdf.addImage(imgData, 'JPG', top_left_margin, -(PDF_Height*i)+(top_left_margin*4),canvas_image_width,canvas_image_height);
			}
			pdf.save("<?php echo strtoupper($user_data['username']).'-'.time() ?>-Learner-Report.pdf");
        });
	}
</script>
<?php 
require_once 'includes/footer.php';
