<?php
ob_start();
session_start();
if (isset($_SESSION['username'])) {
	$user   = $_SESSION['username'];
	$role   = $_SESSION['role'];
	$userid = $_SESSION['userId'];
	$cid    = (isset($_SESSION['client_id'])) ? $_SESSION['client_id'] : exit('Oops. something went wrong please try again');
} else {
	header('location: index.php');
}
require_once 'config/db.class.php';
require_once 'includes/learners_menu.php';
$db = new DBConnection(); ?>
<link rel="stylesheet" type="text/css" href="content/css/report_page.css" />
<link rel="stylesheet" href="content/fancybox/jquery.fancybox.min.css" />
<script type="text/javascript" src="content/fancybox/jquery.fancybox.min.js"></script>
<div class="bottomheader">
	<div class="container-fluid">
		<ul class="breadcrumb">
			<?php echo $db->breadcrumbs(); ?>
		</ul>
	</div>
</div>
<div class="admin-report GROUPpreFormancemain">
	<div class="bottomheader1">
		<div class="container-fluid">
			<div class="row">
				<div class="col-sm-12">
					<form name="report_form" id="report_form" method="post">
						<div class="usermanage-form GrouPerformainbanner">
							<div class="row">
								<div class="col-sm-3">
									<div class="form-group form-select">
										<select name="category" id="category" class="form-control" data-placeholder="Category List" required="required">
											<option value="0" selected="selected" disabled="disabled" hidden>Select Category</option>
											<?php foreach ($db->getCategory() as $category) : ?>
												<option value="<?php echo $category['cat_id'] ?>"><?php echo ucwords($category['category']); ?></option>
											<?php endforeach; ?>
										</select>
									</div>
								</div>
								<div class="col-sm-3">
									<div class="form-group form-select">
										<select name="sim" id="sim" class="form-control selection" data-placeholder="Simulation List" required="required">
											<option value="" selected="selected" disabled="disabled" hidden>Select Simulation</option>
										</select>
									</div>
								</div>
								<div class="col-sm-3">
									<div class="form-group form-select">
										<select name="group" id="group" class="form-control selection" data-placeholder="Group List" required="required">
											<option value="" selected="selected" disabled="disabled" hidden>Select Group</option>
										</select>
									</div>
								</div>
								<div class="col-sm-3">
									<div class="form-group form-select">
										<select name="user_id" id="user_id" class="form-control selection" data-placeholder="Learner List *" required="required">
											<option value="" selected="selected" disabled="disabled" hidden>Select Learner</option>
										</select>
									</div>
								</div>
								<div class="col-sm-3">
									<div class="form-group form-select">
										<select name="attempt" id="attempt" class="form-control selection" data-placeholder="Sim Attempted List *" required="required">
											<option value="" selected="selected" disabled="disabled" hidden>Select Attempted</option>
										</select>
									</div>
								</div>
							</div>
						</div>
					</form>
					<div class="row" id="attention_report" style="display: none;">
						<div class="col-md-12">
							<div class="item">
								<div class="flextlistitem SIMulationT1">
									<div class="textFlex CAtextflex2">
										<div class="col-sm-3">
											<div class="Tright" id="frames">0</div>
											<div class="A16F">FRAMES</div>
										</div>
										<div class="col-sm-3">
											<div class="Tright" id="outoffocus">0</div>
											<div class="A16F">OUTOFFOCUS</div>
										</div>
										<div class="col-sm-3">
											<div class="Tright" id="report">0</div>
											<div class="A16F">REPORT</div>
										</div>
										<div class="col-sm-3">
											<div class="Tright" id="sleep">0</div>
											<div class="A16F">SLEEP</div>
										</div>
										<div class="col-sm-3">
											<div class="Tright" id="status">0</div>
											<div class="A16F">STATUS</div>
										</div>
										<div class="col-sm-3">
											<div class="Tright" id="std">0</div>
											<div class="A16F">STANDARD DEVIATION (MOVEMENT)</div>
										</div>
										<div class="col-sm-3">
											<div class="Tright" id="video"></div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div id="attention_chart" style="height: 350px; width: 100%;"></div>
				</div>
			</div>
		</div>
	</div>
</div>
<style type="text/css">
	@media screen and (-ms-high-contrast: active),
	(-ms-high-contrast: none) {
		p.circleChart_text {
			left: 350%;
		}
	}

	p.circleChart_text {
		font-size: 25px !important;
	}

	.comp-incomp-topbanner {
		background: #fff;
		min-height: 429px;
		border-radius: 0px;
		margin-bottom: 20px;
		padding: 20px;
		border: 1px solid #e2e2e2;
		position: relative;
	}

	.lernar-dashboard-boxes {
		padding: 10px;
	}

	.lernar-dashboard-boxes span {
		display: table-cell;
		font-size: 40px;
		vertical-align: middle;
	}

	.lernar-dashboard-boxes p {
		padding-left: 10px;
	}

	@media(max-width: 1450px) and (min-width: 1024px) {
		.lernar-dashboard-boxes p {
			padding-right: 0px;
		}
	}

	.wordwrap {
		white-space: pre-wrap;
		white-space: -moz-pre-wrap;
		white-space: -pre-wrap;
		white-space: -o-pre-wrap;
		word-wrap: break-word;
	}

	canvas.canvasjs-chart-canvas {
		padding-top: 0px;
	}
</style>
<script type="text/javascript" language="javascript" src="content/js/canvasjs.min.js"></script>
<script type="text/javascript" language="javascript" src="content/js/proctor-api.js"></script>
<script type="text/javascript">
	$('#category').on('change', function() {
		var cid = $(this).val();
		if (cid != '')
			$("#report_form").LoadingOverlay("show");
			$('#attention_report,#attention_chart').hide();
			$('#sim,#group,#user_id,#attempt').empty();
			$.getJSON('includes/ajax.php?getScenarioUpdate=true&catid=' + cid, function(res) {
				$("#report_form").LoadingOverlay("hide", true);
				if (res.success == true) {
					$('#sim').append('<option value="" selected="selected" disabled="disabled" hidden>Select Simulation</option>');
					$('#user_id').append('<option value="" selected="selected" disabled="disabled" hidden>Select Learner</option>');
					$('#group').append('<option value="" selected="selected" disabled="disabled" hidden>Select Group</option>');
					$('#attempt').append('<option value="" selected="selected" disabled="disabled" hidden>Select Attempted</option>');
					$.each(res.data, function(key, val) {
						var option = '<option value="' + val.value + '">' + val.label + '</option>';
						$('#sim').append(option);
					});
				} else if (res.success == false) {
					$('#sim').empty().append('<option value="0" selected="selected" disabled="disabled" hidden>Simulation not found</option>');
				}
			});
	});

	$('#sim').on('change', function() {
		var sid = $(this).val();
		if (sid != '')
			$("#report_form").LoadingOverlay("show");
			$('#attention_report,#attention_chart').hide();
			$('#group,#user_id,#attempt').empty();
			$.getJSON('includes/ajax.php?getLearner=true&sim_id=' + sid, function(res) {
				$("#report_form").LoadingOverlay("hide", true);
				if (res.success == true && res.group == true) {
					$('#group').append('<option value="" selected="selected" disabled="disabled" hidden>Select Group</option>');
					$('#user_id').append('<option value="" selected="selected" disabled="disabled" hidden>Select Learner</option>');
					$('#attempt').append('<option value="" selected="selected" disabled="disabled" hidden>Select Attempted</option>');
					$.each(res.groupData, function(key, val) {
						var option = '<option value="' + val.value + '">' + val.label + '</option>';
						$('#group').append(option);
					});
				} else if (res.success == false) {
					$('#group').empty().append('<option value="0" selected="selected" disabled="disabled" hidden>Group not found</option>');
				}
			});
	});

	$('#group').on('change', function() {
		var uid = $(this).val();
		if (uid != '')
			$("#report_form").LoadingOverlay("show");
			$('#attention_report,#attention_chart').hide();
			$('#user_id,#attempt').empty();
			$.getJSON('includes/ajax.php?getUserData=true&uid=' + uid, function(res) {
				$("#report_form").LoadingOverlay("hide", true);
				if (res.success == true) {
					$('#user_id').append('<option value="" selected="selected" disabled="disabled" hidden>Select Learner</option>');
					$('#attempt').append('<option value="" selected="selected" disabled="disabled" hidden>Select Attempted</option>');
					$.each(res.data, function(key, val) {
						var option = '<option value="' + val.value + '" title="' + val.title + '">' + val.label + '</option>';
						$('#user_id').append(option);
					});
				} else if (res.success == false) {
					$('#user_id').empty().append('<option value="0" selected="selected" disabled="disabled" hidden>Learner not found</option>');
				}
			});
	});

	$('#user_id').on('change', function() {
		var sid = $('#sim option:selected').val();
		var uid = $(this).val();
		if (sid != '' && uid)
			$("#report_form").LoadingOverlay("show");
			$('#attention_report,#attention_chart').hide();
			$('#attempt').empty();
			$.getJSON('includes/ajax.php?getUserSimAttempts=true&uid=' + uid + '&sid=' + sid, function(res) {
				$("#report_form").LoadingOverlay("hide", true);
				if (res.success == true) {
					$('#attempt').append('<option value="" selected="selected" disabled="disabled" hidden>Select Attempted</option>');
					$.each(res.data, function(key, val) {
						var option = '<option value="' + val.userid + '-' + val.uid + '" title="' + val.sim_title + '">' + val.atdate + '</option>';
						$('#attempt').append(option);
					});
				} else if (res.success == false) {
					$('#attempt').empty().append('<option value="0" selected="selected" disabled="disabled" hidden>Attempted not found</option>');
				}
			});
	});

	$('#attempt').on('change', function(e) {
		var aid = $(this).val() + '.webm';
		if (aid != '') {
			$('#attention_report,#attention_chart').hide();
			$.LoadingOverlay("show");
			attention_analysis_report(aid);
		}
	});
</script>
<?php
require_once 'includes/footer.php';
