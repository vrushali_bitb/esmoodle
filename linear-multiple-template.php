<?php 
session_start();
if (isset($_SESSION['username'])) {
    $user   = $_SESSION['username'];
    $role   = $_SESSION['role'];
    $userid = $_SESSION['userId'];
	$domain = (isset($_SESSION['domain_name'])) ? $_SESSION['domain_name'] : $_SERVER['HTTP_HOST'];
}
else {
    header('location: index.php');
}
require_once 'config/db.class.php';
require_once 'includes/learners_menu.php';
$db 		= new DBConnection();
$sim_id		= (isset($_GET['sim_id']) && ! empty($_GET['sim_id'])) ? $_GET['sim_id'] : '';
$ques_id	= (isset($_GET['ques_id'])) ? base64_decode($_GET['ques_id']) : '';
$sim_data 	= $db->getScenario($sim_id);
$sim_com 	= $db->getScenarioCompetency($sim_id);
$sim_page 	= $db->getSimPage($sim_data['scenario_id']);
$sim_brand 	= $db->getSimBranding($sim_data['scenario_id']);
$sim_type	= $db->getMultiScenarioType($sim_data['author_scenario_type']);
$path		= $db->getBaseUrl('scenario/upload/'.$domain.'/');
$uploadpath	= '../scenario/upload/'.$domain.'/';
$root_path	= $db->rootPath() . 'scenario/upload/'.$domain.'/';
$prelink 	= 'launch-sim-linear-multiple.php?preview=true&save=false&sim_id='. $sim_id;
$preview    = ( ! empty($ques_id)) ? $prelink . '&ques_id='. base64_encode($ques_id) : $prelink;
$type_name	= $sim_type[0]['type_name'];
$type 		= strtolower($type_name);
$gettype 	= '';
if ($type == 'image and text'):
	$gettype = 'image';
else:
	$gettype = $type;
endif;
$upload_file_name = '';
if ( ! empty($sim_data['image_url']) && file_exists($root_path . $sim_data['image_url'])):
	$upload_file_name = $sim_data['image_url'];
elseif ( ! empty($sim_data['video_url']) && file_exists($root_path . $sim_data['video_url'])):
	$upload_file_name = $sim_data['video_url'];
elseif ( ! empty($sim_data['audio_url']) && file_exists($root_path . $sim_data['audio_url'])):
	$upload_file_name = $sim_data['audio_url'];
elseif ( ! empty($sim_data['web_object_file'])):
	$upload_file_name = $sim_data['web_object_file'];
endif;
$web_object = '';
if ( ! empty($sim_data['web_object'])):
	$web_object = $sim_data['web_object'];
endif;
$ques_val1 = $ques_val2 = $ques_val3 = $ques_val4 = $ques_val5 = $ques_val6 = 0;
if ( ! empty($ques_id)):
	$qsql = "SELECT * FROM question_tbl WHERE question_id = '". $ques_id ."'";
	try {
		$qstmt	 = $db->prepare($qsql); $qstmt->execute();
		$qrow	 = $qstmt->rowCount();
		$qresult = ($qrow > 0) ? $qstmt->fetch() : [];
		if ( ! empty($qresult)):
			try {
				# Get FeedBack
				$feedsql  = 'SELECT * FROM feedback_tbl WHERE question_id = :question_id';
				$feedstmt = $db->prepare($feedsql);
				$feedstmt->bindParam(':question_id', $qresult['question_id'], PDO::PARAM_INT);
				$feedstmt->execute();
				$feedResult = ($feedstmt->rowCount() > 0) ? $feedstmt->fetchAll(PDO::FETCH_ASSOC) : [];

			} catch(PDOException $e) {
				echo "Oops. something went wrong. ".$e->getMessage(); exit;
			}
		endif;
	}
	catch(PDOException $e) {
		echo "Oops. something went wrong. ".$e->getMessage(); exit;
	}
	$tqsql  = "SELECT * FROM question_tbl WHERE MD5(scenario_id) = '". $sim_id ."'";
	$tqstmt = $db->prepare($tqsql); $tqstmt->execute();
	$tqresult = $tqstmt->fetchAll();
	foreach ($tqresult as $tqueSocre):
		if ( ! empty($tqueSocre['ques_val_1'])) $ques_val1 += $tqueSocre['ques_val_1'];
		if ( ! empty($tqueSocre['ques_val_2'])) $ques_val2 += $tqueSocre['ques_val_2'];
		if ( ! empty($tqueSocre['ques_val_3'])) $ques_val3 += $tqueSocre['ques_val_3'];
		if ( ! empty($tqueSocre['ques_val_4'])) $ques_val4 += $tqueSocre['ques_val_4'];
		if ( ! empty($tqueSocre['ques_val_5'])) $ques_val5 += $tqueSocre['ques_val_5'];
		if ( ! empty($tqueSocre['ques_val_6'])) $ques_val6 += $tqueSocre['ques_val_6'];
	endforeach;
endif; ?>
<link rel="stylesheet" type="text/css" href="content/css/owlcarousel/owl.carousel.min.css" />
<link rel="stylesheet" type="text/css" href="content/css/qustemplate.css" />
<link rel="stylesheet" type="text/css" href="content/css/questionresponsive.css" />
<link rel="stylesheet" type="text/css" href="content/css/template-updated.css" />
<link rel="stylesheet" type="text/css" href="content/css/jquery.fontselect.css">
<script type="text/javascript" src="content/js/inputmask/jquery.inputmask.js"></script>
<script type="text/javascript" src="content/js/spectrum/spectrum.min.js"></script>
<link rel="stylesheet" type="text/css" href="content/js/spectrum/spectrum.min.css" />
<link rel="stylesheet" type="text/css" href="content/fancybox/jquery.fancybox.min.css" />
<script type="text/javascript" src="content/fancybox/jquery.fancybox.min.js"></script>
<script type="text/javascript">
$.LoadingOverlay("show");
</script>
<style type="text/css">
	body {
		overflow-x:hidden;
		background:#efefef;
	}
	[data-title] {
		font-size: 16px;
		position: relative;
		cursor: help;
	}  
	[data-title]:hover::before {
		content: attr(data-title);
		position: absolute;
		bottom: -20px;
		padding: 0px;
		color: #2c3545;
		font-size: 14px;
		z-index: 99;
		white-space: nowrap;
	}
	span.truncateTxt {
		width: 30px;
		white-space: nowrap;
		overflow: hidden;
		text-overflow: ellipsis;
		float: left;
	}
	:required  {
		background: url('img/list/star.png') no-repeat !important;
		background-position:right top !important;
		background-color: #fff !important;
	}
	.required  {
		background: url('img/list/star.png') no-repeat !important;
		background-position:right top !important;
		background-color: #fff !important;
	}
</style>
<form method="post" class="form-inline" name="add_sim_linear_multiple_details_form" id="add_sim_linear_multiple_details_form">
	<input type="hidden" name="update_linear_sim" value="1" />
    <input type="hidden" name="sim_id" id="sim_id" value="<?php echo $sim_data['scenario_id'] ?>" />
    <input type="hidden" name="gettype" value="<?php echo $gettype ?>" />
    <input type="hidden" name="character_type" id="character_type" value="" />
    <input type="hidden" name="submit_type" id="submit_type" />
    <input type="hidden" name="ques_id" id="ques_id" value="<?php echo $ques_id; ?>" />
    <input type="hidden" name="get_question_type" id="get_question_type" value="<?php echo ( ! empty($qresult['question_type'])) ? $qresult['question_type'] : '' ?>" />
    <input type="hidden" name="question_type" id="question_type" value="<?php echo ( ! empty($qresult['question_type'])) ? $qresult['question_type'] : '' ?>" />
    <input type="hidden" name="location" id="location" value="<?php echo $db->getBaseUrl($db->getFile().'?add_data=true&sim_id='. $sim_id); ?>" />
    <div class="sim-side-menu">
        <ul class="side-menu-list nav nav-tabs">
			<li class="Add_Web <?php echo (empty($ques_id)) ? 'active' : ''; ?>"><a data-toggle="tab" href="#Add_Web"><div class="tooltipmenu"><img class="svg addScenario" src="img/list/Add_Learning.svg"/><span class="tooltiptext">Add Learning</span></div></a></li>
			<li class="Add_Scenario"><a data-toggle="tab" href="#Add_Scenario"><div class="tooltipmenu"><img class="svg addScenario" src="img/list/add_Scenario.svg"/><span class="tooltiptext">Add Scenario</span></div></a></li>
            <li class="image_menu"><a data-toggle="tab" href="#image_menu" onclick="openNav1()"><div class="tooltipmenu"><img class="svg add_bg_w" src="img/list/add_background.svg"/><span class="tooltiptext">Add Backgrounds</span></div></a></li>
            <li class="char_menu"><a data-toggle="tab" href="#char_menu" onclick="openNav2()"><div class="tooltipmenu"><img class="svg add_char_w" src="img/list/ad_character.svg"/><span class="tooltiptext">Add Character</span></div></a></li>
            <li class="Add_Branding"><a data-toggle="tab" href="#Add_Branding"><div class="tooltipmenu"><img class="detail_w svg" src="img/list/add_branding.svg"/><span class="tooltiptext">Add Branding</span></div></a></li>
			<li class="main_menu"><a data-toggle="tab" href="#main_menu"><div class="tooltipmenu"><img class="svg add_qa" src="img/list/add_detail.svg"/><span class="tooltiptext">Simulation Details</span></div></a></li>
			<li class="QUeS_menu main_menu <?php echo ( ! empty($ques_id)) ? 'active' : ''; ?>"><a data-toggle="tab" href="#questiontamp"><div class="tooltipmenu"><img class="svg add_qa" src="img/list/add_question1.svg"/><span class="tooltiptext">Question Template</span></div></a></li>
        </ul>
    </div>
    <div class="question_tree_banner tab-content clearfix">
		<!--Add-Web-Object-->
		<div class="bg_selection clearfix tab-pane fade <?php echo (empty($ques_id)) ? 'in active' : ''; ?>" id="Add_Web">
			<div class="question_tree_menu NoBorder" id="question_tree_menu1">
				<div class="Simulation_det stopb clearfix">
					<h3>Add Learning</h3>
					<div class="previewQus Sbtn">
						<a href="<?php echo $preview ?>" target="_blank" title="Preview"><img class="img-fluid" src="img/qus_icon/preview.svg"></a>
					</div>
			    </div>
            	<div class="Group_managment">
					<div class="container">
						<div class="row">
							<div class="col-sm-12">
								<div class="usermanage_main">
									<div class="usermanage-form">
										<div class="row">
											<div class="tille">
												<div class="add_page_option appendSCE">
													<div class="form-group">
														<input type="text" name="web_object_title" class="form-control" placeholder="Title name" autocomplete="off" value="<?php echo ( ! empty($sim_data['web_object_title'])) ? $sim_data['web_object_title'] : ''; ?>" />
													</div>
												</div>
											</div>
											<div class="tilleobj">
												<div class="addsce-img">
													<div class="adimgbox">
														<div class="form-group">
															<input type="file" name="web_file" id="web_file" class="form-control file" />
															<input type="text" class="form-control controls" disabled placeholder="Select Web Object file" value="<?php echo $web_object; ?>" />
															<input type="hidden" name="web_object" id="web_object" value="<?php echo $web_object; ?>" />
															<input type="hidden" name="web_object_type" id="web_object_type" value="application" />
															<div class="browsebtn browsebtntmp">
																<span id="WbDelete" <?php echo ( ! empty($web_object)) ? '' : 'style="display:none"'; ?>>
																	<a href="javascript:void(0);" data-wb="<?php echo ( ! empty($web_object)) ? $web_object : ''; ?>" class="delete_wb" title="Delete File"><i class="fa fa-times" aria-hidden="true"></i></a>
																</span>
															</div>
														</div>
													</div>
													<div class="adimgbox browsebtn"><button class="browse btn btn-primary" type="button">Browse</button></div>
													<div class="adimgbox browsebtn"><button type="button" name="upload_web" id="upload_web" class="btn btn-primary">Upload</button></div>
												</div>
											</div>
                                         </div>
									</div>
								</div>
							</div>
						</div>
					</div>
                </div>
            </div>
        </div>
		<!--Add-Simulation-->
        <div class="bg_selection clearfix tab-pane fade" id="Add_Scenario">
            <div class="question_tree_menu NoBorder" id="question_tree_menu1">
				<div class="Simulation_det stopb clearfix">
					<h3>Add Scenario</h3>
					<div class="previewQus Sbtn">
						<a href="<?php echo $preview ?>" target="_blank" title="Preview"><img class="img-fluid" src="img/qus_icon/preview.svg"></a>
					</div>
			    </div>
            	<div class="Group_managment">
					<div class="container">
						<div class="row">
							<div class="col-sm-12">
								<div class="usermanage_main">
									<div class="usermanage-form">
										<div class="row">
                                        	<?php if ($gettype != 'text only' && $gettype != 'web object'): ?>
                                            <div class="addsce-img">
                                            	<div class="adimgbox">
                                                    <div class="form-group">
                                                        <input type="file" name="file" id="file" class="form-control file">
                                                        <input type="text" id="scenario_media" class="form-control controls" disabled placeholder="Select <?php echo $gettype; ?> file" value="<?php echo $upload_file_name; ?>">
                                                        <input type="hidden" name="scenario_media_file" id="scenario_media_file" value="<?php echo $upload_file_name; ?>" />
                                                        <input type="hidden" name="scenario_media_file_type" id="scenario_media_file_type" value="<?php echo $gettype; ?>" />
														<div class="browsebtn browsebtntmp">
															<span id="splashImg"><?php if ( ! empty($upload_file_name)) : ?><a href="<?php echo $uploadpath . $upload_file_name ?>" target="_blank" title="View Scenario Media File"><i class="fa fa-eye" aria-hidden="true"></i></a><?php endif; ?></span>
															<span id="ImgDelete" <?php echo ( ! empty($upload_file_name)) ? '' : 'style="display:none"'; ?>><a href="javascript:void(0);" data-scenario-media-file="<?php echo ( ! empty($upload_file_name)) ? $upload_file_name : ''; ?>" class="delete_scenario_media_file" title="Delete File"><i class="fa fa-times" aria-hidden="true"></i></a></span>
														</div>
													</div>
                                                </div>
												<div class="adimgbox browsebtn"><button class="browse btn btn-primary" type="button">Browse</button></div>
                                                <div class="adimgbox browsebtn"><button type="button" name="upload" id="upload" class="btn btn-primary">Upload</button></div>
                                            </div>
											<?php elseif ($gettype == 'web object'): ?>
											<div class="addsce-img">
												<div class="adimgbox">
													<div class="form-group">
                                                        <input type="file" name="file" id="file" class="form-control file" />
                                                        <input type="text" class="form-control controls" disabled placeholder="Select <?php echo $gettype; ?> file" value="<?php echo $upload_file_name; ?>" />
                                                        <input type="hidden" name="scenario_media_file" id="scenario_media_file" value="<?php echo $upload_file_name; ?>" />
                                                        <input type="hidden" name="scenario_media_file_type" id="scenario_media_file_type" value="application" />
														<div class="browsebtn browsebtntmp"><span id="ImgDelete" <?php echo ( ! empty($upload_file_name)) ? '' : 'style="display:none"'; ?>><a href="javascript:void(0);" data-wb-file="<?php echo ( ! empty($upload_file_name)) ? $upload_file_name : ''; ?>" class="delete_wb_file" title="Delete File"><i class="fa fa-times" aria-hidden="true"></i></a></span></div>
                                                    </div>
                                                </div>
												<div class="adimgbox browsebtn"><button class="browse btn btn-primary" type="button">Browse</button></div>
                                                <div class="adimgbox browsebtn"><button type="button" name="upload" id="upload" class="btn btn-primary">Upload</button></div>
                                            </div>
                                            <?php endif; ?>
											<div class="user_text addtext">Add Tabs</div>
											<div class="col-sm-12">
												<?php if ( ! empty($sim_page)):
												$pagei = 1; foreach ($sim_page as $pdata): ?>
                                                <div class="plus_page_option">
                                                    <div class="add_page_option appendSCE">
                                                        <div class="form-group">
                                                            <input type="text" name="sim_page_name[]" class="form-control" placeholder="Page name" required="required" value="<?php echo $pdata['sim_page_name'] ?>" />
                                                        </div>
                                                        <div class="form-group">
                                                            <textarea name="sim_page_desc[]" id="sim_page_desc_<?php echo $pagei ?>" class="form-control sim_page_desc"><?php echo $pdata['sim_page_desc'] ?></textarea>
                                                        </div>
                                                        <?php if ($pagei == 1): ?>
                                                         <div class="plus_page"><img src="img/icons/plus.png" title="Add More Page" /></div>
                                                         <?php else: ?>
                                                         <div class="remove minusComp"><img src="img/icons/minus.png" title="Remove Page"></div>
                                                         <?php endif; ?>
                                                    </div>                                               
                                                </div>
                                                <?php $pagei++; endforeach; else: ?>
                                            	<div class="plus_page_option">
                                                    <div class="add_page_option appendSCE">
                                                        <div class="form-group">
                                                            <input type="text" name="sim_page_name[]" class="form-control required" placeholder="Page name" />
                                                        </div>
                                                        <div class="form-group">
                                                            <textarea name="sim_page_desc[]" id="sim_page_desc_0" class="form-control"></textarea>
                                                        </div>
                                                         <div class="plus_page"><img src="img/icons/plus.png" title="Add More Page" /></div>
                                                    </div>                                               
                                                </div>
                                                <?php endif; ?>
											</div>
                                         </div>
									</div>
								</div>
							</div>
						</div>
					</div>
                </div>
            </div>
        </div>
		<!--Add-Backgrounds-->
    	<div class="bg_selection clearfix tab-pane fade" id="image_menu">
            <div class="question_tree_menu NoBorder" id="question_tree_menu1">
				<div class="Simulation_det stopb clearfix">
					<h3>Add Backgrounds</h3>
					<div class="previewQus Sbtn">
						<a href="<?php echo $preview ?>" target="_blank" title="Preview"><img class="img-fluid" src="img/qus_icon/preview.svg"></a>
					</div>
			    </div>
				<div class="Group_managment Adddbackgroundbg">
				<div class="row back-margin">
					<div class="Radio-box MCQRadio">
                    	<label class="radiostyle">
                        	<input type="radio" name="bgoption" value="1" <?php echo ( ! empty($sim_data['bg_color'])) ? 'checked="checked"' : '' ?> />
                            <span class="radiomark white"></span>
                            <div class="user_text">Add background color</div>
                       </label>
                    </div>
					<div class="branding-flex colorbgbrand">
                    	<span class="colorb">Color</span>
                        <input class="type-color-on-page" name="bg_color" id="bg_color" />
                    </div>
				</div>
				<div class="row back-margin">
                    <div class="Radio-box MCQRadio">
                        <label class="radiostyle">
                        	<input type="radio" name="bgoption" value="2" <?php echo ( ! empty($sim_data['bg_own_choice'])) ? 'checked="checked"' : '' ?> />
                        	<span class="radiomark white"></span>
                        	<div class="user_text">Add background of your own choice</div>
                        </label>
                    </div>
                    <div class="addsce-img bgsi">
                        <div class="Scen-preview bgsi"><img class="sce-img own-bg-prev" src="<?php echo ( ! empty($sim_data['bg_own_choice'])) ? $uploadpath . $sim_data['bg_own_choice'] : 'img/scenario-img.png' ?>" /></div>
                        <div class="imgpath bgbtn">
                            <div class="form-group">
                                <input type="file" name="file" id="bg_file" class="form-control file">
                                <input type="text" class="form-control controls" value="<?php echo ( ! empty($sim_data['bg_own_choice'])) ? $sim_data['bg_own_choice'] : '' ?>" disabled placeholder="Select File" />
                                <input type="hidden" name="scenario_bg_file" id="scenario_bg_file" value="<?php echo ( ! empty($sim_data['bg_own_choice'])) ? $sim_data['bg_own_choice'] : '' ?>" />
                             </div>
                        </div>
                        <div class="browsebtn bgbtn">
                        	<a href="javascript:void(0);" style=" <?php echo ( ! empty($sim_data['bg_own_choice'])) ? '' : 'display:none' ?> " data-scenario-bg-file="<?php echo ( ! empty($sim_data['bg_own_choice'])) ? $sim_data['bg_own_choice'] : '' ?>" class="delete_scenario_bg_file" title="Delete File"><i class="fa fa-times" aria-hidden="true"></i></a>
                        </div>
                        <div class="browsebtn bgbtn"><button class="browse btn btn-primary" type="button">Browse & Select</button></div>
                        <div class="browsebtn bgbtn"><button type="button" name="upload" id="uploadBG" class="btn btn-primary">Upload</button></div>
                    </div>
				</div>
				<div class="row back-margin">
                    <div class="Radio-box MCQRadio">
                        <label class="radiostyle">
                        <input type="radio" name="bgoption" value="3" <?php echo ( ! empty($sim_data['sim_back_img'])) ? 'checked="checked"' : '' ?> />
                            <span class="radiomark white"></span>
                            <div class="user_text">Add background Library</div>
                        </label>
                   </div>
				</div>
                <input type="hidden" id="get_bg_img" value="<?php echo $sim_data['sim_back_img'] ?>" />
                <div class="checkbox bluecheckbox BGIMG"></div>
               </div>
			</div>
        </div>
        <!--Add-Character-->
        <div class="char_bg_selection clearfix tab-pane fade" id="char_menu">
            <div class="question_tree_menu NoBorder" id="question_tree_menu2">
				<div class="Simulation_det stopb clearfix">
					<h3>Add Character</h3>
					<div class="previewQus Sbtn">
						<a href="<?php echo $preview ?>" target="_blank" title="Preview"><img class="img-fluid" src="img/qus_icon/preview.svg"></a>
					</div>
			    </div>	
				<div class="Group_managment">				
                    <ul class="nav-char-tab mb-3">
                        <li class="checkbox radiobtn">
                            <div class="Radio-box">
                            <label class="radiostyle">
                            <input type="radio" class="NoCharTab" name="radio-group" value="1" />
                            <span class="radiomark SelC"></span></label>
                            <label for="test1">No Character</label></div>
                        </li>
                        <li class="checkbox radiobtn">
                            <div class="Radio-box">
                            <label class="radiostyle">
                            <input type="radio" class="OneCharTab" name="radio-group" value="2" <?php echo ( ! empty($sim_data['sim_char_img']) || ! empty($sim_data['char_own_choice'])) ? 'checked="checked"' : ''; ?> />
                            <span class="radiomark SelC"></span></label>
                            <label for="test2">One Character</label></div>
                        </li>
                        <li class="checkbox radiobtn">
                            <div class="Radio-box">
                            	<label class="radiostyle">
                            	<input type="radio" class="TwoCharTab" name="radio-group" <?php echo ( ! empty($sim_data['sim_char_left_img']) || ! empty($sim_data['sim_char_right_img'])) ? 'checked="checked"' : ''; ?> /><span class="radiomark SelC"></span></label>
                            	<label for="test3">Two Characters</label>
                            </div>
                        </li>
                    </ul>
                    <div class="row back-margin onec">
                        <div class="Radio-box MCQRadio">
                        	<label class="radiostyle">
                            	<input type="radio" name="charoption" value="1" <?php echo ( ! empty($sim_data['char_own_choice'])) ? 'checked="checked"' : ''; ?>>
                                <span class="radiomark white"></span>
                                <div class="user_text">Add your own character</div>
                           	</label>
                        </div>
                        <div class="addsce-img charboxm">
                            <div class="Scen-preview"><div class="Scen-preview bgsi"><img class="sce-imgchar" src="<?php echo ( ! empty($sim_data['char_own_choice'])) ? $uploadpath . $sim_data['char_own_choice'] : 'img/charbg.png' ?>" /></div></div>
                            <div class="imgpath charbtn">
                                <div class="form-group">
                                    <input type="file" name="file" id="file_char" class="form-control file">
                                    <input type="text" class="form-control controls" value="<?php echo ( ! empty($sim_data['char_own_choice'])) ? $sim_data['char_own_choice'] : '' ?>" disabled placeholder="Select File" />
                                    <input type="hidden" name="scenario_char_file" id="scenario_char_file" value="<?php echo ( ! empty($sim_data['char_own_choice'])) ? $sim_data['char_own_choice'] : '' ?>" />
                                </div>
                            </div>
                            <div class="browsebtn charbtn">
                                <a href="javascript:void(0);" style=" <?php echo ( ! empty($sim_data['char_own_choice'])) ? '' : 'display:none' ?> " data-scenario-char-file="<?php echo ( ! empty($sim_data['char_own_choice'])) ? $sim_data['char_own_choice'] : '' ?>" class="delete_scenario_char_file" title="Delete File"><i class="fa fa-times" aria-hidden="true"></i></a>
                            </div>
                            <div class="browsebtn charbtn"><button class="browse btn btn-primary" type="button">Browse & Select</button></div>
                            <div class="browsebtn charbtn"><button type="button" name="upload" id="uploadChar" class="btn btn-primary">Upload</button></div>
                        </div>
                    </div>
                    <div class="tab-content">
                        <div class="row back-margin onec">
                            <div class="Radio-box MCQRadio">
                            	<label class="radiostyle">
                                	<input type="radio" name="charoption" value="2" <?php echo ( ! empty($sim_data['sim_char_img'])) ? 'checked="checked"' : ''; ?> <?php echo ( ! empty($sim_data['sim_char_left_img']) || ! empty($sim_data['sim_char_right_img'])) ? 'checked="checked"' : ''; ?> />
                                    <span class="radiomark white"></span>
                                    <div class="user_text">Add character form the Library</div>
                               	</label>
                            </div>
                        </div>
                        <div class="OneChar">
                            <input type="hidden" id="get_sim_char_img" value="<?php echo $sim_data['sim_char_img'] ?>"/>
                            <div class="checkbox radiobtn bluecheckbox OneCharIMG"></div>
                        </div>
                        <div class="TwoChar">
                            <input type="hidden" id="sim_char_left_img" value="<?php echo $sim_data['sim_char_left_img'] ?>"/>
                            <input type="hidden" id="sim_char_right_img" value="<?php echo $sim_data['sim_char_right_img'] ?>"/>
                            <div class="checkbox bluecheckbox TwoCharIMG"></div>
                        </div>
                    </div>
            	</div>
			</div>
        </div>
        <!--Add-Branding-->
		<div class="main tab-pane fade" id="Add_Branding">
            <div class="main_sim question_tree_menu">
                <div class="main_sim_scroll">
					<div class="Simulation_det stopb clearfix">
						<h3>Adding Branding</h3>
						<div class="previewQus Sbtn">
							<a href="<?php echo $preview ?>" target="_blank" title="Preview"><img class="img-fluid" src="img/qus_icon/preview.svg"></a>
						</div>
					</div>
					<div class="Group_managment">
						<div class="container">
                        	<div class="branding-color">
								<div class="branding-flex brandingbox">
									<div class="coloredittool-box">
										<div class="coloredittool">
											<div class="user_text colorp">Question Background</div>
											<div class="branding-flex"><span class="colorb">Color</span><input class="type-color-on-page" name="ques_bg_color" id="ques_bg_color" /></div>
										</div>
										<div class="coloredittool">
											<div class="user_text colorp">Option Background</div>
											<div class="branding-flex"><span class="colorb">Color</span><input class="type-color-on-page" name="ques_option_bg_color" id="ques_option_bg_color" /></div>
										</div>
										<div class="coloredittool">
											<div class="user_text colorp">Option Hover</div>
											<div class="branding-flex"><span class="colorb">Color</span><input class="type-color-on-page" name="ques_option_hover_color" id="ques_option_hover_color" /></div>
										</div>
										<div class="coloredittool">
											<div class="user_text colorp">Option Selected</div>
											<div class="branding-flex"><span class="colorb">Color</span><input class="type-color-on-page" name="ques_option_select_color" id="ques_option_select_color" /></div>
										</div>
									</div>
									<div class="coloredittool-box">
										<div class="coloredittool">
											<div class="user_text colorp">Submit button Background</div>
											<div class="branding-flex"><span class="colorb">Color</span><input class="type-color-on-page" name="btn_bg_color" id="btn_bg_color" /></div>
										</div>
										<div class="coloredittool">
											<div class="user_text colorp">Submit button Hover</div>
											<div class="branding-flex"><span class="colorb">Color</span><input class="type-color-on-page" name="btn_hover_color" id="btn_hover_color" /></div>
										</div>
										<div class="coloredittool">
											<div class="user_text colorp">Submit button Selected</div>
											<div class="branding-flex"><span class="colorb">Color</span><input class="type-color-on-page" name="btn_select_color" id="btn_select_color" /></div>
										</div>
									</div>
								</div>
							</div>
							<div class="user_text fonts">Font</div>
							<div class="branding-font">
								<div class="branding-flex">
                                	<span class="colorb">Font Type</span>
									<input id="font_type" name="font_type" type="text" />
									<?php if  ( ! empty($sim_brand['font_type'])) : ?>
									<div class="col-sm-2"><button class="cl_ftype btn btn-primary" type="button">Set Default Font Type</button></div>
									<?php endif; ?>
								</div>
								<div class="branding-flex">
                                	<span class="colorb">Font Color</span>
                                    <input class="type-color-on-page" name="font_color" id="font_color" />
                                </div>
                                <div class="branding-flex"><span class="colorb">Font Size</span>
                                  <select name="font_size" id="font_size" class="form-control">
                                  	<option selected="selected" value="0">Select font size</option>
                                  	<option value="10px" <?php echo ( ! empty($sim_brand['font_size']) && $sim_brand['font_size'] == '10px') ? 'selected="selected"' : '' ?>>10px</option>
                                    <option value="12px" <?php echo ( ! empty($sim_brand['font_size']) && $sim_brand['font_size'] == '12px') ? 'selected="selected"' : '' ?>>12px</option>
                                    <option value="16px" <?php echo ( ! empty($sim_brand['font_size']) && $sim_brand['font_size'] == '16px') ? 'selected="selected"' : '' ?>>16px</option>
                                    <option value="18px" <?php echo ( ! empty($sim_brand['font_size']) && $sim_brand['font_size'] == '18px') ? 'selected="selected"' : '' ?>>18px</option>
                                    <option value="20px" <?php echo ( ! empty($sim_brand['font_size']) && $sim_brand['font_size'] == '20px') ? 'selected="selected"' : '' ?>>20px</option>
                                    <option value="24px" <?php echo ( ! empty($sim_brand['font_size']) && $sim_brand['font_size'] == '24px') ? 'selected="selected"' : '' ?>>24px</option>
                                    <option value="26px" <?php echo ( ! empty($sim_brand['font_size']) && $sim_brand['font_size'] == '26px') ? 'selected="selected"' : '' ?>>26px</option>
                                  </select>
                              </div>
							</div>
						</div>
					</div>
                </div>
            </div>
        </div>
        <!--Add-Sim-Details-->
        <div class="main tab-pane fade" id="main_menu">
            <div class="main_sim question_tree_menu">
            	<div class="Simulation_det stopb clearfix">
                    <h3>Simulation Details</h3>
                    <div class="previewQus Sbtn">
                        <a href="<?php echo $preview ?>" target="_blank" title="Preview"><img class="img-fluid" src="img/qus_icon/preview.svg"></a>
                    </div>
                </div>
                <div class="Group_managment CreateSIMCOMPTBAN">
								
					<div class="row" style="margin-top:20px">
						<div class="col-sm-12">
							<div class="form-group branching ">
								<label class="branching-text">Simulation Name</label>
								<input type="text" name="sim_title" id="sim_title" placeholder="Simulation Name" value="<?php echo $sim_data['Scenario_title'] ?>" class="form-control required" />
							</div>
							<div class="form-group branching">
								<label class="branching-text">Duration(min)</label>
								<input type="text" class="form-control question-box required" name="sim_duration" id="sim_duration" onkeypress="return isNumberKey(event);" autocomplete="off" value="<?php echo $sim_data['duration'] ?>" />
							</div>
							<div class="form-group branching">
								<label class="branching-text">Passing Score(%)</label>
								<input type="text" class="form-control question-box required" name="passing_marks" id="passing_marks" onkeypress="return isNumberKey(event);" autocomplete="off" value="<?php echo $sim_data['passing_marks'] ?>" />
							</div>
						</div>
					</div>
					<?php $sim_cover_img  = '';
					if ( ! empty($sim_data['sim_cover_img']) && file_exists($root_path . $sim_data['sim_cover_img'])):
						$sim_cover_img = $sim_data['sim_cover_img'];
					endif; ?>	
					<div class="edit-sim-box editSimAUTH">
						<div class="Simulation_det VreACom" style="margin-bottom: 20px;">
							<h3>Add Cover Image</h3>
						</div>
						<div class="addsce-img">
							<div class="imgpath imgpathAUT">
								<div class="form-group">
									<input type="file" name="upload_cover_img_file" id="upload_cover_img_file" class="form-control file" />
									<input type="text" class="form-control controls" disabled placeholder="Select File" />
									<input type="hidden" name="scenario_cover_file" id="scenario_cover_file" value="<?php echo ( ! empty($sim_cover_img)) ? $sim_cover_img : ''; ?>" />
									<input type="hidden" name="cover_img_file_type" id="cover_img_file_type" value="image" />
									
									<div class="sim_cover_img_data"><a class="view_assets" data-src="<?php echo $path . $sim_cover_img; ?>" href="javascript:;"><?php echo $sim_cover_img; ?></a></div>
								</div>
							</div>
							<div class="browsebtn">
								<div class="cover_image_container" <?php echo ( ! empty($sim_cover_img)) ? '' : 'style="display:none;"'; ?>>
									<a href="javascript:void(0);" data-cover-img-file="<?php echo ( ! empty($sim_cover_img)) ? $sim_cover_img : ''; ?>" class="delete_cover_img_file" title="Delete File"><i class="fa fa-times" aria-hidden="true"></i></a>
								</div>
							</div>
							<div class="browsebtn"><button class="browse btn btn-primary upload_cover_btn" type="button" <?php echo ( ! empty($sim_cover_img)) ? 'disabled' : ''; ?>>Browse & Select</button></div>
							<div class="browsebtn"><button type="button" name="upload_cover_img" id="upload_cover_img" class="btn btn-primary" <?php echo ( ! empty($sim_cover_img)) ? 'disabled' : ''; ?>>Upload</button></div>
						</div>
					</div>
					<div class="Simulation_det VreACom" style="margin-top: 20px;">
						<h3>Create Competency</h3>
					</div>
					<div class="row"  style="margin-top:20px">
					<?php if ( ! empty($sim_com)): ?>
						<div class="plus_comp_option linaer">
						<?php $i = 1; if ( ! empty($sim_com['comp_col_1'])): ?>
						<div class="add_comp_option">
							<div class="form-group">
								<input type="text" placeholder="Competency Name" class="form-control" autocomplete="off" name="competency_name_1" value="<?php echo $sim_com['comp_col_1'] ?>" required="required" />
							</div>
							<div class="form-group labelbox">
								<label>Competency Score</label>
								<input type="text" class="form-control question-box" name="competency_score_1" onkeypress="return isNumberKey(event);" value="<?php echo $sim_com['comp_val_1'] ?>" required="required" />
							</div>
							<div class="form-group labelbox">
								<label>Weightage</label>
								<input type="text" class="form-control question-box" name="weightage_1" onkeypress="return isNumberKey(event);" value="<?php echo $sim_com['weightage_1'] ?>" required="required" />
							</div>
							<div class="plus_comp"><img src="img/icons/plus.png" title="Add More"></div>
						</div>
						<?php endif;
						if ( ! empty($sim_com['comp_col_2'])): $i += 1; ?>
						<div class="add_comp_option">
							<div class="form-group">
								<input type="text" placeholder="Competency Name" class="form-control" autocomplete="off" name="competency_name_2" value="<?php echo $sim_com['comp_col_2'] ?>" required="required" />
							</div>
							<div class="form-group labelbox">
								<label>Competency Score</label>
								<input type="text" class="form-control question-box" name="competency_score_2" onkeypress="return isNumberKey(event);" value="<?php echo $sim_com['comp_val_2'] ?>" required="required" />
							</div>
							<div class="form-group labelbox">
								<label>Weightage</label>
								<input type="text" class="form-control question-box" name="weightage_2" onkeypress="return isNumberKey(event);" value="<?php echo $sim_com['weightage_2'] ?>" required="required" />
							</div>
							<div class="remove minusComp"><img src="img/icons/minus.png" title="Remove"></div>
						</div>
						<?php endif;
						if ( ! empty($sim_com['comp_col_3'])): $i += 1; ?>
						<div class="add_comp_option">
							<div class="form-group">
								<input type="text" placeholder="Competency Name" class="form-control" autocomplete="off" name="competency_name_3" value="<?php echo $sim_com['comp_col_3'] ?>" required="required" />
							</div>
							<div class="form-group labelbox">
								<label>Competency Score</label>
								<input type="text" class="form-control question-box" name="competency_score_3" onkeypress="return isNumberKey(event);" value="<?php echo $sim_com['comp_val_3'] ?>" required="required" />
							</div>
							<div class="form-group labelbox">
								<label>Weightage</label>
								<input type="text" class="form-control question-box" name="weightage_3" onkeypress="return isNumberKey(event);" value="<?php echo $sim_com['weightage_3'] ?>" required="required" />
							</div>
							<div class="remove minusComp"><img src="img/icons/minus.png" title="Remove"></div>
						</div>
						<?php endif;
						if ( ! empty($sim_com['comp_col_4'])): $i += 1; ?>
						<div class="add_comp_option">
							<div class="form-group">
								<input type="text" placeholder="Competency Name" class="form-control" autocomplete="off" name="competency_name_4" value="<?php echo $sim_com['comp_col_4'] ?>" required="required" />
							</div>
							<div class="form-group labelbox">
								<label>Competency Score</label>
								<input type="text" class="form-control question-box" name="competency_score_4" onkeypress="return isNumberKey(event);" value="<?php echo $sim_com['comp_val_4'] ?>" required="required" />
							</div>
							<div class="form-group labelbox">
								<label>Weightage</label>
								<input type="text" class="form-control question-box" name="weightage_4" onkeypress="return isNumberKey(event);" value="<?php echo $sim_com['weightage_4'] ?>" required="required" />
							</div>
							<div class="remove minusComp"><img src="img/icons/minus.png" title="Remove"></div>
						</div>
						<?php endif;
						if ( ! empty($sim_com['comp_col_5'])): $i += 1; ?>
						<div class="add_comp_option">
							<div class="form-group">
								<input type="text" placeholder="Competency Name" class="form-control" autocomplete="off" name="competency_name_5" value="<?php echo $sim_com['comp_col_5'] ?>" required="required" />
							</div>
							<div class="form-group labelbox">
								<label>Competency Score</label>
								<input type="text" class="form-control question-box" name="competency_score_5" onkeypress="return isNumberKey(event);" value="<?php echo $sim_com['comp_val_5'] ?>" required="required" />
							</div>
							<div class="form-group labelbox">
								<label>Weightage</label>
								<input type="text" class="form-control question-box" name="weightage_5" onkeypress="return isNumberKey(event);" value="<?php echo $sim_com['weightage_5'] ?>" required="required" />
							</div>
							<div class="remove minusComp"><img src="img/icons/minus.png" title="Remove"></div>
						</div>
						<?php endif;
						if ( ! empty($sim_com['comp_col_6'])): $i += 1; ?>
						<div class="add_comp_option">
							<div class="form-group">
								<input type="text" placeholder="Competency Name" class="form-control" autocomplete="off" name="competency_name_6" value="<?php echo $sim_com['comp_col_6'] ?>" required="required" />
							</div>
							<div class="form-group labelbox">
								<label>Competency Score</label>
								<input type="text" class="form-control question-box" name="competency_score_6" onkeypress="return isNumberKey(event);" value="<?php echo $sim_com['comp_val_6'] ?>" required="required" />
							</div>
							<div class="form-group labelbox">
								<label>Weightage</label>
								<input type="text" class="form-control question-box" name="weightage_6" onkeypress="return isNumberKey(event);" value="<?php echo $sim_com['weightage_6'] ?>" required="required" />
							</div>
							<div class="remove minusComp"><img src="img/icons/minus.png" title="Remove"></div>
						</div>
						<?php endif; ?>
						</div>
						<?php else: ?>
						<div class="plus_comp_option linaer">
							<div class="add_comp_option">
								<div class="form-group">
									<input type="text" placeholder="Competency Name" class="form-control required" autocomplete="off" name="competency_name_1" />
								</div>
								<div class="form-group labelbox">
									<label>Competency Score</label>
									<input type="text" class="form-control question-box required" name="competency_score_1" onkeypress="return isNumberKey(event);" />
								</div>
								<div class="form-group labelbox">
									<label>Weightage</label>
									<input type="text" class="form-control question-box required" name="weightage_1" onkeypress="return isNumberKey(event);" />
								</div>
								<div class="plus_comp"><img src="img/icons/plus.png" title="Add More Competency"></div>
							</div>
						</div>
						<?php endif; ?>
					</div>
            	</div>
           </div>
        </div>
		<!--Question-Temp-->
        <div class="Questio-template ClassicMULtitemp tab-pane fade <?php echo ( ! empty($ques_id)) ? 'in active' : ''; ?>" id="questiontamp">
			<div class="main_sim_scroll">
				<div class="Simulation_det stopb clearfix unsetp">
					<div class="skilQue">
						<?php if ( ! empty($sim_com['comp_col_1']) &&  ! empty($sim_com['comp_val_1'])): ?>
						<div class="skills">
							<label data-title="<?php echo $sim_com['comp_col_1'] ?>"><span class="truncateTxt"><?php echo $sim_com['comp_col_1'] ?></span></label>
							<input type="text" class="form-control" id="comp_val_1" readonly="readonly" value="<?php echo ($ques_val1 <= $sim_com['comp_val_1']) ? abs($sim_com['comp_val_1'] - $ques_val1) : 0; ?>" />
						</div>
						<?php endif; ?>
						
						<?php if ( ! empty($sim_com['comp_col_2']) &&  ! empty($sim_com['comp_val_2'])): ?>
						<div class="skills">
							<label data-title="<?php echo $sim_com['comp_col_2'] ?>"><span class="truncateTxt"><?php echo $sim_com['comp_col_2'] ?></span></label>
							<input type="text" class="form-control" id="comp_val_2" readonly="readonly" value="<?php echo ($ques_val2 <= $sim_com['comp_val_2']) ? abs($sim_com['comp_val_2'] - $ques_val2) : 0; ?>" />
						</div>
						<?php endif; ?>
					
						<?php if ( ! empty($sim_com['comp_col_3']) &&  ! empty($sim_com['comp_val_3'])): ?>
						<div class="skills">
							<label data-title="<?php echo $sim_com['comp_col_3'] ?>"><span class="truncateTxt"><?php echo $sim_com['comp_col_3'] ?></span></label>
							<input type="text" class="form-control" id="comp_val_3" readonly="readonly" value="<?php echo ($ques_val3 <= $sim_com['comp_val_3']) ? abs($sim_com['comp_val_3'] - $ques_val3) : 0; ?>" />
						</div>
						<?php endif; ?>
					
						<?php if ( ! empty($sim_com['comp_col_4']) &&  ! empty($sim_com['comp_val_4'])): ?>
						<div class="skills">
							<label data-title="<?php echo $sim_com['comp_col_4'] ?>"><span class="truncateTxt"><?php echo $sim_com['comp_col_4'] ?></span></label>
							<input type="text" class="form-control" id="comp_val_4" readonly="readonly" value="<?php echo ($ques_val4 <= $sim_com['comp_val_4']) ? abs($sim_com['comp_val_4'] - $ques_val4) : 0; ?>" />
						</div>
						<?php endif; ?>
						
						<?php if ( ! empty($sim_com['comp_col_5']) &&  ! empty($sim_com['comp_val_5'])): ?>
						<div class="skills">
							<label data-title="<?php echo $sim_com['comp_col_5'] ?>"><span class="truncateTxt"><?php echo $sim_com['comp_col_5'] ?></span></label>
							<input type="text" class="form-control" id="comp_val_5" readonly="readonly" value="<?php echo ($ques_val5 <= $sim_com['comp_val_5']) ? abs($sim_com['comp_val_5'] - $ques_val5) : 0; ?>" />
						</div>
						<?php endif; ?>
					
						<?php if ( ! empty($sim_com['comp_col_6']) &&  ! empty($sim_com['comp_val_6'])): ?>
						<div class="skills">
							<label data-title="<?php echo $sim_com['comp_col_6'] ?>"><span class="truncateTxt"><?php echo $sim_com['comp_col_6'] ?></span></label>
							<input type="text" class="form-control" id="comp_val_6" readonly="readonly" value="<?php echo ($ques_val6 <= $sim_com['comp_val_6']) ? abs($sim_com['comp_val_6'] - $ques_val6) : 0; ?>" />
						</div>
						<?php endif; ?>
					</div>
					<div class="previewQus Sbtn">
						<a href="<?php echo $preview ?>" target="_blank" title="Preview"><img class="img-fluid" src="img/qus_icon/preview.svg"></a>
					</div>
				</div>
				<div id="wrapper">
					<div id="sidebar-wrapper">
					   <div class="topbannericonright"><img class="img-fluid closerightmenuA" src="img/list/right_arrow.svg"></div>
					   <div class="QusTempBanner" id="QusTempBanner">
							<ul class="nav nav-QusTempTab">
								<li class="matchingQ <?php echo ( ! empty($qresult['question_type']) && $qresult['question_type'] == 1) ? 'active' : ''; ?>">
									<a data-toggle="tab" href="#matchingQuestion" data-qtype="1" class="qtype"><img class="img-fluid svg" src="img/qus_icon/MTF_but_icon.svg">MATCH THE FOLLOWING</a>
								</li>
								<li class="SequanceQ <?php echo ( ! empty($qresult['question_type']) && $qresult['question_type'] == 2) ? 'active' : ''; ?>">
									<a data-toggle="tab" href="#SequanceQuestion" data-qtype="2" class="qtype"><img class="img-fluid svg" src="img/qus_icon/sequence_but_icon.svg">SEQUENCE QUESTIONS</a>
								</li>
								<li class="DregDropQ <?php echo ( ! empty($qresult['question_type']) && $qresult['question_type'] == 3) ? 'active' : ''; ?>">
									<a data-toggle="tab" href="#DregDropQuestion" data-qtype="3" class="qtype"><img class="img-fluid svg" src="img/qus_icon/sorting_but_icon.svg">SORTING</a>
								</li>
								<li class="VideoQ <?php echo ( ! empty($qresult['question_type']) && $qresult['question_type'] == 8) ? 'active' : ''; ?>">
									<a data-toggle="tab" href="#DNDQuestion" data-qtype="8" class="qtype"><img class="img-fluid svg" src="img/list/DAD_icon.svg">DRAG AND DROP</a>
								</li>
								<li class="MCQQ <?php echo ( ! empty($qresult['question_type']) && $qresult['question_type'] == 4) ? 'active' : ''; ?>">
									<a data-toggle="tab" href="#MCQQuestion" data-qtype="4" class="qtype"><img class="img-fluid svg" src="img/qus_icon/MCQ_but_icon.svg">MCQ</a>
								</li>
								<li class="MMCQQ <?php echo ( ! empty($qresult['question_type']) && $qresult['question_type'] == 5) ? 'active' : ''; ?>">
									<a data-toggle="tab" href="#MMCQQuestion" data-qtype="5" class="qtype"><img class="img-fluid svg" src="img/qus_icon/MMCQ_but_icon.svg">MMCQ</a>
								</li>
								<li class="SwapingQ <?php echo ( ! empty($qresult['question_type']) && $qresult['question_type'] == 7) ? 'active' : ''; ?>">
									<a data-toggle="tab" href="#SwapingQuestion" data-qtype="7" class="qtype"><img class="img-fluid svg" src="img/qus_icon/swiping_but_icon.svg">SWIPING QUESTIONS</a>
								</li>
							</ul>
						</div>
					</div>
					<div class="width12" id="left-wrapper">
						<div class="topbannericon"><img class="img-fluid closeleftmenuA" src="img/list/left_arrow.svg"></div>
						<div class="question-list-banner">
							<!--Start-Load-Question-List-->
							<ul class="question-list" id="sortable"></ul>
							<!--End-->
							<div class="list-icon">
								<ul>
									<li class="DuPIcon"><img class="img-fluid" src="img/list/duplicate_question.svg"/></li>
									<li class="AddIcon"><img class="img-fluid" src="img/list/add_question.svg"/></li>
									<li class="DelIcon"><img class="img-fluid" src="img/list/delete_question.svg"/></li>
								</ul>
							</div>
						</div>
					</div>
					<?php $ques_type = ( ! empty($qresult['question_type'])) ? $qresult['question_type'] : ''; ?>
					<div id="page-content-wrapper">
						<div class="container-fluid">
							<div class="row">
								<div class="tab-content">
									<div class="Q-left">
										<a href="#list-toggle" id="list-toggle" class="hideQTmp"><img class="close_panel" src="img/list/questiion_list.svg" /></a>
									</div>
									<div class="Q-right">
										<a href="#menu-toggle" class= "hideQTmp" id="menu-toggle" <?php echo ( ! empty($ques_type)) ? 'disabled="disabled"' : ''; ?>><img class="close_panel" src="img/list/question_template.svg"></a>
									</div>
									<?php 
									#---Questions
									$ques_att  = '';
									$qatt_type = '';
									if ( ! empty($qresult['qaudio']) && file_exists($root_path . $qresult['qaudio'])):
										$qatt_type = 'qa';
										$ques_att = $qresult['qaudio'];
									endif;
									
									#--Questions-Assets
									$ques_assets = '';
									$assets_type = '';
									if ( ! empty($qresult['audio']) && file_exists($root_path . $qresult['audio'])):
										$assets_type = 'a';
										$ques_assets = $qresult['audio'];
									elseif ( ! empty($qresult['video']) && file_exists($root_path . $qresult['video'])):
										$assets_type = 'v';
										$ques_assets = $qresult['video'];
									elseif ( ! empty($qresult['screen']) && file_exists($root_path . $qresult['screen'])):
										$assets_type = 's';
										$ques_assets = $qresult['screen'];
									elseif ( ! empty($qresult['image']) && file_exists($root_path . $qresult['image'])):
										$assets_type = 'i';
										$ques_assets = $qresult['image'];
									elseif ( ! empty($qresult['document']) && file_exists($root_path . $qresult['document'])):
										$assets_type = 'd';
										$ques_assets = $qresult['document'];
									endif;

									#---Correct-Feedback
									$qfeed_assets = '';
									$feed_assets_type = '';
									if ( ! empty($feedResult[0]['feed_audio'])  && file_exists($root_path . $feedResult[0]['feed_audio']) && $feedResult[0]['feedback_type'] == 1):
										$feed_assets_type = 'fa';
										$qfeed_assets = $feedResult[0]['feed_audio'];
									elseif ( ! empty($feedResult[0]['feed_video']) && file_exists($root_path . $feedResult[0]['feed_video']) && $feedResult[0]['feedback_type'] == 1):
										$feed_assets_type = 'fv';
										$qfeed_assets = $feedResult[0]['feed_video'];
									elseif ( ! empty($feedResult[0]['feed_screen']) && file_exists($root_path . $feedResult[0]['feed_screen']) && $feedResult[0]['feedback_type'] == 1):
										$feed_assets_type = 'fs';
										$qfeed_assets = $feedResult[0]['feed_screen'];
									elseif ( ! empty($feedResult[0]['feed_image']) && file_exists($root_path . $feedResult[0]['feed_image']) && $feedResult[0]['feedback_type'] == 1):
										$feed_assets_type = 'fi';
										$qfeed_assets = $feedResult[0]['feed_image'];
									elseif ( ! empty($feedResult[0]['feed_document']) && file_exists($root_path . $feedResult[0]['feed_document']) && $feedResult[0]['feedback_type'] == 1):
										$feed_assets_type = 'fd';
										$qfeed_assets = $feedResult[0]['feed_document'];
									endif;
									
									#---Incorrect-Feedback
									$qifeed_assets = '';
									$ifeed_assets_type = '';
									if ( ! empty($feedResult[1]['feed_audio']) && file_exists($root_path . $feedResult[1]['feed_audio']) && $feedResult[1]['feedback_type'] == 2):
										$ifeed_assets_type = 'fa';
										$qifeed_assets = $feedResult[1]['feed_audio'];
									elseif ( ! empty($feedResult[1]['feed_video']) && file_exists($root_path . $feedResult[1]['feed_video']) && $feedResult[1]['feedback_type'] == 2):
										$ifeed_assets_type = 'fv';
										$qifeed_assets = $feedResult[1]['feed_video'];
									elseif ( ! empty($feedResult[1]['feed_screen']) && file_exists($root_path . $feedResult[1]['feed_screen']) && $feedResult[1]['feedback_type'] == 2):
										$ifeed_assets_type = 'fs';
										$qfeed_assets = $feedResult[1]['feed_screen'];
									elseif ( ! empty($feedResult[1]['feed_image']) && file_exists($root_path . $feedResult[1]['feed_image']) && $feedResult[1]['feedback_type'] == 2):
										$ifeed_assets_type = 'fi';
										$qifeed_assets = $feedResult[1]['feed_image'];
									elseif ( ! empty($feedResult[1]['feed_document']) && file_exists($root_path . $feedResult[1]['feed_document']) && $feedResult[1]['feedback_type'] == 2):
										$ifeed_assets_type = 'fd';
										$qifeed_assets = $feedResult[1]['feed_document'];
									endif; ?>
									
									<!--Matching-Question-->
									<div id="matchingQuestion" class="tab-pane fade <?php echo ($ques_type == 1) ? 'in active' : ''; ?>">
									   <div class="match-Tab widthQ100">
											<ul class="nav nav-tabs">
												<li class="active"><a data-toggle="tab" href="#match-Q">Question</a></li>
												<li><a data-toggle="tab" href="#match-F">Feedback</a></li>
											</ul>
									   </div>
									   <div class="container-fluid">
											<div class="widthQ100 multitemp">
												<div class="tab-content">
													<div id="match-Q" class="match-Q tab-pane fade in active">
														<div class="MTFtwoButtonbanner">
															<ul class="MTFtwoButton">
																<?php if ( ! empty($qresult['question_id']) && $db->checkShuffleQues($qresult['question_id'], 1) == TRUE): ?>
																<li>
																	<div class="Radio-box MCQRadio">
																		<label class="radiostyle">
																			<input type="radio" class="unsuffleCheck" name="Ordered" checked="checked">
																			<span class="radiomark white"></span>
																			<div class="user_text Ordered">Ordered List</div>
																		</label>
																	</div>
																</li>
																<?php elseif ( ! empty($qresult['question_id']) && $db->checkShuffleQues($qresult['question_id'])): ?>
																<li>
																	<div class="Radio-box MCQRadio">
																		<label class="radiostyle">
																			<input type="radio" class="suffleCheck" name="Ordered" checked="checked">
																			<span class="radiomark white"></span>
																			<div class="user_text Ordered">Auto shuffle</div>
																		</label>
																	</div>
																</li>
																<?php else: ?>
																<li>
																	<div class="Radio-box MCQRadio">
																		<label class="radiostyle">
																			<input type="radio" class="unsuffleCheck mtf_shuffle" name="Ordered" checked="checked" value="1">
																			<span class="radiomark white"></span>
																			<div class="user_text Ordered">Ordered List</div>
																		</label>
																	</div>
																</li>
																<li>
																	<div class="Radio-box MCQRadio">
																		<label class="radiostyle">
																			<input type="radio" class="suffleCheck mtf_shuffle" name="Ordered" value="2">
																			<span class="radiomark white"></span>
																			<div class="user_text Ordered">Auto shuffle</div>
																		</label>
																	</div>
																</li>
																<?php endif; ?>
															</ul>
														</div>
														<div class="quesbox">
															<div class="row">
																<div class="col-sm-12 Questiontmpbanner">
																	<div class="critical_div">
																		<h3 class="qusHeading">QUESTION <p class="question-type">(Match the Following)</p></h3>
																		<div class="CIRtiCaltext">
																			Critical Question <input type="checkbox" name="mtf_criticalQ" id="mtf_criticalQ" class="form-control criticalQ" value="1" <?php echo ( ! empty($qresult['critical'])) ? 'checked="checked"' : ''; ?> />
																		</div>
																	</div>
																	<div class="quesbox-flex">
																		<div class="form-group">
																			<input type="text" name="mtfq" id="mtfq" class="form-control QuesForm inputtextWrap mtf" placeholder="Click here to enter question" value="<?php echo ( ! empty($qresult['questions'])) ? stripslashes($qresult['questions']) : ''; ?>" />
																			<div class="mtfqq_assets_container" <?php echo ( ! empty($ques_att) && $ques_type == 1) ? '' : 'style="display:none;"'; ?>>
																				<div class="assets_data mtfqq_assets_data"><a class="view_assets" data-src="<?php echo $path . $ques_att; ?>" href="javascript:;"><?php echo $ques_att; ?></a></div>
																				<a href="javascript:void(0);" data-assets="<?php echo $ques_att; ?>" data-assets-id="mtfqq_assets" data-input-id="#mtfq_qaudio,#mtfq_rec_audio" data-path="<?php echo $uploadpath ?>" class="delete_assets" title="Delete"><i class="fa fa-times" aria-hidden="true"></i></a>
																			</div>
																			<ul class="QueBoxIcon">
																				<li class="dropdown">
																					<button class="btn1 btn-secondary dropdown-toggle mtfqq_assets <?php echo ( ! empty($ques_att) && $ques_type == 1) ? 'disabled' : ''; ?>" <?php echo ( ! empty($ques_att) && $ques_type == 1) ? 'disabled="disabled"' : ''; ?> type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img class="img-fluid" src="img/list/audio.svg"></button>
																					<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
																						<a class="dropdown-item">
																							<div class="tooltipLeft uploadicon">
																								<label for="file-input-mtfqq-addAudio"><img class="img-fluid"  src="img/list/add_audio.svg" /><span class="tooltiptext">Add Audio</span></label>
																								<input id="file-input-mtfqq-addAudio" data-id="#mtfq_qaudio" data-assets="mtfqq_assets" class="uploadAudioFile" type="file" name="mtfq_Audio" />
																								<input type="hidden" name="mtfq_qaudio" id="mtfq_qaudio" value="<?php echo ($ques_type == 1 && $qatt_type == 'qa') ? $ques_att : ''; ?>"/>
																							</div>
																						</a>
																						<a class="dropdown-item">
																							<div class="tooltipLeft">
																								<img class="img-fluid rec-audio" data-input-id="mtfq_rec_audio" data-assets="mtfqq_assets" src="img/list/record_audio.svg" />
																								<span class="tooltiptext">Record Audio</span>
																							</div>
																							<input type="hidden" name="mtfq_rec_audio" id="mtfq_rec_audio" />
																						 </a>
																						<a data-toggle="modal" data-target="#mtfqqtts" class="dropdown-item">
																							<div class="tooltipLeft">
																								<img class="img-fluid" src="img/list/text_speech.svg" />
																								<span class="tooltiptext">Text to Speech</span>
																							</div>
																						 </a>
																					 </div>
																				</li>
																			</ul>
																		</div>
																	</div>
																	<div class="q-icon-banner clearfix">
																		<ul class="QueBoxIcon QueBoxIcon1">
																			<h5>Add Assets</h5>
																			<li class="dropdown">
																				<button class="btn1 btn-secondary dropdown-toggle mtfq_assets <?php echo ( ! empty($ques_assets) && $ques_type == 1) ? 'disabled' : ''; ?>" <?php echo ( ! empty($ques_assets) && $ques_type == 1) ? 'disabled="disabled"' : ''; ?> type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img class="img-fluid" src="img/list/audio.svg"></button>
																				<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
																					<a class="dropdown-item">
																						<div class="tooltip1">
																							<img class="img-fluid" onclick="matchopen1()" src="img/list/text_speech.svg" />
																							<span class="tooltiptext">Text to Speech</span>
																						 </div>
																					</a>
																					<a class="dropdown-item">
																						<div class="tooltip1 uploadicon">
																							<label for="file-input-mtf-addAudio">
																								<img class="img-fluid" src="img/list/add_audio.svg" />
																								<span class="tooltiptext">Add Audio</span>
																							</label>
																							<input id="file-input-mtf-addAudio" data-id="#mtf_qaudio" data-assets="mtfq_assets" class="uploadAudioFile" type="file" name="mtf_Audio" />
																							<input type="hidden" name="mtf_qaudio" id="mtf_qaudio" value="<?php echo ($ques_type == 1 && $assets_type == 'a') ? $ques_assets : ''; ?>"/>
																						</div>
																					</a>
																					<a class="dropdown-item">
																						<div class="tooltip1">
																							<img class="img-fluid rec-audio" data-input-id="mtf_rec_audio" data-assets="mtfq_assets" src="img/list/record_audio.svg" />
																							<span class="tooltiptext">Record Audio</span>
																						</div>
																						<input type="hidden" name="mtf_rec_audio" id="mtf_rec_audio" />
																					 </a>
																				</div>
																			</li>
																			<li class="dropdown">
																				<button class="btn1 btn-secondary dropdown-toggle mtfq_assets <?php echo ( ! empty($ques_assets) && $ques_type == 1) ? 'disabled' : ''; ?>" <?php echo ( ! empty($ques_assets) && $ques_type == 1) ? 'disabled="disabled"' : ''; ?> type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img class="img-fluid" src="img/list/video.svg"></button>
																				<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
																					<a class="dropdown-item">
																						<div class="tooltip1 uploadicon">
																							<label for="file-input-mtf-addVideo">
																								<img class="img-fluid" src="img/list/add_video.svg" />
																								<span class="tooltiptext">Add Video</span>
																							</label>
																							<input id="file-input-mtf-addVideo" data-id="#mtf_video" data-assets="mtfq_assets" class="uploadVideoFile" type="file" name="mtf_Video"/>
																							<input type="hidden" name="mtf_video" id="mtf_video" value="<?php echo ($ques_type == 1 && $assets_type == 'v') ? $ques_assets : ''; ?>"/>
																						</div>
																					</a>
																					<a class="dropdown-item">
																						<div class="tooltip1">
																							<img class="img-fluid rec-video" data-input-id="mtf_rec_video" data-assets="mtfq_assets" src="img/qus_icon/rec_video.svg" />
																							<span class="tooltiptext">Record Video</span>
																						 </div>
																						 <input type="hidden" name="mtf_rec_video" id="mtf_rec_video" />
																					</a>
																					<a class="dropdown-item">
																						<div class="tooltip1">
																							<img class="img-fluid rec-screen" data-input-id="mtf_rec_screen" data-assets="mtfq_assets" src="img/list/screen_record.svg" />
																							<span class="tooltiptext">Record Screen</span>
																						 </div>
																						 <input type="hidden" name="mtf_rec_screen" id="mtf_rec_screen" value="<?php echo ($ques_type == 1 && $assets_type == 's') ? $ques_assets : ''; ?>" />
																					</a>
																				</div>
																			</li>
																			<li>
																				<div class="tooltip uploadicon mtfq_assets <?php echo ( ! empty($ques_assets) && $ques_type == 1) ? 'disabled' : ''; ?>" <?php echo ( ! empty($ques_assets) && $ques_type == 1) ? 'disabled="disabled"' : ''; ?>>
																					<label for="file-input-mtf-addImg">
																						<img class="img-fluid" src="img/list/image.svg" />
																						<span class="tooltiptext">Add Image</span>
																					</label>
																					<input id="file-input-mtf-addImg" data-id="#mtf_img" data-assets="mtfq_assets" class="uploadImgFile mtfq_assets" type="file" name="mtf_Img" <?php echo ( ! empty($ques_assets) && $ques_type == 1) ? 'disabled="disabled"' : ''; ?> />
																					<input type="hidden" name="mtf_img" id="mtf_img" value="<?php echo ($ques_type == 1 && $assets_type == 'i') ? $ques_assets : ''; ?>"/>
																				</div>
																			</li>
																			<li>
																				<div class="tooltip uploadicon mtfq_assets <?php echo ( ! empty($ques_assets) && $ques_type == 1) ? 'disabled' : ''; ?>" <?php echo ( ! empty($ques_assets) && $ques_type == 1) ? 'disabled="disabled"' : ''; ?>>
																					<label for="file-input-mtf-addDoc">
																						<img class="img-fluid" src="img/list/add_document.svg" />
																						<span class="tooltiptext">Add Document</span>
																					</label>
																					<input id="file-input-mtf-addDoc" data-id="#mtf_doc" data-assets="mtfq_assets" class="uploadDocFile mtfq_assets" type="file" name="mtf_Doc"/>
																					<input type="hidden" name="mtf_doc" id="mtf_doc" value="<?php echo ($ques_type == 1 && $assets_type == 'd') ? $ques_assets : ''; ?>"/>
																				</div>
																			</li>
																		</ul>
																		<div class="Ques-comp">
																			<h5>Add Competency Score</h5>
																			<?php if ( ! empty($sim_com['comp_col_1']) &&  ! empty($sim_com['comp_val_1'])): ?>
																			<div class="form-group QusScore">
																				<input type="text" class="form-control mtf" name="ques_val_1" id="ques_val_1" onkeypress="return isNumberKey(event);" value="<?php echo ( ! empty($qresult['ques_val_1'])) ? $qresult['ques_val_1'] : '' ?>" />
																			</div>
																			<input type="hidden" class="form-control" id="ques_val_1h" value="<?php echo ( ! empty($qresult['ques_val_1'])) ? $qresult['ques_val_1'] : ''; ?>" />
																			<?php else: ?>
																			<input type="hidden" class="form-control" name="ques_val_1" value="0" />
																			<?php endif; ?>
																		
																			<?php if ( ! empty($sim_com['comp_col_2']) &&  ! empty($sim_com['comp_val_2'])): ?>
																			<div class="form-group QusScore">
																				<input type="text" class="form-control mtf" name="ques_val_2" id="ques_val_2" onkeypress="return isNumberKey(event);" value="<?php echo ( ! empty($qresult['ques_val_2'])) ? $qresult['ques_val_2'] : ''; ?>" />
																			</div>
																			<input type="hidden" class="form-control" id="ques_val_2h" value="<?php echo ( ! empty($qresult['ques_val_2'])) ? $qresult['ques_val_2'] : ''; ?>" />
																			<?php else: ?>
																			<input type="hidden" class="form-control" name="ques_val_2" value="0" />
																			<?php endif; ?>
																			
																			<?php if ( ! empty($sim_com['comp_col_3']) &&  ! empty($sim_com['comp_val_3'])): ?>
																			<div class="form-group QusScore">
																				<input type="text" class="form-control mtf" name="ques_val_3" id="ques_val_3" onkeypress="return isNumberKey(event);" value="<?php echo ( ! empty($qresult['ques_val_3'])) ? $qresult['ques_val_3'] : ''; ?>" />
																			</div>
																			<input type="hidden" class="form-control" id="ques_val_3h" value="<?php echo ( ! empty($qresult['ques_val_3'])) ? $qresult['ques_val_3'] : ''; ?>" />
																			<?php else: ?>
																			<input type="hidden" class="form-control" name="ques_val_3" value="0" />
																			<?php endif; ?>
																			
																			<?php if ( ! empty($sim_com['comp_col_4']) &&  ! empty($sim_com['comp_val_4'])): ?>
																			<div class="form-group QusScore">
																				<input type="text" class="form-control mtf" name="ques_val_4" id="ques_val_4" onkeypress="return isNumberKey(event);" value="<?php echo ( ! empty($qresult['ques_val_4'])) ? $qresult['ques_val_4'] : ''; ?>" />
																			</div>
																			<input type="hidden" class="form-control" id="ques_val_4h" value="<?php echo ( ! empty($qresult['ques_val_4'])) ? $qresult['ques_val_4'] : ''; ?>" />
																			<?php else: ?>
																			<input type="hidden" class="form-control" name="ques_val_4" value="0" />
																			<?php endif; ?>
																			
																			<?php if ( ! empty($sim_com['comp_col_5']) &&  ! empty($sim_com['comp_val_5'])): ?>
																			<div class="form-group QusScore">
																				<input type="text" class="form-control mtf" name="ques_val_5" id="ques_val_5" onkeypress="return isNumberKey(event);" value="<?php echo ( ! empty($qresult['ques_val_5'])) ? $qresult['ques_val_5'] : ''; ?>" />
																			</div>
																			<input type="hidden" class="form-control" id="ques_val_5h" value="<?php echo ( ! empty($qresult['ques_val_5'])) ? $qresult['ques_val_5'] : ''; ?>" />
																			<?php else: ?>
																			<input type="hidden" class="form-control" name="ques_val_5" value="0" />
																			<?php endif; ?>
																			
																			<?php if ( ! empty($sim_com['comp_col_6']) &&  ! empty($sim_com['comp_val_6'])): ?>
																			<div class="form-group QusScore">
																				<input type="text" class="form-control mtf" name="ques_val_6" id="ques_val_6" onkeypress="return isNumberKey(event);" value="<?php echo ( ! empty($qresult['ques_val_6'])) ? $qresult['ques_val_6'] : ''; ?>" />
																			</div>
																			<input type="hidden" class="form-control" id="ques_val_6h" value="<?php echo ( ! empty($qresult['ques_val_6'])) ? $qresult['ques_val_6'] : ''; ?>" />
																			<?php else: ?>
																			<input type="hidden" class="form-control" name="ques_val_6" value="0" />
																			<?php endif; ?>
																		</div>
																	</div>
																	<div class="mtfq_assets_container" <?php echo ( ! empty($ques_assets) && $ques_type == 1) ? '' : 'style="display:none;"'; ?>>
																		<div class="assets_data mtfq_assets_data"><a class="view_assets" data-src="<?php echo $path . $ques_assets; ?>" href="javascript:;"><?php echo $ques_assets; ?></a></div>
																		<a href="javascript:void(0);" data-assets="<?php echo $ques_assets; ?>" data-assets-id="mtfq_assets" data-input-id="#mtf_img,#mtf_qaudio,#mtf_rec_audio,#mtf_video,#mtf_rec_video,#mtf_rec_screen,#mtf_doc" data-path="<?php echo $uploadpath ?>" class="delete_assets" title="Delete"><i class="fa fa-times" aria-hidden="true"></i></a>
																	</div>
																 </div>
															</div>
														</div>
														<div class="Ansbox">
															<div class="row">
																<div class="col-sm-12">
																	<div class="col-sm-6"><h3 class="AnsHeading AnsHeadingtf">CHOICE</h3></div>
																	<div class="col-sm-6">
																		<h3 class="AnsHeading Mchoice">MATCH</h3>
																	</div>
																	<div class="matchingchice" <?php echo ( ! empty($qresult['question_id']) && $db->checkShuffleQues($qresult['question_id'])) ? 'style="display:none"' : ''; ?>>
																		<div class="ApendOption-box">
																		<?php 
																			$mtf_choice_sql = "SELECT answer_id, choice_option, choice_order_no, match_order_no, match_option FROM answer_tbl WHERE question_id = '". $qresult['question_id'] ."' AND choice_order_no != '0' AND match_order_no != '0'";
																			$mtf_choice = $db->prepare($mtf_choice_sql); $mtf_choice->execute();
																			if ($mtf_choice->rowCount() > 0): $mtfi = 1;
																			foreach ($mtf_choice->fetchAll(PDO::FETCH_ASSOC) as $mtf_choice_row): ?>
																			<div class="MatchOption-box1">
																				<input type="hidden" name="updateAnswerid[]" value="<?php echo $mtf_choice_row['answer_id'] ?>" />
																				<div class="col-sm-6">
																					<div class="matchbox">
																						<div class="form-group">
																							<input type="text" name="mtf_choice[]" class="form-control inputtextWrap shuffle_off mtf" placeholder="Click here to enter choice" value="<?php echo stripslashes($mtf_choice_row['choice_option']); ?>" />
																						</div>
																						<div class="form-group QusScore">
																							<input type="text" name="mtf_choice_order[]" class="form-control shuffle_off mtf" onkeypress="return isNumberKey(event);" value="<?php echo stripslashes($mtf_choice_row['choice_order_no']); ?>" />
																						</div>
																					</div>
																				</div>
																				<div class="col-sm-6">
																					<div class="matchbox">									
																						<div class="form-group QusScore">
																							<input type="text" name="mtf_match_order[]" class="form-control shuffle_off mtf" onkeypress="return isNumberKey(event);" value="<?php echo stripslashes($mtf_choice_row['match_order_no']); ?>" />
																						</div>
																						<div class="form-group">
																							<input type="text" name="mtf_match[]" class="form-control inputtextWrap shuffle_off mtf" placeholder="Click here to enter text" value="<?php echo stripslashes($mtf_choice_row['match_option']); ?>" />
																						</div>
																					</div>
																				</div>
																				<?php if ($mtfi == 1): ?>
																				<div class="AddAns-Option"><img class="img-fluid AddAnsICON" src="img/list/add_field.svg"></div>
																				<?php else: ?>
																				<div class="AddAns-Option removeOption" data-remove-answer-id="<?php echo $mtf_choice_row['answer_id'] ?>"><img class="img-fluid" src="img/list/delete_field.svg"></div>
																				<?php endif; ?>
																			</div>
																			<?php $mtfi++; endforeach; else: ?>
																			<div class="MatchOption-box1">
																				<div class="col-sm-6">
																					<div class="matchbox">
																						<div class="form-group">
																							<input type="text" name="mtf_choice[]" class="form-control inputtextWrap shuffle_off mtf" placeholder="Click here to enter choice" />
																						</div>
																						<div class="form-group QusScore">
																							<input type="text" name="mtf_choice_order[]" class="form-control shuffle_off mtf" onkeypress="return isNumberKey(event);" />
																						</div>
																					</div>
																				</div>
																				<div class="col-sm-6">
																					<div class="matchbox">		
																						<div class="form-group QusScore">
																							<input type="text" name="mtf_match_order[]" class="form-control shuffle_off mtf" onkeypress="return isNumberKey(event);" />
																						</div>
																						<div class="form-group">
																							<input type="text" name="mtf_match[]" class="form-control inputtextWrap shuffle_off mtf" placeholder="Click here to enter text" />
																						</div>
																					</div>
																				</div>
																				<div class="AddAns-Option">
																					<img class="img-fluid AddAnsICON" src="img/list/add_field.svg">
																				</div>
																			</div>
																			<?php endif; ?>
																		</div>
																	</div>
																	<div class="matchingchicesuffle" <?php echo ( ! empty($qresult['question_id']) && $db->checkShuffleQues($qresult['question_id'])) ? 'style="display:block"' : 'style="display:none"'; ?>>
																		<div class="ApendOption-box">
																		<?php 
																			$mtf_choice_sql1 = "SELECT answer_id, choice_option, match_option FROM answer_tbl WHERE question_id = '". $qresult['question_id'] ."' AND choice_order_no = '0' AND match_order_no = '0'";
																			$mtf_choice1 = $db->prepare($mtf_choice_sql1); $mtf_choice1->execute();
																			if ($mtf_choice1->rowCount() > 0): $mtfi1 = 1;
																			foreach ($mtf_choice1->fetchAll(PDO::FETCH_ASSOC) as $mtf_choice_row1): ?>
																			<div class="MatchOption-box2">
																				<input type="hidden" name="updateAnswerid[]" value="<?php echo $mtf_choice_row1['answer_id'] ?>" />
																				<div class="col-sm-6">
																					<div class="matchbox">
																						<div class="form-group">
																							<input type="text" name="mtf_choice2[]" class="form-control inputtextWrap shuffle_on mtf" placeholder="Click here to enter choice" value="<?php echo stripslashes($mtf_choice_row1['choice_option']); ?>" />
																						</div>
																					</div>
																				</div>
																				<div class="col-sm-6">
																					<div class="matchbox">
																						<div class="form-group">
																							<input type="text" name="mtf_match2[]" class="form-control inputtextWrap shuffle_on mtf" placeholder="Click here to enter text" value="<?php echo stripslashes($mtf_choice_row1['match_option']); ?>" />
																						</div>
																					</div>
																				</div>
																				<?php if ($mtfi1 == 1): ?>
																					<div class="AddAns-Option"><img class="img-fluid AddAnsICON1" src="img/list/add_field.svg"></div>
																				<?php else: ?>
																					<div class="AddAns-Option removeOption" data-remove-answer-id="<?php echo $mtf_choice_row1['answer_id'] ?>"><img class="img-fluid" src="img/list/delete_field.svg"></div>
																				<?php endif; ?>
																			</div>
																			<?php $mtfi1++; endforeach; else: ?>
																			<div class="MatchOption-box2 MSuffle">
																				<div class="col-sm-6">
																					<div class="matchbox">
																						<div class="form-group">
																							<input type="text" name="mtf_choice2[]" class="form-control inputtextWrap shuffle_on mtf" placeholder="Click here to enter choice" />
																						</div>
																					</div>
																				</div>
																				<div class="col-sm-6">
																					<div class="matchbox">
																						<div class="form-group">
																							<input type="text" name="mtf_match2[]" class="form-control inputtextWrap shuffle_on mtf" placeholder="Click here to enter text" />
																						</div>
																					</div>
																				</div>
																				<div class="AddAns-Option">
																					<img class="img-fluid AddAnsICON1" src="img/list/add_field.svg">
																				</div>
																			</div>
																			<?php endif; ?>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
													<div class="form-popup draggable Ques-pop-style modal" id="mtfqqtts">
														<div class="popheading">Text to Speech</div>
														<div class="textarea">
															<textarea type="text" class="form-control1 inputtextWrap" name="mtfqq_text_to_speech" id="mtfqq_text_to_speech"><?php echo ($ques_type == 1 && ! empty($qresult['qspeech_text'])) ? stripslashes($qresult['qspeech_text']) : ''; ?></textarea>
														</div>
														<div class="submitRight1">
															<button type="button" class="btn1 submitbtn1" data-dismiss="modal">Insert</button>
															<button type="button" class="btn1 submitbtn1" onclick="clearData('mtfqq_text_to_speech');">Clear</button>
															<button type="button" class="btn1 submitbtn1" data-dismiss="modal">Cancel</button>
														</div>
													</div>
													<div class="form-popup draggable" id="match1">
														<div class="popheading">Text to Speech</div>
														<div class="textarea">
															<textarea type="text" class="form-control1 inputtextWrap" name="mtfq_text_to_speech" id="mtfq_text_to_speech"><?php echo ($ques_type == 1 && ! empty($qresult['speech_text'])) ? stripslashes($qresult['speech_text']) : ''; ?></textarea>
														</div>
														<div class="submitRight1">
															<button type="button" class="btn1 submitbtn1" onclick="matchclose1()">Insert</button>
															<button type="button" class="btn1 submitbtn1" onclick="clearData('mtfq_text_to_speech');">Clear</button>
															<button type="button" class="btn1 submitbtn1" onclick="matchclose1()">Cancel</button>
														</div>
													</div>
													<div id="match-F" class="tab-pane fade in">
														<div class="col-sm-12 Qusfeedback">
															<div class="form-group">
																<div class="feedback-flex">
																	<h4>Correct</h4>
																	<textarea name="mtf_cfeedback" class="form-control FeedForm inputtextWrap mtf" rows="3" placeholder="That's right! You selected the correct response."><?php echo ($ques_type == 1 && ! empty($feedResult[0]['feedback']) && $feedResult[0]['feedback_type'] == 1) ? stripslashes($feedResult[0]['feedback']) : ''; ?></textarea>
																</div>
																<ul class="QueBoxIcon feed LMfeed">
																	<h5>Add assets</h5>
																	<li class="dropdown">
																		<button class="btn1 btn-secondary dropdown-toggle mtff_assets <?php echo ( ! empty($qfeed_assets) && $ques_type == 1) ? 'disabled' : ''; ?>" <?php echo ( ! empty($qfeed_assets) && $ques_type == 1) ? 'disabled="disabled"' : ''; ?> type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img class="img-fluid" src="img/list/audio.svg"></button>
																		<div class="dropdown-menu buttom" aria-labelledby="dropdownMenuButton">
																			<a class="dropdown-item">
																				<div class="tooltip1">
																					<img class="img-fluid" onclick="mtf_feedback_tts()" src="img/list/text_speech.svg" />
																					<span class="tooltiptext">Text to Speech</span>
																				</div>
																			</a>
																			<a class="dropdown-item">
																				<div class="tooltip1 uploadicon">
																					<label for="file-input-mtf-feedback-addAudio">
																						<img class="img-fluid" src="img/list/add_audio.svg" />
																						<span class="tooltiptext">Add Audio</span>
																					</label>
																					<input id="file-input-mtf-feedback-addAudio" data-id="#mtf_feedback_audio" data-assets="mtff_assets" class="uploadAudioFile" type="file" name="mtf_feedback_Audio" />
																					<input type="hidden" name="mtf_feedback_audio" id="mtf_feedback_audio" value="<?php echo ($ques_type == 1 && $feed_assets_type == 'fa') ? $qfeed_assets : ''; ?>" />
																				</div>
																			</a>
																			<a class="dropdown-item">
																				<div class="tooltip1">
																					<img class="img-fluid rec-audio" data-input-id="mtf_feedback_rec_audio" data-assets="mtff_assets" src="img/list/record_audio.svg" />
																					<span class="tooltiptext">Record Audio</span>
																				</div>
																				<input type="hidden" name="mtf_feedback_rec_audio" id="mtf_feedback_rec_audio" />
																			 </a>
																		</div>
																	</li>
																	<li class="dropdown">
																		<button class="btn1 btn-secondary dropdown-toggle mtff_assets <?php echo ( ! empty($qfeed_assets) && $ques_type == 1) ? 'disabled' : ''; ?>" <?php echo ( ! empty($qfeed_assets) && $ques_type == 1) ? 'disabled="disabled"' : ''; ?> type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img class="img-fluid" src="img/list/video.svg"></button>
																		<div class="dropdown-menu buttom" aria-labelledby="dropdownMenuButton">
																			<a class="dropdown-item">
																				<div class="tooltip1 uploadicon">
																					<label for="file-input-mtf-feedback-addVideo">
																						<img class="img-fluid" src="img/list/add_video.svg" />
																						<span class="tooltiptext">Add Video</span>
																					</label>
																					<input id="file-input-mtf-feedback-addVideo" data-id="#mtf_feedback_video" data-assets="mtff_assets" class="uploadVideoFile" type="file" name="mtf_feedback_Video" />
																					<input type="hidden" name="mtf_feedback_video" id="mtf_feedback_video" value="<?php echo ($ques_type == 1 && $feed_assets_type == 'fv') ? $qfeed_assets : ''; ?>" />
																				</div>
																			</a>
																			<a class="dropdown-item">
																				<div class="tooltip1">
																					<img class="img-fluid rec-video" data-input-id="mtf_feedback_rec_video" data-assets="mtff_assets" src="img/qus_icon/rec_video.svg" />
																					<span class="tooltiptext">Record Video</span>
																				 </div>
																				 <input type="hidden" name="mtf_feedback_rec_video" id="mtf_feedback_rec_video" />
																			</a>
																			<a class="dropdown-item">
																				<div class="tooltip1">
																					<img class="img-fluid rec-screen" data-input-id="mtf_feedback_rec_screen" data-assets="mtff_assets" src="img/list/screen_record.svg" />
																					<span class="tooltiptext">Record Screen</span>
																				 </div>
																				 <input type="hidden" name="mtf_feedback_rec_screen" id="mtf_feedback_rec_screen" value="<?php echo ($ques_type == 1 && $feed_assets_type == 'fs') ? $qfeed_assets : ''; ?>" />
																			</a>
																		</div>
																	</li>
																	<li>
																		<div class="tooltip uploadicon mtff_assets <?php echo ( ! empty($qfeed_assets) && $ques_type == 1) ? 'disabled' : ''; ?>" <?php echo ( ! empty($qfeed_assets) && $ques_type == 1) ? 'disabled="disabled"' : ''; ?>>
																			<label for="file-input-mtf-feedback-addImg">
																				<img class="img-fluid" src="img/list/image.svg" />
																				<span class="tooltiptext">Add Image</span>
																			</label>
																			<input id="file-input-mtf-feedback-addImg" data-id="#mtf_feedback_img" data-assets="mtff_assets" class="uploadImgFile mtff_assets" type="file" name="mtf_feedback_Img" <?php echo ( ! empty($qfeed_assets) && $ques_type == 1) ? 'disabled="disabled"' : ''; ?> />
																			<input type="hidden" name="mtf_feedback_img" id="mtf_feedback_img" value="<?php echo ($ques_type == 1 && $feed_assets_type == 'fi') ? $qfeed_assets : ''; ?>" />
																		</div>
																	</li>
																	<li>
																		<div class="tooltip uploadicon mtff_assets <?php echo ( ! empty($qfeed_assets) && $ques_type == 1) ? 'disabled' : ''; ?>" <?php echo ( ! empty($qfeed_assets) && $ques_type == 1) ? 'disabled="disabled"' : ''; ?>>
																			<label for="file-input-mtf-feedback-addDoc">
																				<img class="img-fluid" src="img/list/add_document.svg" />
																				<span class="tooltiptext">Add Document</span>
																			</label>
																			<input id="file-input-mtf-feedback-addDoc" data-id="#mtf_feedback_doc" data-assets="mtff_assets" class="uploadDocFile mtff_assets" type="file" name="mtf_feedback_Doc" <?php echo ( ! empty($qfeed_assets) && $ques_type == 1) ? 'disabled="disabled"' : ''; ?>/>
																			<input type="hidden" name="mtf_feedback_doc" id="mtf_feedback_doc" value="<?php echo ($ques_type == 1 && $feed_assets_type == 'fd') ? $qfeed_assets : ''; ?>" />
																		</div>
																	</li>
																</ul>
															</div>                                                        
															<div class="mtff_assets_container" <?php echo ( ! empty($qfeed_assets) && $ques_type == 1) ? '' : 'style="display:none;"'; ?>>
																<div class="assets_data mtff_assets_data"><a class="view_assets" data-src="<?php echo $path . $qfeed_assets; ?>" href="javascript:;"><?php echo $qfeed_assets; ?></a></div>
																<a href="javascript:void(0);" data-assets="<?php echo $qfeed_assets ?>" data-assets-id="mtff_assets" data-input-id="#mtf_feedback_img,#mtf_feedback_audio,#mtf_feedback_rec_audio,#mtf_feedback_video,#mtf_feedback_rec_video,#mtf_feedback_rec_screen" data-path="<?php echo $uploadpath ?>" class="delete_assets" title="Delete"><i class="fa fa-times" aria-hidden="true"></i></a>
															</div>
															<div class="form-group">
																<div class="feedback-flex">
																	<h4>Incorrect</h4>
																	<textarea name="mtf_ifeedback" class="form-control FeedForm inputtextWrap mtf" rows="3" placeholder="You did not select the correct response."><?php echo ($ques_type == 1 && ! empty($feedResult[1]['feedback']) && $feedResult[1]['feedback_type'] == 2) ? stripslashes($feedResult[1]['feedback']) : ''; ?></textarea>
																</div>
																 <ul class="QueBoxIcon feed LMfeed">
																	<h5>Add assets</h5>
																	<li class="dropdown">
																		<button class="btn1 btn-secondary dropdown-toggle mtffi_assets <?php echo ( ! empty($qifeed_assets) && $ques_type == 1) ? 'disabled' : ''; ?>" <?php echo ( ! empty($qifeed_assets) && $ques_type == 1) ? 'disabled="disabled"' : ''; ?> type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img class="img-fluid" src="img/list/audio.svg"></button>
																		<div class="dropdown-menu buttom" aria-labelledby="dropdownMenuButton">
																			<a class="dropdown-item">
																				<div class="tooltip1">
																					<img class="img-fluid" onclick="mtfi_feedback_tts()" src="img/list/text_speech.svg" />
																					<span class="tooltiptext">Text to Speech</span>
																				</div>
																			</a>
																			<a class="dropdown-item">
																				<div class="tooltip1 uploadicon">
																					<label for="file-input-mtfi-feedback-addAudio">
																						<img class="img-fluid" src="img/list/add_audio.svg" />
																						<span class="tooltiptext">Add Audio</span>
																					</label>
																					<input id="file-input-mtfi-feedback-addAudio" data-id="#mtfi_feedback_audio" data-assets="mtffi_assets" class="uploadAudioFile" type="file" name="mtfi_feedback_Audio" />
																					<input type="hidden" name="mtfi_feedback_audio" id="mtfi_feedback_audio" value="<?php echo ($ques_type == 1 && $ifeed_assets_type == 'fa') ? $qifeed_assets : ''; ?>" />
																				</div>
																			</a>
																			<a class="dropdown-item">
																				<div class="tooltip1">
																					<img class="img-fluid rec-audio" data-input-id="mtfi_feedback_rec_audio" data-assets="mtffi_assets" src="img/list/record_audio.svg" />
																					<span class="tooltiptext">Record Audio</span>
																				</div>
																				<input type="hidden" name="mtfi_feedback_rec_audio" id="mtfi_feedback_rec_audio" />
																			 </a>
																		</div>
																	</li>
																	<li class="dropdown">
																		<button class="btn1 btn-secondary dropdown-toggle mtffi_assets <?php echo ( ! empty($qifeed_assets) && $ques_type == 1) ? 'disabled' : ''; ?>" <?php echo ( ! empty($qifeed_assets) && $ques_type == 1) ? 'disabled="disabled"' : ''; ?> type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img class="img-fluid" src="img/list/video.svg"></button>
																		<div class="dropdown-menu buttom" aria-labelledby="dropdownMenuButton">
																			<a class="dropdown-item">
																				<div class="tooltip1 uploadicon">
																					<label for="file-input-mtfi-feedback-addVideo">
																						<img class="img-fluid" src="img/list/add_video.svg" />
																						<span class="tooltiptext">Add Video</span>
																					</label>
																					<input id="file-input-mtfi-feedback-addVideo" data-id="#mtfi_feedback_video" data-assets="mtffi_assets" class="uploadVideoFile" type="file" name="mtfi_feedback_Video" />
																					<input type="hidden" name="mtfi_feedback_video" id="mtfi_feedback_video" value="<?php echo ($ques_type == 1 && $ifeed_assets_type == 'fv') ? $qifeed_assets : ''; ?>" />
																				</div>
																			</a>
																			<a class="dropdown-item">
																				<div class="tooltip1">
																					<img class="img-fluid rec-video" data-input-id="mtfi_feedback_rec_video" data-assets="mtffi_assets" src="img/qus_icon/rec_video.svg" />
																					<span class="tooltiptext">Record Video</span>
																				 </div>
																				 <input type="hidden" name="mtfi_feedback_rec_video" id="mtfi_feedback_rec_video" />
																			</a>
																			<a class="dropdown-item">
																				<div class="tooltip1">
																					<img class="img-fluid rec-screen" data-input-id="mtfi_feedback_rec_screen" data-assets="mtffi_assets" src="img/list/screen_record.svg" />
																					<span class="tooltiptext">Record Screen</span>
																				 </div>
																				 <input type="hidden" name="mtfi_feedback_rec_screen" id="mtfi_feedback_rec_screen" value="<?php echo ($ques_type == 1 && $ifeed_assets_type == 'fs') ? $qifeed_assets : ''; ?>" />
																			</a>
																		</div>
																	</li>
																	<li>
																		<div class="tooltip uploadicon mtffi_assets <?php echo ( ! empty($qifeed_assets) && $ques_type == 1) ? 'disabled' : ''; ?>" <?php echo ( ! empty($qifeed_assets) && $ques_type == 1) ? 'disabled="disabled"' : ''; ?>>
																			<label for="file-input-mtfi-feedback-addImg">
																				<img class="img-fluid" src="img/list/image.svg" />
																				<span class="tooltiptext">Add Image</span>
																			</label>
																			<input id="file-input-mtfi-feedback-addImg" data-id="#mtfi_feedback_img" data-assets="mtffi_assets" class="uploadImgFile mtffi_assets" type="file" name="mtfi_feedback_Img" <?php echo ( ! empty($qifeed_assets) && $ques_type == 1) ? 'disabled="disabled"' : ''; ?> />
																			<input type="hidden" name="mtfi_feedback_img" id="mtfi_feedback_img" value="<?php echo ($ques_type == 1 && $ifeed_assets_type == 'fi') ? $qfeed_assets : ''; ?>" />
																		</div>
																	</li>
																	<li>
																		<div class="tooltip uploadicon mtffi_assets <?php echo ( ! empty($qfeed_assets) && $ques_type == 1) ? 'disabled' : ''; ?>" <?php echo ( ! empty($qifeed_assets) && $ques_type == 1) ? 'disabled="disabled"' : ''; ?>>
																			<label for="file-input-mtfi-feedback-addDoc">
																				<img class="img-fluid" src="img/list/add_document.svg" />
																				<span class="tooltiptext">Add Document</span>
																			</label>
																			<input id="file-input-mtfi-feedback-addDoc" data-id="#mtfi_feedback_doc" data-assets="mtffi_assets" class="uploadDocFile mtffi_assets" type="file" name="mtfi_feedback_Doc" <?php echo ( ! empty($qifeed_assets) && $ques_type == 1) ? 'disabled="disabled"' : ''; ?>/>
																			<input type="hidden" name="mtfi_feedback_doc" id="mtfi_feedback_doc" value="<?php echo ($ques_type == 1 && $ifeed_assets_type == 'fd') ? $qifeed_assets : ''; ?>" />
																		</div>
																	</li>
																</ul>                              
															</div>                                                        
															<div class="mtffi_assets_container" <?php echo ( ! empty($qifeed_assets) && $ques_type == 1) ? '' : 'style="display:none;"'; ?>>
																<div class="assets_data mtffi_assets_data"><a class="view_assets" data-src="<?php echo $path . $qifeed_assets; ?>" href="javascript:;"><?php echo $qifeed_assets; ?></a></div>
																<a href="javascript:void(0);" data-assets="<?php echo $qifeed_assets ?>" data-assets-id="mtffi_assets" data-input-id="#mtfi_feedback_img,#mtfi_feedback_audio,#mtfi_feedback_rec_audio,#mtfi_feedback_video,#mtfi_feedback_rec_video,#mtfi_feedback_rec_screen" data-path="<?php echo $uploadpath ?>" class="delete_assets" title="Delete"><i class="fa fa-times" aria-hidden="true"></i></a>
															</div>
														</div>
														<div class="form-popup draggable" id="mtf_feedback_tts_model">
															<div class="popheading">Text to Speech</div>
															<div class="textarea">
																<textarea type="text" class="form-control1 inputtextWrap" name="mtf_feedback_text_to_speech" id="mtf_feedback_text_to_speech"><?php echo ($ques_type == 1 && ! empty($feedResult[0]['feedback']) && $feedResult[0]['feedback_type'] == 1) ? stripslashes($feedResult[0]['feed_speech_text']) : ''; ?></textarea>
															</div>
															<div class="submitRight1">
																<button type="button" class="btn1 submitbtn1" onclick="mtf_feedback_tts_close1()">Insert</button>
																<button type="button" class="btn1 submitbtn1" onclick="clearData('mtf_feedback_text_to_speech');">Clear</button>
																<button type="button" class="btn1 submitbtn1" onclick="mtf_feedback_tts_close1()">Cancel</button>
															</div>
														</div>
														<div class="form-popup draggable" id="mtfi_feedback_tts_model">
															<div class="popheading">Text to Speech</div>
															<div class="textarea">
																<textarea type="text" class="form-control1 inputtextWrap" name="mtfi_feedback_text_to_speech" id="mtfi_feedback_text_to_speech"><?php echo ($ques_type == 1 && ! empty($feedResult[1]['feedback']) && $feedResult[1]['feedback_type'] == 2) ? stripslashes($feedResult[1]['feed_speech_text']) : ''; ?></textarea>
															</div>
															<div class="submitRight1">
																<button type="button" class="btn1 submitbtn1" onclick="mtfi_feedback_tts_close1()">Insert</button>
																<button type="button" class="btn1 submitbtn1" onclick="clearData('mtfi_feedback_text_to_speech');">Clear</button>
																<button type="button" class="btn1 submitbtn1" onclick="mtfi_feedback_tts_close1()">Cancel</button>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
									
									<!--Sequance-Question-->
									<div id="SequanceQuestion" class="tab-pane fade <?php echo ($ques_type == 2) ? 'in active' : ''; ?>">
										<div class="match-Tab widthQ100">
											<ul class="nav nav-tabs">
												<li class="active"><a data-toggle="tab" href="#Sequence-Q">Question</a></li>
												<li><a data-toggle="tab" href="#Sequence-F">Feedback</a></li>
											</ul>
										</div>
										<div class="container-fluid">
											<div class="widthQ100 multitemp">
												<div class="tab-content">
													<div id="Sequence-Q" class="tab-pane fade in active">
														<div class="quesbox">
															<div class="row">
																<div class="col-sm-12 Questiontmpbanner Questiontmpbanner1">
																	<div class="critical_div">
																		<h3 class="qusHeading">QUESTION <p class="question-type">(Sequence)</p></h3>
																		<div class="CIRtiCaltext">
																			Critical Question: <input type="checkbox" name="sq_criticalQ" id="sq_criticalQ" class="form-control criticalQ" value="1" <?php echo ( ! empty($qresult['critical'])) ? 'checked="checked"' : ''; ?> />
																		</div>
																	</div>
																	<div class="quesbox-flex Sflex">
																		<div class="form-group">
																			<input type="text" class="form-control QuesForm inputtextWrap sq" name="sq" id="sq" placeholder="Question text goes here" value="<?php echo ( ! empty($qresult['questions'])) ? stripslashes($qresult['questions']) : ''; ?>" />
																			<div class="sqq_assets_container" <?php echo ( ! empty($ques_att) && $ques_type == 2) ? '' : 'style="display:none;"'; ?>>
																				<div class="assets_data sqq_assets_data"><a class="view_assets" data-src="<?php echo $path . $ques_att; ?>" href="javascript:;"><?php echo $ques_att; ?></a></div>
																				<a href="javascript:void(0);" data-assets="<?php echo $ques_att; ?>" data-assets-id="sqq_assets" data-input-id="#sqq_qaudio,#sqq_rec_audio" data-path="<?php echo $uploadpath ?>" class="delete_assets" title="Delete"><i class="fa fa-times" aria-hidden="true"></i></a>
																			</div>
																			<ul class="QueBoxIcon">
																				<li class="dropdown">
																					<button class="btn1 btn-secondary dropdown-toggle sqq_assets <?php echo ( ! empty($ques_att) && $ques_type == 2) ? 'disabled' : ''; ?>" <?php echo ( ! empty($ques_att) && $ques_type == 2) ? 'disabled="disabled"' : ''; ?> type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img class="img-fluid" src="img/list/audio.svg"></button>
																					<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
																						<a class="dropdown-item">
																							<div class="tooltipLeft uploadicon">
																								<label for="file-input-sqq-addAudio"><img class="img-fluid"  src="img/list/add_audio.svg" /><span class="tooltiptext">Add Audio</span></label>
																								<input id="file-input-sqq-addAudio" data-id="#sqq_qaudio" data-assets="sqq_assets" class="uploadAudioFile" type="file" name="sqq_Audio" />
																								<input type="hidden" name="sqq_qaudio" id="sqq_qaudio" value="<?php echo ($ques_type == 2 && $qatt_type == 'qa') ? $ques_att : ''; ?>"/>
																							</div>
																						</a>
																						<a class="dropdown-item">
																							<div class="tooltipLeft">
																								<img class="img-fluid rec-audio" data-input-id="sqq_rec_audio" data-assets="sqq_assets" src="img/list/record_audio.svg" />
																								<span class="tooltiptext">Record Audio</span>
																							</div>
																							<input type="hidden" name="sqq_rec_audio" id="sqq_rec_audio" />
																						 </a>
																						<a data-toggle="modal" data-target="#sqqtts" class="dropdown-item">
																							<div class="tooltipLeft">
																								<img class="img-fluid" src="img/list/text_speech.svg" />
																								<span class="tooltiptext">Text to Speech</span>
																							</div>
																						 </a>
																					 </div>
																				</li>
																			</ul>
																		</div>
																	</div>
																	<div class="q-icon-banner clearfix Sflex">
																		<ul class="QueBoxIcon QueBoxIcon1">
																			<h5>Add assets</h5>
																			<li class="dropdown">
																				<button class="btn1 btn-secondary dropdown-toggle sq_assets <?php echo ( ! empty($ques_assets) && $ques_type == 2) ? 'disabled' : ''; ?>" <?php echo ( ! empty($ques_assets) && $ques_type == 2) ? 'disabled="disabled"' : ''; ?> type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img class="img-fluid" src="img/list/audio.svg"></button>
																				<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
																					<a class="dropdown-item" data-toggle="modal" data-target="#Seqspech">
																						<div class="tooltip1">
																							<img class="img-fluid" src="img/list/text_speech.svg" />
																							<span class="tooltiptext">Text to Speech</span>
																						</div>
																					</a>
																					<a class="dropdown-item">
																						<div class="tooltip1 uploadicon">
																							<label for="file-input-sq-addAudio">
																								<img class="img-fluid" src="img/list/add_audio.svg" />
																								<span class="tooltiptext">Add Audio</span>
																							</label>
																							<input id="file-input-sq-addAudio" data-id="#sq_audio" data-assets="sq_assets" class="uploadAudioFile" type="file" name="sq_Audio"/>
																							<input type="hidden" name="sq_audio" id="sq_audio" value="<?php echo ($ques_type == 2 && $assets_type == 'a') ? $ques_assets : ''; ?>"/>
																						</div>
																					</a>
																					<a class="dropdown-item">
																						<div class="tooltip1">
																							<img class="img-fluid rec-audio" data-input-id="sq_rec_audio" data-assets="sq_assets" src="img/list/record_audio.svg" />
																							<span class="tooltiptext">Record Audio</span>
																						</div>
																						<input type="hidden" name="sq_rec_audio" id="sq_rec_audio" />
																					 </a>
																				</div>
																			</li>
																			<li class="dropdown">
																				<button class="btn1 btn-secondary dropdown-toggle sq_assets <?php echo ( ! empty($ques_assets) && $ques_type == 2) ? 'disabled' : ''; ?>" <?php echo ( ! empty($ques_assets) && $ques_type == 2) ? 'disabled="disabled"' : ''; ?> type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img class="img-fluid" src="img/list/video.svg"></button>
																				<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
																					<a class="dropdown-item">
																						<div class="tooltip1 uploadicon">
																							<label for="file-input-sq-addVideo">
																								<img class="img-fluid" src="img/list/add_video.svg" />
																								<span class="tooltiptext">Add Video</span>
																							</label>
																							<input id="file-input-sq-addVideo" data-id="#sq_video" data-assets="sq_assets" class="uploadVideoFile" type="file" name="sq_Video" />
																							<input type="hidden" name="sq_video" id="sq_video" value="<?php echo ($ques_type == 2 && $assets_type == 'v') ? $ques_assets : ''; ?>" />
																						</div>
																					</a>
																					<a class="dropdown-item">
																						<div class="tooltip1">
																							<img class="img-fluid rec-video" data-input-id="sq_rec_video" data-assets="sq_assets" src="img/qus_icon/rec_video.svg" />
																							<span class="tooltiptext">Record Video</span>
																						 </div>
																						 <input type="hidden" name="sq_rec_video" id="sq_rec_video" />
																					</a>
																					<a class="dropdown-item">
																						<div class="tooltip1">
																							<img class="img-fluid rec-screen" data-input-id="sq_rec_screen" data-assets="sq_assets" src="img/list/screen_record.svg" />
																							<span class="tooltiptext">Record Screen</span>
																						 </div>
																						 <input type="hidden" name="sq_rec_screen" id="sq_rec_screen" value="<?php echo ($ques_type == 2 && $assets_type == 's') ? $ques_assets : ''; ?>" />
																					</a>
																				</div>
																			</li>
																			<li>
																				<div class="tooltip uploadicon sq_assets <?php echo ( ! empty($ques_assets) && $ques_type == 2) ? 'disabled' : ''; ?>" <?php echo ( ! empty($ques_assets) && $ques_type == 2) ? 'disabled="disabled"' : ''; ?>>
																					<label for="file-input-sq-addImg">
																						<img class="img-fluid" src="img/list/image.svg" />
																						<span class="tooltiptext">Add Image</span>
																					</label>
																					<input id="file-input-sq-addImg" data-id="#sq_img" data-assets="sq_assets" class="uploadImgFile sq_assets" type="file" name="sq_Img" <?php echo ( ! empty($ques_assets) && $ques_type == 2) ? 'disabled="disabled"' : ''; ?>/>
																					<input type="hidden" name="sq_img" id="sq_img" value="<?php echo ($ques_type == 2 && $assets_type == 'i') ? $ques_assets : ''; ?>" />
																				</div>
																			</li>
																			<li>
																				<div class="tooltip uploadicon sq_assets <?php echo ( ! empty($ques_assets) && $ques_type == 2) ? 'disabled' : ''; ?>" <?php echo ( ! empty($ques_assets) && $ques_type == 2) ? 'disabled="disabled"' : ''; ?>>
																					<label for="file-input-sq-addDoc">
																						<img class="img-fluid" src="img/list/add_document.svg" />
																						<span class="tooltiptext">Add Document</span>
																					</label>
																					<input id="file-input-sq-addDoc" data-id="#sq_doc" data-assets="sq_assets" class="uploadDocFile sq_assets" type="file" name="sq_Doc"/>
																					<input type="hidden" name="sq_doc" id="sq_doc" value="<?php echo ($ques_type == 1 && $assets_type == 'd') ? $ques_assets : ''; ?>"/>
																				</div>
																			</li>
																		</ul>
																		<div class="Ques-comp">
																			<h5>Add Competency score</h5>
																			<?php if ( ! empty($sim_com['comp_col_1']) &&  ! empty($sim_com['comp_val_1'])): ?>
																			<div class="form-group QusScore">
																				<input type="text" class="form-control sq" name="sq_ques_val_1" id="sq_ques_val_1" onkeypress="return isNumberKey(event);" value="<?php echo ( ! empty($qresult['ques_val_1'])) ? $qresult['ques_val_1'] : '' ?>" />
																			</div>
																			<input type="hidden" class="form-control" id="sq_ques_val_1h" value="<?php echo ( ! empty($qresult['ques_val_1'])) ? $qresult['ques_val_1'] : '' ?>" />
																			<?php else: ?>
																			<input type="hidden" class="form-control" name="sq_ques_val_1" value="0" />
																			<?php endif; ?>
																			
																			<?php if ( ! empty($sim_com['comp_col_2']) &&  ! empty($sim_com['comp_val_2'])): ?>
																			<div class="form-group QusScore">
																				<input type="text" class="form-control sq" name="sq_ques_val_2" id="sq_ques_val_2" onkeypress="return isNumberKey(event);" value="<?php echo ( ! empty($qresult['ques_val_2'])) ? $qresult['ques_val_2'] : ''; ?>" />
																			</div>
																			<input type="hidden" class="form-control" id="sq_ques_val_2h" value="<?php echo ( ! empty($qresult['ques_val_2'])) ? $qresult['ques_val_2'] : ''; ?>" />
																			<?php else: ?>
																			<input type="hidden" class="form-control" name="sq_ques_val_2" value="0" />
																			<?php endif; ?>
																		
																			<?php if ( ! empty($sim_com['comp_col_3']) &&  ! empty($sim_com['comp_val_3'])): ?>
																			<div class="form-group QusScore">
																				<input type="text" class="form-control sq" name="sq_ques_val_3" id="sq_ques_val_3" onkeypress="return isNumberKey(event);" value="<?php echo ( ! empty($qresult['ques_val_3'])) ? $qresult['ques_val_3'] : ''; ?>" />
																			</div>
																			<input type="hidden" class="form-control" id="sq_ques_val_3h" value="<?php echo ( ! empty($qresult['ques_val_3'])) ? $qresult['ques_val_3'] : ''; ?>" />
																			<?php else: ?>
																			<input type="hidden" class="form-control" name="sq_ques_val_3" value="0" />
																			<?php endif; ?>
																		
																			<?php if ( ! empty($sim_com['comp_col_4']) &&  ! empty($sim_com['comp_val_4'])): ?>
																			<div class="form-group QusScore">
																				<input type="text" class="form-control sq" name="sq_ques_val_4" id="sq_ques_val_4" onkeypress="return isNumberKey(event);" value="<?php echo ( ! empty($qresult['ques_val_4'])) ? $qresult['ques_val_4'] : ''; ?>" />
																			</div>
																			<input type="hidden" class="form-control" id="sq_ques_val_4h" value="<?php echo ( ! empty($qresult['ques_val_4'])) ? $qresult['ques_val_4'] : ''; ?>" />
																			<?php else: ?>
																			<input type="hidden" class="form-control" name="sq_ques_val_4" value="0" />
																			<?php endif; ?>
																		
																			<?php if ( ! empty($sim_com['comp_col_5']) &&  ! empty($sim_com['comp_val_5'])): ?>
																			<div class="form-group QusScore">
																				<input type="text" class="form-control sq" name="sq_ques_val_5" id="sq_ques_val_5" onkeypress="return isNumberKey(event);" value="<?php echo ( ! empty($qresult['ques_val_5'])) ? $qresult['ques_val_5'] : ''; ?>" />
																			</div>
																			<input type="hidden" class="form-control" id="sq_ques_val_5h" value="<?php echo ( ! empty($qresult['ques_val_5'])) ? $qresult['ques_val_5'] : ''; ?>" />
																			<?php else: ?>
																			<input type="hidden" class="form-control" name="sq_ques_val_5" value="0" />
																			<?php endif; ?>
																		
																			<?php if ( ! empty($sim_com['comp_col_6']) &&  ! empty($sim_com['comp_val_6'])): ?>
																			<div class="form-group QusScore">
																				<input type="text" class="form-control sq" name="sq_ques_val_6" id="sq_ques_val_6" onkeypress="return isNumberKey(event);" value="<?php echo ( ! empty($qresult['ques_val_6'])) ? $qresult['ques_val_6'] : ''; ?>" />
																			</div>
																			<input type="hidden" class="form-control" id="sq_ques_val_6h" value="<?php echo ( ! empty($qresult['ques_val_6'])) ? $qresult['ques_val_6'] : ''; ?>" />
																			<?php else: ?>
																			<input type="hidden" class="form-control" name="sq_ques_val_6" value="0" />
																			<?php endif; ?>
																		</div>
																	</div>
																	<div class="sq_assets_container" <?php echo ( ! empty($ques_assets) && $ques_type == 2) ? '' : 'style="display:none;"'; ?>>
																		<div class="assets_data sq_assets_data"><a class="view_assets" data-src="<?php echo $path . $ques_assets; ?>" href="javascript:;"><?php echo $ques_assets; ?></a></div>
																		<a href="javascript:void(0);" data-assets="<?php echo $ques_assets ?>" data-assets-id="sq_assets" data-input-id="#sq_audio,#sq_rec_audio,#sq_video,#sq_rec_video,#sq_rec_screen,#sq_img,#sq_doc" data-path="<?php echo $uploadpath ?>" class="delete_assets" title="Delete"><i class="fa fa-times" aria-hidden="true"></i></a>
																	</div>
																</div>
															</div>
														</div>
														<div class="Ansbox">
															<div class="row">
																<div class="col-sm-12">
																	<h3 class="AnsHeading matcht">CHOICE</h3>
																	<div class="Check-box suffel suffe3">
																		<div class="tooltiptop"><span class="tooltiptext">Shuffle ON</span></div>
																		<label class="checkstyle"><input type="checkbox" name="sq_shuffle" class="suffleCheck" value="1" <?php echo ( ! empty($qresult['shuffle'])) ? 'checked="checked"' : ''; ?>><span class="checkmark"></span><span class="shuffleT"> Shuffle Option</span></label>
																	</div>
																	<div class="SeqApendOption">
																	<?php 
																	$sq_sql = "SELECT answer_id, choice_option FROM answer_tbl WHERE question_id = '". $qresult['question_id'] ."'";
																	$sq_res = $db->prepare($sq_sql); $sq_res->execute();
																	if ($sq_res->rowCount() > 0): $sqi = 1;
																	foreach ($sq_res->fetchAll(PDO::FETCH_ASSOC) as $sq_res_row): ?>
																	<div class="Option-box">
																		<div class="form-group formwidth">
																			<input type="hidden" name="updateSQAnswerid[]" value="<?php echo $sq_res_row['answer_id'] ?>" />
																			<input type="text" name="sq_ans[]" class="form-control inputtextWrap sq" placeholder="Option text goes here" value="<?php echo $sq_res_row['choice_option']; ?>" />
																		 </div>
																		<?php if ($sqi == 1): ?>
																		<div class="AddAns-Option"><img class="img-fluid SeqAdd" src="img/list/add.svg"></div>
																		<?php else: ?>
																		<div class="AddAns-Option removeOption" data-remove-answer-id="<?php echo $sq_res_row['answer_id'] ?>"><img class="img-fluid" src="img/list/delete_field.svg"></div>
																		<?php endif; ?>
																	</div>
																	<?php $sqi++; endforeach; else: ?>
																	<div class="Option-box">
																		<div class="form-group formwidth">
																			<input type="text" name="sq_ans[]" class="form-control inputtextWrap sq" placeholder="Option text goes here" />
																		 </div>
																		<div class="AddAns-Option"><img class="img-fluid SeqAdd" src="img/list/add.svg"></div>
																	</div>
																	<?php endif; ?>
																	</div>
																</div>
															</div>
														</div>
													</div>
													<div class="form-popup draggable Ques-pop-style modal" id="sqqtts">
														<div class="popheading">Text to Speech</div>
														<div class="textarea">
															<textarea type="text" class="form-control1 inputtextWrap" name="sqq_text_to_speech" id="sqq_text_to_speech"><?php echo ($ques_type == 2 && ! empty($qresult['qspeech_text'])) ? stripslashes($qresult['qspeech_text']) : ''; ?></textarea>
														</div>
														<div class="submitRight1">
															<button type="button" class="btn1 submitbtn1" data-dismiss="modal">Insert</button>
															<button type="button" class="btn1 submitbtn1" onclick="clearData('sqq_text_to_speech');">Clear</button>
															<button type="button" class="btn1 submitbtn1" data-dismiss="modal">Cancel</button>
														</div>
													</div>
													<div class="form-popup draggable Ques-pop-style modal" id="Seqspech">
														<div class="popheading">Insert Text to Speech</div>
														<div class="textarea">
															<textarea type="text" class="form-control1 inputtextWrap" name="sq_text_to_speech" id="sq_text_to_speech"><?php echo ($ques_type == 2 && ! empty($qresult['speech_text'])) ? stripslashes($qresult['speech_text']) : ''; ?></textarea>
														</div>
														<div class="modal-footer">
															<button type="button" class="btn1 submitbtn1" data-dismiss="modal">Insert</button>
															<button type="button" class="btn1 submitbtn1" onclick="clearData('sq_text_to_speech');">Clear</button>
															<button type="button" class="btn1 submitbtn1" data-dismiss="modal">Cancel</button>
														</div>
													</div>
													<div id="Sequence-F" class="tab-pane fade in">
														<div class="col-sm-12 Qusfeedback">
															<div class="form-group">
																<div class="feedback-flex">
																	<h4>Correct</h4>
																	<textarea class="form-control FeedForm inputtextWrap sq" name="sq_feedback" id="sq_feedback" rows="3" placeholder="That's right! You selected the correct response."><?php echo ($ques_type == 2 && ! empty($feedResult[0]['feedback'])) ? $feedResult[0]['feedback'] : ''; ?></textarea>
																</div>
																<ul class="QueBoxIcon feed LMfeed">
																	<h5>Add assets</h5>
																	<li class="dropdown">
																		<button class="btn1 btn-secondary dropdown-toggle sqf_assets <?php echo ( ! empty($qfeed_assets) && $ques_type == 2) ? 'disabled' : ''; ?>" <?php echo ( ! empty($qfeed_assets) && $ques_type == 2) ? 'disabled="disabled"' : ''; ?> type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img class="img-fluid" src="img/list/audio.svg"></button>
																		<div class="dropdown-menu buttom" aria-labelledby="dropdownMenuButton">
																			<a class="dropdown-item" data-toggle="modal" data-target="#C-Seqfeedspech">
																				<div class="tooltip1">
																					<img class="img-fluid" src="img/list/text_speech.svg" />
																					<span class="tooltiptext">Text to Speech</span>
																				</div>
																			</a>
																			<a class="dropdown-item">
																				<div class="tooltip1 uploadicon">
																					<label for="file-input-sq-feedback-addAudio">
																						<img class="img-fluid" src="img/list/add_audio.svg" />
																						<span class="tooltiptext">Add Audio</span>
																					</label>
																					<input id="file-input-sq-feedback-addAudio" data-id="#sq_feedback_audio" data-assets="sqf_assets" class="uploadAudioFile" type="file" name="sq_feedback_Audio"/>
																					<input type="hidden" name="sq_feedback_audio" id="sq_feedback_audio" value="<?php echo ($ques_type == 2 && $feed_assets_type == 'fa') ? $qfeed_assets : ''; ?>" />
																				</div>
																			</a>
																			<a class="dropdown-item">
																				<div class="tooltip1">
																					<img class="img-fluid rec-audio" data-input-id="sq_feedback_rec_audio" data-assets="sqf_assets" src="img/list/record_audio.svg" />
																					<span class="tooltiptext">Record Audio</span>
																				</div>
																				<input type="hidden" name="sq_feedback_rec_audio" id="sq_feedback_rec_audio" />
																			 </a>
																		</div>
																	</li>
																	<li class="dropdown">
																		<button class="btn1 btn-secondary dropdown-toggle sqf_assets <?php echo ( ! empty($qfeed_assets) && $ques_type == 2) ? 'disabled' : ''; ?>" <?php echo ( ! empty($qfeed_assets) && $ques_type == 2) ? 'disabled="disabled"' : ''; ?> type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img class="img-fluid" src="img/list/video.svg"></button>
																		<div class="dropdown-menu buttom" aria-labelledby="dropdownMenuButton">
																			<a class="dropdown-item">
																				<div class="tooltip1 uploadicon">
																					<label for="file-input-sq-feedback-addVideo">
																						<img class="img-fluid" src="img/list/add_video.svg" />
																						<span class="tooltiptext">Add Video</span>
																					</label>
																					<input id="file-input-sq-feedback-addVideo" data-id="#sq_feedback_video" data-assets="sqf_assets" class="uploadVideoFile" type="file" name="sq_feedback_Video"/>
																					<input type="hidden" name="sq_feedback_video" id="sq_feedback_video" value="<?php echo ($ques_type == 2 && $feed_assets_type == 'fv') ? $qfeed_assets : ''; ?>" />
																				</div>
																			</a>
																			<a class="dropdown-item">
																				<div class="tooltip1">
																					<img class="img-fluid rec-video" data-input-id="sq_feedback_rec_video" data-assets="sqf_assets" src="img/qus_icon/rec_video.svg" />
																					<span class="tooltiptext">Record Video</span>
																				 </div>
																				 <input type="hidden" name="sq_feedback_rec_video" id="sq_feedback_rec_video" />
																			</a>
																			<a class="dropdown-item">
																				<div class="tooltip1">
																					<img class="img-fluid rec-screen" data-input-id="sq_feedback_rec_screen" data-assets="sqf_assets" src="img/list/screen_record.svg" />
																					<span class="tooltiptext">Record Screen</span>
																				 </div>
																				 <input type="hidden" name="sq_feedback_rec_screen" id="sq_feedback_rec_screen" value="<?php echo ($ques_type == 2 && $feed_assets_type == 'fs') ? $qfeed_assets : ''; ?>" />
																			</a>
																		</div>
																	</li>
																	<li>
																		<div class="tooltip uploadicon sqf_assets <?php echo ( ! empty($qfeed_assets) && $ques_type == 2) ? 'disabled' : ''; ?>" <?php echo ( ! empty($qfeed_assets) && $ques_type == 2) ? 'disabled="disabled"' : ''; ?>>
																			<label for="file-input-sq-feedback-addImg">
																				<img class="img-fluid" src="img/list/image.svg" />
																				<span class="tooltiptext">Add Image</span>
																			</label>
																			<input id="file-input-sq-feedback-addImg" data-id="#sq_feedback_img" data-assets="sqf_assets" class="uploadImgFile sqf_assets" type="file" name="sq_feedback_Img" <?php echo ( ! empty($qfeed_assets) && $ques_type == 2) ? 'disabled="disabled"' : ''; ?>/>
																			<input type="hidden" name="sq_feedback_img" id="sq_feedback_img" value="<?php echo ($ques_type == 2 && $feed_assets_type == 'fi') ? $qfeed_assets : ''; ?>" />
																		</div>
																	</li>
																	<li>
																		<div class="tooltip uploadicon sqf_assets <?php echo ( ! empty($qfeed_assets) && $ques_type == 2) ? 'disabled' : ''; ?>" <?php echo ( ! empty($qfeed_assets) && $ques_type == 2) ? 'disabled="disabled"' : ''; ?>>
																			<label for="file-input-sq-feedback-addDoc">
																				<img class="img-fluid" src="img/list/add_document.svg" />
																				<span class="tooltiptext">Add Document</span>
																			</label>
																			<input id="file-input-sq-feedback-addDoc" data-id="#sq_feedback_doc" data-assets="sqf_assets" class="uploadDocFile sqf_assets" type="file" name="sq_feedback_Doc" <?php echo ( ! empty($qfeed_assets) && $ques_type == 2) ? 'disabled="disabled"' : ''; ?>/>
																			<input type="hidden" name="sq_feedback_doc" id="sq_feedback_doc" value="<?php echo ($ques_type == 2 && $feed_assets_type == 'fd') ? $qfeed_assets : ''; ?>" />
																		</div>
																	</li>
																</ul>
															</div>                                                    
															<div class="sqf_assets_container" <?php echo ( ! empty($qfeed_assets) && $ques_type == 2) ? '' : 'style="display:none;"'; ?>>
																<div class="assets_data sqf_assets_data"><a class="view_assets" data-src="<?php echo $path . $qfeed_assets; ?>" href="javascript:;"><?php echo $qfeed_assets; ?></a></div>
																<a href="javascript:void(0);" data-assets="<?php echo $qfeed_assets ?>" data-assets-id="sqf_assets" data-input-id="#sq_feedback_audio,#sq_feedback_rec_audio,#sq_feedback_video,#sq_feedback_rec_video,#sq_feedback_rec_screen,#sq_feedback_img" data-path="<?php echo $uploadpath ?>" class="delete_assets" title="Delete"><i class="fa fa-times" aria-hidden="true"></i></a>
															</div>
															<div class="form-group">
																<div class="feedback-flex">
																	<h4>Incorrect</h4>
																	<textarea class="form-control FeedForm inputtextWrap sq" name="sq_ifeedback" id="sq_ifeedback" rows="3" placeholder="You did not select the correct response."><?php echo ($ques_type == 2 && ! empty($feedResult[1]['feedback'])) ? $feedResult[1]['feedback'] : ''; ?></textarea>
																</div>
																<ul class="QueBoxIcon feed LMfeed">
																	<h5>Add assets</h5>
																	<li class="dropdown">
																		<button class="btn1 btn-secondary dropdown-toggle sqfi_assets <?php echo ( ! empty($qifeed_assets) && $ques_type == 2) ? 'disabled' : ''; ?>" <?php echo ( ! empty($qifeed_assets) && $ques_type == 2) ? 'disabled="disabled"' : ''; ?> type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img class="img-fluid" src="img/list/audio.svg"></button>
																		<div class="dropdown-menu buttom" aria-labelledby="dropdownMenuButton">
																			<a class="dropdown-item" data-toggle="modal" data-target="#I-Seqfeedspech">
																				<div class="tooltip1">
																					<img class="img-fluid" src="img/list/text_speech.svg" />
																					<span class="tooltiptext">Text to Speech</span>
																				</div>
																			</a>
																			<a class="dropdown-item">
																				<div class="tooltip1 uploadicon">
																					<label for="file-input-sqi-feedback-addAudio">
																						<img class="img-fluid" src="img/list/add_audio.svg" />
																						<span class="tooltiptext">Add Audio</span>
																					</label>
																					<input id="file-input-sqi-feedback-addAudio" data-id="#sqi_feedback_audio" data-assets="sqfi_assets" class="uploadAudioFile" type="file" name="sqi_feedback_Audio" />
																					<input type="hidden" name="sqi_feedback_audio" id="sqi_feedback_audio" value="<?php echo ($ques_type == 2 && $feed_assets_type == 'fa') ? $qifeed_assets : ''; ?>" />
																				</div>
																			</a>
																			<a class="dropdown-item">
																				<div class="tooltip1">
																					<img class="img-fluid rec-audio" data-input-id="sqi_feedback_rec_audio" data-assets="sqfi_assets" src="img/list/record_audio.svg" />
																					<span class="tooltiptext">Record Audio</span>
																				</div>
																				<input type="hidden" name="sqi_feedback_rec_audio" id="sqi_feedback_rec_audio" />
																			 </a>
																		</div>
																	</li>
																	<li class="dropdown">
																		<button class="btn1 btn-secondary dropdown-toggle sqfi_assets <?php echo ( ! empty($qifeed_assets) && $ques_type == 2) ? 'disabled' : ''; ?>" <?php echo ( ! empty($qifeed_assets) && $ques_type == 2) ? 'disabled="disabled"' : ''; ?> type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img class="img-fluid" src="img/list/video.svg"></button>
																		<div class="dropdown-menu buttom" aria-labelledby="dropdownMenuButton">
																			<a class="dropdown-item">
																				<div class="tooltip1 uploadicon">
																					<label for="file-input-sqi-feedback-addVideo">
																						<img class="img-fluid" src="img/list/add_video.svg" />
																						<span class="tooltiptext">Add Video</span>
																					</label>
																					<input id="file-input-sqi-feedback-addVideo" data-id="#sqi_feedback_video" data-assets="sqfi_assets" class="uploadVideoFile" type="file" name="sqi_feedback_Video" />
																					<input type="hidden" name="sqi_feedback_video" id="sqi_feedback_video" value="<?php echo ($ques_type == 2 && $ifeed_assets_type == 'fv') ? $qifeed_assets : ''; ?>" />
																				</div>
																			</a>
																			<a class="dropdown-item">
																				<div class="tooltip1">
																					<img class="img-fluid rec-video" data-input-id="sqi_feedback_rec_video" data-assets="sqfi_assets" src="img/qus_icon/rec_video.svg" />
																					<span class="tooltiptext">Record Video</span>
																				 </div>
																				 <input type="hidden" name="sqi_feedback_rec_video" id="sqi_feedback_rec_video" />
																			</a>
																			<a class="dropdown-item">
																				<div class="tooltip1">
																					<img class="img-fluid rec-screen" data-input-id="sqi_feedback_rec_screen" data-assets="sqfi_assets" src="img/list/screen_record.svg" />
																					<span class="tooltiptext">Record Screen</span>
																				 </div>
																				 <input type="hidden" name="sqi_feedback_rec_screen" id="sqi_feedback_rec_screen" value="<?php echo ($ques_type == 2 && $ifeed_assets_type == 'fs') ? $qifeed_assets : ''; ?>" />
																			</a>
																		</div>
																	</li>
																	<li>
																		<div class="tooltip uploadicon sqfi_assets <?php echo ( ! empty($qifeed_assets) && $ques_type == 2) ? 'disabled' : ''; ?>" <?php echo ( ! empty($qifeed_assets) && $ques_type == 2) ? 'disabled="disabled"' : ''; ?>>
																			<label for="file-input-sqi-feedback-addImg">
																				<img class="img-fluid" src="img/list/image.svg" />
																				<span class="tooltiptext">Add Image</span>
																			</label>
																			<input id="file-input-sqi-feedback-addImg" data-id="#sqi_feedback_img" data-assets="sqfi_assets" class="uploadImgFile sqfi_assets" type="file" name="sqi_feedback_Img" <?php echo ( ! empty($qifeed_assets) && $ques_type == 2) ? 'disabled="disabled"' : ''; ?>/>
																			<input type="hidden" name="sqi_feedback_img" id="sqi_feedback_img" value="<?php echo ($ques_type == 2 && $feed_assets_type == 'fi') ? $qifeed_assets : ''; ?>" />
																		</div>
																	</li>
																	<li>
																		<div class="tooltip uploadicon sqfi_assets <?php echo ( ! empty($qifeed_assets) && $ques_type == 2) ? 'disabled' : ''; ?>" <?php echo ( ! empty($qifeed_assets) && $ques_type == 2) ? 'disabled="disabled"' : ''; ?>>
																			<label for="file-input-sqi-feedback-addDoc">
																				<img class="img-fluid" src="img/list/add_document.svg" />
																				<span class="tooltiptext">Add Document</span>
																			</label>
																			<input id="file-input-sqi-feedback-addDoc" data-id="#sqi_feedback_doc" data-assets="sqfi_assets" class="uploadDocFile sqfi_assets" type="file" name="sqi_feedback_Doc" <?php echo ( ! empty($qifeed_assets) && $ques_type == 2) ? 'disabled="disabled"' : ''; ?>/>
																			<input type="hidden" name="sqi_feedback_doc" id="sqi_feedback_doc" value="<?php echo ($ques_type == 2 && $ifeed_assets_type == 'fd') ? $qifeed_assets : ''; ?>" />
																		</div>
																	</li>
																</ul>
															</div>
															<div class="sqfi_assets_container" <?php echo ( ! empty($qifeed_assets) && $ques_type == 2) ? '' : 'style="display:none;"'; ?>>
																<div class="assets_data sqfi_assets_data"><a class="view_assets" data-src="<?php echo $path . $qifeed_assets; ?>" href="javascript:;"><?php echo $qifeed_assets; ?></a></div>
																<a href="javascript:void(0);" data-assets="<?php echo $qifeed_assets ?>" data-assets-id="sqfi_assets" data-input-id="#sqi_feedback_audio,#sqi_ieedback_rec_audio,#sqi_feedback_video,#sqi_feedback_rec_video,#sqi_feedback_rec_screen,#sqi_feedback_img" data-path="<?php echo $uploadpath ?>" class="delete_assets" title="Delete"><i class="fa fa-times" aria-hidden="true"></i></a>
															</div>
														</div>
														<div class="form-popup draggable Ques-pop-style modal" id="C-Seqfeedspech">
															<div class="popheading">Insert Text to Speech</div>
															<div class="textarea">
																<textarea type="text" class="form-control1 inputtextWrap" name="sq_feedback_text_to_speech" id="sq_feedback_text_to_speech"><?php echo ($ques_type == 2 && ! empty($feedResult[0]['feedback']) && $feedResult[0]['feedback_type'] == 1) ? stripslashes($feedResult[0]['feed_speech_text']) : ''; ?></textarea>
															</div>
															<div class="modal-footer">
																<button type="button" class="btn1 submitbtn1" data-dismiss="modal">Insert</button>
																<button type="button" class="btn1 submitbtn1" onclick="clearData('sq_feedback_text_to_speech');">Clear</button>
																<button type="button" class="btn1 submitbtn1" data-dismiss="modal">Cancel</button>
															</div>
														</div>
														<div class="form-popup draggable Ques-pop-style modal" id="I-Seqfeedspech">
															<div class="popheading">Insert Text to Speech</div>
															<div class="textarea">
																<textarea type="text" class="form-control inputtextWrap" name="sqi_feedback_text_to_speech" id="sqi_feedback_text_to_speech"><?php echo ($ques_type == 1 && ! empty($feedResult[1]['feedback']) && $feedResult[1]['feedback_type'] == 2) ? stripslashes($feedResult[1]['feed_speech_text']) : ''; ?></textarea>
															</div>
															<div class="modal-footer">
																<button type="button" class="btn1 submitbtn1" data-dismiss="modal">Insert</button>
																<button type="button" class="btn1 submitbtn1" onclick="clearData('sqi_feedback_text_to_speech');">Clear</button>
																<button type="button" class="btn1 submitbtn1" data-dismiss="modal">Cancel</button>
															</div>
														</div>
													 </div>
												</div>
											</div>
										</div>
									</div>
									
									<!--Sorting-Question-->
									<div id="DregDropQuestion" class="tab-pane fade <?php echo ($ques_type == 3) ? 'in active' : ''; ?>">
										<div class="match-Tab widthQ100">
											<ul class="nav nav-tabs">
												<li class="active"><a data-toggle="tab" href="#Dshorting-Q">Question</a></li>
												<li><a data-toggle="tab" href="#Dshorting-F">Feedback</a></li>
											 </ul>
										</div>
										<div class="container-fluid">
											<div class="widthQ100 multitemp">
												<div class="tab-content">
												<div id="Dshorting-Q" class="tab-pane fade in active">
													<div class="quesbox">
														<div class="row">
															<div class="col-sm-12 Questiontmpbanner">
																<div class="critical_div">
																	<h3 class="qusHeading">QUESTION <p class="question-type">(Sorting)</p></h3>
																	<div class="CIRtiCaltext">
																		Critical Question: <input type="checkbox" name="sort_criticalQ" id="sort_criticalQ" class="form-control criticalQ" value="1" <?php echo ( ! empty($qresult['critical'])) ? 'checked="checked"' : ''; ?> />
																	</div>
																</div>
																<div class="quesbox-flex">
																	<div class="form-group">
																		<input type="text" name="sortq" id="sortq" class="form-control QuesForm inputtextWrap sortq" placeholder="Click here to enter question" value="<?php echo ( ! empty($qresult['questions'])) ? stripslashes($qresult['questions']) : ''; ?>" />
																		<div class="sortqq_assets_container" <?php echo ( ! empty($ques_att) && $ques_type == 3) ? '' : 'style="display:none;"'; ?>>
																			<div class="assets_data sortqq_assets_data"><a class="view_assets" data-src="<?php echo $path . $ques_att; ?>" href="javascript:;"><?php echo $ques_att; ?></a></div>
																			<a href="javascript:void(0);" data-assets="<?php echo $ques_att; ?>" data-assets-id="sortqq_assets" data-input-id="#sortqq_qaudio,#sortqq_rec_audio" data-path="<?php echo $uploadpath ?>" class="delete_assets" title="Delete"><i class="fa fa-times" aria-hidden="true"></i></a>
																		</div>
																		<ul class="QueBoxIcon">
																			<li class="dropdown">
																				<button class="btn1 btn-secondary dropdown-toggle sortqq_assets <?php echo ( ! empty($ques_att) && $ques_type == 3) ? 'disabled' : ''; ?>" <?php echo ( ! empty($ques_att) && $ques_type == 3) ? 'disabled="disabled"' : ''; ?> type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img class="img-fluid" src="img/list/audio.svg"></button>
																				<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
																					<a class="dropdown-item">
																						<div class="tooltipLeft uploadicon">
																							<label for="file-input-sortqq-addAudio"><img class="img-fluid" src="img/list/add_audio.svg" /><span class="tooltiptext">Add Audio</span></label>
																							<input id="file-input-sortqq-addAudio" data-id="#sortqq_qaudio" data-assets="sortqq_assets" class="uploadAudioFile" type="file" name="sortqq_Audio" />
																							<input type="hidden" name="sortqq_qaudio" id="sortqq_qaudio" value="<?php echo ($ques_type == 3 && $qatt_type == 'qa') ? $ques_att : ''; ?>" />
																						</div>
																					</a>
																					<a class="dropdown-item">
																						<div class="tooltipLeft">
																							<img class="img-fluid rec-audio" data-input-id="sortqq_rec_audio" data-assets="sortqq_assets" src="img/list/record_audio.svg" />
																							<span class="tooltiptext">Record Audio</span>
																						</div>
																						<input type="hidden" name="sortqq_rec_audio" id="sortqq_rec_audio" />
																					 </a>
																					<a data-toggle="modal" data-target="#sortqqtts" class="dropdown-item">
																						<div class="tooltipLeft">
																							<img class="img-fluid" src="img/list/text_speech.svg" />
																							<span class="tooltiptext">Text to Speech</span>
																						</div>
																					 </a>
																				 </div>
																			</li>
																		</ul>
																	 </div>
																</div>
																<div class="q-icon-banner clearfix">
																	<ul class="QueBoxIcon QueBoxIcon1">
																		<h5>Add assets</h5>
																		<li class="dropdown">
																			<button class="btn1 btn-secondary dropdown-toggle sortq_assets <?php echo ( ! empty($ques_assets) && $ques_type == 3) ? 'disabled' : ''; ?>" <?php echo ( ! empty($ques_assets) && $ques_type == 3) ? 'disabled="disabled"' : ''; ?> type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img class="img-fluid" src="img/list/audio.svg"></button>
																			<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
																				<a data-toggle="modal" data-target="#sortqtts" class="dropdown-item">
																					<div class="tooltip1">
																						<img class="img-fluid" src="img/list/text_speech.svg">
																						<span class="tooltiptext">Text to Speech</span>
																					</div>
																				</a>
																				<a class="dropdown-item">
																					<div class="tooltip1 uploadicon">
																						<label for="file-input-sortq-addAudio">
																							<img class="img-fluid" src="img/list/add_audio.svg">
																							<span class="tooltiptext">Add Audio</span>
																						</label>
																						<input id="file-input-sortq-addAudio" data-id="#sortq_audio" data-assets="sortq_assets" class="uploadAudioFile" type="file" name="sortq_Audio"/>
																						<input type="hidden" name="sortq_audio" id="sortq_audio" value="<?php echo ($ques_type == 3 && $assets_type == 'a') ? $ques_assets : ''; ?>"/>
																					</div>
																				</a>
																				<a class="dropdown-item">
																					<div class="tooltip1">
																						<img class="img-fluid rec-audio" data-input-id="sortq_rec_audio" data-assets="sortq_assets" src="img/list/record_audio.svg">
																						<span class="tooltiptext">Record Audio</span>
																					</div>
																					<input type="hidden" name="sortq_rec_audio" id="sortq_rec_audio" />
																				 </a>
																			</div>
																		</li>
																		<li class="dropdown">
																			<button class="btn1 btn-secondary dropdown-toggle sortq_assets <?php echo ( ! empty($ques_assets) && $ques_type == 3) ? 'disabled' : ''; ?>" <?php echo ( ! empty($ques_assets) && $ques_type == 3) ? 'disabled="disabled"' : ''; ?> type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img class="img-fluid" src="img/list/video.svg"></button>
																			<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
																				<a class="dropdown-item">
																					<div class="tooltip1 uploadicon">
																						<label for="file-input-sortq-addVideo">
																							<img class="img-fluid" src="img/list/add_video.svg">
																							<span class="tooltiptext">Add Video</span>
																						</label>
																						<input id="file-input-sortq-addVideo" data-id="#sortq_video" data-assets="sortq_assets" class="uploadVideoFile" type="file" name="sortq_Video" />
																						<input type="hidden" name="sortq_video" id="sortq_video"  value="<?php echo ($ques_type == 3 && $assets_type == 'v') ? $ques_assets : ''; ?>"/>
																					</div>
																				</a>
																				<a class="dropdown-item">
																					<div class="tooltip1">
																						<img class="img-fluid rec-video" data-input-id="sortq_rec_video" data-assets="sortq_assets" src="img/qus_icon/rec_video.svg">
																						<span class="tooltiptext">Record Video</span>
																					 </div>
																					 <input type="hidden" name="sortq_rec_video" id="sortq_rec_video" />
																				</a>
																				<a class="dropdown-item">
																					<div class="tooltip1">
																						<img class="img-fluid rec-screen" data-input-id="sortq_rec_screen" data-assets="sortq_assets" src="img/list/screen_record.svg">
																						<span class="tooltiptext">Record Screen</span>
																					 </div>
																					 <input type="hidden" name="sortq_rec_screen" id="sortq_rec_screen" value="<?php echo ($ques_type == 3 && $assets_type == 's') ? $ques_assets : ''; ?>"/>
																				</a>
																			</div>
																		</li>
																		<li>
																			<div class="tooltip uploadicon sortq_assets <?php echo ( ! empty($ques_assets) && $ques_type == 3) ? 'disabled' : ''; ?>" <?php echo ( ! empty($ques_assets) && $ques_type == 3) ? 'disabled="disabled"' : ''; ?>>
																				<label for="file-input-sortq-addImg">
																					<img class="img-fluid" src="img/list/image.svg">
																					<span class="tooltiptext">Add Image</span>
																				</label>
																				<input id="file-input-sortq-addImg" data-id="#sortq_img"  data-assets="sortq_assets" class="uploadImgFile sortq_assets" type="file" name="sortq_Img" <?php echo ( ! empty($ques_assets) && $ques_type == 3) ? 'disabled="disabled"' : ''; ?> />
																				<input type="hidden" name="sortq_img" id="sortq_img" value="<?php echo ($ques_type == 3 && $assets_type == 'i') ? $ques_assets : ''; ?>" />
																			</div>
																		</li>
																		<li>
																			<div class="tooltip uploadicon sortq_assets <?php echo ( ! empty($ques_assets) && $ques_type == 3) ? 'disabled' : ''; ?>" <?php echo ( ! empty($ques_assets) && $ques_type == 3) ? 'disabled="disabled"' : ''; ?>>
																				<label for="file-input-sortq-addDoc">
																					<img class="img-fluid" src="img/list/add_document.svg" />
																					<span class="tooltiptext">Add Document</span>
																				</label>
																				<input id="file-input-sortq-addDoc" data-id="#sortq_doc" data-assets="sortq_assets" class="uploadDocFile sortq_assets" type="file" name="sortq_Doc" />
																				<input type="hidden" name="sortq_doc" id="sortq_doc" value="<?php echo ($ques_type == 3 && $assets_type == 'd') ? $ques_assets : ''; ?>"/>
																			</div>
																		</li>
																	</ul>                                                               
																	<div class="Ques-comp">
																		<h5>Add Competency score</h5>
																		<?php if ( ! empty($sim_com['comp_col_1']) &&  ! empty($sim_com['comp_val_1'])): ?>
																		<div class="form-group QusScore">
																			<input type="text" class="form-control sortq" name="sort_ques_val_1" id="sort_ques_val_1" onkeypress="return isNumberKey(event);"  value="<?php echo $qresult['ques_val_1'] ?>" />
																		</div>
																		<input type="hidden" class="form-control" id="sort_ques_val_1h" value="<?php echo $qresult['ques_val_1'] ?>" />
																		<?php else: ?>
																		<input type="hidden" class="form-control" name="sort_ques_val_1" value="0" />
																		<?php endif; ?>
																		
																		<?php if ( ! empty($sim_com['comp_col_2']) &&  ! empty($sim_com['comp_val_2'])): ?>
																		<div class="form-group QusScore">
																			<input type="text" class="form-control sortq" name="sort_ques_val_2" id="sort_ques_val_2" onkeypress="return isNumberKey(event);" value="<?php echo $qresult['ques_val_2'] ?>" />
																		</div>
																		<input type="hidden" class="form-control" id="sort_ques_val_2h" value="<?php echo $qresult['ques_val_2'] ?>" />
																		<?php else: ?>
																		<input type="hidden" class="form-control" name="sort_ques_val_2" value="0" />
																		<?php endif; ?>
																		
																		<?php if ( ! empty($sim_com['comp_col_3']) &&  ! empty($sim_com['comp_val_3'])): ?>
																		<div class="form-group QusScore">
																			<input type="text" class="form-control sortq" name="sort_ques_val_3" id="sort_ques_val_3" onkeypress="return isNumberKey(event);"  value="<?php echo $qresult['ques_val_3'] ?>" />
																		</div>
																		<input type="hidden" class="form-control" id="sort_ques_val_3h" value="<?php echo $qresult['ques_val_3'] ?>" />
																		<?php else: ?>
																		<input type="hidden" class="form-control" name="sort_ques_val_3" value="0" />
																		<?php endif; ?>
																		
																		<?php if ( ! empty($sim_com['comp_col_4']) &&  ! empty($sim_com['comp_val_4'])): ?>
																		<div class="form-group QusScore">
																			<input type="text" class="form-control sortq" name="sort_ques_val_4" id="sort_ques_val_4" onkeypress="return isNumberKey(event);" value="<?php echo $qresult['ques_val_4'] ?>" />
																		</div>
																		<input type="hidden" class="form-control" id="sort_ques_val_4h" value="<?php echo $qresult['ques_val_4'] ?>" />
																		<?php else: ?>
																		<input type="hidden" class="form-control" name="sort_ques_val_4" value="0" />
																		<?php endif; ?>
																		
																		<?php if ( ! empty($sim_com['comp_col_5']) &&  ! empty($sim_com['comp_val_5'])): ?>
																		<div class="form-group QusScore">
																			<input type="text" class="form-control sortq" name="sort_ques_val_5" id="sort_ques_val_5" onkeypress="return isNumberKey(event);" value="<?php echo $qresult['ques_val_5'] ?>" />
																		</div>
																		<input type="hidden" class="form-control" id="sort_ques_val_5h" value="<?php echo $qresult['ques_val_5'] ?>" />
																		<?php else: ?>
																		<input type="hidden" class="form-control" name="sort_ques_val_5" value="0" />
																		<?php endif; ?>
																		
																		<?php if ( ! empty($sim_com['comp_col_6']) &&  ! empty($sim_com['comp_val_6'])): ?>
																		<div class="form-group QusScore">
																			<input type="text" class="form-control sortq" name="sort_ques_val_6" id="sort_ques_val_6" onkeypress="return isNumberKey(event);" value="<?php echo $qresult['ques_val_6'] ?>" />
																		</div>
																		<input type="hidden" class="form-control" id="sort_ques_val_6h" value="<?php echo $qresult['ques_val_6'] ?>" />
																		<?php else: ?>
																		<input type="hidden" class="form-control" name="sort_ques_val_6" value="0" />
																		<?php endif; ?>
																	</div>
																</div>
																<div class="sortq_assets_container" <?php echo ( ! empty($ques_assets) && $ques_type == 3) ? '' : 'style="display:none;"'; ?>>
																	<div class="assets_data sortq_assets_data"><a class="view_assets" data-src="<?php echo $path . $ques_assets; ?>" href="javascript:;"><?php echo $ques_assets; ?></a></div>
																	<a href="javascript:void(0);" data-assets="<?php echo $ques_assets ?>" data-assets-id="sortq_assets" data-input-id="#sortq_audio,#sortq_rec_audio,#sortq_video,#sortq_rec_video,#sortq_rec_screen,#sortq_img" data-path="<?php echo $uploadpath ?>" class="delete_assets" title="Delete"><i class="fa fa-times" aria-hidden="true"></i></a>
																</div>
															 </div>
														</div>
													</div>
													<div class="form-popup draggable Ques-pop-style modal" id="sortqqtts">
														<div class="popheading">Insert Text to Speech</div>
														<div class="textarea">
															<textarea type="text" class="form-control1 inputtextWrap" name="sortqq_text_to_speech" id="sortqq_text_to_speech"><?php echo ($ques_type == 3 && ! empty($qresult['qspeech_text'])) ? stripslashes($qresult['qspeech_text']) : ''; ?></textarea>
														</div>
														<div class="modal-footer">
															<button type="button" class="btn1 submitbtn1" data-dismiss="modal">Insert</button>
															<button type="button" class="btn1 submitbtn1" onclick="clearData('sortqq_text_to_speech');">Clear</button>
															<button type="button" class="btn1 submitbtn1" data-dismiss="modal">Cancel</button>
														</div>
													</div>
													<div class="form-popup draggable Ques-pop-style modal" id="sortqfitts">
														<div class="popheading">Insert Text to Speech</div>
														<div class="textarea">
															<textarea type="text" class="form-control1 inputtextWrap" name="sortqi_feedback_text_to_speech" id="sortqi_feedback_text_to_speech"><?php echo ($ques_type == 3 && ! empty($feedResult[1]['feedback']) && $feedResult[1]['feedback_type'] == 2) ? stripslashes($feedResult[1]['feed_speech_text']) : ''; ?></textarea>
														</div>
														<div class="modal-footer">
															<button type="button" class="btn1 submitbtn1" data-dismiss="modal">Insert</button>
															<button type="button" class="btn1 submitbtn1" onclick="clearData('sortqi_feedback_text_to_speech');">Clear</button>
															<button type="button" class="btn1 submitbtn1" data-dismiss="modal">Cancel</button>
														</div>
													</div>
													<div class="Ansbox">
														<div class="row">
															<div class="col-sm-12">
																<h3 class="AnsHeading">SORTING ITEM</h3>
																<div class="Check-box suffel suffe3">
																	<div class="tooltiptop"><span class="tooltiptext">Shuffle ON</span></div>
																	<label class="checkstyle">
																		<input type="checkbox" name="sort_shuffle" class="suffleCheck" value="1" <?php echo ( ! empty($qresult['shuffle'])) ? 'checked="checked"' : ''; ?>>
																		<span class="checkmark"></span><span class="shuffleT">Shuffle Option</span>
																	</label>
																</div>
																<div class="ApendShortOption-banner shortINGinput">
																<?php 
																$sort_sql = "SELECT answer_id, choice_option, match_option, match_sorting_item, image FROM answer_tbl WHERE question_id = '". $qresult['question_id'] ."'";
																$sort_res = $db->prepare($sort_sql); $sort_res->execute();
																if ($sort_res->rowCount() > 0): $sorti = 1; $sortop = 0;
																foreach ($sort_res->fetchAll(PDO::FETCH_ASSOC) as $sort_res_row): ?>
																<div class="ApendShortOption-box">
																	<input type="hidden" name="updateSortAnswerid[]" value="<?php echo $sort_res_row['answer_id'] ?>" />
																	<div class="shortbox2">
																		<span class="QuesNo"><?php echo $sorti; ?></span>
																		<div class="form-group">
																			<input type="text" name="sortq_sorting_items[]" id="sortq_sorting_items" class="form-control inputtextWrap sortq" placeholder="Sorting text goes here" value="<?php echo stripslashes($sort_res_row['choice_option']); ?>" />
																			<ul class="QueBoxIcon">
																				<li>
																					<div class="tooltip uploadicon sortq_item_assets<?php echo $sorti ?> <?php echo ( ! empty($sort_res_row['image']) && file_exists($root_path . $sort_res_row['image']) && $ques_type == 3) ? 'disabled' : ''; ?>" <?php echo ( ! empty($sort_res_row['image']) && file_exists($root_path . $sort_res_row['image']) && $ques_type == 3) ? 'disabled="disabled"' : ''; ?>>
																						<label for="file-input-sortq-item-addImg<?php echo $sorti ?>">
																							<img class="img-fluid" src="img/list/image.svg" />
																							<span class="tooltiptext">Add Image</span>
																						</label>
																						<input id="file-input-sortq-item-addImg<?php echo $sorti ?>" data-id="#sortq_item_img<?php echo $sorti ?>" data-assets="<?php echo $sorti ?>" class="uploadImgFileSort sortq_item_assets<?php echo $sorti ?> <?php echo ( ! empty($sort_res_row['image']) && file_exists($root_path . $sort_res_row['image']) && $ques_type == 3) ? 'disabled' : ''; ?>" type="file" name="sortq_item_Img<?php echo $sorti ?>" <?php echo ( ! empty($sort_res_row['image']) && file_exists($root_path . $sort_res_row['image']) && $ques_type == 3) ? 'disabled="disabled"' : ''; ?> />
																						<input type="hidden" name="sortq_item_img[]" id="sortq_item_img<?php echo $sorti ?>" value="<?php echo ( ! empty($sort_res_row['image']) && $ques_type == 3) ? $sort_res_row['image'] : ''; ?>" />
																					</div>                                                    
																				</li>
																			</ul>
																			<div class="sortq_items_assets_container_<?php echo $sorti ?>" id="sortq_items_assets" <?php echo ( ! empty($sort_res_row['image']) && file_exists($root_path . $sort_res_row['image']) && $ques_type == 3) ? '' : 'style="display:none;"'; ?>>
																				<div class="assets_data sortq_items_assets_data_<?php echo $sorti ?>"><?php echo ( ! empty($sort_res_row['image']) && file_exists($root_path . $sort_res_row['image']) && $ques_type == 3) ? '<a class="view_assets" data-src="'. $path . $sort_res_row['image']. '" href="javascript:;">'. $sort_res_row['image'] .'</a>' : '<a class="view_assets" data-src="" href="javascript:;"></a>'; ?></div>
																				<a href="javascript:void(0);" data-assets="<?php echo ( ! empty($sort_res_row['image']) && file_exists($root_path . $sort_res_row['image']) && $ques_type == 3) ? $sort_res_row['image'] : ''; ?>" data-assets-id="sortq_item_assets<?php echo $sorti ?>" data-input-id="#sortq_item_img<?php echo $sorti ?>" data-path="<?php echo $uploadpath ?>" class="delete_assets" title="Delete"><i class="fa fa-times" aria-hidden="true"></i></a>
																			</div>
																		</div>
																		<?php if ($sorti == 1): ?>
																			<div class="AddAns-Option"><img class="img-fluid ShortAddICON" src="img/list/add_field.svg" /></div>
																		<?php else: ?>
																			<div class="shortMinus removeSortingOption" data-remove-answer-id="<?php echo $sort_res_row['answer_id'] ?>"><img class="img-fluid" src="img/list/delete_field.svg" /></div>
																		<?php endif; ?>
																	</div>
																	<div class="SA-choicebox ShortOptions_<?php echo $sorti; ?>">
																		<h3 class="AnsHeading dragH">Add Correct Drag Option</h3>
																		<?php $sortop_sql = "SELECT sub_options_id, answer_id, sub_option FROM sub_options_tbl WHERE answer_id = '". $sort_res_row['answer_id'] ."'";
																		$sortop_res = $db->prepare($sortop_sql); $sortop_res->execute();
																		if ($sortop_res->rowCount() > 0): $sortid = 0;
																		foreach ($sortop_res->fetchAll(PDO::FETCH_ASSOC) as $sort_drop_row): ?>
																		<div class="choicebox ShortAC ShortAC_<?php echo $sorti; ?>">
																			<input type="hidden" name="updateSubOptionid[<?php echo $sortop; ?>][]" value="<?php echo $sort_drop_row['sub_options_id'] ?>" />
																			<div class="form-group">
																				<input type="text" name="sortq_drag_items[<?php echo $sortop; ?>][option][]" id="sortq_drag_items" class="form-control inputtextWrap sortq" placeholder="Drag text goes here" value="<?php echo $sort_drop_row['sub_option']; ?>">
																			</div>
																			<?php if ($sortid == 0): ?>
																			<div class="AddAns-Option"><img class="img-fluid ShortDragAddICON" data-sortq-option="<?php echo $sorti; ?>" data-sortq-suboption="<?php echo $sortop; ?>" src="img/list/add_field.svg" /></div>
																			<?php else: ?>
																			<div class="AddAns-Option"><img class="img-fluid removeSortingSubOption" data-remove-answer-id="<?php echo $sort_drop_row['sub_options_id'] ?>" src="img/list/delete_field.svg" /></div>
																			<?php endif; ?>
																		</div>
																		<?php $sortid++; endforeach; endif; ?>
																	</div>
																</div>
																<?php $sorti++; $sortop++; endforeach; else: ?>
																<div class="ApendShortOption-box">
																	<div class="shortbox2">
																		<span class="QuesNo">1</span>
																		<div class="form-group">
																			<input type="text" name="sortq_sorting_items[]" id="sortq_sorting_items" class="form-control inputtextWrap sortq" placeholder="Sorting text goes here" />
																			<ul class="QueBoxIcon">
																			<li>
																				<div class="tooltip uploadicon sortq_item_assets1">
																					<label for="file-input-sortq-item-addImg1">
																						<img class="img-fluid" src="img/list/image.svg" />
																						<span class="tooltiptext">Add Image</span>
																					</label>
																					<input id="file-input-sortq-item-addImg1" data-id="#sortq_item_img1" data-assets="1" class="uploadImgFileSort sortq_item_assets1" type="file" name="sortq_item_Img1" />
																					<input type="hidden" name="sortq_item_img[]" id="sortq_item_img1" />
																				</div>                                                    
																			</li>
																		</ul>
																			<div class="sortq_items_assets_container_1" id="sortq_items_assets" style="display:none;">
																				<div class="assets_data sortq_items_assets_data_1"><a class="view_assets" data-src="" href="javascript:;"></a></div>
																				<a href="javascript:void(0);" data-assets="" data-assets-id="sortq_item_assets1" data-input-id="#sortq_item_img1" data-path="<?php echo $uploadpath ?>" class="delete_assets" title="Delete"><i class="fa fa-times" aria-hidden="true"></i></a>
																			</div>
																		</div>
																		<div class="AddAns-Option"><img class="img-fluid ShortAddICON" src="img/list/add_field.svg" /></div>                                                                  
																	</div>
																	<div class="SA-choicebox ShortOptions_1">
																		<h3 class="AnsHeading dragH">Add Correct Drag Option</h3>
																		<div class="choicebox ShortAC ShortAC_1">
																			<div class="form-group">
																				<input type="text" name="sortq_drag_items[0][option][]" id="sortq_drag_items" class="form-control inputtextWrap sortq" placeholder="Drag text goes here" />
																			</div>
																			<div class="AddAns-Option">
																				<img class="img-fluid ShortDragAddICON" data-sortq-option="1" data-sortq-suboption="0" src="img/list/add_field.svg" />
																			</div>
																		</div>
																	</div>
																 </div>
																 <?php endif; ?>
																</div>
															</div>
														</div>
													 </div>
												</div>
												<div id="Dshorting-F" class="tab-pane fade in">
													<div class="col-sm-12 Qusfeedback">
														<div class="form-group">
															<div class="feedback-flex">
																<h4>Correct</h4>
																<textarea class="form-control FeedForm inputtextWrap sortq" name="sortq_cfeedback" id="sortq_cfeedback" placeholder="That's right! You selected the correct response."><?php echo ($ques_type == 3 && ! empty($feedResult[0]['feedback']) && $feedResult[0]['feedback_type'] == 1) ? stripslashes($feedResult[0]['feedback']) : ''; ?></textarea>
															</div>
															<ul class="QueBoxIcon feed LMfeed">
																<h5>Add assets</h5>
																<li class="dropdown">
																	<button class="btn1 btn-secondary dropdown-toggle sortqf_assets <?php echo ( ! empty($qfeed_assets) && $ques_type == 3) ? 'disabled' : ''; ?>" <?php echo ( ! empty($qfeed_assets) && $ques_type == 3) ? 'disabled="disabled"' : ''; ?> type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img class="img-fluid" src="img/list/audio.svg"></button>
																	<div class="dropdown-menu buttom" aria-labelledby="dropdownMenuButton">
																	   <a data-toggle="modal" data-target="#sortqfctts" class="dropdown-item">
																			<div class="tooltip1">
																				<img class="img-fluid" src="img/list/text_speech.svg">
																				<span class="tooltiptext">Text to Speech</span>
																			</div>
																		</a>
																		<a class="dropdown-item">
																			<div class="tooltip1 uploadicon">
																				<label for="file-input-sortq-feedback-addAudio">
																					<img class="img-fluid" src="img/list/add_audio.svg">
																					<span class="tooltiptext">Add Audio</span>
																				</label>
																				<input id="file-input-sortq-feedback-addAudio" data-id="#sortq_feedback_audio" data-assets="sortqf_assets" class="uploadAudioFile" type="file" name="sortq_feedback_Audio"/>
																				<input type="hidden" name="sortq_feedback_audio" id="sortq_feedback_audio" value="<?php echo ($ques_type == 3 && $feed_assets_type == 'fa') ? $qfeed_assets : ''; ?>"/>
																			</div>
																		</a>
																		<a class="dropdown-item">
																			<div class="tooltip1">
																				<img class="img-fluid rec-audio" data-input-id="sortq_feedback_rec_audio" data-assets="sortqf_assets" src="img/list/record_audio.svg"/>
																				<span class="tooltiptext">Record Audio</span>
																			</div>
																			<input type="hidden" name="sortq_feedback_rec_audio" id="sortq_feedback_rec_audio" />
																		 </a>
																	</div>
																</li>
																<li class="dropdown">
																	<button class="btn1 btn-secondary dropdown-toggle sortqf_assets <?php echo ( ! empty($qfeed_assets) && $ques_type == 3) ? 'disabled' : ''; ?>" <?php echo ( ! empty($qfeed_assets) && $ques_type == 3) ? 'disabled="disabled"' : ''; ?> type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img class="img-fluid" src="img/list/video.svg"></button>
																	<div class="dropdown-menu buttom" aria-labelledby="dropdownMenuButton">
																		<a class="dropdown-item">
																			<div class="tooltip1 uploadicon">
																				<label for="file-input-sortq-feedback-addVideo">
																					<img class="img-fluid" src="img/list/add_video.svg" />
																					<span class="tooltiptext">Add Video</span>
																				</label>
																				<input id="file-input-sortq-feedback-addVideo" data-id="#sortq_feedback_video" data-assets="sortqf_assets" class="uploadVideoFile" type="file" name="sortq_feedback_Video" />
																				<input type="hidden" name="sortq_feedback_video" id="sortq_feedback_video" value="<?php echo ($ques_type == 3 && $feed_assets_type == 'fv') ? $qfeed_assets : ''; ?>" />
																			</div>
																		</a>
																		<a class="dropdown-item">
																			<div class="tooltip1">
																				<img class="img-fluid rec-video" data-input-id="sortq_feedback_rec_video" data-assets="sortqf_assets" src="img/qus_icon/rec_video.svg" />
																				<span class="tooltiptext">Record Video</span>
																			 </div>
																			 <input type="hidden" name="sortq_feedback_rec_video" id="sortq_feedback_rec_video" />
																		</a>
																		<a class="dropdown-item">
																			<div class="tooltip1">
																				<img class="img-fluid rec-screen" data-input-id="sortq_feedback_rec_screen" data-assets="sortqf_assets" src="img/list/screen_record.svg" />
																				<span class="tooltiptext">Record Screen</span>
																			 </div>
																			 <input type="hidden" name="sortq_feedback_rec_screen" id="sortq_feedback_rec_screen" value="<?php echo ($ques_type == 3 && $feed_assets_type == 'fs') ? $qfeed_assets : ''; ?>" />
																		</a>
																	</div>
																</li>
																<li>
																	<div class="tooltip uploadicon sortqf_assets <?php echo ( ! empty($qfeed_assets) && $ques_type == 3) ? 'disabled' : ''; ?>" <?php echo ( ! empty($qfeed_assets) && $ques_type == 3) ? 'disabled="disabled"' : ''; ?>>
																		<label for="file-input-sortq-feedback-addImg">
																			<img class="img-fluid" src="img/list/image.svg" />
																			<span class="tooltiptext">Add Image</span>
																		</label>
																		<input id="file-input-sortq-feedback-addImg" data-id="#sortq_feedback_img" data-assets="sortqf_assets" class="uploadImgFile sortqf_assets" type="file" name="sortq_feedback_Img" <?php echo ( ! empty($qfeed_assets) && $ques_type == 3) ? 'disabled="disabled"' : ''; ?> />
																		<input type="hidden" name="sortq_feedback_img" id="sortq_feedback_img" value="<?php echo ($ques_type == 3 && $feed_assets_type == 'fi') ? $qfeed_assets : ''; ?>" />
																	</div>
																</li>
																<li>
																	<div class="tooltip uploadicon sortqf_assets <?php echo ( ! empty($qfeed_assets) && $ques_type == 3) ? 'disabled' : ''; ?>" <?php echo ( ! empty($qfeed_assets) && $ques_type == 3) ? 'disabled="disabled"' : ''; ?>>
																		<label for="file-input-sortq-feedback-addDoc">
																			<img class="img-fluid" src="img/list/add_document.svg"/>
																			<span class="tooltiptext">Add Document</span>
																		</label>
																		<input id="file-input-sortq-feedback-addDoc" data-id="#sortq_feedback_doc" data-assets="sortqf_assets" class="uploadDocFile sortqf_assets" type="file" name="sortq_feedback_Doc" <?php echo ( ! empty($qfeed_assets) && $ques_type == 3) ? 'disabled="disabled"' : ''; ?> />
																		<input type="hidden" name="sortq_feedback_doc" id="sortq_feedback_doc" value="<?php echo ($ques_type == 4 && $feed_assets_type == 'fd') ? $qfeed_assets : ''; ?>" />
																	</div>
																</li>
															</ul>
														</div>
														<div class="sortqf_assets_container" <?php echo ( ! empty($qfeed_assets) && $ques_type == 3) ? '' : 'style="display:none;"'; ?>>
															<div class="assets_data sortqf_assets_data"><a class="view_assets" data-src="<?php echo $path . $qfeed_assets; ?>" href="javascript:;"><?php echo $qfeed_assets; ?></a></div>
															<a href="javascript:void(0);" data-assets="<?php echo $qfeed_assets ?>" data-assets-id="sortqf_assets" data-input-id="#sortq_feedback_audio,#sortq_feedback_rec_audio,#sortq_feedback_video,#sortq_feedback_rec_video,#sortq_feedback_rec_screen,#sortq_feedback_img" data-path="<?php echo $uploadpath ?>" class="delete_assets" title="Delete"><i class="fa fa-times" aria-hidden="true"></i></a>
														</div>
														<div class="form-group">
															<div class="feedback-flex">
																<h4>Incorrect</h4>
																<textarea class="form-control FeedForm inputtextWrap sortq" name="sortq_ifeedback" id="sortq_ifeedback" placeholder="You did not select the correct response."><?php echo ($ques_type == 3 && ! empty($feedResult[1]['feedback']) && $feedResult[1]['feedback_type'] == 2) ? stripslashes($feedResult[1]['feedback']) : ''; ?></textarea>
															</div>
															<ul class="QueBoxIcon feed LMfeed">
																<h5>Add assets</h5>
																<li class="dropdown">
																	<button class="btn1 btn-secondary dropdown-toggle sortqfi_assets <?php echo ( ! empty($qifeed_assets) && $ques_type == 3) ? 'disabled' : ''; ?>" <?php echo ( ! empty($qifeed_assets) && $ques_type == 3) ? 'disabled="disabled"' : ''; ?> type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img class="img-fluid" src="img/list/audio.svg"></button>
																	<div class="dropdown-menu buttom" aria-labelledby="dropdownMenuButton">
																		<a data-toggle="modal" data-target="#sortqfitts" class="dropdown-item">
																			<div class="tooltip1">
																				<img class="img-fluid" src="img/list/text_speech.svg" />
																				<span class="tooltiptext">Text to Speech</span>
																			</div>
																		</a>
																		<a class="dropdown-item">
																			<div class="tooltip1 uploadicon">
																				<label for="file-input-sortqi-feedback-addAudio">
																					<img class="img-fluid" src="img/list/add_audio.svg">
																					<span class="tooltiptext">Add Audio</span>
																				</label>
																				<input id="file-input-sortqi-feedback-addAudio" data-id="#sortqi_feedback_audio" data-assets="sortqfi_assets" class="uploadAudioFile" type="file" name="sortqi_feedback_Audio" />
																				<input type="hidden" name="sortqi_feedback_audio" id="sortqi_feedback_audio" value="<?php echo ($ques_type == 3 && $feed_assets_type == 'fa') ? $qifeed_assets : ''; ?>" />
																			</div>
																		</a>
																		<a class="dropdown-item">
																			<div class="tooltip1">
																				<img class="img-fluid rec-audio" data-input-id="sortqi_feedback_rec_audio" data-assets="sortqfi_assets" src="img/list/record_audio.svg" />
																				<span class="tooltiptext">Record Audio</span>
																			</div>
																			<input type="hidden" name="sortqi_feedback_rec_audio" id="sortqi_feedback_rec_audio" />
																		 </a>
																	</div>
																</li>
																<li class="dropdown">
																	<button class="btn1 btn-secondary dropdown-toggle sortqfi_assets <?php echo ( ! empty($qifeed_assets) && $ques_type == 3) ? 'disabled' : ''; ?>" <?php echo ( ! empty($qifeed_assets) && $ques_type == 3) ? 'disabled="disabled"' : ''; ?> type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img class="img-fluid" src="img/list/video.svg"></button>
																	<div class="dropdown-menu buttom" aria-labelledby="dropdownMenuButton">
																		<a class="dropdown-item">
																			<div class="tooltip1 uploadicon">
																				<label for="file-input-sortqi-feedback-addVideo">
																					<img class="img-fluid" src="img/list/add_video.svg" />
																					<span class="tooltiptext">Add Video</span>
																				</label>
																				<input id="file-input-sortqi-feedback-addVideo" data-id="#sortqi_feedback_video" data-assets="sortqfi_assets" class="uploadVideoFile" type="file" name="sortqi_feedback_Video" />
																				<input type="hidden" name="sortqi_feedback_video" id="sortqi_feedback_video" value="<?php echo ($ques_type == 3 && $feed_assets_type == 'fv') ? $qifeed_assets : ''; ?>" />
																			</div>
																		</a>
																		<a class="dropdown-item">
																			<div class="tooltip1">
																				<img class="img-fluid rec-video" data-input-id="sortqi_feedback_rec_video" data-assets="sortqfi_assets" src="img/qus_icon/rec_video.svg" />
																				<span class="tooltiptext">Record Video</span>
																			 </div>
																			 <input type="hidden" name="sortqi_feedback_rec_video" id="sortqi_feedback_rec_video" />
																		</a>
																		<a class="dropdown-item">
																			<div class="tooltip1">
																				<img class="img-fluid rec-screen" data-input-id="sortqi_feedback_rec_screen" data-assets="sortqfi_assets" src="img/list/screen_record.svg" />
																				<span class="tooltiptext">Record Screen</span>
																			 </div>
																			 <input type="hidden" name="sortqi_feedback_rec_screen" id="sortqi_feedback_rec_screen" value="<?php echo ($ques_type == 3 && $feed_assets_type == 'fs') ? $qifeed_assets : ''; ?>" />
																		</a>
																	</div>
																</li>
																<li>
																	<div class="tooltip uploadicon sortqfi_assets <?php echo ( ! empty($qifeed_assets) && $ques_type == 3) ? 'disabled' : ''; ?>" <?php echo ( ! empty($qifeed_assets) && $ques_type == 3) ? 'disabled="disabled"' : ''; ?>>
																		<label for="file-input-sortqi-feedback-addImg">
																			<img class="img-fluid" src="img/list/image.svg" />
																			<span class="tooltiptext">Add Image</span>
																		</label>
																		<input id="file-input-sortqi-feedback-addImg" data-id="#sortqi_feedback_img" data-assets="sortqfi_assets" class="uploadImgFile sortqfi_assets" type="file" name="sortqi_feedback_Img" <?php echo ( ! empty($qifeed_assets) && $ques_type == 3) ? 'disabled="disabled"' : ''; ?> />
																		<input type="hidden" name="sortqi_feedback_img" id="sortqi_feedback_img" value="<?php echo ($ques_type == 4 && $feed_assets_type == 'fi') ? $qifeed_assets : ''; ?>" />
																	</div>
																</li>
																<li>
																	<div class="tooltip uploadicon sortqfi_assets <?php echo ( ! empty($qifeed_assets) && $ques_type == 3) ? 'disabled' : ''; ?>" <?php echo ( ! empty($qifeed_assets) && $ques_type == 3) ? 'disabled="disabled"' : ''; ?>>
																		<label for="file-input-sortqi-feedback-addDoc">
																			<img class="img-fluid" src="img/list/add_document.svg" />
																			<span class="tooltiptext">Add Document</span>
																		</label>
																		<input id="file-input-sortqi-feedback-addDoc" data-id="#sortqi_feedback_doc" data-assets="sortqfi_assets" class="uploadDocFile sortqfi_assets" type="file" name="sortqi_feedback_Doc" <?php echo ( ! empty($qifeed_assets) && $ques_type == 3) ? 'disabled="disabled"' : ''; ?>/>
																		<input type="hidden" name="sortqi_feedback_doc" id="sortqi_feedback_doc" value="<?php echo ($ques_type == 3 && $feed_assets_type == 'fd') ? $qifeed_assets : ''; ?>" />
																	</div>
																</li>
															</ul>
														</div>
														<div class="sortqfi_assets_container" <?php echo ( ! empty($qifeed_assets) && $ques_type == 3) ? '' : 'style="display:none;"'; ?>>
															<div class="assets_data sortqfi_assets_data"><a class="view_assets" data-src="<?php echo $path . $qifeed_assets; ?>" href="javascript:;"><?php echo $qifeed_assets; ?></a></div>
															<a href="javascript:void(0);" data-assets="<?php echo $qifeed_assets ?>" data-assets-id="sortqfi_assets" data-input-id="#sortqi_feedback_audio,#sortqi_feedback_rec_audio,#sortqi_feedback_video,#sortqi_feedback_rec_video,#sortqi_feedback_rec_screen,#sortqi_feedback_img" data-path="<?php echo $uploadpath ?>" class="delete_assets" title="Delete"><i class="fa fa-times" aria-hidden="true"></i></a>
														</div>
													</div>                                                
													<div class="form-popup draggable Ques-pop-style modal" id="sortqtts">
														<div class="popheading">Insert Text to Speech</div>
														<div class="textarea">
															<textarea type="text" class="form-control1 inputtextWrap" name="sortq_text_to_speech" id="sortq_text_to_speech"><?php echo ($ques_type == 3 && ! empty($qresult['speech_text'])) ? stripslashes($qresult['speech_text']) : ''; ?></textarea>
														</div>
														<div class="modal-footer">
															<button type="button" class="btn1 submitbtn1" data-dismiss="modal">Insert</button>
															<button type="button" class="btn1 submitbtn1" onclick="clearData('sortq_text_to_speech');">Clear</button>
															<button type="button" class="btn1 submitbtn1" data-dismiss="modal">Cancel</button>
														</div>
													</div>
													<div class="form-popup draggable Ques-pop-style modal" id="sortqfctts">
														<div class="popheading">Insert Text to Speech</div>
														<div class="textarea">
															<textarea type="text" class="form-control1 inputtextWrap" name="sortq_feedback_text_to_speech" id="sortq_feedback_text_to_speech"><?php echo ($ques_type == 3 && ! empty($feedResult[0]['feedback']) && $feedResult[0]['feedback_type'] == 1) ? stripslashes($feedResult[0]['feed_speech_text']) : ''; ?></textarea>
														</div>
														<div class="modal-footer">
															<button type="button" class="btn1 submitbtn1" data-dismiss="modal">Insert</button>
															<button type="button" class="btn1 submitbtn1" onclick="clearData('sortq_feedback_text_to_speech');">Clear</button>
															<button type="button" class="btn1 submitbtn1" data-dismiss="modal">Cancel</button>
														</div>
													</div>
												 </div>
											   </div>
											</div>
										</div>
									</div>
									
									<!--DND-Question-->
									<div id="DNDQuestion" class="tab-pane fade <?php echo ($ques_type == 8) ? 'in active' : ''; ?>">
										<div class="match-Tab widthQ100">
											<ul class="nav nav-tabs">
												<li class="active"><a data-toggle="tab" href="#DAD-Q">Question</a></li>
												<li><a data-toggle="tab" href="#DAD-F">Feedback</a></li>
											</ul>
										</div>
										<div class="container-fluid">
											<div class="widthQ100 multitemp">
												<div class="tab-content">
													<div id="DAD-Q" class="match-Q tab-pane fade in active">
														<input type="hidden" name="seleted_drag_option" id="seleted_drag_option" value="<?php echo ( ! empty($qresult['seleted_drag_option'])) ? $qresult['seleted_drag_option'] : 1 ?>" />
														<ul class="DNDOptionbanner nav nav-tabs">
															<?php if ( ! empty($qresult['seleted_drag_option']) && $qresult['seleted_drag_option'] == 1): ?>
															<li class="Radio-box Dragradio tab1">
																<label class="radiostyle rad_S1">
																	<input type="radio" name="dragoption" class="dragoption" value="1" checked />
																	<span class="radiomark"></span>
																	<div class="DD01 tooltiptop"><img class="img-fluid" src="img/list/D&D01.svg" /><span class="tooltiptexttop">Multiple Drop</span></div>
																</label>
															</li>
															<?php elseif ( empty($qresult['seleted_drag_option'])): ?>
															<li class="Radio-box Dragradio tab1">
																<label class="radiostyle rad_S1">
																	<input type="radio" name="dragoption" class="dragoption" value="1" checked />
																	<span class="radiomark"></span>
																	<div class="DD01 tooltiptop"><img class="img-fluid" src="img/list/D&D01.svg" /><span class="tooltiptexttop">Multiple Drop</span></div>
																</label>
															</li>
															<?php endif; 
															if ( ! empty($qresult['seleted_drag_option']) && $qresult['seleted_drag_option'] == 2): ?>
															<li class="Radio-box Dragradio tab2">
																<label class="radiostyle rad_S1">
																	<input type="radio" name="dragoption" class="dragoption" value="2" checked />
																	<span class="radiomark"></span>
																	<div class="DD02 tooltiptop"><img class="img-fluid" src="img/list/D&D02.svg" /><span class="tooltiptexttop">Single Drop</span></div>
																</label>
															</li>
															<?php elseif ( empty($qresult['seleted_drag_option'])): ?>
															<li class="Radio-box Dragradio tab2">
																<label class="radiostyle rad_S1">
																	<input type="radio" name="dragoption" class="dragoption" value="2" />
																	<span class="radiomark"></span>
																	<div class="DD02 tooltiptop"><img class="img-fluid" src="img/list/D&D02.svg" /><span class="tooltiptexttop">single drop</span></div>
																</label>
															</li>
															<?php endif; ?>
														</ul>
														<div>
															<div class="quesbox">
																<div class="row">
																	<div class="col-sm-12 Questiontmpbanner">
																		<div class="critical_div">
																			<h3 class="qusHeading">QUESTION <p class="question-type">(Drag &amp; Drop)</p></h3>
																			<div class="CIRtiCaltext">
																				Critical Question: <input type="checkbox" name="ddq_criticalQ" id="ddq_criticalQ" class="form-control criticalQ" value="1" <?php echo ( ! empty($qresult['critical'])) ? 'checked="checked"' : ''; ?> />
																			</div>
																		</div>
																		<div class="quesbox-flex">
																			<div class="form-group">
																				<input type="text" name="ddq" id="ddq" class="form-control QuesForm inputtextWrap ddq" placeholder="Click here to enter question" value="<?php echo ( ! empty($qresult['questions'])) ? stripslashes($qresult['questions']) : ''; ?>" />
																				<div class="ddqq_assets_container" <?php echo ( ! empty($ques_att) && $ques_type == 8) ? '' : 'style="display:none;"'; ?>>
																					<div class="assets_data ddqq_assets_data"><a class="view_assets" data-src="<?php echo $path . $ques_att; ?>" href="javascript:;"><?php echo $ques_att; ?></a></div>
																					<a href="javascript:void(0);" data-assets="<?php echo $ques_att; ?>" data-assets-id="ddqq_assets" data-input-id="#ddqq_qaudio,#ddqq_rec_audio" data-path="<?php echo $uploadpath ?>" class="delete_assets" title="Delete"><i class="fa fa-times" aria-hidden="true"></i></a>
																				</div>
																				<ul class="QueBoxIcon">
																					<li class="dropdown">
																						<button class="btn1 btn-secondary dropdown-toggle ddqq_assets <?php echo ( ! empty($ques_att) && $ques_type == 8) ? 'disabled' : ''; ?>" <?php echo ( ! empty($ques_att) && $ques_type == 8) ? 'disabled="disabled"' : ''; ?> type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img class="img-fluid" src="img/list/audio.svg"></button>
																						<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
																							<a class="dropdown-item">
																								<div class="tooltipLeft uploadicon">
																									<label for="file-input-ddqq-addAudio"><img class="img-fluid"  src="img/list/add_audio.svg" /><span class="tooltiptext">Add Audio</span></label>
																									<input id="file-input-ddqq-addAudio" data-id="#ddqq_qaudio" data-assets="ddqq_assets" class="uploadAudioFile" type="file" name="ddqq_Audio" />
																									<input type="hidden" name="ddqq_qaudio" id="ddqq_qaudio" value="<?php echo ($ques_type == 8 && $qatt_type == 'qa') ? $ques_att : ''; ?>"/>
																								</div>
																							</a>
																							<a class="dropdown-item">
																								<div class="tooltipLeft">
																									<img class="img-fluid rec-audio" data-input-id="ddqq_rec_audio" data-assets="ddqq_assets" src="img/list/record_audio.svg" />
																									<span class="tooltiptext">Record Audio</span>
																								</div>
																								<input type="hidden" name="ddqq_rec_audio" id="ddqq_rec_audio" />
																							 </a>
																							<a data-toggle="modal" data-target="#ddqqtts" class="dropdown-item">
																								<div class="tooltipLeft">
																									<img class="img-fluid" src="img/list/text_speech.svg" />
																									<span class="tooltiptext">Text to Speech</span>
																								</div>
																							 </a>
																						 </div>
																					</li>
																				</ul>
																			</div>
																		</div>
																		<div class="q-icon-banner clearfix">
																			<ul class="QueBoxIcon QueBoxIcon1">
																				<h5>Add assets</h5>
																				<li class="dropdown">
																					<button class="btn1 btn-secondary dropdown-toggle ddq_assets <?php echo ( ! empty($ques_assets) && $ques_type == 8) ? 'disabled' : ''; ?>" <?php echo ( ! empty($ques_assets) && $ques_type == 8) ? 'disabled="disabled"' : ''; ?> type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img class="img-fluid" src="img/list/audio.svg"></button>
																					<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
																						<a data-toggle="modal" data-target="#ddqtts" class="dropdown-item">
																							<div class="tooltip1">
																								<img class="img-fluid" src="img/list/text_speech.svg">
																								<span class="tooltiptext">Text to Speech</span>
																							</div>
																						</a>
																						<a class="dropdown-item">
																							<div class="tooltip1 uploadicon">
																								<label for="file-input-ddq-addAudio">
																									<img class="img-fluid" src="img/list/add_audio.svg" />
																									<span class="tooltiptext">Add Audio</span>
																								</label>
																								<input id="file-input-ddq-addAudio" data-id="#ddq_qaudio" data-assets="ddq_assets" class="uploadAudioFile" type="file" name="ddq_Audio" />
																								<input type="hidden" name="ddq_qaudio" id="ddq_qaudio" value="<?php echo ($ques_type == 8 && $assets_type == 'a') ? $ques_assets : ''; ?>" />
																							</div>
																						</a>
																						<a class="dropdown-item">
																							<div class="tooltip1">
																								<img class="img-fluid rec-audio" data-input-id="ddq_rec_audio" data-assets="ddq_assets" src="img/list/record_audio.svg" />
																								<span class="tooltiptext">Record Audio</span>
																							</div>
																							<input type="hidden" name="ddq_rec_audio" id="ddq_rec_audio" />
																						 </a>
																					</div>
																				</li>
																				<li class="dropdown">
																					<button class="btn1 btn-secondary dropdown-toggle ddq_assets <?php echo ( ! empty($ques_assets) && $ques_type == 8) ? 'disabled' : ''; ?>" <?php echo ( ! empty($ques_assets) && $ques_type == 8) ? 'disabled="disabled"' : ''; ?> type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img class="img-fluid" src="img/list/video.svg"></button>
																					<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
																						<a class="dropdown-item">
																							<div class="tooltip1 uploadicon">
																								<label for="file-input-ddq-addVideo">
																									<img class="img-fluid" src="img/list/add_video.svg" />
																									<span class="tooltiptext">Add Video</span>
																								</label>
																								<input id="file-input-ddq-addVideo" data-id="#ddq_video" data-assets="ddq_assets" class="uploadVideoFile" type="file" name="ddq_Video" />
																								<input type="hidden" name="ddq_video" id="ddq_video" value="<?php echo ($ques_type == 8 && $assets_type == 'v') ? $ques_assets : ''; ?>" />
																							</div>
																						</a>
																						<a class="dropdown-item">
																							<div class="tooltip1">
																								<img class="img-fluid rec-video" data-input-id="ddq_rec_video" data-assets="ddq_assets" src="img/qus_icon/rec_video.svg" />
																								<span class="tooltiptext">Record Video</span>
																							 </div>
																							 <input type="hidden" name="ddq_rec_video" id="ddq_rec_video" />
																						</a>
																						<a class="dropdown-item">
																							<div class="tooltip1">
																								<img class="img-fluid rec-screen" data-input-id="ddq_rec_screen" data-assets="ddq_assets" src="img/list/screen_record.svg" />
																								<span class="tooltiptext">Record Screen</span>
																							 </div>
																							 <input type="hidden" name="ddq_rec_screen" id="ddq_rec_screen" value="<?php echo ($ques_type == 8 && $assets_type == 's') ? $ques_assets : ''; ?>" />
																						</a>
																					</div>
																				</li>
																				<li>
																					<div class="tooltip uploadicon ddq_assets <?php echo ( ! empty($ques_assets) && $ques_type == 8) ? 'disabled' : ''; ?>" <?php echo ( ! empty($ques_assets) && $ques_type == 8) ? 'disabled="disabled"' : ''; ?>>
																						<label for="file-input-ddq-addImg">
																							<img class="img-fluid" src="img/list/image.svg" />
																							<span class="tooltiptext">Add Image</span>
																						</label>
																						<input id="file-input-ddq-addImg" data-id="#ddq_img" data-assets="ddq_assets" class="uploadImgFile ddq_assets" type="file" name="ddq_Img" <?php echo ( ! empty($ques_assets) && $ques_type == 8) ? 'disabled="disabled"' : ''; ?> />
																						<input type="hidden" name="ddq_img" id="ddq_img" value="<?php echo ($ques_type == 8 && $assets_type == 'i') ? $ques_assets : ''; ?>" />
																					</div>
																				</li>
																				<li>
																					<div class="tooltip uploadicon ddq_assets <?php echo ( ! empty($ques_assets) && $ques_type == 8) ? 'disabled' : ''; ?>" <?php echo ( ! empty($ques_assets) && $ques_type == 8) ? 'disabled="disabled"' : ''; ?>>
																						<label for="file-input-ddq-addDoc">
																							<img class="img-fluid" src="img/list/add_document.svg" />
																							<span class="tooltiptext">Add Document</span>
																						</label>
																						<input id="file-input-ddq-addDoc" data-id="#ddq_doc" data-assets="ddq_assets" class="uploadDocFile ddq_assets" type="file" name="ddq_Doc" />
																						<input type="hidden" name="ddq_doc" id="ddq_doc" value="<?php echo ($ques_type == 8 && $assets_type == 'd') ? $ques_assets : ''; ?>" />
																					</div>
																				</li>
																			</ul>
																			<div class="Ques-comp">
																				<h5>Add Competency Score</h5>
																				<?php if ( ! empty($sim_com['comp_col_1']) &&  ! empty($sim_com['comp_val_1'])): ?>
																				<div class="form-group QusScore">
																					<input type="text" class="form-control ddq" name="ddq_ques_val_1" id="ddq_ques_val_1" onkeypress="return isNumberKey(event);" value="<?php echo $qresult['ques_val_1'] ?>" />
																				</div>
																				<input type="hidden" class="form-control" id="ddq_ques_val_1h" value="<?php echo $qresult['ques_val_1'] ?>" />
																				<?php else: ?>
																				<input type="hidden" class="form-control" name="ddq_ques_val_1" value="0" />
																				<?php endif; ?>
																				
																				<?php if ( ! empty($sim_com['comp_col_2']) &&  ! empty($sim_com['comp_val_2'])): ?>
																				<div class="form-group QusScore">
																					<input type="text" class="form-control ddq" name="ddq_ques_val_2" id="ddq_ques_val_2" onkeypress="return isNumberKey(event);" value="<?php echo $qresult['ques_val_2'] ?>" />
																				</div>
																				<input type="hidden" class="form-control" id="ddq_ques_val_2h" value="<?php echo $qresult['ques_val_2'] ?>" />
																				<?php else: ?>
																				<input type="hidden" class="form-control" name="ddq_ques_val_2" value="0" />
																				<?php endif; ?>
																				
																				<?php if ( ! empty($sim_com['comp_col_3']) &&  ! empty($sim_com['comp_val_3'])): ?>
																				<div class="form-group QusScore">
																					<input type="text" class="form-control ddq" name="ddq_ques_val_3" id="ddq_ques_val_3" onkeypress="return isNumberKey(event);" value="<?php echo $qresult['ques_val_3'] ?>" />
																				</div>
																				<input type="hidden" class="form-control" id="ddq_ques_val_3h" value="<?php echo $qresult['ques_val_3'] ?>" />
																				<?php else: ?>
																				<input type="hidden" class="form-control" name="ddq_ques_val_3" value="0" />
																				<?php endif; ?>
																				
																				<?php if ( ! empty($sim_com['comp_col_4']) &&  ! empty($sim_com['comp_val_4'])): ?>
																				<div class="form-group QusScore">
																					<input type="text" class="form-control ddq" name="ddq_ques_val_4" id="ddq_ques_val_4" onkeypress="return isNumberKey(event);" value="<?php echo $qresult['ques_val_4'] ?>" />
																				</div>
																				<input type="hidden" class="form-control" id="ddq_ques_val_4h" value="<?php echo $qresult['ques_val_4'] ?>" />
																				<?php else: ?>
																				<input type="hidden" class="form-control" name="ddq_ques_val_4" value="0" />
																				<?php endif; ?>
																				
																				<?php if ( ! empty($sim_com['comp_col_5']) &&  ! empty($sim_com['comp_val_5'])): ?>
																				<div class="form-group QusScore">
																					<input type="text" class="form-control ddq" name="ddq_ques_val_5" id="ddq_ques_val_5" onkeypress="return isNumberKey(event);" value="<?php echo $qresult['ques_val_5'] ?>" />
																				</div>
																				<input type="hidden" class="form-control" id="ddq_ques_val_5h" value="<?php echo $qresult['ques_val_5'] ?>" />
																				<?php else: ?>
																				<input type="hidden" class="form-control" name="ddq_ques_val_5h" value="0" />
																				<?php endif; ?>
																				
																				<?php if ( ! empty($sim_com['comp_col_6']) &&  ! empty($sim_com['comp_val_6'])): ?>
																				<div class="form-group QusScore">
																					<input type="text" class="form-control ddq" name="ddq_ques_val_6" id="ddq_ques_val_6" onkeypress="return isNumberKey(event);" value="<?php echo $qresult['ques_val_6'] ?>" />
																				</div>
																				<input type="hidden" class="form-control" id="ddq_ques_val_6h" value="<?php echo $qresult['ques_val_6'] ?>" />
																				<?php else: ?>
																				<input type="hidden" class="form-control" name="ddq_ques_val_6" value="0" />
																				<?php endif; ?>
																			</div>
																		</div>
																		<div class="ddq_assets_container" <?php echo ( ! empty($ques_assets) && $ques_type == 8) ? '' : 'style="display:none;"'; ?>>
																			<div class="assets_data ddq_assets_data"><a class="view_assets" data-src="<?php echo $path . $ques_assets; ?>" href="javascript:;"><?php echo $ques_assets; ?></a></div>
																			<a href="javascript:void(0);" data-assets="<?php echo $ques_assets; ?>" data-assets-id="ddq_assets" data-input-id="#ddq_img,#ddq_qaudio,#ddq_rec_audio,#ddq_video,#ddq_rec_video,#ddq_rec_screen" data-path="<?php echo $uploadpath ?>" class="delete_assets" title="Delete"><i class="fa fa-times" aria-hidden="true"></i></a>
																		</div>
																	</div>
																</div>
															</div>
															<div class="form-popup draggable Ques-pop-style modal" id="ddqqtts">
																<div class="popheading">Insert Text to Speech</div>
																<div class="textarea">
																	<textarea type="text" class="form-control1 inputtextWrap" name="ddqq_text_to_speech" id="ddqq_text_to_speech"><?php echo ($ques_type == 8 && ! empty($qresult['qspeech_text'])) ? stripslashes($qresult['qspeech_text']) : ''; ?></textarea>
																</div>
																<div class="modal-footer">
																	<button type="button" class="btn1 submitbtn1" data-dismiss="modal">Insert</button>
																	<button type="button" class="btn1 submitbtn1" onclick="clearData('ddqq_text_to_speech');">Clear</button>
																	<button type="button" class="btn1 submitbtn1" data-dismiss="modal">Cancel</button>
																</div>
															</div>
															<div class="form-popup draggable Ques-pop-style modal" id="ddqtts">
																<div class="popheading">Insert Text to Speech</div>
																<div class="textarea">
																	<textarea type="text" class="form-control1 inputtextWrap" name="ddq_text_to_speech" id="ddq_text_to_speech"><?php echo ($ques_type == 8 && ! empty($qresult['speech_text'])) ? stripslashes($qresult['speech_text']) : ''; ?></textarea>
																</div>
																<div class="modal-footer">
																	<button type="button" class="btn1 submitbtn1" data-dismiss="modal">Insert</button>
																	<button type="button" class="btn1 submitbtn1" onclick="clearData('ddq_text_to_speech');">Clear</button>
																	<button type="button" class="btn1 submitbtn1" data-dismiss="modal">Cancel</button>
																</div>
															</div>
															<div class="Ansbox Dragrtab1" <?php echo ( ! empty($qresult['seleted_drag_option']) && $qresult['seleted_drag_option'] == 1) ? 'style="display:block"' : '' ?> <?php echo ( ! empty($qresult['seleted_drag_option']) && $qresult['seleted_drag_option'] == 2) ? 'style="display:none"' : '' ?>>
																<div class="row">
																	<div class="col-sm-12">
																		<div class="col-sm-6">
																			<h3 class="AnsHeading">DROP TARGET</h3>
																		</div>
																		<div class="col-sm-6">
																			<h3 class="AnsHeading matcht">DRAG ITEM</h3>
																			<div class="Check-box suffel">
																				<div class="tooltiptop"><span class="tooltiptext">Shuffle ON</span></div>
																				<label class="checkstyle">
																					<input type="checkbox" name="ddq_shuffle1" class="suffleCheck" value="1" <?php echo ( ! empty($qresult['shuffle'])) ? 'checked="checked"' : ''; ?> />
																					<span class="checkmark"></span><span class="shuffleT"> Shuffle Option</span>
																				</label>
																			</div>
																		</div>
																		<div class="ApendOption-box">
																		<?php 
																		$ddq_sql = "SELECT answer_id, choice_option, match_option, image, image2 FROM answer_tbl WHERE question_id = '". $qresult['question_id'] ."'";
																		$ddq_res = $db->prepare($ddq_sql); $ddq_res->execute();
																		if ($ddq_res->rowCount() > 0): $ddqi = 1;
																		foreach ($ddq_res->fetchAll(PDO::FETCH_ASSOC) as $ddq_res_row): ?>
																		<div class="DNDOption-box1">
																			<input type="hidden" name="updateDDqAnswerid[]" value="<?php echo $ddq_res_row['answer_id'] ?>" />
																			<div class="col-sm-6">
																				<div class="matchbox">
																					<span class="QuesNo"><?php echo $ddqi ?></span>
																					<div class="form-group">
																						<ul class="QueBoxIcon">
																							<li>
																								<div class="tooltip uploadicon ddq_item_assets<?php echo $ddqi ?> <?php echo ( ! empty($ddq_res_row['image']) && file_exists($root_path . $ddq_res_row['image'])) ? 'disabled' : '' ?>" <?php echo ( ! empty($ddq_res_row['image']) && file_exists($root_path . $ddq_res_row['image'])) ? 'disabled="disabled"' : '' ?>>
																									<label for="file-input-ddq-item-addImg<?php echo $ddqi ?>">
																										<img class="img-fluid" src="img/list/image.svg" />
																										<span class="tooltiptext">Add Image</span>
																									</label>
																									<input id="file-input-ddq-item-addImg<?php echo $ddqi ?>" data-id="#ddq_item_img<?php echo $ddqi ?>" data-assets="<?php echo $ddqi ?>" class="uploadImgFileDDq ddq_item_assets<?php echo $ddqi ?>" type="file" name="ddq_item_Img<?php echo $ddqi ?>" <?php echo ( ! empty($ddq_res_row['image']) && file_exists($root_path . $ddq_res_row['image'])) ? 'disabled="disabled"' : '' ?> />
																									<input type="hidden" name="ddq_item_img[]" id="ddq_item_img<?php echo $ddqi ?>" value="<?php echo $ddq_res_row['image']; ?>" />
																								</div>
																							</li>
																						</ul>
																						<input type="text" name="ddq_drop[]" class="form-control inputtextWrap ddq" placeholder="Drop Target text goes here" value="<?php echo stripslashes($ddq_res_row['choice_option']); ?>" />
																						<div class="ddq_items_assets_container_<?php echo $ddqi ?>" id="ddq_items_assets" <?php echo ( ! empty($ddq_res_row['image']) && file_exists($root_path . $ddq_res_row['image'])) ? '' : 'style="display:none;"'; ?>>
																							<div class="assets_data ddq_items_assets_data_<?php echo $ddqi ?>"><a class="view_assets" data-src="<?php echo $path . $ddq_res_row['image']; ?>" href="javascript:;"><?php echo $ddq_res_row['image']; ?></a></div>
																							<a href="javascript:void(0);" data-assets="<?php echo $ddq_res_row['image']; ?>" data-assets-id="ddq_item_assets<?php echo $ddqi ?>" data-input-id="#ddq_item_img<?php echo $ddqi ?>" data-path="<?php echo $uploadpath ?>" class="delete_assets" title="Delete"><i class="fa fa-times" aria-hidden="true"></i></a>
																						</div>
																					</div>
																				</div>
																			</div>
																			<div class="col-sm-6">
																				<div class="matchbox">
																					<div class="form-group">
																						<ul class="QueBoxIcon">
																							<li>
																								<div class="tooltip uploadicon ddq_drag_item_assets<?php echo $ddqi ?> <?php echo ( ! empty($ddq_res_row['image2'])) ? 'disabled' : '' ?>" <?php echo ( ! empty($ddq_res_row['image2'])) ? 'disabled="disabled"' : '' ?>>
																									<label for="file-input-ddq-drag-item-addImg<?php echo $ddqi ?>">
																										<img class="img-fluid" src="img/list/image.svg" />
																										<span class="tooltiptext">Add Image</span>
																									</label>
																									<input id="file-input-ddq-drag-item-addImg<?php echo $ddqi ?>" data-id="#ddq_drag_item_img<?php echo $ddqi ?>" data-assets="<?php echo $ddqi ?>" class="uploadImgFileDragDDq ddq_drag_item_assets<?php echo $ddqi ?>" type="file" name="ddq_drag_Img<?php echo $ddqi ?>" <?php echo ( ! empty($ddq_res_row['image2'])) ? 'disabled="disabled"' : '' ?> />
																									<input type="hidden" name="ddq_drag_item_img[]" id="ddq_drag_item_img<?php echo $ddqi ?>" value="<?php echo $ddq_res_row['image2']; ?>" />
																								</div>
																							</li>
																						</ul>
																						<input type="text" name="ddq_drag[]" class="form-control inputtextWrap" placeholder="Drag text goes here" value="<?php echo stripslashes($ddq_res_row['match_option']); ?>" />
																						<div class="ddq_drag_items_assets_container_<?php echo $ddqi ?>" id="ddq_drag_items_assets" <?php echo ( ! empty($ddq_res_row['image2'])) ? '' : 'style="display:none;"'; ?>>
																							<div class="assets_data ddq_drag_items_assets_data_<?php echo $ddqi ?>"><a class="view_assets" data-src="<?php echo $path . $ddq_res_row['image2']; ?>" href="javascript:;"><?php echo $ddq_res_row['image2']; ?></a></div>
																							<a href="javascript:void(0);" data-assets="<?php echo $ddq_res_row['image2']; ?>" data-assets-id="ddq_drag_item_assets<?php echo $ddqi ?>" data-input-id="#ddq_drag_item_img<?php echo $ddqi ?>" data-path="<?php echo $uploadpath ?>" class="delete_assets" title="Delete"><i class="fa fa-times" aria-hidden="true"></i></a>
																						</div>
																					</div>
																				</div>
																			</div>
																			<?php if ($ddqi == 1): ?>
																			<div class="AddAns-Option"><img class="img-fluid DNDAddAnsICON" src="img/list/add_field.svg" /></div>
																			<?php else: ?>
																			<div class="AddAns-Option removeOption" data-remove-answer-id="<?php echo $ddq_res_row['answer_id'] ?>"><img class="img-fluid" src="img/list/delete_field.svg" /></div>
																			<?php endif; ?>
																		</div>
																		<?php $ddqi++; endforeach; else: ?>
																		<div class="DNDOption-box1">
																			<div class="col-sm-6">
																				<div class="matchbox">
																					<span class="QuesNo">1</span>
																					<div class="form-group">
																						<ul class="QueBoxIcon">
																							<li>
																								<div class="tooltip uploadicon ddq_item_assets1">
																									<label for="file-input-ddq-item-addImg1">
																										<img class="img-fluid" src="img/list/image.svg" />
																										<span class="tooltiptext">Add Image</span>
																									</label>
																									<input id="file-input-ddq-item-addImg1" data-id="#ddq_item_img1" data-assets="1" class="uploadImgFileDDq ddq_item_assets1" type="file" name="ddq_item_Img1" />
																									<input type="hidden" name="ddq_item_img[]" id="ddq_item_img1" />
																								</div>
																							</li>
																						</ul>
																						<input type="text" name="ddq_drop[]" class="form-control inputtextWrap ddq" placeholder="Drop Target text goes here" />
																						<div class="ddq_items_assets_container_1" id="ddq_items_assets" style="display:none;">
																							<div class="assets_data ddq_items_assets_data_1"><a class="view_assets" data-src="" href="javascript:;"></a></div>
																							<a href="javascript:void(0);" data-assets="" data-assets-id="ddq_item_assets1" data-input-id="#ddq_item_img1" data-path="<?php echo $uploadpath ?>" class="delete_assets" title="Delete"><i class="fa fa-times" aria-hidden="true"></i></a>
																						</div>
																					</div>
																				</div>
																			</div>
																			<div class="col-sm-6">
																				<div class="matchbox">
																					<div class="form-group">
																						<ul class="QueBoxIcon">
																							<li>
																								<div class="tooltip uploadicon ddq_drag_item_assets1">
																									<label for="file-input-ddq-drag-item-addImg1">
																										<img class="img-fluid" src="img/list/image.svg" />
																										<span class="tooltiptext">Add Image</span>
																									</label>
																									<input id="file-input-ddq-drag-item-addImg1" data-id="#ddq_drag_item_img1" data-assets="1" class="uploadImgFileDragDDq ddq_drag_item_assets1" type="file" name="ddq_drag_Img1" />
																									<input type="hidden" name="ddq_drag_item_img[]" id="ddq_drag_item_img1" />
																								</div>
																							</li>
																						</ul>
																						<input type="text" name="ddq_drag[]" class="form-control inputtextWrap" placeholder="Drag text goes here" />
																						<div class="ddq_drag_items_assets_container_1" id="ddq_drag_items_assets" style="display:none;">
																							<div class="assets_data ddq_drag_items_assets_data_1"><a class="view_assets" data-src="" href="javascript:;"></a></div>
																							<a href="javascript:void(0);" data-assets="" data-assets-id="ddq_drag_item_assets1" data-input-id="#ddq_drag_item_img1" data-path="<?php echo $uploadpath ?>" class="delete_assets" title="Delete"><i class="fa fa-times" aria-hidden="true"></i></a>
																						</div>
																					</div>
																				</div>
																			</div>
																			<div class="AddAns-Option">
																				<img class="img-fluid DNDAddAnsICON" src="img/list/add_field.svg" />
																			</div>
																		</div>
																		<?php endif; ?>
																		</div>
																	</div>
																</div>
															</div>
															<div class="Ansbox Dragrtab2" <?php echo ( ! empty($qresult['seleted_drag_option']) && $qresult['seleted_drag_option'] == 2) ? 'style="display:block"' : 'style="display:none"' ?>>
																<div class="row">
																	<div class="col-sm-12">
																		<h3 class="AnsHeading DrOPHeading">DROP TARGET</h3>
																		<?php $ddq2_sql = "SELECT * FROM sub_options_tbl WHERE question_id = '". $qresult['question_id'] ."'";
																		$ddq2_res = $db->prepare($ddq2_sql); $ddq2_res->execute();
																		if ($ddq2_res->rowCount() > 0):
																		$ddq2_res_data = $ddq2_res->fetch(PDO::FETCH_ASSOC); ?>
																		<div class="matchbox DNDQfIeLd DNDQfIeLd1">
																			<input type="hidden" name="updateDDq2Answerid" value="<?php echo $ddq2_res_data['sub_options_id']; ?>" />
																			<div class="form-group">
																				<ul class="QueBoxIcon DTQuebox">
																					<li>
																						<div class="tooltip uploadicon ddq2_item_assets1 <?php echo ( ! empty($ddq2_res_data['image']) && file_exists($root_path . $ddq2_res_data['image'])) ? 'disabled' : '' ?>" <?php echo ( ! empty($ddq2_res_data['image']) && file_exists($root_path . $ddq2_res_data['image'])) ? 'disabled="disabled"' : '' ?>>
																							<label for="file-input-ddq2-item-addImg1">
																								<img class="img-fluid" src="img/list/image.svg" />
																								<span class="tooltiptext">Add Image</span>
																							</label>
																							<input id="file-input-ddq2-item-addImg1" data-id="#ddq2_item_img1" data-assets="1" class="uploadImgFileDDq2 ddq2_item_assets1" type="file" name="ddq2_item_Img1" <?php echo ( ! empty($ddq2_res_data['image']) && file_exists($root_path . $ddq2_res_data['image'])) ? 'disabled="disabled"' : '' ?> />
																							<input type="hidden" name="ddq2_item_img" id="ddq2_item_img1" value="<?php echo ( ! empty($ddq2_res_data['image']) && file_exists($root_path . $ddq2_res_data['image'])) ? $ddq2_res_data['image'] : '' ?>" />
																						</div>
																					</li>
																				</ul>
																				<input type="text" name="ddq2_drop" class="form-control inputtextWrap ddq" placeholder="Drop Target text goes here" value="<?php echo ( ! empty($ddq2_res_data['sub_option'])) ? $ddq2_res_data['sub_option'] : '' ?>" />
																				<div class="ddq2_items_assets_container_1" id="ddq2_items_assets" <?php echo ( ! empty($ddq2_res_data['image']) && file_exists($root_path . $ddq2_res_data['image'])) ? '' : 'style="display:none;"'; ?>>
																					<div class="assets_data ddq2_items_assets_data_1"><?php echo ( ! empty($ddq2_res_data['image']) && file_exists($root_path . $ddq2_res_data['image'])) ? '<a class="view_assets" data-src="'. $path . $ddq2_res_data['image'] .'" href="javascript:;">'. $ddq2_res_data['image'] .'</a>' : '' ?></div>
																					<a href="javascript:void(0);" data-assets="<?php echo ( ! empty($ddq2_res_data['image'])) ? $ddq2_res_data['image'] : '' ?>" data-assets-id="ddq2_item_assets1" data-input-id="#ddq2_item_img1" data-path="<?php echo $uploadpath ?>" class="delete_assets" title="Delete"><i class="fa fa-times" aria-hidden="true"></i></a>
																				</div>
																			</div>
																		</div>
																		<?php else: ?>
																		<div class="matchbox DNDQfIeLd">
																			<div class="form-group">
																				<ul class="QueBoxIcon DTQuebox">
																					<li>
																						<div class="tooltip uploadicon ddq2_item_assets1">
																							<label for="file-input-ddq2-item-addImg1">
																								<img class="img-fluid" src="img/list/image.svg" />
																								<span class="tooltiptext">Add Image</span>
																							</label>
																							<input id="file-input-ddq2-item-addImg1" data-id="#ddq2_item_img1" data-assets="1" class="uploadImgFileDDq2 ddq2_item_assets1" type="file" name="ddq2_item_Img1" />
																							<input type="hidden" name="ddq2_item_img" id="ddq2_item_img1" />
																						</div>
																					</li>
																				</ul>
																				<input type="text" name="ddq2_drop" class="form-control inputtextWrap ddq" placeholder="Drop Target text goes here" />
																				<div class="ddq2_items_assets_container_1" id="ddq2_items_assets" style="display:none;">
																					<div class="assets_data ddq2_items_assets_data_1"><a class="view_assets" data-src="" href="javascript:;"></a></div>
																					<a href="javascript:void(0);" data-assets="" data-assets-id="ddq2_item_assets1" data-input-id="#ddq2_item_img1" data-path="<?php echo $uploadpath ?>" class="delete_assets" title="Delete"><i class="fa fa-times" aria-hidden="true"></i></a>
																				</div>
																			</div>
																		</div>
																		<?php endif; ?>
																	</div>
																	<div class="col-sm-12">
																		<h3 class="AnsHeading matcht">DRAG ITEM</h3>
																		<div class="Check-box suffel">
																			<div class="tooltiptop"><span class="tooltiptext">Shuffle ON</span></div>
																			<label class="checkstyle">
																				<input type="checkbox" name="ddq_shuffle2" class="suffleCheck" value="1" <?php echo ( ! empty($qresult['shuffle'])) ? 'checked="checked"' : ''; ?> />
																				<span class="checkmark"></span><span class="shuffleT"> Shuffle Option</span>
																			</label>
																		</div>
																	</div>
																	<div class="ApendOption-box">
																	<?php $ddqop_sql = "SELECT * FROM answer_tbl WHERE question_id = '". $qresult['question_id'] ."'";
																		$ddqop_res = $db->prepare($ddqop_sql); $ddqop_res->execute();
																		if ($ddqop_res->rowCount() > 0): $ddqopid = 1;
																		foreach ($ddqop_res->fetchAll(PDO::FETCH_ASSOC) as $ddqop_drop_row): ?>
																		<div class="DNDOption-box2">
																			<input type="hidden" name="updateDDq2SubAnswerid[]" value="<?php echo $ddqop_drop_row['answer_id'] ?>" />
																			<div class="col-sm-12">
																				<div class="Radio-box">
																					<label class="radiostyle">
																						<input type="radio" name="ddq_true_option" value="<?php echo $ddqopid ?>" <?php echo ($ddqopid == $qresult['true_options']) ? 'checked="checked"' : ''; ?> />
																						<span class="radiomark"></span>
																				   </label>
																				   <div class="col-sm-12">
																						<div class="choicebox">
																							<div class="form-group">
																							<?php 
																							$ddqop_cls_dis	= '';
																							$ddqop_dis 		= '';
																							$ddqop_item		= '';
																							if ( ! empty($ddqop_drop_row['image']) && file_exists($root_path . $ddqop_drop_row['image']) && $ques_type == 8):
																								$ddqop_cls_dis	= 'disabled';
																								$ddqop_dis		= 'disabled="disabled"';
																								$ddqop_item 	= $ddqop_drop_row['image'];
																							endif; ?>
																							<ul class="QueBoxIcon">
																								<li>
																									<div class="tooltip uploadicon ddq2_drag_item_assets<?php echo $ddqopid ?> <?php echo $ddqop_cls_dis; ?>" <?php echo $ddqop_dis; ?>>
																										<label for="file-input-ddq2-drag-item-addImg<?php echo $ddqopid ?>">
																											<img class="img-fluid" src="img/list/image.svg" />
																											<span class="tooltiptext">Add Image</span>
																										</label>
																										<input id="file-input-ddq2-drag-item-addImg<?php echo $ddqopid ?>" data-id="#ddq2_drag_item_img<?php echo $ddqopid ?>" data-assets="<?php echo $ddqopid ?>" class="uploadImgFileDragDDq2 ddq2_drag_item_assets<?php echo $ddqopid ?>" type="file" name="ddq2_drag_Img<?php echo $ddqopid ?>" <?php echo $ddqop_dis; ?> />
																										<input type="hidden" name="ddq2_drag_item_img[]" id="ddq2_drag_item_img<?php echo $ddqopid ?>" value="<?php echo $ddqop_item; ?>" />
																									</div>
																								</li>
																							</ul>
																							<input type="text" name="ddq2_drag[]" class="form-control inputtextWrap" placeholder="Drag text goes here" value="<?php echo stripslashes($ddqop_drop_row['choice_option']); ?>" />
																							<div class="ddq2_drag_items_assets_container_<?php echo $ddqopid ?>" id="ddq2_drag_items_assets" <?php echo ( ! empty($ddqop_item)) ? '' : 'style="display:none;"'; ?>>
																								<div class="assets_data ddq2_drag_items_assets_data_<?php echo $ddqopid ?>"><a class="view_assets" data-src="<?php echo $path . $ddqop_item; ?>" href="javascript:;"><?php echo $ddqop_item; ?></a></div>
																								<a href="javascript:void(0);" data-assets="<?php echo $ddqop_item; ?>" data-assets-id="ddq2_drag_item_assets<?php echo $ddqopid ?>" data-input-id="#ddq2_drag_item_img<?php echo $ddqopid ?>" data-path="<?php echo $uploadpath ?>" class="delete_assets" title="Delete"><i class="fa fa-times" aria-hidden="true"></i></a>
																							</div>
																						</div>
																					</div>
																				</div>
																				<?php if ($ddqopid == 1): ?>
																				<div class="AddAns-Option"><img class="img-fluid DNDAddAnsICON1" src="img/list/add_field.svg" /></div>
																				<?php else: ?>
																				<div class="AddAns-Option removeOption" data-remove-answer-id="<?php echo $ddqop_drop_row['answer_id'] ?>"><img class="img-fluid" src="img/list/delete_field.svg" /></div>
																				<?php endif; ?>
																				</div>
																			</div>
																		</div>
																		<?php $ddqopid++; endforeach; else: ?>
																		<div class="DNDOption-box2">
																			<div class="col-sm-12">
																				<div class="Radio-box">
																					<label class="radiostyle">
																						<input type="radio" name="ddq_true_option" value="1" checked /><span class="radiomark"></span>
																				   </label>
																				   <div class="col-sm-12">
																						<div class="choicebox">
																							<div class="form-group">
																								<ul class="QueBoxIcon">
																									<li>
																										<div class="tooltip uploadicon ddq2_drag_item_assets1">
																											<label for="file-input-ddq2-drag-item-addImg1">
																												<img class="img-fluid" src="img/list/image.svg" />
																												<span class="tooltiptext">Add Image</span>
																											</label>
																											<input id="file-input-ddq2-drag-item-addImg1" data-id="#ddq2_drag_item_img1" data-assets="1" class="uploadImgFileDragDDq2 ddq2_drag_item_assets1" type="file" name="ddq2_drag_Img1" />
																											<input type="hidden" name="ddq2_drag_item_img[]" id="ddq2_drag_item_img1" />
																										</div>
																									</li>
																								</ul>
																								<input type="text" name="ddq2_drag[]" class="form-control inputtextWrap" placeholder="Drag text goes here" />
																								<div class="ddq2_drag_items_assets_container_1" id="ddq2_drag_items_assets" style="display:none;">
																									<div class="assets_data ddq2_drag_items_assets_data_1"><a class="view_assets" data-src="" href="javascript:;"></a></div>
																									<a href="javascript:void(0);" data-assets="" data-assets-id="ddq2_drag_item_assets1" data-input-id="#ddq2_drag_item_img1" data-path="<?php echo $uploadpath ?>" class="delete_assets" title="Delete"><i class="fa fa-times" aria-hidden="true"></i></a>
																								</div>
																							</div>
																						</div>
																					</div>
																				   <div class="AddAns-Option">
																						<img class="img-fluid DNDAddAnsICON1" src="img/list/add_field.svg">
																					</div>
																				</div>
																			</div>
																	   </div>
																	   <?php endif; ?>
																	</div>
																</div>
															</div>
														</div>
													</div>
													<div id="DAD-F" class="tab-pane fade in">
														<div class="col-sm-12 Qusfeedback">
															<div class="form-group">
																<div class="feedback-flex">
																	<h4>Correct</h4>
																	<textarea class="form-control FeedForm inputtextWrap ddq" name="ddq_cfeedback" id="ddq_cfeedback" placeholder="That's right! You selected the correct response."><?php echo ($ques_type == 8 && ! empty($feedResult[0]['feedback']) && $feedResult[0]['feedback_type'] == 1) ? stripslashes($feedResult[0]['feedback']) : ''; ?></textarea>
																</div>
																<ul class="QueBoxIcon feed LMfeed">
																	<h5>Add assets</h5>
																	<li class="dropdown">
																		<button class="btn1 btn-secondary dropdown-toggle ddqf_assets <?php echo ( ! empty($qfeed_assets) && $ques_type == 8) ? 'disabled' : ''; ?>" <?php echo ( ! empty($qfeed_assets) && $ques_type == 8) ? 'disabled="disabled"' : ''; ?> type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img class="img-fluid" src="img/list/audio.svg"></button>
																		<div class="dropdown-menu buttom" aria-labelledby="dropdownMenuButton">
																			<a data-toggle="modal" data-target="#ddqfctts" class="dropdown-item">
																				<div class="tooltip1">
																					<img class="img-fluid" src="img/list/text_speech.svg" />
																					<span class="tooltiptext">Text to Speech</span>
																				</div>
																			</a>
																			<a class="dropdown-item">
																				<div class="tooltip1 uploadicon">
																					<label for="file-input-ddq-feedback-addAudio">
																						<img class="img-fluid" src="img/list/add_audio.svg" />
																						<span class="tooltiptext">Add Audio</span>
																					</label>
																					<input id="file-input-ddq-feedback-addAudio" data-id="#ddq_feedback_audio" data-assets="ddqf_assets" class="uploadAudioFile" type="file" name="ddq_feedback_Audio" />
																					<input type="hidden" name="ddq_feedback_audio" id="ddq_feedback_audio" value="<?php echo ($ques_type == 8 && $feed_assets_type == 'fa') ? $qfeed_assets : ''; ?>" />
																				</div>
																			</a>
																			<a class="dropdown-item">
																				<div class="tooltip1">
																					<img class="img-fluid rec-audio" data-input-id="ddq_feedback_rec_audio" data-assets="ddqf_assets" src="img/list/record_audio.svg" />
																					<span class="tooltiptext">Record Audio</span>
																				</div>
																				<input type="hidden" name="ddq_feedback_rec_audio" id="ddq_feedback_rec_audio" />
																			 </a>
																		</div>
																	</li>
																	<li class="dropdown">
																		<button class="btn1 btn-secondary dropdown-toggle ddqf_assets <?php echo ( ! empty($qfeed_assets) && $ques_type == 8) ? 'disabled' : ''; ?>" <?php echo ( ! empty($qfeed_assets) && $ques_type == 8) ? 'disabled="disabled"' : ''; ?> type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img class="img-fluid" src="img/list/video.svg"></button>
																		<div class="dropdown-menu buttom" aria-labelledby="dropdownMenuButton">
																			<a class="dropdown-item">
																				<div class="tooltip1 uploadicon">
																					<label for="file-input-ddq-feedback-addVideo">
																						<img class="img-fluid" src="img/list/add_video.svg">
																						<span class="tooltiptext">Add Video</span>
																					</label>
																					<input id="file-input-ddq-feedback-addVideo" data-id="#ddq_feedback_video" data-assets="ddqf_assets" class="uploadVideoFile" type="file" name="ddq_feedback_Video" />
																					<input type="hidden" name="ddq_feedback_video" id="ddq_feedback_video" value="<?php echo ($ques_type == 8 && $feed_assets_type == 'fv') ? $qfeed_assets : ''; ?>" />
																				</div>
																			</a>
																			<a class="dropdown-item">
																				<div class="tooltip1">
																					<img class="img-fluid rec-video" data-input-id="ddq_feedback_rec_video" data-assets="ddqf_assets" src="img/qus_icon/rec_video.svg">
																					<span class="tooltiptext">Record Video</span>
																				 </div>
																				 <input type="hidden" name="ddq_feedback_rec_video" id="ddq_feedback_rec_video" />
																			</a>
																			<a class="dropdown-item">
																				<div class="tooltip1">
																					<img class="img-fluid rec-screen" data-input-id="ddq_feedback_rec_screen" data-assets="ddq_assets" src="img/list/screen_record.svg">
																					<span class="tooltiptext">Record Screen</span>
																				 </div>
																				 <input type="hidden" name="ddq_feedback_rec_screen" id="ddq_feedback_rec_screen" value="<?php echo ($ques_type == 8 && $feed_assets_type == 'fs') ? $qfeed_assets : ''; ?>" />
																			</a>
																		</div>
																	</li>
																	<li>
																		<div class="tooltip uploadicon ddqf_assets <?php echo ( ! empty($qfeed_assets) && $ques_type == 8) ? 'disabled' : ''; ?>" <?php echo ( ! empty($qfeed_assets) && $ques_type == 8) ? 'disabled="disabled"' : ''; ?>>
																			<label for="file-input-ddq-feedback-addImg">
																				<img class="img-fluid" src="img/list/image.svg">
																				<span class="tooltiptext">Add Image</span>
																			</label>
																			<input id="file-input-ddq-feedback-addImg" data-id="#ddq_feedback_img" data-assets="ddqf_assets" class="uploadImgFile ddqf_assets" type="file" name="ddq_feedback_Img" <?php echo ( ! empty($qfeed_assets) && $ques_type == 8) ? 'disabled="disabled"' : ''; ?> />
																			<input type="hidden" name="ddq_feedback_img" id="ddq_feedback_img" value="<?php echo ($ques_type == 8 && $feed_assets_type == 'fi') ? $qfeed_assets : ''; ?>" />
																		</div>
																	</li>
																	<li>
																		<div class="tooltip uploadicon ddqf_assets <?php echo ( ! empty($qfeed_assets) && $ques_type == 8) ? 'disabled' : ''; ?>" <?php echo ( ! empty($qfeed_assets) && $ques_type == 8) ? 'disabled="disabled"' : ''; ?>>
																			<label for="file-input-ddq-feedback-addDoc">
																				<img class="img-fluid" src="img/list/add_document.svg" />
																				<span class="tooltiptext">Add Document</span>
																			</label>
																			<input id="file-input-ddq-feedback-addDoc" data-id="#ddq_feedback_doc" data-assets="ddqf_assets" class="uploadDocFile ddqf_assets" type="file" name="ddq_feedback_Doc" <?php echo ( ! empty($qfeed_assets) && $ques_type == 8) ? 'disabled="disabled"' : ''; ?> />
																			<input type="hidden" name="ddq_feedback_doc" id="ddq_feedback_doc" value="<?php echo ($ques_type == 8 && $feed_assets_type == 'fd') ? $qfeed_assets : ''; ?>" />
																		</div>
																	</li>
																</ul>
															</div>                                                        
															<div class="ddqf_assets_container" <?php echo ( ! empty($qfeed_assets) && $ques_type == 8) ? '' : 'style="display:none;"'; ?>>
																<div class="assets_data ddqf_assets_data"><a class="view_assets" data-src="<?php echo $path . $qfeed_assets; ?>" href="javascript:;"><?php echo $qfeed_assets; ?></a></div>
																<a href="javascript:void(0);" data-assets="<?php echo $qfeed_assets ?>" data-assets-id="ddqf_assets" data-input-id="#ddq_feedback_audio,#ddq_feedback_rec_audio,#ddq_feedback_video,#ddq_feedback_rec_video,#ddq_feedback_rec_screen,#ddq_feedback_img" data-path="<?php echo $uploadpath ?>" class="delete_assets" title="Delete"><i class="fa fa-times" aria-hidden="true"></i></a>
															</div>
															<div class="form-group">
																<div class="feedback-flex">
																	<h4>Incorrect</h4>
																	<textarea class="form-control FeedForm inputtextWrap ddq" name="ddq_ifeedback" id="ddq_ifeedback" placeholder="You did not select the correct response."><?php echo ($ques_type == 8 && ! empty($feedResult[1]['feedback']) && $feedResult[1]['feedback_type'] == 2) ? stripslashes($feedResult[1]['feedback']) : ''; ?></textarea>
																</div>
																<ul class="QueBoxIcon feed LMfeed">
																	<h5>Add assets</h5>
																	<li class="dropdown">
																		<button class="btn1 btn-secondary dropdown-toggle ddqfi_assets <?php echo ( ! empty($qifeed_assets) && $ques_type == 8) ? 'disabled' : ''; ?>" <?php echo ( ! empty($qifeed_assets) && $ques_type == 8) ? 'disabled="disabled"' : ''; ?> type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img class="img-fluid" src="img/list/audio.svg"></button>
																		<div class="dropdown-menu buttom" aria-labelledby="dropdownMenuButton">
																			<a data-toggle="modal" data-target="#ddqfitts" class="dropdown-item">
																				<div class="tooltip1">
																					<img class="img-fluid" src="img/list/text_speech.svg">
																					<span class="tooltiptext">Text to Speech</span>
																				</div>
																			</a>
																			<a class="dropdown-item">
																				<div class="tooltip1 uploadicon">
																					<label for="file-input-ddqi-feedback-addAudio">
																						<img class="img-fluid" src="img/list/add_audio.svg">
																						<span class="tooltiptext">Add Audio</span>
																					</label>
																					<input id="file-input-ddqi-feedback-addAudio" data-id="#ddqi_feedback_audio" data-assets="ddqfi_assets" class="uploadAudioFile" type="file" name="ddqi_feedback_Audio" />
																					<input type="hidden" name="ddqi_feedback_audio" id="ddqi_feedback_audio" value="<?php echo ($ques_type == 8 && $feed_assets_type == 'fa') ? $qifeed_assets : ''; ?>" />
																				</div>
																			</a>
																			<a class="dropdown-item">
																				<div class="tooltip1">
																					<img class="img-fluid rec-audio" data-input-id="ddqi_feedback_rec_audio" data-assets="ddqfi_assets" src="img/list/record_audio.svg">
																					<span class="tooltiptext">Record Audio</span>
																				</div>
																				<input type="hidden" name="ddqi_feedback_rec_audio" id="ddqi_feedback_rec_audio" />
																			 </a>
																		</div>
																	</li>
																	<li class="dropdown">
																		<button class="btn1 btn-secondary dropdown-toggle ddqfi_assets <?php echo ( ! empty($qifeed_assets) && $ques_type == 8) ? 'disabled' : ''; ?>" <?php echo ( ! empty($qifeed_assets) && $ques_type == 8) ? 'disabled="disabled"' : ''; ?> type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img class="img-fluid" src="img/list/video.svg"></button>
																		<div class="dropdown-menu buttom" aria-labelledby="dropdownMenuButton">
																			<a class="dropdown-item">
																				<div class="tooltip1 uploadicon">
																					<label for="file-input-ddqi-feedback-addVideo">
																						<img class="img-fluid" src="img/list/video.svg">
																						<span class="tooltiptext">Add Video</span>
																					</label>
																					<input id="file-input-ddqi-feedback-addVideo" data-id="#ddqi_feedback_video" data-assets="ddqfi_assets" class="uploadVideoFile" type="file" name="ddqi_feedback_Video" />
																					<input type="hidden" name="ddqi_feedback_video" id="ddqi_feedback_video" value="<?php echo ($ques_type == 8 && $feed_assets_type == 'fv') ? $qifeed_assets : ''; ?>" />
																				</div>
																			</a>
																			<a class="dropdown-item">
																				<div class="tooltip1">
																					<img class="img-fluid rec-video" data-input-id="ddqi_feedback_rec_video" data-assets="ddqfi_assets" src="img/qus_icon/rec_video.svg">
																					<span class="tooltiptext">Record Video</span>
																				 </div>
																				 <input type="hidden" name="ddqi_feedback_rec_video" id="ddqi_feedback_rec_video" />
																			</a>
																			<a class="dropdown-item">
																				<div class="tooltip1">
																					<img class="img-fluid rec-screen" data-input-id="ddqi_feedback_rec_screen" data-assets="ddqfi_assets" src="img/list/screen_record.svg">
																					<span class="tooltiptext">Record Screen</span>
																				 </div>
																				 <input type="hidden" name="ddqi_feedback_rec_screen" id="ddqi_feedback_rec_screen" value="<?php echo ($ques_type == 8 && $feed_assets_type == 'fs') ? $qifeed_assets : ''; ?>" />
																			</a>
																		</div>
																	</li>
																	<li>
																		<div class="tooltip uploadicon ddqfi_assets <?php echo ( ! empty($qifeed_assets) && $ques_type == 8) ? 'disabled' : ''; ?>" <?php echo ( ! empty($qifeed_assets) && $ques_type == 8) ? 'disabled="disabled"' : ''; ?>>
																			<label for="file-input-ddqi-feedback-addImg">
																				<img class="img-fluid" src="img/list/image.svg">
																				<span class="tooltiptext">Add Image</span>
																			</label>
																			<input id="file-input-ddqi-feedback-addImg" data-id="#ddqi_feedback_img" data-assets="ddqfi_assets" class="uploadImgFile ddqfi_assets" type="file" name="ddqi_feedback_Img" <?php echo ( ! empty($qifeed_assets) && $ques_type == 8) ? 'disabled="disabled"' : ''; ?> />
																			<input type="hidden" name="ddqi_feedback_img" id="ddqi_feedback_img" value="<?php echo ($ques_type == 8 && $feed_assets_type == 'fi') ? $qifeed_assets : ''; ?>" />
																		</div>
																	</li>
																	<li>
																		<div class="tooltip uploadicon ddqfi_assets <?php echo ( ! empty($qifeed_assets) && $ques_type == 8) ? 'disabled' : ''; ?>" <?php echo ( ! empty($qifeed_assets) && $ques_type == 8) ? 'disabled="disabled"' : ''; ?>>
																			<label for="file-input-ddqi-feedback-addDoc">
																				<img class="img-fluid" src="img/list/add_document.svg" />
																				<span class="tooltiptext">Add Document</span>
																			</label>
																			<input id="file-input-ddqi-feedback-addDoc" data-id="#ddqi_feedback_doc" data-assets="ddqfi_assets" class="uploadDocFile ddqfi_assets" type="file" name="ddqi_feedback_Doc" <?php echo ( ! empty($qifeed_assets) && $ques_type == 8) ? 'disabled="disabled"' : ''; ?>/>
																			<input type="hidden" name="ddqi_feedback_doc" id="ddqi_feedback_doc" value="<?php echo ($ques_type == 8 && $feed_assets_type == 'fd') ? $qifeed_assets : ''; ?>" />
																		</div>
																	</li>
																</ul>
															</div>                                                        
															<div class="ddqfi_assets_container" <?php echo ( ! empty($qifeed_assets) && $ques_type == 8) ? '' : 'style="display:none;"'; ?>>
																<div class="assets_data ddqfi_assets_data"><a class="view_assets" data-src="<?php echo $path . $qifeed_assets; ?>" href="javascript:;"><?php echo $qifeed_assets; ?></a></div>
																<a href="javascript:void(0);" data-assets="<?php echo $qifeed_assets ?>" data-assets-id="ddqfi_assets" data-input-id="#ddqi_feedback_audio,#ddqi_feedback_rec_audio,#ddqi_feedback_video,#ddqi_feedback_rec_video,#ddqi_feedback_rec_screen,#ddqi_feedback_img" data-path="<?php echo $uploadpath ?>" class="delete_assets" title="Delete"><i class="fa fa-times" aria-hidden="true"></i></a>
															</div>
														</div>
														<div class="form-popup draggable Ques-pop-style modal" id="ddqfctts">
															<div class="popheading">Insert Text to Speech</div>
															<div class="textarea">
																<textarea type="text" class="form-control1 inputtextWrap" name="ddq_feedback_text_to_speech" id="ddq_feedback_text_to_speech"><?php echo ($ques_type == 8 && ! empty($feedResult[0]['feedback']) && $feedResult[0]['feedback_type'] == 1) ? stripslashes($feedResult[0]['feed_speech_text']) : ''; ?></textarea>
															</div>
															<div class="modal-footer">
																<button type="button" class="btn1 submitbtn1" data-dismiss="modal">Insert</button>
																<button type="button" class="btn1 submitbtn1" onclick="clearData('ddq_feedback_text_to_speech');">Clear</button>
																<button type="button" class="btn1 submitbtn1" data-dismiss="modal">Cancel</button>
															</div>
														</div>
														<div class="form-popup draggable Ques-pop-style modal" id="ddqfitts">
															<div class="popheading">Insert Text to Speech</div>
															<div class="textarea">
																<textarea type="text" class="form-control1 inputtextWrap" name="ddqi_feedback_text_to_speech" id="ddqi_feedback_text_to_speech"><?php echo ($ques_type == 8 && ! empty($feedResult[1]['feedback']) && $feedResult[1]['feedback_type'] == 2) ? stripslashes($feedResult[1]['feed_speech_text']) : ''; ?></textarea>
															</div>
															<div class="modal-footer">
																<button type="button" class="btn1 submitbtn1" data-dismiss="modal">Insert</button>
																<button type="button" class="btn1 submitbtn1" onclick="clearData('ddqi_feedback_text_to_speech');">Clear</button>
																<button type="button" class="btn1 submitbtn1" data-dismiss="modal">Cancel</button>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
									
									<!--MCQ-Question-->
									<div id="MCQQuestion" class="tab-pane fade <?php echo ($ques_type == 4) ? 'in active' : ''; ?>">
										<div class="match-Tab widthQ100">
											<ul class="nav nav-tabs">
												<li class="active"><a data-toggle="tab" href="#MCQ-Q">Question</a></li>
												<li><a data-toggle="tab" href="#MCQ-F">Feedback</a></li>
											</ul>
										</div>
										<div class="container-fluid">
											<div class="widthQ100 multitemp">
												<div class="tab-content">
												<div id="MCQ-Q" class="match-Q tab-pane fade in active">
													<div class="quesbox">
														<div class="row">
															<div class="col-sm-12 Questiontmpbanner">
																<div class="critical_div">
																	<h3 class="qusHeading">QUESTION <p class="question-type">(MCQ)</p></h3>
																	<div class="CIRtiCaltext">
																		Critical Question: <input type="checkbox" name="mcq_criticalQ" id="mcq_criticalQ" class="form-control criticalQ" value="1" <?php echo ( ! empty($qresult['critical'])) ? 'checked="checked"' : ''; ?> />
																	</div>
																</div>
																<div class="quesbox-flex">
																	<div class="form-group">
																		<input type="text" name="mcq" id="mcq" class="form-control QuesForm inputtextWrap mcq" placeholder="Click here to enter question" value="<?php echo ( ! empty($qresult['questions'])) ? stripslashes($qresult['questions']) : ''; ?>" />
																		 <div class="mcqq_assets_container" <?php echo ( ! empty($ques_att) && $ques_type == 4) ? '' : 'style="display:none;"'; ?>>
																			<div class="assets_data mcqq_assets_data"><a class="view_assets" data-src="<?php echo $path . $ques_att; ?>" href="javascript:;"><?php echo $ques_att; ?></a></div>
																			<a href="javascript:void(0);" data-assets="<?php echo $ques_att; ?>" data-assets-id="mcqq_assets" data-input-id="#mcqq_qaudio,#mcqq_rec_audio" data-path="<?php echo $uploadpath ?>" class="delete_assets" title="Delete"><i class="fa fa-times" aria-hidden="true"></i></a>
																		</div>
																		<ul class="QueBoxIcon">
																			<li class="dropdown">
																				<button class="btn1 btn-secondary dropdown-toggle mcqq_assets <?php echo ( ! empty($ques_att) && $ques_type == 4) ? 'disabled' : ''; ?>" <?php echo ( ! empty($ques_att) && $ques_type == 4) ? 'disabled="disabled"' : ''; ?> type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img class="img-fluid" src="img/list/audio.svg"></button>
																				<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
																					<a class="dropdown-item">
																						<div class="tooltipLeft uploadicon">
																							<label for="file-input-mcqq-addAudio"><img class="img-fluid" src="img/list/add_audio.svg" /><span class="tooltiptext">Add Audio</span></label>
																							<input id="file-input-mcqq-addAudio" data-id="#mcqq_qaudio" data-assets="mcqq_assets" class="uploadAudioFile" type="file" name="mcqq_Audio" />
																							<input type="hidden" name="mcqq_qaudio" id="mcqq_qaudio" value="<?php echo ($ques_type == 4 && $qatt_type == 'qa') ? $ques_att : ''; ?>" />
																						</div>
																					</a>
																					<a class="dropdown-item">
																						<div class="tooltipLeft">
																							<img class="img-fluid rec-audio" data-input-id="mcqq_rec_audio" data-assets="mcqq_assets" src="img/list/record_audio.svg" />
																							<span class="tooltiptext">Record Audio</span>
																						</div>
																						<input type="hidden" name="mcqq_rec_audio" id="mcqq_rec_audio" />
																					 </a>
																					<a data-toggle="modal" data-target="#mcqqtts" class="dropdown-item">
																						<div class="tooltipLeft">
																							<img class="img-fluid" src="img/list/text_speech.svg" />
																							<span class="tooltiptext">Text to Speech</span>
																						</div>
																					 </a>
																				 </div>
																			</li>
																		</ul>
																	</div>
																</div>
																<div class="q-icon-banner clearfix">                                                            	
																	<ul class="QueBoxIcon QueBoxIcon1">
																		<h5>Add Assets</h5>
																		<li class="dropdown">
																			<button class="btn1 btn-secondary dropdown-toggle mcq_assets <?php echo ( ! empty($ques_assets) && $ques_type == 4) ? 'disabled' : ''; ?>" <?php echo ( ! empty($ques_assets) && $ques_type == 4) ? 'disabled="disabled"' : ''; ?> type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img class="img-fluid" src="img/list/audio.svg" /></button>
																			<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
																				<a data-toggle="modal" data-target="#mcqtts" class="dropdown-item">
																					<div class="tooltip1">
																						<img class="img-fluid" src="img/list/text_speech.svg">
																						<span class="tooltiptext">Text to Speech</span>
																					</div>
																				</a>
																				<a class="dropdown-item">
																					<div class="tooltip1 uploadicon">
																						<label for="file-input-mcq-addAudio">
																							<img class="img-fluid" src="img/list/add_audio.svg" />
																							<span class="tooltiptext">Add Audio</span>
																						</label>
																						<input id="file-input-mcq-addAudio" data-id="#mcq_audio" data-assets="mcq_assets" class="uploadAudioFile" type="file" name="mcq_Audio"/>
																						<input type="hidden" name="mcq_audio" id="mcq_audio" value="<?php echo ($ques_type == 4 && $assets_type == 'a') ? $ques_assets : ''; ?>" />
																					</div>
																				</a>
																				<a class="dropdown-item">
																					<div class="tooltip1">
																						<img class="img-fluid rec-audio" data-input-id="mcq_rec_audio" data-assets="mcq_assets" src="img/list/record_audio.svg" />
																						<span class="tooltiptext">Record Audio</span>
																					</div>
																					<input type="hidden" name="mcq_rec_audio" id="mcq_rec_audio" />
																				 </a>
																			</div>
																		</li>
																		<li class="dropdown">
																			<button class="btn1 btn-secondary dropdown-toggle mcq_assets <?php echo ( ! empty($ques_assets) && $ques_type == 4) ? 'disabled' : ''; ?>" <?php echo ( ! empty($ques_assets) && $ques_type == 4) ? 'disabled="disabled"' : ''; ?> type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img class="img-fluid" src="img/list/video.svg"></button>
																			<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
																				<a class="dropdown-item">
																					<div class="tooltip1 uploadicon">
																						<label for="file-input-mcq-addVideo">
																							<img class="img-fluid" src="img/list/add_video.svg" />
																							<span class="tooltiptext">Add Video</span>
																						</label>
																						<input id="file-input-mcq-addVideo" data-id="#mcq_video" data-assets="mcq_assets" class="uploadVideoFile" type="file" name="mcq_Video" />
																						<input type="hidden" name="mcq_video" id="mcq_video" value="<?php echo ($ques_type == 4 && $assets_type == 'v') ? $ques_assets : ''; ?>" />
																					</div>
																				</a>
																				<a class="dropdown-item">
																					<div class="tooltip1">
																						<img class="img-fluid rec-video" data-input-id="mcq_rec_video" data-assets="mcq_assets" src="img/qus_icon/rec_video.svg" />
																						<span class="tooltiptext">Record Video</span>
																					 </div>
																					 <input type="hidden" name="mcq_rec_video" id="mcq_rec_video" />
																				</a>
																				<a class="dropdown-item">
																					<div class="tooltip1">
																						<img class="img-fluid rec-screen" data-input-id="mcq_rec_screen" data-assets="mcq_assets" src="img/list/screen_record.svg" />
																						<span class="tooltiptext">Record Screen</span>
																					 </div>
																					 <input type="hidden" name="mcq_rec_screen" id="mcq_rec_screen" value="<?php echo ($ques_type == 4 && $assets_type == 's') ? $ques_assets : ''; ?>" />
																				</a>
																			</div>
																		</li>
																		<li>
																			<div class="tooltip uploadicon mcq_assets <?php echo ( ! empty($ques_assets) && $ques_type == 4) ? 'disabled' : ''; ?>" <?php echo ( ! empty($ques_assets) && $ques_type == 4) ? 'disabled="disabled"' : ''; ?>>
																				<label for="file-input-mcq-addImg">
																					<img class="img-fluid" src="img/list/image.svg" />
																					<span class="tooltiptext">Add Image</span>
																				</label>
																				<input id="file-input-mcq-addImg" data-id="#mcq_img" data-assets="mcq_assets" class="uploadImgFile mcq_assets" type="file" name="mcq_Img" <?php echo ( ! empty($ques_assets) && $ques_type == 4) ? 'disabled="disabled"' : ''; ?>/>
																				<input type="hidden" name="mcq_img" id="mcq_img" value="<?php echo ($ques_type == 4 && $assets_type == 'i') ? $ques_assets : ''; ?>" />
																			</div>
																		</li>
																		<li>
																			<div class="tooltip uploadicon mcq_assets <?php echo ( ! empty($ques_assets) && $ques_type == 4) ? 'disabled' : ''; ?>" <?php echo ( ! empty($ques_assets) && $ques_type == 4) ? 'disabled="disabled"' : ''; ?>>
																				<label for="file-input-mcq-addDoc">
																					<img class="img-fluid" src="img/list/add_document.svg" />
																					<span class="tooltiptext">Add Document</span>
																				</label>
																				<input id="file-input-mcq-addDoc" data-id="#mcq_doc" data-assets="mcq_assets" class="uploadDocFile mcq_assets" type="file" name="mcq_Doc"/>
																				<input type="hidden" name="mcq_doc" id="mcq_doc" value="<?php echo ($ques_type == 4 && $assets_type == 'd') ? $ques_assets : ''; ?>"/>
																			</div>
																		</li>
																	</ul>
																	<div class="Ques-comp">
																		<h5>Add Competency score</h5>
																		<?php if ( ! empty($sim_com['comp_col_1']) && ! empty($sim_com['comp_val_1'])): ?>
																		<div class="form-group QusScore">
																			<input type="text" class="form-control mcq" name="mcq_ques_val_1" id="mcq_ques_val_1" onkeypress="return isNumberKey(event);" value="<?php echo $qresult['ques_val_1'] ?>" />
																		</div>
																		<input type="hidden" class="form-control" id="mcq_ques_val_1h" value="<?php echo $qresult['ques_val_1'] ?>" />
																		<?php else: ?>
																		<input type="hidden" class="form-control" name="mcq_ques_val_1" value="0" />
																		<?php endif; ?>
																		
																		<?php if ( ! empty($sim_com['comp_col_2']) && ! empty($sim_com['comp_val_2'])): ?>
																		<div class="form-group QusScore">
																			<input type="text" class="form-control mcq" name="mcq_ques_val_2" id="mcq_ques_val_2" onkeypress="return isNumberKey(event);" value="<?php echo $qresult['ques_val_2'] ?>" />
																		</div>
																		<input type="hidden" class="form-control" id="mcq_ques_val_2h" value="<?php echo $qresult['ques_val_2'] ?>" />
																		<?php else: ?>
																		<input type="hidden" class="form-control" name="mcq_ques_val_2" value="0" />
																		<?php endif; ?>
																		
																		<?php if ( ! empty($sim_com['comp_col_3']) && ! empty($sim_com['comp_val_3'])): ?>
																		<div class="form-group QusScore">
																			<input type="text" class="form-control mcq" name="mcq_ques_val_3" id="mcq_ques_val_3" onkeypress="return isNumberKey(event);" value="<?php echo $qresult['ques_val_3'] ?>" />
																		</div>
																		<input type="hidden" class="form-control" id="mcq_ques_val_3h" value="<?php echo $qresult['ques_val_3'] ?>" />
																		<?php else: ?>
																		<input type="hidden" class="form-control" name="mcq_ques_val_3" value="0" />
																		<?php endif; ?>
																		
																		<?php if ( ! empty($sim_com['comp_col_4']) && ! empty($sim_com['comp_val_4'])): ?>
																		<div class="form-group QusScore">
																			<input type="text" class="form-control mcq" name="mcq_ques_val_4" id="mcq_ques_val_4" onkeypress="return isNumberKey(event);" value="<?php echo $qresult['ques_val_4'] ?>" />
																		</div>
																		<input type="hidden" class="form-control" id="mcq_ques_val_4h" value="<?php echo $qresult['ques_val_4'] ?>" />
																		<?php else: ?>
																		<input type="hidden" class="form-control" name="mcq_ques_val_4" value="0" />
																		<?php endif; ?>
																		
																		<?php if ( ! empty($sim_com['comp_col_5']) && ! empty($sim_com['comp_val_5'])): ?>
																		<div class="form-group QusScore">
																			<input type="text" class="form-control mcq" name="mcq_ques_val_5" id="mcq_ques_val_5" onkeypress="return isNumberKey(event);" value="<?php echo $qresult['ques_val_5'] ?>" />
																		</div>
																		<input type="hidden" class="form-control" id="mcq_ques_val_5h" value="<?php echo $qresult['ques_val_5'] ?>" />
																		<?php else: ?>
																		<input type="hidden" class="form-control" name="mcq_ques_val_5h" value="0" />
																		<?php endif; ?>
																		
																		<?php if ( ! empty($sim_com['comp_col_6']) && ! empty($sim_com['comp_val_6'])): ?>
																		<div class="form-group QusScore">
																			<input type="text" class="form-control mcq" name="mcq_ques_val_6" id="mcq_ques_val_6" onkeypress="return isNumberKey(event);" value="<?php echo $qresult['ques_val_6'] ?>" />
																		</div>
																		<input type="hidden" class="form-control" id="mcq_ques_val_6h" value="<?php echo $qresult['ques_val_6'] ?>" />
																		<?php else: ?>
																		<input type="hidden" class="form-control" name="mcq_ques_val_6" value="0" />
																		<?php endif; ?>
																	</div>
																</div>
																<div class="mcq_assets_container" <?php echo ( ! empty($ques_assets) && $ques_type == 4) ? '' : 'style="display:none;"'; ?>>
																	<div class="assets_data mcq_assets_data"><a class="view_assets" data-src="<?php echo $path . $ques_assets; ?>" href="javascript:;"><?php echo $ques_assets; ?></a></div>
																	<a href="javascript:void(0);" data-assets="<?php echo $ques_assets ?>" data-assets-id="mcq_assets" data-input-id="#mcq_audio,#mcq_rec_audio,#mcq_video,#mcq_rec_video,#mcq_rec_screen,#mcq_img,#mcq_doc" data-path="<?php echo $uploadpath ?>" class="delete_assets" title="Delete"><i class="fa fa-times" aria-hidden="true"></i></a>
																</div>
															</div>
														</div>
													</div>
													<div class="form-popup draggable Ques-pop-style modal" id="mcqqtts">
														<div class="popheading">Insert Text to Speech</div>
														<div class="textarea">
															<textarea type="text" class="form-control1 inputtextWrap" name="mcqq_text_to_speech" id="mcqq_text_to_speech"><?php echo ($ques_type == 4 && ! empty($qresult['qspeech_text'])) ? stripslashes($qresult['qspeech_text']) : ''; ?></textarea>
														</div>
														<div class="modal-footer">
															<button type="button" class="btn1 submitbtn1" data-dismiss="modal">Insert</button>
															<button type="button" class="btn1 submitbtn1" onclick="clearData('mcqq_text_to_speech');">Clear</button>
															<button type="button" class="btn1 submitbtn1" data-dismiss="modal">Cancel</button>
														</div>
													</div>
													<div class="form-popup draggable Ques-pop-style modal" id="mcqtts">
														<div class="popheading">Insert Text to Speech</div>
														<div class="textarea">
															<textarea type="text" class="form-control1 inputtextWrap" name="mcq_text_to_speech" id="mcq_text_to_speech"><?php echo ($ques_type == 4 && ! empty($qresult['speech_text'])) ? stripslashes($qresult['speech_text']) : ''; ?></textarea>
														</div>
														<div class="modal-footer">
															<button type="button" class="btn1 submitbtn1" data-dismiss="modal">Insert</button>
															<button type="button" class="btn1 submitbtn1" onclick="clearData('mcq_text_to_speech');">Clear</button>
															<button type="button" class="btn1 submitbtn1" data-dismiss="modal">Cancel</button>
														</div>
													</div>
													<div class="Ansbox">
														<div class="row">
															<div class="col-sm-12">
																<h3 class="AnsHeading matcht">CHOICE</h3>
																<div class="Check-box suffel suffe2">
																	<div class="tooltiptop"><span class="tooltiptext">Shuffle ON</span></div>
																	<label class="checkstyle">
																		<input type="checkbox" name="mcq_shuffle" class="suffleCheck" value="1" <?php echo ( ! empty($qresult['shuffle'])) ? 'checked="checked"' : ''; ?> />
																		<span class="checkmark"></span><span class="shuffleT"> Shuffle Option</span>
																	</label>
																</div>
																<div class="MCQApendOption">
																<?php 
																$mcq_sql = "SELECT answer_id, choice_option FROM answer_tbl WHERE question_id = '". $qresult['question_id'] ."'";
																$mcq_res = $db->prepare($mcq_sql); $mcq_res->execute();
																if ($mcq_res->rowCount() > 0): $mcqi = 1;
																foreach ($mcq_res->fetchAll(PDO::FETCH_ASSOC) as $mcq_res_row): ?>
																<div class="Radio-box MCQRadio">
																	<input type="hidden" name="updateMCQAnswerid[]" value="<?php echo $mcq_res_row['answer_id'] ?>" />
																	<label class="radiostyle">
																		<input type="radio" name="mcq_true_option" value="<?php echo $mcqi ?>" <?php echo ($mcqi == $qresult['true_options']) ? 'checked="checked"' : ''; ?> /><span class="radiomark"></span>
																	 </label>
																	<div class="col-sm-12 mcwidth">
																		<div class="choicebox">
																			<div class="form-group">
																			   <input type="text" name="mcq_option[]" id="mcq_option" class="form-control inputtextWrap mcq" placeholder="Option text goes here" value="<?php echo stripslashes($mcq_res_row['choice_option']); ?>"/>
																			</div>
																		</div>
																	</div>
																	<?php if ($mcqi == 1): ?>
																	<div class="AddAns-Option"><img class="img-fluid MCQAdd" src="img/list/add_field.svg"></div>
																	<?php else: ?>
																	<div class="AddAns-Option removeOption" data-remove-answer-id="<?php echo $mcq_res_row['answer_id'] ?>"><img class="img-fluid" src="img/list/delete_field.svg"></div>
																	<?php endif; ?>
																</div>
																<?php $mcqi++; endforeach; else: ?>
																<div class="Radio-box MCQRadio">
																	<label class="radiostyle">
																		<input type="radio" name="mcq_true_option" value="1" checked /><span class="radiomark"></span>
																	 </label>
																	<div class="col-sm-12 mcwidth">
																		<div class="choicebox">
																			<div class="form-group">
																			   <input type="text" name="mcq_option[]" id="mcq_option" class="form-control inputtextWrap mcq" placeholder="Option text goes here" />
																			</div>
																		</div>
																	</div>
																	<div class="AddAns-Option"><img class="img-fluid MCQAdd" src="img/list/add_field.svg"></div>
																</div>
																<?php endif; ?>
																</div>
															</div>
														</div>
													</div>
												</div>
												<div id="MCQ-F" class="tab-pane fade in">
													<div class="col-sm-12 Qusfeedback">
														<div class="form-group">
															<div class="feedback-flex">
																<h4>Correct</h4>
																<textarea class="form-control FeedForm inputtextWrap mcq" name="mcq_cfeedback" id="mcq_cfeedback" placeholder="That's right! You selected the correct response."><?php echo ($ques_type == 4 && ! empty($feedResult[0]['feedback']) && $feedResult[0]['feedback_type'] == 1) ? stripslashes($feedResult[0]['feedback']) : ''; ?></textarea>
															</div>
															<ul class="QueBoxIcon feed LMfeed">
																<h5>Add assets</h5>
																<li class="dropdown">
																	<button class="btn1 btn-secondary dropdown-toggle mcqf_assets <?php echo ( ! empty($qfeed_assets) && $ques_type == 4) ? 'disabled' : ''; ?>" <?php echo ( ! empty($qfeed_assets) && $ques_type == 4) ? 'disabled="disabled"' : ''; ?> type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img class="img-fluid" src="img/list/audio.svg"></button>
																	<div class="dropdown-menu buttom" aria-labelledby="dropdownMenuButton">
																		<a data-toggle="modal" data-target="#mcqfctts" class="dropdown-item">
																			<div class="tooltip1">
																				<img class="img-fluid" src="img/list/text_speech.svg">
																				<span class="tooltiptext">Text to Speech</span>
																			</div>
																		</a>
																		<a class="dropdown-item">
																			<div class="tooltip1 uploadicon">
																				<label for="file-input-mcq-feedback-addAudio">
																					<img class="img-fluid" src="img/list/add_audio.svg">
																					<span class="tooltiptext">Add Audio</span>
																				</label>
																				<input id="file-input-mcq-feedback-addAudio" data-id="#mcq_feedback_audio" data-assets="mcqf_assets" class="uploadAudioFile" type="file" name="mcq_feedback_Audio"/>
																				<input type="hidden" name="mcq_feedback_audio" id="mcq_feedback_audio" value="<?php echo ($ques_type == 4 && $feed_assets_type == 'fa') ? $qfeed_assets : ''; ?>" />
																			</div>
																		</a>
																		<a class="dropdown-item">
																			<div class="tooltip1">
																				<img class="img-fluid rec-audio" data-input-id="mcq_feedback_rec_audio" data-assets="mcqf_assets" src="img/list/record_audio.svg">
																				<span class="tooltiptext">Record Audio</span>
																			</div>
																			<input type="hidden" name="mcq_feedback_rec_audio" id="mcq_feedback_rec_audio" />
																		 </a>
																	</div>
																</li>
																<li class="dropdown">
																	<button class="btn1 btn-secondary dropdown-toggle mcqf_assets <?php echo ( ! empty($qfeed_assets) && $ques_type == 4) ? 'disabled' : ''; ?>" <?php echo ( ! empty($qfeed_assets) && $ques_type == 4) ? 'disabled="disabled"' : ''; ?> type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img class="img-fluid" src="img/list/video.svg"></button>
																	<div class="dropdown-menu buttom" aria-labelledby="dropdownMenuButton">
																		<a class="dropdown-item">
																			<div class="tooltip1 uploadicon">
																				<label for="file-input-mcq-feedback-addVideo">
																					<img class="img-fluid" src="img/list/add_video.svg">
																					<span class="tooltiptext">Add Video</span>
																				</label>
																				<input id="file-input-mcq-feedback-addVideo" data-id="#mcq_feedback_video" data-assets="mcqf_assets" class="uploadVideoFile" type="file" name="mcq_feedback_Video" />
																				<input type="hidden" name="mcq_feedback_video" id="mcq_feedback_video" value="<?php echo ($ques_type == 4 && $feed_assets_type == 'fv') ? $qfeed_assets : ''; ?>" />
																			</div>
																		</a>
																		<a class="dropdown-item">
																			<div class="tooltip1">
																				<img class="img-fluid rec-video" data-input-id="mcq_feedback_rec_video" data-assets="mcqf_assets" src="img/qus_icon/rec_video.svg">
																				<span class="tooltiptext">Record Video</span>
																			 </div>
																			 <input type="hidden" name="mcq_feedback_rec_video" id="mcq_feedback_rec_video" />
																		</a>
																		<a class="dropdown-item">
																			<div class="tooltip1">
																				<img class="img-fluid rec-screen" data-input-id="mcq_feedback_rec_screen" data-assets="mcqf_assets" src="img/list/screen_record.svg">
																				<span class="tooltiptext">Record Screen</span>
																			 </div>
																			 <input type="hidden" name="mcq_feedback_rec_screen" id="mcq_feedback_rec_screen" value="<?php echo ($ques_type == 4 && $feed_assets_type == 'fs') ? $qfeed_assets : ''; ?>" />
																		</a>
																	</div>
																</li>
																<li>
																	<div class="tooltip uploadicon mcqf_assets <?php echo ( ! empty($qfeed_assets) && $ques_type == 4) ? 'disabled' : ''; ?>" <?php echo ( ! empty($qfeed_assets) && $ques_type == 4) ? 'disabled="disabled"' : ''; ?>>
																		<label for="file-input-mcq-feedback-addImg">
																			<img class="img-fluid" src="img/list/image.svg">
																			<span class="tooltiptext">Add Image</span>
																		</label>
																		<input id="file-input-mcq-feedback-addImg" data-id="#mcq_feedback_img" data-assets="mcqf_assets" class="uploadImgFile mcqf_assets" type="file" name="mcq_feedback_Img" <?php echo (!empty($qfeed_assets) && $ques_type == 4) ? 'disabled="disabled"' : ''; ?>/>
																		<input type="hidden" name="mcq_feedback_img" id="mcq_feedback_img" value="<?php echo ($ques_type == 4 && $feed_assets_type == 'fi') ? $qfeed_assets : ''; ?>" />
																	</div>
																</li>
																<li>
																	<div class="tooltip uploadicon mcqf_assets <?php echo ( ! empty($qfeed_assets) && $ques_type == 4) ? 'disabled' : ''; ?>" <?php echo ( ! empty($qfeed_assets) && $ques_type == 4) ? 'disabled="disabled"' : ''; ?>>
																		<label for="file-input-mcq-feedback-addDoc">
																			<img class="img-fluid" src="img/list/add_document.svg" />
																			<span class="tooltiptext">Add Document</span>
																		</label>
																		<input id="file-input-mcq-feedback-addDoc" data-id="#mcq_feedback_doc" data-assets="mcqf_assets" class="uploadDocFile mcqf_assets" type="file" name="mcq_feedback_Doc" <?php echo ( ! empty($qfeed_assets) && $ques_type == 4) ? 'disabled="disabled"' : ''; ?>/>
																		<input type="hidden" name="mcq_feedback_doc" id="mcq_feedback_doc" value="<?php echo ($ques_type == 4 && $feed_assets_type == 'fd') ? $qfeed_assets : ''; ?>" />
																	</div>
																</li>
															</ul>                              
														</div>
														<div class="mcqf_assets_container" <?php echo ( ! empty($qfeed_assets) && $ques_type == 4) ? '' : 'style="display:none;"'; ?>>
															<div class="assets_data mcqf_assets_data"><a class="view_assets" data-src="<?php echo $path . $qfeed_assets; ?>" href="javascript:;"><?php echo $qfeed_assets; ?></a></div>
															<a href="javascript:void(0);" data-assets="<?php echo $qfeed_assets ?>" data-assets-id="mcqf_assets" data-input-id="#mcq_feedback_audio,#mcq_feedback_rec_audio,#mcq_feedback_video,#mcq_feedback_rec_video,#mcq_feedback_rec_screen,#mcq_feedback_img" data-path="<?php echo $uploadpath ?>" class="delete_assets" title="Delete"><i class="fa fa-times" aria-hidden="true"></i></a>
														</div>
														<div class="form-group">
															<div class="feedback-flex">
																<h4>Incorrect</h4>
																<textarea class="form-control FeedForm inputtextWrap mcq" name="mcq_ifeedback" id="mcq_ifeedback" placeholder="You did not select the correct response."><?php echo ($ques_type == 4 && ! empty($feedResult[1]['feedback']) && $feedResult[1]['feedback_type'] == 2) ? stripslashes($feedResult[1]['feedback']) : ''; ?></textarea>
															</div>
															<ul class="QueBoxIcon feed LMfeed">
																<h5>Add assets</h5>
																<li class="dropdown">
																	<button class="btn1 btn-secondary dropdown-toggle mcqfi_assets <?php echo ( ! empty($qifeed_assets) && $ques_type == 4) ? 'disabled' : ''; ?>" <?php echo ( ! empty($qifeed_assets) && $ques_type == 4) ? 'disabled="disabled"' : ''; ?> type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img class="img-fluid" src="img/list/audio.svg"></button>
																	<div class="dropdown-menu buttom" aria-labelledby="dropdownMenuButton">
																		<a data-toggle="modal" data-target="#mcqfitts" class="dropdown-item">
																			<div class="tooltip1">
																				<img class="img-fluid" src="img/list/text_speech.svg">
																				<span class="tooltiptext">Text to Speech</span>
																			</div>
																		</a>
																		<a class="dropdown-item">
																			<div class="tooltip1 uploadicon">
																				<label for="file-input-mcqi-feedback-addAudio">
																					<img class="img-fluid" src="img/list/add_audio.svg">
																					<span class="tooltiptext">Add Audio</span>
																				</label>
																				<input id="file-input-mcqi-feedback-addAudio" data-id="#mcqi_feedback_audio" data-assets="mcqfi_assets" class="uploadAudioFile" type="file" name="mcqi_feedback_Audio"/>
																				<input type="hidden" name="mcqi_feedback_audio" id="mcqi_feedback_audio" value="<?php echo ($ques_type == 4 && $feed_assets_type == 'fa') ? $qifeed_assets : ''; ?>" />
																			</div>
																		</a>
																		<a class="dropdown-item">
																			<div class="tooltip1">
																				<img class="img-fluid rec-audio" data-input-id="mcqi_feedback_rec_audio" data-assets="mcqfi_assets" src="img/list/record_audio.svg">
																				<span class="tooltiptext">Record Audio</span>
																			</div>
																			<input type="hidden" name="mcqi_feedback_rec_audio" id="mcqi_feedback_rec_audio" />
																		 </a>
																	</div>
																</li>
																<li class="dropdown">
																	<button class="btn1 btn-secondary dropdown-toggle mcqfi_assets <?php echo ( ! empty($qifeed_assets) && $ques_type == 4) ? 'disabled' : ''; ?>" <?php echo ( ! empty($qifeed_assets) && $ques_type == 4) ? 'disabled="disabled"' : ''; ?> type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img class="img-fluid" src="img/list/video.svg"></button>
																	<div class="dropdown-menu buttom" aria-labelledby="dropdownMenuButton">
																		<a class="dropdown-item">
																			<div class="tooltip1 uploadicon">
																				<label for="file-input-mcqi-feedback-addVideo">
																					<img class="img-fluid" src="img/list/add_video.svg">
																					<span class="tooltiptext">Add Video</span>
																				</label>
																				<input id="file-input-mcqi-feedback-addVideo" data-id="#mcqi_feedback_video" data-assets="mcqfi_assets" class="uploadVideoFile" type="file" name="mcqi_feedback_Video" />
																				<input type="hidden" name="mcqi_feedback_video" id="mcqi_feedback_video" value="<?php echo ($ques_type == 4 && $feed_assets_type == 'fv') ? $qifeed_assets : ''; ?>" />
																			</div>
																		</a>
																		<a class="dropdown-item">
																			<div class="tooltip1">
																				<img class="img-fluid rec-video" data-input-id="mcqi_feedback_rec_video" data-assets="mcqfi_assets" src="img/qus_icon/rec_video.svg">
																				<span class="tooltiptext">Record Video</span>
																			 </div>
																			 <input type="hidden" name="mcqi_feedback_rec_video" id="mcqi_feedback_rec_video" />
																		</a>
																		<a class="dropdown-item">
																			<div class="tooltip1">
																				<img class="img-fluid rec-screen" data-input-id="mcqi_feedback_rec_screen" data-assets="mcqfi_assets" src="img/list/screen_record.svg">
																				<span class="tooltiptext">Record Screen</span>
																			 </div>
																			 <input type="hidden" name="mcqi_feedback_rec_screen" id="mcqi_feedback_rec_screen" value="<?php echo ($ques_type == 4 && $feed_assets_type == 'fs') ? $qifeed_assets : ''; ?>" />
																		</a>
																	</div>
																</li>
																<li>
																	<div class="tooltip uploadicon mcqfi_assets <?php echo ( ! empty($qifeed_assets) && $ques_type == 4) ? 'disabled' : ''; ?>" <?php echo ( ! empty($qifeed_assets) && $ques_type == 4) ? 'disabled="disabled"' : ''; ?>>
																		<label for="file-input-mcqi-feedback-addImg">
																			<img class="img-fluid" src="img/list/image.svg">
																			<span class="tooltiptext">Add Image</span>
																		</label>
																		<input id="file-input-mcqi-feedback-addImg" data-id="#mcqi_feedback_img" data-assets="mcqfi_assets" class="uploadImgFile mcqfi_assets" type="file" name="mcqi_feedback_Img" <?php echo ( ! empty($qifeed_assets) && $ques_type == 4) ? 'disabled="disabled"' : ''; ?> />
																		<input type="hidden" name="mcqi_feedback_img" id="mcqi_feedback_img" value="<?php echo ($ques_type == 4 && $feed_assets_type == 'fi') ? $qifeed_assets : ''; ?>" />
																	</div>
																</li>
																<li>
																	<div class="tooltip uploadicon mcqfi_assets <?php echo ( ! empty($qifeed_assets) && $ques_type == 4) ? 'disabled' : ''; ?>" <?php echo ( ! empty($qifeed_assets) && $ques_type == 4) ? 'disabled="disabled"' : ''; ?>>
																		<label for="file-input-mcqi-feedback-addDoc">
																			<img class="img-fluid" src="img/list/add_document.svg" />
																			<span class="tooltiptext">Add Document</span>
																		</label>
																		<input id="file-input-mcqi-feedback-addDoc" data-id="#mcqi_feedback_doc" data-assets="mcqfi_assets" class="uploadDocFile mcqfi_assets" type="file" name="mcqi_feedback_Doc" <?php echo ( ! empty($qifeed_assets) && $ques_type == 4) ? 'disabled="disabled"' : ''; ?>/>
																		<input type="hidden" name="mcqi_feedback_doc" id="mcqi_feedback_doc" value="<?php echo ($ques_type == 4 && $feed_assets_type == 'fd') ? $qifeed_assets : ''; ?>" />
																	</div>
																</li>
															</ul>                              
														</div>
														<div class="mcqfi_assets_container" <?php echo ( ! empty($qifeed_assets) && $ques_type == 4) ? '' : 'style="display:none;"'; ?>>
															<div class="assets_data mcqfi_assets_data"><a class="view_assets" data-src="<?php echo $path . $qifeed_assets; ?>" href="javascript:;"><?php echo $qifeed_assets; ?></a></div>
															<a href="javascript:void(0);" data-assets="<?php echo $qifeed_assets ?>" data-assets-id="mcqfi_assets" data-input-id="#mcqi_feedback_audio,#mcqi_feedback_rec_audio,#mcqi_feedback_video,#mcqi_feedback_rec_video,#mcqi_feedback_rec_screen,#mcqi_feedback_img" data-path="<?php echo $uploadpath ?>" class="delete_assets" title="Delete"><i class="fa fa-times" aria-hidden="true"></i></a>
														</div>
													</div>
													<div class="form-popup draggable Ques-pop-style modal" id="mcqfctts">
														<div class="popheading">Insert Text to Speech</div>
														<div class="textarea">
															<textarea type="text" class="form-control1 inputtextWrap" name="mcq_feedback_text_to_speech" id="mcq_feedback_text_to_speech"><?php echo ($ques_type == 4 && ! empty($feedResult[0]['feedback']) && $feedResult[0]['feedback_type'] == 1) ? stripslashes($feedResult[0]['feed_speech_text']) : ''; ?></textarea>
														</div>
														<div class="modal-footer">
															<button type="button" class="btn1 submitbtn1" data-dismiss="modal">Insert</button>
															<button type="button" class="btn1 submitbtn1" onclick="clearData('mcq_feedback_text_to_speech');">Clear</button>
															<button type="button" class="btn1 submitbtn1" data-dismiss="modal">Cancel</button>
														</div>
													</div>
													<div class="form-popup draggable Ques-pop-style modal" id="mcqfitts">
														<div class="popheading">Insert Text to Speech</div>
														<div class="textarea">
															<textarea type="text" class="form-control1 inputtextWrap" name="mcqi_feedback_text_to_speech" id="mcqi_feedback_text_to_speech"><?php echo ($ques_type == 4 && ! empty($feedResult[1]['feedback']) && $feedResult[1]['feedback_type'] == 2) ? stripslashes($feedResult[1]['feed_speech_text']) : ''; ?></textarea>
														</div>
														<div class="modal-footer">
															<button type="button" class="btn1 submitbtn1" data-dismiss="modal">Insert</button>
															<button type="button" class="btn1 submitbtn1" onclick="clearData('mcqi_feedback_text_to_speech');">Clear</button>
															<button type="button" class="btn1 submitbtn1" data-dismiss="modal">Cancel</button>
														</div>
													</div>
												</div>
											 </div>
										   </div>
									   </div>
									</div>
									
									<!--MMCQ-Question-->
									<div id="MMCQQuestion" class="tab-pane fade <?php echo ($ques_type == 5) ? 'in active' : ''; ?>">
										<div class="match-Tab widthQ100">
											<ul class="nav nav-tabs">
												<li class="active"><a data-toggle="tab" href="#MMCQ-Q">Question</a></li>
												<li><a data-toggle="tab" href="#MMCQ-F">Feedback</a></li>
											</ul>
										</div>
										<div class="container-fluid">
											<div class="widthQ100 multitemp">
												<div class="tab-content">
													<div id="MMCQ-Q" class="match-Q tab-pane fade in active">
														<div class="quesbox">
															<div class="row">
																<div class="col-sm-12 Questiontmpbanner Qtempmmcq">
																	<div class="critical_div">
																		<h3 class="qusHeading">QUESTION <p class="question-type">(MMCQ)</p></h3>
																		<div class="CIRtiCaltext">
																			Critical Question: <input type="checkbox" name="mmcq_criticalQ" id="mmcq_criticalQ" class="form-control criticalQ" value="1" <?php echo ( ! empty($qresult['critical'])) ? 'checked="checked"' : ''; ?> />
																		</div>
																	</div>
																	<div class="quesbox-flex">
																		<div class="form-group">
																			<input type="text" name="mmcq" id="mmcq" class="form-control QuesForm inputtextWrap mmcq" placeholder="Click here to enter question" value="<?php echo ( ! empty($qresult['questions'])) ? stripslashes($qresult['questions']) : ''; ?>"/>
																			<div class="mmcqq_assets_container" <?php echo ( ! empty($ques_att) && $ques_type == 5) ? '' : 'style="display:none;"'; ?>>
																				<div class="assets_data mmcqq_assets_data"><a class="view_assets" data-src="<?php echo $path . $ques_att; ?>" href="javascript:;"><?php echo $ques_att; ?></a></div>
																				<a href="javascript:void(0);" data-assets="<?php echo $ques_att; ?>" data-assets-id="mmcqq_assets" data-input-id="#mmcqq_qaudio,#mmcqq_rec_audio" data-path="<?php echo $uploadpath ?>" class="delete_assets" title="Delete"><i class="fa fa-times" aria-hidden="true"></i></a>
																			</div>
																			<ul class="QueBoxIcon">
																				<li class="dropdown">
																					<button class="btn1 btn-secondary dropdown-toggle mmcqq_assets <?php echo ( ! empty($ques_att) && $ques_type == 5) ? 'disabled' : ''; ?>" <?php echo ( ! empty($ques_att) && $ques_type == 5) ? 'disabled="disabled"' : ''; ?> type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img class="img-fluid" src="img/list/audio.svg"></button>
																					<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
																						<a class="dropdown-item">
																							<div class="tooltipLeft uploadicon">
																								<label for="file-input-mmcqq-addAudio"><img class="img-fluid" src="img/list/text_speech.svg" /><span class="tooltiptext">Add Audio</span></label>
																								<input id="file-input-mmcqq-addAudio" data-id="#mmcqq_qaudio" data-assets="mmcqq_assets" class="uploadAudioFile" type="file" name="mmcqq_Audio" />
																								<input type="hidden" name="mmcqq_qaudio" id="mmcqq_qaudio" value="<?php echo ($ques_type == 5 && $qatt_type == 'qa') ? $ques_att : ''; ?>" />
																							</div>
																						</a>
																						<a class="dropdown-item">
																							<div class="tooltipLeft">
																								<img class="img-fluid rec-audio" data-input-id="mmcqq_rec_audio" data-assets="mmcqq_assets" src="img/list/add_audio.svg" />
																								<span class="tooltiptext">Record Audio</span>
																							</div>
																							<input type="hidden" name="mmcqq_rec_audio" id="mmcqq_rec_audio" />
																						</a>
																						<a data-toggle="modal" data-target="#mmcqqtts" class="dropdown-item">
																							<div class="tooltipLeft">
																								<img class="img-fluid" src="img/list/record_audio.svg" />
																								<span class="tooltiptext">Text to Speech</span>
																							</div>
																						</a>
																					</div>
																				</li>
																			</ul>
																		</div>
																	</div>
																	<div class="q-icon-banner clearfix">
																		<ul class="QueBoxIcon QueBoxIcon1">
																			<h5>Add assets</h5>
																			<li class="dropdown">
																				<button class="btn1 btn-secondary dropdown-toggle mmcq_assets <?php echo ( ! empty($ques_assets) && $ques_type == 5) ? 'disabled' : ''; ?>" <?php echo ( ! empty($ques_assets) && $ques_type == 5) ? 'disabled="disabled"' : ''; ?> type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img class="img-fluid" src="img/list/audio.svg"></button>
																				<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
																					<a data-toggle="modal" data-target="#mmcqtts" class="dropdown-item">
																						<div class="tooltip1">
																							<img class="img-fluid" src="img/list/text_speech.svg">
																							<span class="tooltiptext">Text to Speech</span>
																						</div>
																					</a>
																					<a class="dropdown-item">
																						<div class="tooltip1 uploadicon">
																							<label for="file-input-mmcq-addAudio">
																								<img class="img-fluid" src="img/qus_icon/rec_video.svg">
																								<span class="tooltiptext">Add Audio</span>
																							</label>
																							<input id="file-input-mmcq-addAudio" data-id="#mmcq_audio" data-assets="mmcq_assets" class="uploadAudioFile" type="file" name="mmcq_Audio" />
																							<input type="hidden" name="mmcq_audio" id="mmcq_audio" value="<?php echo ($ques_type == 5 && $assets_type == 'a') ? $ques_assets : ''; ?>" />
																						</div>
																					</a>
																					<a class="dropdown-item">
																						<div class="tooltip1">
																							<img class="img-fluid rec-audio" data-input-id="mmcq_rec_audio" data-assets="mmcq_assets" src="img/list/screen_record.svg">
																							<span class="tooltiptext">Record Audio</span>
																						</div>
																						<input type="hidden" name="mmcq_rec_audio" id="mmcq_rec_audio" />
																					</a>
																				</div>
																			</li>
																			<li class="dropdown">
																				<button class="btn1 btn-secondary dropdown-toggle mmcq_assets <?php echo ( ! empty($ques_assets) && $ques_type == 5) ? 'disabled' : ''; ?>" <?php echo ( ! empty($ques_assets) && $ques_type == 5) ? 'disabled="disabled"' : ''; ?> type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img class="img-fluid" src="img/list/video.svg"></button>
																				<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
																					<a class="dropdown-item">
																						<div class="tooltip1 uploadicon">
																							<label for="file-input-mmcq-addVideo">
																								<img class="img-fluid" src="img/list/add_video.svg">
																								<span class="tooltiptext">Add Video</span>
																							</label>
																							<input id="file-input-mmcq-addVideo" data-id="#mmcq_video" data-assets="mmcq_assets" class="uploadVideoFile" type="file" name="mmcq_Video" />
																							<input type="hidden" name="mmcq_video" id="mmcq_video" value="<?php echo ($ques_type == 5 && $assets_type == 'v') ? $ques_assets : ''; ?>" />
																						</div>
																					</a>
																					<a class="dropdown-item">
																						<div class="tooltip1">
																							<img class="img-fluid rec-video" data-input-id="mmcq_rec_video" data-assets="mmcq_assets" src="img/qus_icon/rec_video.svg" />
																							<span class="tooltiptext">Record Video</span>
																						</div>
																						<input type="hidden" name="mmcq_rec_video" id="mmcq_rec_video" />
																					</a>
																					<a class="dropdown-item">
																						<div class="tooltip1">
																							<img class="img-fluid rec-screen" data-input-id="mmcq_rec_screen" data-assets="mmcq_assets" src="img/list/screen_record.svg" />
																							<span class="tooltiptext">Record Screen</span>
																						</div>
																						<input type="hidden" name="mmcq_rec_screen" id="mmcq_rec_screen" value="<?php echo ($ques_type == 5 && $assets_type == 's') ? $ques_assets : ''; ?>" />
																					</a>
																				</div>
																			</li>
																			<li>
																				<div class="tooltip uploadicon mmcq_assets <?php echo ( ! empty($ques_assets) && $ques_type == 5) ? 'disabled' : ''; ?>" <?php echo ( ! empty($ques_assets) && $ques_type == 5) ? 'disabled="disabled"' : ''; ?>>
																					<label for="file-input-mmcq-addImg">
																						<img class="img-fluid" src="img/list/image.svg">
																						<span class="tooltiptext">Add Image</span>
																					</label>
																					<input id="file-input-mmcq-addImg" data-id="#mmcq_img" data-assets="mmcq_assets" class="uploadImgFile mmcq_assets" type="file" name="mmcq_Img" <?php echo ( ! empty($ques_assets) && $ques_type == 5) ? 'disabled="disabled"' : ''; ?> />
																					<input type="hidden" name="mmcq_img" id="mmcq_img" value="<?php echo ($ques_type == 5 && $assets_type == 'i') ? $ques_assets : ''; ?>" />
																				</div>
																			</li>
																			<li>
																				<div class="tooltip uploadicon mmcq_assets <?php echo ( ! empty($ques_assets) && $ques_type == 5) ? 'disabled' : ''; ?>" <?php echo ( ! empty($ques_assets) && $ques_type == 5) ? 'disabled="disabled"' : ''; ?>>
																					<label for="file-input-mmcq-addDoc">
																						<img class="img-fluid" src="img/list/add_document.svg" />
																						<span class="tooltiptext">Add Document</span>
																					</label>
																					<input id="file-input-mmcq-addDoc" data-id="#mmcq_doc" data-assets="mmcq_assets" class="uploadDocFile mmcq_assets" type="file" name="mmcq_Doc" />
																					<input type="hidden" name="mmcq_doc" id="mmcq_doc" value="<?php echo ($ques_type == 5 && $assets_type == 'd') ? $ques_assets : ''; ?>" />
																				</div>
																			</li>
																		</ul>
																		<div class="Ques-comp">
																			<h5>Add Competency score</h5>
																			<?php if ( ! empty($sim_com['comp_col_1']) &&  ! empty($sim_com['comp_val_1'])): ?>
																			<div class="form-group QusScore">
																				<input type="text" class="form-control mmcq" name="mmcq_ques_val_1" id="mmcq_ques_val_1" onkeypress="return isNumberKey(event);" value="<?php echo $qresult['ques_val_1'] ?>" />
																			</div>
																			<input type="hidden" class="form-control" id="mmcq_ques_val_1h" value="<?php echo $qresult['ques_val_1'] ?>" />
																			<?php else: ?>
																			<input type="hidden" class="form-control" name="mmcq_ques_val_1" value="0" />
																			<?php endif; ?>
																			
																			<?php if ( ! empty($sim_com['comp_col_2']) &&  ! empty($sim_com['comp_val_2'])): ?>
																			<div class="form-group QusScore">
																				<input type="text" class="form-control mmcq" name="mmcq_ques_val_2" id="mmcq_ques_val_2" onkeypress="return isNumberKey(event);" value="<?php echo $qresult['ques_val_2'] ?>" />
																			</div>
																			<input type="hidden" class="form-control" id="mmcq_ques_val_2h" value="<?php echo $qresult['ques_val_2'] ?>" />
																			<?php else: ?>
																			<input type="hidden" class="form-control" name="mmcq_ques_val_2" value="0" />
																			<?php endif; ?>
																			
																			<?php if ( ! empty($sim_com['comp_col_3']) &&  ! empty($sim_com['comp_val_3'])): ?>
																			<div class="form-group QusScore">
																				<input type="text" class="form-control mmcq" name="mmcq_ques_val_3" id="mmcq_ques_val_3" onkeypress="return isNumberKey(event);" value="<?php echo $qresult['ques_val_3'] ?>" />
																			</div>
																			<input type="hidden" class="form-control" id="mmcq_ques_val_3h" value="<?php echo $qresult['ques_val_3'] ?>" />
																			<?php else: ?>
																			<input type="hidden" class="form-control" name="mmcq_ques_val_3" value="0" />
																			<?php endif; ?>
																			
																			<?php if ( ! empty($sim_com['comp_col_4']) &&  ! empty($sim_com['comp_val_4'])): ?>
																			<div class="form-group QusScore">
																				<input type="text" class="form-control mmcq" name="mmcq_ques_val_4" id="mmcq_ques_val_4" onkeypress="return isNumberKey(event);" value="<?php echo $qresult['ques_val_4'] ?>" />
																			</div>
																			<input type="hidden" class="form-control" id="mmcq_ques_val_4h" value="<?php echo $qresult['ques_val_4'] ?>" />
																			<?php else: ?>
																			<input type="hidden" class="form-control" name="mmcq_ques_val_4" value="0" />
																			<?php endif; ?>
																			
																			<?php if ( ! empty($sim_com['comp_col_5']) &&  ! empty($sim_com['comp_val_5'])): ?>
																			<div class="form-group QusScore">
																				<input type="text" class="form-control mmcq" name="mmcq_ques_val_5" id="mmcq_ques_val_5" onkeypress="return isNumberKey(event);" value="<?php echo $qresult['ques_val_5'] ?>" />
																			</div>
																			<input type="hidden" class="form-control" id="mmcq_ques_val_5h" value="<?php echo $qresult['ques_val_5'] ?>" />
																			<?php else: ?>
																			<input type="hidden" class="form-control" name="mmcq_ques_val_5" value="0" />
																			<?php endif; ?>
																			
																			<?php if ( ! empty($sim_com['comp_col_6']) &&  ! empty($sim_com['comp_val_6'])): ?>
																			<div class="form-group QusScore">
																				<input type="text" class="form-control mmcq" name="mmcq_ques_val_6" id="mmcq_ques_val_6" onkeypress="return isNumberKey(event);" value="<?php echo $qresult['ques_val_6'] ?>" />
																			</div>
																			<input type="hidden" class="form-control" id="mmcq_ques_val_6h" value="<?php echo $qresult['ques_val_6'] ?>" />
																			<?php else: ?>
																			<input type="hidden" class="form-control" name="mmcq_ques_val_6" value="0" />
																			<?php endif; ?>
																		</div>
																	</div>                                                            
																	<div class="mmcq_assets_container" <?php echo ( ! empty($ques_assets) && $ques_type == 5) ? '' : 'style="display:none;"'; ?>>
																		<div class="assets_data mmcq_assets_data"><a class="view_assets" data-src="<?php echo $path . $ques_assets; ?>" href="javascript:;"><?php echo $ques_assets; ?></a></div>
																		<a href="javascript:void(0);" data-assets="<?php echo $ques_assets ?>" data-assets-id="mmcq_assets" data-input-id="#mmcq_audio,#mmcq_rec_audio,#mmcq_video,#mmcq_rec_video,#mmcq_rec_screen,#mmcq_img" data-path="<?php echo $uploadpath ?>" class="delete_assets" title="Delete"><i class="fa fa-times" aria-hidden="true"></i></a>
																	</div>
																</div>
															</div>
														</div>
														<div class="form-popup draggable Ques-pop-style modal" id="mmcqqtts">
															<div class="popheading">Insert Text to Speech</div>
															<div class="textarea">
																<textarea type="text" class="form-control1 inputtextWrap" name="mmcqq_text_to_speech" id="mmcqq_text_to_speech"><?php echo ($ques_type == 5 && ! empty($qresult['qspeech_text'])) ? stripslashes($qresult['qspeech_text']) : ''; ?></textarea>
															</div>
															<div class="modal-footer">
																<button type="button" class="btn1 submitbtn1" data-dismiss="modal">Insert</button>
																<button type="button" class="btn1 submitbtn1" onclick="clearData('mmcqq_text_to_speech');">Clear</button>
																<button type="button" class="btn1 submitbtn1" data-dismiss="modal">Cancel</button>
															</div>
														</div>
														<div class="form-popup draggable Ques-pop-style modal" id="mmcqtts">
															<div class="popheading">Insert Text to Speech</div>
															<div class="textarea">
																<textarea type="text" class="form-control1 inputtextWrap" name="mmcq_text_to_speech" id="mmcq_text_to_speech"><?php echo ($ques_type == 5 && ! empty($qresult['speech_text'])) ? stripslashes($qresult['speech_text']) : ''; ?></textarea>
															</div>
															<div class="modal-footer">
																<button type="button" class="btn1 submitbtn1" data-dismiss="modal">Insert</button>
																<button type="button" class="btn1 submitbtn1" onclick="clearData('mmcq_text_to_speech');">Clear</button>
																<button type="button" class="btn1 submitbtn1" data-dismiss="modal">Cancel</button>
															</div>
														</div>
														<div class="Ansbox">
															<div class="row">
																<div class="col-sm-12">
																	<h3 class="AnsHeading matcht">CHOICE</h3>
																	<div class="Check-box suffel suffe2">
																		<div class="tooltiptop"><span class="tooltiptext">Shuffle ON</span></div>
																		<label class="checkstyle">
																			<input type="checkbox" name="mmcq_shuffle" class="suffleCheck" value="1" <?php echo ( ! empty($qresult['shuffle'])) ? 'checked="checked"' : ''; ?> />
																			<span class="checkmark"></span><span class="shuffleT"> Shuffle Option</span>
																		</label>
																	</div>
																</div>
																<div class="MMCQApendOption">
																<?php 
																	$mmcq_sql = "SELECT answer_id, choice_option FROM answer_tbl WHERE question_id = '". $qresult['question_id'] ."'";
																	$mmcq_res = $db->prepare($mmcq_sql); $mmcq_res->execute();
																	if ($mmcq_res->rowCount() > 0): $mmcqi = 1;
																	foreach ($mmcq_res->fetchAll(PDO::FETCH_ASSOC) as $mmcq_res_row): ?>
																	<div class="Check-box MMCQCheck">
																		<?php $true_options = explode(',', $qresult['true_options']); ?>
																		<input type="hidden" name="updateMMCQAnswerid[]" value="<?php echo $mmcq_res_row['answer_id'] ?>" />
																		<label class="checkstyle"><input type="checkbox" name="mmcq_true_option[]" value="<?php echo $mmcqi ?>" <?php echo (in_array($mmcqi, $true_options)) ? 'checked="checked"' : ''; ?> /><span class="checkmark"></span></label>
																		<div class="col-sm-12 mcwidth">
																			<div class="choicebox">
																				<div class="form-group">
																					<input type="text" name="mmcq_option[]" id="mmcq_option" class="form-control inputtextWrap mmcq" placeholder="Option text goes here" value="<?php echo stripslashes($mmcq_res_row['choice_option']); ?>" />
																				</div>
																			</div>
																		</div>
																		<?php if ($mmcqi == 1): ?>
																		<div class="AddAns-Checkbox"><img class="img-fluid MMCQAdd" src="img/list/add_field.svg"></div>
																		<?php else: ?>
																		<div class="AddAns-Checkbox removeOption" data-remove-answer-id="<?php echo $mmcq_res_row['answer_id'] ?>"><img class="img-fluid" src="img/list/delete_field.svg"></div>
																		<?php endif; ?>
																	</div>
																	<?php $mmcqi++; endforeach; else: ?>
																	<div class="Check-box MMCQCheck">
																		<label class="checkstyle">
																			<input type="checkbox" name="mmcq_true_option[]" value="1" />
																			<span class="checkmark"></span>
																		</label>
																		<div class="col-sm-12 mcwidth">
																			<div class="choicebox">
																				<div class="form-group">
																					<input type="text" name="mmcq_option[]" id="mmcq_option" class="form-control inputtextWrap mmcq" placeholder="Option text goes here" />
																				</div>
																			</div>
																		</div>
																		<div class="AddAns-Option">
																			<img class="img-fluid MMCQAdd" src="img/list/add_field.svg">
																		</div>
																	</div>
																	<?php endif; ?>
																</div>
															</div>
														</div>
													</div>
													<div id="MMCQ-F" class="tab-pane fade in">
														<div class="col-sm-12 Qusfeedback">
															<div class="form-group">
																<div class="feedback-flex">
																	<h4>Correct</h4>
																	<textarea class="form-control FeedForm inputtextWrap mmcq" name="mmcq_cfeedback" id="mmcq_cfeedback" rows="3" placeholder="That's right! You selected the correct response."><?php echo ($ques_type == 5 && ! empty($feedResult[0]['feedback']) && $feedResult[0]['feedback_type'] == 1) ? stripslashes($feedResult[0]['feedback']) : ''; ?></textarea>
																</div>
																<ul class="QueBoxIcon feed LMfeed">
																	<h5>Add assets</h5>
																	<li class="dropdown">
																		<button class="btn1 btn-secondary dropdown-toggle mmcqf_assets <?php echo ( ! empty($qfeed_assets) && $ques_type == 5) ? 'disabled' : ''; ?>" <?php echo ( ! empty($qfeed_assets) && $ques_type == 5) ? 'disabled="disabled"' : ''; ?> type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img class="img-fluid" src="img/list/audio.svg"></button>
																		<div class="dropdown-menu buttom" aria-labelledby="dropdownMenuButton">
																			<a data-toggle="modal" data-target="#mmcqfctts" class="dropdown-item">
																				<div class="tooltip1">
																					<img class="img-fluid" src="img/list/text_speech.svg">
																					<span class="tooltiptext">Text to Speech</span>
																				</div>
																			</a>
																			<a class="dropdown-item">
																				<div class="tooltip1 uploadicon">
																					<label for="file-input-mmcq-feedback-addAudio">
																						<img class="img-fluid" src="img/list/add_audio.svg">
																						<span class="tooltiptext">Add Audio</span>
																					</label>
																					<input id="file-input-mmcq-feedback-addAudio" data-id="#mmcq_feedback_audio" data-assets="mmcqf_assets" class="uploadAudioFile" type="file" name="mmcq_feedback_Audio"/>
																					<input type="hidden" name="mmcq_feedback_audio" id="mmcq_feedback_audio" value="<?php echo ($ques_type == 5 && $feed_assets_type == 'fa') ? $qfeed_assets : ''; ?>" />
																				</div>
																			</a>
																			<a class="dropdown-item">
																				<div class="tooltip1">
																					<img class="img-fluid rec-audio" data-input-id="mmcq_feedback_rec_audio" data-assets="mmcqf_assets" src="img/list/record_audio.svg">
																					<span class="tooltiptext">Record Audio</span>
																				</div>
																				<input type="hidden" name="mmcq_feedback_rec_audio" id="mmcq_feedback_rec_audio" />
																			</a>
																		</div>
																	</li>
																	<li class="dropdown">
																		<button class="btn1 btn-secondary dropdown-toggle mmcqf_assets <?php echo ( ! empty($qfeed_assets) && $ques_type == 5) ? 'disabled' : ''; ?>" <?php echo ( ! empty($qfeed_assets) && $ques_type == 5) ? 'disabled="disabled"' : ''; ?> type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img class="img-fluid" src="img/list/video.svg"></button>
																		<div class="dropdown-menu buttom" aria-labelledby="dropdownMenuButton">
																			<a class="dropdown-item">
																				<div class="tooltip1 uploadicon">
																					<label for="file-input-mmcq-feedback-addVideo">
																						<img class="img-fluid" src="img/list/add_video.svg">
																						<span class="tooltiptext">Add Video</span>
																					</label>
																					<input id="file-input-mmcq-feedback-addVideo" data-id="#mmcq_feedback_video" data-assets="mmcqf_assets" class="uploadVideoFile" type="file" name="mmcq_feedback_Video" />
																					<input type="hidden" name="mmcq_feedback_video" id="mmcq_feedback_video" value="<?php echo ($ques_type == 5 && $feed_assets_type == 'fv') ? $qfeed_assets : ''; ?>" />
																				</div>
																			</a>
																			<a class="dropdown-item">
																				<div class="tooltip1">
																					<img class="img-fluid rec-video" data-input-id="mmcq_feedback_rec_video" data-assets="mmcqf_assets" src="img/qus_icon/rec_video.svg">
																					<span class="tooltiptext">Record Video</span>
																				</div>
																				<input type="hidden" name="mmcq_feedback_rec_video" id="mmcq_feedback_rec_video" />
																			</a>
																			<a class="dropdown-item">
																				<div class="tooltip1">
																					<img class="img-fluid rec-screen" data-input-id="mmcq_feedback_rec_screen" data-assets="mmcqf_assets" src="img/list/screen_record.svg">
																					<span class="tooltiptext">Record Screen</span>
																				</div>
																				<input type="hidden" name="mmcq_feedback_rec_screen" id="mmcq_feedback_rec_screen" value="<?php echo ($ques_type == 5 && $feed_assets_type == 'fs') ? $qfeed_assets : ''; ?>" />
																			</a>
																		</div>
																	</li>
																	<li>
																		<div class="tooltip uploadicon mmcqf_assets <?php echo ( ! empty($qfeed_assets) && $ques_type == 5) ? 'disabled' : ''; ?>" <?php echo ( ! empty($qfeed_assets) && $ques_type == 5) ? 'disabled="disabled"' : ''; ?>>
																			<label for="file-input-mmcq-feedback-addImg">
																				<img class="img-fluid" src="img/list/image.svg">
																				<span class="tooltiptext">Add Image</span>
																			</label>
																			<input id="file-input-mmcq-feedback-addImg" data-id="#mmcq_feedback_img" data-assets="mmcqf_assets" class="uploadImgFile mmcqf_assets" type="file" name="mmcq_feedback_Img" <?php echo ( ! empty($qfeed_assets) && $ques_type == 5) ? 'disabled="disabled"' : ''; ?> />
																			<input type="hidden" name="mmcq_feedback_img" id="mmcq_feedback_img" value="<?php echo ($ques_type == 5 && $feed_assets_type == 'fi') ? $qfeed_assets : ''; ?>" />
																		</div>
																	</li>
																	<li>
																		<div class="tooltip uploadicon mmcqf_assets <?php echo ( ! empty($qfeed_assets) && $ques_type == 5) ? 'disabled' : ''; ?>" <?php echo ( ! empty($qfeed_assets) && $ques_type == 5) ? 'disabled="disabled"' : ''; ?>>
																			<label for="file-input-mmcq-feedback-addDoc">
																				<img class="img-fluid" src="img/list/add_document.svg" />
																				<span class="tooltiptext">Add Document</span>
																			</label>
																			<input id="file-input-mmcq-feedback-addDoc" data-id="#mmcq_feedback_doc" data-assets="mmcqf_assets" class="uploadDocFile mmcqf_assets" type="file" name="mmcq_feedback_Doc" <?php echo ( ! empty($qfeed_assets) && $ques_type == 5) ? 'disabled="disabled"' : ''; ?> />
																			<input type="hidden" name="mmcq_feedback_doc" id="mmcq_feedback_doc" value="<?php echo ($ques_type == 5 && $feed_assets_type == 'fd') ? $qfeed_assets : ''; ?>" />
																		</div>
																	</li>
																</ul>                              
															</div>
															<div class="mmcqf_assets_container" <?php echo ( ! empty($qfeed_assets) && $ques_type == 5) ? '' : 'style="display:none;"'; ?>>
																<div class="assets_data mmcqf_assets_data"><a class="view_assets" data-src="<?php echo $path . $qfeed_assets; ?>" href="javascript:;"><?php echo $qfeed_assets; ?></a></div>
																<a href="javascript:void(0);" data-assets="<?php echo $qfeed_assets ?>" data-assets-id="mmcqf_assets" data-input-id="#mmcq_feedback_audio,#mmcq_feedback_rec_audio,#mcq_feedback_video,#mmcq_feedback_rec_video,#mmcq_feedback_rec_screen,#mmcq_feedback_img" data-path="<?php echo $uploadpath ?>" class="delete_assets" title="Delete"><i class="fa fa-times" aria-hidden="true"></i></a>
															</div>
															<div class="form-group">
																<div class="feedback-flex">
																	<h4>Incorrect</h4>
																	<textarea class="form-control FeedForm inputtextWrap mmcq" name="mmcq_ifeedback" id="mmcq_ifeedback" placeholder="You did not select the correct response."><?php echo ($ques_type == 5 && ! empty($feedResult[1]['feedback']) && $feedResult[1]['feedback_type'] == 2) ? stripslashes($feedResult[1]['feedback']) : ''; ?></textarea>
																</div>
																<ul class="QueBoxIcon feed LMfeed">
																	<h5>Add assets</h5>
																	<li class="dropdown">
																		<button class="btn1 btn-secondary dropdown-toggle mmcqfi_assets <?php echo ( ! empty($qifeed_assets) && $ques_type == 5) ? 'disabled' : ''; ?>" <?php echo ( ! empty($qifeed_assets) && $ques_type == 5) ? 'disabled="disabled"' : ''; ?> type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img class="img-fluid" src="img/list/audio.svg"></button>
																		<div class="dropdown-menu buttom" aria-labelledby="dropdownMenuButton">
																			<a data-toggle="modal" data-target="#mmcqfitts" class="dropdown-item">
																				<div class="tooltip1">
																					<img class="img-fluid" src="img/list/text_speech.svg">
																					<span class="tooltiptext">Text to Speech</span>
																				</div>
																			</a>
																			<a class="dropdown-item">
																				<div class="tooltip1 uploadicon">
																					<label for="file-input-mmcqi-feedback-addAudio">
																						<img class="img-fluid" src="img/list/add_audio.svg">
																						<span class="tooltiptext">Add Audio</span>
																					</label>
																					<input id="file-input-mmcqi-feedback-addAudio" data-id="#mmcqi_feedback_audio" data-assets="mmcqfi_assets" class="uploadAudioFile" type="file" name="mmcqi_feedback_Audio" />
																					<input type="hidden" name="mmcqi_feedback_audio" id="mmcqi_feedback_audio" value="<?php echo ($ques_type == 5 && $feed_assets_type == 'fa') ? $qifeed_assets : ''; ?>" />
																				</div>
																			</a>
																			<a class="dropdown-item">
																				<div class="tooltip1">
																					<img class="img-fluid rec-audio" data-input-id="mmcqi_feedback_rec_audio" data-assets="mmcqfi_assets" src="img/list/record_audio.svg">
																					<span class="tooltiptext">Record Audio</span>
																				</div>
																				<input type="hidden" name="mmcqi_feedback_rec_audio" id="mmcqi_feedback_rec_audio" />
																			</a>
																		</div>
																	</li>
																	<li class="dropdown">
																		<button class="btn1 btn-secondary dropdown-toggle mmcqfi_assets <?php echo ( ! empty($qifeed_assets) && $ques_type == 5) ? 'disabled' : ''; ?>" <?php echo ( ! empty($qifeed_assets) && $ques_type == 5) ? 'disabled="disabled"' : ''; ?> type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img class="img-fluid" src="img/list/video.svg"></button>
																		<div class="dropdown-menu buttom" aria-labelledby="dropdownMenuButton">
																			<a class="dropdown-item">
																				<div class="tooltip1 uploadicon">
																					<label for="file-input-mmcqi-feedback-addVideo">
																						<img class="img-fluid" src="img/list/add_video.svg">
																						<span class="tooltiptext">Add Video</span>
																					</label>
																					<input id="file-input-mmcqi-feedback-addVideo" data-id="#mmcqi_feedback_video" data-assets="mmcqfi_assets" class="uploadVideoFile" type="file" name="mmcqi_feedback_Video" />
																					<input type="hidden" name="mmcqi_feedback_video" id="mmcqi_feedback_video" value="<?php echo ($ques_type == 5 && $feed_assets_type == 'fv') ? $qifeed_assets : ''; ?>" />
																				</div>
																			</a>
																			<a class="dropdown-item">
																				<div class="tooltip1">
																					<img class="img-fluid rec-video" data-input-id="mmcqi_feedback_rec_video" data-assets="mmcqfi_assets" src="img/qus_icon/rec_video.svg">
																					<span class="tooltiptext">Record Video</span>
																				</div>
																				<input type="hidden" name="mmcqi_feedback_rec_video" id="mmcqi_feedback_rec_video" />
																			</a>
																			<a class="dropdown-item">
																				<div class="tooltip1">
																					<img class="img-fluid rec-screen" data-input-id="mmcqi_feedback_rec_screen" data-assets="mmcqfi_assets" src="img/list/screen_record.svg">
																					<span class="tooltiptext">Record Screen</span>
																				</div>
																				<input type="hidden" name="mmcqi_feedback_rec_screen" id="mmcqi_feedback_rec_screen" value="<?php echo ($ques_type == 5 && $feed_assets_type == 'fs') ? $qifeed_assets : ''; ?>" />
																			</a>
																		</div>
																	</li>
																	<li>
																		<div class="tooltip uploadicon mmcqfi_assets <?php echo ( ! empty($qifeed_assets) && $ques_type == 5) ? 'disabled' : ''; ?>" <?php echo ( ! empty($qifeed_assets) && $ques_type == 5) ? 'disabled="disabled"' : ''; ?>>
																			<label for="file-input-mmcqi-feedback-addImg">
																				<img class="img-fluid" src="img/list/image.svg">
																				<span class="tooltiptext">Add Image</span>
																			</label>
																			<input id="file-input-mmcqi-feedback-addImg" data-id="#mmcqi_feedback_img" data-assets="mmcqfi_assets" class="uploadImgFile mmcqfi_assets" type="file" name="mmcqi_feedback_Img" <?php echo ( ! empty($qifeed_assets) && $ques_type == 5) ? 'disabled="disabled"' : ''; ?> />
																			<input type="hidden" name="mmcqi_feedback_img" id="mmcqi_feedback_img" value="<?php echo ($ques_type == 5 && $feed_assets_type == 'fi') ? $qifeed_assets : ''; ?>" />
																		</div>
																	</li>
																	<li>
																		<div class="tooltip uploadicon mmcqfi_assets <?php echo ( ! empty($qifeed_assets) && $ques_type == 5) ? 'disabled' : ''; ?>" <?php echo ( ! empty($qifeed_assets) && $ques_type == 5) ? 'disabled="disabled"' : ''; ?>>
																			<label for="file-input-mmcqi-feedback-addDoc">
																				<img class="img-fluid" src="img/list/add_document.svg" />
																				<span class="tooltiptext">Add Document</span>
																			</label>
																			<input id="file-input-mmcqi-feedback-addDoc" data-id="#mmcqi_feedback_doc" data-assets="mmcqfi_assets" class="uploadDocFile mmcqfi_assets" type="file" name="mmcqi_feedback_Doc" <?php echo ( ! empty($qifeed_assets) && $ques_type == 5) ? 'disabled="disabled"' : ''; ?> />
																			<input type="hidden" name="mmcqi_feedback_doc" id="mmcqi_feedback_doc" value="<?php echo ($ques_type == 5 && $feed_assets_type == 'fd') ? $qifeed_assets : ''; ?>" />
																		</div>
																	</li>
																</ul>                              
															</div>
															<div class="mmcqfi_assets_container" <?php echo ( ! empty($qifeed_assets) && $ques_type == 5) ? '' : 'style="display:none;"'; ?>>
																<div class="assets_data mmcqfi_assets_data"><a class="view_assets" data-src="<?php echo $path . $qifeed_assets; ?>" href="javascript:;"><?php echo $qifeed_assets; ?></a></div>
																<a href="javascript:void(0);" data-assets="<?php echo $qifeed_assets ?>" data-assets-id="mmcqfi_assets" data-input-id="#mmcqi_feedback_audio,#mmcqi_feedback_rec_audio,#mmcqi_feedback_video,#mmcqi_feedback_rec_video,#mmcqi_feedback_rec_screen,#mmcqi_feedback_img" data-path="<?php echo $uploadpath ?>" class="delete_assets" title="Delete"><i class="fa fa-times" aria-hidden="true"></i></a>
															</div>
														</div>
														<div class="form-popup draggable Ques-pop-style modal" id="mmcqfctts">
															<div class="popheading">Insert Text to Speech</div>
															<div class="textarea">
																<textarea type="text" class="form-control1 inputtextWrap" name="mmcq_feedback_text_to_speech" id="mmcq_feedback_text_to_speech"><?php echo ($ques_type == 5 && ! empty($feedResult[0]['feedback']) && $feedResult[0]['feedback_type'] == 1) ? stripslashes($feedResult[0]['feed_speech_text']) : ''; ?></textarea>
															</div>
															<div class="modal-footer">
																<button type="button" class="btn1 submitbtn1" data-dismiss="modal">Insert</button>
																<button type="button" class="btn1 submitbtn1" onclick="clearData('mmcq_feedback_text_to_speech');">Clear</button>
																<button type="button" class="btn1 submitbtn1" data-dismiss="modal">Cancel</button>
															</div>
														</div>
														<div class="form-popup draggable Ques-pop-style modal" id="mmcqfitts">
															<div class="popheading">Insert Text to Speech</div>
															<div class="textarea">
																<textarea type="text" class="form-control1 inputtextWrap" name="mmcqi_feedback_text_to_speech" id="mmcqi_feedback_text_to_speech"><?php echo ($ques_type == 5 && ! empty($feedResult[1]['feedback']) && $feedResult[1]['feedback_type'] == 2) ? stripslashes($feedResult[1]['feed_speech_text']) : ''; ?></textarea>
															</div>
															<div class="modal-footer">
																<button type="button" class="btn1 submitbtn1" data-dismiss="modal">Insert</button>
																<button type="button" class="btn1 submitbtn1" onclick="clearData('mmcqi_feedback_text_to_speech');">Clear</button>
																<button type="button" class="btn1 submitbtn1" data-dismiss="modal">Cancel</button>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
									
									<!--Swaping-Question-->
									<div id="SwapingQuestion" class="tab-pane fade <?php echo ($ques_type == 7) ? 'in active' : ''; ?>">
										<div class="match-Tab widthQ100">
											<ul class="nav nav-tabs">
												<li class="active"><a data-toggle="tab" href="#Swaping-Q">Question</a></li>
												<li><a data-toggle="tab" href="#Swaping-F">Feedback</a></li>
											</ul>
										</div>
										<div class="container-fluid">
											<div class="widthQ100 multitemp">
												<div class="tab-content">
													<div id="Swaping-Q" class="match-Q tab-pane fade in active">
														<div class="quesbox">
															<div class="row">
																<div class="col-sm-12 Questiontmpbanner">
																	<div class="critical_div">
																		<h3 class="qusHeading">QUESTION <p class="question-type">(Swiping)</p></h3>
																		<div class="CIRtiCaltext">
																			Critical Question: <input type="checkbox" name="swip_criticalQ" id="swip_criticalQ" class="form-control criticalQ" value="1" <?php echo ( ! empty($qresult['critical'])) ? 'checked="checked"' : ''; ?> />
																		</div>
																	</div>
																	<div class="quesbox-flex">
																		<div class="form-group">
																			<input type="text" name="swipq" id="swipq" class="form-control QuesForm inputtextWrap swipq" placeholder="Click here to enter question" value="<?php echo ( ! empty($qresult['questions'])) ? stripslashes($qresult['questions']) : ''; ?>" />
																			<div class="swipqq_assets_container" <?php echo ( ! empty($ques_att) && $ques_type == 7) ? '' : 'style="display:none;"'; ?>>
																				<div class="assets_data swipqq_assets_data"><a class="view_assets" data-src="<?php echo $path . $ques_att; ?>" href="javascript:;"><?php echo $ques_att; ?></a></div>
																				<a href="javascript:void(0);" data-assets="<?php echo $ques_att; ?>" data-assets-id="swipqq_assets" data-input-id="#swipqq_qaudio,#swipqq_rec_audio" data-path="<?php echo $uploadpath ?>" class="delete_assets" title="Delete"><i class="fa fa-times" aria-hidden="true"></i></a>
																			</div>
																			<ul class="QueBoxIcon">
																				<li class="dropdown">
																					<button class="btn1 btn-secondary dropdown-toggle swipqq_assets <?php echo ( ! empty($ques_att) && $ques_type == 7) ? 'disabled' : ''; ?>" <?php echo ( ! empty($ques_att) && $ques_type == 7) ? 'disabled="disabled"' : ''; ?> type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img class="img-fluid" src="img/list/audio.svg"></button>
																					<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
																						<a class="dropdown-item">
																							<div class="tooltipLeft uploadicon">
																								<label for="file-input-swipqq-addAudio"><img class="img-fluid" src="img/list/add_audio.svg" /><span class="tooltiptext">Add Audio</span></label>
																								<input id="file-input-swipqq-addAudio" data-id="#swipqq_qaudio" data-assets="swipqq_assets" class="uploadAudioFile" type="file" name="swipqq_Audio" />
																								<input type="hidden" name="swipqq_qaudio" id="swipqq_qaudio" value="<?php echo ($ques_type == 7 && $qatt_type == 'qa') ? $ques_att : ''; ?>" />
																							</div>
																						</a>
																						<a class="dropdown-item">
																							<div class="tooltipLeft">
																								<img class="img-fluid rec-audio" data-input-id="swipqq_rec_audio" data-assets="swipqq_assets" src="img/list/record_audio.svg" />
																								<span class="tooltiptext">Record Audio</span>
																							</div>
																							<input type="hidden" name="swipqq_rec_audio" id="swipqq_rec_audio" />
																						</a>
																						<a data-toggle="modal" data-target="#swipqqtts" class="dropdown-item">
																							<div class="tooltipLeft">
																								<img class="img-fluid" src="img/list/text_speech.svg" />
																								<span class="tooltiptext">Text to Speech</span>
																							</div>
																						</a>
																					</div>
																				</li>
																			</ul>
																		</div>
																	</div>
																	<div class="q-icon-banner clearfix">
																		<ul class="QueBoxIcon QueBoxIcon1">
																			<h5>Add assets</h5>
																			<li class="dropdown">
																				<button class="btn1 btn-secondary dropdown-toggle swipq_assets <?php echo ( ! empty($ques_assets) && $ques_type == 7) ? 'disabled' : ''; ?>" <?php echo ( ! empty($ques_assets) && $ques_type == 7) ? 'disabled="disabled"' : ''; ?> type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img class="img-fluid" src="img/list/audio.svg"></button>
																				<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
																					<a data-toggle="modal" data-target="#swipqtts" class="dropdown-item">
																						<div class="tooltip1">
																							<img class="img-fluid" src="img/list/text_speech.svg" />
																							<span class="tooltiptext">Text to Speech</span>
																						</div>
																					</a>
																					<a class="dropdown-item">
																						<div class="tooltip1 uploadicon">
																							<label for="file-input-swipq-addAudio">
																								<img class="img-fluid" src="img/list/add_audio.svg">
																								<span class="tooltiptext">Add Audio</span>
																							</label>
																							<input id="file-input-swipq-addAudio" data-id="#swipq_audio" data-assets="swipq_assets" class="uploadAudioFile" type="file" name="swipq_Audio" />
																							<input type="hidden" name="swipq_audio" id="swipq_audio" value="<?php echo ($ques_type == 7 && $assets_type == 'a') ? $ques_assets : ''; ?>"/>
																						</div>
																					</a>
																					<a class="dropdown-item">
																						<div class="tooltip1">
																							<img class="img-fluid rec-audio" data-input-id="swipq_rec_audio" data-assets="swipq_assets" src="img/list/record_audio.svg">
																							<span class="tooltiptext">Record Audio</span>
																						</div>
																						<input type="hidden" name="swipq_rec_audio" id="swipq_rec_audio" />
																					</a>
																				</div>
																			</li>
																			<li class="dropdown">
																				<button class="btn1 btn-secondary dropdown-toggle swipq_assets <?php echo ( ! empty($ques_assets) && $ques_type == 7) ? 'disabled' : ''; ?>" <?php echo ( ! empty($ques_assets) && $ques_type == 7) ? 'disabled="disabled"' : ''; ?> type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img class="img-fluid" src="img/list/video.svg"></button>
																				<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
																					<a class="dropdown-item">
																						<div class="tooltip1 uploadicon">
																							<label for="file-input-swipq-addVideo">
																								<img class="img-fluid" src="img/list/add_video.svg">
																								<span class="tooltiptext">Add Video</span>
																							</label>
																							<input id="file-input-swipq-addVideo" data-id="#swipq_video" data-assets="swipq_assets" class="uploadVideoFile" type="file" name="swipq_Video" />
																							<input type="hidden" name="swipq_video" id="swipq_video" value="<?php echo ($ques_type == 7 && $assets_type == 'v') ? $ques_assets : ''; ?>" />
																						</div>
																					</a>
																					<a class="dropdown-item">
																						<div class="tooltip1">
																							<img class="img-fluid rec-video" data-input-id="swipq_rec_video" data-assets="swipq_assets" src="img/qus_icon/rec_video.svg" />
																							<span class="tooltiptext">Record Video</span>
																						</div>
																						<input type="hidden" name="swipq_rec_video" id="swipq_rec_video" />
																					</a>
																					<a class="dropdown-item">
																						<div class="tooltip1">
																							<img class="img-fluid rec-screen" data-input-id="swipq_rec_screen" data-assets="swipq_assets" src="img/list/screen_record.svg" />
																							<span class="tooltiptext">Record Screen</span>
																						</div>
																						<input type="hidden" name="swipq_rec_screen" id="swipq_rec_screen" value="<?php echo ($ques_type == 7 && $assets_type == 's') ? $ques_assets : ''; ?>" />
																					</a>
																				</div>
																			</li>
																			<li>
																				<div class="tooltip uploadicon swipq_assets <?php echo ( ! empty($ques_assets) && $ques_type == 7) ? 'disabled' : ''; ?>" <?php echo ( ! empty($ques_assets) && $ques_type == 7) ? 'disabled="disabled"' : ''; ?>>
																					<label for="file-input-swipq-addImg">
																						<img class="img-fluid" src="img/list/image.svg" />
																						<span class="tooltiptext">Add Image</span>
																					</label>
																					<input id="file-input-swipq-addImg" data-id="#swipq_img" data-assets="swipq_assets" class="uploadImgFile swipq_assets" type="file" name="swipq_Img" <?php echo ( ! empty($ques_assets) && $ques_type == 7) ? 'disabled="disabled"' : ''; ?> />
																					<input type="hidden" name="swipq_img" id="swipq_img" value="<?php echo ($ques_type == 7 && $assets_type == 'i') ? $ques_assets : ''; ?>" />
																				</div>
																			</li>
																			<li>
																				<div class="tooltip uploadicon swipq_assets <?php echo ( ! empty($ques_assets) && $ques_type == 7) ? 'disabled' : ''; ?>" <?php echo ( ! empty($ques_assets) && $ques_type == 7) ? 'disabled="disabled"' : ''; ?>>
																					<label for="file-input-swipq-addDoc">
																						<img class="img-fluid" src="img/list/add_document.svg" />
																						<span class="tooltiptext">Add Document</span>
																					</label>
																					<input id="file-input-swipq-addDoc" data-id="#swipq_doc" data-assets="swipq_assets" class="uploadDocFile swipq_assets" type="file" name="swipq_Doc" />
																					<input type="hidden" name="swipq_doc" id="swipq_doc" value="<?php echo ($ques_type == 7 && $assets_type == 'd') ? $ques_assets : ''; ?>"/>
																				</div>
																			</li>
																		</ul>
																		<div class="Ques-comp">
																			<h5>Add Competency score</h5>
																			<?php if ( ! empty($sim_com['comp_col_1']) &&  ! empty($sim_com['comp_val_1'])): ?>
																			<div class="form-group QusScore">
																				<input type="text" class="form-control swipq" name="swipq_ques_val_1" id="swipq_ques_val_1" onkeypress="return isNumberKey(event);" value="<?php echo $qresult['ques_val_1'] ?>" />
																			</div>
																			<input type="hidden" class="form-control" id="swipq_ques_val_1h" value="<?php echo $qresult['ques_val_1'] ?>" />
																			<?php else: ?>
																			<input type="hidden" class="form-control" name="swipq_ques_val_1" value="0" />
																			<?php endif; ?>
																			
																			<?php if ( ! empty($sim_com['comp_col_2']) &&  ! empty($sim_com['comp_val_2'])): ?>
																			<div class="form-group QusScore">
																				<input type="text" class="form-control swipq" name="swipq_ques_val_2" id="swipq_ques_val_2" onkeypress="return isNumberKey(event);" value="<?php echo $qresult['ques_val_2'] ?>" />
																			</div>
																			<input type="hidden" class="form-control" id="swipq_ques_val_2h" value="<?php echo $qresult['ques_val_2'] ?>" />
																			<?php else: ?>
																			<input type="hidden" class="form-control" name="swipq_ques_val_2" value="0" />
																			<?php endif; ?>
																			
																			<?php if ( ! empty($sim_com['comp_col_3']) &&  ! empty($sim_com['comp_val_3'])): ?>
																			<div class="form-group QusScore">
																				<input type="text" class="form-control swipq" name="swipq_ques_val_3" id="swipq_ques_val_3" onkeypress="return isNumberKey(event);" value="<?php echo $qresult['ques_val_3'] ?>" />
																			</div>
																			<input type="hidden" class="form-control" id="swipq_ques_val_3h" value="<?php echo $qresult['ques_val_3'] ?>" />
																			<?php else: ?>
																			<input type="hidden" class="form-control" name="swipq_ques_val_3" value="0" />
																			<?php endif; ?>
																			
																			<?php if ( ! empty($sim_com['comp_col_4']) &&  ! empty($sim_com['comp_val_4'])): ?>
																			<div class="form-group QusScore">
																				<input type="text" class="form-control swipq" name="swipq_ques_val_4" id="swipq_ques_val_4" onkeypress="return isNumberKey(event);" value="<?php echo $qresult['ques_val_4'] ?>" />
																			</div>
																			<input type="hidden" class="form-control" id="swipq_ques_val_4h" value="<?php echo $qresult['ques_val_4'] ?>" />
																			<?php else: ?>
																			<input type="hidden" class="form-control" name="swipq_ques_val_4" value="0" />
																			<?php endif; ?>
																			
																			<?php if ( ! empty($sim_com['comp_col_5']) &&  ! empty($sim_com['comp_val_5'])): ?>
																			<div class="form-group QusScore">
																				<input type="text" class="form-control swipq" name="swipq_ques_val_5" id="swipq_ques_val_5" onkeypress="return isNumberKey(event);" value="<?php echo $qresult['ques_val_5'] ?>" />
																			</div>
																			<input type="hidden" class="form-control" id="swipq_ques_val_5h" value="<?php echo $qresult['ques_val_5'] ?>" />
																			<?php else: ?>
																			<input type="hidden" class="form-control" name="swipq_ques_val_5" value="0" />
																			<?php endif; ?>
																			
																			<?php if ( ! empty($sim_com['comp_col_6']) &&  ! empty($sim_com['comp_val_6'])): ?>
																			<div class="form-group QusScore">
																				<input type="text" class="form-control swipq" name="swipq_ques_val_6" id="swipq_ques_val_6" onkeypress="return isNumberKey(event);" value="<?php echo $qresult['ques_val_6'] ?>" />
																			</div>
																			<input type="hidden" class="form-control" id="swipq_ques_val_6h" value="<?php echo $qresult['ques_val_6'] ?>" />
																			<?php else: ?>
																			<input type="hidden" class="form-control" name="swipq_ques_val_6" value="0" />
																			<?php endif; ?>
																		 </div>
																	</div>
																	<div class="swipq_assets_container" <?php echo ( ! empty($ques_assets) && $ques_type == 7) ? '' : 'style="display:none;"'; ?>>
																		<div class="assets_data swipq_assets_data"><a class="view_assets" data-src="<?php echo $path . $ques_assets; ?>" href="javascript:;"><?php echo $ques_assets; ?></a></div>
																		<a href="javascript:void(0);" data-assets="<?php echo $ques_assets ?>" data-assets-id="swipq_assets" data-input-id="#swipq_audio,#swipq_rec_audio,#swipq_video,#swipq_rec_video,#swipq_rec_screen,#swipq_img" data-path="<?php echo $uploadpath ?>" class="delete_assets" title="Delete"><i class="fa fa-times" aria-hidden="true"></i></a>
																	</div>
																</div>
															</div>
														</div>
														<div class="form-popup draggable Ques-pop-style modal" id="swipqqtts">
															<div class="popheading">Insert Text to Speech</div>
															<div class="textarea">
																<textarea type="text" class="form-control1 inputtextWrap" name="swipqq_text_to_speech" id="swipqq_text_to_speech"><?php echo ($ques_type == 7 && ! empty($qresult['qspeech_text'])) ? stripslashes($qresult['qspeech_text']) : ''; ?></textarea>
															</div>
															<div class="modal-footer">
																<button type="button" class="btn1 submitbtn1" data-dismiss="modal">Insert</button>
																<button type="button" class="btn1 submitbtn1" onclick="clearData('mcqq_text_to_speech');">Clear</button>
																<button type="button" class="btn1 submitbtn1" data-dismiss="modal">Cancel</button>
															</div>
														</div>
														<div class="form-popup draggable Ques-pop-style modal" id="swipqtts">
															<div class="popheading">Insert Text to Speech</div>
															<div class="textarea">
																<textarea type="text" class="form-control1 inputtextWrap" name="swipq_text_to_speech" id="swipq_text_to_speech"><?php echo ($ques_type == 7 && ! empty($qresult['speech_text'])) ? stripslashes($qresult['speech_text']) : ''; ?></textarea>
															</div>
															<div class="modal-footer">
																<button type="button" class="btn1 submitbtn1" data-dismiss="modal">Insert</button>
																<button type="button" class="btn1 submitbtn1" onclick="clearData('mcq_text_to_speech');">Clear</button>
																<button type="button" class="btn1 submitbtn1" data-dismiss="modal">Cancel</button>
															</div>
														</div>
														<div class="Ansbox">
															<div class="row">
																<div class="col-sm-12">
																	<h3 class="AnsHeading matcht">CHOICE</h3>
																	<div class="Check-box suffel suffe2">
																		<div class="tooltiptop"><span class="tooltiptext">Shuffle ON</span></div>
																		<label class="checkstyle">
																			<input type="checkbox" name="swipq_shuffle" class="suffleCheck" value="1" <?php echo ( ! empty($qresult['shuffle'])) ? 'checked="checked"' : ''; ?> />
																			<span class="checkmark"></span><span class="shuffleT"> Shuffle Option</span>
																		</label>
																	</div>
																	<div class="SWApendOption">
																	<?php 
																	$swa_sql = "SELECT answer_id, choice_option, image FROM answer_tbl WHERE question_id = '". $qresult['question_id'] ."'";
																	$swa_res = $db->prepare($swa_sql); $swa_res->execute();
																	if ($swa_res->rowCount() > 0): $swai = 1;
																	foreach ($swa_res->fetchAll(PDO::FETCH_ASSOC) as $swa_row): ?>
																	<input type="hidden" name="updateSwipqAnswerid[]" value="<?php echo $swa_row['answer_id'] ?>" />
																	<div class="Swa-box">
																		<div class="Radio-box">
																			<label class="radiostyle">
																				<input type="radio" name="swipq_true_option" value="<?php echo $swai ?>" <?php echo ($swai == $qresult['true_options']) ? 'checked="checked"' : ''; ?>>
																				<span class="radiomark"></span>
																			</label>
																			<div class="col-sm-12 mcwidth">
																				<div class="choicebox">
																					<div class="form-group">
																						<input type="text" name="swipq_option[]" id="swipq_option" class="form-control QuesForm1 inputtextWrap swipq" placeholder="Click here to enter question" value="<?php echo $swa_row['choice_option'] ?>" />
																						<?php 
																						$swa_cls_dis	= '';
																						$swa_dis 		= '';
																						$swa_item		= '';
																						if ( ! empty($swa_row['image']) && file_exists($root_path . $swa_row['image']) && $ques_type == 7):
																							$swa_cls_dis	= 'disabled';
																							$swa_dis 		= 'disabled="disabled"';
																							$swa_item 		= $swa_row['image'];
																						endif; ?>
																						<ul class="QueBoxIcon">
																							<li>
																								<div class="tooltip uploadicon swipq_item_assets<?php echo $swai ?> <?php echo $swa_cls_dis; ?>" <?php echo $swa_dis; ?>>
																									<label for="file-input-swipq-item-addImg<?php echo $swai ?>">
																										<img class="img-fluid" src="img/list/image.svg" />
																										<span class="tooltiptext">Add Image</span>
																									</label>
																									<input id="file-input-swipq-item-addImg<?php echo $swai ?>" data-id="#swipq_item_img<?php echo $swai ?>" data-assets="<?php echo $swai ?>" class="uploadImgFileSwip swipq_item_assets<?php echo $swai ?>" type="file" name="swipq_item_Img<?php echo $swai ?>" <?php echo $swa_dis; ?> />
																									<input type="hidden" name="swipq_item_img[]" id="swipq_item_img<?php echo $swai ?>" value="<?php echo $swa_item; ?>" />
																								</div>
																							</li>
																						</ul>
																						<div class="swipq_items_assets_container_<?php echo $swai ?>" id="swipq_items_assets" <?php echo ( ! empty($swa_item)) ? '' : 'style="display:none;"'; ?>>
																							<div class="assets_data swipq_items_assets_data_<?php echo $swai ?>"><a class="view_assets" data-src="<?php echo $path . $swa_item; ?>" href="javascript:;"><?php echo $swa_item; ?></a></div>
																							<a href="javascript:void(0);" data-assets="<?php echo $swa_item; ?>" data-assets-id="swipq_item_assets<?php echo $swai ?>" data-input-id="#swipq_item_img<?php echo $swai ?>" data-path="<?php echo $uploadpath ?>" class="delete_assets" title="Delete"><i class="fa fa-times" aria-hidden="true"></i></a>
																						</div>
																					</div>
																				</div>
																			</div>
																			<?php if ($swai == 1): ?>
																			<div class="AddAns-Option AddAns-Swaping"><img class="img-fluid SwaAdd" src="img/list/add_field.svg" /></div>
																			<?php else: ?>
																			<div class="AddAns-Option AddAns-Sequ removeOption" data-remove-answer-id="<?php echo $swa_row['answer_id'] ?>"><img class="img-fluid" src="img/list/delete_field.svg" /></div>
																			<?php endif; ?>
																		</div>
																	</div>
																	<?php $swai++; endforeach; else: ?>
																	<div class="Swa-box">
																		<div class="Radio-box">
																			<label class="radiostyle">
																				<input type="radio" name="swipq_true_option" value="1" checked /><span class="radiomark"></span>
																			</label>
																			<div class="col-sm-12 mcwidth">
																				<div class="choicebox">
																					<div class="form-group">
																						<input type="text" name="swipq_option[]" id="swipq_option" class="form-control QuesForm1 inputtextWrap swipq" placeholder="Click here to enter question" />
																						<ul class="QueBoxIcon">
																							<li>
																								<div class="tooltip uploadicon swipq_item_assets1">
																									<label for="file-input-swipq-item-addImg1">
																										<img class="img-fluid" src="img/list/image.svg" />
																										<span class="tooltiptext">Add Image</span>
																									</label>
																									<input id="file-input-swipq-item-addImg1" data-id="#swipq_item_img1" data-assets="1" class="uploadImgFileSwip swipq_item_assets1" type="file" name="swipq_item_Img1" />
																									<input type="hidden" name="swipq_item_img[]" id="swipq_item_img1" />
																								</div>
																							</li>
																						</ul>
																						<div class="swipq_items_assets_container_1" id="swipq_items_assets" style="display:none">
																							<div class="assets_data swipq_items_assets_data_1"><a class="view_assets" data-src="" href="javascript:;"></a></div>
																							<a href="javascript:void(0);" data-assets="" data-assets-id="swipq_item_assets1" data-input-id="#swipq_item_img1" data-path="<?php echo $uploadpath ?>" class="delete_assets" title="Delete"><i class="fa fa-times" aria-hidden="true"></i></a>
																						</div>
																					</div>
																				</div>
																			</div>
																			<div class="AddAns-Option AddAns-Swaping"><img class="img-fluid SwaAdd" src="img/list/add_field.svg" /></div>
																		</div>
																	</div>
																	<?php endif; ?>
																	</div>
																</div>
															</div>														
														</div>
													</div>
													<div id="Swaping-F" class="tab-pane fade in">
														<div class="col-sm-12 Qusfeedback">
															<div class="form-group">
																<div class="feedback-flex">
																	<h4>Correct</h4>
																	<textarea class="form-control FeedForm inputtextWrap swipq" name="swipq_cfeedback" id="swipq_cfeedback" rows="3" placeholder="Feedback text goes here"><?php echo ($ques_type == 7 && ! empty($feedResult[0]['feedback']) && $feedResult[0]['feedback_type'] == 1) ? stripslashes($feedResult[0]['feedback']) : ''; ?></textarea>
																</div>
																<ul class="QueBoxIcon feed LMfeed">
																	<h5>Add assets</h5>
																	<li class="dropdown">
																		<button class="btn1 btn-secondary dropdown-toggle swipqf_assets <?php echo ( ! empty($qfeed_assets) && $ques_type == 7) ? 'disabled' : ''; ?>" <?php echo ( ! empty($qfeed_assets) && $ques_type == 7) ? 'disabled="disabled"' : ''; ?> type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img class="img-fluid" src="img/list/audio.svg"></button>
																		<div class="dropdown-menu buttom" aria-labelledby="dropdownMenuButton">
																			<a data-toggle="modal" data-target="#swipqfctts" class="dropdown-item">
																				<div class="tooltip1">
																					<img class="img-fluid" src="img/list/text_speech.svg">
																					<span class="tooltiptext">Text to Speech</span>
																				</div>
																			</a>
																			<a class="dropdown-item">
																				<div class="tooltip1 uploadicon">
																					<label for="file-input-swipq-feedback-addAudio">
																						<img class="img-fluid" src="img/list/add_audio.svg">
																						<span class="tooltiptext">Add Audio</span>
																					</label>
																					<input id="file-input-swipq-feedback-addAudio" data-id="#swipq_feedback_audio" data-assets="swipqf_assets" class="uploadAudioFile" type="file" name="swipq_feedback_Audio"/>
																					<input type="hidden" name="swipq_feedback_audio" id="swipq_feedback_audio" value="<?php echo ($ques_type == 7 && $feed_assets_type == 'fa') ? $qfeed_assets : ''; ?>"  />
																				</div>
																			</a>
																			<a class="dropdown-item">
																				<div class="tooltip1">
																					<img class="img-fluid rec-audio" data-input-id="swipq_feedback_rec_audio" data-assets="swipqf_assets" src="img/list/record_audio.svg">
																					<span class="tooltiptext">Record Audio</span>
																				</div>
																				<input type="hidden" name="swipq_feedback_rec_audio" id="swipq_feedback_rec_audio" />
																			</a>
																		</div>
																	</li>
																	<li class="dropdown">
																		<button class="btn1 btn-secondary dropdown-toggle swipqf_assets <?php echo ( ! empty($qfeed_assets) && $ques_type == 7) ? 'disabled' : ''; ?>" <?php echo ( ! empty($qfeed_assets) && $ques_type == 7) ? 'disabled="disabled"' : ''; ?> type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img class="img-fluid" src="img/list/video.svg"></button>
																		<div class="dropdown-menu buttom" aria-labelledby="dropdownMenuButton">
																			<a class="dropdown-item">
																				<div class="tooltip1 uploadicon">
																					<label for="file-input-swipq-feedback-addVideo">
																						<img class="img-fluid" src="img/list/add_video.svg">
																						<span class="tooltiptext">Add Video</span>
																					</label>
																					<input id="file-input-swipq-feedback-addVideo" data-id="#swipq_feedback_video" data-assets="swipqf_assets" class="uploadVideoFile" type="file" name="swipq_feedback_Video" />
																					<input type="hidden" name="swipq_feedback_video" id="swipq_feedback_video" value="<?php echo ($ques_type == 7 && $feed_assets_type == 'fv') ? $qfeed_assets : ''; ?>" />
																				</div>
																			</a>
																			<a class="dropdown-item">
																				<div class="tooltip1">
																					<img class="img-fluid rec-video" data-input-id="swipq_feedback_rec_video" data-assets="swipqf_assets" src="img/qus_icon/rec_video.svg" />
																					<span class="tooltiptext">Record Video</span>
																				</div>
																				<input type="hidden" name="swipq_feedback_rec_video" id="swipq_feedback_rec_video" />
																			</a>
																			<a class="dropdown-item">
																				<div class="tooltip1">
																					<img class="img-fluid rec-screen" data-input-id="swipq_feedback_rec_screen" data-assets="swipqf_assets" src="img/list/screen_record.svg" />
																					<span class="tooltiptext">Record Screen</span>
																				</div>
																				<input type="hidden" name="swipq_feedback_rec_screen" id="swipq_feedback_rec_screen" value="<?php echo ($ques_type == 7 && $feed_assets_type == 'fs') ? $qfeed_assets : ''; ?>"  />
																			</a>
																		</div>
																	</li>
																	<li>
																		<div class="tooltip uploadicon swipqf_assets <?php echo ( ! empty($qfeed_assets) && $ques_type == 7) ? 'disabled' : ''; ?>" <?php echo ( ! empty($qfeed_assets) && $ques_type == 7) ? 'disabled="disabled"' : ''; ?>>
																			<label for="file-input-swipq-feedback-addImg">
																				<img class="img-fluid" src="img/list/image.svg">
																				<span class="tooltiptext">Add Image</span>
																			</label>
																			<input id="file-input-swipq-feedback-addImg" data-id="#swipq_feedback_img" data-assets="swipqf_assets" class="uploadImgFile swipqf_assets" type="file" name="swipq_feedback_Img" <?php echo ( ! empty($qfeed_assets) && $ques_type == 7) ? 'disabled="disabled"' : ''; ?> />
																			<input type="hidden" name="swipq_feedback_img" id="swipq_feedback_img" value="<?php echo ($ques_type == 7 && $feed_assets_type == 'fi') ? $qfeed_assets : ''; ?>"  />
																		</div>
																	</li>
																	<li>
																		<div class="tooltip uploadicon swipqf_assets <?php echo ( ! empty($qfeed_assets) && $ques_type == 7) ? 'disabled' : ''; ?>" <?php echo ( ! empty($qfeed_assets) && $ques_type == 7) ? 'disabled="disabled"' : ''; ?>>
																			<label for="file-input-swipq-feedback-addDoc">
																				<img class="img-fluid" src="img/list/add_document.svg" />
																				<span class="tooltiptext">Add Document</span>
																			</label>
																			<input id="file-input-swipq-feedback-addDoc" data-id="#swipq_feedback_doc" data-assets="swipqf_assets" class="uploadDocFile swipqf_assets" type="file" name="swipq_feedback_Doc" <?php echo ( ! empty($qfeed_assets) && $ques_type == 7) ? 'disabled="disabled"' : ''; ?>/>
																			<input type="hidden" name="swipq_feedback_doc" id="swipq_feedback_doc" value="<?php echo ($ques_type == 7 && $feed_assets_type == 'fd') ? $qfeed_assets : ''; ?>" />
																		</div>
																	</li>
																</ul>                              
															</div>
															<div class="swipqf_assets_container" <?php echo ( ! empty($qfeed_assets) && $ques_type == 7) ? '' : 'style="display:none;"'; ?>>
																<div class="assets_data swipqf_assets_data"><a class="view_assets" data-src="<?php echo $path . $qfeed_assets; ?>" href="javascript:;"><?php echo $qfeed_assets; ?></a></div>
																<a href="javascript:void(0);" data-assets="<?php echo $qfeed_assets ?>" data-assets-id="swipqf_assets" data-input-id="#swipq_feedback_audio,#swipq_feedback_rec_audio,#swipq_feedback_video,#swipq_feedback_rec_video,#swipq_feedback_rec_screen,#swipq_feedback_img" data-path="<?php echo $uploadpath ?>" class="delete_assets" title="Delete"><i class="fa fa-times" aria-hidden="true"></i></a>
															</div>
															<div class="form-group">
																<div class="feedback-flex">
																	<h4>Incorrect</h4>
																	<textarea class="form-control FeedForm inputtextWrap swipq" name="swipq_ifeedback" id="swipq_ifeedback" placeholder="You did not select the correct response."><?php echo ($ques_type == 7 && ! empty($feedResult[1]['feedback']) && $feedResult[1]['feedback_type'] == 2) ? stripslashes($feedResult[1]['feedback']) : ''; ?></textarea>
																</div>
																<ul class="QueBoxIcon feed LMfeed">
																	<h5>Add assets</h5>
																	<li class="dropdown">
																		<button class="btn1 btn-secondary dropdown-toggle swipqfi_assets <?php echo ( ! empty($qifeed_assets) && $ques_type == 7) ? 'disabled' : ''; ?>" <?php echo ( ! empty($qifeed_assets) && $ques_type == 7) ? 'disabled="disabled"' : ''; ?> type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img class="img-fluid" src="img/list/audio.svg" /></button>
																		<div class="dropdown-menu buttom" aria-labelledby="dropdownMenuButton">
																			<a data-toggle="modal" data-target="#swipqfitts" class="dropdown-item">
																				<div class="tooltip1">
																					<img class="img-fluid" src="img/list/text_speech.svg" />
																					<span class="tooltiptext">Text to Speech</span>
																				</div>
																			</a>
																			<a class="dropdown-item">
																				<div class="tooltip1 uploadicon">
																					<label for="file-input-swipqi-feedback-addAudio">
																						<img class="img-fluid" src="img/list/add_audio.svg" />
																						<span class="tooltiptext">Add Audio</span>
																					</label>
																					<input id="file-input-swipqi-feedback-addAudio" data-id="#swipqi_feedback_audio" data-assets="swipqfi_assets" class="uploadAudioFile" type="file" name="swipqi_feedback_Audio"/>
																					<input type="hidden" name="swipqi_feedback_audio" id="swipqi_feedback_audio" value="<?php echo ($ques_type == 7 && $feed_assets_type == 'fa') ? $qifeed_assets : ''; ?>" />
																				</div>
																			</a>
																			<a class="dropdown-item">
																				<div class="tooltip1">
																					<img class="img-fluid rec-audio" data-input-id="swipqi_feedback_rec_audio" data-assets="swipqfi_assets" src="img/list/record_audio.svg" />
																					<span class="tooltiptext">Record Audio</span>
																				</div>
																				<input type="hidden" name="swipqi_feedback_rec_audio" id="swipqi_feedback_rec_audio" />
																			</a>
																		</div>
																	</li>
																	<li class="dropdown">
																		<button class="btn1 btn-secondary dropdown-toggle swipqfi_assets <?php echo ( ! empty($qifeed_assets) && $ques_type == 7) ? 'disabled' : ''; ?>" <?php echo ( ! empty($qifeed_assets) && $ques_type == 7) ? 'disabled="disabled"' : ''; ?> type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img class="img-fluid" src="img/list/video.svg"></button>
																		<div class="dropdown-menu buttom" aria-labelledby="dropdownMenuButton">
																			<a class="dropdown-item">
																				<div class="tooltip1 uploadicon">
																					<label for="file-input-swipqi-feedback-addVideo">
																						<img class="img-fluid" src="img/list/add_video.svg">
																						<span class="tooltiptext">Add Video</span>
																					</label>
																					<input id="file-input-swipqi-feedback-addVideo" data-id="#swipqi_feedback_video" data-assets="swipqfi_assets" class="uploadVideoFile" type="file" name="swipqi_feedback_Video" />
																					<input type="hidden" name="swipqi_feedback_video" id="swipqi_feedback_video" value="<?php echo ($ques_type == 7 && $feed_assets_type == 'fv') ? $qifeed_assets : ''; ?>" />
																				</div>
																			</a>
																			<a class="dropdown-item">
																				<div class="tooltip1">
																					<img class="img-fluid rec-video" data-input-id="swipqi_feedback_rec_video" data-assets="swipqfi_assets" src="img/qus_icon/rec_video.svg" />
																					<span class="tooltiptext">Record Video</span>
																				</div>
																				<input type="hidden" name="swipqi_feedback_rec_video" id="swipqi_feedback_rec_video" />
																			</a>
																			<a class="dropdown-item">
																				<div class="tooltip1">
																					<img class="img-fluid rec-screen" data-input-id="swipqi_feedback_rec_screen" data-assets="swipqfi_assets" src="img/list/screen_record.svg" />
																					<span class="tooltiptext">Record Screen</span>
																				</div>
																				<input type="hidden" name="swipqi_feedback_rec_screen" id="swipqi_feedback_rec_screen" value="<?php echo ($ques_type == 7 && $feed_assets_type == 'fs') ? $qifeed_assets : ''; ?>" />
																			</a>
																		</div>
																	</li>
																	<li>
																		<div class="tooltip uploadicon swipqfi_assets <?php echo ( ! empty($qifeed_assets) && $ques_type == 7) ? 'disabled' : ''; ?>" <?php echo ( ! empty($qifeed_assets) && $ques_type == 7) ? 'disabled="disabled"' : ''; ?>>
																			<label for="file-input-swipqi-feedback-addImg">
																				<img class="img-fluid" src="img/list/image.svg">
																				<span class="tooltiptext">Add Image</span>
																			</label>
																			<input id="file-input-swipqi-feedback-addImg" data-id="#swipqi_feedback_img" data-assets="swipqfi_assets" class="uploadImgFile swipqfi_assets" type="file" name="swipqi_feedback_Img" <?php echo ( ! empty($qifeed_assets) && $ques_type == 7) ? 'disabled="disabled"' : ''; ?> />
																			<input type="hidden" name="swipqi_feedback_img" id="swipqi_feedback_img" value="<?php echo ($ques_type == 7 && $feed_assets_type == 'fi') ? $qifeed_assets : ''; ?>" />
																		</div>
																	</li>
																	<li>
																		<div class="tooltip uploadicon swipqfi_assets <?php echo ( ! empty($qifeed_assets) && $ques_type == 7) ? 'disabled' : ''; ?>" <?php echo ( ! empty($qifeed_assets) && $ques_type == 7) ? 'disabled="disabled"' : ''; ?>>
																			<label for="file-input-swipqi-feedback-addDoc">
																				<img class="img-fluid" src="img/list/add_document.svg" />
																				<span class="tooltiptext">Add Document</span>
																			</label>
																			<input id="file-input-swipqi-feedback-addDoc" data-id="#swipqi_feedback_doc" data-assets="swipqfi_assets" class="uploadDocFile swipqfi_assets" type="file" name="swipqi_feedback_Doc" <?php echo ( ! empty($qifeed_assets) && $ques_type == 7) ? 'disabled="disabled"' : ''; ?> />
																			<input type="hidden" name="swipqi_feedback_doc" id="swipqi_feedback_doc" value="<?php echo ($ques_type == 7 && $feed_assets_type == 'fd') ? $qifeed_assets : ''; ?>" />
																		</div>
																	</li>
																</ul>                              
															</div>                                                    
															<div class="swipqfi_assets_container" <?php echo ( ! empty($qifeed_assets) && $ques_type == 7) ? '' : 'style="display:none;"'; ?>>
																<div class="assets_data swipqfi_assets_data"><a class="view_assets" data-src="<?php echo $path . $qifeed_assets; ?>" href="javascript:;"><?php echo $qifeed_assets; ?></a></div>
																<a href="javascript:void(0);" data-assets="<?php echo $qifeed_assets ?>" data-assets-id="swipqfi_assets" data-input-id="#swipqi_feedback_audio,#swipqi_feedback_rec_audio,#swipqi_feedback_video,#mcqi_feedback_rec_video,#swipqi_feedback_rec_screen,#swipqi_feedback_img" data-path="<?php echo $uploadpath ?>" class="delete_assets" title="Delete"><i class="fa fa-times" aria-hidden="true"></i></a>
															</div>
														</div>
														<div class="form-popup draggable Ques-pop-style modal" id="swipqfctts">
															<div class="popheading">Insert Text to Speech</div>
															<div class="textarea">
																<textarea type="text" class="form-control1 inputtextWrap" name="swipq_feedback_text_to_speech" id="swipq_feedback_text_to_speech"><?php echo ($ques_type == 7 && ! empty($feedResult[0]['feedback']) && $feedResult[0]['feedback_type'] == 1) ? stripslashes($feedResult[0]['feed_speech_text']) : ''; ?></textarea>
															</div>
															<div class="modal-footer">
																<button type="button" class="btn1 submitbtn1" data-dismiss="modal">Insert</button>
																<button type="button" class="btn1 submitbtn1" onclick="clearData('swipq_feedback_text_to_speech');">Clear</button>
																<button type="button" class="btn1 submitbtn1" data-dismiss="modal">Cancel</button>
															</div>
														</div>
														<div class="form-popup draggable Ques-pop-style modal" id="swipqfitts">
															<div class="popheading">Insert Text to Speech</div>
															<div class="textarea">
																<textarea type="text" class="form-control1 inputtextWrap" name="swipqi_feedback_text_to_speech" id="swipqi_feedback_text_to_speech"><?php echo ($ques_type == 7 && ! empty($feedResult[1]['feedback']) && $feedResult[1]['feedback_type'] == 2) ? stripslashes($feedResult[1]['feed_speech_text']) : ''; ?></textarea>
															</div>
															<div class="modal-footer">
																<button type="button" class="btn1 submitbtn1" data-dismiss="modal">Insert</button>
																<button type="button" class="btn1 submitbtn1" onclick="clearData('swipqi_feedback_text_to_speech');">Clear</button>
																<button type="button" class="btn1 submitbtn1" data-dismiss="modal">Cancel</button>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
    <div class="sim_bluebanner">
        <ul class="Ques_linear_save">
           <li><button type="submit" class="submitques update_next"><img src="img/list/save-t.svg" />SAVE & CONTINUE</button></li>
           <li><button type="submit" class="submitques update_close"><img src="img/list/close.svg" />SAVE & CLOSE</button></li>
        </ul> 
    </div>
</form>

<div id="load_popup_modal_show" class="modal fade" tabindex="-1"></div>

<script src="content/js/jquery-ui-1.12.1/jquery-ui.js"></script>
<script src="content/js/jquery.fontselect.js"></script>
<script src="content/js/competency-validate.js"></script>
<script type="text/javascript">
$(function(){
	$(".draggable").draggable();
});

var assets_path = '<?php echo $path; ?>';

$('body').on('click', '.view_assets', function(e){
    e.preventDefault();
    var to = $(this).attr('data-src');
    $.fancybox.open({
        type: 'iframe',
        src: to,
        toolbar  : false,
        smallBtn : true,
		closeExisting: false,
		scrolling: 'no',
        iframe : {
            preload : true,
            scrolling: 'no',
			css:{
					width:'100%',
					height:'100%'
				}
        }
    });
});

$(".inputtextWrap").hover(function() {
	$(this).attr('title', $(this).val());
}, function() {
	$(this).css('cursor','auto');
});

/* Set Default Font Type */
$('.cl_ftype').click(function () {
	$('#font_type').trigger('setFont', '');
});

function isNumberKey(evt) {
	var charCode = (evt.which) ? evt.which : evt.keyCode
	return !(charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57));
}

function clearData(id){
	$('#'+ id).val('');
}

$("#sortable").sortable({
	update: function() {
		serial = $('#sortable').sortable('serialize');
		$.LoadingOverlay("show");
		$.ajax({
            data: serial + '&qorder=true&sim_id=<?php echo $sim_id ?>',
            type: 'POST',
            url: 'includes/process.php',
			success:function(resdata) {
				getQuesList();
				$.LoadingOverlay("hide");
			},error: function() {
				$.LoadingOverlay("hide");
				swal({text: 'Oops. something went wrong please try again.', buttons: false, icon: "error", timer: 2000 });
			}
        });
	}
});

/* Get Questions List */
function getQuesList() {
	$.ajax({
		type: 'GET',
		url: 'includes/process.php',
		data: 'getQuesList=true&sim_id=<?php echo $sim_id ?>',
		success:function(resdata) {
			$.LoadingOverlay("hide");
			var res = $.parseJSON(resdata);
			if (res.success == true) {
				var base 	= $('#location').val();
				var current	= window.location.href;
				var qurl 	= '';
				var qclass	= '';
				var qno		= 1;
				$('.question-list').empty();
				$.each(res.data, function (key, val) {
					qurl = base + '&ques_id='+ val.ques_id;
					if (current == qurl){
						qclass = 'active';
					} else {
						qclass = '';
					}
					var qlist = '<label class="ui-state-default '+ qclass +'" id="question-'+ val.qid +'"><span class="QNum">'+ qno +'</span><input type="checkbox" name="ques_no[]" class="form-control ques_no ui-state-CheCK" value="'+ val.qid +'" /><a  class="ui-state-text" href="'+ qurl +'"><img class="updownQues" src="img/list/up-down.svg"><li>'+ val.qname +'</li></a></label>';
					$('.question-list').append(qlist);
					qno++;
				});
			} else {
				swal({text: 'Questions list not load please try again later.', buttons: false, icon: "error", timer: 2000 });
			}
		},error: function() {
			$.LoadingOverlay("hide");
			swal({text: 'Oops. something went wrong please try again.', buttons: false, icon: "error", timer: 2000 });
		}
	});
}

getQuesList();
	
$('.update_next').on('click', function(){
	$('#submit_type').val(1);
});

$('.update_close').on('click', function(){
	$('#submit_type').val(2);
});

$('.type-color-on-page').spectrum({
	type: "component",
	togglePaletteOnly: "true",
	hideAfterPaletteSelect: "true",
	showInput: "true",
	showInitial: "true",
});

$('#font_type').fontselect({
	placeholder: 'Pick a font from the list',
	searchable: false,
	systemFonts: [],
	googleFonts: [<?php echo $db::googleFont; ?>]
});

/* Drag and Drop Selected Option */
$('.dragoption').on('click', function(){
	$('#seleted_drag_option').val($(this).val());
});

/* Backgrounds */
$.ajax({
	type: 'GET',
	url: 'includes/ajax.php',
	data: 'getCharBackground=true&type=1',
	success:function(resdata) {
		$.LoadingOverlay("hide");
		var res = $.parseJSON(resdata);
		if (res.success == true) {
			var bg_selected = '';
			$.each(res.data, function (key, val) {
				if (val.img_name == $('#get_bg_img').val()){
					bg_selected = 'checked="checked"';
				}
				else { bg_selected = ''; }
				var option = '<div class="bacground_banner">\
				<div class="Radio-box MCQRadio"><label class="radiostyle"><input type="radio" name="sim_background_img" value="'+val.img_name+'" '+bg_selected+'><span class="radiomark"></span></label><a data-fancybox="gallery" href="img/char_bg/'+val.img_name+'"><img src="img/char_bg/'+val.img_name+'"></a>';
				<?php if (isset($role) && $role == 'admin'): ?>
				option += '<a href="javascript:void(0);" data-upload-id="'+val.upload_id+'" data-img-name="'+val.img_name+'" class="delete_char_bg_img" title="Delete File"><i class="fa fa-times" aria-hidden="true"></i></a></div>';
				<?php endif; ?>
				$('.BGIMG').append(option);
			});
		}
		else { swal({text: 'Background image not loading please try again later.', buttons: false, icon: "error", timer: 2000 }); }
	},error: function() {
		$.LoadingOverlay("hide");
		swal({text: 'Oops. something went wrong please try again.', buttons: false, icon: "error", timer: 2000 });
	}
});

/* One Character */
$.ajax({
	type: 'GET',
	url: 'includes/ajax.php',
	data: 'getCharBackground=true&type=2,3',
	success:function(resdata) {
		$.LoadingOverlay("hide");
		var res = $.parseJSON(resdata);
		if (res.success == true) {
			var one_char_selected = '';
			$.each(res.data, function (key, val) {
				if (val.img_name == $('#get_sim_char_img').val()){
					one_char_selected = 'checked="checked"';
				}
				else { one_char_selected = ''; }
				var option = '<div class="bacground_banner">\
				<div class="Radio-box MCQRadio"><label class="radiostyle"><input type="radio" name="sim_char_img" class="1char" value="'+val.img_name+'" '+one_char_selected+'><span class="radiomark"></span></label><a data-fancybox="gallery1" href="img/char_bg/'+val.img_name+'"><img src="img/char_bg/'+val.img_name+'"></a>';
				<?php if (isset($role) && $role == 'admin'): ?>
				option += '<a href="javascript:void(0);" data-upload-id="'+val.upload_id+'" data-img-name="'+val.img_name+'" class="delete_char_bg_img" title="Delete File"><i class="fa fa-times" aria-hidden="true"></i></a></div>';
				<?php endif; ?>
				$('.OneCharIMG').append(option);
			});
		}
		else { swal({text: 'Background image not loading please try again later.', buttons: false, icon: "error", timer: 2000 }); }
	},error: function() {
		$.LoadingOverlay("hide");
		swal({text: 'Oops. something went wrong please try again.', buttons: false, icon: "error", timer: 2000 });
	}
});

/* Two Characters */
$.ajax({
	type: 'GET',
	url: 'includes/ajax.php',
	data: 'getCharBackground=true&type=2,3',
	success:function(resdata) {
		$.LoadingOverlay("hide");
		var res = $.parseJSON(resdata);
		if (res.success == true) {
			var sim_char_left_img  = $('#sim_char_left_img').val();
			var sim_char_right_img = $('#sim_char_right_img').val();
			var char_left  = '';
			var char_right = '';
			$.each(res.data, function (key, val) {
				if (sim_char_left_img != '' && sim_char_left_img == val.img_name) {
					char_left = 'checked="checked"';
					get_value = 'value="'+val.img_name+'|L"'; 
				} else { char_left = ''; get_value = 'value="'+val.img_name+'"';  }
				
				if (sim_char_right_img != '' && sim_char_right_img == val.img_name) {
					char_right = 'checked="checked"';
					get_value = 'value="'+val.img_name+'|R"'; 
				} else { char_right = ''; get_value = 'value="'+val.img_name+'"';  }
				
				var option = '<div class="bacground_banner">\
				<div class="Check-box"><label class="checkstyle"><input type="checkbox" name="sim_two_char_img[]" class="2char" '+get_value+' '+char_left+' '+char_right+'>';
				if (char_left != ''){
					option += '<span class="leftIMG" title="Left side character">L</span>';
				}
				else if (char_right != ''){
					option += '<span class="RightIMG" title="Right side character">R</span>';
				}
				option += '<span class="checkmark"></span></label><a data-fancybox="gallery2" href="img/char_bg/'+val.img_name+'"><img src="img/char_bg/'+val.img_name+'"></a>';
				<?php if (isset($role) && $role == 'admin'): ?>
				option += '<a href="javascript:void(0);" data-upload-id="'+val.upload_id+'" data-img-name="'+val.img_name+'" class="delete_char_bg_img" title="Delete File"><i class="fa fa-times" aria-hidden="true"></i></a>';
				<?php endif; ?>
				$('.TwoCharIMG').append(option);
			});
		}
		else { swal({text: 'Background image not loading please try again later.', buttons: false, icon: "error", timer: 2000 }); }
	},error: function() {
		$.LoadingOverlay("hide");
		swal({text: 'Oops. something went wrong please try again.', buttons: false, icon: "error", timer: 2000 });
	}
});

/* Add New Question */
$('.AddIcon').on('click', function(e){
	e.preventDefault();
	$.LoadingOverlay("show");
	var simID = $('#sim_id').val();
	if (simID != '') {
		var dataString = 'add_Question='+ true +'&sim_id='+ simID;
		$.ajax({
			type: 'POST',
			url: 'includes/process.php',
			data: dataString,
			cache: false,
			success: function(resdata) {
				var res = $.parseJSON(resdata);
				if (res.success == true) {
					$.LoadingOverlay("hide");
					getQuesList();
				}
				else if (res.success == false) {
					$.LoadingOverlay("hide");
					swal({text: res.msg, buttons: false, icon: "warning", timer: 2000});
				}
			}, error: function() {
				swal({text: 'Oops. something went wrong please try again.', buttons: false, icon: "error", timer: 2000 });
			}
		});
	}
});

/* Delete Question */
$('.DelIcon').on('click', function(e){
	e.preventDefault();
	var ques = $('.ques_no:checked').map(function(){ return this.value; }).get();
	if (ques.length > 0) {
		$.LoadingOverlay("show");
		var dataString = 'del_Question='+ true +'&ques_id='+ ques;
		$.ajax({
			type: 'POST',
			url: 'includes/process.php',
			data: dataString,
			cache: false,
			success: function(resdata) {
				var res = $.parseJSON(resdata);
				if (res.success == true) {
					$.LoadingOverlay("hide");
					getQuesList();
				}
				else if (res.success == false) {
					$.LoadingOverlay("hide");
					swal({text: res.msg, buttons: false, icon: "warning", timer: 2000});
				}
			}, error: function() {
				swal({text: 'Oops. something went wrong please try again.', buttons: false, icon: "error", timer: 2000 });
			}
		});
	} else {
		swal({text: 'Please select at-least one Question', buttons: false, icon: "warning", timer: 2000 });
	}
});

/* Clone Question */
$('.DuPIcon').on('click', function(e){
	e.preventDefault();
	var ques = $('.ques_no:checked').map(function(){ return this.value; }).get();
	if (ques.length == 1) {
		$.LoadingOverlay("show");
		var dataString = 'clone_Question='+ true +'&ques_id='+ ques;
		$.ajax({
			type: 'POST',
			url: 'includes/process.php',
			data: dataString,
			cache: false,
			success: function(resdata) {
				$.LoadingOverlay("hide");
				var res = $.parseJSON(resdata);
				if (res.success == true) {
					swal({text: res.msg, buttons: false, icon: "success", timer: 2000});
					getQuesList();
				}
				else if (res.success == false) {
					swal({text: res.msg, buttons: false, icon: "warning", timer: 2000});
				}
			}, error: function() {
				$.LoadingOverlay("hide");
				swal({text: 'Oops. something went wrong please try again.', buttons: false, icon: "error", timer: 2000 });
			}
		});
	} else {
		swal({text: 'Please select one Question', buttons: false, icon: "warning", timer: 2000 });
	}
});

/* Template Type */
$('.qtype').on('click', function(){
	var type = $(this).data('qtype');
	var get_type = $('#get_question_type') .val();
	/* MTF */
	if (type == 1){
		$('.shuffle_off').attr("required", true);
	}
	else {
		$('.shuffle_off').removeAttr("required", true);
	}

	if (get_type == '' && type != '' && type == 1) {
		$('#question_type').val(type);
	}
	else if (get_type == '' && type != '' && type == 2) {
		$('#question_type').val(type);
	}
	else if (get_type == '' && type != '' && type == 3) {
		$('#question_type').val(type);
	}
	else if (get_type == '' && type != '' && type == 8) {
		$('#question_type').val(type);
	}
	else if (get_type == '' && type != '' && type == 4) {
		$('#question_type').val(type);
	}
	else if (get_type == '' && type != '' && type == 5) {
		$('#question_type').val(type);
	}
	else if (get_type == '' && type != '' && type == 7) {
		$('#question_type').val(type);
	}
});

$('.mtf_shuffle').on('click', function(){
	var mtf_check = $(this).val();
	if (mtf_check == 2) {
		$('.shuffle_off').removeAttr("required", true);
		$('.shuffle_on').attr("required", true);
	}
	else {
		$('.shuffle_on').removeAttr("required", true);
		$('.shuffle_off').attr("required", true);
	}
});

//------Competency-Score-------
$('#ques_val_1,#sq_ques_val_1,#sort_ques_val_1,#mcq_ques_val_1,#mmcq_ques_val_1,#swipq_ques_val_1').on('change', function(){
	var score = parseInt($(this).val());
	var maxallow = parseInt($('#comp_val_1').val());
	if (maxallow < score) {
		$(this).val('');
		swal('Maximum '+maxallow+' score', { buttons: false, timer: 1000 });
	}
});

$('#ques_val_2,#sq_ques_val_2,#sort_ques_val_2,#mcq_ques_val_2,#mmcq_ques_val_2,#swipq_ques_val_2').on('change', function(){
	var score = parseInt($(this).val());
	var maxallow = parseInt($('#comp_val_2').val());
	if (score > maxallow) {
		$(this).val('');
		swal('Maximum '+maxallow+' score', { buttons: false, timer: 1000 });
	}
});

$('#ques_val_3,#sq_ques_val_3,#sort_ques_val_3,#mcq_ques_val_3,#mmcq_ques_val_3,#swipq_ques_val_3').on('change', function(){
	var score = parseInt($(this).val());
	var maxallow = parseInt($('#comp_val_3').val());
	if (score > maxallow) {
		$(this).val('');
		swal('Maximum '+maxallow+' score', { buttons: false, timer: 1000 });
	}
});

$('#ques_val_4,#sq_ques_val_4,#sort_ques_val_4,#mcq_ques_val_4,#mmcq_ques_val_4,#swipq_ques_val_4').on('change', function(){
	var score = parseInt($(this).val());
	var maxallow = parseInt($('#comp_val_4').val());
	if (score > maxallow) {
		$(this).val('');
		swal('Maximum '+maxallow+' score', { buttons: false, timer: 1000 });
	}
});

$('#ques_val_5,#sq_ques_va_5,#sort_ques_va_5,#mcq_ques_val_5,#mmcq_ques_val_5,#swipq_ques_val_5').on('change', function(){
	var score = parseInt($(this).val());
	var maxallow = parseInt($('#comp_val_5').val());
	if (score > maxallow) {
		$(this).val('');
		swal('Maximum '+maxallow+' score', { buttons: false, timer: 1000 });
	}
});

$('#ques_val_6,#sq_ques_va_6,#sort_ques_va_6,#mcq_ques_val_6,#mmcq_ques_val_6,#swipq_ques_val_6').on('change', function(){
	var score = parseInt($(this).val());
	var maxallow = parseInt($('#comp_val_6').val());
	if (score > maxallow) {
		$(this).val('');
		swal('Maximum '+maxallow+' score', { buttons: false, timer: 1000 });
	}
});

//----------Audio-Upload-All-Template--------------------
$('body').on('change', '.uploadAudioFile', function(){
	$.LoadingOverlay("show");
	var input_id	= $(this).data('id');
	var assets_id	= $(this).data('assets');
	var file_data	= $(this).prop('files')[0];
	var form_data	= new FormData();
	form_data.append('file', file_data);
	form_data.append('qAudiofiles', '1');
	$.ajax({
		url: "includes/upload.php",
		type: "POST",
		data: form_data,
		contentType: false,
		cache: false,
		processData: false,
		success: function(resdata){
			var res = $.parseJSON(resdata);
			if (res.success == true){
				$(input_id).val(res.file_name);						
				$('.'+ assets_id).addClass("disabled").attr('disabled', true);
				$('.'+ assets_id + '_container').show();
				$('.'+ assets_id + '_data a').text(res.file_name);
				$('.'+ assets_id + '_data a').attr('data-src', assets_path + res.file_name);
				$('.'+ assets_id + '_container a.delete_assets').attr('data-assets', res.file_name);
				$('.'+ assets_id + '_container a.delete_assets').attr('data-assets-id', assets_id);
				$('.'+ assets_id + '_container a.delete_assets').attr('data-input-id', input_id);
				$('.'+ assets_id + '_container a.delete_assets').attr('data-path', res.path);
				swal({text: res.msg, buttons: false, timer: 1000});
			}
			else if (res.success == false){
				$(input_id).val('');
				swal({text: res.msg, buttons: false, icon: "warning", timer: 1000});
			}
			$.LoadingOverlay("hide");
		},error: function(){
			$.LoadingOverlay("hide");
			swal("Error", 'Oops, something went wrong. Please try again later.?', "error");
		}
	});
});

//-----------Video-Upload-All-Template-------------------
$('body').on('change', '.uploadVideoFile', function() {
	$.LoadingOverlay("show");
	var input_id	= $(this).data('id');
	var assets_id	= $(this).data('assets');
	var file_data	= $(this).prop('files')[0];
	var form_data	= new FormData();
	form_data.append('file', file_data);
	form_data.append('qVideofiles', '1');
	$.ajax({
		url: "includes/upload.php",
		type: "POST",
		data: form_data,
		contentType: false,
		cache: false,
		processData: false,
		success: function(resdata) {
			var res = $.parseJSON(resdata);
			if (res.success == true) {
				$(input_id).val(res.file_name);
				swal({text: res.msg, buttons: false, timer: 1000});
				$('.'+ assets_id).addClass("disabled").attr('disabled', true);
				$('.'+ assets_id + '_container').show();
				$('.'+ assets_id + '_data a').text(res.file_name);
				$('.'+ assets_id + '_data a').attr('data-src', assets_path + res.file_name);
				$('.'+ assets_id + '_container a.delete_assets').attr('data-assets', res.file_name);
				$('.'+ assets_id + '_container a.delete_assets').attr('data-assets-id', assets_id);
				$('.'+ assets_id + '_container a.delete_assets').attr('data-input-id', input_id);
				$('.'+ assets_id + '_container a.delete_assets').attr('data-path', res.path);
			}
			else if (res.success == false) {
				$(input_id).val('');
				swal({text: res.msg, buttons: false, icon: "warning", timer: 1000});
			}
			$.LoadingOverlay("hide");
		},error: function() {
			$.LoadingOverlay("hide");
			swal("Error", 'Oops, something went wrong. Please try again later.?', "error");
		}
	});
});

//-----------Image-Upload-All-Template-------------------
$('body').on('change', '.uploadImgFile', function() {
	$.LoadingOverlay("show");
	var cur 		= $(this);
	var input_id	= $(this).data('id');
	var assets_id	= $(this).data('assets');
	var file_data	= $(this).prop('files')[0];
	var form_data	= new FormData();
	form_data.append('file', file_data);
	form_data.append('qImgfiles', '1');
	$.ajax({
		url: "includes/upload.php",
		type: "POST",
		data: form_data,
		contentType: false,
		cache: false,
		processData: false,
		success: function(resdata) {
			var res = $.parseJSON(resdata);
			if (res.success == true) {
				$(input_id).val(res.file_name);
				swal({text: res.msg, buttons: false, timer: 1000});
				cur.closest('div').addClass("disabled");
				$('.'+ assets_id).addClass("disabled").attr('disabled', true);
				$('.'+ assets_id + '_container').show();
				$('.'+ assets_id + '_data a').text(res.file_name);
				$('.'+ assets_id + '_data a').attr('data-src', assets_path + res.file_name);
				$('.'+ assets_id + '_container a.delete_assets').attr('data-assets', res.file_name);
				$('.'+ assets_id + '_container a.delete_assets').attr('data-assets-id', assets_id);
				$('.'+ assets_id + '_container a.delete_assets').attr('data-input-id', input_id);
				$('.'+ assets_id + '_container a.delete_assets').attr('data-path', res.path);
			}
			else if (res.success == false) {
				$(input_id).val('');
				swal({text: res.msg, buttons: false, icon: "warning", timer: 1000});
			}
			$.LoadingOverlay("hide");
		},error: function() {
			$.LoadingOverlay("hide");
			swal("Error", 'Oops, something went wrong. Please try again later.?', "error");
		}
	});
});

//-----------Doc-Upload-All-Template---------------------
$('body').on('change', '.uploadDocFile', function() {
	$.LoadingOverlay("show");
	var cur 		= $(this);
	var input_id	= $(this).data('id');
	var assets_id	= $(this).data('assets');
	var file_data	= $(this).prop('files')[0];
	var form_data	= new FormData();
	form_data.append('file', file_data);
	form_data.append('qDocfiles', '1');
	$.ajax({
		url: "includes/upload.php",
		type: "POST",
		data: form_data,
		contentType: false,
		cache: false,
		processData: false,
		success: function(resdata) {
			var res = $.parseJSON(resdata);
			if (res.success == true) {
				$(input_id).val(res.file_name);
				swal({text: res.msg, buttons: false, timer: 1000});
				cur.closest('div').addClass("disabled");
				$('.'+ assets_id).addClass("disabled").attr('disabled', true);
				$('.'+ assets_id + '_container').show();
				$('.'+ assets_id + '_data a').text(res.file_name);
				$('.'+ assets_id + '_data a').attr('data-src', assets_path + res.file_name);
				$('.'+ assets_id + '_container a.delete_assets').attr('data-assets', res.file_name);
				$('.'+ assets_id + '_container a.delete_assets').attr('data-assets-id', assets_id);
				$('.'+ assets_id + '_container a.delete_assets').attr('data-input-id', input_id);
				$('.'+ assets_id + '_container a.delete_assets').attr('data-path', res.path);
			}
			else if (res.success == false) {
				$(input_id).val('');
				swal({text: res.msg, buttons: false, icon: "warning", timer: 1000});
			}
			$.LoadingOverlay("hide");
		},error: function() {
			$.LoadingOverlay("hide");
			swal("Error", 'Oops, something went wrong. Please try again later.?', "error");
		}
	});
});

//-----------Image-Upload-Sorting-Template---------------
$('body').on('change', '.uploadImgFileSort', function() {
	$.LoadingOverlay("show");
	var cur 		= $(this);
	var input_id	= $(this).data('id');
	var assets_id	= $(this).data('assets');
	var file_data	= $(this).prop('files')[0];
	var form_data	= new FormData();
	form_data.append('file', file_data);
	form_data.append('qImgfiles', '1');
	$.ajax({
		url: "includes/upload.php",
		type: "POST",
		data: form_data,
		contentType: false,
		cache: false,
		processData: false,
		success: function(resdata) {
			var res = $.parseJSON(resdata);
			if (res.success == true) {
				$(input_id).val(res.file_name);
				$('.sortq_item_assets'+ assets_id).addClass("disabled").attr('disabled', true);
				cur.closest('div').addClass("disabled");
				$('.sortq_items_assets_container_'+ assets_id).show();
				$('.sortq_items_assets_data_'+ assets_id + ' a.view_assets').text(res.file_name);
				$('.sortq_items_assets_data_'+ assets_id + ' a.view_assets').attr('data-src', assets_path + res.file_name);
				$('.sortq_items_assets_container_'+ assets_id +' a.delete_assets').attr('data-assets', res.file_name);
				$('.sortq_items_assets_container_'+ assets_id +' a.delete_assets').attr('data-assets-id', 'sortq_item_assets'+ assets_id);
				$('.sortq_items_assets_container_'+ assets_id +' a.delete_assets').attr('data-input-id', input_id);
				$('.sortq_items_assets_container_'+ assets_id +' a.delete_assets').attr('data-path', res.path);
				swal({text: res.msg, buttons: false, timer: 1000});
			}
			else if (res.success == false){
				$(input_id).val('');
				swal({text: res.msg, buttons: false, icon: "warning", timer: 1000});
			}
			$.LoadingOverlay("hide");
		},error: function() {
			$.LoadingOverlay("hide");
			swal("Error", 'Oops, something went wrong. Please try again later.?', "error");
		}
	});
});

//-----------Image-Upload-Swip-Template-----------------
$('body').on('change', '.uploadImgFileSwip', function() {
	$.LoadingOverlay("show");
	var cur 		= $(this);
	var input_id	= $(this).data('id');
	var assets_id	= $(this).data('assets');
	var file_data	= $(this).prop('files')[0];
	var form_data	= new FormData();
	form_data.append('file', file_data);
	form_data.append('qImgfiles', '1');
	$.ajax({
		url: "includes/upload.php",
		type: "POST",
		data: form_data,
		contentType: false,
		cache: false,
		processData: false,
		success: function(resdata) {
			var res = $.parseJSON(resdata);
			if (res.success == true) {
				$(input_id).val(res.file_name);
				cur.closest('div').addClass("disabled");
				$('.swipq_item_assets'+ assets_id).addClass("disabled").attr('disabled', true);
				$('.swipq_items_assets_container_'+ assets_id).show();
				$('.swipq_items_assets_data_'+ assets_id + ' a.view_assets').text(res.file_name);
				$('.swipq_items_assets_data_'+ assets_id + ' a.view_assets').attr('data-src', assets_path + res.file_name);
				$('.swipq_items_assets_container_'+ assets_id +' a.delete_assets').attr('data-assets', res.file_name);
				$('.swipq_items_assets_container_'+ assets_id +' a.delete_assets').attr('data-assets-id', 'swipq_item_assets'+ assets_id);
				$('.swipq_items_assets_container_'+ assets_id +' a.delete_assets').attr('data-input-id', input_id);
				$('.swipq_items_assets_container_'+ assets_id +' a.delete_assets').attr('data-path', res.path);
				swal({text: res.msg, buttons: false, timer: 1000});
			}
			else if (res.success == false){
				$(input_id).val('');
				swal({text: res.msg, buttons: false, icon: "warning", timer: 1000});
			}
			$.LoadingOverlay("hide");
		},error: function() {
			$.LoadingOverlay("hide");
			swal("Error", 'Oops, something went wrong. Please try again later.?', "error");
		}
	});
});

//-----------Video-Upload-Swip-Template-----------------
$('body').on('change', '.uploadVideoFileSwip', function() {
	$.LoadingOverlay("show");
	var cur 		= $(this);
	var input_id	= $(this).data('id');
	var assets_id	= $(this).data('assets');
	var file_data	= $(this).prop('files')[0];
	var form_data	= new FormData();
	form_data.append('file', file_data);
	form_data.append('qVideofiles', '1');
	$.ajax({
		url: "includes/upload.php",
		type: "POST",
		data: form_data,
		contentType: false,
		cache: false,
		processData: false,
		success: function(resdata) {
			var res = $.parseJSON(resdata);
			if (res.success == true) {
				$(input_id).val(res.file_name);
				cur.closest('div').addClass("disabled");
				$('.swipq_item_assets'+ assets_id).addClass("disabled").attr('disabled', true);
				$('.swipq_items_assets_container_'+ assets_id).show();
				$('.swipq_items_assets_data_'+ assets_id).text(res.file_name);
				$('.swipq_items_assets_container_'+ assets_id +' a').attr('data-assets', res.file_name);
				$('.swipq_items_assets_container_'+ assets_id +' a').attr('data-assets-id', 'swipq_item_assets'+ assets_id);
				$('.swipq_items_assets_container_'+ assets_id +' a').attr('data-input-id', input_id);
				$('.swipq_items_assets_container_'+ assets_id +' a').attr('data-path', res.path);
				swal({text: res.msg, buttons: false, timer: 1000});
			}
			else if (res.success == false){
				$(input_id).val('');
				swal({text: res.msg, buttons: false, icon: "warning", timer: 1000});
			}
			$.LoadingOverlay("hide");
		},error: function() {
			$.LoadingOverlay("hide");
			swal("Error", 'Oops, something went wrong. Please try again later.?', "error");
		}
	});
});

//-----------Image-Upload-Drag-Drop-Template-------------

	//--------------DROP-TARGET-----------------
$('body').on('change', '.uploadImgFileDDq', function() {
	$.LoadingOverlay("show");
	var cur 		= $(this);
	var input_id	= $(this).data('id');
	var assets_id	= $(this).data('assets');
	var file_data	= $(this).prop('files')[0];
	var form_data	= new FormData();
	form_data.append('file', file_data);
	form_data.append('qImgfiles', '1');
	$.ajax({
		url: "includes/upload.php",
		type: "POST",
		data: form_data,
		contentType: false,
		cache: false,
		processData: false,
		success: function(resdata) {
			var res = $.parseJSON(resdata);
			if (res.success == true) {
				$(input_id).val(res.file_name);
				cur.closest('div').addClass("disabled");
				$('.ddq_item_assets'+ assets_id).addClass("disabled").attr('disabled', true);
				$('.ddq_items_assets_container_'+ assets_id).show();
				$('.ddq_items_assets_data_'+ assets_id + ' a.view_assets').text(res.file_name);
				$('.ddq_items_assets_data_'+ assets_id + ' a.view_assets').attr('data-src', assets_path + res.file_name);
				$('.ddq_items_assets_container_'+ assets_id +' a.delete_assets').attr('data-assets', res.file_name);
				$('.ddq_items_assets_container_'+ assets_id +' a.delete_assets').attr('data-assets-id', 'ddq_item_assets'+ assets_id);
				$('.ddq_items_assets_container_'+ assets_id +' a.delete_assets').attr('data-input-id', input_id);
				$('.ddq_items_assets_container_'+ assets_id +' a.delete_assets').attr('data-path', res.path);
				swal({text: res.msg, buttons: false, timer: 1000});
			}
			else if (res.success == false){
				$(input_id).val('');
				swal({text: res.msg, buttons: false, icon: "warning", timer: 1000});
			}
			$.LoadingOverlay("hide");
		},error: function() {
			$.LoadingOverlay("hide");
			swal("Error", 'Oops, something went wrong. Please try again later.?', "error");
		}
	});
});

	//-------------- DRAG-ITEM-----------------
$('body').on('change', '.uploadImgFileDragDDq', function() {
	$.LoadingOverlay("show");
	var cur 		= $(this);
	var input_id	= $(this).data('id');
	var assets_id	= $(this).data('assets');
	var file_data	= $(this).prop('files')[0];
	var form_data	= new FormData();
	form_data.append('file', file_data);
	form_data.append('qImgfiles', '1');
	$.ajax({
		url: "includes/upload.php",
		type: "POST",
		data: form_data,
		contentType: false,
		cache: false,
		processData: false,
		success: function(resdata) {
			var res = $.parseJSON(resdata);
			if (res.success == true) {
				$(input_id).val(res.file_name);
				cur.closest('div').addClass("disabled");
				$('.ddq_drag_item_assets'+ assets_id).addClass("disabled").attr('disabled', true);
				$('.ddq_drag_items_assets_container_'+ assets_id).show();
				$('.ddq_drag_items_assets_data_'+ assets_id + ' a.view_assets').text(res.file_name);
				$('.ddq_drag_items_assets_data_'+ assets_id + ' a.view_assets').attr('data-src', assets_path + res.file_name);
				$('.ddq_drag_items_assets_container_'+ assets_id +' a.delete_assets').attr('data-assets', res.file_name);
				$('.ddq_drag_items_assets_container_'+ assets_id +' a.delete_assets').attr('data-assets-id', 'ddq_drag_item_assets'+ assets_id);
				$('.ddq_drag_items_assets_container_'+ assets_id +' a.delete_assets').attr('data-input-id', input_id);
				$('.ddq_drag_items_assets_container_'+ assets_id +' a.delete_assets').attr('data-path', res.path);
				swal({text: res.msg, buttons: false, timer: 1000});
			}
			else if (res.success == false){
				$(input_id).val('');
				swal({text: res.msg, buttons: false, icon: "warning", timer: 1000});
			}
			$.LoadingOverlay("hide");
		},error: function() {
			$.LoadingOverlay("hide");
			swal("Error", 'Oops, something went wrong. Please try again later.?', "error");
		}
	});
});

	//--------------DROP-TARGET-2----------------
$('body').on('change', '.uploadImgFileDDq2', function() {
	$.LoadingOverlay("show");
	var cur 		= $(this);
	var input_id	= $(this).data('id');
	var assets_id	= $(this).data('assets');
	var file_data	= $(this).prop('files')[0];
	var form_data	= new FormData();
	form_data.append('file', file_data);
	form_data.append('qImgfiles', '1');
	$.ajax({
		url: "includes/upload.php",
		type: "POST",
		data: form_data,
		contentType: false,
		cache: false,
		processData: false,
		success: function(resdata) {
			var res = $.parseJSON(resdata);
			if (res.success == true) {
				$(input_id).val(res.file_name);
				cur.closest('div').addClass("disabled");
				$('.ddq2_item_assets'+ assets_id).addClass("disabled").attr('disabled', true);
				$('.ddq2_items_assets_container_'+ assets_id).show();
				$('.ddq2_items_assets_data_'+ assets_id + ' a.view_assets').text(res.file_name);
				$('.ddq2_items_assets_data_'+ assets_id + ' a.view_assets').attr('data-src', assets_path + res.file_name);
				$('.ddq2_items_assets_container_'+ assets_id +' a.delete_assets').attr('data-assets', res.file_name);
				$('.ddq2_items_assets_container_'+ assets_id +' a.delete_assets').attr('data-assets-id', 'ddq2_item_assets'+ assets_id);
				$('.ddq2_items_assets_container_'+ assets_id +' a.delete_assets').attr('data-input-id', input_id);
				$('.ddq2_items_assets_container_'+ assets_id +' a.delete_assets').attr('data-path', res.path);
				swal({text: res.msg, buttons: false, timer: 1000});
			}
			else if (res.success == false){
				$(input_id).val('');
				swal({text: res.msg, buttons: false, icon: "warning", timer: 1000});
			}
			$.LoadingOverlay("hide");
		},error: function() {
			$.LoadingOverlay("hide");
			swal("Error", 'Oops, something went wrong. Please try again later.?', "error");
		}
	});
});

	//-------------- DRAG-ITEM-2----------------
$('body').on('change', '.uploadImgFileDragDDq2', function() {
	$.LoadingOverlay("show");
	var cur 		= $(this);
	var input_id	= $(this).data('id');
	var assets_id	= $(this).data('assets');
	var file_data	= $(this).prop('files')[0];
	var form_data	= new FormData();
	form_data.append('file', file_data);
	form_data.append('qImgfiles', '1');
	$.ajax({
		url: "includes/upload.php",
		type: "POST",
		data: form_data,
		contentType: false,
		cache: false,
		processData: false,
		success: function(resdata) {
			var res = $.parseJSON(resdata);
			if (res.success == true) {
				$(input_id).val(res.file_name);
				cur.closest('div').addClass("disabled");
				$('.ddq2_drag_item_assets'+ assets_id).addClass("disabled").attr('disabled', true);
				$('.ddq2_drag_items_assets_container_'+ assets_id).show();
				$('.ddq2_drag_items_assets_data_'+ assets_id + ' a.view_assets').text(res.file_name);
				$('.ddq2_drag_items_assets_data_'+ assets_id + ' a.view_assets').attr('data-src', assets_path + res.file_name);
				$('.ddq2_drag_items_assets_container_'+ assets_id +' a.delete_assets').attr('data-assets', res.file_name);
				$('.ddq2_drag_items_assets_container_'+ assets_id +' a.delete_assets').attr('data-assets-id', 'ddq2_drag_item_assets'+ assets_id);
				$('.ddq2_drag_items_assets_container_'+ assets_id +' a.delete_assets').attr('data-input-id', input_id);
				$('.ddq2_drag_items_assets_container_'+ assets_id +' a.delete_assets').attr('data-path', res.path);
				swal({text: res.msg, buttons: false, timer: 1000});
			}
			else if (res.success == false){
				$(input_id).val('');
				swal({text: res.msg, buttons: false, icon: "warning", timer: 1000});
			}
			$.LoadingOverlay("hide");
		},error: function() {
			$.LoadingOverlay("hide");
			swal("Error", 'Oops, something went wrong. Please try again later.?', "error");
		}
	});
});

//-----------Record Audio-All-Template------------------
$('body').on('click', '.rec-audio', function() {
	var input_id  = $(this).attr('data-input-id');
	var assets_id = $(this).attr('data-assets');
	if (input_id != '') {
		$.LoadingOverlay("show");
		var $modal = $('#load_popup_modal_show');
		$modal.load('rec-audio-modal.php', {'data-id': input_id, 'assets-id': assets_id}, function(res) {
			if (res != '') {
				$.LoadingOverlay("hide");
				$modal.modal('show', {backdrop: 'static', keyboard: false});
			}
			else {
				$.LoadingOverlay("hide");
				swal({text: 'Oops, something went wrong. Please try again later', buttons: false, icon: "error", timer: 1000});
			}
		});
	}
});

//-----------Record Video-All-Template------------------
$('body').on('click', '.rec-video', function() {
	var input_id  = $(this).attr('data-input-id');
	var assets_id = $(this).attr('data-assets');
	if (input_id != '') {
		$.LoadingOverlay("show");
		var $modal = $('#load_popup_modal_show');
		$modal.load('rec-video-modal.php', {'data-id': input_id, 'assets-id': assets_id}, function(res) {
			if (res != '') {
				$.LoadingOverlay("hide");
				$modal.modal('show', {backdrop: 'static', keyboard: false});
			}
			else {
				$.LoadingOverlay("hide");
				swal({text: 'Oops, something went wrong. Please try again later', buttons: false, icon: "error", timer: 1000});
			}
		});
	}
});

//-----------Record Screen-All-Template-----------------
$('body').on('click', '.rec-screen', function() {
	var input_id  = $(this).attr('data-input-id');
	var assets_id = $(this).attr('data-assets');
	if (input_id != '') {
		$.LoadingOverlay("show");
		var $modal = $('#load_popup_modal_show');
		$modal.load('rec-screen-modal.php', {'data-id': input_id, 'assets-id': assets_id}, function(res) {
			if (res != '') {
				$.LoadingOverlay("hide");
				$modal.modal('show', {backdrop: 'static', keyboard: false});
			}
			else {
				$.LoadingOverlay("hide");
				swal({text: 'Oops, something went wrong. Please try again later', buttons: false, icon: "error", timer: 1000});
			}
		});
	}
});

//--------------Delete-Assets-All-Templates------------
$('body').on('click', '.delete_assets', function() {
	var cur   		= $(this);
	var file_name	= cur.attr('data-assets');
	var assetsId	= cur.attr('data-assets-id');
	var inputId		= cur.attr('data-input-id');
	var path		= cur.attr('data-path');
	var divId		= cur.closest('div').attr('class');
	var dataString  = 'delete_assets='+ true +'&assets_path='+ path +'&file='+ file_name;
	if (file_name){
		swal({
			title: "Are you sure?",
			text: "Delete this Assets.",
			icon: "warning",
			buttons: [true, 'Delete'],
			dangerMode: true, }).then((willDelete) => { if (willDelete) {
				$.LoadingOverlay("show");
				$.ajax({
					url: "includes/delete-file.php",
					type: "POST",
					data: dataString,
					cache: false,
					success: function(resdata){
						var res = $.parseJSON(resdata);
						if (res.success == false) {
							$.LoadingOverlay("hide");
							swal({text: res.msg, buttons: false, icon: "error", timer: 2000});
						}
						else if (res.success == true) {
							$(inputId).val('');
							$('.'+ assetsId).removeClass("disabled").removeAttr('disabled', true);
							$('.'+ divId).hide();
							$.LoadingOverlay("hide");
							swal({text: res.msg, buttons: [false, 'OK'], closeOnClickOutside: false, closeOnEsc: false});
						}
					},error: function() {
						swal({text: 'Oops. something went wrong please try again.', buttons: false, icon: "error", timer: 1000});
					}
				});
			} else { swal({text: 'Your file is safe', buttons: false, timer: 1000}); }
		});
	}
});

var x = <?php echo ( ! empty($i)) ? $i : 1; ?>;
var maxField = 6;
$(".plus_comp").click(function() {
	if (x < maxField) {
		x++;
		$(".plus_comp_option").append('<div class="add_comp_option linaer">\
		<div class="form-group"><input type="text" class="form-control control1" placeholder="Competency Name" name="competency_name_'+x+'" autocomplete="off" required="required"></div>\
		<div class="form-group labelbox"><label>Competency Score</label><input type="text" class="form-control question-box" name="competency_score_'+x+'" onkeypress="return isNumberKey(event);" autocomplete="off" required="required"></div>\
		<div class="form-group labelbox"><label>Weightage</label><input type="text" class="form-control question-box" name="weightage_'+x+'" onkeypress="return isNumberKey(event);" autocomplete="off" required="required"></div><div class="remove minusComp"><img src="img/icons/minus.png" title="Remove"></div></div>');
		var getMax = $('.add_comp_option').length;
		if (getMax >= maxField) {
			$('.plus_comp').addClass('disabled');
		}
		else { $('.plus_comp').removeClass('disabled'); }
	}
	else {
		$('.plus_comp').addClass('disabled');
	}
});

$('.plus_comp_option').on('click', '.remove', function(e) {
	e.preventDefault();
	$(this).parent().remove();
	x--;
	$('.plus_comp').removeClass('disabled');
});

$('.suffleCheck').click(function() {
	if ($(".suffleCheck").is(":checked") == true) { 
		$('.AnsHeading.Mchoice').text('CORRECT CHOICE'); 
		$('.tooltiptop .tooltiptext').text('Shuffle ON'); 
		$('.tooltiptop .tooltiptext').css('display', 'block'); 
		$('.matchingchice').css('display', 'none');
		$('.matchingchicesuffle').css('display', 'block');
		
	} else { 
		$('.AnsHeading.Mchoice').text('MATCH'); 
		$('.tooltiptop .tooltiptext').text('Shuffle OFF'); 
		$('.tooltiptop .tooltiptext').delay(2000).fadeOut(300);
		$('.matchingchicesuffle').css('display', 'none')
		$('.matchingchice').css('display', 'block') 
	} 
});

$('.unsuffleCheck').click(function() {
	if ($(".unsuffleCheck").is(":checked") == true) { 		
		$('.AnsHeading.Mchoice').text('MATCH'); 
		$('.tooltiptop .tooltiptext').text('Shuffle OFF'); 
		$('.tooltiptop .tooltiptext').delay(2000).fadeOut(300);
		$('.matchingchicesuffle').css('display', 'none')
		$('.matchingchice').css('display', 'block') 
		
	} else { 
		$('.AnsHeading.Mchoice').text('CORRECT CHOICE'); 
		$('.tooltiptop .tooltiptext').text('Shuffle ON'); 
		$('.tooltiptop .tooltiptext').css('display', 'block'); 
		$('.matchingchice').css('display', 'none');
		$('.matchingchicesuffle').css('display', 'block');
	} 
});

$(".suffleCheck").click(function(){
	$('.AnsHeading.Mchoice').addClass('ANSCOrrectcho');
});

$(".closerightmenuA").click(function(e) {
	e.preventDefault();
	$("#menu-toggle").removeClass("changebtn");
	$("#wrapper").removeClass("toggled");
	$(".match-Q.tab-pane.fade").removeClass("tabpadding150");
	$(".main_sim_scroll").removeClass("TogglewidthQ100");		
	$(".widthQ100").removeClass("TogglewidthQ100");		
	$(".modal.Ques-pop-style.in ").removeClass("TogglewidthQ100");		
});

$("#menu-toggle").click(function(e) {
	e.preventDefault();
	$("#menu-toggle").toggleClass("changebtn");
	$("#wrapper").toggleClass("toggled");
	$(".match-Q.tab-pane.fade").toggleClass("tabpadding150");
	$(".main_sim_scroll").toggleClass("TogglewidthQ100");		
	$(".widthQ100").toggleClass("TogglewidthQ100");		
	$(".modal.Ques-pop-style.in ").toggleClass("TogglewidthQ100");		
});

$(".closeleftmenuA").click(function(e) {
	e.preventDefault();
	$("#list-toggle").addClass("changebtnleft");
	$("#wrapper").addClass("toggledlist");
	$(".match-Q.tab-pane.fade").addClass("tabpadding150");
	$(".Q-left").addClass("left150");
	$(".widthQ100").addClass("TogglelistwidthQ100");
	$(".main_sim_scroll").addClass("TogglelistwidthQ100");	
	$(".modal.Ques-pop-style.in ").addClass("TogglelistwidthQ100");
    $(".list-icon").addClass("Labsolute");			
});

$("#list-toggle").click(function(e) {
	e.preventDefault();
	$("#list-toggle").toggleClass("changebtnleft");
	$("#wrapper").toggleClass("toggledlist");
	$(".match-Q.tab-pane.fade").toggleClass("tabpadding150");
	$(".Q-left").toggleClass("left150");
	$(".widthQ100").toggleClass("TogglelistwidthQ100");	
	$(".main_sim_scroll").toggleClass("TogglelistwidthQ100");		
	$(".modal.Ques-pop-style.in ").toggleClass("TogglelistwidthQ100");
    $(".list-icon").toggleClass("Labsolute");			
});

$(".matchingQ").click(function(e) {
	e.preventDefault();
	$("#menu-toggle").removeClass("changebtn");
	$("#wrapper").removeClass("toggled");
	$(".match-Q.tab-pane.fade").removeClass("tabpadding150");
	$(".main_sim_scroll").removeClass("TogglewidthQ100");		
	$(".widthQ100").removeClass("TogglewidthQ100");		
	$(".modal.Ques-pop-style.in ").removeClass("TogglewidthQ100");		
});

$(".SequanceQ").click(function(e) {
	e.preventDefault();
	$("#menu-toggle").removeClass("changebtn");
	$("#wrapper").removeClass("toggled");
	$(".match-Q.tab-pane.fade").removeClass("tabpadding150");
	$(".main_sim_scroll").removeClass("TogglewidthQ100");		
	$(".widthQ100").removeClass("TogglewidthQ100");		
	$(".modal.Ques-pop-style.in ").removeClass("TogglewidthQ100");		
});

$(".DregDropQ").click(function(e) {
	e.preventDefault();
	$("#menu-toggle").removeClass("changebtn");
	$("#wrapper").removeClass("toggled");
	$(".match-Q.tab-pane.fade").removeClass("tabpadding150");
	$(".main_sim_scroll").removeClass("TogglewidthQ100");		
	$(".widthQ100").removeClass("TogglewidthQ100");		
	$(".modal.Ques-pop-style.in ").removeClass("TogglewidthQ100");		
});

$(".VideoQ").click(function(e) {
	e.preventDefault();
	$("#menu-toggle").removeClass("changebtn");
	$("#wrapper").removeClass("toggled");
	$(".match-Q.tab-pane.fade").removeClass("tabpadding150");
	$(".main_sim_scroll").removeClass("TogglewidthQ100");		
	$(".widthQ100").removeClass("TogglewidthQ100");		
	$(".modal.Ques-pop-style.in ").removeClass("TogglewidthQ100");		
});

$(".MCQQ").click(function(e) {
	e.preventDefault();
	$("#menu-toggle").removeClass("changebtn");
	$("#wrapper").removeClass("toggled");
	$(".match-Q.tab-pane.fade").removeClass("tabpadding150");
	$(".main_sim_scroll").removeClass("TogglewidthQ100");		
	$(".widthQ100").removeClass("TogglewidthQ100");		
	$(".modal.Ques-pop-style.in").removeClass("TogglewidthQ100");		
});

$(".MMCQQ").click(function(e) {
	e.preventDefault();
	$("#menu-toggle").removeClass("changebtn");
	$("#wrapper").removeClass("toggled");
	$(".match-Q.tab-pane.fade").removeClass("tabpadding150");
	$(".main_sim_scroll").removeClass("TogglewidthQ100");		
	$(".widthQ100").removeClass("TogglewidthQ100");		
	$(".modal.Ques-pop-style.in").removeClass("TogglewidthQ100");		
});

$(".SwapingQ").click(function(e) {
	e.preventDefault();
	$("#menu-toggle").removeClass("changebtn");
	$("#wrapper").removeClass("toggled");
	$(".match-Q.tab-pane.fade").removeClass("tabpadding150");
	$(".main_sim_scroll").removeClass("TogglewidthQ100");		
	$(".widthQ100").removeClass("TogglewidthQ100");		
	$(".modal.Ques-pop-style.in").removeClass("TogglewidthQ100");		
});

$(document).ready(function(){
	//----------------MTF----------------//
	$(".AddAnsICON").click(function(){
		$(".MatchOption-box1:last").after('<div class="MatchOption-box1">\
			<div class="col-sm-6">\
				<div class="matchbox">\
					<div class="form-group"><input type="text" name="mtf_choice[]" class="form-control inputtextWrap shuffle_off mtf" placeholder="Click here to enter choice" required="required"></div>\
					<div class="form-group QusScore"><input type="text" name="mtf_choice_order[]" class="form-control shuffle_off mtf" onkeypress="return isNumberKey(event);" required="required"></div>\
				</div>\
			</div>\
			<div class="col-sm-6">\
				<div class="matchbox">\
					<div class="form-group QusScore"><input type="text" name="mtf_match_order[]" class="form-control shuffle_off mtf" onkeypress="return isNumberKey(event);" required="required"></div>\
					<div class="form-group"><input type="text" name="mtf_match[]" class="form-control inputtextWrap shuffle_off mtf" placeholder="Click here to enter text" required="required"></div>\
				</div>\
			</div>\
			<div class="AddAns-Option RemoveAnsICON"><img class="img-fluid" src="img/list/delete_field.svg"></div></div>');
	});
	
	$('.ApendOption-box').on('click', '.AddAns-Option.RemoveAnsICON', function(e) {
		e.preventDefault();
		$(this).parent().remove();
	});
	
	//-----MTF-Shuffle-ON------------//
	$(".AddAnsICON1").click(function(){
		$(".MatchOption-box2:last").after('<div class="MatchOption-box2 MSuffle">\
		<div class="col-sm-6">\
			<div class="matchbox">\
				<div class="form-group">\
					<input type="text" name="mtf_choice2[]" class="form-control inputtextWrap shuffle_on mtf" placeholder="Click here to enter choice" required="required" />\
				</div>\
			</div>\
		</div>\
		<div class="col-sm-6">\
			<div class="matchbox">\
				<div class="form-group">\
					<input type="text" name="mtf_match2[]" class="form-control inputtextWrap shuffle_on mtf" placeholder="Click here to enter text" required="required" />\
				</div>\
			</div>\
		</div>\
		<div class="AddAns-Option RemoveAnsICON1"><img class="img-fluid" src="img/list/delete_field.svg"></div></div>');
	});
	
	$('.ApendOption-box').on('click', '.AddAns-Option.RemoveAnsICON1', function(e){
		e.preventDefault();
		$(this).parent().remove();
	});
	
	//------DNDQuestion1-----------//
	var dd2i = $('.DNDOption-box2').length;
	$(".DNDAddAnsICON1").click(function(){
		dd2i++;
		$(".DNDOption-box2:last").after('<div class="DNDOption-box2">\
			<div class="col-sm-12">\
				<div class="Radio-box">\
					<label class="radiostyle">\
						<input type="radio" name="ddq_true_option" value="'+dd2i+'" />\
						<span class="radiomark"></span>\
				   </label>\
				   <div class="col-sm-12">\
						<div class="choicebox">\
							<div class="form-group">\
								<ul class="QueBoxIcon">\
									<li>\
										<div class="tooltip uploadicon ddq2_drag_item_assets'+dd2i+'">\
											<label for="file-input-ddq2-drag-item-addImg'+dd2i+'">\
												<img class="img-fluid" src="img/list/image.svg" />\
												<span class="tooltiptext">Add Image</span>\
											</label>\
											<input id="file-input-ddq2-drag-item-addImg'+dd2i+'" data-id="#ddq2_drag_item_img'+dd2i+'" data-assets="'+dd2i+'" class="uploadImgFileDragDDq2 ddq2_drag_item_assets'+dd2i+'" type="file" name="ddq2_drag_Img'+dd2i+'" />\
											<input type="hidden" name="ddq2_drag_item_img[]" id="ddq2_drag_item_img'+dd2i+'" />\
										</div>\
									</li>\
								</ul>\
								<input type="text" name="ddq2_drag[]" class="form-control inputtextWrap" placeholder="Drag text goes here" />\
								<div class="ddq2_drag_items_assets_container_'+dd2i+'" id="ddq2_drag_items_assets" style="display:none;">\
									<div class="assets_data ddq2_drag_items_assets_data_'+dd2i+'"><a class="view_assets" data-src="" href="javascript:;"></a></div>\
									<a href="javascript:void(0);" data-assets="" data-assets-id="ddq2_drag_item_assets'+dd2i+'" data-input-id="#ddq2_drag_item_img'+dd2i+'" data-path="<?php echo $uploadpath ?>" class="delete_assets" title="Delete"><i class="fa fa-times" aria-hidden="true"></i></a>\
								</div>\
							</div>\
						</div>\
					</div>\
				   <div class="AddAns-Option RemoveAnsICON2"><img class="img-fluid" src="img/list/delete_field.svg"></div>\
				</div>\
			</div>\
		</div>');
	});
	
	$('.ApendOption-box').on('click', '.AddAns-Option.RemoveAnsICON2', function(){
		$(this).parent().parent().parent().remove();
		dd2i--;
	});
	
	//-------DNDQuestion---------------//
	var ddi = $('.DNDOption-box1').length;
	$(".DNDAddAnsICON").click(function(){
		ddi++;
		$(".DNDOption-box1:last").after('<div class="DNDOption-box1">\
			<div class="col-sm-6">\
				<div class="matchbox">\
					<span class="QuesNo">'+ddi+'</span>\
					<div class="form-group">\
						<ul class="QueBoxIcon">\
							<li>\
								<div class="tooltip uploadicon ddq_item_assets'+ddi+'">\
									<label for="file-input-ddq-item-addImg'+ddi+'">\
										<img class="img-fluid" src="img/list/image.svg" />\
										<span class="tooltiptext">Add Image</span>\
									</label>\
									<input id="file-input-ddq-item-addImg'+ddi+'" data-id="#ddq_item_img'+ddi+'" data-assets="'+ddi+'" class="uploadImgFileDDq ddq_item_assets'+ddi+'" type="file" name="ddq_item_Img'+ddi+'" />\
									<input type="hidden" name="ddq_item_img[]" id="ddq_item_img'+ddi+'" />\
								</div>\
							</li>\
						</ul>\
						<input type="text" name="ddq_drop[]" class="form-control inputtextWrap ddq" placeholder="Drop Target text goes here" />\
						<div class="ddq_items_assets_container_'+ddi+'" id="ddq_items_assets" style="display:none;">\
							<div class="assets_data ddq_items_assets_data_'+ddi+'"><a class="view_assets" data-src="" href="javascript:;"></a></div>\
							<a href="javascript:void(0);" data-assets="" data-assets-id="ddq_item_assets'+ddi+'" data-input-id="#ddq_item_img'+ddi+'" data-path="<?php echo $uploadpath ?>" class="delete_assets" title="Delete"><i class="fa fa-times" aria-hidden="true"></i></a>\
						</div>\
					</div>\
				</div>\
			</div>\
			<div class="col-sm-6">\
				<div class="matchbox">\
					<div class="form-group">\
						<ul class="QueBoxIcon">\
							<li>\
								<div class="tooltip uploadicon ddq_drag_item_assets'+ddi+'">\
									<label for="file-input-ddq-drag-item-addImg'+ddi+'">\
										<img class="img-fluid" src="img/list/image.svg" />\
										<span class="tooltiptext">Add Image</span>\
									</label>\
									<input id="file-input-ddq-drag-item-addImg'+ddi+'" data-id="#ddq_drag_item_img'+ddi+'" data-assets="'+ddi+'" class="uploadImgFileDragDDq ddq_drag_item_assets'+ddi+'" type="file" name="ddq_drag_Img'+ddi+'" />\
									<input type="hidden" name="ddq_drag_item_img[]" id="ddq_drag_item_img'+ddi+'" />\
								</div>\
							</li>\
						</ul>\
						<input type="text" name="ddq_drag[]" class="form-control inputtextWrap" placeholder="Drag text goes here" />\
						<div class="ddq_drag_items_assets_container_'+ddi+'" id="ddq_drag_items_assets" style="display:none;">\
							<div class="assets_data ddq_drag_items_assets_data_'+ddi+'"><a class="view_assets" data-src="" href="javascript:;"></a></div>\
							<a href="javascript:void(0);" data-assets="" data-assets-id="ddq_drag_item_assets'+ddi+'" data-input-id="#ddq_drag_item_img'+ddi+'" data-path="<?php echo $uploadpath ?>" class="delete_assets" title="Delete"><i class="fa fa-times" aria-hidden="true"></i></a>\
						</div>\
					</div>\
				</div>\
			</div><div class="AddAns-Option RemoveAnsICON"><img class="img-fluid" src="img/list/delete_field.svg"></div></div>');
	});
	
	$('.ApendOption-box').on('click', '.AddAns-Option.RemoveAnsICON', function(){
		$(this).parent().remove();
		ddi--;
	});
	
	//---SequanceQuestion-------//	
	$(".SeqAdd").click(function(){
		$(".Option-box:last").after('<div class="Option-box">\
		<div class="form-group formwidth">\
			<input type="text" name="sq_ans[]" class="form-control inputtextWrap sq" placeholder="Option text goes here" />\
		</div>\
		<div class="AddAns-Option"><img class="img-fluid SeqRemove" src="img/list/delete_field.svg"></div></div>');
	});
	
	$('.SeqApendOption').on('click', '.SeqRemove', function() {
		$(this).parent().parent().remove();
	});
	
	//----------Shorting-Item----------------//
	var sortingI = $('.ApendShortOption-box').length;
	$(".ShortAddICON").click(function(){
		dragitem = sortingI;
		sortingI++;
		$(".ApendShortOption-box:last").after('<div class="ApendShortOption-box">\
		<div class="shortbox2">\
			<span class="QuesNo">'+sortingI+'</span>\
			<div class="form-group">\
				<input type="text" name="sortq_sorting_items[]" id="sortq_sorting_items" class="form-control inputtextWrap sortq" placeholder="Sorting text goes here" />\
				<ul class="QueBoxIcon">\
					<li>\
						<div class="tooltip uploadicon sortq_item_assets'+sortingI+'">\
							<label for="file-input-sortq-item-addImg'+sortingI+'">\
								<img class="img-fluid" src="img/list/image.svg" />\
								<span class="tooltiptext">Add Image</span>\
							</label>\
							<input id="file-input-sortq-item-addImg'+sortingI+'" data-id="#sortq_item_img'+sortingI+'" data-assets="'+sortingI+'" class="uploadImgFileSort sortq_item_assets'+sortingI+'" type="file" name="sortq_item_Img'+sortingI+'" />\
							<input type="hidden" name="sortq_item_img[]" id="sortq_item_img'+sortingI+'" />\
						</div>\
					</li>\
			   </ul>\
			   <div class="sortq_items_assets_container_'+sortingI+'" id="sortq_items_assets" style="display:none;">\
			   		<div class="assets_data sortq_items_assets_data_'+sortingI+'"><a class="view_assets" data-src="" href="javascript:;"></a></div>\
					<a href="javascript:void(0);" data-assets="" data-assets-id="sortq_item_assets'+sortingI+'" data-input-id="#sortq_item_img'+sortingI+'" data-path="<?php echo $uploadpath ?>" class="delete_assets" title="Delete"><i class="fa fa-times" aria-hidden="true"></i></a>\
				</div>\
			</div>\
			<div class="AddAns-Option"><img class="img-fluid ShortdelICON" src="img/list/delete_field.svg" /></div>\
		</div>\
		<div class="SA-choicebox ShortOptions_'+sortingI+'">\
			<h3 class="AnsHeading dragH">Add Correct Drag Option</h3>\
			<div class="choicebox ShortAC">\
				<div class="form-group">\
					<input type="text" name="sortq_drag_items['+dragitem+'][option][]" id="sortq_drag_items" class="form-control inputtextWrap sortq" placeholder="Drag text goes here" />\
				</div>\
				<div class="AddAns-Option"><img class="img-fluid ShortDragAddICON" data-sortq-option="'+sortingI+'" data-sortq-suboption="'+dragitem+'" src="img/list/add_field.svg"></div>\
			</div>\
		</div>\
	 </div>');
	});
	
	$('.ApendShortOption-banner').on('click', '.ShortdelICON', function(){
		$(this).parent().parent().parent().remove();
		sortingI--;
	});
	
	$('body').on('click', '.ShortDragAddICON', function() {
		var option_id = $(this).data('sortq-option');
		var sub_option_id = $(this).data('sortq-suboption');
		$('.ShortOptions_'+ option_id).append('<div class="choicebox ShortAC ShortAC_'+option_id+'">\
			<div class="form-group">\
				<input type="text" name="sortq_drag_items['+sub_option_id+'][option][]" id="sortq_drag_items" class="form-control inputtextWrap sortq" placeholder="Drag text goes here" />\
			</div>\
			<div class="AddAns-Option">\
				<img class="img-fluid ShortDragdelICON" src="img/list/delete_field.svg" />\
			</div>\
		</div>');
	});
	
	$('.ApendShortOption-banner').on('click', '.ShortDragdelICON', function(){
		$(this).parent().parent().remove();
	});
	
	//--------------MCQQuestion----------------//
	var mcqI = $('.MCQApendOption .Radio-box').length;
	$(".MCQAdd").click(function(){
		mcqI++;
		$(".Radio-box.MCQRadio:last").after('<div class="Radio-box MCQRadio">\
		<label class="radiostyle">\
			<input type="radio" name="mcq_true_option" value="'+mcqI+'" /><span class="radiomark"></span>\
		 </label>\
		<div class="col-sm-12 mcwidth">\
			<div class="choicebox">\
				<div class="form-group">\
				   <input type="text" name="mcq_option[]" id="mcq_option" class="form-control inputtextWrap mcq" placeholder="Option text goes here" />\
				</div>\
			</div>\
		</div>\
		<div class="AddAns-Option MCQRemove"><img class="img-fluid" src="img/list/delete_field.svg"></div></div>');
	});

	$('.MCQApendOption').on('click', '.AddAns-Option.MCQRemove', function(){
		$(this).parent().remove();
		mcqI--;
	});
	
	//--------------MCQQuestion----------------//
	var mmcqI = $('.MMCQApendOption .Check-box').length;
	$(".MMCQAdd").click(function(){
		mmcqI++;
		$(".Check-box.MMCQCheck:last").after('<div class="Check-box MMCQCheck">\
		<label class="checkstyle">\
			<input type="checkbox" name="mmcq_true_option[]" value="'+mmcqI+'" />\
			<span class="checkmark"></span>\
		</label>\
		<div class="col-sm-12 mcwidth">\
			<div class="choicebox">\
				<div class="form-group">\
					<input type="text" name="mmcq_option[]" id="mmcq_option" class="form-control inputtextWrap mmcq" placeholder="Option text goes here" />\
				</div>\
			</div>\
		</div>\
		<div class="AddAns-Option MCCQRemove"><img class="img-fluid" src="img/list/delete_field.svg"></div></div>');
	});

	$('.MMCQApendOption').on('click', '.MCCQRemove', function(){
		$(this).parent().remove();
		mmcqI--;
	});
	
	//-------------SWIPING ----------------//
	var swipI = $('.Swa-box').length;
	$(".SwaAdd").click(function(){
		swipI++;
		$(".Swa-box:last").after('<div class="Swa-box">\
		<div class="Radio-box">\
			<label class="radiostyle">\
				<input type="radio" name="swipq_true_option" value="'+swipI+'" /><span class="radiomark"></span></label>\
				<div class="col-sm-12 mcwidth">\
					<div class="choicebox">\
						<div class="form-group">\
							<input type="text" name="swipq_option[]" id="swipq_option" class="form-control QuesForm1 inputtextWrap swipq" placeholder="Click here to enter question" />\
							<ul class="QueBoxIcon">\
								<li>\
									<div class="tooltip uploadicon swipq_item_assets'+swipI+'">\
										<label for="file-input-swipq-item-addImg'+swipI+'">\
											<img class="img-fluid" src="img/list/image.svg">\
											<span class="tooltiptext">Add Image</span>\
										</label>\
										<input id="file-input-swipq-item-addImg'+swipI+'" data-id="#swipq_item_img'+swipI+'" data-assets="'+swipI+'" class="uploadImgFileSwip swipq_item_assets'+swipI+'" type="file" name="swipq_item_Img'+swipI+'"/>\
										<input type="hidden" name="swipq_item_img[]" id="swipq_item_img'+swipI+'"/>\
									</div>\
								</li>\
							</ul>\
							<div class="swipq_items_assets_container_'+swipI+'" id="swipq_items_assets" style="display:none">\
								<div class="assets_data swipq_items_assets_data_'+swipI+'"><a class="view_assets" data-src="" href="javascript:;"></a></div>\
								<a href="javascript:void(0);" data-assets="" data-assets-id="" data-input-id="" data-path="" class="delete_assets" title="Delete">\
								<i class="fa fa-times" aria-hidden="true"></i></a>\
							</div>\
						</div>\
					</div>\
				</div>\
				<div class="AddAns-Option"><img class="img-fluid SWARemove" src="img/list/delete_field.svg"></div>\
			</div></div>');
	});
	
	$('.SWApendOption').on('click', '.SWARemove', function(){
		$(this).parent().parent().remove();
		swipI--;
	});
	
	$('.Radio-box.Dragradio.tab1').click(function(){
		$('.Dragrtab1').css('display' , 'block');
		$('.Dragrtab2').css('display' , 'none');
	});

	$('.Radio-box.Dragradio.tab2').click(function(){
		$('.Dragrtab2').css('display' , 'block');
		$('.Dragrtab1').css('display' , 'none');
	});

	//--------------Delete-Options------------
	$('body').on('click', '.removeOption', function(e) {
		e.preventDefault();
		var ans_id	= $(this).attr('data-remove-answer-id');
		var curr	= $(this);
		if (ans_id) {
			var dataString = 'delete_answer='+ true +'&ans_id='+ ans_id;
			swal({
				title: "Are you sure?",
				text:  "Delete this Option",
				icon: "warning",
				buttons: [true, 'Delete'],
				dangerMode: true, }).then((willDelete) => { if (willDelete) {
					$.LoadingOverlay("show");
					$.ajax({
						url: "includes/process.php",
						type: "POST",
						data: dataString,
						cache: false,
						success: function(resdata) {
							var res = $.parseJSON(resdata);
							$.LoadingOverlay("hide");
							if (res.success == true) {
								swal(res.msg, { buttons: false, timer: 2000 });
								curr.parent().remove();
								x--;
							}
							else if (res.success == false) {
								swal(res.msg, { buttons: false, timer: 2000 });
							}
						},error: function() {
							$.LoadingOverlay("hide");
							swal("Error", 'Oops, something went wrong. Please try again later.?', "error");
						}
					});
				}
			});
		}
	});

	//--------------Delete-SORTING-ITEM------------
	$('body').on('click', '.removeSortingOption', function(e) {
		e.preventDefault();
		var ans_id	= $(this).attr('data-remove-answer-id');
		var curr	= $(this);
		if (ans_id) {
			var dataString = 'delete_sorting_answer='+ true +'&ans_id='+ ans_id;
			swal({
				title: "Are you sure?",
				text:  "Delete this Option",
				icon: "warning",
				buttons: [true, 'Delete'],
				dangerMode: true, }).then((willDelete) => { if (willDelete) {
					$.LoadingOverlay("show");
					$.ajax({
						url: "includes/process.php",
						type: "POST",
						data: dataString,
						cache: false,
						success: function(resdata) {
							var res = $.parseJSON(resdata);
							$.LoadingOverlay("hide");
							if (res.success == true) {
								swal(res.msg, { buttons: false, timer: 2000 });
								curr.parent().parent().remove();
							}
							else if (res.success == false) {
								swal(res.msg, { buttons: false, timer: 2000 });
							}
						},error: function() {
							$.LoadingOverlay("hide");
							swal("Error", 'Oops, something went wrong. Please try again later.?', "error");
						}
					});
				}
			});
		}
	});

	//----------Delete-SORTING-DRAG-OPTION------------
	$('body').on('click', '.removeSortingSubOption', function(e) {
		e.preventDefault();
		var ans_id	= $(this).attr('data-remove-answer-id');
		var curr	= $(this);
		if (ans_id) {
			var dataString = 'delete_sorting_sub_answer='+ true +'&ans_id='+ ans_id;
			swal({
				title: "Are you sure?",
				text:  "Delete this Option",
				icon: "warning",
				buttons: [true, 'Delete'],
				dangerMode: true, }).then((willDelete) => { if (willDelete) {
					$.LoadingOverlay("show");
					$.ajax({
						url: "includes/process.php",
						type: "POST",
						data: dataString,
						cache: false,
						success: function(resdata) {
							var res = $.parseJSON(resdata);
							$.LoadingOverlay("hide");
							if (res.success == true) {
								swal(res.msg, { buttons: false, timer: 2000 });
								curr.parent().parent().remove();
							}
							else if (res.success == false) {
								swal(res.msg, { buttons: false, timer: 2000 });
							}
						},error: function() {
							$.LoadingOverlay("hide");
							swal("Error", 'Oops, something went wrong. Please try again later.?', "error");
						}
					});
				}
			});
		}
	});
});

$('#upload').click(function() {
	$(this).attr('disabled', 'disabled').html('<i class="fa fa-refresh fa-spin fa-fw"></i><span class="sr-only">Loading...</span> Please wait....');
	var file_data = $('#file').prop('files')[0];
	var form_data = new FormData();
	form_data.append('file', file_data);
	form_data.append('scenario_media_file', true);
	var ftype   = file_data.type;
	var gettype = $('#scenario_media_file_type').val();
	var ext 	= ftype.split('/')[0];
	if (ext == gettype) {
		$.ajax({
			url: "includes/upload.php",
			type: "POST",
			data: form_data,
			contentType: false,
			cache: false,
			processData:false,
			success: function(resdata) {
				var res = $.parseJSON(resdata);
				if (res.success == true) {
					if (res.zip == true) {
						$('#ImgDelete').show();
						$('.delete_wb_file').attr('data-wb-file', res.file_name);
						$('#scenario_media_file').val(res.file_name);
						$('#upload').html('Upload').removeAttr('disabled', 'disabled');
					}
					else {
						var viewHtml = '<a href="<?php echo $uploadpath ?>'+res.file_name+'" target="_blank" title="View Scenario Media File"><i class="fa fa-eye" aria-hidden="true"></i></a>';
						$('#splashImg').show().html(viewHtml);
						$('#ImgDelete').show();
						$('.delete_scenario_media_file').attr('data-scenario-media-file', res.file_name);
						$('#scenario_media_file').val(res.file_name);
						$('#upload').html('Upload').removeAttr('disabled', 'disabled');
					}
					swal(res.msg, { buttons: false, timer: 1000 });
				}
				else if (res.success == false) {
					$('#scenario_media_file').val('');
					swal("Error", res.msg, "error");
					$('#splashImg').hide('slow');
					$('#upload').html('Upload').removeAttr('disabled', 'disabled');
				}
			},error: function() {
				$('#splashImg').hide('slow');
				swal("Error", 'Oops. something went wrong please try again.?', "error");
			}
		});
	} else {
		$('#scenario_media_file,#scenario_media').val('');
		swal("Warning", 'Please select valid file. accept only '+gettype+' file', "warning");
		$('#upload').html('Upload').removeAttr('disabled', 'disabled');
	}
});

$('.delete_scenario_media_file').click(function() {
	var file_name   = $(this).attr("data-scenario-media-file");
	var dataString	= 'delete='+ true + '&scenario_media_file='+ file_name;
	if (file_name) {
		swal({
			title: "Are you sure?",
			text: "Delete this file.",
			icon: "warning",
			buttons: [true, 'Delete'],
			dangerMode: true, }).then((willDelete) => { if (willDelete) {
				$.LoadingOverlay("show");
				$.ajax({
					url: "includes/delete-file.php",
					type: "POST",
					data: dataString,
					cache: false,
					success: function(resdata) {
						$.LoadingOverlay("hide");
						var res = $.parseJSON(resdata);
						if (res.success == false) {
							swal("Error", res.msg, "error");
							$('#splashImg').hide('slow');
						}
						else if (res.success == true) {
							$('#splashImg, #ImgDelete').hide('slow');
							$('#scenario_media_file,#scenario_media').val('');
							swal(res.msg, { buttons: false, timer: 1000});
						}
					},error: function() {
						swal("Error", 'Oops, something went wrong. Please try again later.?', "error");
					}
				});
			} else { swal("Your file is safe.!", { buttons: false, timer: 1000, }); }
		});
	}
});

$('.delete_wb_file').click(function() {
	var file_name   = $(this).attr("data-wb-file");
	var dataString	= 'delete='+ true + '&wb_file='+ file_name;
	if (file_name) {
		swal({
			title: "Are you sure?",
			text: "Delete Web Object file.",
			icon: "warning",
			buttons: [true, 'Delete'],
			dangerMode: true, }).then((willDelete) => { if (willDelete) {
				$.LoadingOverlay("show");
				$.ajax({
					url: "includes/delete-file.php",
					type: "POST",
					data: dataString,
					cache: false,
					success: function(resdata) {
						$.LoadingOverlay("hide");
						var res = $.parseJSON(resdata);
						if (res.success == false) {
							swal("Error", res.msg, "error");
							$('#splashImg').hide('slow');
						}
						else if (res.success == true) {
							$('#splashImg, #ImgDelete').hide('slow');
							$('#scenario_media_file').val('');
							swal(res.msg, { buttons: false, timer: 1000, });
						}
					},error: function() {
						swal("Error", 'Oops, something went wrong. Please try again later.?', "error");
					}
				});
			} else { swal("Your file is safe.!", { buttons: false, timer: 1000, }); }
		});
	}
});

/* Upload Web Object */
$('#upload_web').click(function() {
	$(this).attr('disabled', 'disabled').html('<i class="fa fa-refresh fa-spin fa-fw"></i><span class="sr-only">Loading...</span> Please wait....');
	var file_data = $('#web_file').prop('files')[0];
	var form_data = new FormData();
	form_data.append('file', file_data);
	form_data.append('web_object_file', true);
	var ftype = file_data.type;
	var type  = $('#web_object_type').val();
	var ext   = ftype.split('/')[0];
	if (ext == type) {
		$.ajax({
			url: "includes/upload.php",
			type: "POST",
			data: form_data,
			contentType: false,
			cache: false,
			processData: false,
			success: function(resdata) {
				var res = $.parseJSON(resdata);
				if (res.success == true) {
					$('#WbDelete').show();
					$('.delete_wb').attr('data-wb', res.file_name);
					$('#web_object').val(res.file_name);
					$('#upload_web').html('Upload').removeAttr('disabled', 'disabled');
					swal(res.msg, { buttons: false, timer: 1000, });
				}
				else if (res.success == false) {
					swal("Error", res.msg, "error");
					$('#web_object').val('');
					$('#WbDelete').hide('slow');
					$('#upload_web').html('Upload').removeAttr('disabled', 'disabled');
				}
			},error: function() {
				$('#WbDelete').hide('slow');
				swal("Error", 'Oops. something went wrong please try again.?', "error");
			}
		});
	} else { 
		swal("Warning", 'Please select valid file. accept only zip file', "warning");
		$('#upload_web').html('Upload').removeAttr('disabled', 'disabled');
	}
});

/* Delete Web Object */
$('.delete_wb').click(function() {
	var file_name   = $(this).attr("data-wb");
	var dataString	= 'delete='+ true + '&wb_file='+ file_name;
	if (file_name) {
		swal({
			title: "Are you sure?",
			text: "Delete Web Object file.",
			icon: "warning",
			buttons: [true, 'Delete'],
			dangerMode: true, }).then((willDelete) => { if (willDelete) {
				$.LoadingOverlay("show");
				$.ajax({
					url: "includes/delete-file.php",
					type: "POST",
					data: dataString,
					cache: false,
					success: function(resdata) {
						$.LoadingOverlay("hide");
						var res = $.parseJSON(resdata);
						if (res.success == false) {
							swal("Error", res.msg, "error");
							$('#WbDelete').hide('slow');
						}
						else if (res.success == true) {
							$('#WbDelete').hide('slow');
							$('#web_object').val('');
							swal(res.msg, { buttons: false, timer: 1000, });
						}
					},error: function() {
						swal("Error", 'Oops, something went wrong. Please try again later.?', "error");
					}
				});
			} else { swal("Your file is safe.!", { buttons: false, timer: 1000, }); }
		});
	}
});

$('#uploadBG').click(function() {
	$(this).attr('disabled', 'disabled').html('<i class="fa fa-refresh fa-spin fa-fw"></i><span class="sr-only">Loading...</span> Please wait....');
	var file_data = $('#bg_file').prop('files')[0];
	var form_data = new FormData();
	form_data.append('file', file_data);
	form_data.append('add_own_background', true);
	var ftype = file_data.type;
	var ext = ftype.split('/')[0];
	if (ext == 'image') {
		$.ajax({
			url: "includes/upload.php",
			type: "POST",
			data: form_data,
			contentType: false,
			cache: false,
			processData:false,
			success: function(resdata) {
				var res = $.parseJSON(resdata);
				if (res.success == true) {
					$('.delete_scenario_bg_file').attr('data-scenario-bg-file', res.file_name).show();
					$('#scenario_bg_file').val(res.file_name);
					$('.own-bg-prev').attr('src', '<?php echo $uploadpath ?>' + res.file_name);
					$('#uploadBG').html('Upload').removeAttr('disabled', 'disabled');
					swal(res.msg, { buttons: false, timer: 1000, });
				}
				else if (res.success == false) {
					$('#scenario_bg_file').val('');
					swal("Error", res.msg, "error");
					$('.own-bg-prev').attr('src', 'img/scenario-img.png');
					$('#uploadBG').html('Upload').removeAttr('disabled', 'disabled');
				}
			},error: function() {
				swal("Error", 'Oops. something went wrong please try again.?', "error");
			}
		});
	} else { 
		swal("Warning", 'Please select valid file. accept only jpeg, jpg, png file', "warning");
		$('#uploadBG').html('Upload').removeAttr('disabled', 'disabled');
	}
});

$('.delete_scenario_bg_file').click(function() {
	var file_name   = $(this).attr("data-scenario-bg-file");
	var dataString	= 'delete='+ true + '&scenario_media_file='+file_name;
	if (file_name) {
		swal({
			title: "Are you sure?",
			text: "Delete this file.",
			icon: "warning",
			buttons: [true, 'Delete'],
			dangerMode: true, }).then((willDelete) => { if (willDelete) {
				$.LoadingOverlay("show");
				$.ajax({
					url: "includes/delete-file.php",
					type: "POST",
					data: dataString,
					cache: false,
					success: function(resdata) {
						$.LoadingOverlay("hide");
						var res = $.parseJSON(resdata);
						if (res.success == false) {
							swal("Error", res.msg, "error");
						}
						else if (res.success == true) {
							$('#scenario_bg_file').val('');
							$('.delete_scenario_bg_file').hide();
							$('.own-bg-prev').attr('src', 'img/scenario-img.png');
							swal(res.msg, { buttons: false, timer: 1000, });
						}
					},error: function() {
						$.LoadingOverlay("hide");
						swal("Error", 'Oops, something went wrong. Please try again later.?', "error");
					}
				});
			} else { swal("Your file is safe.!", { buttons: false, timer: 1000, }); }
		});
	}
});

$('#uploadChar').click(function() {
	$(this).attr('disabled', 'disabled').html('<i class="fa fa-refresh fa-spin fa-fw"></i><span class="sr-only">Loading...</span> Please wait....');
	var file_data = $('#file_char').prop('files')[0];
	var form_data = new FormData();
	form_data.append('file', file_data);
	form_data.append('add_char', true);
	var ftype = file_data.type;
	var ext = ftype.split('/')[0];
	if (ext == 'image') {
		$.ajax({
			url: "includes/upload.php",
			type: "POST",
			data: form_data,
			contentType: false,
			cache: false,
			processData:false,
			success: function(resdata) {
				var res = $.parseJSON(resdata);
				if (res.success == true) {
					$('.delete_scenario_char_file').attr('data-scenario-char-file', res.file_name).show();
					$('#scenario_char_file').val(res.file_name);
					$('.sce-imgchar').attr('src', '<?php echo $uploadpath ?>' + res.file_name);
					$('#uploadChar').html('Upload').removeAttr('disabled', 'disabled');
					swal(res.msg, { buttons: false, timer: 1000, });
				}
				else if (res.success == false) {
					$('#scenario_char_file').val('');
					swal("Error", res.msg, "error");
					$('.sce-imgchar').attr('src', 'img/charbg.png');
					$('#uploadChar').html('Upload').removeAttr('disabled', 'disabled');
				}
			},error: function() {
				swal("Error", 'Oops. something went wrong please try again.?', "error");
			}
		});
	} else { 
		swal("Warning", 'Please select valid file. accept only jpeg, jpg, png file', "warning");
		$('#uploadChar').html('Upload').removeAttr('disabled', 'disabled');
	}
});

$('.delete_scenario_char_file').click(function() {
	var file_name   = $(this).attr("data-scenario-char-file");
	var dataString	= 'delete='+ true + '&scenario_media_file='+file_name;
	if (file_name) {
		swal({
			title: "Are you sure?",
			text: "Delete this file.",
			icon: "warning",
			buttons: [true, 'Delete'],
			dangerMode: true, }).then((willDelete) => { if (willDelete) {
				$.LoadingOverlay("show");
				$.ajax({
					url: "includes/delete-file.php",
					type: "POST",
					data: dataString,
					cache: false,
					success: function(resdata) {
						$.LoadingOverlay("hide");
						var res = $.parseJSON(resdata);
						if (res.success == false) {
							swal("Error", res.msg, "error");
						}
						else if (res.success == true) {
							$('#scenario_char_file').val('');
							$('.delete_scenario_char_file').hide();
							$('.sce-imgchar').attr('src', 'img/charbg.png');
							swal(res.msg, { buttons: false, timer: 1000, });
						}
					},error: function() {
						$.LoadingOverlay("hide");
						swal("Error", 'Oops, something went wrong. Please try again later.?', "error");
					}
				});
			} else { swal("Your file is safe.!", { buttons: false, timer: 1000, }); }
		});
	}
});

/* Add Cover Image */
$('#upload_cover_img').click(function() {
	$(this).attr('disabled', 'disabled').html('<i class="fa fa-refresh fa-spin fa-fw"></i><span class="sr-only">Loading...</span> Please wait....');
	var file_data = $('#upload_cover_img_file').prop('files')[0];
	var form_data = new FormData();
	form_data.append('file', file_data);
	form_data.append('scenario_cover_file', true);
	var ftype   = file_data.type;
	var type	= $('#cover_img_file_type').val();
	var ext 	= ftype.split('/')[0];
	if (ext == type) {
		$.ajax({
			url: "includes/upload.php",
			type: "POST",
			data: form_data,
			contentType: false,
			cache: false,
			processData:false,
			success: function(resdata) {
				var res = $.parseJSON(resdata);
				if (res.success == true) {
					$('#scenario_cover_file').val(res.file_name);
					$('.sim_cover_img_data,.delete_cover_img_file').show();
					$('.sim_cover_img_data a').text(res.file_name);
					$('.sim_cover_img_data a').attr('data-src', assets_path + res.file_name);
					$('.cover_image_container a.delete_cover_img_file').attr('data-cover-img-file', res.file_name);
					$('#upload_cover_img').html('Upload').attr('disabled', 'disabled');
					$('.upload_cover_btn').attr('disabled', 'disabled');
					swal(res.msg, { buttons: false, timer: 1000 });
				}
				else if (res.success == false) {
					swal("Error", res.msg, "error");
					$('.sim_cover_img_data').hide('slow');
					$('#upload_cover_img').html('Upload').removeAttr('disabled', 'disabled');
					$('.upload_cover_btn').removeAttr('disabled', 'disabled');
				}
			},error: function() {
				swal("Error", 'Oops. something went wrong please try again.?', "error");
				$('#upload_cover_img').html('Upload').removeAttr('disabled', 'disabled');
				$('.upload_cover_btn').removeAttr('disabled', 'disabled');
			}
		});
	} else {
		swal("Warning", 'Please select valid file. accept only '+ type +' file', "warning");
		$('#upload_cover_img').html('Upload').removeAttr('disabled', 'disabled');
		$('.upload_cover_btn').removeAttr('disabled', 'disabled');
	}
});

/* Delete Cover Image */
$('.delete_cover_img_file').click(function() {
	var file_name   = $(this).attr("data-cover-img-file");
	var dataString	= 'delete='+ true + '&scenario_media_file='+ file_name;
	if (file_name) {
		swal({
			title: "Are you sure?",
			text: "Delete this file.",
			icon: "warning",
			buttons: [true, 'Delete'],
			dangerMode: true }).then((willDelete) => { if (willDelete) {
				$.LoadingOverlay("show");
				$.ajax({
					url: "includes/delete-file.php",
					type: "POST",
					data: dataString,
					cache: false,
					success: function(resdata) {
						$.LoadingOverlay("hide");
						var res = $.parseJSON(resdata);
						if (res.success == false) {
							swal("Error", res.msg, "error");
						}
						else if (res.success == true) {
							$('#scenario_cover_file').val('');
							$('.sim_cover_img_data,.delete_cover_img_file').hide('slow');
							$('.sim_cover_img_data a').text('');
							$('.sim_cover_img_data a').attr('data-src', '');
							$('.cover_image_container a.delete_cover_img_file').attr('data-cover-img-file', '');
							$('#upload_cover_img').html('Upload').removeAttr('disabled', 'disabled');
							$('.upload_cover_btn').removeAttr('disabled', 'disabled');
							swal(res.msg, { buttons: false, timer: 1000 });
						}
					},error: function() {
						swal("Error", 'Oops, something went wrong. Please try again later.?', "error");
					}
				});
			} else { swal("Your file is safe.!", { buttons: false, timer: 1000, }); }
		});
	}
});

$('.sim_page_desc').each(function(e){
	CKEDITOR.replace(this.id, { customConfig: "config-max-description.js" });
});

if ($('#sim_page_desc_0').length) {
	CKEDITOR.replace('sim_page_desc_0', {
		customConfig : "config-max-description.js"
	});
}

var page = <?php echo ( ! empty($pagei)) ? $pagei++ : 1; ?>;
$(".plus_page").click(function() {
	$(".plus_page_option:last").append('<div class="add_page_option appendSCE">\
	<div class="form-group">\
		<input type="text" name="sim_page_name[]" class="form-control" placeholder="Page name" required="required" />\
	</div>\
	<div class="form-group">\
		<textarea name="sim_page_desc[]" id="sim_page_desc_'+ page +'" class="form-control"></textarea>\
	</div>\
	 <div class="remove minusComp"><img src="img/icons/minus.png" title="Remove Page"></div></div>');
	 var page_des_id = 'sim_page_desc_'+ page;
	 CKEDITOR.replace(page_des_id, { customConfig : "config-max-description.js" });
	page++;
});

$('.plus_page_option').on('click', '.remove', function(e) {
	e.preventDefault();
	$(this).parent().remove();
	page--;
});

$('input[name="bgoption"]').on('change', function (){
	var bg = $(this).val();
	if (bg == 1) {
		$('#scenario_bg_file').val('');
		$('input[name="sim_background_img"]').attr('checked', false);
	}
	else if (bg == 2) {
		$("#bg_color").spectrum("set", '');
		$('input[name="sim_background_img"]').attr('checked', false);
	}
	else if (bg == 3) {
		$("#bg_color").spectrum("set", '');
		$('#scenario_bg_file').val('');
	}
});

/**** MTF ****/
function matchopen1() {
  document.getElementById("match1").style.display = "block";
}
function matchclose1() {
  document.getElementById("match1").style.display = "none";
}

function mtf_feedback_tts() {
  document.getElementById("mtf_feedback_tts_model").style.display = "block";
}

function mtf_feedback_tts_close1() {
  document.getElementById("mtf_feedback_tts_model").style.display = "none";
}

function mtfi_feedback_tts() {
  document.getElementById("mtfi_feedback_tts_model").style.display = "block";
}

function mtfi_feedback_tts_close1() {
  document.getElementById("mtfi_feedback_tts_model").style.display = "none";
}

<?php if  ( ! empty($sim_data['bg_color'])) : ?>
$("#bg_color").spectrum("set", '<?php echo $sim_data['bg_color'] ?>');
<?php endif; 
if  ( ! empty($sim_brand['ques_bg'])) : ?>
$("#ques_bg_color").spectrum("set", '<?php echo $sim_brand['ques_bg'] ?>');
<?php endif; 
if  ( ! empty($sim_brand['option_bg'])) : ?>
$("#ques_option_bg_color").spectrum("set", '<?php echo $sim_brand['option_bg'] ?>');
<?php endif; 
if  ( ! empty($sim_brand['option_hover'])) : ?>
$("#ques_option_hover_color").spectrum("set", '<?php echo $sim_brand['option_hover'] ?>');
<?php endif; 
if  ( ! empty($sim_brand['option_selected'])) : ?>
$("#ques_option_select_color").spectrum("set", '<?php echo $sim_brand['option_selected'] ?>');
<?php endif; 
if  ( ! empty($sim_brand['btn_bg'])) : ?>
$("#btn_bg_color").spectrum("set", '<?php echo $sim_brand['btn_bg'] ?>');
<?php endif; 
if  ( ! empty($sim_brand['btn_hover'])) : ?>
$("#btn_hover_color").spectrum("set", '<?php echo $sim_brand['btn_hover'] ?>');
<?php endif; 
if  ( ! empty($sim_brand['btn_selected'])) : ?>
$("#btn_select_color").spectrum("set", '<?php echo $sim_brand['btn_selected'] ?>');
<?php endif; 
if  ( ! empty($sim_brand['font_color'])) : ?>
$("#font_color").spectrum("set", '<?php echo $sim_brand['font_color'] ?>');
<?php endif; 
if  ( ! empty($sim_brand['font_type'])) : ?>
$("#font_type").trigger("setFont", '<?php echo $sim_brand['font_type'] ?>');
<?php endif; ?>
document.onreadystatechange = function() {
	if (document.readyState == "complete"){
		$.LoadingOverlay("hide");
	}
};
</script>
<?php 
require_once 'includes/footer.php';
