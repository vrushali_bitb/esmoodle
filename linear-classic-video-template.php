<?php 
session_start();
if (isset($_SESSION['username'])) {
    $user   = $_SESSION['username'];
    $role   = $_SESSION['role'];
    $userid = $_SESSION['userId'];
	$domain = (isset($_SESSION['domain_name'])) ? $_SESSION['domain_name'] : $_SERVER['HTTP_HOST'];
}
else {
    header('location: index.php');
}
require_once 'config/db.class.php';
require_once 'includes/learners_menu.php';
$db 		= new DBConnection();
$sim_id		= (isset($_GET['sim_id']) && ! empty($_GET['sim_id'])) ? $_GET['sim_id'] : '';
$ques_id	= (isset($_GET['ques_id'])) ? base64_decode($_GET['ques_id']) : '';
$sim_data 	= $db->getScenario($sim_id);
$sim_com 	= $db->getScenarioCompetency($sim_id);
$sim_page 	= $db->getSimPage($sim_data['scenario_id']);
$sim_brand 	= $db->getSimBranding($sim_data['scenario_id']);
$sim_type	= $db->getMultiScenarioType($sim_data['author_scenario_type']);
$path		= $db->getBaseUrl('scenario/upload/'.$domain.'/');
$uploadpath	= '../scenario/upload/'.$domain.'/';
$root_path	= $db->rootPath() . 'scenario/upload/'.$domain.'/';
$prelink 	= 'launch-sim-linear-classic-video.php?preview=true&save=false&sim_id='. $sim_id;
$preview    = ( ! empty($ques_id)) ? $prelink . '&ques_id='. base64_encode($ques_id) : $prelink;
$type_name	= $sim_type[0]['type_name'];
$type 		= strtolower($type_name);
$gettype 	= '';
if ($type == 'image and text'):
	$gettype = 'image';
else:
	$gettype = $type;
endif;
$upload_file_name = '';
if ( ! empty($sim_data['image_url'])):
	$upload_file_name = $sim_data['image_url'];
elseif ( ! empty($sim_data['video_url'])):
	$upload_file_name = $sim_data['video_url'];
elseif ( ! empty($sim_data['audio_url'])):
	$upload_file_name = $sim_data['audio_url'];
elseif ( ! empty($sim_data['web_object_file'])):
	$upload_file_name = $sim_data['web_object_file'];
endif;
$web_object = '';
if ( ! empty($sim_data['web_object'])):
	$web_object = $sim_data['web_object'];
endif;
$ques_val1 = $ques_val2 = $ques_val3 = $ques_val4 = $ques_val5 = $ques_val6 = 0;
$qresult = [];
if ( ! empty($ques_id)):
	$qsql = "SELECT * FROM question_tbl WHERE question_id = '". $ques_id ."'";
	try {
		$qstmt	 = $db->prepare($qsql); $qstmt->execute();
		$qrow	 = $qstmt->rowCount();
		$qresult = ($qrow > 0) ? $qstmt->fetch() : [];
	}
	catch(PDOException $e) {
		echo "Oops. something went wrong. ".$e->getMessage(); exit;
	}	
endif;
$tqsql  = "SELECT * FROM question_tbl WHERE MD5(scenario_id) = '". $sim_id ."'";
$tqstmt = $db->prepare($tqsql); $tqstmt->execute();
$tques	 = $tqstmt->rowCount();
$tqresult = $tqstmt->fetchAll();
$question_type = ( ! empty($tqresult[0]["question_type"])) ? $tqresult[0]["question_type"] : '';
$videoq_media_file = ( ! empty($tqresult[0]["videoq_media_file"])) ? $tqresult[0]["videoq_media_file"] : '';
foreach ($tqresult as $tqueSocre):
	if ( ! empty($tqueSocre['ques_val_1'])) $ques_val1 += $tqueSocre['ques_val_1'];
	if ( ! empty($tqueSocre['ques_val_2'])) $ques_val2 += $tqueSocre['ques_val_2'];
	if ( ! empty($tqueSocre['ques_val_3'])) $ques_val3 += $tqueSocre['ques_val_3'];
	if ( ! empty($tqueSocre['ques_val_4'])) $ques_val4 += $tqueSocre['ques_val_4'];
	if ( ! empty($tqueSocre['ques_val_5'])) $ques_val5 += $tqueSocre['ques_val_5'];
	if ( ! empty($tqueSocre['ques_val_6'])) $ques_val6 += $tqueSocre['ques_val_6'];
endforeach; ?>
<link rel="stylesheet" type="text/css" href="content/css/owlcarousel/owl.carousel.min.css" />
<link rel="stylesheet" type="text/css" href="content/css/qustemplate.css" />
<link rel="stylesheet" type="text/css" href="content/css/questionresponsive.css" />
<link rel="stylesheet" type="text/css" href="content/css/template-updated.css" />
<link rel="stylesheet" type="text/css" href="content/css/jquery.fontselect.css">
<script type="text/javascript" src="content/js/inputmask/jquery.inputmask.js"></script>
<script type="text/javascript" src="content/js/spectrum/spectrum.min.js"></script>
<link rel="stylesheet" type="text/css" href="content/js/spectrum/spectrum.min.css" />
<link rel="stylesheet" type="text/css" href="content/fancybox/jquery.fancybox.min.css" />
<script type="text/javascript" src="content/fancybox/jquery.fancybox.min.js"></script>
<script type="text/javascript">
$.LoadingOverlay("show");
</script>
<style type="text/css">
	body {
		overflow:hidden;
		background:#efefef;
	}
	[data-title] {
		font-size: 16px;
		position: relative;
		cursor: help;
	}
	[data-title]:hover::before {
		content: attr(data-title);
		position: absolute;
		bottom: -20px;
		padding: 0px;
		color: #2c3545;
		font-size: 14px;
		white-space: nowrap;
		z-index: 99;
	}
	span.truncateTxt {
		width: 100px;
		white-space: nowrap;
		overflow: hidden;
		text-overflow: ellipsis;
		float: left;
	}
	:required  {
		background: url('img/list/star.png') no-repeat !important;
		background-position:right top !important;
		background-color: #fff !important;
	}
	.required  {
		background: url('img/list/star.png') no-repeat !important;
		background-position:right top !important;
		background-color: #fff !important;
	}
</style>
<form method="post" class="form-inline" name="Form_linear_Video_Template" id="Form_linear_Video_Template">
	<input type="hidden" name="update_linear_video_sim" value="1" />
    <input type="hidden" name="sim_id" id="sim_id" value="<?php echo $sim_data['scenario_id'] ?>" />
    <input type="hidden" name="gettype" value="<?php echo $gettype ?>" />
    <input type="hidden" name="character_type" id="character_type" />
    <input type="hidden" name="submit_type" id="submit_type" />
    <input type="hidden" name="ques_id" id="ques_id" value="<?php echo $ques_id; ?>" />
    <input type="hidden" name="get_question_type" id="get_question_type" value="<?php echo ( ! empty($qresult['question_type'])) ? $qresult['question_type'] : '' ?>" />
    <input type="hidden" name="question_type" id="question_type" value="<?php echo ( ! empty($qresult['question_type'])) ? $qresult['question_type'] : '' ?>" />
    <input type="hidden" name="location" id="location" value="<?php echo $db->getBaseUrl($db->getFile().'?add_data=true&sim_id='. $sim_id); ?>" />
	<input type="hidden" name="cuePoint" id="cuePoint" value="<?php echo ( ! empty($tqresult[0]['videoq_cue_point'])) ? $tqresult[0]['videoq_cue_point'] : ''; ?>">
	<input type="hidden" id="vtype" value="" />
    <div class="sim-side-menu">
        <ul class="side-menu-list nav nav-tabs">
			<li class="Add_Web <?php echo (empty($ques_id)) ? 'active' : ''; ?>"><a data-toggle="tab" href="#Add_Web"><div class="tooltipmenu"><img class="svg addScenario" src="img/list/Add_Learning.svg"/><span class="tooltiptext">Add Learning</span></div></a></li>
			<li class="Add_Scenario"><a data-toggle="tab" href="#Add_Scenario"><div class="tooltipmenu"><img class="svg addScenario" src="img/list/add_Scenario.svg"/><span class="tooltiptext">Add Scenario</span></div></a></li>
            <li class="Add_Branding"><a data-toggle="tab" href="#Add_Branding"><div class="tooltipmenu"><img class="detail_w svg" src="img/list/add_branding.svg"/><span class="tooltiptext">Add Branding</span></div></a></li>
			<li class="main_menu"><a data-toggle="tab" href="#main_menu"><div class="tooltipmenu"><img class="svg add_qa" src="img/list/add_detail.svg"/><span class="tooltiptext">Simulation Details</span></div></a></li>
			<?php if ( ! empty($ques_id)){ ?>
				<li class="QUeS_menu main_menu <?php echo ( ! empty($ques_id)) ? 'active' : ''; ?>"><a href="<?php echo $db->getBaseUrl($db->getFile().'?add_data=true&sim_id='. $sim_id.'&ques_id='.base64_encode($tqresult[0]['question_id'])); ?>"><div class="tooltipmenu"><img class="svg add_qa" src="img/list/add_question1.svg"/><span class="tooltiptext">Question Template</span></div></a></li>
			<?php } else { ?>
				<li class="QUeS_menu main_menu <?php echo ( ! empty($ques_id)) ? 'active' : ''; ?>"><a data-toggle="tab"  href="#questiontamp"><div class="tooltipmenu"><img class="svg add_qa" src="img/list/add_question1.svg"/><span class="tooltiptext">Question Template</span></div></a></li>
			<?php } ?>
		</ul>
    </div>
    <div class="question_tree_banner tab-content clearfix">
		<!--Add-Web-Object-->
        <div class="bg_selection clearfix tab-pane fade <?php echo (empty($ques_id)) ? 'in active' : ''; ?>" id="Add_Web">
            <div class="question_tree_menu NoBorder" id="question_tree_menu1">
				<div class="Simulation_det stopb clearfix">
					<h3>Add Learning</h3>
					<div class="previewQus Sbtn">
						<a href="<?php echo $preview ?>" target="_blank" title="Preview"><img class="img-fluid" src="img/qus_icon/preview.svg"></a>
					</div>
			    </div>
            	<div class="Group_managment">
					<div class="container">
						<div class="row">
							<div class="col-sm-12">
								<div class="usermanage_main">
									<div class="usermanage-form">
										<div class="row">
											<div class="tilleobj">
												<div class="add_page_option appendSCE">
													<div class="form-group">
														<input type="text" name="web_object_title" class="form-control" placeholder="Title name" autocomplete="off" value="<?php echo ( ! empty($sim_data['web_object_title'])) ? $sim_data['web_object_title'] : ''; ?>" />
													</div>
												</div>
											</div>
											<div class="tilleobj">
												<div class="addsce-img">
													<div class="adimgbox">
														<div class="form-group">
															<input type="file" name="web_file" id="web_file" class="form-control file" />
															<input type="text" class="form-control controls" disabled placeholder="Select Web Object file" value="<?php echo $web_object; ?>" />
															<input type="hidden" name="web_object" id="web_object" value="<?php echo $web_object; ?>" />
															<input type="hidden" name="web_object_type" id="web_object_type" value="application" />
															<div class="browsebtn browsebtntmp">
																<span id="WbDelete" <?php echo ( ! empty($web_object)) ? '' : 'style="display:none"'; ?>>
																	<a href="javascript:void(0);" data-wb="<?php echo ( ! empty($web_object)) ? $web_object : ''; ?>" class="delete_wb" title="Delete File"><i class="fa fa-times" aria-hidden="true"></i></a>
																</span>
															</div>
														</div>
													</div>
													<div class="adimgbox browsebtn"><button class="browse btn btn-primary" type="button">Browse</button></div>
													<div class="adimgbox browsebtn"><button type="button" name="upload_web" id="upload_web" class="btn btn-primary">Upload</button></div>
												</div>
											</div>										
                                         </div>
									</div>
								</div>
							</div>
						</div>
					</div>
                </div>
            </div>
        </div>
		<!--Add-Simulation-->
        <div class="bg_selection clearfix tab-pane fade" id="Add_Scenario">
            <div class="question_tree_menu NoBorder" id="question_tree_menu1">
				<div class="Simulation_det stopb clearfix">
					<h3>Add Scenario</h3>
					<div class="previewQus Sbtn">
						<a href="<?php echo $preview ?>" target="_blank" title="Preview"><img class="img-fluid" src="img/qus_icon/preview.svg"></a>
					</div>
			    </div>
            	<div class="Group_managment">
					<div class="container">
						<div class="row">
							<div class="col-sm-12">
								<div class="usermanage_main">
									<div class="usermanage-form">
										<div class="row">
                                        	<?php if ($gettype != 'text only' && $gettype != 'web object'): ?>
                                            <div class="addsce-img">
                                            	<div class="adimgbox">
                                                    <div class="form-group">
                                                        <input type="file" name="file" id="file" class="form-control file" />
                                                        <input type="text" class="form-control controls" disabled placeholder="Select <?php echo $gettype; ?> file" value="<?php echo $upload_file_name; ?>" />
                                                        <input type="hidden" name="scenario_media_file" id="scenario_media_file" value="<?php echo $upload_file_name; ?>" />
                                                        <input type="hidden" name="scenario_media_file_type" id="scenario_media_file_type" value="<?php echo $gettype; ?>" />
														<div class="browsebtn browsebtntmp">
															<span id="splashImg"><?php if ( ! empty($upload_file_name)) : ?><a href="<?php echo $uploadpath . $upload_file_name ?>" target="_blank" title="View Scenario Media File"><i class="fa fa-eye" aria-hidden="true"></i></a><?php endif; ?></span>
															<span id="ImgDelete" <?php echo ( ! empty($upload_file_name)) ? '' : 'style="display:none"'; ?>><a href="javascript:void(0);" data-scenario-media-file="<?php echo ( ! empty($upload_file_name)) ? $upload_file_name : ''; ?>" class="delete_scenario_media_file" title="Delete File"><i class="fa fa-times" aria-hidden="true"></i></a></span>
														</div>
                                                    </div>
                                                </div>
												<div class="adimgbox browsebtn"><button class="browse btn btn-primary" type="button">Browse</button></div>
                                                <div class="adimgbox browsebtn"><button type="button" name="upload" id="upload" class="btn btn-primary">Upload</button></div>
                                            </div>
											<?php elseif ($gettype == 'web object'): ?>
											<div class="addsce-img">
												<div class="adimgbox">
													<div class="form-group">
                                                        <input type="file" name="file" id="file" class="form-control file" />
                                                        <input type="text" class="form-control controls" disabled placeholder="Select <?php echo $gettype; ?> file" value="<?php echo $upload_file_name; ?>" />
                                                        <input type="hidden" name="scenario_media_file" id="scenario_media_file" value="<?php echo $upload_file_name; ?>" />
                                                        <input type="hidden" name="scenario_media_file_type" id="scenario_media_file_type" value="application" />
														<div class="browsebtn browsebtntmp"><span id="ImgDelete" <?php echo ( ! empty($upload_file_name)) ? '' : 'style="display:none"'; ?>><a href="javascript:void(0);" data-wb-file="<?php echo ( ! empty($upload_file_name)) ? $upload_file_name : ''; ?>" class="delete_wb_file" title="Delete File"><i class="fa fa-times" aria-hidden="true"></i></a></span></div>
                                                    </div>
                                                </div>
												<div class="adimgbox browsebtn"><button class="browse btn btn-primary" type="button">Browse</button></div>
                                                <div class="adimgbox browsebtn"><button type="button" name="upload" id="upload" class="btn btn-primary">Upload</button></div>
                                            </div>
                                            <?php endif; ?>
											<div class="user_text addtext">Add Tabs</div>
											<div class="col-sm-12">
												<?php if ( ! empty($sim_page)):
												$pagei = 1; foreach ($sim_page as $pdata): ?>
                                                <div class="plus_page_option">
                                                    <div class="add_page_option appendSCE">
                                                        <div class="form-group">
                                                            <input type="text" name="sim_page_name[]" class="form-control" placeholder="Page name" required="required" autocomplete="off" value="<?php echo stripcslashes($pdata['sim_page_name']); ?>" />
                                                        </div>
                                                        <div class="form-group">
                                                            <textarea name="sim_page_desc[]" id="sim_page_desc_<?php echo $pagei ?>" class="form-control sim_page_desc"><?php echo $pdata['sim_page_desc'] ?></textarea>
                                                        </div>
                                                        <?php if ($pagei == 1): ?>
                                                         <div class="plus_page"><img src="img/icons/plus.png" title="Add More Page" /></div>
                                                         <?php else: ?>
                                                         <div class="remove minusComp"><img src="img/icons/minus.png" title="Remove Page"></div>
                                                         <?php endif; ?>
                                                    </div>
                                                </div>
                                                <?php $pagei++; endforeach; else: ?>
                                            	<div class="plus_page_option">
                                                    <div class="add_page_option appendSCE">
                                                        <div class="form-group">
                                                            <input type="text" name="sim_page_name[]" class="form-control required" placeholder="Page name" autocomplete="off" />
                                                        </div>
                                                        <div class="form-group">
                                                            <textarea name="sim_page_desc[]" id="sim_page_desc_0" class="form-control"></textarea>
                                                        </div>
                                                        <div class="plus_page"><img src="img/icons/plus.png" title="Add More Page" /></div>
                                                    </div>
                                                </div>
                                                <?php endif; ?>
											</div>
                                         </div>
									</div>
								</div>
							</div>
						</div>
					</div>
                </div>
            </div>
        </div>
        <!--Add-Branding-->
		<div class="main tab-pane fade" id="Add_Branding">
            <div class="main_sim question_tree_menu ClassicMainSim">
				<div class="Simulation_det stopb clearfix">
					<h3>Adding Branding</h3>
					<div class="previewQus Sbtn">
						<a href="<?php echo $preview ?>" target="_blank" title="Preview"><img class="img-fluid" src="img/qus_icon/preview.svg"></a>
					</div>
				</div>
				<div class="Group_managment">
					<div class="container">
						<div class="branding-color">
							<div class="branding-flex brandingbox">
								<div class="coloredittool-box">
									<div class="coloredittool">
										<div class="user_text colorp">Question Background</div>
										<div class="branding-flex"><span class="colorb">Color</span><input class="type-color-on-page" name="ques_bg_color" id="ques_bg_color" /></div>
					
									</div>
									<div class="coloredittool">
										<div class="user_text colorp">Option Background</div>
										<div class="branding-flex"><span class="colorb">Color</span><input class="type-color-on-page" name="ques_option_bg_color" id="ques_option_bg_color" /></div>
									</div>
									<div class="coloredittool">
										<div class="user_text colorp">Option Hover</div>
										<div class="branding-flex"><span class="colorb">Color</span><input class="type-color-on-page" name="ques_option_hover_color" id="ques_option_hover_color" /></div>
									</div>
									<div class="coloredittool">
										<div class="user_text colorp">Option Selected</div>
										<div class="branding-flex"><span class="colorb">Color</span><input class="type-color-on-page" name="ques_option_select_color" id="ques_option_select_color" /></div>
									</div>
								</div>
								<div class="coloredittool-box">
									<div class="coloredittool">
										<div class="user_text colorp">Submit button Background</div>
										<div class="branding-flex"><span class="colorb">Color</span><input class="type-color-on-page" name="btn_bg_color" id="btn_bg_color" /></div>
									</div>
									<div class="coloredittool">
										<div class="user_text colorp">Submit button Hover</div>
										<div class="branding-flex"><span class="colorb">Color</span><input class="type-color-on-page" name="btn_hover_color" id="btn_hover_color" /></div>
									</div>
									<div class="coloredittool">
										<div class="user_text colorp">Submit button Selected</div>
										<div class="branding-flex"><span class="colorb">Color</span><input class="type-color-on-page" name="btn_select_color" id="btn_select_color" /></div>
									</div>
								</div>
							</div>
						</div>
						<div class="user_text fonts">Font</div>
						<div class="branding-font">
							<div class="branding-flex">
								<span class="colorb">Font Type</span>
								<input id="font_type" name="font_type" type="text" />
								<?php if ( ! empty($sim_brand['font_type'])): ?>
								<div class="col-sm-2"><button class="cl_ftype btn btn-primary" type="button">Set Default Font Type</button></div>
								<?php endif; ?>
							</div>
							<div class="branding-flex">
								<span class="colorb">Font Color</span>
								<input class="type-color-on-page" name="font_color" id="font_color" />
							</div>
							<div class="branding-flex"><span class="colorb">Font Size</span>
							  <select name="font_size" id="font_size" class="form-control">
							    <option selected="selected" value="0">Select font size</option>
								<option value="10px" <?php echo ( ! empty($sim_brand['font_size']) && $sim_brand['font_size'] == '10px') ? 'selected="selected"' : '' ?>>10px</option>
								<option value="12px" <?php echo ( ! empty($sim_brand['font_size']) && $sim_brand['font_size'] == '12px') ? 'selected="selected"' : '' ?>>12px</option>
								<option value="16px" <?php echo ( ! empty($sim_brand['font_size']) && $sim_brand['font_size'] == '16px') ? 'selected="selected"' : '' ?>>16px</option>
								<option value="18px" <?php echo ( ! empty($sim_brand['font_size']) && $sim_brand['font_size'] == '18px') ? 'selected="selected"' : '' ?>>18px</option>
								<option value="20px" <?php echo ( ! empty($sim_brand['font_size']) && $sim_brand['font_size'] == '20px') ? 'selected="selected"' : '' ?>>20px</option>
								<option value="24px" <?php echo ( ! empty($sim_brand['font_size']) && $sim_brand['font_size'] == '24px') ? 'selected="selected"' : '' ?>>24px</option>
								<option value="26px" <?php echo ( ! empty($sim_brand['font_size']) && $sim_brand['font_size'] == '26px') ? 'selected="selected"' : '' ?>>26px</option>
							  </select>
						  </div>
						</div>
					</div>
				</div>
            </div>
        </div>
        <!--Add-Sim-Details-->
        <div class="main tab-pane fade" id="main_menu">
            <div class="main_sim question_tree_menu">
                <div class="Simulation_det stopb clearfix">
                    <h3>Simulation Details</h3>
                    <div class="previewQus Sbtn">
                        <a href="<?php echo $preview ?>" target="_blank" title="Preview"><img class="img-fluid" src="img/qus_icon/preview.svg"></a>
                    </div>
                </div>
                <div class="Group_managment CreateSIMCOMPTBAN">				
					<div class="row" style="margin-top:20px">
						<div class="col-sm-12">
							<div class="form-group branching ">
								<label class="branching-text">Simulation Name</label>
								<input type="text" name="sim_title" id="sim_title" placeholder="Simulation Name" value="<?php echo $sim_data['Scenario_title'] ?>" class="form-control required" />
							</div>
							<div class="form-group branching">
								<label class="branching-text">Duration(min)</label>
								<input type="text" class="form-control question-box required" name="sim_duration" id="sim_duration" onkeypress="return isNumberKey(event);" autocomplete="off" value="<?php echo $sim_data['duration'] ?>" />
							</div>
							<div class="form-group branching">
								<label class="branching-text">Passing Score(%)</label>
								<input type="text" class="form-control question-box required" name="passing_marks" id="passing_marks" onkeypress="return isNumberKey(event);" autocomplete="off" value="<?php echo $sim_data['passing_marks'] ?>" />
							</div>
						</div>
					</div>
					<?php $sim_cover_img  = '';
					if ( ! empty($sim_data['sim_cover_img']) && file_exists($root_path . $sim_data['sim_cover_img'])):
						$sim_cover_img = $sim_data['sim_cover_img'];
					endif; ?>
					<div class="edit-sim-box editSimAUTH">
						<div class="Simulation_det VreACom" style="margin-bottom: 20px;">
							<h3>Add Cover Image</h3>
						</div>
						<div class="addsce-img">
							<div class="imgpath imgpathAUT">
								<div class="form-group">
									<input type="file" name="upload_cover_img_file" id="upload_cover_img_file" class="form-control file" />
									<input type="text" class="form-control controls" disabled placeholder="Select File" />
									<input type="hidden" name="scenario_cover_file" id="scenario_cover_file" value="<?php echo ( ! empty($sim_cover_img)) ? $sim_cover_img : ''; ?>" />
									<input type="hidden" name="cover_img_file_type" id="cover_img_file_type" value="image" />
									<div class="sim_cover_img_data"><a class="view_assets" data-src="<?php echo $path . $sim_cover_img; ?>" href="javascript:;"><?php echo $sim_cover_img; ?></a></div>
								</div>
							</div>
							<div class="browsebtn">
								<div class="cover_image_container" <?php echo ( ! empty($sim_cover_img)) ? '' : 'style="display:none;"'; ?>>
									<a href="javascript:void(0);" data-cover-img-file="<?php echo ( ! empty($sim_cover_img)) ? $sim_cover_img : ''; ?>" class="delete_cover_img_file" title="Delete File"><i class="fa fa-times" aria-hidden="true"></i></a>
								</div>
							</div>
							<div class="browsebtn"><button class="browse btn btn-primary upload_cover_btn" type="button" <?php echo ( ! empty($sim_cover_img)) ? 'disabled' : ''; ?>>Browse & Select</button></div>
							<div class="browsebtn"><button type="button" name="upload_cover_img" id="upload_cover_img" class="btn btn-primary" <?php echo ( ! empty($sim_cover_img)) ? 'disabled' : ''; ?>>Upload</button></div>
						</div>
					</div>
					<div class="Simulation_det VreACom" style="margin-top: 20px;">
						<h3>Create Competency</h3>
					</div>
					<div class="row" style="margin-top:20px">
					<?php if ( ! empty($sim_com)): ?>
						<div class="plus_comp_option linaer">
						<?php $i = 1; if ( ! empty($sim_com['comp_col_1'])): ?>
						<div class="add_comp_option">
							<div class="form-group">
								<input type="text" placeholder="Competency Name" class="form-control" name="competency_name_1" autocomplete="off" value="<?php echo stripcslashes($sim_com['comp_col_1']); ?>" required="required" />
							</div>
							<div class="form-group labelbox">
								<label>Competency Score</label>
								<input type="text" class="form-control question-box" name="competency_score_1" onkeypress="return isNumberKey(event);" value="<?php echo $sim_com['comp_val_1'] ?>" required="required" />
							</div>
							<div class="form-group labelbox">
								<label>Weightage</label>
								<input type="text" class="form-control question-box" name="weightage_1" onkeypress="return isNumberKey(event);" value="<?php echo $sim_com['weightage_1'] ?>" required="required" />
							</div>
							<div class="plus_comp"><img src="img/icons/plus.png" title="Add More"></div>
						</div>
						<?php endif;
						if ( ! empty($sim_com['comp_col_2'])): $i += 1; ?>
						<div class="add_comp_option">
							<div class="form-group">
								<input type="text" placeholder="Competency Name" class="form-control" name="competency_name_2" autocomplete="off" value="<?php echo stripcslashes($sim_com['comp_col_2']); ?>" required="required" />
							</div>
							<div class="form-group labelbox">
								<label>Competency Score</label>
								<input type="text" class="form-control question-box" name="competency_score_2" onkeypress="return isNumberKey(event);" value="<?php echo $sim_com['comp_val_2'] ?>" required="required" />
							</div>
							<div class="form-group labelbox">
								<label>Weightage</label>
								<input type="text" class="form-control question-box" name="weightage_2" onkeypress="return isNumberKey(event);" value="<?php echo $sim_com['weightage_2'] ?>" required="required" />
							</div>
							<div class="remove minusComp"><img src="img/icons/minus.png" title="Remove"></div>
						</div>
						<?php endif;
						if ( ! empty($sim_com['comp_col_3'])): $i += 1; ?>
						<div class="add_comp_option">
							<div class="form-group">
								<input type="text" placeholder="Competency Name" class="form-control" name="competency_name_3" autocomplete="off" value="<?php echo stripcslashes($sim_com['comp_col_3']); ?>" required="required" />
							</div>
							<div class="form-group labelbox">
								<label>Competency Score</label>
								<input type="text" class="form-control question-box" name="competency_score_3" onkeypress="return isNumberKey(event);" value="<?php echo $sim_com['comp_val_3'] ?>" required="required" />
							</div>
							<div class="form-group labelbox">
								<label>Weightage</label>
								<input type="text" class="form-control question-box" name="weightage_3" onkeypress="return isNumberKey(event);" value="<?php echo $sim_com['weightage_3'] ?>" required="required" />
							</div>
							<div class="remove minusComp"><img src="img/icons/minus.png" title="Remove"></div>
						</div>
						<?php endif;
						if ( ! empty($sim_com['comp_col_4'])): $i += 1; ?>
						<div class="add_comp_option">
							<div class="form-group">
								<input type="text" placeholder="Competency Name" class="form-control" name="competency_name_4" autocomplete="off" value="<?php echo stripcslashes($sim_com['comp_col_4']); ?>" required="required" />
							</div>
							<div class="form-group labelbox">
								<label>Competency Score</label>
								<input type="text" class="form-control question-box" name="competency_score_4" onkeypress="return isNumberKey(event);" value="<?php echo $sim_com['comp_val_4'] ?>" required="required" />
							</div>
							<div class="form-group labelbox">
								<label>Weightage</label>
								<input type="text" class="form-control question-box" name="weightage_4" onkeypress="return isNumberKey(event);" value="<?php echo $sim_com['weightage_4'] ?>" required="required" />
							</div>
							<div class="remove minusComp"><img src="img/icons/minus.png" title="Remove"></div>
						</div>
						<?php endif;
						if ( ! empty($sim_com['comp_col_5'])): $i += 1; ?>
						<div class="add_comp_option">
							<div class="form-group">
								<input type="text" placeholder="Competency Name" class="form-control" name="competency_name_5" autocomplete="off" value="<?php echo stripcslashes($sim_com['comp_col_5']); ?>" required="required" />
							</div>
							<div class="form-group labelbox">
								<label>Competency Score</label>
								<input type="text" class="form-control question-box" name="competency_score_5" onkeypress="return isNumberKey(event);" value="<?php echo $sim_com['comp_val_5'] ?>" required="required" />
							</div>
							<div class="form-group labelbox">
								<label>Weightage</label>
								<input type="text" class="form-control question-box" name="weightage_5" onkeypress="return isNumberKey(event);" value="<?php echo $sim_com['weightage_5'] ?>" required="required" />
							</div>
							<div class="remove minusComp"><img src="img/icons/minus.png" title="Remove"></div>
						</div>
						<?php endif;
						if ( ! empty($sim_com['comp_col_6'])): $i += 1; ?>
						<div class="add_comp_option">
							<div class="form-group">
								<input type="text" placeholder="Competency Name" class="form-control" name="competency_name_6" autocomplete="off" value="<?php echo stripcslashes($sim_com['comp_col_6']); ?>" required="required" />
							</div>
							<div class="form-group labelbox">
								<label>Competency Score</label>
								<input type="text" class="form-control question-box" name="competency_score_6" onkeypress="return isNumberKey(event);" value="<?php echo $sim_com['comp_val_6'] ?>" required="required" />
							</div>
							<div class="form-group labelbox">
								<label>Weightage</label>
								<input type="text" class="form-control question-box" name="weightage_6" onkeypress="return isNumberKey(event);" value="<?php echo $sim_com['weightage_6'] ?>" required="required" />
							</div>
							<div class="remove minusComp"><img src="img/icons/minus.png" title="Remove"></div>
						</div>
						<?php endif; ?>
						</div>
						<?php else: ?>
						<div class="plus_comp_option linaer">
							<div class="add_comp_option">
								<div class="form-group">
									<input type="text" placeholder="Competency Name" class="form-control required" name="competency_name_1" autocomplete="off" />
								</div>
								<div class="form-group labelbox">
									<label>Competency Score</label>
									<input type="text" class="form-control question-box required" name="competency_score_1" onkeypress="return isNumberKey(event);" />
								</div>
								<div class="form-group labelbox">
									<label>Weightage</label>
									<input type="text" class="form-control question-box required" name="weightage_1" onkeypress="return isNumberKey(event);" />
								</div>
								<div class="plus_comp"><img src="img/icons/plus.png" title="Add More"></div>
							</div>
						</div>
						<?php endif; ?>
					</div>
				</div>
           </div>
        </div>
        <!--Question-Temp-->
        <div class="Questio-template ClassicMULtitemp tab-pane fade <?php echo ( ! empty($ques_id)) ? 'in active' : ''; ?>" id="questiontamp">
			<div class="main_sim_scroll">
				<div class="Simulation_det stopb clearfix unsetp">
					<div class="skilQue">
						<?php if ( ! empty($sim_com['comp_col_1']) &&  ! empty($sim_com['comp_val_1'])): ?>
						<div class="skills">
							<label data-title="<?php echo $sim_com['comp_col_1'] ?>"><span class="truncateTxt"><?php echo $sim_com['comp_col_1'] ?></span></label>
							<input type="text" class="form-control" id="comp_val_1" readonly="readonly" value="<?php echo abs($sim_com['comp_val_1'] - $ques_val1); ?>" />
						</div>
						<?php endif; ?>
						
						<?php if ( ! empty($sim_com['comp_col_2']) &&  ! empty($sim_com['comp_val_2'])): ?>
						<div class="skills">
							<label data-title="<?php echo $sim_com['comp_col_2'] ?>"><span class="truncateTxt"><?php echo $sim_com['comp_col_2'] ?></span></label>
							<input type="text" class="form-control" id="comp_val_2" readonly="readonly" value="<?php echo abs($sim_com['comp_val_2'] - $ques_val2); ?>" />
						</div>
						<?php endif; ?>
					
						<?php if ( ! empty($sim_com['comp_col_3']) &&  ! empty($sim_com['comp_val_3'])): ?>
						<div class="skills">
							<label data-title="<?php echo $sim_com['comp_col_3'] ?>"><span class="truncateTxt"><?php echo $sim_com['comp_col_3'] ?></span></label>
							<input type="text" class="form-control" id="comp_val_3" readonly="readonly" value="<?php echo abs($sim_com['comp_val_3'] - $ques_val3); ?>" />
						</div>
						<?php endif; ?>
					
						<?php if ( ! empty($sim_com['comp_col_4']) &&  ! empty($sim_com['comp_val_4'])): ?>
						<div class="skills">
							<label data-title="<?php echo $sim_com['comp_col_4'] ?>"><span class="truncateTxt"><?php echo $sim_com['comp_col_4'] ?></span></label>
							<input type="text" class="form-control" id="comp_val_4" readonly="readonly" value="<?php echo abs($sim_com['comp_val_4'] - $ques_val4); ?>" />
						</div>
						<?php endif; ?>
						
						<?php if ( ! empty($sim_com['comp_col_5']) &&  ! empty($sim_com['comp_val_5'])): ?>
						<div class="skills">
							<label data-title="<?php echo $sim_com['comp_col_5'] ?>"><span class="truncateTxt"><?php echo $sim_com['comp_col_5'] ?></span></label>
							<input type="text" class="form-control" id="comp_val_5" readonly="readonly" value="<?php echo abs($sim_com['comp_val_5'] - $ques_val5); ?>" />
						</div>
						<?php endif; ?>
					
						<?php if ( ! empty($sim_com['comp_col_6']) &&  ! empty($sim_com['comp_val_6'])): ?>
						<div class="skills">
							<label data-title="<?php echo $sim_com['comp_col_6'] ?>"><span class="truncateTxt"><?php echo $sim_com['comp_col_6'] ?></span></label>
							<input type="text" class="form-control" id="comp_val_6" readonly="readonly" value="<?php echo abs($sim_com['comp_val_6'] - $ques_val6); ?>" />
						</div>
						<?php endif; ?>
					</div>
					<div class="previewQus Sbtn">
						<a href="<?php echo $preview ?>" target="_blank" title="Preview"><img class="img-fluid" src="img/qus_icon/preview.svg"></a>
					</div>
				</div>
				<div id="wrapper">
					<div id="sidebar-wrapper">
					   <div class="topbannericonright"><img class="img-fluid closerightmenuA" src="img/list/right_arrow.svg"></div>
					   <div class="QusTempBanner" id="QusTempBanner">
							<ul class="nav nav-QusTempTab">
								<li class="MCQQ <?php echo ( ! empty($qresult['question_type']) && $qresult['question_type'] == 4) ? 'active' : ''; ?>">
									<a data-toggle="tab" href="#MCQQuestion" data-qtype="4" class="qtype"><img class="img-fluid svg" src="img/qus_icon/MCQ_but_icon.svg" />MCQ</a>
								</li>
								<li class="VideoQ <?php echo ( ! empty($qresult['question_type']) && $qresult['question_type'] == 8) ? 'active' : ''; ?>">
									<a data-toggle="tab" href="#DNDQuestion" data-qtype="8" class="qtype"><img class="img-fluid svg" src="img/list/DAD_icon.svg">DRAG AND DROP</a>
								</li>
								<li class="SwapingQ <?php echo ( ! empty($qresult['question_type']) && $qresult['question_type'] == 7) ? 'active' : ''; ?>">
									<a data-toggle="tab" href="#SwapingQuestion" data-qtype="7" class="qtype"><img class="img-fluid svg" src="img/qus_icon/swiping_but_icon.svg">SWIPING QUESTIONS</a>
								</li>
							</ul>
						</div>
					</div>
					<div class="width12" id="left-wrapper">
						<div class="topbannericon"><img class="img-fluid closeleftmenuA" src="img/list/left_arrow.svg"></div>
						<div class="question-list-banner">
							<!--Start-Load-Question-List-->
							<ul class="question-list" <?php echo (empty($qresult['videoq_cue_point'])) ? 'id="sortable"' : ''; ?>></ul>
							<!--End-->
							<?php if ( ! empty($ques_id)): ?>
							<div class="list-icon list-icon40">
								<ul>
								<?php $videoq_cue_point = ( ! empty($qresult['videoq_cue_point'])) ? $qresult['videoq_cue_point'] : '';
								if (empty($videoq_cue_point)) { ?>
									<li class="AddIconMultiple"><img class="img-fluid" src="img/list/add_question.svg"/></li>
									<li class="DelIcon"><img class="img-fluid" src="img/list/delete_question.svg"/></li>
								<?php } else { ?>
									<li class="DelIcon"><img class="img-fluid" src="img/list/delete_question.svg"/></li>
								<?php } ?>
								</ul>
							</div>
							<?php endif; ?>
						</div>
					</div>
					<?php $ques_type = ( ! empty($qresult['question_type'])) ? $qresult['question_type'] : ''; ?>
					<div id="page-content-wrapper" class="classicTempBnnaR">
						<div class="container-fluid">
							<div class="row">
								<div class="tab-content C_liner_managment">
									<div class="Q-left">
										<a href="#list-toggle" id="list-toggle" class="hideQTmp"><img class="close_panel" src="img/list/questiion_list.svg" /></a>
									</div>
									<?php if (empty($ques_type)): ?>
									<div class="Q-right">
										<a href="#menu-toggle" class="hideQTmp" id="menu-toggle"><img class="close_panel" src="img/list/question_template.svg"></a>
									</div>
									<?php if (empty($ques_id)): ?>
									<div class="widthQ100 classic-template videoTypeSelect">
										<div class="videotab">
											<div class="Radio-box VideobX">
												<label class="radiostyle single-video">
													<input type="radio" id="radioSingleVideo" class="NoCharTab" name="radio-group-video-single-multipe" value="singleVideo">
													<span class="radiomark SelC"></span>
													<div class="DD01 tooltiptop"><img src="img/list/single video.svg"><span class="tooltiptexttop">Single Video</span></div>
												</label>
											</div>
											<div class="Radio-box VideobX">
												<label class="radiostyle multiple-video">
													<input type="radio" id="radioMultipleVideo" class="NoCharTab" name="radio-group-video-single-multipe" value="multipleVideo">
													<span class="radiomark SelC"></span>
													<div class="DD01 tooltiptop"><img src="img/list/multiple video.svg"><span class="tooltiptexttop">Multiple Video</span></div>
												</label>
											</div>
										</div>
									</div>
									<?php 
									endif; endif;
									#---Questions
									$ques_att  = '';
									$qatt_type = '';
									if ( ! empty($qresult['qaudio']) && file_exists($root_path . $qresult['qaudio'])):
										$qatt_type = 'qa';
										$ques_att  = $qresult['qaudio'];
									endif;
									
									#--Questions-Assets
									$ques_assets = '';
									$assets_type = '';
									if ( ! empty($qresult['audio']) && file_exists($root_path . $qresult['audio'])):
										$assets_type = 'a';
										$ques_assets = $qresult['audio'];
									elseif ( ! empty($qresult['video']) && file_exists($root_path . $qresult['video'])):
										$assets_type = 'v';
										$ques_assets = $qresult['video'];
									elseif ( ! empty($qresult['screen']) && file_exists($root_path . $qresult['screen'])):
										$assets_type = 's';
										$ques_assets = $qresult['screen'];
									elseif ( ! empty($qresult['image']) && file_exists($root_path . $qresult['image'])):
										$assets_type = 'i';
										$ques_assets = $qresult['image'];
									elseif ( ! empty($qresult['document']) && file_exists($root_path . $qresult['document'])):
										$assets_type = 'd';
										$ques_assets = $qresult['document'];
									endif; ?>
									<div class="panel panel-video panel-Mult_video" <?php echo ( ! empty($qresult)) ? '' : 'style="display:none"'; ?>>
										<div class="panel-heading panel-Mult_videoT" role="tab" id="questionTwo">
											<a data-toggle="collapse"><div class="video-text"><span>VIDEO</span><i class="fa fa-chevron-down pinvideo"></i><i class="fa fa-chevron-up unpinvideo"></i></div></a>
										</div>
										<div class="panel-collapseV collapse in">
											<div class="panel-body">
												<div class="videoboxBanner Classic_branch_video">
													<div class="videouplaodingbox clearfix">
														<div class="cuepointbtn uploadvideo">
															<div class="quesbox-flex videoflexbox">
																<input type="file" name="videoqfile" id="videoqfile" class="form-control file" data-id="#videoq_media_file" data-assets="video_assets" />
																<input type="text" class="form-control controls simvid-n" disabled placeholder="Select Video File" />
																<input type="hidden" name="videoq_media_file" id="videoq_media_file" value="<?php echo ( ! empty($qresult['videoq_media_file'])) ? $qresult['videoq_media_file'] : ''; ?>"/>
																<?php if (empty($qresult['videoq_media_file'])){ ?>
																<div class="videoflex browsebtn">
																	<button class="browse btn btn-primary video_assets" type="button">Browse</button>
																</div>
																<div class="videoflex browsebtn">
																	<button type="button" name="upload_videoq_file" id="upload_videoq_file" class="btn btn-primary video_assets">Upload</button>
																</div>
																<?php } ?>
															</div>
														</div>
														<div class="VideoTimebox">
															<div class="videoframe video_container" <?php echo ( ! empty($qresult['videoq_media_file'])) ? '' : 'style="display:none"'; ?>>
																<div class="videoflexcrossbtn browsebtn">
																	<div class="video_assets_container" style="display:none;">
																		<a href="javascript:void(0);" data-assets="" data-assets-id="" data-input-id="" data-path="" class="delete_video_assets" title="Delete"><i class="fa fa-times" aria-hidden="true"></i></a>
																	</div>
																</div>
																<video id="videoclip" width="320" height="240" controls <?php echo ( ! empty($qresult['videoq_media_file'])) ? '' : 'style="display:none"'; ?>>
																	<source src="<?php echo ( ! empty($qresult['videoq_media_file'])) ? $path . $qresult['videoq_media_file'] : ''; ?>" type="video/mp4" id="qvideo">
																</video>
																<div class="cue_pt_show"></div>
																<div class="cue_pt">
																	<input type="text" class="timing inputtextWrap cue_points" id="cue_point" placeholder="mm:ss" />
																	<button type="button" class="add_cue videocueAdd">Add Cue Point</button>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="videoq_box"></div>
									</div>

									<!--MCQQuestion-Question-->
									<div id="MCQQuestion" class="Linr_mulVid_baner tab-pane fade <?php echo ($ques_type == 4) ? 'in active' : ''; ?>">
										<div class="container-fluid">
											<div class="widthQ100 classic-template">
												<div class="tab-content">
													<div id="match-Q" class="match-Q tab-pane fade in active">
														<div class="quesbox CQtmp">
															<div class="row">
																<div class="col-sm-12">
																	<div class="critical_div">
																		<h3 class="qusHeading">QUESTION <p class="question-type">(MCQ)</p></h3>
																		<div class="CIRtiCaltext">
																			Critical Question: <input type="checkbox" name="mcq_criticalQ" id="mcq_criticalQ" class="form-control criticalQ" value="1" <?php echo ( ! empty($qresult['critical'])) ? 'checked="checked"' : ''; ?> />
																		</div>
																	</div>
																	<div class="quesbox-flex">
																		<div class="form-group">
																			<input type="text" name="mcq" id="mcq" class="form-control QuesForm inputtextWrap mcq" placeholder="Click here to enter question" value="<?php echo ( ! empty($qresult['questions'])) ? stripslashes($qresult['questions']) : ''; ?>" />
																			 <div class="mcqq_assets_container" <?php echo ( ! empty($ques_att) && $ques_type == 4) ? '' : 'style="display:none;"'; ?>>
																				<div class="assets_data mcqq_assets_data"><a class="view_assets" data-src="<?php echo $path . $ques_att; ?>" href="javascript:;"><?php echo $ques_att; ?></a></div>
																				<a href="javascript:void(0);" data-assets="<?php echo $ques_att; ?>" data-assets-id="mcqq_assets" data-input-id="#mcqq_qaudio,#mcqq_rec_audio" data-path="<?php echo $uploadpath ?>" class="delete_assets" title="Delete"><i class="fa fa-times" aria-hidden="true"></i></a>
																			</div>
																			 <ul class="QueBoxIcon">
																				<li class="dropdown">
																					<button class="btn1 btn-secondary dropdown-toggle mcqq_assets <?php echo ( ! empty($ques_att) && $ques_type == 4) ? 'disabled' : ''; ?>" <?php echo ( ! empty($ques_att) && $ques_type == 4) ? 'disabled="disabled"' : ''; ?> type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img class="img-fluid" src="img/list/audio.svg"></button>
																					<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
																						<a class="dropdown-item">
																							<div class="tooltipLeft uploadicon">
																								<label for="file-input-mcqq-addAudio"><img class="img-fluid" src="img/list/add_audio.svg" /><span class="tooltiptext">Add Audio</span></label>
																								<input id="file-input-mcqq-addAudio" data-id="#mcqq_qaudio" data-assets="mcqq_assets" class="uploadAudioFile" type="file" name="mcqq_Audio" />
																								<input type="hidden" name="mcqq_qaudio" id="mcqq_qaudio" value="<?php echo ($ques_type == 4 && $qatt_type == 'qa') ? $ques_att : ''; ?>" />
																							</div>
																						</a>
																						<a class="dropdown-item">
																							<div class="tooltipLeft">
																								<img class="img-fluid rec-audio" data-input-id="mcqq_rec_audio" data-assets="mcqq_assets" src="img/list/record_audio.svg" />
																								<span class="tooltiptext">Record Audio</span>
																							</div>
																							<input type="hidden" name="mcqq_rec_audio" id="mcqq_rec_audio" />
																						 </a>
																						<a data-toggle="modal" data-target="#mcqqtts" class="dropdown-item">
																							<div class="tooltipLeft">
																								<img class="img-fluid" src="img/list/text_speech.svg" />
																								<span class="tooltiptext">Text to Speech</span>
																							</div>
																						 </a>
																					 </div>
																				</li>
																			</ul>
																		</div>
																	</div>
																	<div class="q-icon-banner clearfix">
																		<ul class="QueBoxIcon QueBoxIcon1">
																			<h5>Add assets</h5>
																			<li class="dropdown">
																				<button class="btn1 btn-secondary dropdown-toggle mcq_assets <?php echo ( ! empty($ques_assets) && $ques_type == 4) ? 'disabled' : ''; ?>" <?php echo ( ! empty($ques_assets) && $ques_type == 4) ? 'disabled="disabled"' : ''; ?> type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img class="img-fluid" src="img/list/audio.svg" /></button>
																				<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
																					<a data-toggle="modal" data-target="#mcqtts" class="dropdown-item">
																						<div class="tooltip1">
																							<img class="img-fluid" src="img/list/text_speech.svg">
																							<span class="tooltiptext">Text to Speech</span>
																						</div>
																					</a>
																					<a class="dropdown-item">
																						<div class="tooltip1 uploadicon">
																							<label for="file-input-mcq-addAudio">
																								<img class="img-fluid" src="img/list/add_audio.svg" />
																								<span class="tooltiptext">Add Audio</span>
																							</label>
																							<input id="file-input-mcq-addAudio" data-id="#mcq_audio" data-assets="mcq_assets" class="uploadAudioFile" type="file" name="mcq_Audio"/>
																							<input type="hidden" name="mcq_audio" id="mcq_audio" value="<?php echo ($ques_type == 4 && $assets_type == 'a') ? $ques_assets : ''; ?>" />
																						</div>
																					</a>
																					<a class="dropdown-item">
																						<div class="tooltip1">
																							<img class="img-fluid rec-audio" data-input-id="mcq_rec_audio" data-assets="mcq_assets" src="img/list/record_audio.svg" />
																							<span class="tooltiptext">Record Audio</span>
																						</div>
																						<input type="hidden" name="mcq_rec_audio" id="mcq_rec_audio" />
																					 </a>
																				</div>
																			</li>
																			<li class="dropdown">
																				<button class="btn1 btn-secondary dropdown-toggle mcq_assets <?php echo ( ! empty($ques_assets) && $ques_type == 4) ? 'disabled' : ''; ?>" <?php echo ( ! empty($ques_assets) && $ques_type == 4) ? 'disabled="disabled"' : ''; ?> type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img class="img-fluid" src="img/list/video.svg"></button>
																				<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
																					<a class="dropdown-item">
																						<div class="tooltip1 uploadicon">
																							<label for="file-input-mcq-addVideo">
																								<img class="img-fluid" src="img/list/add_video.svg" />
																								<span class="tooltiptext">Add Video</span>
																							</label>
																							<input id="file-input-mcq-addVideo" data-id="#mcq_video" data-assets="mcq_assets" class="uploadVideoFile" type="file" name="mcq_Video" />
																							<input type="hidden" name="mcq_video" id="mcq_video" value="<?php echo ($ques_type == 4 && $assets_type == 'v') ? $ques_assets : ''; ?>" />
																						</div>
																					</a>
																					<a class="dropdown-item">
																						<div class="tooltip1">
																							<img class="img-fluid rec-video" data-input-id="mcq_rec_video" data-assets="mcq_assets" src="img/qus_icon/rec_video.svg" />
																							<span class="tooltiptext">Record Video</span>
																						 </div>
																						 <input type="hidden" name="mcq_rec_video" id="mcq_rec_video" />
																					</a>
																					<a class="dropdown-item">
																						<div class="tooltip1">
																							<img class="img-fluid rec-screen" data-input-id="mcq_rec_screen" data-assets="mcq_assets" src="img/list/screen_record.svg" />
																							<span class="tooltiptext">Record Screen</span>
																						 </div>
																						 <input type="hidden" name="mcq_rec_screen" id="mcq_rec_screen" value="<?php echo ($ques_type == 4 && $assets_type == 's') ? $ques_assets : ''; ?>" />
																					</a>
																				</div>
																			</li>
																			<li>
																				<div class="tooltip uploadicon mcq_assets <?php echo ( ! empty($ques_assets) && $ques_type == 4) ? 'disabled' : ''; ?>" <?php echo ( ! empty($ques_assets) && $ques_type == 4) ? 'disabled="disabled"' : ''; ?>>
																					<label for="file-input-mcq-addImg">
																						<img class="img-fluid" src="img/list/image.svg" />
																						<span class="tooltiptext">Add Image</span>
																					</label>
																					<input id="file-input-mcq-addImg" data-id="#mcq_img" data-assets="mcq_assets" class="uploadImgFile mcq_assets" type="file" name="mcq_Img" <?php echo ( ! empty($ques_assets) && $ques_type == 4) ? 'disabled="disabled"' : ''; ?>/>
																					<input type="hidden" name="mcq_img" id="mcq_img" value="<?php echo ($ques_type == 4 && $assets_type == 'i') ? $ques_assets : ''; ?>" />
																				</div>
																			</li>
																			<li>
																				<div class="tooltip uploadicon mcq_assets <?php echo ( ! empty($ques_assets) && $ques_type == 4) ? 'disabled' : ''; ?>" <?php echo ( ! empty($ques_assets) && $ques_type == 4) ? 'disabled="disabled"' : ''; ?>>
																					<label for="file-input-mcq-addDoc">
																						<img class="img-fluid" src="img/list/add_document.svg" />
																						<span class="tooltiptext">Add Document</span>
																					</label>
																					<input id="file-input-mcq-addDoc" data-id="#mcq_doc" data-assets="mcq_assets" class="uploadDocFile mcq_assets" type="file" name="mcq_Doc" <?php echo ( ! empty($ques_assets) && $ques_type == 4) ? 'disabled="disabled"' : ''; ?> />
																					<input type="hidden" name="mcq_doc" id="mcq_doc" value="<?php echo ($ques_type == 4 && $assets_type == 'd') ? $ques_assets : ''; ?>" />
																				</div>
																			</li>
																		</ul>
																		<div class="Ques-comp">
																			<h5>Add Competency score</h5>
																			<?php if ( ! empty($sim_com['comp_col_1']) && ! empty($sim_com['comp_val_1'])): ?>
																			<div class="form-group QusScore">
																				<input type="text" class="form-control mcq" name="mcq_ques_val_1" id="mcq_ques_val_1" onkeypress="return isNumberKey(event);" value="<?php echo ( ! empty($qresult['ques_val_1'])) ? $qresult['ques_val_1'] : ''; ?>" />
																			</div>
																			<input type="hidden" class="form-control" id="mcq_ques_val_1h" value="<?php echo ( ! empty($qresult['ques_val_1'])) ? $qresult['ques_val_1'] : ''; ?>" />
																			<?php else: ?>
																			<input type="hidden" class="form-control" name="mcq_ques_val_1" value="0" />
																			<?php endif; ?>
																			
																			<?php if ( ! empty($sim_com['comp_col_2']) && ! empty($sim_com['comp_val_2'])): ?>
																			<div class="form-group QusScore">
																				<input type="text" class="form-control mcq" name="mcq_ques_val_2" id="mcq_ques_val_2" onkeypress="return isNumberKey(event);" value="<?php echo ( ! empty($qresult['ques_val_2'])) ? $qresult['ques_val_2'] : ''; ?>" />
																			</div>
																			<input type="hidden" class="form-control" id="mcq_ques_val_2h" value="<?php echo ( ! empty($qresult['ques_val_2'])) ? $qresult['ques_val_2'] : ''; ?>" />
																			<?php else: ?>
																			<input type="hidden" class="form-control" name="mcq_ques_val_2" value="0" />
																			<?php endif; ?>
																			
																			<?php if ( ! empty($sim_com['comp_col_3']) && ! empty($sim_com['comp_val_3'])): ?>
																			<div class="form-group QusScore">
																				<input type="text" class="form-control mcq" name="mcq_ques_val_3" id="mcq_ques_val_3" onkeypress="return isNumberKey(event);" value="<?php echo ( ! empty($qresult['ques_val_3'])) ? $qresult['ques_val_3'] : ''; ?>" />
																			</div>
																			<input type="hidden" class="form-control" id="mcq_ques_val_3h" value="<?php echo ( ! empty($qresult['ques_val_3'])) ? $qresult['ques_val_3'] : ''; ?>" />
																			<?php else: ?>
																			<input type="hidden" class="form-control" name="mcq_ques_val_3" value="0" />
																			<?php endif; ?>
																			
																			<?php if ( ! empty($sim_com['comp_col_4']) && ! empty($sim_com['comp_val_4'])): ?>
																			<div class="form-group QusScore">
																				<input type="text" class="form-control mcq" name="mcq_ques_val_4" id="mcq_ques_val_4" onkeypress="return isNumberKey(event);" value="<?php echo ( ! empty($qresult['ques_val_4'])) ? $qresult['ques_val_4'] : ''; ?>" />
																			</div>
																			<input type="hidden" class="form-control" id="mcq_ques_val_4h" value="<?php echo ( ! empty($qresult['ques_val_4'])) ? $qresult['ques_val_4'] : ''; ?>" />
																			<?php else: ?>
																			<input type="hidden" class="form-control" name="mcq_ques_val_4" value="0" />
																			<?php endif; ?>
																			
																			<?php if ( ! empty($sim_com['comp_col_5']) && ! empty($sim_com['comp_val_5'])): ?>
																			<div class="form-group QusScore">
																				<input type="text" class="form-control mcq" name="mcq_ques_val_5" id="mcq_ques_val_5" onkeypress="return isNumberKey(event);" value="<?php echo ( ! empty($qresult['ques_val_5'])) ? $qresult['ques_val_5'] : ''; ?>" />
																			</div>
																			<input type="hidden" class="form-control" id="mcq_ques_val_5h" value="<?php echo ( ! empty($qresult['ques_val_5'])) ? $qresult['ques_val_5'] : ''; ?>" />
																			<?php else: ?>
																			<input type="hidden" class="form-control" name="mcq_ques_val_5h" value="0" />
																			<?php endif; ?>
																			
																			<?php if ( ! empty($sim_com['comp_col_6']) && ! empty($sim_com['comp_val_6'])): ?>
																			<div class="form-group QusScore">
																				<input type="text" class="form-control mcq" name="mcq_ques_val_6" id="mcq_ques_val_6" onkeypress="return isNumberKey(event);" value="<?php echo ( ! empty($qresult['ques_val_6'])) ? $qresult['ques_val_6'] : ''; ?>" />
																			</div>
																			<input type="hidden" class="form-control" id="mcq_ques_val_6h" value="<?php echo ( ! empty($qresult['ques_val_6'])) ? $qresult['ques_val_6'] : ''; ?>" />
																			<?php else: ?>
																			<input type="hidden" class="form-control" name="mcq_ques_val_6" value="0" />
																			<?php endif; ?>
																		</div>
																	</div>
																	<div class="mcq_assets_container" <?php echo ( ! empty($ques_assets) && $ques_type == 4) ? '' : 'style="display:none;"'; ?>>
																		<div class="assets_data mcq_assets_data"><a class="view_assets" data-src="<?php echo $path . $ques_assets; ?>" href="javascript:;"><?php echo $ques_assets; ?></a></div>
																		<a href="javascript:void(0);" data-assets="<?php echo $ques_assets ?>" data-assets-id="mcq_assets" data-input-id="#mcq_audio,#mcq_rec_audio,#mcq_video,#mcq_rec_video,#mcq_rec_screen,#mcq_img,#mcq_doc" data-path="<?php echo $uploadpath ?>" class="delete_assets" title="Delete"><i class="fa fa-times" aria-hidden="true"></i></a>
																	</div>
																</div>
															</div>
														</div>
														<div class="form-popup draggable Ques-pop-style modal" id="mcqqtts">
															<div class="popheading">Insert Text to Speech</div>
															<div class="textarea">
																<textarea type="text" class="form-control1 inputtextWrap" name="mcqq_text_to_speech" id="mcqq_text_to_speech"><?php echo ($ques_type == 4 && ! empty($qresult['qspeech_text'])) ? stripslashes($qresult['qspeech_text']) : ''; ?></textarea>
															</div>
															<div class="modal-footer">
																<button type="button" class="btn1 submitbtn1" data-dismiss="modal">Insert</button>
																<button type="button" class="btn1 submitbtn1" onclick="clearData('mcqq_text_to_speech');">Clear</button>
																<button type="button" class="btn1 submitbtn1" data-dismiss="modal">Cancel</button>
															</div>
														</div>
														<div class="form-popup draggable Ques-pop-style modal" id="mcqtts">
															<div class="popheading">Insert Text to Speech</div>
															<div class="textarea">
																<textarea type="text" class="form-control1 inputtextWrap" name="mcq_text_to_speech" id="mcq_text_to_speech"><?php echo ($ques_type == 4 && ! empty($qresult['speech_text'])) ? stripslashes($qresult['speech_text']) : ''; ?></textarea>
															</div>
															<div class="modal-footer">
																<button type="button" class="btn1 submitbtn1" data-dismiss="modal">Insert</button>
																<button type="button" class="btn1 submitbtn1" onclick="clearData('mcq_text_to_speech');">Clear</button>
																<button type="button" class="btn1 submitbtn1" data-dismiss="modal">Cancel</button>
															</div>
														</div>
														<div class="Ansbox LinerClassicTMPAns">
															<div class="row">
																<div class="col-sm-12">
																	<div class="col-sm-6">
																		<h3 class="AnsHeading">CHOICE</h3>
																	</div>
																	<div class="col-sm-6">
																		<div class="Check-box suffel classicCH">
																			<div class="tooltiptop"><span class="tooltiptext">Shuffle ON</span></div>
																			<label class="checkstyle">
																				<input type="checkbox" name="mcq_shuffle" class="suffleCheck" value="1" <?php echo ( ! empty($qresult['shuffle'])) ? 'checked="checked"' : ''; ?> />
																				<span class="checkmark"></span><span class="shuffleT"> Shuffle Option</span>
																			</label>
																		</div>
																	</div>
																	<div class="ApendOption-banner">
																		<div class="MCQApendOption-box">
																			<?php 
																			$mcq_sql = "SELECT a.*, f.* FROM answer_tbl a LEFT JOIN feedback_tbl f ON f.answer_id = a.answer_id WHERE a.question_id = '". $qresult['question_id'] ."'";
																			$mcq_res = $db->prepare($mcq_sql); $mcq_res->execute();
																			if (($mcq_res->rowCount() > 0) && (!empty($qresult['question_id']))): $mcqi = 1;
																			foreach ($mcq_res->fetchAll(PDO::FETCH_ASSOC) as $mcq_res_row): ?>
																			<div class="MCQRadio">
																				<input type="hidden" name="updateMCQAnswerid[]" value="<?php echo $mcq_res_row['answer_id'] ?>" />
																				<div class="Radio-box classtmpR">
																					<label class="radiostyle">
																						<input type="radio" name="mcq_true_option" value="<?php echo $mcqi ?>" <?php echo ($mcqi == $qresult['true_options']) ? 'checked="checked"' : ''; ?> /><span class="radiomark"></span>
																					</label>
																					<div class="col-sm-12 mcwidth">
																						<div class="choicebox">
																							<div class="form-group">
																								<input type="text" name="mcq_option[]" id="mcq_option" class="form-control inputtextWrap mcq" placeholder="Option text goes here" value="<?php echo stripslashes($mcq_res_row['choice_option']); ?>" />
																							</div>
																						</div>
																					</div>
																					<?php if ($mcqi == 1): ?>
																					<div class="AddAns-Option"><img class="img-fluid MCQadd" src="img/list/add_field.svg" /></div>
																					<?php else: ?>
																					<div class="AddAns-Option removeOption" data-remove-answer-id="<?php echo $mcq_res_row['answer_id'] ?>"><img class="img-fluid" src="img/list/delete_field.svg" /></div>
																					<?php endif; ?>
																				</div>
																				<div class="classicQ classicFEED">
																					<div class="q-icon-banner clearfix">
																						<div class="Ques-comp">
																							<h5>Add Competency score</h5>
																							<?php if ( ! empty($sim_com['comp_col_1']) && ! empty($sim_com['comp_val_1'])): ?>
																							<div class="form-group QusScore">
																								<input type="text" class="form-control score mcq" name="mcq_ans_val1[]" id="mcq_ans_val1" onkeypress="return isNumberKey(event);" value="<?php echo $mcq_res_row['ans_val1'] ?>"  />
																							</div>
																							<?php endif; if ( ! empty($sim_com['comp_col_2']) && ! empty($sim_com['comp_val_2'])): ?>
																							<div class="form-group QusScore">
																								<input type="text" class="form-control score mcq" name="mcq_ans_val2[]" id="mcq_ans_val2" onkeypress="return isNumberKey(event);" value="<?php echo $mcq_res_row['ans_val2'] ?>"  />
																							</div>
																							<?php endif; if ( ! empty($sim_com['comp_col_3']) && ! empty($sim_com['comp_val_3'])): ?>
																							<div class="form-group QusScore">
																								<input type="text" class="form-control score mcq" name="mcq_ans_val3[]" id="mcq_ans_val3" onkeypress="return isNumberKey(event);" value="<?php echo $mcq_res_row['ans_val3'] ?>"  />
																							</div>
																							<?php endif; if ( ! empty($sim_com['comp_col_4']) && ! empty($sim_com['comp_val_4'])): ?>
																							<div class="form-group QusScore">
																								<input type="text" class="form-control score mcq" name="mcq_ans_val4[]" id="mcq_ans_val4" onkeypress="return isNumberKey(event);" value="<?php echo $mcq_res_row['ans_val4'] ?>"  />
																							</div>
																							<?php endif; if ( ! empty($sim_com['comp_col_5']) && ! empty($sim_com['comp_val_5'])): ?>
																							<div class="form-group QusScore">
																								<input type="text" class="form-control score mcq" name="mcq_ans_val5[]" id="mcq_ans_val5" onkeypress="return isNumberKey(event);" value="<?php echo $mcq_res_row['ans_val5'] ?>"  />
																							</div>
																							<?php endif; if ( ! empty($sim_com['comp_col_6']) && ! empty($sim_com['comp_val_6'])): ?>
																							<div class="form-group QusScore">
																								<input type="text" class="form-control score mcq" name="mcq_ans_val6[]" id="mcq_ans_val6" onkeypress="return isNumberKey(event);" value="<?php echo $mcq_res_row['ans_val6'] ?>"  />
																							</div>
																							<?php endif; ?>
																						</div>
																					</div>
																					<?php 
																					#----Feedback----
																					$mcq_feed_assets = '';
																					$mcq_feed_assets_type = '';
																					if ( ! empty($mcq_res_row['feed_audio']) && file_exists($root_path . $mcq_res_row['feed_audio'])):
																						$mcq_feed_assets_type = 'fa';
																						$mcq_feed_assets = $mcq_res_row['feed_audio'];
																					elseif ( ! empty($mcq_res_row['feed_video']) && file_exists($root_path . $mcq_res_row['feed_video'])):
																						$mcq_feed_assets_type = 'fv';
																						$mcq_feed_assets = $mcq_res_row['feed_video'];
																					elseif ( ! empty($mcq_res_row['feed_screen']) && file_exists($root_path . $mcq_res_row['feed_screen'])):
																						$mcq_feed_assets_type = 'fs';
																						$mcq_feed_assets = $mcq_res_row['feed_screen'];
																					elseif ( ! empty($mcq_res_row['feed_image']) && file_exists($root_path . $mcq_res_row['feed_image'])):
																						$mcq_feed_assets_type = 'fi';
																						$mcq_feed_assets = $mcq_res_row['feed_image'];
																					elseif ( ! empty($mcq_res_row['feed_document']) && file_exists($root_path . $mcq_res_row['feed_document'])):
																						$mcq_feed_assets_type = 'fd';
																						$mcq_feed_assets = $mcq_res_row['feed_document'];
																					endif; ?>
																					<div class="form-group feed">
																						<h5>Feedback</h5>
																						<textarea class="form-control FeedForm inputtextWrap mcq" name="mcq_cfeedback[]" id="mcq_cfeedback" placeholder="Feedback text goes here."><?php echo $mcq_res_row['feedback'] ?></textarea>
																						<div class="mcqf_assets_container_<?php echo $mcqi ?>" <?php echo ( ! empty($mcq_feed_assets) && $ques_type == 4) ? '' : 'style="display:none;"'; ?>>
																							<div class="assets_data mcqf_assets_data_<?php echo $mcqi ?>"><a class="view_assets" data-src="<?php echo $path . $mcq_feed_assets; ?>" href="javascript:;"><?php echo $mcq_feed_assets; ?></a></div>
																							<a href="javascript:void(0);" data-assets="<?php echo $mcq_feed_assets ?>" data-assets-id="mcqf_assets_<?php echo $mcqi ?>" data-input-id="#mcq_feedback_audio_<?php echo $mcqi ?>,#mcq_feedback_rec_audio_<?php echo $mcqi ?>,#mcq_feedback_video_<?php echo $mcqi ?>,#mcq_feedback_rec_video_<?php echo $mcqi ?>,#mcq_feedback_rec_screen_<?php echo $mcqi ?>,#mcq_feedback_img_<?php echo $mcqi ?>,#mcq_feedback_doc_<?php echo $mcqi ?>" data-path="<?php echo $uploadpath ?>" class="delete_assets" title="Delete"><i class="fa fa-times" aria-hidden="true"></i></a>
																						</div>
																						<ul class="QueBoxIcon feedQ">
																							<h5>Add assets</h5>
																							<li class="dropdown">
																								<button class="btn1 btn-secondary dropdown-toggle mcqf_assets_<?php echo $mcqi ?> <?php echo ( ! empty($mcq_feed_assets) && $ques_type == 4) ? 'disabled' : ''; ?>" <?php echo ( ! empty($mcq_feed_assets) && $ques_type == 4) ? 'disabled="disabled"' : ''; ?> type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img class="img-fluid" src="img/list/audio.svg"></button>
																								<div class="dropdown-menu buttom" aria-labelledby="dropdownMenuButton">
																									<a data-toggle="modal" data-target="#mcqfctts<?php echo $mcqi ?>" class="dropdown-item">
																										<div class="tooltip1 tooltip<?php echo $mcqi ?>">
																											<img class="img-fluid" src="img/list/text_speech.svg" />
																											<span class="tooltiptext">Text to Speech</span>
																										</div>
																									</a>
																									<a class="dropdown-item">
																										<div class="tooltip1 uploadicon">
																											<label for="file-input-mcq-feedback-addAudio-<?php echo $mcqi ?>">
																												<img class="img-fluid" src="img/list/add_audio.svg" />
																												<span class="tooltiptext">Add Audio</span>
																											</label>
																											<input id="file-input-mcq-feedback-addAudio-<?php echo $mcqi ?>" data-id="#mcq_feedback_audio_<?php echo $mcqi ?>" data-assets="mcqf_assets_<?php echo $mcqi ?>" class="uploadAudioFileVideoTemp" type="file" name="mcq_feedback_Audio_<?php echo $mcqi ?>" />
																											<input type="hidden" name="mcq_feedback_audio[]" id="mcq_feedback_audio_<?php echo $mcqi ?>" value="<?php echo ($ques_type == 4 && $mcq_feed_assets_type == 'fa') ? $mcq_feed_assets : ''; ?>" />
																										</div>
																									</a>
																									<a class="dropdown-item">
																										<div class="tooltip1">
																											<img class="img-fluid rec-audio-videoq" data-input-id="mcq_feedback_rec_audio_<?php echo $mcqi ?>" data-assets="mcqf_assets_<?php echo $mcqi ?>" src="img/list/record_audio.svg" />
																											<span class="tooltiptext">Record Audio</span>
																										</div>
																										<input type="hidden" name="mcq_feedback_rec_audio[]" id="mcq_feedback_rec_audio_<?php echo $mcqi ?>" />
																									</a>
																								</div>
																							</li>
																							<li class="dropdown">
																								<button class="btn1 btn-secondary dropdown-toggle mcqf_assets_<?php echo $mcqi ?> <?php echo ( ! empty($mcq_feed_assets) && $ques_type == 4) ? 'disabled' : ''; ?>" <?php echo ( ! empty($mcq_feed_assets) && $ques_type == 4) ? 'disabled="disabled"' : ''; ?> type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img class="img-fluid" src="img/list/video.svg"></button>
																								<div class="dropdown-menu buttom" aria-labelledby="dropdownMenuButton">
																									<a class="dropdown-item">
																										<div class="tooltip1 uploadicon">
																											<label for="file-input-mcq-feedback-addVideo-<?php echo $mcqi ?>">
																												<img class="img-fluid" src="img/list/add_video.svg">
																												<span class="tooltiptext">Add Video</span>
																											</label>
																											<input id="file-input-mcq-feedback-addVideo-<?php echo $mcqi ?>" data-id="#mcq_feedback_video_<?php echo $mcqi ?>" data-assets="mcqf_assets_<?php echo $mcqi ?>" class="uploadFileVideoTemp" type="file" name="mcq_feedback_Video_<?php echo $mcqi ?>" />
																											<input type="hidden" name="mcq_feedback_video[]" id="mcq_feedback_video_<?php echo $mcqi ?>" value="<?php echo ($ques_type == 4 && $mcq_feed_assets_type == 'fv') ? $mcq_feed_assets : ''; ?>" />
																										</div>
																									</a>
																									<a class="dropdown-item">
																										<div class="tooltip1">
																											<img class="img-fluid rec-video-videoq" data-input-id="mcq_feedback_rec_video_<?php echo $mcqi ?>" data-assets="mcqf_assets_<?php echo $mcqi ?>" src="img/qus_icon/rec_video.svg">
																											<span class="tooltiptext">Record Video</span>
																										</div>
																										<input type="hidden" name="mcq_feedback_rec_video[]" id="mcq_feedback_rec_video_<?php echo $mcqi ?>" />
																									</a>
																									<a class="dropdown-item">
																										<div class="tooltip1">
																											<img class="img-fluid rec-screen-videoq" data-input-id="mcq_feedback_rec_screen_<?php echo $mcqi ?>" data-assets="mcqf_assets_<?php echo $mcqi ?>" src="img/list/screen_record.svg">
																											<span class="tooltiptext">Record Screen</span>
																										</div>
																										<input type="hidden" name="mcq_feedback_rec_screen[]" id="mcq_feedback_rec_screen_<?php echo $mcqi ?>" value="<?php echo ($ques_type == 4 && $mcq_feed_assets_type == 'fs') ? $mcq_feed_assets : ''; ?>" />
																									</a>
																								</div>
																							</li>
																							<li>
																								<div class="tooltip uploadicon mcqf_assets_<?php echo $mcqi ?> <?php echo ( ! empty($mcq_feed_assets) && $ques_type == 4) ? 'disabled' : ''; ?>" <?php echo ( ! empty($mcq_feed_assets) && $ques_type == 4) ? 'disabled="disabled"' : ''; ?>>
																									<label for="file-input-mcq-feedback-addImg-<?php echo $mcqi ?>">
																										<img class="img-fluid" src="img/list/image.svg">
																										<span class="tooltiptext">Add Image</span>
																									</label>
																									<input id="file-input-mcq-feedback-addImg-<?php echo $mcqi ?>" data-id="#mcq_feedback_img_<?php echo $mcqi ?>" data-assets="mcqf_assets_<?php echo $mcqi ?>" class="uploadImgFileVideo mcqf_assets_<?php echo $mcqi ?>" type="file" name="mcq_feedback_Img_<?php echo $mcqi ?>" <?php echo ( ! empty($mcq_feed_assets) && $ques_type == 4) ? 'disabled="disabled"' : ''; ?> />
																									<input type="hidden" name="mcq_feedback_img[]" id="mcq_feedback_img_<?php echo $mcqi ?>" value="<?php echo ($ques_type == 4 && $mcq_feed_assets_type == 'fi') ? $mcq_feed_assets : ''; ?>" />
																								</div>
																							</li>
																							<li>
																								<div class="tooltip uploadicon mcqf_assets_<?php echo $mcqi ?> <?php echo ( ! empty($mcq_feed_assets) && $ques_type == 4) ? 'disabled' : ''; ?>" <?php echo ( ! empty($mcq_feed_assets) && $ques_type == 4) ? 'disabled="disabled"' : ''; ?>>
																									<label for="file-input-mcq-feedback-addDoc-<?php echo $mcqi ?>">
																										<img class="img-fluid" src="img/list/add_document.svg" />
																										<span class="tooltiptext">Add Document</span>
																									</label>
																									<input id="file-input-mcq-feedback-addDoc-<?php echo $mcqi ?>" data-id="#mcq_feedback_doc_<?php echo $mcqi ?>" data-assets="mcqf_assets_<?php echo $mcqi ?>" class="uploadDocFileVideo mcqf_assets_<?php echo $mcqi ?>" type="file" name="mcq_feedback_Doc_<?php echo $mcqi ?>" <?php echo ( ! empty($mcq_feed_assets) && $ques_type == 4) ? 'disabled="disabled"' : ''; ?> />
																									<input type="hidden" name="mcq_feedback_doc[]" id="mcq_feedback_doc_<?php echo $mcqi ?>" value="<?php echo ($ques_type == 4 && $mcq_feed_assets_type == 'fd') ? $mcq_feed_assets : ''; ?>" />
																								</div>
																							</li>
																						</ul>
																						<div class="form-popup draggable Ques-pop-style modal" id="mcqfctts<?php echo $mcqi ?>">
																							<div class="popheading">Insert Text to Speech</div>
																							<div class="textarea">
																								<textarea type="text" class="form-control1 inputtextWrap" name="mcq_feedback_text_to_speech[]" id="mcq_feedback_text_to_speech_<?php echo $mcqi ?>"><?php echo ($ques_type == 4 && ! empty($mcq_res_row['feed_speech_text'])) ? stripslashes($mcq_res_row['feed_speech_text']) : ''; ?></textarea>
																							</div>
																							<div class="modal-footer">
																								<button type="button" class="btn1 submitbtn1" data-dismiss="modal">Insert</button>
																								<button type="button" class="btn1 submitbtn1" onclick="clearData('mcq_feedback_text_to_speech_<?php echo $mcqi ?>');">Clear</button>
																								<button type="button" class="btn1 submitbtn1" data-dismiss="modal">Cancel</button>
																							</div>
																						</div>
																					</div>
																				</div>
																			</div>
																			<?php $mcqi++; endforeach; else: ?>
																			<div class="MCQRadio">
																				<div class="Radio-box classtmpR">
																					<label class="radiostyle">
																						<input type="radio" name="mcq_true_option" value="1" checked /><span class="radiomark"></span>
																					</label>
																					<div class="col-sm-12 mcwidth">
																						<div class="choicebox">
																							<div class="form-group">
																								<input type="text" name="mcq_option[]" id="mcq_option" class="form-control inputtextWrap mcq" placeholder="Option text goes here" />
																							</div>
																						</div>
																					</div>
																					<div class="AddAns-Option">
																						<img class="img-fluid MCQadd" src="img/list/add_field.svg" />
																					</div>
																				</div>
																				<div class="classicQ classicFEED">
																					<div class="q-icon-banner clearfix">
																						<div class="Ques-comp">
																							<h5>Add Competency score</h5>
																							<?php if ( ! empty($sim_com['comp_col_1']) && ! empty($sim_com['comp_val_1'])): ?>
																							<div class="form-group QusScore">
																								<input type="text" class="form-control score mcq" name="mcq_ans_val1[]" id="mcq_ans_val1" onkeypress="return isNumberKey(event);" />
																							</div>
																							<?php endif; if ( ! empty($sim_com['comp_col_2']) &&  ! empty($sim_com['comp_val_2'])): ?>
																							<div class="form-group QusScore">
																								<input type="text" class="form-control score mcq" name="mcq_ans_val2[]" id="mcq_ans_val2" onkeypress="return isNumberKey(event);" />
																							</div>
																							<?php endif; if ( ! empty($sim_com['comp_col_3']) && ! empty($sim_com['comp_val_3'])): ?>
																							<div class="form-group QusScore">
																								<input type="text" class="form-control score mcq" name="mcq_ans_val3[]" id="mcq_ans_val3" onkeypress="return isNumberKey(event);" />
																							</div>
																							<?php endif; if ( ! empty($sim_com['comp_col_4']) && ! empty($sim_com['comp_val_4'])): ?>
																							<div class="form-group QusScore">
																								<input type="text" class="form-control score mcq" name="mcq_ans_val4[]" id="mcq_ans_val4" onkeypress="return isNumberKey(event);" />
																							</div>
																							<?php endif; if ( ! empty($sim_com['comp_col_5']) && ! empty($sim_com['comp_val_5'])): ?>
																							<div class="form-group QusScore">
																								<input type="text" class="form-control score mcq" name="mcq_ans_val5[]" id="mcq_ans_val5" onkeypress="return isNumberKey(event);" />
																							</div>
																							<?php endif; if ( ! empty($sim_com['comp_col_6']) && ! empty($sim_com['comp_val_6'])): ?>
																							<div class="form-group QusScore">
																								<input type="text" class="form-control score mcq" name="mcq_ans_val6[]" id="mcq_ans_val6" onkeypress="return isNumberKey(event);" />
																							</div>
																							<?php endif; ?>
																						</div>
																					</div>
																					<div class="form-group feed">
																						<h5>Feedback</h5>
																						<textarea class="form-control FeedForm inputtextWrap mcq" name="mcq_cfeedback[]" id="mcq_cfeedback" placeholder="Feedback text goes here."></textarea>
																						<div class="mcqf_assets_container_1" style="display:none;">
																							<div class="assets_data mcqf_assets_data_1"><a class="view_assets" data-src="" href="javascript:;"></a></div>
																							<a href="javascript:void(0);" data-assets="" data-assets-id="" data-input-id="" data-path="<?php echo $uploadpath ?>" class="delete_assets" title="Delete"><i class="fa fa-times" aria-hidden="true"></i></a>
																						</div>
																						<ul class="QueBoxIcon feedQ">
																							<h5>Add assets</h5>
																							<li class="dropdown">
																								<button class="btn1 btn-secondary dropdown-toggle mcqf_assets_1" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img class="img-fluid" src="img/list/audio.svg"></button>
																								<div class="dropdown-menu buttom" aria-labelledby="dropdownMenuButton">
																									<a data-toggle="modal" data-target="#mcqfctts1" class="dropdown-item">
																										<div class="tooltip1">
																											<img class="img-fluid" src="img/list/text_speech.svg" />
																											<span class="tooltiptext">Text to Speech</span>
																										</div>
																									</a>
																									<a class="dropdown-item">
																										<div class="tooltip1 uploadicon">
																											<label for="file-input-mcq-feedback-addAudio-1">
																												<img class="img-fluid" src="img/list/add_audio.svg" />
																												<span class="tooltiptext">Add Audio</span>
																											</label>
																											<input id="file-input-mcq-feedback-addAudio-1" data-id="#mcq_feedback_audio_1" data-assets="mcqf_assets_1" class="uploadAudioFileVideoTemp" type="file" name="mcq_feedback_Audio_1" />
																											<input type="hidden" name="mcq_feedback_audio[]" id="mcq_feedback_audio_1" />
																										</div>
																									</a>
																									<a class="dropdown-item">
																										<div class="tooltip1">
																											<img class="img-fluid rec-audio-videoq" data-input-id="mcq_feedback_rec_audio_1" data-assets="mcqf_assets_1" src="img/list/record_audio.svg" />
																											<span class="tooltiptext">Record Audio</span>
																										</div>
																										<input type="hidden" name="mcq_feedback_rec_audio[]" id="mcq_feedback_rec_audio_1" />
																									</a>
																								</div>
																							</li>
																							<li class="dropdown">
																								<button class="btn1 btn-secondary dropdown-toggle mcqf_assets_1" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img class="img-fluid" src="img/list/video.svg"></button>
																								<div class="dropdown-menu buttom" aria-labelledby="dropdownMenuButton">
																									<a class="dropdown-item">
																										<div class="tooltip1 uploadicon">
																											<label for="file-input-mcq-feedback-addVideo-1">
																												<img class="img-fluid" src="img/list/add_video.svg">
																												<span class="tooltiptext">Add Video</span>
																											</label>
																											<input id="file-input-mcq-feedback-addVideo-1" data-id="#mcq_feedback_video_1" data-assets="mcqf_assets_1" class="uploadFileVideoTemp" type="file" name="mcq_feedback_Video_1" />
																											<input type="hidden" name="mcq_feedback_video[]" id="mcq_feedback_video_1" />
																										</div>
																									</a>
																									<a class="dropdown-item">
																										<div class="tooltip1">
																											<img class="img-fluid rec-video-videoq" data-input-id="mcq_feedback_rec_video_1" data-assets="mcqf_assets_1" src="img/qus_icon/rec_video.svg">
																											<span class="tooltiptext">Record Video</span>
																										</div>
																										<input type="hidden" name="mcq_feedback_rec_video[]" id="mcq_feedback_rec_video_1" />
																									</a>
																									<a class="dropdown-item">
																										<div class="tooltip1">
																											<img class="img-fluid rec-screen-videoq" data-input-id="mcq_feedback_rec_screen_1" data-assets="mcqf_assets_1" src="img/list/screen_record.svg">
																											<span class="tooltiptext">Record Screen</span>
																										</div>
																										<input type="hidden" name="mcq_feedback_rec_screen[]" id="mcq_feedback_rec_screen_1" />
																									</a>
																								</div>
																							</li>
																							<li>
																								<div class="tooltip uploadicon mcqf_assets_1">
																									<label for="file-input-mcq-feedback-addImg-1">
																										<img class="img-fluid" src="img/list/image.svg">
																										<span class="tooltiptext">Add Image</span>
																									</label>
																									<input id="file-input-mcq-feedback-addImg-1" data-id="#mcq_feedback_img_1" data-assets="mcqf_assets_1" class="uploadImgFileVideo mcqf_assets_1" type="file" name="mcq_feedback_Img_1" />
																									<input type="hidden" name="mcq_feedback_img[]" id="mcq_feedback_img_1" />
																								</div>
																							</li>
																							<li>
																								<div class="tooltip uploadicon mcqf_assets_1">
																									<label for="file-input-mcq-feedback-addDoc-1">
																										<img class="img-fluid" src="img/list/add_document.svg" />
																										<span class="tooltiptext">Add Document</span>
																									</label>
																									<input id="file-input-mcq-feedback-addDoc-1" data-id="#mcq_feedback_doc_1" data-assets="mcqf_assets_1" class="uploadDocFileVideo mcqf_assets_1" type="file" name="mcq_feedback_Doc_1" />
																									<input type="hidden" name="mcq_feedback_doc[]" id="mcq_feedback_doc_1" />
																								</div>
																							</li>
																						</ul>
																						<div class="form-popup draggable Ques-pop-style modal" id="mcqfctts1">
																							<div class="popheading">Insert Text to Speech</div>
																							<div class="textarea">
																								<textarea type="text" class="form-control1 inputtextWrap" name="mcq_feedback_text_to_speech[]" id="mcq_feedback_text_to_speech_1"></textarea>
																							</div>
																							<div class="modal-footer">
																								<button type="button" class="btn1 submitbtn1" data-dismiss="modal">Insert</button>
																								<button type="button" class="btn1 submitbtn1" onclick="clearData('mcq_feedback_text_to_speech_1');">Clear</button>
																								<button type="button" class="btn1 submitbtn1" data-dismiss="modal">Cancel</button>
																							</div>
																						</div>
																					</div>
																				</div>
																			</div>
																			<?php endif; ?>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>

									<!--DND-Question-->
									<div id="DNDQuestion" class="Linr_mulVid_baner tab-pane fade <?php echo ($ques_type == 8) ? 'in active' : ''; ?>">
										<div class="container-fluid">
											<div class="widthQ100 classic-template">
												<div class="tab-content">
													<div id="match-Q" class="match-Q tab-pane fade in active">
														<div class="quesbox CQtmp">
															<div class="row">
																<div class="col-sm-12">
																	<div class="critical_div">
																		<h3 class="qusHeading">QUESTION <p class="question-type">(Drag and Drop)</p></h3>
																		<div class="CIRtiCaltext">
																			Critical Question: <input type="checkbox" name="dnd_criticalQ" id="dnd_criticalQ" class="form-control criticalQ" value="1" <?php echo ( ! empty($qresult['critical'])) ? 'checked="checked"' : ''; ?> />
																		</div>
																	</div>
																	<div class="quesbox-flex">
																		<div class="form-group">
																			<input type="text" name="ddq" id="ddq" class="form-control QuesForm inputtextWrap ddq" placeholder="Click here to enter question" value="<?php echo ( ! empty($qresult['questions'])) ? stripslashes($qresult['questions']) : ''; ?>" />
																			<div class="ddqq_assets_container" <?php echo ( ! empty($ques_att) && $ques_type == 8) ? '' : 'style="display:none;"'; ?>>
																				<div class="assets_data ddqq_assets_data"><a class="view_assets" data-src="<?php echo $path . $ques_att; ?>" href="javascript:;"><?php echo $ques_att; ?></a></div>
																				<a href="javascript:void(0);" data-assets="<?php echo $ques_att; ?>" data-assets-id="ddqq_assets" data-input-id="#ddqq_qaudio,#ddqq_rec_audio" data-path="<?php echo $uploadpath ?>" class="delete_assets" title="Delete"><i class="fa fa-times" aria-hidden="true"></i></a>
																			</div>
																			<ul class="QueBoxIcon">
																				<li class="dropdown">
																					<button class="btn1 btn-secondary dropdown-toggle ddqq_assets <?php echo ( ! empty($ques_att) && $ques_type == 8) ? 'disabled' : ''; ?>" <?php echo ( ! empty($ques_att) && $ques_type == 8) ? 'disabled="disabled"' : ''; ?> type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img class="img-fluid" src="img/list/audio.svg"></button>
																					<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
																						<a class="dropdown-item">
																							<div class="tooltipLeft uploadicon">
																								<label for="file-input-ddqq-addAudio"><img class="img-fluid"  src="img/list/add_audio.svg" /><span class="tooltiptext">Add Audio</span></label>
																								<input id="file-input-ddqq-addAudio" data-id="#ddqq_qaudio" data-assets="ddqq_assets" class="uploadAudioFile" type="file" name="ddqq_Audio" />
																								<input type="hidden" name="ddqq_qaudio" id="ddqq_qaudio" value="<?php echo ($ques_type == 8 && $qatt_type == 'qa') ? $ques_att : ''; ?>"/>
																							</div>
																						</a>
																						<a class="dropdown-item">
																							<div class="tooltipLeft">
																								<img class="img-fluid rec-audio" data-input-id="ddqq_rec_audio" data-assets="ddqq_assets" src="img/list/record_audio.svg" />
																								<span class="tooltiptext">Record Audio</span>
																							</div>
																							<input type="hidden" name="ddqq_rec_audio" id="ddqq_rec_audio" />
																						 </a>
																						<a data-toggle="modal" data-target="#ddqqtts" class="dropdown-item">
																							<div class="tooltipLeft">
																								<img class="img-fluid" src="img/list/text_speech.svg" />
																								<span class="tooltiptext">Text to Speech</span>
																							</div>
																						</a>
																					 </div>
																				</li>
																			</ul>
																		</div>
																	</div>
																	<div class="q-icon-banner clearfix">
																		<ul class="QueBoxIcon QueBoxIcon1">
																			<h5>Add assets</h5>
																			<li class="dropdown">
																				<button class="btn1 btn-secondary dropdown-toggle ddq_assets <?php echo ( ! empty($ques_assets) && $ques_type == 8) ? 'disabled' : ''; ?>" <?php echo ( ! empty($ques_assets) && $ques_type == 8) ? 'disabled="disabled"' : ''; ?> type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img class="img-fluid" src="img/list/audio.svg"></button>
																				<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
																					<a data-toggle="modal" data-target="#ddqtts" class="dropdown-item">
																						<div class="tooltip1">
																							<img class="img-fluid" src="img/list/text_speech.svg">
																							<span class="tooltiptext">Text to Speech</span>
																						</div>
																					</a>
																					<a class="dropdown-item">
																						<div class="tooltip1 uploadicon">
																							<label for="file-input-ddq-addAudio">
																								<img class="img-fluid" src="img/list/add_audio.svg" />
																								<span class="tooltiptext">Add Audio</span>
																							</label>
																							<input id="file-input-ddq-addAudio" data-id="#ddq_qaudio" data-assets="ddq_assets" class="uploadAudioFile" type="file" name="ddq_Audio" />
																							<input type="hidden" name="ddq_qaudio" id="ddq_qaudio" value="<?php echo ($ques_type == 8 && $assets_type == 'a') ? $ques_assets : ''; ?>" />
																						</div>
																					</a>
																					<a class="dropdown-item">
																						<div class="tooltip1">
																							<img class="img-fluid rec-audio" data-input-id="ddq_rec_audio" data-assets="ddq_assets" src="img/list/record_audio.svg" />
																							<span class="tooltiptext">Record Audio</span>
																						</div>
																						<input type="hidden" name="ddq_rec_audio" id="ddq_rec_audio" />
																					 </a>
																				</div>
																			</li>
																			<li class="dropdown">
																				<button class="btn1 btn-secondary dropdown-toggle ddq_assets <?php echo ( ! empty($ques_assets) && $ques_type == 8) ? 'disabled' : ''; ?>" <?php echo ( ! empty($ques_assets) && $ques_type == 8) ? 'disabled="disabled"' : ''; ?> type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img class="img-fluid" src="img/list/video.svg"></button>
																				<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
																					<a class="dropdown-item">
																						<div class="tooltip1 uploadicon">
																							<label for="file-input-ddq-addVideo">
																								<img class="img-fluid" src="img/list/add_video.svg" />
																								<span class="tooltiptext">Add Video</span>
																							</label>
																							<input id="file-input-ddq-addVideo" data-id="#ddq_video" data-assets="ddq_assets" class="uploadVideoFile" type="file" name="ddq_Video" />
																							<input type="hidden" name="ddq_video" id="ddq_video" value="<?php echo ($ques_type == 8 && $assets_type == 'v') ? $ques_assets : ''; ?>" />
																						</div>
																					</a>
																					<a class="dropdown-item">
																						<div class="tooltip1">
																							<img class="img-fluid rec-video" data-input-id="ddq_rec_video" data-assets="ddq_assets" src="img/qus_icon/rec_video.svg" />
																							<span class="tooltiptext">Record Video</span>
																						 </div>
																						 <input type="hidden" name="ddq_rec_video" id="ddq_rec_video" />
																					</a>
																					<a class="dropdown-item">
																						<div class="tooltip1">
																							<img class="img-fluid rec-screen" data-input-id="ddq_rec_screen" data-assets="ddq_assets" src="img/list/screen_record.svg" />
																							<span class="tooltiptext">Record Screen</span>
																						 </div>
																						 <input type="hidden" name="ddq_rec_screen" id="ddq_rec_screen" value="<?php echo ($ques_type == 8 && $assets_type == 's') ? $ques_assets : ''; ?>" />
																					</a>
																				</div>
																			</li>
																			<li>
																				<div class="tooltip uploadicon ddq_assets <?php echo ( ! empty($ques_assets) && $ques_type == 8) ? 'disabled' : ''; ?>" <?php echo ( ! empty($ques_assets) && $ques_type == 8) ? 'disabled="disabled"' : ''; ?>>
																					<label for="file-input-ddq-addImg">
																						<img class="img-fluid" src="img/list/image.svg" />
																						<span class="tooltiptext">Add Image</span>
																					</label>
																					<input id="file-input-ddq-addImg" data-id="#ddq_img" data-assets="ddq_assets" class="uploadImgFile ddq_assets" type="file" name="ddq_Img" <?php echo ( ! empty($ques_assets) && $ques_type == 8) ? 'disabled="disabled"' : ''; ?> />
																					<input type="hidden" name="ddq_img" id="ddq_img" value="<?php echo ($ques_type == 8 && $assets_type == 'i') ? $ques_assets : ''; ?>" />
																				</div>
																			</li>
																			<li>
																				<div class="tooltip uploadicon ddq_assets <?php echo ( ! empty($ques_assets) && $ques_type == 8) ? 'disabled' : ''; ?>" <?php echo ( ! empty($ques_assets) && $ques_type == 8) ? 'disabled="disabled"' : ''; ?>>
																					<label for="file-input-ddq-addDoc">
																						<img class="img-fluid" src="img/list/add_document.svg" />
																						<span class="tooltiptext">Add Document</span>
																					</label>
																					<input id="file-input-ddq-addDoc" data-id="#ddq_doc" data-assets="ddq_assets" class="uploadDocFile ddq_assets" type="file" name="ddq_Doc" <?php echo ( ! empty($ques_assets) && $ques_type == 8) ? 'disabled="disabled"' : ''; ?> />
																					<input type="hidden" name="ddq_doc" id="ddq_doc" value="<?php echo ($ques_type == 8 && $assets_type == 'd') ? $ques_assets : ''; ?>" />
																				</div>
																			</li>
																		</ul>
																		<div class="ddq_assets_container" <?php echo ( ! empty($ques_assets) && $ques_type == 8) ? '' : 'style="display:none;"'; ?>>
																			<div class="assets_data ddq_assets_data"><a class="view_assets" data-src="<?php echo $path . $ques_assets; ?>" href="javascript:;"><?php echo $ques_assets; ?></a></div>
																			<a href="javascript:void(0);" data-assets="<?php echo $ques_assets; ?>" data-assets-id="ddq_assets" data-input-id="#ddq_img,#ddq_qaudio,#ddq_rec_audio,#ddq_video,#ddq_rec_video,#ddq_rec_screen" data-path="<?php echo $uploadpath ?>" class="delete_assets" title="Delete"><i class="fa fa-times" aria-hidden="true"></i></a>
																		</div>
																		<div class="Ques-comp">
																			<h5>Add Competency Score</h5>
																			<?php if ( ! empty($sim_com['comp_col_1']) && ! empty($sim_com['comp_val_1'])): ?>
																			<div class="form-group QusScore">
																				<input type="text" class="form-control ddq" name="ddq_ques_val_1" id="ddq_ques_val_1" onkeypress="return isNumberKey(event);" value="<?php echo $qresult['ques_val_1'] ?>" />
																			</div>
																			<input type="hidden" class="form-control" id="ddq_ques_val_1h" value="<?php echo $qresult['ques_val_1'] ?>" />
																			<?php else: ?>
																			<input type="hidden" class="form-control" name="ddq_ques_val_1" value="0" />
																			<?php endif; ?>
																			
																			<?php if ( ! empty($sim_com['comp_col_2']) && ! empty($sim_com['comp_val_2'])): ?>
																			<div class="form-group QusScore">
																				<input type="text" class="form-control ddq" name="ddq_ques_val_2" id="ddq_ques_val_2" onkeypress="return isNumberKey(event);" value="<?php echo $qresult['ques_val_2'] ?>" />
																			</div>
																			<input type="hidden" class="form-control" id="ddq_ques_val_2h" value="<?php echo $qresult['ques_val_2'] ?>" />
																			<?php else: ?>
																			<input type="hidden" class="form-control" name="ddq_ques_val_2" value="0" />
																			<?php endif; ?>
																			
																			<?php if ( ! empty($sim_com['comp_col_3']) && ! empty($sim_com['comp_val_3'])): ?>
																			<div class="form-group QusScore">
																				<input type="text" class="form-control ddq" name="ddq_ques_val_3" id="ddq_ques_val_3" onkeypress="return isNumberKey(event);" value="<?php echo $qresult['ques_val_3'] ?>" />
																			</div>
																			<input type="hidden" class="form-control" id="ddq_ques_val_3h" value="<?php echo $qresult['ques_val_3'] ?>" />
																			<?php else: ?>
																			<input type="hidden" class="form-control" name="ddq_ques_val_3" value="0" />
																			<?php endif; ?>
																			
																			<?php if ( ! empty($sim_com['comp_col_4']) && ! empty($sim_com['comp_val_4'])): ?>
																			<div class="form-group QusScore">
																				<input type="text" class="form-control ddq" name="ddq_ques_val_4" id="ddq_ques_val_4" onkeypress="return isNumberKey(event);" value="<?php echo $qresult['ques_val_4'] ?>" />
																			</div>
																			<input type="hidden" class="form-control" id="ddq_ques_val_4h" value="<?php echo $qresult['ques_val_4'] ?>" />
																			<?php else: ?>
																			<input type="hidden" class="form-control" name="ddq_ques_val_4" value="0" />
																			<?php endif; ?>
																			
																			<?php if ( ! empty($sim_com['comp_col_5']) && ! empty($sim_com['comp_val_5'])): ?>
																			<div class="form-group QusScore">
																				<input type="text" class="form-control ddq" name="ddq_ques_val_5" id="ddq_ques_val_5" onkeypress="return isNumberKey(event);" value="<?php echo $qresult['ques_val_5'] ?>" />
																			</div>
																			<input type="hidden" class="form-control" id="ddq_ques_val_5h" value="<?php echo $qresult['ques_val_5'] ?>" />
																			<?php else: ?>
																			<input type="hidden" class="form-control" name="ddq_ques_val_5h" value="0" />
																			<?php endif; ?>
																			
																			<?php if ( ! empty($sim_com['comp_col_6']) && ! empty($sim_com['comp_val_6'])): ?>
																			<div class="form-group QusScore">
																				<input type="text" class="form-control ddq" name="ddq_ques_val_6" id="ddq_ques_val_6" onkeypress="return isNumberKey(event);" value="<?php echo $qresult['ques_val_6'] ?>" />
																			</div>
																			<input type="hidden" class="form-control" id="ddq_ques_val_6h" value="<?php echo $qresult['ques_val_6'] ?>" />
																			<?php else: ?>
																			<input type="hidden" class="form-control" name="ddq_ques_val_6" value="0" />
																			<?php endif; ?>
																		</div>
																	</div>
																</div>
															</div>
														</div>
														<div class="Ansbox LinerClassicTMPAns">
															<div class="row">
																<div class="col-sm-12">
																	<h3 class="AnsHeading DrOPHeading">DROP TARGET</h3>
																	<?php 
																	$ddq2_sql = "SELECT * FROM sub_options_tbl WHERE question_id = '". $qresult['question_id'] ."'";
																	$ddq2_res = $db->prepare($ddq2_sql); $ddq2_res->execute();
																	if ($ddq2_res->rowCount() > 0):
																	$ddq2_res_data = $ddq2_res->fetch(PDO::FETCH_ASSOC); ?>
																	<div class="matchbox DNDQfIeLd classicdnd">
																		<input type="hidden" name="updateDDq2Answerid" value="<?php echo $ddq2_res_data['sub_options_id']; ?>" />
																		<div class="form-group">
																			<ul class="QueBoxIcon DTQuebox">
																				<li>
																					<div class="tooltip uploadicon ddq2_item_assets1 <?php echo ( ! empty($ddq2_res_data['image']) && file_exists($root_path . $ddq2_res_data['image'])) ? 'disabled' : '' ?>" <?php echo ( ! empty($ddq2_res_data['image']) && file_exists($root_path . $ddq2_res_data['image'])) ? 'disabled="disabled"' : '' ?>>
																						<label for="file-input-ddq2-item-addImg1">
																							<img class="img-fluid" src="img/list/image.svg" />
																							<span class="tooltiptext">Add Image</span>
																						</label>
																						<input id="file-input-ddq2-item-addImg1" data-id="#ddq2_item_img1" data-assets="1" class="uploadImgFileDDq2 ddq2_item_assets1" type="file" name="ddq2_item_Img1" <?php echo ( ! empty($ddq2_res_data['image'])) ? 'disabled="disabled"' : '' ?> />
																						<input type="hidden" name="ddq2_item_img" id="ddq2_item_img1" value="<?php echo ( ! empty($ddq2_res_data['image']) && file_exists($root_path . $ddq2_res_data['image'])) ? $ddq2_res_data['image'] : '' ?>" />
																					</div>
																				</li>
																			</ul>
																			<input type="text" name="ddq2_drop" class="form-control inputtextWrap ddq" placeholder="Drop Target text goes here" value="<?php echo ( ! empty($ddq2_res_data['sub_option'])) ? $ddq2_res_data['sub_option'] : '' ?>" />
																			<div class="ddq2_items_assets_container_1" id="ddq2_items_assets" <?php echo ( ! empty($ddq2_res_data['image']) && file_exists($root_path . $ddq2_res_data['image'])) ? '' : 'style="display:none;"'; ?>>
																				<div class="assets_data ddq2_items_assets_data_1"><a class="view_assets" data-src="<?php echo $path . $ddq2_res_data['image']; ?>" href="javascript:;"><?php echo $ddq2_res_data['image']; ?></a></div>
																				<a href="javascript:void(0);" data-assets="<?php echo ( ! empty($ddq2_res_data['image'])) ? $ddq2_res_data['image'] : '' ?>" data-assets-id="ddq2_item_assets1" data-input-id="#ddq2_item_img1" data-path="<?php echo $uploadpath ?>" class="delete_assets" title="Delete"><i class="fa fa-times" aria-hidden="true"></i></a>
																			</div>
																		</div>
																	</div>
																	<?php else: ?>
																	<div class="matchbox DNDQfIeLd classicdnd">
																		<div class="form-group">
																			<ul class="QueBoxIcon DTQuebox">
																				<li>
																					<div class="tooltip uploadicon ddq2_item_assets1">
																						<label for="file-input-ddq2-item-addImg1">
																							<img class="img-fluid" src="img/list/image.svg" />
																							<span class="tooltiptext">Add Image</span>
																						</label>
																						<input id="file-input-ddq2-item-addImg1" data-id="#ddq2_item_img1" data-assets="1" class="uploadImgFileDDq2 ddq2_item_assets1" type="file" name="ddq2_item_Img1" />
																						<input type="hidden" name="ddq2_item_img" id="ddq2_item_img1" />
																					</div>
																				</li>
																			</ul>
																			<input type="text" name="ddq2_drop" class="form-control inputtextWrap ddq" placeholder="Drop Target text goes here" />
																			<div class="ddq2_items_assets_container_1" id="ddq2_items_assets" style="display:none;">
																				<div class="assets_data ddq2_items_assets_data_1"><a class="view_assets" data-src="" href="javascript:;"></a></div>
																				<a href="javascript:void(0);" data-assets="" data-assets-id="ddq2_item_assets1" data-input-id="#ddq2_item_img1" data-path="<?php echo $uploadpath ?>" class="delete_assets" title="Delete"><i class="fa fa-times" aria-hidden="true"></i></a>
																			</div>
																		</div>
																	</div>
																	<?php endif; ?>
																</div>
																<div class="col-sm-12">
																	<div class="col-sm-6"><h3 class="AnsHeading DrOPHeading">DRAG ITEM</h3></div>
																	<div class="col-sm-6">
																		<div class="Check-box suffel classicCH">
																			<div class="tooltiptop"><span class="tooltiptext">Shuffle ON</span></div>
																			<label class="checkstyle">
																				<input type="checkbox" name="ddq_shuffle2" class="suffleCheck" value="1" <?php echo ( ! empty($qresult['shuffle'])) ? 'checked="checked"' : ''; ?> />
																				<span class="checkmark"></span><span class="shuffleT"> Shuffle Option</span>
																			</label>
																		</div>
																	</div>
																	<div class="ApendOption-banner">
																		<div class="DND2ApendOption-box">
																			<?php 
																			$ddqop_sql = "SELECT a.*, f.* FROM answer_tbl a LEFT JOIN feedback_tbl f ON a.answer_id = f.answer_id WHERE a.question_id = '". $qresult['question_id'] ."'";
																			$ddqop_res = $db->prepare($ddqop_sql); $ddqop_res->execute();
																			if (($ddqop_res->rowCount() > 0) && (!empty($qresult['question_id']))): $ddqopid = 1;
																			foreach ($ddqop_res->fetchAll(PDO::FETCH_ASSOC) as $ddqop_drop_row): ?>
																			<div class="DNDOption2">
																				<input type="hidden" name="updateDDq2SubAnswerid[]" value="<?php echo $ddqop_drop_row['answer_id'] ?>" />
																				<div class="Radio-box classtmpR">
																					<label class="radiostyle">
																						<input type="radio" name="ddq_true_option" value="<?php echo $ddqopid ?>" <?php echo ($ddqopid == $qresult['true_options']) ? 'checked="checked"' : ''; ?> />
																						<span class="radiomark"></span>
																					</label>
																					<div class="col-sm-12 mcwidth">
																						<div class="choicebox">
																							<div class="form-group">
																								<input type="text" name="ddq2_drag[]" class="form-control inputtextWrap" placeholder="Drag text goes here" value="<?php echo stripslashes($ddqop_drop_row['choice_option']); ?>" />
																								<?php 
																								$ddqop_cls_dis = '';
																								$ddqop_dis = '';
																								$ddqop_item = '';
																								if ( ! empty($ddqop_drop_row['image']) && $ques_type == 8):
																									$ddqop_cls_dis = 'disabled';
																									$ddqop_dis 	= 'disabled="disabled"';
																									$ddqop_item = $ddqop_drop_row['image'];
																								endif; ?>
																								<div class="ddq2_drag_items_assets_container_<?php echo $ddqopid ?>" id="ddq2_drag_items_assets" <?php echo ( ! empty($ddqop_item)) ? '' : 'style="display:none;"'; ?>>
																									<div class="assets_data ddq2_drag_items_assets_data_<?php echo $ddqopid ?>"><a class="view_assets" data-src="<?php echo $path . $ddqop_item; ?>" href="javascript:;"><?php echo $ddqop_item; ?></a></div>
																									<a href="javascript:void(0);" data-assets="<?php echo $ddqop_item; ?>" data-assets-id="ddq2_drag_item_assets<?php echo $ddqopid ?>" data-input-id="#ddq2_drag_item_img<?php echo $ddqopid ?>" data-path="<?php echo $uploadpath ?>" class="delete_assets" title="Delete"><i class="fa fa-times" aria-hidden="true"></i></a>
																								</div>
																								<ul class="QueBoxIcon">
																									<li>
																										<div class="tooltip uploadicon ddq2_drag_item_assets<?php echo $ddqopid ?> <?php echo $ddqop_cls_dis; ?>" <?php echo $ddqop_dis; ?>>
																											<label for="file-input-ddq2-drag-item-addImg<?php echo $ddqopid ?>">
																												<img class="img-fluid" src="img/list/image.svg" />
																												<span class="tooltiptext">Add Image</span>
																											</label>
																											<input id="file-input-ddq2-drag-item-addImg<?php echo $ddqopid ?>" data-id="#ddq2_drag_item_img<?php echo $ddqopid ?>" data-assets="<?php echo $ddqopid ?>" class="uploadImgFileDragDDq2 ddq2_drag_item_assets<?php echo $ddqopid ?>" type="file" name="ddq2_drag_Img<?php echo $ddqopid ?>" <?php echo $ddqop_dis; ?> />
																											<input type="hidden" name="ddq2_drag_item_img[]" id="ddq2_drag_item_img<?php echo $ddqopid ?>" value="<?php echo $ddqop_item; ?>" />
																										</div>
																									</li>
																								</ul>
																							</div>
																						</div>
																					</div>
																					<?php if ($ddqopid == 1): ?>
																					<div class="AddAns-Option"><img class="img-fluid Add_Drag" src="img/list/add_field.svg" /></div>
																					<?php else: ?>
																					<div class="AddAns-Option removeOption" data-remove-answer-id="<?php echo $ddqop_drop_row['answer_id'] ?>"><img class="img-fluid" src="img/list/delete_field.svg"></div>
																					<?php endif; ?>
																				</div>
																				<div class="classicQ classicFEED">
																					<div class="q-icon-banner clearfix">
																						<div class="Ques-comp">
																							<h5>Add Competency score</h5>
																							<?php if ( ! empty($sim_com['comp_col_1']) && ! empty($sim_com['comp_val_1'])): ?>
																							<div class="form-group QusScore">
																								<input type="text" class="form-control score dd2" name="dd2_ans_val1[]" id="dd2_ans_val1" onkeypress="return isNumberKey(event);" value="<?php echo $ddqop_drop_row['ans_val1'] ?>" />
																							</div>
																							<?php endif; if ( ! empty($sim_com['comp_col_2']) && ! empty($sim_com['comp_val_2'])): ?>
																							<div class="form-group QusScore">
																								<input type="text" class="form-control score dd2" name="dd2_ans_val2[]" id="dd2_ans_val2" onkeypress="return isNumberKey(event);" value="<?php echo $ddqop_drop_row['ans_val2'] ?>" />
																							</div>
																							<?php endif; if ( ! empty($sim_com['comp_col_3']) && ! empty($sim_com['comp_val_3'])): ?>
																							<div class="form-group QusScore">
																								<input type="text" class="form-control score dd2" name="dd2_ans_val3[]" id="dd2_ans_val3" onkeypress="return isNumberKey(event);" value="<?php echo $ddqop_drop_row['ans_val3'] ?>" />
																							</div>
																							<?php endif; if ( ! empty($sim_com['comp_col_4']) && ! empty($sim_com['comp_val_4'])): ?>
																							<div class="form-group QusScore">
																								<input type="text" class="form-control score dd2" name="dd2_ans_val4[]" id="dd2_ans_val4" onkeypress="return isNumberKey(event);" value="<?php echo $ddqop_drop_row['ans_val4'] ?>" />
																							</div>
																							<?php endif; if ( ! empty($sim_com['comp_col_5']) && ! empty($sim_com['comp_val_5'])): ?>
																							<div class="form-group QusScore">
																								<input type="text" class="form-control score dd2" name="dd2_ans_val5[]" id="dd2_ans_val5" onkeypress="return isNumberKey(event);" value="<?php echo $ddqop_drop_row['ans_val5'] ?>" />
																							</div>
																							<?php endif; if ( ! empty($sim_com['comp_col_6']) && ! empty($sim_com['comp_val_6'])): ?>
																							<div class="form-group QusScore">
																								<input type="text" class="form-control score dd2" name="dd2_ans_val6[]" id="dd2_ans_val6" onkeypress="return isNumberKey(event);" value="<?php echo $ddqop_drop_row['ans_val6'] ?>" />
																							</div>
																							<?php endif; ?>
																						</div>
																					</div>
																					<?php 
																					#----Feedback----
																					$dd2_feed_assets = '';
																					$dd2_feed_assets_type = '';
																					if ( ! empty($ddqop_drop_row['feed_audio']) && file_exists($root_path . $ddqop_drop_row['feed_audio'])):
																						$dd2_feed_assets_type = 'fa';
																						$dd2_feed_assets = $ddqop_drop_row['feed_audio'];
																					elseif ( ! empty($ddqop_drop_row['feed_video']) && file_exists($root_path . $ddqop_drop_row['feed_video'])):
																						$dd2_feed_assets_type = 'fv';
																						$dd2_feed_assets = $ddqop_drop_row['feed_video'];
																					elseif ( ! empty($ddqop_drop_row['feed_screen']) && file_exists($root_path . $ddqop_drop_row['feed_screen'])):
																						$dd2_feed_assets_type = 'fs';
																						$dd2_feed_assets = $ddqop_drop_row['feed_screen'];
																					elseif ( ! empty($ddqop_drop_row['feed_image']) && file_exists($root_path . $ddqop_drop_row['feed_image'])):
																						$dd2_feed_assets_type = 'fi';
																						$dd2_feed_assets = $ddqop_drop_row['feed_image'];
																					elseif ( ! empty($ddqop_drop_row['feed_document']) && file_exists($root_path . $ddqop_drop_row['feed_document'])):
																						$dd2_feed_assets_type = 'fd';
																						$dd2_feed_assets = $ddqop_drop_row['feed_document'];
																					endif; ?>
																					<div class="form-group feed">
																						<h5>Feedback</h5>
																						<textarea class="form-control FeedForm inputtextWrap dd2" name="dd2_cfeedback[]" id="dd2_cfeedback" placeholder="Feedback text goes here."><?php echo $ddqop_drop_row['feedback'] ?></textarea>
																						<div class="dd2f_assets_container_<?php echo $ddqopid ?>" <?php echo ( ! empty($dd2_feed_assets) && $ques_type == 8) ? '' : 'style="display:none;"'; ?>>
																							<div class="assets_data dd2f_assets_data_<?php echo $ddqopid ?>"><a class="view_assets" data-src="<?php echo $path . $dd2_feed_assets; ?>" href="javascript:;"><?php echo $dd2_feed_assets; ?></a></div>
																							<a href="javascript:void(0);" data-assets="<?php echo $dd2_feed_assets ?>" data-assets-id="dd2f_assets_<?php echo $ddqopid ?>" data-input-id="#dd2_feedback_audio_<?php echo $ddqopid ?>,#dd2_feedback_rec_audio_<?php echo $ddqopid ?>,#dd2_feedback_video_<?php echo $ddqopid ?>,#dd2_feedback_rec_video_<?php echo $ddqopid ?>,#dd2_feedback_rec_screen_<?php echo $ddqopid ?>,#dd2_feedback_img_<?php echo $ddqopid ?>,#dd2_feedback_doc_<?php echo $ddqopid ?>" data-path="<?php echo $uploadpath ?>" class="delete_assets" title="Delete"><i class="fa fa-times" aria-hidden="true"></i></a>
																						</div>
																						<ul class="QueBoxIcon feedQ">
																							<h5>Add assets</h5>
																							<li class="dropdown">
																								<button class="btn1 btn-secondary dropdown-toggle dd2f_assets_<?php echo $ddqopid ?> <?php echo ( ! empty($dd2_feed_assets) && $ques_type == 8) ? 'disabled' : ''; ?>" <?php echo ( ! empty($dd2_feed_assets) && $ques_type == 8) ? 'disabled="disabled"' : ''; ?> type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img class="img-fluid" src="img/list/audio.svg"></button>
																								<div class="dropdown-menu buttom" aria-labelledby="dropdownMenuButton">
																									<a data-toggle="modal" data-target="#dd2fctts<?php echo $ddqopid ?>" class="dropdown-item">
																										<div class="tooltip1 tooltip<?php echo $ddqopid ?>">
																											<img class="img-fluid" src="img/list/text_speech.svg" />
																											<span class="tooltiptext">Text to Speech</span>
																										</div>
																									</a>
																									<a class="dropdown-item">
																										<div class="tooltip1 uploadicon">
																											<label for="file-input-dd2-feedback-addAudio-<?php echo $ddqopid ?>">
																												<img class="img-fluid" src="img/list/add_audio.svg" />
																												<span class="tooltiptext">Add Audio</span>
																											</label>
																											<input id="file-input-dd2-feedback-addAudio-<?php echo $ddqopid ?>" data-id="#dd2_feedback_audio_<?php echo $ddqopid ?>" data-assets="dd2f_assets_<?php echo $ddqopid ?>" class="uploadAudioFileVideoTemp" type="file" name="dd2_feedback_Audio_<?php echo $ddqopid ?>" />
																											<input type="hidden" name="dd2_feedback_audio[]" id="dd2_feedback_audio_<?php echo $ddqopid ?>" value="<?php echo ($ques_type == 8 && $dd2_feed_assets_type == 'fa') ? $dd2_feed_assets : ''; ?>" />
																										</div>
																									</a>
																									<a class="dropdown-item">
																										<div class="tooltip1">
																											<img class="img-fluid rec-audio-videoq" data-input-id="dd2_feedback_rec_audio_<?php echo $ddqopid ?>" data-assets="dd2f_assets_<?php echo $ddqopid ?>" src="img/list/record_audio.svg" />
																											<span class="tooltiptext">Record Audio</span>
																										</div>
																										<input type="hidden" name="dd2_feedback_rec_audio[]" id="dd2_feedback_rec_audio_<?php echo $ddqopid ?>" />
																									 </a>
																								</div>
																							</li>
																							<li class="dropdown">
																								<button class="btn1 btn-secondary dropdown-toggle dd2f_assets_<?php echo $ddqopid ?> <?php echo ( ! empty($dd2_feed_assets) && $ques_type == 8) ? 'disabled' : ''; ?>" <?php echo ( ! empty($dd2_feed_assets) && $ques_type == 8) ? 'disabled="disabled"' : ''; ?> type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img class="img-fluid" src="img/list/video.svg"></button>
																								<div class="dropdown-menu buttom" aria-labelledby="dropdownMenuButton">
																									<a class="dropdown-item">
																										<div class="tooltip1 uploadicon">
																											<label for="file-input-dd2-feedback-addVideo-<?php echo $ddqopid ?>">
																												<img class="img-fluid" src="img/list/add_video.svg">
																												<span class="tooltiptext">Add Video</span>
																											</label>
																											<input id="file-input-dd2-feedback-addVideo-<?php echo $ddqopid ?>" data-id="#dd2_feedback_video_<?php echo $ddqopid ?>" data-assets="dd2f_assets_<?php echo $ddqopid ?>" class="uploadFileVideoTemp" type="file" name="dd2_feedback_Video_<?php echo $ddqopid ?>" />
																											<input type="hidden" name="dd2_feedback_video[]" id="dd2_feedback_video_<?php echo $ddqopid ?>" value="<?php echo ($ques_type == 8 && $dd2_feed_assets_type == 'fv') ? $dd2_feed_assets : ''; ?>" />
																										</div>
																									</a>
																									<a class="dropdown-item">
																										<div class="tooltip1">
																											<img class="img-fluid rec-video-videoq" data-input-id="dd2_feedback_rec_video_<?php echo $ddqopid ?>" data-assets="dd2f_assets_<?php echo $ddqopid ?>" src="img/qus_icon/rec_video.svg">
																											<span class="tooltiptext">Record Video</span>
																										 </div>
																										 <input type="hidden" name="dd2_feedback_rec_video[]" id="dd2_feedback_rec_video_<?php echo $ddqopid ?>" />
																									</a>
																									<a class="dropdown-item">
																										<div class="tooltip1">
																											<img class="img-fluid rec-screen-videoq" data-input-id="dd2_feedback_rec_screen_<?php echo $ddqopid ?>" data-assets="dd2f_assets_<?php echo $ddqopid ?>" src="img/list/screen_record.svg">
																											<span class="tooltiptext">Record Screen</span>
																										 </div>
																										 <input type="hidden" name="dd2_feedback_rec_screen[]" id="dd2_feedback_rec_screen_<?php echo $ddqopid ?>" value="<?php echo ($ques_type == 8 && $dd2_feed_assets_type == 'fs') ? $mcq_feed_assets : ''; ?>" />
																									</a>
																								</div>
																							</li>
																							<li>
																								<div class="tooltip uploadicon dd2f_assets_<?php echo $ddqopid ?> <?php echo ( ! empty($dd2_feed_assets) && $ques_type == 8) ? 'disabled' : ''; ?>" <?php echo ( ! empty($dd2_feed_assets) && $ques_type == 8) ? 'disabled="disabled"' : ''; ?>>
																									<label for="file-input-dd2-feedback-addImg-<?php echo $ddqopid ?>">
																										<img class="img-fluid" src="img/list/image.svg">
																										<span class="tooltiptext">Add Image</span>
																									</label>
																									<input id="file-input-dd2-feedback-addImg-<?php echo $ddqopid ?>" data-id="#dd2_feedback_img_<?php echo $ddqopid ?>" data-assets="dd2f_assets_<?php echo $ddqopid ?>" class="uploadImgFileVideo dd2f_assets_<?php echo $ddqopid ?>" type="file" name="dd2_feedback_Img_<?php echo $ddqopid ?>" <?php echo ( ! empty($dd2_feed_assets) && $ques_type == 8) ? 'disabled="disabled"' : ''; ?> />
																									<input type="hidden" name="dd2_feedback_img[]" id="dd2_feedback_img_<?php echo $ddqopid ?>" value="<?php echo ($ques_type == 8 && $dd2_feed_assets_type == 'fi') ? $dd2_feed_assets : ''; ?>" />
																								</div>
																							</li>
																							<li>
																								<div class="tooltip uploadicon dd2f_assets_<?php echo $ddqopid ?> <?php echo ( ! empty($dd2_feed_assets) && $ques_type == 8) ? 'disabled' : ''; ?>" <?php echo ( ! empty($dd2_feed_assets) && $ques_type == 8) ? 'disabled="disabled"' : ''; ?>>
																									<label for="file-input-dd2-feedback-addDoc-<?php echo $ddqopid ?>">
																										<img class="img-fluid" src="img/list/add_document.svg" />
																										<span class="tooltiptext">Add Document</span>
																									</label>
																									<input id="file-input-dd2-feedback-addDoc-<?php echo $ddqopid ?>" data-id="#dd2_feedback_doc_<?php echo $ddqopid ?>" data-assets="dd2f_assets_<?php echo $ddqopid ?>" class="uploadDocFileVideo dd2f_assets_<?php echo $ddqopid ?>" type="file" name="dd2_feedback_Doc_<?php echo $ddqopid ?>" <?php echo ( ! empty($dd2_feed_assets) && $ques_type == 8) ? 'disabled="disabled"' : ''; ?> />
																									<input type="hidden" name="dd2_feedback_doc[]" id="dd2_feedback_doc_<?php echo $ddqopid ?>" value="<?php echo ($ques_type == 8 && $dd2_feed_assets_type == 'fd') ? $dd2_feed_assets : ''; ?>" />
																								</div>
																							</li>
																						</ul>
																						<div class="form-popup draggable Ques-pop-style modal" id="dd2fctts<?php echo $ddqopid ?>">
																							<div class="popheading">Insert Text to Speech</div>
																							<div class="textarea">
																								<textarea type="text" class="form-control1 inputtextWrap" name="dd2_feedback_text_to_speech[]" id="dd2_feedback_text_to_speech_<?php echo $ddqopid ?>"><?php echo ($ques_type == 8 && ! empty($ddqop_drop_row['feed_speech_text'])) ? stripslashes($ddqop_drop_row['feed_speech_text']) : ''; ?></textarea>
																							</div>
																							<div class="modal-footer">
																								<button type="button" class="btn1 submitbtn1" data-dismiss="modal">Insert</button>
																								<button type="button" class="btn1 submitbtn1" onclick="clearData('dd2_feedback_text_to_speech_<?php echo $ddqopid ?>');">Clear</button>
																								<button type="button" class="btn1 submitbtn1" data-dismiss="modal">Cancel</button>
																							</div>
																						</div>
																					</div>
																				</div>
																			</div>
																			<?php $ddqopid++; endforeach; else: ?>
																			<div class="DNDOption2">
																				<div class="Radio-box classtmpR">
																					<label class="radiostyle">
																						<input type="radio" name="ddq_true_option" value="1" checked /><span class="radiomark"></span>
																					</label>
																					<div class="col-sm-12 mcwidth">
																						<div class="choicebox">
																							<div class="form-group">
																								<input type="text" name="ddq2_drag[]" class="form-control inputtextWrap" placeholder="Drag text goes here" />
																								<div class="ddq2_drag_items_assets_container_1" id="ddq2_drag_items_assets" style="display:none;">
																									<div class="assets_data ddq2_drag_items_assets_data_1"><a class="view_assets" data-src="" href="javascript:;"></a></div>
																									<a href="javascript:void(0);" data-assets="" data-assets-id="ddq2_drag_item_assets1" data-input-id="#ddq2_drag_item_img1" data-path="<?php echo $uploadpath ?>" class="delete_assets" title="Delete"><i class="fa fa-times" aria-hidden="true"></i></a>
																								</div>
																								<ul class="QueBoxIcon">
																									<li>
																										<div class="tooltip uploadicon ddq2_drag_item_assets1">
																											<label for="file-input-ddq2-drag-item-addImg1">
																												<img class="img-fluid" src="img/list/image.svg" />
																												<span class="tooltiptext">Add Image</span>
																											</label>
																											<input id="file-input-ddq2-drag-item-addImg1" data-id="#ddq2_drag_item_img1" data-assets="1" class="uploadImgFileDragDDq2 ddq2_drag_item_assets1" type="file" name="ddq2_drag_Img1" />
																											<input type="hidden" name="ddq2_drag_item_img[]" id="ddq2_drag_item_img1" />
																										</div>
																									</li>
																								</ul>
																							</div>
																						</div>
																					</div>
																					<div class="AddAns-Option">
																						<img class="img-fluid Add_Drag" src="img/list/add_field.svg">
																					</div>
																				</div>
																				<div class="classicQ classicFEED">
																					<div class="q-icon-banner clearfix">
																						<div class="Ques-comp">
																							<h5>Add Competency score</h5>
																							<?php if ( ! empty($sim_com['comp_col_1']) && ! empty($sim_com['comp_val_1'])): ?>
																							<div class="form-group QusScore">
																								<input type="text" class="form-control score dd2" name="dd2_ans_val1[]" id="dd2_ans_val1" onkeypress="return isNumberKey(event);" />
																							</div>
																							<?php endif; if ( ! empty($sim_com['comp_col_2']) && ! empty($sim_com['comp_val_2'])): ?>
																							<div class="form-group QusScore">
																								<input type="text" class="form-control score dd2" name="dd2_ans_val2[]" id="dd2_ans_val2" onkeypress="return isNumberKey(event);" />
																							</div>
																							<?php endif; if ( ! empty($sim_com['comp_col_3']) && ! empty($sim_com['comp_val_3'])): ?>
																							<div class="form-group QusScore">
																								<input type="text" class="form-control score dd2" name="dd2_ans_val3[]" id="dd2_ans_val3" onkeypress="return isNumberKey(event);" />
																							</div>
																							<?php endif; if ( ! empty($sim_com['comp_col_4']) && ! empty($sim_com['comp_val_4'])): ?>
																							<div class="form-group QusScore">
																								<input type="text" class="form-control score dd2" name="dd2_ans_val4[]" id="dd2_ans_val4" onkeypress="return isNumberKey(event);" />
																							</div>
																							<?php endif; if ( ! empty($sim_com['comp_col_5']) && ! empty($sim_com['comp_val_5'])): ?>
																							<div class="form-group QusScore">
																								<input type="text" class="form-control score dd2" name="dd2_ans_val5[]" id="dd2_ans_val5" onkeypress="return isNumberKey(event);" />
																							</div>
																							<?php endif; if ( ! empty($sim_com['comp_col_6']) && ! empty($sim_com['comp_val_6'])): ?>
																							<div class="form-group QusScore">
																								<input type="text" class="form-control score dd2" name="dd2_ans_val6[]" id="dd2_ans_val6" onkeypress="return isNumberKey(event);" />
																							</div>
																							<?php endif; ?>
																						</div>
																					</div>
																					<div class="form-group feed">
																						<h5>Feedback</h5>
																						<textarea class="form-control FeedForm inputtextWrap dd2" name="dd2_cfeedback[]" id="dd2_cfeedback" placeholder="Feedback text goes here."></textarea>
																						<div class="dd2f_assets_container_1" style="display:none;">
																							<div class="assets_data dd2f_assets_data_1"><a class="view_assets" data-src="" href="javascript:;"></a></div>
																							<a href="javascript:void(0);" data-assets="" data-assets-id="" data-input-id="" data-path="<?php echo $uploadpath ?>" class="delete_assets" title="Delete"><i class="fa fa-times" aria-hidden="true"></i></a>
																						</div>
																						<ul class="QueBoxIcon feedQ">
																							<h5>Add assets</h5>
																							<li class="dropdown">
																								<button class="btn1 btn-secondary dropdown-toggle dd2f_assets_1" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img class="img-fluid" src="img/list/audio.svg"></button>
																								<div class="dropdown-menu buttom" aria-labelledby="dropdownMenuButton">
																									<a data-toggle="modal" data-target="#dd2fctts1" class="dropdown-item">
																										<div class="tooltip1">
																											<img class="img-fluid" src="img/list/text_speech.svg" />
																											<span class="tooltiptext">Text to Speech</span>
																										</div>
																									</a>
																									<a class="dropdown-item">
																										<div class="tooltip1 uploadicon">
																											<label for="file-input-dd2-feedback-addAudio-1">
																												<img class="img-fluid" src="img/list/add_audio.svg" />
																												<span class="tooltiptext">Add Audio</span>
																											</label>
																											<input id="file-input-dd2-feedback-addAudio-1" data-id="#dd2_feedback_audio_1" data-assets="dd2f_assets_1" class="uploadAudioFileVideoTemp" type="file" name="dd2_feedback_Audio_1" />
																											<input type="hidden" name="dd2_feedback_audio[]" id="dd2_feedback_audio_1" />
																										</div>
																									</a>
																									<a class="dropdown-item">
																										<div class="tooltip1">
																											<img class="img-fluid rec-audio-videoq" data-input-id="dd2_feedback_rec_audio_1" data-assets="dd2f_assets_1" src="img/list/record_audio.svg" />
																											<span class="tooltiptext">Record Audio</span>
																										</div>
																										<input type="hidden" name="dd2_feedback_rec_audio[]" id="dd2_feedback_rec_audio_1" />
																									 </a>
																								</div>
																							</li>
																							<li class="dropdown">
																								<button class="btn1 btn-secondary dropdown-toggle dd2f_assets_1" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img class="img-fluid" src="img/list/video.svg"></button>
																								<div class="dropdown-menu buttom" aria-labelledby="dropdownMenuButton">
																									<a class="dropdown-item">
																										<div class="tooltip1 uploadicon">
																											<label for="file-input-dd2-feedback-addVideo-1">
																												<img class="img-fluid" src="img/list/add_video.svg">
																												<span class="tooltiptext">Add Video</span>
																											</label>
																											<input id="file-input-dd2-feedback-addVideo-1" data-id="#dd2_feedback_video_1" data-assets="dd2f_assets_1" class="uploadFileVideoTemp" type="file" name="dd2_feedback_Video_1" />
																											<input type="hidden" name="dd2_feedback_video[]" id="dd2_feedback_video_1" />
																										</div>
																									</a>
																									<a class="dropdown-item">
																										<div class="tooltip1">
																											<img class="img-fluid rec-video-videoq" data-input-id="dd2_feedback_rec_video_1" data-assets="dd2f_assets_1" src="img/qus_icon/rec_video.svg">
																											<span class="tooltiptext">Record Video</span>
																										 </div>
																										 <input type="hidden" name="dd2_feedback_rec_video[]" id="dd2_feedback_rec_video_1" />
																									</a>
																									<a class="dropdown-item">
																										<div class="tooltip1">
																											<img class="img-fluid rec-screen-videoq" data-input-id="dd2_feedback_rec_screen_1" data-assets="dd2f_assets_1" src="img/list/screen_record.svg">
																											<span class="tooltiptext">Record Screen</span>
																										 </div>
																										 <input type="hidden" name="dd2_feedback_rec_screen[]" id="dd2_feedback_rec_screen_1" />
																									</a>
																								</div>
																							</li>
																							<li>
																								<div class="tooltip uploadicon dd2f_assets_1">
																									<label for="file-input-dd2-feedback-addImg-1">
																										<img class="img-fluid" src="img/list/image.svg">
																										<span class="tooltiptext">Add Image</span>
																									</label>
																									<input id="file-input-dd2-feedback-addImg-1" data-id="#dd2_feedback_img_1" data-assets="dd2f_assets_1" class="uploadImgFileVideo dd2f_assets_1" type="file" name="dd2_feedback_Img_1" />
																									<input type="hidden" name="dd2_feedback_img[]" id="dd2_feedback_img_1" />
																								</div>
																							</li>
																							<li>
																								<div class="tooltip uploadicon dd2f_assets_1">
																									<label for="file-input-dd2-feedback-addDoc-1">
																										<img class="img-fluid" src="img/list/add_document.svg" />
																										<span class="tooltiptext">Add Document</span>
																									</label>
																									<input id="file-input-dd2-feedback-addDoc-1" data-id="#dd2_feedback_doc_1" data-assets="dd2f_assets_1" class="uploadDocFileVideo dd2f_assets_1" type="file" name="dd2_feedback_Doc_1" />
																									<input type="hidden" name="dd2_feedback_doc[]" id="dd2_feedback_doc_1" />
																								</div>
																							</li>
																						</ul>
																						<div class="form-popup draggable Ques-pop-style modal" id="dd2fctts1">
																							<div class="popheading">Insert Text to Speech</div>
																							<div class="textarea">
																								<textarea type="text" class="form-control1 inputtextWrap" name="dd2_feedback_text_to_speech[]" id="dd2_feedback_text_to_speech_1"></textarea>
																							</div>
																							<div class="modal-footer">
																								<button type="button" class="btn1 submitbtn1" data-dismiss="modal">Insert</button>
																								<button type="button" class="btn1 submitbtn1" onclick="clearData('dd2_feedback_text_to_speech_1');">Clear</button>
																								<button type="button" class="btn1 submitbtn1" data-dismiss="modal">Cancel</button>
																							</div>
																						</div>
																					 </div>
																				</div>
																			</div>
																			<?php endif; ?>
																		 </div>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
									 
									<!--SwapingQuestion-Question-->
									<div id="SwapingQuestion" class="Linr_mulVid_baner tab-pane fade <?php echo ($ques_type == 7) ? 'in active' : ''; ?>">
										<div class="container-fluid">
											<div class="widthQ100 classic-template">
												<div class="tab-content">
													<div id="match-Q" class="match-Q tab-pane fade in active">
														<div class="quesbox CQtmp">
															<div class="row">
																<div class="col-sm-12">
																	<div class="critical_div">
																		<h3 class="qusHeading">QUESTION <p class="question-type">(Swiping Question)</p></h3>								
																		<div class="CIRtiCaltext">
																			Critical Question: <input type="checkbox" name="swip_criticalQ" id="swip_criticalQ" class="form-control criticalQ" value="1" <?php echo ( ! empty($qresult['critical'])) ? 'checked="checked"' : ''; ?> />
																		</div>
																	</div>
																	<div class="quesbox-flex">
																		<div class="form-group">
																			<input type="text" name="swipq" id="swipq" class="form-control QuesForm inputtextWrap swipq" placeholder="Click here to enter question" value="<?php echo ( ! empty($qresult['questions'])) ? stripslashes($qresult['questions']) : ''; ?>" />
																			<div class="swipqq_assets_container" <?php echo ( ! empty($ques_att) && $ques_type == 7) ? '' : 'style="display:none;"'; ?>>
																				<div class="assets_data swipqq_assets_data"><a class="view_assets" data-src="<?php echo $path . $ques_att; ?>" href="javascript:;"><?php echo $ques_att; ?></a></div>
																				<a href="javascript:void(0);" data-assets="<?php echo $ques_att; ?>" data-assets-id="swipqq_assets" data-input-id="#swipqq_qaudio,#swipqq_rec_audio" data-path="<?php echo $uploadpath ?>" class="delete_assets" title="Delete"><i class="fa fa-times" aria-hidden="true"></i></a>
																			</div>
																			<ul class="QueBoxIcon">
																				<li class="dropdown">
																					<button class="btn1 btn-secondary dropdown-toggle swipqq_assets <?php echo ( ! empty($ques_att) && $ques_type == 7) ? 'disabled' : ''; ?>" <?php echo ( ! empty($ques_att) && $ques_type == 7) ? 'disabled="disabled"' : ''; ?> type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img class="img-fluid" src="img/list/audio.svg"></button>
																					<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
																						<a class="dropdown-item">
																							<div class="tooltipLeft uploadicon">
																								<label for="file-input-swipqq-addAudio"><img class="img-fluid" src="img/list/add_audio.svg" /><span class="tooltiptext">Add Audio</span></label>
																								<input id="file-input-swipqq-addAudio" data-id="#swipqq_qaudio" data-assets="swipqq_assets" class="uploadAudioFile" type="file" name="swipqq_Audio" />
																								<input type="hidden" name="swipqq_qaudio" id="swipqq_qaudio" value="<?php echo ($ques_type == 7 && $qatt_type == 'qa') ? $ques_att : ''; ?>" />
																							</div>
																						</a>
																						<a class="dropdown-item">
																							<div class="tooltipLeft">
																								<img class="img-fluid rec-audio" data-input-id="swipqq_rec_audio" data-assets="swipqq_assets" src="img/list/record_audio.svg" />
																								<span class="tooltiptext">Record Audio</span>
																							</div>
																							<input type="hidden" name="swipqq_rec_audio" id="swipqq_rec_audio" />
																						 </a>
																						<a data-toggle="modal" data-target="#swipqqtts" class="dropdown-item">
																							<div class="tooltipLeft">
																								<img class="img-fluid" src="img/list/text_speech.svg" />
																								<span class="tooltiptext">Text to Speech</span>
																							</div>
																						 </a>
																					 </div>
																				</li>
																			</ul>
																		</div>
																	</div>
																	<div class="q-icon-banner clearfix">
																		<ul class="QueBoxIcon QueBoxIcon1">
																			<h5>Add assets</h5>
																			<li class="dropdown">
																				<button class="btn1 btn-secondary dropdown-toggle swipq_assets <?php echo ( ! empty($ques_assets) && $ques_type == 7) ? 'disabled' : ''; ?>" <?php echo ( ! empty($ques_assets) && $ques_type == 7) ? 'disabled="disabled"' : ''; ?> type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img class="img-fluid" src="img/list/audio.svg"></button>
																				<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
																					<a data-toggle="modal" data-target="#swipqtts" class="dropdown-item">
																						<div class="tooltip1">
																							<img class="img-fluid" src="img/list/text_speech.svg" />
																							<span class="tooltiptext">Text to Speech</span>
																						</div>
																					</a>
																					<a class="dropdown-item">
																						<div class="tooltip1 uploadicon">
																							<label for="file-input-swipq-addAudio">
																								<img class="img-fluid" src="img/list/add_audio.svg">
																								<span class="tooltiptext">Add Audio</span>
																							</label>
																							<input id="file-input-swipq-addAudio" data-id="#swipq_audio" data-assets="swipq_assets" class="uploadAudioFile" type="file" name="swipq_Audio" />
																							<input type="hidden" name="swipq_audio" id="swipq_audio" value="<?php echo ($ques_type == 7 && $assets_type == 'a') ? $ques_assets : ''; ?>"/>
																						</div>
																					</a>
																					<a class="dropdown-item">
																						<div class="tooltip1">
																							<img class="img-fluid rec-audio" data-input-id="swipq_rec_audio" data-assets="swipq_assets" src="img/list/record_audio.svg">
																							<span class="tooltiptext">Record Audio</span>
																						</div>
																						<input type="hidden" name="swipq_rec_audio" id="swipq_rec_audio" />
																					 </a>
																				</div>
																			</li>
																			<li class="dropdown">
																				<button class="btn1 btn-secondary dropdown-toggle swipq_assets <?php echo ( ! empty($ques_assets) && $ques_type == 7) ? 'disabled' : ''; ?>" <?php echo ( ! empty($ques_assets) && $ques_type == 7) ? 'disabled="disabled"' : ''; ?> type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img class="img-fluid" src="img/list/video.svg"></button>
																				<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
																					<a class="dropdown-item">
																						<div class="tooltip1 uploadicon">
																							<label for="file-input-swipq-addVideo">
																								<img class="img-fluid" src="img/list/add_video.svg">
																								<span class="tooltiptext">Add Video</span>
																							</label>
																							<input id="file-input-swipq-addVideo" data-id="#swipq_video" data-assets="swipq_assets" class="uploadVideoFile" type="file" name="swipq_Video" />
																							<input type="hidden" name="swipq_video" id="swipq_video" value="<?php echo ($ques_type == 7 && $assets_type == 'v') ? $ques_assets : ''; ?>" />
																						</div>
																					</a>
																					<a class="dropdown-item">
																						<div class="tooltip1">
																							<img class="img-fluid rec-video" data-input-id="swipq_rec_video" data-assets="swipq_assets" src="img/qus_icon/rec_video.svg" />
																							<span class="tooltiptext">Record Video</span>
																						 </div>
																						 <input type="hidden" name="swipq_rec_video" id="swipq_rec_video" />
																					</a>
																					<a class="dropdown-item">
																						<div class="tooltip1">
																							<img class="img-fluid rec-screen" data-input-id="swipq_rec_screen" data-assets="swipq_assets" src="img/list/screen_record.svg" />
																							<span class="tooltiptext">Record Screen</span>
																						 </div>
																						 <input type="hidden" name="swipq_rec_screen" id="swipq_rec_screen" value="<?php echo ($ques_type == 7 && $assets_type == 's') ? $ques_assets : ''; ?>" />
																					</a>
																				</div>
																			</li>
																			<li>
																				<div class="tooltip uploadicon swipq_assets <?php echo ( ! empty($ques_assets) && $ques_type == 7) ? 'disabled' : ''; ?>" <?php echo ( ! empty($ques_assets) && $ques_type == 7) ? 'disabled="disabled"' : ''; ?>>
																					<label for="file-input-swipq-addImg">
																						<img class="img-fluid" src="img/list/image.svg" />
																						<span class="tooltiptext">Add Image</span>
																					</label>
																					<input id="file-input-swipq-addImg" data-id="#swipq_img" data-assets="swipq_assets" class="uploadImgFile swipq_assets" type="file" name="swipq_Img" <?php echo ( ! empty($ques_assets) && $ques_type == 7) ? 'disabled="disabled"' : ''; ?> />
																					<input type="hidden" name="swipq_img" id="swipq_img" value="<?php echo ($ques_type == 7 && $assets_type == 'i') ? $ques_assets : ''; ?>" />
																				</div>
																			</li>
																			<li>
																				<div class="tooltip uploadicon swipq_assets <?php echo ( ! empty($ques_assets) && $ques_type == 7) ? 'disabled' : ''; ?>" <?php echo ( ! empty($ques_assets) && $ques_type == 7) ? 'disabled="disabled"' : ''; ?>>
																					<label for="file-input-swipq-addDoc">
																						<img class="img-fluid" src="img/list/add_document.svg" />
																						<span class="tooltiptext">Add Document</span>
																					</label>
																					<input id="file-input-swipq-addDoc" data-id="#swipq_doc" data-assets="swipq_assets" class="uploadDocFile swipq_assets" type="file" name="swipq_Doc" <?php echo ( ! empty($ques_assets) && $ques_type == 7) ? 'disabled="disabled"' : ''; ?> />
																					<input type="hidden" name="swipq_doc" id="swipq_doc" value="<?php echo ($ques_type == 7 && $assets_type == 'd') ? $ques_assets : ''; ?>"/>
																				</div>
																			</li>
																		</ul>
																		<div class="Ques-comp">
																			<h5>Add Competency score</h5>
																			<?php if ( ! empty($sim_com['comp_col_1']) && ! empty($sim_com['comp_val_1'])): ?>
																			<div class="form-group QusScore">
																				<input type="text" class="form-control swipq" name="swipq_ques_val_1" id="swipq_ques_val_1" onkeypress="return isNumberKey(event);" value="<?php echo $qresult['ques_val_1'] ?>" />
																			</div>
																			<input type="hidden" class="form-control" id="swipq_ques_val_1h" value="<?php echo $qresult['ques_val_1'] ?>" />
																			<?php else: ?>
																			<input type="hidden" class="form-control" name="swipq_ques_val_1" value="0" />
																			<?php endif; ?>
																			
																			<?php if ( ! empty($sim_com['comp_col_2']) && ! empty($sim_com['comp_val_2'])): ?>
																			<div class="form-group QusScore">
																				<input type="text" class="form-control swipq" name="swipq_ques_val_2" id="swipq_ques_val_2" onkeypress="return isNumberKey(event);" value="<?php echo $qresult['ques_val_2'] ?>" />
																			</div>
																			<input type="hidden" class="form-control" id="swipq_ques_val_2h" value="<?php echo $qresult['ques_val_2'] ?>" />
																			<?php else: ?>
																			<input type="hidden" class="form-control" name="swipq_ques_val_2" value="0" />
																			<?php endif; ?>
																			
																			<?php if ( ! empty($sim_com['comp_col_3']) && ! empty($sim_com['comp_val_3'])): ?>
																			<div class="form-group QusScore">
																				<input type="text" class="form-control swipq" name="swipq_ques_val_3" id="swipq_ques_val_3" onkeypress="return isNumberKey(event);" value="<?php echo $qresult['ques_val_3'] ?>" />
																			</div>
																			<input type="hidden" class="form-control" id="swipq_ques_val_3h" value="<?php echo $qresult['ques_val_3'] ?>" />
																			<?php else: ?>
																			<input type="hidden" class="form-control" name="swipq_ques_val_3" value="0" />
																			<?php endif; ?>
																			
																			<?php if ( ! empty($sim_com['comp_col_4']) && ! empty($sim_com['comp_val_4'])): ?>
																			<div class="form-group QusScore">
																				<input type="text" class="form-control swipq" name="swipq_ques_val_4" id="swipq_ques_val_4" onkeypress="return isNumberKey(event);" value="<?php echo $qresult['ques_val_4'] ?>" />
																			</div>
																			<input type="hidden" class="form-control" id="swipq_ques_val_4h" value="<?php echo $qresult['ques_val_4'] ?>" />
																			<?php else: ?>
																			<input type="hidden" class="form-control" name="swipq_ques_val_4" value="0" />
																			<?php endif; ?>
																			
																			<?php if ( ! empty($sim_com['comp_col_5']) && ! empty($sim_com['comp_val_5'])): ?>
																			<div class="form-group QusScore">
																				<input type="text" class="form-control swipq" name="swipq_ques_val_5" id="swipq_ques_val_5" onkeypress="return isNumberKey(event);" value="<?php echo $qresult['ques_val_5'] ?>" />
																			</div>
																			<input type="hidden" class="form-control" id="swipq_ques_val_5h" value="<?php echo $qresult['ques_val_5'] ?>" />
																			<?php else: ?>
																			<input type="hidden" class="form-control" name="swipq_ques_val_5" value="0" />
																			<?php endif; ?>
																			
																			<?php if ( ! empty($sim_com['comp_col_6']) && ! empty($sim_com['comp_val_6'])): ?>
																			<div class="form-group QusScore">
																				<input type="text" class="form-control swipq" name="swipq_ques_val_6" id="swipq_ques_val_6" onkeypress="return isNumberKey(event);" value="<?php echo $qresult['ques_val_6'] ?>" />
																			</div>
																			<input type="hidden" class="form-control" id="swipq_ques_val_6h" value="<?php echo $qresult['ques_val_6'] ?>" />
																			<?php else: ?>
																			<input type="hidden" class="form-control" name="swipq_ques_val_6" value="0" />
																			<?php endif; ?>
																		</div>
																	</div>
																	<div class="swipq_assets_container" <?php echo ( ! empty($ques_assets) && $ques_type == 7) ? '' : 'style="display:none;"'; ?>>
																		<div class="assets_data swipq_assets_data"><a class="view_assets" data-src="<?php echo $path . $ques_assets; ?>" href="javascript:;"><?php echo $ques_assets; ?></a></div>
																		<a href="javascript:void(0);" data-assets="<?php echo $ques_assets ?>" data-assets-id="swipq_assets" data-input-id="#swipq_audio,#swipq_rec_audio,#swipq_video,#swipq_rec_video,#swipq_rec_screen,#swipq_img" data-path="<?php echo $uploadpath ?>" class="delete_assets" title="Delete"><i class="fa fa-times" aria-hidden="true"></i></a>
																	</div>
																 </div>
															</div>
														</div>
														<div class="form-popup draggable Ques-pop-style modal" id="swipqqtts">
															<div class="popheading">Insert Text to Speech</div>
															<div class="textarea">
																<textarea type="text" class="form-control1 inputtextWrap" name="swipqq_text_to_speech" id="swipqq_text_to_speech"><?php echo ($ques_type == 7 && ! empty($qresult['qspeech_text'])) ? stripslashes($qresult['qspeech_text']) : ''; ?></textarea>
															</div>
															<div class="modal-footer">
																<button type="button" class="btn1 submitbtn1" data-dismiss="modal">Insert</button>
																<button type="button" class="btn1 submitbtn1" onclick="clearData('swipqq_text_to_speech');">Clear</button>
																<button type="button" class="btn1 submitbtn1" data-dismiss="modal">Cancel</button>
															</div>
														</div>
														<div class="form-popup draggable Ques-pop-style modal" id="swipqtts">
															<div class="popheading">Insert Text to Speech</div>
															<div class="textarea">
																<textarea type="text" class="form-control1 inputtextWrap" name="swipq_text_to_speech" id="swipq_text_to_speech"><?php echo ($ques_type == 7 && ! empty($qresult['speech_text'])) ? stripslashes($qresult['speech_text']) : ''; ?></textarea>
															</div>
															<div class="modal-footer">
																<button type="button" class="btn1 submitbtn1" data-dismiss="modal">Insert</button>
																<button type="button" class="btn1 submitbtn1" onclick="clearData('swipq_text_to_speech');">Clear</button>
																<button type="button" class="btn1 submitbtn1" data-dismiss="modal">Cancel</button>
															</div>
														</div>
														<div class="Ansbox LinerClassicTMPAns">
															<div class="row">
																<div class="col-sm-12">
																	<div class="col-sm-6">
																		<h3 class="AnsHeading">CHOICE</h3>
																	</div>
																	<div class="col-sm-6">
																		<div class="Check-box suffel classicCH">
																			<div class="tooltiptop">
																				<span class="tooltiptext">Shuffle ON</span>
																			</div>
																			<label class="checkstyle">
																				<input type="checkbox" name="swipq_shuffle" class="suffleCheck" value="1" <?php echo ( ! empty($qresult['shuffle'])) ? 'checked="checked"' : ''; ?> />
																				<span class="checkmark"></span><span class="shuffleT"> Shuffle Option</span>
																			</label>
																		</div>
																	</div>
																	<div class="ApendOption-banner">
																		<div class="SwipApendOption-box">
																		<?php 
																		$swa_sql = "SELECT a.*, f.* FROM answer_tbl a LEFT JOIN feedback_tbl f ON f.answer_id = a.answer_id WHERE a.question_id = '". $qresult['question_id'] ."'";
																		$swa_res = $db->prepare($swa_sql); $swa_res->execute();
																		if (($swa_res->rowCount() > 0) && (!empty($qresult['question_id']))): $swai = 1;
																		foreach ($swa_res->fetchAll(PDO::FETCH_ASSOC) as $swip_res_row): ?>
																		<input type="hidden" name="updateSwipqAnswerid[]" value="<?php echo $swip_res_row['answer_id'] ?>" />
																		<div class="Swap-box">
																			<div class="Radio-boxclasstmpR classicChAling">
																				<label class="radiostyle"><input type="radio" name="swipq_true_option" value="<?php echo $swai ?>" <?php echo ($swai == $qresult['true_options']) ? 'checked="checked"' : ''; ?> /><span class="radiomark"></span></label>
																				<div class="col-sm-12 mcwidth">
																					<div class="choicebox">
																						<div class="form-group">
																							<input type="text" name="swipq_option[]" id="swipq_option" class="form-control QuesForm1 inputtextWrap swipq" placeholder="Click here to enter question" value="<?php echo $swip_res_row['choice_option'] ?>" />
																							<?php 
																							$swa_cls_dis	= '';
																							$swa_dis 		= '';
																							$swa_item 		= '';
																							if ( ! empty($swip_res_row['image']) && file_exists($root_path . $swip_res_row['image']) && $ques_type == 7):
																								$swa_cls_dis = 'disabled';
																								$swa_dis 	 = 'disabled="disabled"';
																								$swa_item 	 = $swip_res_row['image'];
																							endif; ?>
																							<ul class="QueBoxIcon">
																								<li>
																									<div class="tooltip uploadicon swipq_item_assets<?php echo $swai ?> <?php echo $swa_cls_dis; ?>" <?php echo $swa_dis; ?>>
																										<label for="file-input-swipq-item-addImg<?php echo $swai ?>">
																											<img class="img-fluid" src="img/list/image.svg" />
																											<span class="tooltiptext">Add Image</span>
																										</label>
																										<input id="file-input-swipq-item-addImg<?php echo $swai ?>" data-id="#swipq_item_img<?php echo $swai ?>" data-assets="<?php echo $swai ?>" class="uploadImgFileSwip swipq_item_assets<?php echo $swai ?>" type="file" name="swipq_item_Img<?php echo $swai ?>" <?php echo $swa_dis; ?> />
																										<input type="hidden" name="swipq_item_img[]" id="swipq_item_img<?php echo $swai ?>" value="<?php echo $swa_item; ?>" />
																									</div>
																								</li>
																							</ul>
																						</div>
																					</div>
																				</div>
																				<?php if ($swai == 1): ?>
																				<div class="AddAns-Option AddAns-Swaping"><img class="img-fluid swipAdd" src="img/list/add_field.svg" /></div>
																				<?php else: ?>
																				<div class="AddAns-Option removeOption" data-remove-answer-id="<?php echo $swip_res_row['answer_id'] ?>"><img class="img-fluid" src="img/list/delete_field.svg" /></div>
																				<?php endif; ?>
																			</div>
																			<div class="classicQ classicFEED">
																				<div class="q-icon-banner clearfix">                                                                                        
																					<div class="swipq_items_assets_container_<?php echo $swai ?>" id="swipq_items_assets" <?php echo ( ! empty($swa_item)) ? '' : 'style="display:none;"'; ?>>
																						<div class="assets_data swipq_items_assets_data_<?php echo $swai ?>"><a class="view_assets" data-src="<?php echo $path . $swa_item; ?>" href="javascript:;"><?php echo $swa_item; ?></a></div>
																						<a href="javascript:void(0);" data-assets="<?php echo $swa_item; ?>" data-assets-id="swipq_item_assets<?php echo $swai ?>" data-input-id="#swipq_item_img<?php echo $swai ?>" data-path="<?php echo $uploadpath ?>" class="delete_assets" title="Delete"><i class="fa fa-times" aria-hidden="true"></i></a>
																					</div>
																					<div class="Ques-comp">
																						<h5>Add Competency score</h5>
																						<?php if ( ! empty($sim_com['comp_col_1']) && ! empty($sim_com['comp_val_1'])): ?>
																						<div class="form-group QusScore">
																							<input type="text" class="form-control score swip" name="swip_ans_val1[]" id="swip_ans_val1" onkeypress="return isNumberKey(event);" value="<?php echo $swip_res_row['ans_val1'] ?>" />
																						</div>
																						<?php endif; if ( ! empty($sim_com['comp_col_2']) && ! empty($sim_com['comp_val_2'])): ?>
																						<div class="form-group QusScore">
																							<input type="text" class="form-control score swip" name="swip_ans_val2[]" id="swip_ans_val2" onkeypress="return isNumberKey(event);" value="<?php echo $swip_res_row['ans_val2'] ?>" />
																						</div>
																						<?php endif; if ( ! empty($sim_com['comp_col_3']) && ! empty($sim_com['comp_val_3'])): ?>
																						<div class="form-group QusScore">
																							<input type="text" class="form-control score swip" name="swip_ans_val3[]" id="swip_ans_val3" onkeypress="return isNumberKey(event);" value="<?php echo $swip_res_row['ans_val3'] ?>" />
																						</div>
																						<?php endif; if ( ! empty($sim_com['comp_col_4']) && ! empty($sim_com['comp_val_4'])): ?>
																						<div class="form-group QusScore">
																							<input type="text" class="form-control score swip" name="swip_ans_val4[]" id="swip_ans_val4" onkeypress="return isNumberKey(event);" value="<?php echo $swip_res_row['ans_val4'] ?>" />
																						</div>
																						<?php endif; if ( ! empty($sim_com['comp_col_5']) && ! empty($sim_com['comp_val_5'])): ?>
																						<div class="form-group QusScore">
																							<input type="text" class="form-control score swip" name="swip_ans_val5[]" id="swip_ans_val5" onkeypress="return isNumberKey(event);" value="<?php echo $swip_res_row['ans_val5'] ?>" />
																						</div>
																						<?php endif; if ( ! empty($sim_com['comp_col_6']) && ! empty($sim_com['comp_val_6'])): ?>
																						<div class="form-group QusScore">
																							<input type="text" class="form-control score swip" name="swip_ans_val6[]" id="swip_ans_val6" onkeypress="return isNumberKey(event);" value="<?php echo $swip_res_row['ans_val6'] ?>" />
																						</div>
																						<?php endif; ?>
																					</div>
																				</div>
																				<?php 
																				#----Feedback----
																				$swip_feed_assets = '';
																				$swip_feed_assets_type = '';
																				if ( ! empty($swip_res_row['feed_audio']) && file_exists($root_path . $swip_res_row['feed_audio'])):
																					$swip_feed_assets_type = 'fa';
																					$swip_feed_assets = $swip_res_row['feed_audio'];
																				elseif ( ! empty($swip_res_row['feed_video']) && file_exists($root_path . $swip_res_row['feed_video'])):
																					$swip_feed_assets_type = 'fv';
																					$swip_feed_assets = $swip_res_row['feed_video'];
																				elseif ( ! empty($swip_res_row['feed_screen']) && file_exists($root_path . $swip_res_row['feed_screen'])):
																					$swip_feed_assets_type = 'fs';
																					$swip_feed_assets = $swip_res_row['feed_screen'];
																				elseif ( ! empty($swip_res_row['feed_image']) && file_exists($root_path . $swip_res_row['feed_image'])):
																					$swip_feed_assets_type = 'fi';
																					$swip_feed_assets = $swip_res_row['feed_image'];
																				elseif ( ! empty($swip_res_row['feed_document']) && file_exists($root_path . $swip_res_row['feed_document'])):
																					$swip_feed_assets_type = 'fd';
																					$swip_feed_assets = $swip_res_row['feed_document'];
																				endif; ?>
																				<div class="form-group feed">
																					<h5>Feedback</h5>
																					<textarea class="form-control FeedForm inputtextWrap swip" name="swip_cfeedback[]" id="swip_cfeedback" placeholder="Feedback text goes here."><?php echo $swip_res_row['feedback'] ?></textarea>
																					<div class="swipf_assets_container_<?php echo $swai ?>" <?php echo ( ! empty($swip_feed_assets) && $ques_type == 7) ? '' : 'style="display:none;"'; ?>>
																						<div class="assets_data swipf_assets_data_<?php echo $swai ?>"><a class="view_assets" data-src="<?php echo $path . $swip_feed_assets; ?>" href="javascript:;"><?php echo $swip_feed_assets; ?></a></div>
																						<a href="javascript:void(0);" data-assets="<?php echo $swip_feed_assets ?>" data-assets-id="swipf_assets_<?php echo $swai ?>" data-input-id="#swip_feedback_audio_<?php echo $swai ?>,#swip_feedback_rec_audio_<?php echo $swai ?>,#swip_feedback_video_<?php echo $swai ?>,#swip_feedback_rec_video_<?php echo $swai ?>,#swip_feedback_rec_screen_<?php echo $swai ?>,#swip_feedback_img_<?php echo $swai ?>,#swip_feedback_doc_<?php echo $swai ?>" data-path="<?php echo $uploadpath ?>" class="delete_assets" title="Delete"><i class="fa fa-times" aria-hidden="true"></i></a>
																					</div>
																					<ul class="QueBoxIcon feedQ">
																						<h5>Add assets</h5>
																						<li class="dropdown">
																							<button class="btn1 btn-secondary dropdown-toggle swipf_assets_<?php echo $swai ?> <?php echo ( ! empty($swip_feed_assets) && $ques_type == 7) ? 'disabled' : ''; ?>" <?php echo ( ! empty($swip_feed_assets) && $ques_type == 7) ? 'disabled="disabled"' : ''; ?> type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img class="img-fluid" src="img/list/audio.svg"></button>
																							<div class="dropdown-menu buttom" aria-labelledby="dropdownMenuButton">
																								<a data-toggle="modal" data-target="#swipfctts<?php echo $swai ?>" class="dropdown-item">
																									<div class="tooltip1 tooltip<?php echo $swai ?>">
																										<img class="img-fluid" src="img/list/text_speech.svg" />
																										<span class="tooltiptext">Text to Speech</span>
																									</div>
																								</a>
																								<a class="dropdown-item">
																									<div class="tooltip1 uploadicon">
																										<label for="file-input-swip-feedback-addAudio-<?php echo $swai ?>">
																											<img class="img-fluid" src="img/list/add_audio.svg" />
																											<span class="tooltiptext">Add Audio</span>
																										</label>
																										<input id="file-input-swip-feedback-addAudio-<?php echo $swai ?>" data-id="#swip_feedback_audio_<?php echo $swai ?>" data-assets="swipf_assets_<?php echo $swai ?>" class="uploadAudioFileVideoTemp" type="file" name="swip_feedback_Audio_<?php echo $swai ?>" />
																										<input type="hidden" name="swip_feedback_audio[]" id="swip_feedback_audio_<?php echo $swai ?>" value="<?php echo ($ques_type == 7 && $swip_feed_assets_type == 'fa') ? $swip_feed_assets : ''; ?>" />
																									</div>
																								</a>
																								<a class="dropdown-item">
																									<div class="tooltip1">
																										<img class="img-fluid rec-audio-videoq" data-input-id="swip_feedback_rec_audio_<?php echo $swai ?>" data-assets="swipf_assets_<?php echo $swai ?>" src="img/list/record_audio.svg" />
																										<span class="tooltiptext">Record Audio</span>
																									</div>
																									<input type="hidden" name="swip_feedback_rec_audio[]" id="swip_feedback_rec_audio_<?php echo $swai ?>" />
																								 </a>
																							</div>
																						</li>
																						<li class="dropdown">
																							<button class="btn1 btn-secondary dropdown-toggle swipf_assets_<?php echo $swai ?> <?php echo ( ! empty($swip_feed_assets) && $ques_type == 7) ? 'disabled' : ''; ?>" <?php echo ( ! empty($swip_feed_assets) && $ques_type == 7) ? 'disabled="disabled"' : ''; ?> type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img class="img-fluid" src="img/list/video.svg"></button>
																							<div class="dropdown-menu buttom" aria-labelledby="dropdownMenuButton">
																								<a class="dropdown-item">
																									<div class="tooltip1 uploadicon">
																										<label for="file-input-swip-feedback-addVideo-<?php echo $swai ?>">
																											<img class="img-fluid" src="img/list/add_video.svg">
																											<span class="tooltiptext">Add Video</span>
																										</label>
																										<input id="file-input-swip-feedback-addVideo-<?php echo $swai ?>" data-id="#swip_feedback_video_<?php echo $swai ?>" data-assets="swipf_assets_<?php echo $swai ?>" class="uploadFileVideoTemp" type="file" name="swip_feedback_Video_<?php echo $swai ?>" />
																										<input type="hidden" name="swip_feedback_video[]" id="swip_feedback_video_<?php echo $swai ?>" value="<?php echo ($ques_type == 7 && $swip_feed_assets_type == 'fv') ? $swip_feed_assets : ''; ?>" />
																									</div>
																								</a>
																								<a class="dropdown-item">
																									<div class="tooltip1">
																										<img class="img-fluid rec-video-videoq" data-input-id="swip_feedback_rec_video_<?php echo $swai ?>" data-assets="swipf_assets_<?php echo $swai ?>" src="img/qus_icon/rec_video.svg">
																										<span class="tooltiptext">Record Video</span>
																									 </div>
																									 <input type="hidden" name="swip_feedback_rec_video[]" id="swip_feedback_rec_video_<?php echo $swai ?>" />
																								</a>
																								<a class="dropdown-item">
																									<div class="tooltip1">
																										<img class="img-fluid rec-screen-videoq" data-input-id="swip_feedback_rec_screen_<?php echo $swai ?>" data-assets="swipf_assets_<?php echo $swai ?>" src="img/list/screen_record.svg">
																										<span class="tooltiptext">Record Screen</span>
																									 </div>
																									 <input type="hidden" name="swip_feedback_rec_screen[]" id="swip_feedback_rec_screen_<?php echo $swai ?>" value="<?php echo ($ques_type == 7 && $swip_feed_assets_type == 'fs') ? $swip_feed_assets : ''; ?>" />
																								</a>
																							</div>
																						</li>
																						<li>
																							<div class="tooltip uploadicon swipf_assets_<?php echo $swai ?> <?php echo ( ! empty($swip_feed_assets) && $ques_type == 7) ? 'disabled' : ''; ?>" <?php echo ( ! empty($swip_feed_assets) && $ques_type == 7) ? 'disabled="disabled"' : ''; ?>>
																								<label for="file-input-swip-feedback-addImg-<?php echo $swai ?>">
																									<img class="img-fluid" src="img/list/image.svg">
																									<span class="tooltiptext">Add Image</span>
																								</label>
																								<input id="file-input-swip-feedback-addImg-<?php echo $swai ?>" data-id="#swip_feedback_img_<?php echo $swai ?>" data-assets="swipf_assets_<?php echo $swai ?>" class="uploadImgFileVideo swipf_assets_<?php echo $swai ?>" type="file" name="swip_feedback_Img_<?php echo $swai ?>" <?php echo ( ! empty($swip_feed_assets) && $ques_type == 7) ? 'disabled="disabled"' : ''; ?> />
																								<input type="hidden" name="swip_feedback_img[]" id="swip_feedback_img_<?php echo $swai ?>" value="<?php echo ($ques_type == 7 && $swip_feed_assets_type == 'fi') ? $swip_feed_assets : ''; ?>" />
																							</div>
																						</li>
																						<li>
																							<div class="tooltip uploadicon swipf_assets_<?php echo $swai ?> <?php echo ( ! empty($swip_feed_assets) && $ques_type == 7) ? 'disabled' : ''; ?>" <?php echo ( ! empty($swip_feed_assets) && $ques_type == 7) ? 'disabled="disabled"' : ''; ?>>
																								<label for="file-input-swip-feedback-addDoc-<?php echo $swai ?>">
																									<img class="img-fluid" src="img/list/add_document.svg" />
																									<span class="tooltiptext">Add Document</span>
																								</label>
																								<input id="file-input-swip-feedback-addDoc-<?php echo $swai ?>" data-id="#swip_feedback_doc_<?php echo $swai ?>" data-assets="swipf_assets_<?php echo $swai ?>" class="uploadDocFileVideo swipf_assets_<?php echo $swai ?>" type="file" name="swip_feedback_Doc_<?php echo $swai ?>" <?php echo ( ! empty($swip_feed_assets) && $ques_type == 7) ? 'disabled="disabled"' : ''; ?> />
																								<input type="hidden" name="swip_feedback_doc[]" id="swip_feedback_doc_<?php echo $swai ?>" value="<?php echo ($ques_type == 7 && $swip_feed_assets_type == 'fd') ? $swip_feed_assets : ''; ?>" />
																							</div>
																						</li>
																					</ul>
																					<div class="form-popup draggable Ques-pop-style modal" id="swipfctts<?php echo $swai ?>">
																						<div class="popheading">Insert Text to Speech</div>
																						<div class="textarea">
																							<textarea type="text" class="form-control1 inputtextWrap" name="swip_feedback_text_to_speech[]" id="swip_feedback_text_to_speech_<?php echo $swai ?>"><?php echo ($ques_type == 7 && ! empty($swip_res_row['feed_speech_text'])) ? stripslashes($swip_res_row['feed_speech_text']) : ''; ?></textarea>
																						</div>
																						<div class="modal-footer">
																							<button type="button" class="btn1 submitbtn1" data-dismiss="modal">Insert</button>
																							<button type="button" class="btn1 submitbtn1" onclick="clearData('swip_feedback_text_to_speech_<?php echo $swai ?>');">Clear</button>
																							<button type="button" class="btn1 submitbtn1" data-dismiss="modal">Cancel</button>
																						</div>
																					</div>
																				</div>
																			</div>
																		</div>
																		<?php $swai++; endforeach; else: ?>
																		<div class="Swap-box">
																			<div class="Radio-boxclasstmpR classicChAling">
																				<label class="radiostyle"><input type="radio" name="swipq_true_option" value="1" checked /><span class="radiomark"></span></label>
																				<div class="col-sm-12 mcwidth">
																					<div class="choicebox">
																						<div class="form-group">
																							<input type="text" name="swipq_option[]" id="swipq_option" class="form-control QuesForm1 inputtextWrap swipq" placeholder="Click here to enter question" />
																							<ul class="QueBoxIcon">
																								<li>
																									<div class="tooltip uploadicon swipq_item_assets1">
																										<label for="file-input-swipq-item-addImg1">
																											<img class="img-fluid" src="img/list/image.svg" />
																											<span class="tooltiptext">Add Image</span>
																										</label>
																										<input id="file-input-swipq-item-addImg1" data-id="#swipq_item_img1" data-assets="1" class="uploadImgFileSwip swipq_item_assets1" type="file" name="swipq_item_Img1" />
																										<input type="hidden" name="swipq_item_img[]" id="swipq_item_img1" />
																									</div>
																								</li>
																							</ul>
																						</div>
																					</div>
																				</div>
																				<div class="AddAns-Option">
																					<img class="img-fluid swipAdd" src="img/list/add_field.svg" />
																				</div>
																			</div>
																			<div class="classicQ classicFEED">
																				<div class="q-icon-banner clearfix">
																					<div class="swipq_items_assets_container_1" id="swipq_items_assets" style="display:none">
																						<div class="assets_data swipq_items_assets_data_1"><a class="view_assets" data-src="" href="javascript:;"></a></div>
																						<a href="javascript:void(0);" data-assets="" data-assets-id="swipq_item_assets1" data-input-id="#swipq_item_img1" data-path="<?php echo $uploadpath ?>" class="delete_assets" title="Delete"><i class="fa fa-times" aria-hidden="true"></i></a>
																					</div>
																					<div class="Ques-comp">
																						<h5>Add Competency score</h5>
																						<?php if ( ! empty($sim_com['comp_col_1']) && ! empty($sim_com['comp_val_1'])): ?>
																						<div class="form-group QusScore">
																							<input type="text" class="form-control score swip" name="swip_ans_val1[]" id="swip_ans_val1" onkeypress="return isNumberKey(event);" />
																						</div>
																						<?php endif; if ( ! empty($sim_com['comp_col_2']) && ! empty($sim_com['comp_val_2'])): ?>
																						<div class="form-group QusScore">
																							<input type="text" class="form-control score swip" name="swip_ans_val2[]" id="swip_ans_val2" onkeypress="return isNumberKey(event);" />
																						</div>
																						<?php endif; if ( ! empty($sim_com['comp_col_3']) && ! empty($sim_com['comp_val_3'])): ?>
																						<div class="form-group QusScore">
																							<input type="text" class="form-control score swip" name="swip_ans_val3[]" id="swip_ans_val3" onkeypress="return isNumberKey(event);" />
																						</div>
																						<?php endif; if ( ! empty($sim_com['comp_col_4']) && ! empty($sim_com['comp_val_4'])): ?>
																						<div class="form-group QusScore">
																							<input type="text" class="form-control score swip" name="swip_ans_val4[]" id="swip_ans_val4" onkeypress="return isNumberKey(event);" />
																						</div>
																						<?php endif; if ( ! empty($sim_com['comp_col_5']) && ! empty($sim_com['comp_val_5'])): ?>
																						<div class="form-group QusScore">
																							<input type="text" class="form-control score swip" name="swip_ans_val5[]" id="swip_ans_val5" onkeypress="return isNumberKey(event);" />
																						</div>
																						<?php endif; if ( ! empty($sim_com['comp_col_6']) && ! empty($sim_com['comp_val_6'])): ?>
																						<div class="form-group QusScore">
																							<input type="text" class="form-control score swip" name="swip_ans_val6[]" id="swip_ans_val6" onkeypress="return isNumberKey(event);" />
																						</div>
																						<?php endif; ?>
																					</div>
																				</div>
																				<div class="form-group feed">
																					<h5>Feedback</h5>
																					<textarea class="form-control FeedForm inputtextWrap swip" name="swip_cfeedback[]" id="swip_cfeedback" placeholder="Feedback text goes here."></textarea>
																					<div class="swipf_assets_container_1" style="display:none;">
																						<div class="assets_data swipf_assets_data_1"><a class="view_assets" data-src="" href="javascript:;"></a></div>
																						<a href="javascript:void(0);" data-assets="" data-assets-id="" data-input-id="" data-path="<?php echo $uploadpath ?>" class="delete_assets" title="Delete"><i class="fa fa-times" aria-hidden="true"></i></a>
																					</div>
																					<ul class="QueBoxIcon feedQ">
																						<h5>Add assets</h5>
																						<li class="dropdown">
																							<button class="btn1 btn-secondary dropdown-toggle swipf_assets_1" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img class="img-fluid" src="img/list/audio.svg"></button>
																							<div class="dropdown-menu buttom" aria-labelledby="dropdownMenuButton">
																								<a data-toggle="modal" data-target="#swipfctts1" class="dropdown-item">
																									<div class="tooltip1">
																										<img class="img-fluid" src="img/list/text_speech.svg" />
																										<span class="tooltiptext">Text to Speech</span>
																									</div>
																								</a>
																								<a class="dropdown-item">
																									<div class="tooltip1 uploadicon">
																										<label for="file-input-swip-feedback-addAudio-1">
																											<img class="img-fluid" src="img/list/add_audio.svg" />
																											<span class="tooltiptext">Add Audio</span>
																										</label>
																										<input id="file-input-swip-feedback-addAudio-1" data-id="#swip_feedback_audio_1" data-assets="swipf_assets_1" class="uploadAudioFileVideoTemp" type="file" name="swip_feedback_Audio_1" />
																										<input type="hidden" name="swip_feedback_audio[]" id="swip_feedback_audio_1" />
																									</div>
																								</a>
																								<a class="dropdown-item">
																									<div class="tooltip1">
																										<img class="img-fluid rec-audio-videoq" data-input-id="swip_feedback_rec_audio_1" data-assets="swipf_assets_1" src="img/list/record_audio.svg" />
																										<span class="tooltiptext">Record Audio</span>
																									</div>
																									<input type="hidden" name="swip_feedback_rec_audio[]" id="swip_feedback_rec_audio_1" />
																								 </a>
																							</div>
																						</li>
																						<li class="dropdown">
																							<button class="btn1 btn-secondary dropdown-toggle swipf_assets_1" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img class="img-fluid" src="img/list/video.svg"></button>
																							<div class="dropdown-menu buttom" aria-labelledby="dropdownMenuButton">
																								<a class="dropdown-item">
																									<div class="tooltip1 uploadicon">
																										<label for="file-input-swip-feedback-addVideo-1">
																											<img class="img-fluid" src="img/list/add_video.svg">
																											<span class="tooltiptext">Add Video</span>
																										</label>
																										<input id="file-input-swip-feedback-addVideo-1" data-id="#swip_feedback_video_1" data-assets="swipf_assets_1" class="uploadFileVideoTemp" type="file" name="swip_feedback_Video_1" />
																										<input type="hidden" name="swip_feedback_video[]" id="swip_feedback_video_1" />
																									</div>
																								</a>
																								<a class="dropdown-item">
																									<div class="tooltip1">
																										<img class="img-fluid rec-video-videoq" data-input-id="swip_feedback_rec_video_1" data-assets="swipf_assets_1" src="img/qus_icon/rec_video.svg">
																										<span class="tooltiptext">Record Video</span>
																									 </div>
																									 <input type="hidden" name="swip_feedback_rec_video[]" id="swip_feedback_rec_video_1" />
																								</a>
																								<a class="dropdown-item">
																									<div class="tooltip1">
																										<img class="img-fluid rec-screen-videoq" data-input-id="swip_feedback_rec_screen_1" data-assets="swipf_assets_1" src="img/list/screen_record.svg">
																										<span class="tooltiptext">Record Screen</span>
																									 </div>
																									 <input type="hidden" name="swip_feedback_rec_screen[]" id="swip_feedback_rec_screen_1" />
																								</a>
																							</div>
																						</li>
																						<li>
																							<div class="tooltip uploadicon swipf_assets_1">
																								<label for="file-input-swip-feedback-addImg-1">
																									<img class="img-fluid" src="img/list/image.svg">
																									<span class="tooltiptext">Add Image</span>
																								</label>
																								<input id="file-input-swip-feedback-addImg-1" data-id="#swip_feedback_img_1" data-assets="swipf_assets_1" class="uploadImgFileVideo swipf_assets_1" type="file" name="swip_feedback_Img_1" />
																								<input type="hidden" name="swip_feedback_img[]" id="swip_feedback_img_1" />
																							</div>
																						</li>
																						<li>
																							<div class="tooltip uploadicon swipf_assets_1">
																								<label for="file-input-swip-feedback-addDoc-1">
																									<img class="img-fluid" src="img/list/add_document.svg" />
																									<span class="tooltiptext">Add Document</span>
																								</label>
																								<input id="file-input-swip-feedback-addDoc-1" data-id="#swip_feedback_doc_1" data-assets="swipf_assets_1" class="uploadDocFileVideo swipf_assets_1" type="file" name="swip_feedback_Doc_1" />
																								<input type="hidden" name="swip_feedback_doc[]" id="swip_feedback_doc_1" />
																							</div>
																						</li>
																					</ul>
																					<div class="form-popup draggable Ques-pop-style modal" id="swipfctts1">
																						<div class="popheading">Insert Text to Speech</div>
																						<div class="textarea">
																							<textarea type="text" class="form-control1 inputtextWrap" name="swip_feedback_text_to_speech[]" id="swip_feedback_text_to_speech_1"></textarea>
																						</div>
																						<div class="modal-footer">
																							<button type="button" class="btn1 submitbtn1" data-dismiss="modal">Insert</button>
																							<button type="button" class="btn1 submitbtn1" onclick="clearData('swip_feedback_text_to_speech_1');">Clear</button>
																							<button type="button" class="btn1 submitbtn1" data-dismiss="modal">Cancel</button>
																						</div>
																					</div>
																				</div>
																			</div>
																		</div>
																		<?php endif; ?>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													 </div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
    <div class="sim_bluebanner">
        <ul class="Ques_linear_save">
           <li><button type="submit" class="submitques update_next"><img src="img/list/save-t.svg" />SAVE & CONTINUE</button></li>
           <li><button type="submit" class="submitques update_close"><img src="img/list/close.svg" />SAVE & CLOSE</button></li>
        </ul> 
    </div>
</form>

<div id="load_popup_modal_show" class="modal fade" tabindex="-1"></div>

<script src="content/js/jquery-ui-1.12.1/jquery-ui.js"></script>
<script src="content/js/jquery.fontselect.js"></script>
<script src="content/js/competency-validate.js"></script>
<script type="text/javascript">
$(function(){
	$(".draggable").draggable();
	$("#cue_point").inputmask("99:99", {"clearIncomplete": true});	
});
<?php if ($tques > 0): ?>
$("div.videotab").css("display", "none");
var getCuePoint = $('#cuePoint').val();
if (getCuePoint == '') {
	$("div.cue_pt").css("display", "none");
}
<?php endif; ?>

var assets_path = '<?php echo $path; ?>';

$('body').on('click', '.view_assets', function(e){
    e.preventDefault();
    var to = $(this).attr('data-src');
    $.fancybox.open({
        type: 'iframe',
        src: to,
        toolbar  : false,
        smallBtn : true,
		closeExisting: false,
        iframe : {
            preload : false,
            scrolling: 'auto'
        },
		opts:{
			iframe:{
				css:{
					width:600,
					height:300
				}
			},
        fullScreen:false,
    	}
    });
});

$(".inputtextWrap").hover(function() {
	$(this).attr('title', $(this).val());
}, function() {
	$(this).css('cursor','auto');
});

function convertDurationtoSeconds(duration){
	if (duration != '') {
		const [minutes, seconds] = duration.split(':');
		return Number(minutes) * 60 + Number(seconds);
	}
};

//Add Cue Point
var videoI = 1;
$('body').on('click', '.videocueAdd', function(){
	var videoclip = document.getElementById('videoclip').duration;
	var cue_point = $('#cue_point').val();
	var duration  = convertDurationtoSeconds(cue_point);
	if (cue_point == '') {
		swal({text: 'Please enter cue point.', buttons: false, icon: "warning", timer: 3000});
		return false;
	}
	else if (duration != '' && duration > videoclip){
		swal({text: 'Please enter valid cue point.', buttons: false, icon: "warning", timer: 3000});
		return false;
	}
	else if (cue_point != '' && duration <= videoclip) {
		//Add Dummy Question To Database
		$.LoadingOverlay("show");
		var simID = $('#sim_id').val();
		var file  = $('#videoq_media_file').val();
		if (simID != '') {
			var dataString = 'add_cue_point='+ cue_point +'&sim_id='+ simID +'&file='+ file;
			$.ajax({
				type: 'POST',
				url: 'includes/process.php',
				data: dataString,
				cache: false,
				success: function(resdata) {
					var res = $.parseJSON(resdata);
					if (res.success == true) {
						videoI++;
						$.LoadingOverlay("hide");
						getCueList();
					}
					else if (res.success == false) {
						$.LoadingOverlay("hide");
						swal({text: res.msg, buttons: false, icon: "warning", timer: 2000});
					}
				}, error: function() {
					swal({text: 'Oops. something went wrong please try again.', buttons: false, icon: "error", timer: 2000 });
				}
			});
		}
		$('#cue_point').val('');
		var cuePointer = $('#cuePoint').val();
		cuePointer++;
		$('#cuePoint').val(cuePointer);
	}
});

//Delete Video Cue Points - Saumya updated
$('body').on('click', '.close_cue', function() {
	var cur = $(this);
	var id	= cur.data('delete-id');
	swal({
		title: "Are you sure?",
		text: "Delete this cue point.",
		icon: "warning",
		buttons: [true, 'Delete'],
		dangerMode: true, }).then((willDelete) => { if (willDelete) {
			//delete question from DB
			$.LoadingOverlay("show");
			var dataString = 'del_Question='+ true +'&ques_id='+ id;
			$.ajax({
				type: 'POST',
				url: 'includes/process.php',
				data: dataString,
				cache: false,
				success: function(resdata) {
					var res = $.parseJSON(resdata);
					if (res.success == true) {
						$('.cue_points_' +id).remove();
						$('.Ans_box_' +id).remove();
						$.LoadingOverlay("hide");
					}
					else if (res.success == false) {
						$.LoadingOverlay("hide");
						swal({text: res.msg, buttons: false, icon: "warning", timer: 2000});
					}
				}, error: function() {
					swal({text: 'Oops. something went wrong please try again.', buttons: false, icon: "error", timer: 2000});
				}
			});

			//Hide question template tab
			$("div.questionTemplateTab").removeClass("active in");
			//Disable question template
			//$("#menu-toggle").attr('disabled', true);
			//$("#sidebar-wrapper").css("display", "none");
			//$("#wrapper").toggleClass("toggled");
			var cuePointer = $('#cuePoint').val();
			cuePointer--;
			$('#cuePoint').val(cuePointer);
		}
	});
});

/* Set Default Font Type */
$('.cl_ftype').click(function () {
	$('#font_type').trigger('setFont', '');
});

function isNumberKey(evt) {
	var charCode = (evt.which) ? evt.which : evt.keyCode
	return !(charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57));
}

function clearData(id){
	$('#'+ id).val('');
}

$("#sortable").sortable({
	update: function() {
		serial = $('#sortable').sortable('serialize');
		$.LoadingOverlay("show");
		$.ajax({
            data: serial + '&qorder=true&sim_id=<?php echo $sim_id ?>',
            type: 'POST',
            url: 'includes/process.php',
			success:function(resdata) {
				getQuesList();
				$.LoadingOverlay("hide");
			},error: function() {
				$.LoadingOverlay("hide");
				swal({text: 'Oops. something went wrong please try again.', buttons: false, icon: "error", timer: 2000 });
			}
        });
	}
});

/* Get Questions List */
function getQuesList() {
	var cuePoint = $('#cuePoint').val();
	var ques_id	 = $('#ques_id').val();
	if (cuePoint == '') {
		$.ajax({
			type: 'GET',
			url: 'includes/process.php',
			data: 'getQuesList=true&sim_id=<?php echo $sim_id ?>',
			success:function(resdata) {
				$.LoadingOverlay("hide");
				var res = $.parseJSON(resdata);
				if (res.success == true) {
					var base 	= $('#location').val();
					var current	= window.location.href;
					var qurl 	= '';
					var qclass	= '';
					var qno		= 1;
					$('.question-list').empty();
					$.each(res.data, function (key, val) {
						qurl = base + '&ques_id='+ val.ques_id;
						if (current == qurl){
							qclass = 'active';
						} else {
							qclass = '';
						}
						var qlist = '<label class="ui-state-default '+ qclass +'" id="question-'+ val.qid +'"><span class="QNum">'+ qno +'</span><input type="checkbox" name="ques_no[]" class="form-control ques_no ui-state-CheCK" value="'+ val.qid +'" /><a class="ui-state-text" href="'+ qurl +'"><img class="updownQues" src="img/list/up-down.svg"><li>'+ val.qname +'</li></a></label>';
						$('.question-list').append(qlist);
						qno++;
					});
				} else {
					//swal({text: 'Questions list not load please try again later.', buttons: false, icon: "error", timer: 2000 });
				}
			},error: function() {
				$.LoadingOverlay("hide");
				swal({text: 'Oops. something went wrong please try again.', buttons: false, icon: "error", timer: 2000 });
			}
		});
	}
}

/* Get Questions List */
function getCueList() {
	var cuePoint = $('#cuePoint').val();
	if (cuePoint != '') {
		$.ajax({
			type: 'GET',
			url: 'includes/process.php',
			data: 'getCuePointList=true&sim_id=<?php echo $sim_id ?>',
			success:function(resdata) {
				$.LoadingOverlay("hide");
				var res = $.parseJSON(resdata);
				if (res.success == true) {
					var base 	= $('#location').val();
					var current	= window.location.href;
					var qurl 	= '';
					var qclass	= '';
					var qno		= 1;
					$('.question-list').empty();
					$.each(res.data, function (key, val) {
						qurl = base + '&ques_id='+ val.ques_id;
						if (current == qurl){
							qclass = 'active';
						} else {
							qclass = '';
						}
						var qlist = '<label class="ui-state-default '+ qclass +'" id="question-'+ val.qid +'"><span class="QNum">'+ qno +'</span><input type="checkbox" name="ques_no[]" class="form-control ques_no ui-state-CheCK" value="'+ val.qid +'" /><a class="ui-state-text" href="'+ qurl +'"><img class="updownQues" src="img/list/up-down.svg"><li>'+ val.qname +'</li></a></label>';
						$('.question-list').append(qlist);
						qno++;
					});
				} else {
					//swal({text: 'Questions list not load please try again later.', buttons: false, icon: "error", timer: 2000 });
				}
			},error: function() {
				$.LoadingOverlay("hide");
				swal({text: 'Oops. something went wrong please try again.', buttons: false, icon: "error", timer: 2000 });
			}
		});
	}
}

getQuesList();
getCueList();

$('.update_next').on('click', function(){
	$('#submit_type').val(1);
});

$('.update_close').on('click', function(){
	$('#submit_type').val(2);
});

$('.type-color-on-page').spectrum({
	type: "component",
	togglePaletteOnly: "true",
	hideAfterPaletteSelect: "true",
	showInput: "true",
	showInitial: "true",
});

$('#font_type').fontselect({
	placeholder: 'Pick a font from the list',
	searchable: true,
	googleFonts: [<?php echo $db::googleFont; ?>]
});

/* Drag and Drop Selected Option */
$('.dragoption').on('click', function(){
	$('#seleted_drag_option').val($(this).val());
});

/* Add New Question */
$('.AddIconMultiple').on('click', function(e){
	e.preventDefault();
	$.LoadingOverlay("show");
	var simID = $('#sim_id').val();
	if (simID != '') {
		var dataString = 'add_Question='+ true +'&sim_id='+ simID;
		$.ajax({
			type: 'POST',
			url: 'includes/process.php',
			data: dataString,
			cache: false,
			success: function(resdata) {
				var res = $.parseJSON(resdata);
				if (res.success == true) {
					$.LoadingOverlay("hide");
					getQuesList();
				}
				else if (res.success == false) {
					$.LoadingOverlay("hide");
					swal({text: res.msg, buttons: false, icon: "warning", timer: 2000});
				}
			}, error: function() {
				swal({text: 'Oops. something went wrong please try again.', buttons: false, icon: "error", timer: 2000 });
			}
		});
	}
});

/* Delete Question */
$('.DelIcon').on('click', function(e){
	e.preventDefault();
	var ques = $('.ques_no:checked').map(function(){ return this.value; }).get();
	if (ques.length > 0) {
		$.LoadingOverlay("show");
		var dataString = 'del_Question='+ true +'&ques_id='+ ques;
		$.ajax({
			type: 'POST',
			url: 'includes/process.php',
			data: dataString,
			cache: false,
			success: function(resdata) {
				var res = $.parseJSON(resdata);
				if (res.success == true) {
					$.LoadingOverlay("hide");
					var return_url = $('#location').val();
					setTimeout(function() { window.location.href = return_url; }, 2000);
				}
				else if (res.success == false) {
					$.LoadingOverlay("hide");
					swal({text: res.msg, buttons: false, icon: "warning", timer: 2000});
				}
			}, error: function() {
				swal({text: 'Oops. something went wrong please try again.', buttons: false, icon: "error", timer: 2000 });
			}
		});
	} else {
		swal({text: 'Please select at-least one Question', buttons: false, icon: "warning", timer: 2000 });
	}
});

//Select Video Type single or multiple - Saumya
$("input[name='radio-group-video-single-multipe']").click(function(){
	//get value if single or multiple is selected
	var radioValue = $("input[name='radio-group-video-single-multipe']:checked").val();

	//disable radio after selection
	$("input[name='radio-group-video-single-multipe']").attr('disabled', true);

	//Now hide this tab
	$("div.videotab").css("display", "none");

	//Display video panel to upload video
	$("div.panel-video").css("display", "block");
	
	if (radioValue == 'singleVideo') {
		$('#vtype').val(1);
	} 
	else if (radioValue == 'multipleVideo') {
		//hide cue points in case of multi video
		//$("div.cue_pt_show").css("display","none");
		//$("div.cue_pt").css("display","none");
		$('#vtype').val(2);
		$('.videoTypeSelect,.panel-Mult_video').hide();
		var simID = $('#sim_id').val();
		if (simID != '') {
			var dataString = 'add_Question='+ true +'&sim_id='+ simID;
			$.ajax({
				type: 'POST',
				url: 'includes/process.php',
				data: dataString,
				cache: false,
				success: function(resdata) {
					var res = $.parseJSON(resdata);
					if (res.success == true) {
						$.LoadingOverlay("hide");
						getQuesList();
					}
					else if (res.success == false) {
						$.LoadingOverlay("hide");
						swal({text: res.msg, buttons: false, icon: "warning", timer: 2000});
					}
				}, error: function() {
					swal({text: 'Oops. something went wrong please try again.', buttons: false, icon: "error", timer: 2000 });
				}
			});
		}
	}
});

//Upload Video for question - Saumya updated
$('#upload_videoq_file').click(function() {
	var cuePoint = $('#cuePoint').val();
	var vtype = $('#vtype').val();
	$.LoadingOverlay("show");
	var input_id	= $('#videoqfile').data('id');
	var assets_id	= $('#videoqfile').data('assets');
	var file_data	= $('#videoqfile').prop('files')[0];
	var form_data	= new FormData();
	form_data.append('file', file_data);
	form_data.append('videoq_file', true);
	$.ajax({
		url: "includes/upload.php",
		type: "POST",
		data: form_data,
		contentType: false,
		cache: false,
		processData:false,
		success: function(resdata) {
			var res = $.parseJSON(resdata);
			if (res.success == true) {
				$(input_id).val(res.file_name);
				$('.'+ assets_id).addClass("disabled").attr('disabled', true);
				$('.video_assets_container,.video_container').show();
				$('.video_assets_container a').attr('data-assets', res.file_name);
				$('.video_assets_container a').attr('data-assets-id', assets_id);
				$('.video_assets_container a').attr('data-input-id', input_id);
				$('.video_assets_container a').attr('data-path', res.path);
				
				//-----Load-New-Upload-Video-Question-File------
				$('#videoclip').css('display', 'block');
				$('#videoclip').get(0).pause();
				$('#qvideo').attr('src', res.loadPath + res.file_name);
				$('#videoclip').get(0).load();
				if (cuePoint == '') {
					$("div.cue_pt").css("display", "none");
				}
				if (vtype == 1) {
					$("div.cue_pt").css("display", "block");
				}
				swal({text: res.msg, buttons: false, timer: 1000});
			}
			else if (res.success == false){
				$(input_id).val('');
				swal({text: res.msg, buttons: false, icon: "warning", timer: 1000});
			}
			$.LoadingOverlay("hide");
		},error: function() {
			$.LoadingOverlay("hide");
			swal("Error", 'Oops, something went wrong. Please try again later.?', "error");
		}
	});
});

/* Template Type */
$('.qtype').on('click', function(){
	var type = $(this).data('qtype');
	var get_type = $('#get_question_type') .val();
	if (get_type == '' && type != '' && type == 8) {
		$('#question_type').val(type);
	}
	else if (get_type == '' && type != '' && type == 4) {
		$('#question_type').val(type);
	}
	else if (get_type == '' && type != '' && type == 7) {
		$('#question_type').val(type);
	}
});

//------Competency-Score-------
$('#ques_val_1,#sq_ques_val_1,#sort_ques_val_1,#mcq_ques_val_1,#mmcq_ques_val_1,#swipq_ques_val_1').on('change', function(){
	var score = parseInt($(this).val());
	var maxallow = parseInt($('#comp_val_1').val());
	if (maxallow < score) {
		$(this).val('');
		swal('Maximum '+maxallow+' score', { buttons: false, timer: 1000 });
	}
});

$('#ques_val_2,#sq_ques_val_2,#sort_ques_val_2,#mcq_ques_val_2,#mmcq_ques_val_2,#swipq_ques_val_2').on('change', function(){
	var score = parseInt($(this).val());
	var maxallow = parseInt($('#comp_val_2').val());
	if (score > maxallow) {
		$(this).val('');
		swal('Maximum '+maxallow+' score', { buttons: false, timer: 1000 });
	}
});

$('#ques_val_3,#sq_ques_val_3,#sort_ques_val_3,#mcq_ques_val_3,#mmcq_ques_val_3,#swipq_ques_val_3').on('change', function(){
	var score = parseInt($(this).val());
	var maxallow = parseInt($('#comp_val_3').val());
	if (score > maxallow) {
		$(this).val('');
		swal('Maximum '+maxallow+' score', { buttons: false, timer: 1000 });
	}
});

$('#ques_val_4,#sq_ques_val_4,#sort_ques_val_4,#mcq_ques_val_4,#mmcq_ques_val_4,#swipq_ques_val_4').on('change', function(){
	var score = parseInt($(this).val());
	var maxallow = parseInt($('#comp_val_4').val());
	if (score > maxallow) {
		$(this).val('');
		swal('Maximum '+maxallow+' score', { buttons: false, timer: 1000 });
	}
});

$('#ques_val_5,#sq_ques_va_5,#sort_ques_va_5,#mcq_ques_val_5,#mmcq_ques_val_5,#swipq_ques_val_5').on('change', function(){
	var score = parseInt($(this).val());
	var maxallow = parseInt($('#comp_val_5').val());
	if (score > maxallow) {
		$(this).val('');
		swal('Maximum '+maxallow+' score', { buttons: false, timer: 1000 });
	}
});

$('#ques_val_6,#sq_ques_va_6,#sort_ques_va_6,#mcq_ques_val_6,#mmcq_ques_val_6,#swipq_ques_val_6').on('change', function(){
	var score = parseInt($(this).val());
	var maxallow = parseInt($('#comp_val_6').val());
	if (score > maxallow) {
		$(this).val('');
		swal('Maximum '+maxallow+' score', { buttons: false, timer: 1000 });
	}
});

//----------Audio-Upload-All-Template--------------------
$('body').on('change', '.uploadAudioFile', function(){
	$.LoadingOverlay("show");
	var input_id	= $(this).data('id');
	var assets_id	= $(this).data('assets');
	var file_data	= $(this).prop('files')[0];
	var form_data	= new FormData();
	form_data.append('file', file_data);
	form_data.append('qAudiofiles', '1');
	$.ajax({
		url: "includes/upload.php",
		type: "POST",
		data: form_data,
		contentType: false,
		cache: false,
		processData: false,
		success: function(resdata){
			var res = $.parseJSON(resdata);
			if (res.success == true){
				$(input_id).val(res.file_name);						
				$('.'+ assets_id).addClass("disabled").attr('disabled', true);
				$('.'+ assets_id + '_container').show();
				$('.'+ assets_id + '_data a').text(res.file_name);
				$('.'+ assets_id + '_data a').attr('data-src', assets_path + res.file_name);
				$('.'+ assets_id + '_container a.delete_assets').attr('data-assets', res.file_name);
				$('.'+ assets_id + '_container a.delete_assets').attr('data-assets-id', assets_id);
				$('.'+ assets_id + '_container a.delete_assets').attr('data-input-id', input_id);
				$('.'+ assets_id + '_container a.delete_assets').attr('data-path', res.path);
				swal({text: res.msg, buttons: false, timer: 1000});
			}
			else if (res.success == false){
				$(input_id).val('');
				swal({text: res.msg, buttons: false, icon: "warning", timer: 1000});
			}
			$.LoadingOverlay("hide");
		},error: function(){
			$.LoadingOverlay("hide");
			swal("Error", 'Oops, something went wrong. Please try again later.?', "error");
		}
	});
});

//-----------Video-Upload-All-Template-------------------
$('body').on('change', '.uploadVideoFile', function() {
	$.LoadingOverlay("show");
	var input_id	= $(this).data('id');
	var assets_id	= $(this).data('assets');
	var file_data	= $(this).prop('files')[0];
	var form_data	= new FormData();
	form_data.append('file', file_data);
	form_data.append('qVideofiles', '1');
	$.ajax({
		url: "includes/upload.php",
		type: "POST",
		data: form_data,
		contentType: false,
		cache: false,
		processData: false,
		success: function(resdata) {
			var res = $.parseJSON(resdata);
			if (res.success == true) {
				$(input_id).val(res.file_name);
				swal({text: res.msg, buttons: false, timer: 1000});
				$('.'+ assets_id).addClass("disabled").attr('disabled', true);
				$('.'+ assets_id + '_container').show();
				$('.'+ assets_id + '_data a').text(res.file_name);
				$('.'+ assets_id + '_data a').attr('data-src', assets_path + res.file_name);
				$('.'+ assets_id + '_container a.delete_assets').attr('data-assets', res.file_name);
				$('.'+ assets_id + '_container a.delete_assets').attr('data-assets-id', assets_id);
				$('.'+ assets_id + '_container a.delete_assets').attr('data-input-id', input_id);
				$('.'+ assets_id + '_container a.delete_assets').attr('data-path', res.path);
			}
			else if (res.success == false) {
				$(input_id).val('');
				swal({text: res.msg, buttons: false, icon: "warning", timer: 1000});
			}
			$.LoadingOverlay("hide");
		},error: function() {
			$.LoadingOverlay("hide");
			swal("Error", 'Oops, something went wrong. Please try again later.?', "error");
		}
	});
});

//-----------Image-Upload-All-Template-------------------
$('body').on('change', '.uploadImgFile', function() {
	$.LoadingOverlay("show");
	var cur 		= $(this);
	var input_id	= $(this).data('id');
	var assets_id	= $(this).data('assets');
	var file_data	= $(this).prop('files')[0];
	var form_data	= new FormData();
	form_data.append('file', file_data);
	form_data.append('qImgfiles', '1');
	$.ajax({
		url: "includes/upload.php",
		type: "POST",
		data: form_data,
		contentType: false,
		cache: false,
		processData: false,
		success: function(resdata) {
			var res = $.parseJSON(resdata);
			if (res.success == true) {
				$(input_id).val(res.file_name);
				swal({text: res.msg, buttons: false, timer: 1000});
				cur.closest('div').addClass("disabled");
				$('.'+ assets_id).addClass("disabled").attr('disabled', true);
				$('.'+ assets_id + '_container').show();
				$('.'+ assets_id + '_data a').text(res.file_name);
				$('.'+ assets_id + '_data a').attr('data-src', assets_path + res.file_name);
				$('.'+ assets_id + '_container a.delete_assets').attr('data-assets', res.file_name);
				$('.'+ assets_id + '_container a.delete_assets').attr('data-assets-id', assets_id);
				$('.'+ assets_id + '_container a.delete_assets').attr('data-input-id', input_id);
				$('.'+ assets_id + '_container a.delete_assets').attr('data-path', res.path);
			}
			else if (res.success == false) {
				$(input_id).val('');
				swal({text: res.msg, buttons: false, icon: "warning", timer: 1000});
			}
			$.LoadingOverlay("hide");
		},error: function() {
			$.LoadingOverlay("hide");
			swal("Error", 'Oops, something went wrong. Please try again later.?', "error");
		}
	});
});

//-----------Doc-Upload-All-Template---------------------
$('body').on('change', '.uploadDocFile', function() {
	$.LoadingOverlay("show");
	var cur 		= $(this);
	var input_id	= $(this).data('id');
	var assets_id	= $(this).data('assets');
	var file_data	= $(this).prop('files')[0];
	var form_data	= new FormData();
	form_data.append('file', file_data);
	form_data.append('qDocfiles', '1');
	$.ajax({
		url: "includes/upload.php",
		type: "POST",
		data: form_data,
		contentType: false,
		cache: false,
		processData: false,
		success: function(resdata) {
			var res = $.parseJSON(resdata);
			if (res.success == true) {
				$(input_id).val(res.file_name);
				swal({text: res.msg, buttons: false, timer: 1000});
				cur.closest('div').addClass("disabled");
				$('.'+ assets_id).addClass("disabled").attr('disabled', true);
				$('.'+ assets_id + '_container').show();
				$('.'+ assets_id + '_data a').text(res.file_name);
				$('.'+ assets_id + '_data a').attr('data-src', assets_path + res.file_name);
				$('.'+ assets_id + '_container a.delete_assets').attr('data-assets', res.file_name);
				$('.'+ assets_id + '_container a.delete_assets').attr('data-assets-id', assets_id);
				$('.'+ assets_id + '_container a.delete_assets').attr('data-input-id', input_id);
				$('.'+ assets_id + '_container a.delete_assets').attr('data-path', res.path);
			}
			else if (res.success == false) {
				$(input_id).val('');
				swal({text: res.msg, buttons: false, icon: "warning", timer: 1000});
			}
			$.LoadingOverlay("hide");
		},error: function() {
			$.LoadingOverlay("hide");
			swal("Error", 'Oops, something went wrong. Please try again later.?', "error");
		}
	});
});

//-----------Image-Upload-Sorting-Template---------------
$('body').on('change', '.uploadImgFileSort', function() {
	$.LoadingOverlay("show");
	var cur 		= $(this);
	var input_id	= $(this).data('id');
	var assets_id	= $(this).data('assets');
	var file_data	= $(this).prop('files')[0];
	var form_data	= new FormData();
	form_data.append('file', file_data);
	form_data.append('qImgfiles', '1');
	$.ajax({
		url: "includes/upload.php",
		type: "POST",
		data: form_data,
		contentType: false,
		cache: false,
		processData: false,
		success: function(resdata) {
			var res = $.parseJSON(resdata);
			if (res.success == true) {
				$(input_id).val(res.file_name);
				$('.sortq_item_assets'+ assets_id).addClass("disabled").attr('disabled', true);
				cur.closest('div').addClass("disabled");
				$('.sortq_items_assets_container_'+ assets_id).show();
				$('.sortq_items_assets_data_'+ assets_id + ' a.view_assets').text(res.file_name);
				$('.sortq_items_assets_data_'+ assets_id + ' a.view_assets').attr('data-src', assets_path + res.file_name);
				$('.sortq_items_assets_container_'+ assets_id +' a.delete_assets').attr('data-assets', res.file_name);
				$('.sortq_items_assets_container_'+ assets_id +' a.delete_assets').attr('data-assets-id', 'sortq_item_assets'+ assets_id);
				$('.sortq_items_assets_container_'+ assets_id +' a.delete_assets').attr('data-input-id', input_id);
				$('.sortq_items_assets_container_'+ assets_id +' a.delete_assets').attr('data-path', res.path);
				swal({text: res.msg, buttons: false, timer: 1000});
			}
			else if (res.success == false){
				$(input_id).val('');
				swal({text: res.msg, buttons: false, icon: "warning", timer: 1000});
			}
			$.LoadingOverlay("hide");
		},error: function() {
			$.LoadingOverlay("hide");
			swal("Error", 'Oops, something went wrong. Please try again later.?', "error");
		}
	});
});

//-----------Image-Upload-Swip-Template-----------------
$('body').on('change', '.uploadImgFileSwip', function() {
	$.LoadingOverlay("show");
	var cur 		= $(this);
	var input_id	= $(this).data('id');
	var assets_id	= $(this).data('assets');
	var file_data	= $(this).prop('files')[0];
	var form_data	= new FormData();
	form_data.append('file', file_data);
	form_data.append('qImgfiles', '1');
	$.ajax({
		url: "includes/upload.php",
		type: "POST",
		data: form_data,
		contentType: false,
		cache: false,
		processData: false,
		success: function(resdata) {
			var res = $.parseJSON(resdata);
			if (res.success == true) {
				$(input_id).val(res.file_name);
				cur.closest('div').addClass("disabled");
				$('.swipq_item_assets'+ assets_id).addClass("disabled").attr('disabled', true);
				$('.swipq_items_assets_container_'+ assets_id).show();
				$('.swipq_items_assets_data_'+ assets_id + ' a.view_assets').text(res.file_name);
				$('.swipq_items_assets_data_'+ assets_id + ' a.view_assets').attr('data-src', assets_path + res.file_name);
				$('.swipq_items_assets_container_'+ assets_id +' a.delete_assets').attr('data-assets', res.file_name);
				$('.swipq_items_assets_container_'+ assets_id +' a.delete_assets').attr('data-assets-id', 'swipq_item_assets'+ assets_id);
				$('.swipq_items_assets_container_'+ assets_id +' a.delete_assets').attr('data-input-id', input_id);
				$('.swipq_items_assets_container_'+ assets_id +' a.delete_assets').attr('data-path', res.path);
				swal({text: res.msg, buttons: false, timer: 1000});
			}
			else if (res.success == false){
				$(input_id).val('');
				swal({text: res.msg, buttons: false, icon: "warning", timer: 1000});
			}
			$.LoadingOverlay("hide");
		},error: function() {
			$.LoadingOverlay("hide");
			swal("Error", 'Oops, something went wrong. Please try again later.?', "error");
		}
	});
});

//-----------Video-Upload-Swip-Template-----------------
$('body').on('change', '.uploadVideoFileSwip', function() {
	$.LoadingOverlay("show");
	var cur 		= $(this);
	var input_id	= $(this).data('id');
	var assets_id	= $(this).data('assets');
	var file_data	= $(this).prop('files')[0];
	var form_data	= new FormData();
	form_data.append('file', file_data);
	form_data.append('qVideofiles', '1');
	$.ajax({
		url: "includes/upload.php",
		type: "POST",
		data: form_data,
		contentType: false,
		cache: false,
		processData: false,
		success: function(resdata) {
			var res = $.parseJSON(resdata);
			if (res.success == true) {
				$(input_id).val(res.file_name);
				cur.closest('div').addClass("disabled");
				$('.swipq_item_assets'+ assets_id).addClass("disabled").attr('disabled', true);
				$('.swipq_items_assets_container_'+ assets_id).show();
				$('.swipq_items_assets_data_'+ assets_id).text(res.file_name);
				$('.swipq_items_assets_container_'+ assets_id +' a').attr('data-assets', res.file_name);
				$('.swipq_items_assets_container_'+ assets_id +' a').attr('data-assets-id', 'swipq_item_assets'+ assets_id);
				$('.swipq_items_assets_container_'+ assets_id +' a').attr('data-input-id', input_id);
				$('.swipq_items_assets_container_'+ assets_id +' a').attr('data-path', res.path);
				swal({text: res.msg, buttons: false, timer: 1000});
			}
			else if (res.success == false){
				$(input_id).val('');
				swal({text: res.msg, buttons: false, icon: "warning", timer: 1000});
			}
			$.LoadingOverlay("hide");
		},error: function() {
			$.LoadingOverlay("hide");
			swal("Error", 'Oops, something went wrong. Please try again later.?', "error");
		}
	});
});

//-------Image-Upload-Drag-Drop-Template-------------

	//--------------DROP-TARGET-----------------
$('body').on('change', '.uploadImgFileDDq', function() {
	$.LoadingOverlay("show");
	var cur 		= $(this);
	var input_id	= $(this).data('id');
	var assets_id	= $(this).data('assets');
	var file_data	= $(this).prop('files')[0];
	var form_data	= new FormData();
	form_data.append('file', file_data);
	form_data.append('qImgfiles', '1');
	$.ajax({
		url: "includes/upload.php",
		type: "POST",
		data: form_data,
		contentType: false,
		cache: false,
		processData: false,
		success: function(resdata) {
			var res = $.parseJSON(resdata);
			if (res.success == true) {
				$(input_id).val(res.file_name);
				cur.closest('div').addClass("disabled");
				$('.ddq_item_assets'+ assets_id).addClass("disabled").attr('disabled', true);
				$('.ddq_items_assets_container_'+ assets_id).show();
				$('.ddq_items_assets_data_'+ assets_id + ' a.view_assets').text(res.file_name);
				$('.ddq_items_assets_data_'+ assets_id + ' a.view_assets').attr('data-src', assets_path + res.file_name);
				$('.ddq_items_assets_container_'+ assets_id +' a.delete_assets').attr('data-assets', res.file_name);
				$('.ddq_items_assets_container_'+ assets_id +' a.delete_assets').attr('data-assets-id', 'ddq_item_assets'+ assets_id);
				$('.ddq_items_assets_container_'+ assets_id +' a.delete_assets').attr('data-input-id', input_id);
				$('.ddq_items_assets_container_'+ assets_id +' a.delete_assets').attr('data-path', res.path);
				swal({text: res.msg, buttons: false, timer: 1000});
			}
			else if (res.success == false){
				$(input_id).val('');
				swal({text: res.msg, buttons: false, icon: "warning", timer: 1000});
			}
			$.LoadingOverlay("hide");
		},error: function() {
			$.LoadingOverlay("hide");
			swal("Error", 'Oops, something went wrong. Please try again later.?', "error");
		}
	});
});

	//-------------- DRAG-ITEM-----------------
$('body').on('change', '.uploadImgFileDragDDq', function() {
	$.LoadingOverlay("show");
	var cur 		= $(this);
	var input_id	= $(this).data('id');
	var assets_id	= $(this).data('assets');
	var file_data	= $(this).prop('files')[0];
	var form_data	= new FormData();
	form_data.append('file', file_data);
	form_data.append('qImgfiles', '1');
	$.ajax({
		url: "includes/upload.php",
		type: "POST",
		data: form_data,
		contentType: false,
		cache: false,
		processData: false,
		success: function(resdata) {
			var res = $.parseJSON(resdata);
			if (res.success == true) {
				$(input_id).val(res.file_name);
				cur.closest('div').addClass("disabled");
				$('.ddq_drag_item_assets'+ assets_id).addClass("disabled").attr('disabled', true);
				$('.ddq_drag_items_assets_container_'+ assets_id).show();
				$('.ddq_drag_items_assets_data_'+ assets_id + ' a.view_assets').text(res.file_name);
				$('.ddq_drag_items_assets_data_'+ assets_id + ' a.view_assets').attr('data-src', assets_path + res.file_name);
				$('.ddq_drag_items_assets_container_'+ assets_id +' a.delete_assets').attr('data-assets', res.file_name);
				$('.ddq_drag_items_assets_container_'+ assets_id +' a.delete_assets').attr('data-assets-id', 'ddq_drag_item_assets'+ assets_id);
				$('.ddq_drag_items_assets_container_'+ assets_id +' a.delete_assets').attr('data-input-id', input_id);
				$('.ddq_drag_items_assets_container_'+ assets_id +' a.delete_assets').attr('data-path', res.path);
				swal({text: res.msg, buttons: false, timer: 1000});
			}
			else if (res.success == false){
				$(input_id).val('');
				swal({text: res.msg, buttons: false, icon: "warning", timer: 1000});
			}
			$.LoadingOverlay("hide");
		},error: function() {
			$.LoadingOverlay("hide");
			swal("Error", 'Oops, something went wrong. Please try again later.?', "error");
		}
	});
});

	//--------------DROP-TARGET-2----------------
$('body').on('change', '.uploadImgFileDDq2', function() {
	$.LoadingOverlay("show");
	var cur 		= $(this);
	var input_id	= $(this).data('id');
	var assets_id	= $(this).data('assets');
	var file_data	= $(this).prop('files')[0];
	var form_data	= new FormData();
	form_data.append('file', file_data);
	form_data.append('qImgfiles', '1');
	$.ajax({
		url: "includes/upload.php",
		type: "POST",
		data: form_data,
		contentType: false,
		cache: false,
		processData: false,
		success: function(resdata) {
			var res = $.parseJSON(resdata);
			if (res.success == true) {
				$(input_id).val(res.file_name);
				cur.closest('div').addClass("disabled");
				$('.ddq2_item_assets'+ assets_id).addClass("disabled").attr('disabled', true);
				$('.ddq2_items_assets_container_'+ assets_id).show();
				$('.ddq2_items_assets_data_'+ assets_id + ' a.view_assets').text(res.file_name);
				$('.ddq2_items_assets_data_'+ assets_id + ' a.view_assets').attr('data-src', assets_path + res.file_name);
				$('.ddq2_items_assets_container_'+ assets_id +' a.delete_assets').attr('data-assets', res.file_name);
				$('.ddq2_items_assets_container_'+ assets_id +' a.delete_assets').attr('data-assets-id', 'ddq2_item_assets'+ assets_id);
				$('.ddq2_items_assets_container_'+ assets_id +' a.delete_assets').attr('data-input-id', input_id);
				$('.ddq2_items_assets_container_'+ assets_id +' a.delete_assets').attr('data-path', res.path);
				swal({text: res.msg, buttons: false, timer: 1000});
			}
			else if (res.success == false){
				$(input_id).val('');
				swal({text: res.msg, buttons: false, icon: "warning", timer: 1000});
			}
			$.LoadingOverlay("hide");
		},error: function() {
			$.LoadingOverlay("hide");
			swal("Error", 'Oops, something went wrong. Please try again later.?', "error");
		}
	});
});

	//-------------- DRAG-ITEM-2----------------
$('body').on('change', '.uploadImgFileDragDDq2', function() {
	$.LoadingOverlay("show");
	var cur 		= $(this);
	var input_id	= $(this).data('id');
	var assets_id	= $(this).data('assets');
	var file_data	= $(this).prop('files')[0];
	var form_data	= new FormData();
	form_data.append('file', file_data);
	form_data.append('qImgfiles', '1');
	$.ajax({
		url: "includes/upload.php",
		type: "POST",
		data: form_data,
		contentType: false,
		cache: false,
		processData: false,
		success: function(resdata) {
			var res = $.parseJSON(resdata);
			if (res.success == true) {
				$(input_id).val(res.file_name);
				cur.closest('div').addClass("disabled");
				$('.ddq2_drag_item_assets'+ assets_id).addClass("disabled").attr('disabled', true);
				$('.ddq2_drag_items_assets_container_'+ assets_id).show();
				$('.ddq2_drag_items_assets_data_'+ assets_id + ' a.view_assets').text(res.file_name);
				$('.ddq2_drag_items_assets_data_'+ assets_id + ' a.view_assets').attr('data-src', assets_path + res.file_name);
				$('.ddq2_drag_items_assets_container_'+ assets_id +' a.delete_assets').attr('data-assets', res.file_name);
				$('.ddq2_drag_items_assets_container_'+ assets_id +' a.delete_assets').attr('data-assets-id', 'ddq2_drag_item_assets'+ assets_id);
				$('.ddq2_drag_items_assets_container_'+ assets_id +' a.delete_assets').attr('data-input-id', input_id);
				$('.ddq2_drag_items_assets_container_'+ assets_id +' a.delete_assets').attr('data-path', res.path);
				swal({text: res.msg, buttons: false, timer: 1000});
			}
			else if (res.success == false){
				$(input_id).val('');
				swal({text: res.msg, buttons: false, icon: "warning", timer: 1000});
			}
			$.LoadingOverlay("hide");
		},error: function() {
			$.LoadingOverlay("hide");
			swal("Error", 'Oops, something went wrong. Please try again later.?', "error");
		}
	});
});

//-----------Record-Audio-All-Template---------
$('body').on('click', '.rec-audio', function() {
	var input_id  = $(this).attr('data-input-id');
	var assets_id = $(this).attr('data-assets');
	if (input_id != '') {
		$.LoadingOverlay("show");
		var $modal = $('#load_popup_modal_show');
		$modal.load('rec-audio-modal.php', {'data-id': input_id, 'assets-id': assets_id}, function(res) {
			if (res != '') {
				$.LoadingOverlay("hide");
				$modal.modal('show', {backdrop: 'static', keyboard: false});
			}
			else {
				$.LoadingOverlay("hide");
				swal({text: 'Oops, something went wrong. Please try again later', buttons: false, icon: "error", timer: 1000});
			}
		});
	}
});

//-----------Record-Video-All-Template---------
$('body').on('click', '.rec-video', function() {
	var input_id  = $(this).attr('data-input-id');
	var assets_id = $(this).attr('data-assets');
	if (input_id != '') {
		$.LoadingOverlay("show");
		var $modal = $('#load_popup_modal_show');
		$modal.load('rec-video-modal.php', {'data-id': input_id, 'assets-id': assets_id}, function(res) {
			if (res != '') {
				$.LoadingOverlay("hide");
				$modal.modal('show', {backdrop: 'static', keyboard: false});
			}
			else {
				$.LoadingOverlay("hide");
				swal({text: 'Oops, something went wrong. Please try again later', buttons: false, icon: "error", timer: 1000});
			}
		});
	}
});

//-----------Record Screen-All-Template--------
$('body').on('click', '.rec-screen', function() {
	var input_id  = $(this).attr('data-input-id');
	var assets_id = $(this).attr('data-assets');
	if (input_id != '') {
		$.LoadingOverlay("show");
		var $modal = $('#load_popup_modal_show');
		$modal.load('rec-screen-modal.php', {'data-id': input_id, 'assets-id': assets_id}, function(res) {
			if (res != '') {
				$.LoadingOverlay("hide");
				$modal.modal('show', {backdrop: 'static', keyboard: false});
			}
			else {
				$.LoadingOverlay("hide");
				swal({text: 'Oops, something went wrong. Please try again later', buttons: false, icon: "error", timer: 1000});
			}
		});
	}
});

//--------------Delete-Assets-All-Templates--------
$('body').on('click', '.delete_assets', function() {
	var cur   		= $(this);
	var file_name	= cur.attr('data-assets');
	var assetsId	= cur.attr('data-assets-id');
	var inputId		= cur.attr('data-input-id');
	var path		= cur.attr('data-path');
	var divId		= cur.closest('div').attr('class');
	var dataString  = 'delete_assets='+true+'&assets_path='+path+'&file='+file_name;
	if (file_name){
		swal({
			title: "Are you sure?",
			text: "Delete this Assets.",
			icon: "warning",
			buttons: [true, 'Delete'],
			dangerMode: true, }).then((willDelete) => { if (willDelete) {
				$.LoadingOverlay("show");
				$.ajax({
					url: "includes/delete-file.php",
					type: "POST",
					data: dataString,
					cache: false,
					success: function(resdata){
						var res = $.parseJSON(resdata);
						if (res.success == false) {
							$.LoadingOverlay("hide");
							swal({text: res.msg, buttons: false, icon: "error", timer: 1000});
						}
						else if (res.success == true) {
							$(inputId).val('');
							$('.'+ assetsId).removeClass("disabled").removeAttr('disabled', true);
							$('.'+ divId).hide();
							$.LoadingOverlay("hide");
							swal({text: res.msg, buttons: false, timer: 1000});
						}
					},error: function() {
						swal({text: 'Oops. something went wrong please try again.', buttons: false, icon: "error", timer: 1000});
					}
				});
			} else { swal({text: 'Your file is safe', buttons: false, timer: 1000}); }
		});
	}
});

//-----------Image-Upload-Multi-Feedback-Section--------
$('body').on('change', '.uploadImgFileVideo', function() {
	$.LoadingOverlay("show");
	var cur 		= $(this);
	var input_id	= $(this).data('id');
	var assets_id	= $(this).data('assets');
	var file_data	= $(this).prop('files')[0];
	var form_data	= new FormData();
	form_data.append('file', file_data);
	form_data.append('qImgfiles', '1');
	$.ajax({
		url: "includes/upload.php",
		type: "POST",
		data: form_data,
		contentType: false,
		cache: false,
		processData: false,
		success: function(resdata) {
			var res = $.parseJSON(resdata);
			if (res.success == true) {
				$(input_id).val(res.file_name);
				$('.'+ assets_id).addClass("disabled").attr('disabled', true);
				var video	= assets_id.split('_');
				var videoQ	= video[0];
				var videoID	= video[2];
				cur.closest('div').addClass("disabled");
				$('.'+ videoQ +'_assets_container_'+ videoID).show();
				$('.'+ videoQ +'_assets_data_'+ videoID + ' a.view_assets').text(res.file_name);
				$('.'+ videoQ +'_assets_data_'+ videoID + ' a.view_assets').attr('data-src', assets_path + res.file_name);
				$('.'+ videoQ +'_assets_container_'+ videoID +' a.delete_assets').attr('data-assets', res.file_name);
				$('.'+ videoQ +'_assets_container_'+ videoID +' a.delete_assets').attr('data-assets-id', assets_id);
				$('.'+ videoQ +'_assets_container_'+ videoID +' a.delete_assets').attr('data-input-id', input_id);
				$('.'+ videoQ +'_assets_container_'+ videoID +' a.delete_assets').attr('data-path', res.path);
				swal({text: res.msg, buttons: false, timer: 1000});
			}
			else if (res.success == false){
				$(input_id).val('');
				swal({text: res.msg, buttons: false, icon: "warning", timer: 1000});
			}
			$.LoadingOverlay("hide");
		},error: function() {
			$.LoadingOverlay("hide");
			swal("Error", 'Oops, something went wrong. Please try again later.?', "error");
		}
	});
});

//-----------Doc-Upload-Multi-Feedback-Section----------
$('body').on('change', '.uploadDocFileVideo', function() {
	$.LoadingOverlay("show");
	var cur 		= $(this);
	var input_id	= $(this).data('id');
	var assets_id	= $(this).data('assets');
	var file_data	= $(this).prop('files')[0];
	var form_data	= new FormData();
	form_data.append('file', file_data);
	form_data.append('qDocfiles', '1');
	$.ajax({
		url: "includes/upload.php",
		type: "POST",
		data: form_data,
		contentType: false,
		cache: false,
		processData: false,
		success: function(resdata) {
			var res = $.parseJSON(resdata);
			if (res.success == true) {
				$(input_id).val(res.file_name);
				$('.'+ assets_id).addClass("disabled").attr('disabled', true);
				var video	= assets_id.split('_');
				var videoQ	= video[0];
				var videoID	= video[2];
				cur.closest('div').addClass("disabled");
				$('.'+ videoQ +'_assets_container_'+ videoID).show();
				$('.'+ videoQ +'_assets_data_'+ videoID + ' a.view_assets').text(res.file_name);
				$('.'+ videoQ +'_assets_data_'+ videoID + ' a.view_assets').attr('data-src', assets_path + res.file_name);
				$('.'+ videoQ +'_assets_container_'+ videoID +' a.delete_assets').attr('data-assets', res.file_name);
				$('.'+ videoQ +'_assets_container_'+ videoID +' a.delete_assets').attr('data-assets-id', assets_id);
				$('.'+ videoQ +'_assets_container_'+ videoID +' a.delete_assets').attr('data-input-id', input_id);
				$('.'+ videoQ +'_assets_container_'+ videoID +' a.delete_assets').attr('data-path', res.path);
				swal({text: res.msg, buttons: false, timer: 1000});
			}
			else if (res.success == false){
				$(input_id).val('');
				swal({text: res.msg, buttons: false, icon: "warning", timer: 1000});
			}
			$.LoadingOverlay("hide");
		},error: function() {
			$.LoadingOverlay("hide");
			swal("Error", 'Oops, something went wrong. Please try again later.?', "error");
		}
	});
});

//-----------Video-Upload-Multi-Feedback-Section---------
$('body').on('change', '.uploadFileVideoTemp', function() {
	$.LoadingOverlay("show");
	var input_id	= $(this).data('id');
	var assets_id	= $(this).data('assets');
	var file_data	= $(this).prop('files')[0];
	var form_data	= new FormData();
	form_data.append('file', file_data);
	form_data.append('qVideofiles', '1');
	$.ajax({
		url: "includes/upload.php",
		type: "POST",
		data: form_data,
		contentType: false,
		cache: false,
		processData: false,
		success: function(resdata) {
			var res = $.parseJSON(resdata);
			if (res.success == true) {
				$(input_id).val(res.file_name);
				$('.'+ assets_id).addClass("disabled").attr('disabled', true);
				var video	= assets_id.split('_');
				var videoQ	= video[0];
				var videoID	= video[2];
				$('.'+ videoQ +'_assets_container_'+ videoID).show();
				$('.'+ videoQ +'_assets_data_'+ videoID + ' a.view_assets').text(res.file_name);
				$('.'+ videoQ +'_assets_data_'+ videoID + ' a.view_assets').attr('data-src', assets_path + res.file_name);
				$('.'+ videoQ +'_assets_container_'+ videoID +' a.delete_assets').attr('data-assets', res.file_name);
				$('.'+ videoQ +'_assets_container_'+ videoID +' a.delete_assets').attr('data-assets-id', assets_id);
				$('.'+ videoQ +'_assets_container_'+ videoID +' a.delete_assets').attr('data-input-id', input_id);
				$('.'+ videoQ +'_assets_container_'+ videoID +' a.delete_assets').attr('data-path', res.path);
				swal({text: res.msg, buttons: false, timer: 1000});
			}
			else if (res.success == false){
				$(input_id).val('');
				swal({text: res.msg, buttons: false, icon: "warning", timer: 1000});
			}
			$.LoadingOverlay("hide");
		},error: function() {
			$.LoadingOverlay("hide");
			swal("Error", 'Oops, something went wrong. Please try again later.?', "error");
		}
	});
});

//----------Audio-Upload-Multi-Feedback-Section--------------
$('body').on('change', '.uploadAudioFileVideoTemp', function(){
	$.LoadingOverlay("show");
	var input_id	= $(this).data('id');
	var assets_id	= $(this).data('assets');
	var file_data	= $(this).prop('files')[0];
	var form_data	= new FormData();
	form_data.append('file', file_data);
	form_data.append('qAudiofiles', '1');
	$.ajax({
		url: "includes/upload.php",
		type: "POST",
		data: form_data,
		contentType: false,
		cache: false,
		processData: false,
		success: function(resdata) {
			var res = $.parseJSON(resdata);
			if (res.success == true) {
				$(input_id).val(res.file_name);
				$('.'+ assets_id).addClass("disabled").attr('disabled', true);
				var video	= assets_id.split('_');
				var videoQ	= video[0];
				var videoID	= video[2];
				$('.'+ videoQ +'_assets_container_'+ videoID).show();
				$('.'+ videoQ +'_assets_data_'+ videoID + ' a.view_assets').text(res.file_name);
				$('.'+ videoQ +'_assets_data_'+ videoID + ' a.view_assets').attr('data-src', assets_path + res.file_name);
				$('.'+ videoQ +'_assets_container_'+ videoID +' a.delete_assets').attr('data-assets', res.file_name);
				$('.'+ videoQ +'_assets_container_'+ videoID +' a.delete_assets').attr('data-assets-id', assets_id);
				$('.'+ videoQ +'_assets_container_'+ videoID +' a.delete_assets').attr('data-input-id', input_id);
				$('.'+ videoQ +'_assets_container_'+ videoID +' a.delete_assets').attr('data-path', res.path);
				swal({text: res.msg, buttons: false, timer: 1000});
			}
			else if (res.success == false){
				$(input_id).val('');
				swal({text: res.msg, buttons: false, icon: "warning", timer: 1000});
			}
			$.LoadingOverlay("hide");
		},error: function() {
			$.LoadingOverlay("hide");
			swal("Error", 'Oops, something went wrong. Please try again later.?', "error");
		}
	});
});	
	
//------Record-Audio-Video-Question-Template---------
$('body').on('click', '.rec-audio-videoq', function() {
	var input_id  = $(this).attr('data-input-id');
	var assets_id = $(this).attr('data-assets');
	if (input_id != '') {
		$.LoadingOverlay("show");
		var $modal = $('#load_popup_modal_show');
		$modal.load('rec-audio-modal.php', {'data-id': input_id, 'assets-id': assets_id, 'video': true}, function(res) {
			if (res != '') {
				$.LoadingOverlay("hide");
				$modal.modal('show', {backdrop: 'static', keyboard: false});
			}
			else {
				$.LoadingOverlay("hide");
				swal({text: 'Oops, something went wrong. Please try again later', buttons: false, icon: "error", timer: 1000});
			}
		});
	}
});

//------Record-Video-Video-Question-Template---------
$('body').on('click', '.rec-video-videoq', function() {
	var input_id  = $(this).attr('data-input-id');
	var assets_id = $(this).attr('data-assets');
	if (input_id != '') {
		$.LoadingOverlay("show");
		var $modal = $('#load_popup_modal_show');
		$modal.load('rec-video-modal.php', {'data-id': input_id, 'assets-id': assets_id, 'video': true}, function(res) {
			if (res != '') {
				$.LoadingOverlay("hide");
				$modal.modal('show', {backdrop: 'static', keyboard: false});
			}
			else {
				$.LoadingOverlay("hide");
				swal({text: 'Oops, something went wrong. Please try again later', buttons: false, icon: "error", timer: 1000});
			}
		});
	}
});

//-----Record-Screen-Video-Question-Template----------
$('body').on('click', '.rec-screen-videoq', function() {
	var input_id  = $(this).attr('data-input-id');
	var assets_id = $(this).attr('data-assets');
	if (input_id != '') {
		$.LoadingOverlay("show");
		var $modal = $('#load_popup_modal_show');
		$modal.load('rec-screen-modal.php', {'data-id': input_id, 'assets-id': assets_id, 'video': true}, function(res) {
			if (res != '') {
				$.LoadingOverlay("hide");
				$modal.modal('show', {backdrop: 'static', keyboard: false});
			}
			else {
				$.LoadingOverlay("hide");
				swal({text: 'Oops, something went wrong. Please try again later', buttons: false, icon: "error", timer: 1000});
			}
		});
	}
});

var x = <?php echo ( ! empty($i)) ? $i : 1; ?>;
var maxField = 6;
$(".plus_comp").click(function() {
	if (x < maxField) {
		x++;
		$(".plus_comp_option").append('<div class="add_comp_option linaer">\
		<div class="form-group"><input type="text" class="form-control control1" placeholder="Competency Name" name="competency_name_'+x+'" autocomplete="off" required="required"></div>\
		<div class="form-group labelbox"><label>Competency Score</label><input type="text" class="form-control question-box" name="competency_score_'+x+'" onkeypress="return isNumberKey(event);" autocomplete="off" required="required"></div>\
		<div class="form-group labelbox"><label>Weightage</label><input type="text" class="form-control question-box" name="weightage_'+x+'" onkeypress="return isNumberKey(event);" autocomplete="off" required="required"></div><div class="remove minusComp"><img src="img/icons/minus.png" title="Remove"></div></div>');
		var getMax = $('.add_comp_option').length;
		if (getMax >= maxField) {
			$('.plus_comp').addClass('disabled');
		}
		else { $('.plus_comp').removeClass('disabled'); }
	}
	else {
		$('.plus_comp').addClass('disabled');
	}
});

$('.plus_comp_option').on('click', '.remove', function(e) {
	e.preventDefault();
	$(this).parent().remove();
	x--;
	$('.plus_comp').removeClass('disabled');
});

$('.suffleCheck').click(function() {
	if ($(".suffleCheck").is(":checked") == true) { 
		$('.AnsHeading.Mchoice').text('CORRECT CHOICE'); 
		$('.tooltiptop .tooltiptext').text('Shuffle ON'); 
		$('.tooltiptop .tooltiptext').css('display', 'block'); 
		$('.matchingchice').css('display', 'none');
		$('.matchingchicesuffle').css('display', 'block');
		
	} else { 
		$('.AnsHeading.Mchoice').text('CHOICE'); 
		$('.tooltiptop .tooltiptext').text('Shuffle OFF'); 
		$('.tooltiptop .tooltiptext').delay(2000).fadeOut(300);
		$('.matchingchicesuffle').css('display', 'none')
		$('.matchingchice').css('display', 'block') 
	} 
});

$(".closerightmenuA").click(function(e) {
	e.preventDefault();
	$("#menu-toggle").removeClass("changebtn");
	$("#wrapper").removeClass("toggled");
	$(".match-Q.tab-pane.fade").removeClass("tabpadding150");
	$(".main_sim_scroll").removeClass("TogglewidthQ100");
	$(".widthQ100").removeClass("TogglewidthQ100");
	$(".modal.Ques-pop-style.in ").removeClass("TogglewidthQ100");
});

$("#menu-toggle").click(function(e) {
	e.preventDefault();
	$("#menu-toggle").toggleClass("changebtn");
	$("#wrapper").toggleClass("toggled");
	$(".match-Q.tab-pane.fade").toggleClass("tabpadding150");
	$(".main_sim_scroll").toggleClass("TogglewidthQ100");
	$(".widthQ100").toggleClass("TogglewidthQ100");
	$(".modal.Ques-pop-style.in ").toggleClass("TogglewidthQ100");
});

$(".closeleftmenuA").click(function(e) {
	e.preventDefault();
	$("#list-toggle").addClass("changebtnleft");
	$("#wrapper").addClass("toggledlist");
	$(".match-Q.tab-pane.fade").addClass("tabpadding150");
	$(".Q-left").addClass("left150");
	$(".widthQ100").addClass("TogglelistwidthQ100");
	$(".main_sim_scroll").addClass("TogglelistwidthQ100");
	$(".modal.Ques-pop-style.in ").addClass("TogglelistwidthQ100");
    $(".list-icon").addClass("Labsolute");
	$(".panel-Mult_video").addClass("panel-Mult_video100");
});

$("#list-toggle").click(function(e) {
	e.preventDefault();
	$("#list-toggle").toggleClass("changebtnleft");
	$("#wrapper").toggleClass("toggledlist");
	$(".match-Q.tab-pane.fade").toggleClass("tabpadding150");
	$(".Q-left").toggleClass("left150");
	$(".widthQ100").toggleClass("TogglelistwidthQ100");
	$(".main_sim_scroll").toggleClass("TogglelistwidthQ100");
	$(".modal.Ques-pop-style.in ").toggleClass("TogglelistwidthQ100");
    $(".list-icon").toggleClass("Labsolute");
	$(".panel-Mult_video").toggleClass("panel-Mult_video100");
});

$(".VideoQ").click(function(e) {
	e.preventDefault();
	$("#menu-toggle").removeClass("changebtn");
	$("#wrapper").removeClass("toggled");
	$(".match-Q.tab-pane.fade").removeClass("tabpadding150");
	$(".main_sim_scroll").removeClass("TogglewidthQ100");
	$(".widthQ100").removeClass("TogglewidthQ100");
	$(".modal.Ques-pop-style.in").removeClass("TogglewidthQ100");
});

$(".MCQQ").click(function(e) {
	e.preventDefault();
	$("#menu-toggle").removeClass("changebtn");
	$("#wrapper").removeClass("toggled");
	$(".match-Q.tab-pane.fade").removeClass("tabpadding150");
	$(".main_sim_scroll").removeClass("TogglewidthQ100");
	$(".widthQ100").removeClass("TogglewidthQ100");
	$(".modal.Ques-pop-style.in").removeClass("TogglewidthQ100");
});

$(".SwapingQ").click(function(e) {
	e.preventDefault();
	$("#menu-toggle").removeClass("changebtn");
	$("#wrapper").removeClass("toggled");
	$(".match-Q.tab-pane.fade").removeClass("tabpadding150");
	$(".main_sim_scroll").removeClass("TogglewidthQ100");
	$(".widthQ100").removeClass("TogglewidthQ100");
	$(".modal.Ques-pop-style.in").removeClass("TogglewidthQ100");
});

//-----------------------DNDQuestion----------------//
var dd2i = $('.DND2ApendOption-box .DNDOption2').length;
$(".Add_Drag").click(function(){
	dd2i++;
	$(".DNDOption2:last").after('<div class="DNDOption2">\
		<div class="Radio-box classtmpR">\
			<label class="radiostyle">\
				<input type="radio" name="ddq_true_option" value="'+dd2i+'" />\
				<span class="radiomark"></span>\
			</label>\
			<div class="col-sm-12 mcwidth">\
				<div class="choicebox">\
					<div class="form-group">\
						<input type="text" name="ddq2_drag[]" class="form-control inputtextWrap" placeholder="Drag text goes here" />\
						<div class="ddq2_drag_items_assets_container_'+dd2i+'" id="ddq2_drag_items_assets" style="display:none;">\
							<div class="assets_data ddq2_drag_items_assets_data_'+dd2i+'"><a class="view_assets" data-src="" href="javascript:;"></a></div>\
							<a href="javascript:void(0);" data-assets="" data-assets-id="ddq2_drag_item_assets'+dd2i+'" data-input-id="#ddq2_drag_item_img'+dd2i+'" data-path="<?php echo $uploadpath ?>" class="delete_assets" title="Delete"><i class="fa fa-times" aria-hidden="true"></i></a>\
						</div>\
						<ul class="QueBoxIcon">\
							<li>\
								<div class="tooltip uploadicon ddq2_drag_item_assets'+dd2i+'">\
									<label for="file-input-ddq2-drag-item-addImg'+dd2i+'">\
										<img class="img-fluid" src="img/list/image.svg" />\
										<span class="tooltiptext">Add Image</span>\
									</label>\
									<input id="file-input-ddq2-drag-item-addImg'+dd2i+'" data-id="#ddq2_drag_item_img'+dd2i+'" data-assets="'+dd2i+'" class="uploadImgFileDragDDq2 ddq2_drag_item_assets'+dd2i+'" type="file" name="ddq2_drag_Img'+dd2i+'" />\
									<input type="hidden" name="ddq2_drag_item_img[]" id="ddq2_drag_item_img'+dd2i+'" />\
								</div>\
							</li>\
						</ul>\
					</div>\
				</div>\
			</div>\
			<div class="AddAns-Option DD2Remove"><img class="img-fluid" src="img/list/delete_field.svg"></div>\
		</div>\
		<div class="classicQ classicFEED">\
			<div class="q-icon-banner clearfix">\
				<div class="Ques-comp">\
					<h5>Add Competency score</h5>\
					<?php if ( ! empty($sim_com['comp_col_1']) && ! empty($sim_com['comp_val_1'])): ?>
					<div class="form-group QusScore"><input type="text" class="form-control score dd2" name="dd2_ans_val1[]" id="dd2_ans_val1" onkeypress="return isNumberKey(event);" /></div>\
					<?php endif;
					if ( ! empty($sim_com['comp_col_2']) && ! empty($sim_com['comp_val_2'])): ?>
					<div class="form-group QusScore"><input type="text" class="form-control score dd2" name="dd2_ans_val2[]" id="dd2_ans_val2" onkeypress="return isNumberKey(event);" /></div>\
					<?php endif;
					if ( ! empty($sim_com['comp_col_3']) && ! empty($sim_com['comp_val_3'])): ?>
					<div class="form-group QusScore"><input type="text" class="form-control score dd2" name="dd2_ans_val3[]" id="dd2_ans_val3" onkeypress="return isNumberKey(event);" /></div>\
					<?php endif;
					if ( ! empty($sim_com['comp_col_4']) && ! empty($sim_com['comp_val_4'])): ?>
					<div class="form-group QusScore"><input type="text" class="form-control score dd2" name="dd2_ans_val4[]" id="dd2_ans_val4" onkeypress="return isNumberKey(event);" /></div>\
					<?php endif;
					if ( ! empty($sim_com['comp_col_5']) && ! empty($sim_com['comp_val_5'])): ?>
					<div class="form-group QusScore"><input type="text" class="form-control score dd2" name="dd2_ans_val5[]" id="dd2_ans_val5" onkeypress="return isNumberKey(event);" /></div>\
					<?php endif;
					if ( ! empty($sim_com['comp_col_6']) && ! empty($sim_com['comp_val_6'])): ?>
					<div class="form-group QusScore"><input type="text" class="form-control score dd2" name="dd2_ans_val6[]" id="dd2_ans_val6" onkeypress="return isNumberKey(event);" /></div>\
					<?php endif; ?>
				</div>\
			</div>\
			<div class="form-group feed">\
				<h5>Feedback</h5>\
				<textarea class="form-control FeedForm inputtextWrap dd2" name="dd2_cfeedback[]" id="dd2_cfeedback" placeholder="Feedback text goes here."></textarea>\
				<div class="dd2f_assets_container_'+dd2i+'" style="display:none;">\
					<div class="assets_data dd2f_assets_data_'+dd2i+'"><a class="view_assets" data-src="" href="javascript:;"></a></div>\
					<a href="javascript:void(0);" data-assets="" data-assets-id="" data-input-id="" data-path="<?php echo $uploadpath ?>" class="delete_assets" title="Delete"><i class="fa fa-times" aria-hidden="true"></i></a>\
				</div>\
				<ul class="QueBoxIcon feedQ">\
					<h5>Add assets</h5>\
					<li class="dropdown">\
						<button class="btn1 btn-secondary dropdown-toggle dd2f_assets_'+dd2i+'" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img class="img-fluid" src="img/list/audio.svg"></button>\
						<div class="dropdown-menu buttom" aria-labelledby="dropdownMenuButton">\
                            <a data-toggle="modal" data-target="#dd2fctts'+dd2i+'" class="dropdown-item">\
								<div class="tooltip1 tooltip'+dd2i+'">\
									<img class="img-fluid" src="img/list/text_speech.svg" />\
									<span class="tooltiptext">Text to Speech</span>\
								</div>\
							</a>\
							<a class="dropdown-item">\
								<div class="tooltip1 uploadicon">\
									<label for="file-input-dd2-feedback-addAudio-'+dd2i+'">\
										<img class="img-fluid" src="img/list/add_audio.svg" />\
										<span class="tooltiptext">Add Audio</span>\
									</label>\
									<input id="file-input-dd2-feedback-addAudio-'+dd2i+'" data-id="#dd2_feedback_audio_'+dd2i+'" data-assets="dd2f_assets_'+dd2i+'" class="uploadAudioFileVideoTemp" type="file" name="dd2_feedback_Audio_'+dd2i+'" />\
									<input type="hidden" name="dd2_feedback_audio[]" id="dd2_feedback_audio_'+dd2i+'" />\
								</div>\
							</a>\
							<a class="dropdown-item">\
								<div class="tooltip1">\
									<img class="img-fluid rec-audio-videoq" data-input-id="dd2_feedback_rec_audio_'+dd2i+'" data-assets="dd2f_assets_'+dd2i+'" src="img/list/record_audio.svg" />\
									<span class="tooltiptext">Record Audio</span>\
								</div>\
								<input type="hidden" name="dd2_feedback_rec_audio[]" id="dd2_feedback_rec_audio_'+dd2i+'" />\
							 </a>\
						</div>\
					</li>\
					<li class="dropdown">\
						<button class="btn1 btn-secondary dropdown-toggle dd2f_assets_'+dd2i+'" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img class="img-fluid" src="img/list/video.svg"></button>\
						<div class="dropdown-menu buttom" aria-labelledby="dropdownMenuButton">\
							<a class="dropdown-item">\
								<div class="tooltip1 uploadicon">\
									<label for="file-input-dd2-feedback-addVideo-'+dd2i+'">\
										<img class="img-fluid" src="img/list/add_video.svg">\
										<span class="tooltiptext">Add Video</span>\
									</label>\
									<input id="file-input-dd2-feedback-addVideo-'+dd2i+'" data-id="#dd2_feedback_video_'+dd2i+'" data-assets="dd2f_assets_'+dd2i+'" class="uploadFileVideoTemp" type="file" name="dd2_feedback_Video_'+dd2i+'" />\
									<input type="hidden" name="dd2_feedback_video[]" id="dd2_feedback_video_'+dd2i+'" />\
								</div>\
							</a>\
							<a class="dropdown-item">\
								<div class="tooltip1">\
									<img class="img-fluid rec-video-videoq" data-input-id="dd2_feedback_rec_video_'+dd2i+'" data-assets="dd2f_assets_'+dd2i+'" src="img/qus_icon/rec_video.svg">\
									<span class="tooltiptext">Record Video</span>\
								 </div>\
								 <input type="hidden" name="dd2_feedback_rec_video[]" id="dd2_feedback_rec_video_'+dd2i+'" />\
							</a>\
							<a class="dropdown-item">\
								<div class="tooltip1">\
									<img class="img-fluid rec-screen-videoq" data-input-id="dd2_feedback_rec_screen_'+dd2i+'" data-assets="dd2f_assets_'+dd2i+'" src="img/list/screen_record.svg">\
									<span class="tooltiptext">Record Screen</span>\
								 </div>\
								 <input type="hidden" name="dd2_feedback_rec_screen[]" id="dd2_feedback_rec_screen_'+dd2i+'" />\
							</a>\
						</div>\
					</li>\
					<li>\
						<div class="tooltip uploadicon dd2f_assets_'+dd2i+'">\
							<label for="file-input-dd2-feedback-addImg-'+dd2i+'">\
								<img class="img-fluid" src="img/list/image.svg">\
								<span class="tooltiptext">Add Image</span>\
							</label>\
							<input id="file-input-dd2-feedback-addImg-'+dd2i+'" data-id="#dd2_feedback_img_'+dd2i+'" data-assets="dd2f_assets_'+dd2i+'" class="uploadImgFileVideo dd2f_assets_'+dd2i+'" type="file" name="dd2_feedback_Img_'+dd2i+'" />\
							<input type="hidden" name="dd2_feedback_img[]" id="dd2_feedback_img_'+dd2i+'" />\
						</div>\
					</li>\
					<li>\
						<div class="tooltip uploadicon dd2f_assets_'+dd2i+'">\
							<label for="file-input-dd2-feedback-addDoc-'+dd2i+'">\
								<img class="img-fluid" src="img/list/add_document.svg" />\
								<span class="tooltiptext">Add Document</span>\
							</label>\
							<input id="file-input-dd2-feedback-addDoc-'+dd2i+'" data-id="#dd2_feedback_doc_'+dd2i+'" data-assets="dd2f_assets_'+dd2i+'" class="uploadDocFileVideo dd2f_assets_'+dd2i+'" type="file" name="dd2_feedback_Doc_'+dd2i+'" />\
							<input type="hidden" name="dd2_feedback_doc[]" id="dd2_feedback_doc_'+dd2i+'" />\
						</div>\
					</li>\
				</ul>\
				<div class="form-popup draggable Ques-pop-style modal" id="dd2fctts'+dd2i+'">\
					<div class="popheading">Insert Text to Speech</div>\
					<div class="textarea">\
						<textarea type="text" class="form-control1 inputtextWrap" name="dd2_feedback_text_to_speech[]" id="dd2_feedback_text_to_speech_'+dd2i+'"></textarea>\
					</div>\
					<div class="modal-footer">\
						<button type="button" class="btn1 submitbtn1" data-dismiss="modal">Insert</button>\
						<button type="button" class="btn1 submitbtn1" onclick="clearData("dd2_feedback_text_to_speech_'+dd2i+'");">Clear</button>\
						<button type="button" class="btn1 submitbtn1" data-dismiss="modal">Cancel</button>\
					</div>\
				</div>\
			 </div>\
		</div>\
	</div>');
});

$('body').on('click', '.AddAns-Option.DD2Remove', function() {
	$(this).parent().parent().remove();
	dd2i--;
});

//------------------MCQQuestion----------------//
var mcqi = $('.MCQApendOption-box .MCQRadio').length;
$(".MCQadd").click(function(){
	mcqi++;
	$(".MCQRadio:last").after('<div class="MCQRadio">\
	<div class="Radio-box classtmpR">\
		<label class="radiostyle">\
			<input type="radio" name="mcq_true_option" value="'+mcqi+'">\
			<span class="radiomark"></span>\
		</label>\
		<div class="col-sm-12 mcwidth">\
			<div class="choicebox">\
				<div class="form-group">\
					<input type="text" name="mcq_option[]" id="mcq_option" class="form-control inputtextWrap mcq" placeholder="Option text goes here" />\
				</div>\
			</div>\
		</div>\
		<div class="AddAns-Option MCQRemove"><img class="img-fluid" src="img/list/delete_field.svg"></div>\
	</div>\
	<div class="classicQ classicFEED">\
		<div class="q-icon-banner clearfix">\
			<div class="Ques-comp">\
				<h5>Add Competency score</h5>\
				<?php if ( ! empty($sim_com['comp_col_1']) &&  ! empty($sim_com['comp_val_1'])): ?>
				<div class="form-group QusScore"><input type="text" class="form-control score mcq" name="mcq_ans_val1[]" id="mcq_ans_val1" onkeypress="return isNumberKey(event);" /></div>\
				<?php endif;
				if ( ! empty($sim_com['comp_col_2']) &&  ! empty($sim_com['comp_val_2'])): ?>
				<div class="form-group QusScore"><input type="text" class="form-control score mcq" name="mcq_ans_val2[]" id="mcq_ans_val2" onkeypress="return isNumberKey(event);" /></div>\
				<?php endif;
				if ( ! empty($sim_com['comp_col_3']) &&  ! empty($sim_com['comp_val_3'])): ?>
				<div class="form-group QusScore"><input type="text" class="form-control score mcq" name="mcq_ans_val3[]" id="mcq_ans_val3" onkeypress="return isNumberKey(event);" /></div>\
				<?php endif;
				if ( ! empty($sim_com['comp_col_4']) &&  ! empty($sim_com['comp_val_4'])): ?>
				<div class="form-group QusScore"><input type="text" class="form-control score mcq" name="mcq_ans_val4[]" id="mcq_ans_val4" onkeypress="return isNumberKey(event);" /></div>\
				<?php endif;
				if ( ! empty($sim_com['comp_col_5']) &&  ! empty($sim_com['comp_val_5'])): ?>
				<div class="form-group QusScore"><input type="text" class="form-control score mcq" name="mcq_ans_val5[]" id="mcq_ans_val5" onkeypress="return isNumberKey(event);" /></div>\
				<?php endif;
				if ( ! empty($sim_com['comp_col_6']) &&  ! empty($sim_com['comp_val_6'])): ?>
				<div class="form-group QusScore"><input type="text" class="form-control score mcq" name="mcq_ans_val6[]" id="mcq_ans_val6" onkeypress="return isNumberKey(event);" /></div>\
				<?php endif; ?>
			</div>\
		</div>\
		<div class="form-group feed">\
			<h5>Feedback</h5>\
			<textarea class="form-control FeedForm inputtextWrap mcq" name="mcq_cfeedback[]" id="mcq_cfeedback" placeholder="Feedback text goes here."></textarea>\
			<div class="mcqf_assets_container_'+mcqi+'" style="display:none;">\
				<div class="assets_data mcqf_assets_data_'+mcqi+'"><a class="view_assets" data-src="" href="javascript:;"></a></div>\
				<a href="javascript:void(0);" data-assets="" data-assets-id="" data-input-id="" data-path="<?php echo $uploadpath ?>" class="delete_assets" title="Delete"><i class="fa fa-times" aria-hidden="true"></i></a>\
			</div>\
			<ul class="QueBoxIcon feedQ">\
				<h5>Add assets</h5>\
				<li class="dropdown">\
					<button class="btn1 btn-secondary dropdown-toggle mcqf_assets_'+mcqi+'" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img class="img-fluid" src="img/list/audio.svg"></button>\
					<div class="dropdown-menu buttom" aria-labelledby="dropdownMenuButton">\
						<a data-toggle="modal" data-target="#mcqfctts'+mcqi+'" class="dropdown-item">\
							<div class="tooltip1">\
								<img class="img-fluid" src="img/list/text_speech.svg"/>\
								<span class="tooltiptext">Text to Speech</span>\
							</div>\
						</a>\
						<a class="dropdown-item">\
							<div class="tooltip1 uploadicon">\
								<label for="file-input-mcq-feedback-addAudio-'+mcqi+'">\
									<img class="img-fluid" src="img/list/add_audio.svg" />\
									<span class="tooltiptext">Add Audio</span>\
								</label>\
								<input id="file-input-mcq-feedback-addAudio-'+mcqi+'" data-id="#mcq_feedback_audio_'+mcqi+'" data-assets="mcqf_assets_'+mcqi+'" class="uploadAudioFileVideoTemp" type="file" name="mcq_feedback_Audio_'+mcqi+'" />\
								<input type="hidden" name="mcq_feedback_audio[]" id="mcq_feedback_audio_'+mcqi+'" />\
							</div>\
						</a>\
						<a class="dropdown-item">\
							<div class="tooltip1">\
								<img class="img-fluid rec-audio-videoq" data-input-id="mcq_feedback_rec_audio_'+mcqi+'" data-assets="mcqf_assets_'+mcqi+'" src="img/list/record_audio.svg" />\
								<span class="tooltiptext">Record Audio</span>\
							</div>\
							<input type="hidden" name="mcq_feedback_rec_audio[]" id="mcq_feedback_rec_audio_'+mcqi+'" />\
						 </a>\
					</div>\
				</li>\
				<li class="dropdown">\
					<button class="btn1 btn-secondary dropdown-toggle mcqf_assets_'+mcqi+'" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img class="img-fluid" src="img/list/video.svg"></button>\
					<div class="dropdown-menu buttom" aria-labelledby="dropdownMenuButton">\
						<a class="dropdown-item">\
							<div class="tooltip1 uploadicon">\
								<label for="file-input-mcq-feedback-addVideo-'+mcqi+'">\
									<img class="img-fluid" src="img/list/add_video.svg">\
									<span class="tooltiptext">Add Video</span>\
								</label>\
								<input id="file-input-mcq-feedback-addVideo-'+mcqi+'" data-id="#mcq_feedback_video_'+mcqi+'" data-assets="mcqf_assets_'+mcqi+'" class="uploadFileVideoTemp" type="file" name="mcq_feedback_Video_'+mcqi+'" />\
								<input type="hidden" name="mcq_feedback_video[]" id="mcq_feedback_video_'+mcqi+'" />\
							</div>\
						</a>\
						<a class="dropdown-item">\
							<div class="tooltip1">\
								<img class="img-fluid rec-video-videoq" data-input-id="mcq_feedback_rec_video_'+mcqi+'" data-assets="mcqf_assets_'+mcqi+'" src="img/qus_icon/rec_video.svg">\
								<span class="tooltiptext">Record Video</span>\
							 </div>\
							 <input type="hidden" name="mcq_feedback_rec_video[]" id="mcq_feedback_rec_video_'+mcqi+'" />\
						</a>\
						<a class="dropdown-item">\
							<div class="tooltip1">\
								<img class="img-fluid rec-screen-videoq" data-input-id="mcq_feedback_rec_screen_'+mcqi+'" data-assets="mcqf_assets_'+mcqi+'" src="img/list/screen_record.svg">\
								<span class="tooltiptext">Record Screen</span>\
							 </div>\
							 <input type="hidden" name="mcq_feedback_rec_screen[]" id="mcq_feedback_rec_screen_'+mcqi+'" />\
						</a>\
					</div>\
				</li>\
				<li>\
					<div class="tooltip uploadicon mcqf_assets_'+mcqi+'">\
						<label for="file-input-mcq-feedback-addImg-'+mcqi+'">\
							<img class="img-fluid" src="img/list/image.svg">\
							<span class="tooltiptext">Add Image</span>\
						</label>\
						<input id="file-input-mcq-feedback-addImg-'+mcqi+'" data-id="#mcq_feedback_img_'+mcqi+'" data-assets="mcqf_assets_'+mcqi+'" class="uploadImgFileVideo mcqf_assets_'+mcqi+'" type="file" name="mcq_feedback_Img_'+mcqi+'" />\
						<input type="hidden" name="mcq_feedback_img[]" id="mcq_feedback_img_'+mcqi+'" />\
					</div>\
				</li>\
				<li>\
					<div class="tooltip uploadicon mcqf_assets_'+mcqi+'">\
						<label for="file-input-mcq-feedback-addDoc-'+mcqi+'">\
							<img class="img-fluid" src="img/list/add_document.svg" />\
							<span class="tooltiptext">Add Document</span>\
						</label>\
						<input id="file-input-mcq-feedback-addDoc-'+mcqi+'" data-id="#mcq_feedback_doc_'+mcqi+'" data-assets="mcqf_assets_'+mcqi+'" class="uploadDocFileVideo mcqf_assets_'+mcqi+'" type="file" name="mcq_feedback_Doc_'+mcqi+'" />\
						<input type="hidden" name="mcq_feedback_doc[]" id="mcq_feedback_doc_'+mcqi+'" />\
					</div>\
				</li>\
			</ul>\
			<div class="form-popup draggable Ques-pop-style modal" id="mcqfctts'+mcqi+'">\
				<div class="popheading">Insert Text to Speech</div>\
				<div class="textarea">\
					<textarea type="text" class="form-control1 inputtextWrap" name="mcq_feedback_text_to_speech[]" id="mcq_feedback_text_to_speech_'+mcqi+'"></textarea>\
				</div>\
				<div class="modal-footer">\
					<button type="button" class="btn1 submitbtn1" data-dismiss="modal">Insert</button>\
					<button type="button" class="btn1 submitbtn1" onclick="clearData("mcq_feedback_text_to_speech_'+mcqi+'");">Clear</button>\
					<button type="button" class="btn1 submitbtn1" data-dismiss="modal">Cancel</button>\
				</div>\
			</div>\
		</div>\
	</div></div>');
});

$('body').on('click', '.AddAns-Option.MCQRemove', function() {
	$(this).parent().parent().remove();
	mcqi--;
});

//-----------SWIPING ----------------//
var swipI = $('.Swap-box').length;
$(".swipAdd").click(function(){
	swipI++;
	$(".Swap-box:last").after('<div class="Swap-box">\
	<div class="Radio-boxclasstmpR classicChAling">\
		<label class="radiostyle"><input type="radio" name="swipq_true_option" value="'+swipI+'" /><span class="radiomark"></span></label>\
		<div class="col-sm-12 mcwidth">\
			<div class="choicebox">\
				<div class="form-group">\
					<input type="text" name="swipq_option[]" id="swipq_option" class="form-control QuesForm1 inputtextWrap swipq" placeholder="Click here to enter question" />\
					<ul class="QueBoxIcon">\
						<li>\
							<div class="tooltip uploadicon swipq_item_assets'+swipI+'">\
								<label for="file-input-swipq-item-addImg'+swipI+'">\
									<img class="img-fluid" src="img/list/image.svg" />\
									<span class="tooltiptext">Add Image</span>\
								</label>\
								<input id="file-input-swipq-item-addImg'+swipI+'" data-id="#swipq_item_img'+swipI+'" data-assets="'+swipI+'" class="uploadImgFileSwip swipq_item_assets'+swipI+'" type="file" name="swipq_item_Img'+swipI+'" />\
								<input type="hidden" name="swipq_item_img[]" id="swipq_item_img'+swipI+'" />\
							</div>\
						</li>\
					</ul>\
				</div>\
			</div>\
		</div>\
		<div class="AddAns-Option"><img class="img-fluid SwipRemove" src="img/list/delete_field.svg"></div>\
	</div>\
	<div class="classicQ classicFEED">\
		<div class="q-icon-banner clearfix">\
            <div class="swipq_items_assets_container_'+swipI+'" id="swipq_items_assets" style="display:none">\
				<div class="assets_data swipq_items_assets_data_'+swipI+'"><a class="view_assets" data-src="" href="javascript:;"></a></div>\
				<a href="javascript:void(0);" data-assets="" data-assets-id="swipq_item_assets'+swipI+'" data-input-id="#swipq_item_img'+swipI+'" data-path="<?php echo $uploadpath ?>" class="delete_assets" title="Delete"><i class="fa fa-times" aria-hidden="true"></i></a>\
			</div>\
			<div class="Ques-comp">\
				<h5>Add Competency score</h5>\
				<?php if ( ! empty($sim_com['comp_col_1']) && ! empty($sim_com['comp_val_1'])): ?>
				<div class="form-group QusScore"><input type="text" class="form-control score swip" name="swip_ans_val1[]" id="swip_ans_val1" onkeypress="return isNumberKey(event);" /></div>\
				<?php endif; if ( ! empty($sim_com['comp_col_2']) && ! empty($sim_com['comp_val_2'])): ?>
				<div class="form-group QusScore"><input type="text" class="form-control score swip" name="swip_ans_val2[]" id="swip_ans_val2" onkeypress="return isNumberKey(event);" /></div>\
				<?php endif; if ( ! empty($sim_com['comp_col_3']) && ! empty($sim_com['comp_val_3'])): ?>
				<div class="form-group QusScore"><input type="text" class="form-control score swip" name="swip_ans_val3[]" id="swip_ans_val3" onkeypress="return isNumberKey(event);" /></div>\
				<?php endif; if ( ! empty($sim_com['comp_col_4']) && ! empty($sim_com['comp_val_4'])): ?>
				<div class="form-group QusScore"><input type="text" class="form-control score swip" name="swip_ans_val4[]" id="swip_ans_val4" onkeypress="return isNumberKey(event);" /></div>\
				<?php endif; if ( ! empty($sim_com['comp_col_5']) && ! empty($sim_com['comp_val_5'])): ?>
				<div class="form-group QusScore"><input type="text" class="form-control score swip" name="swip_ans_val5[]" id="swip_ans_val5" onkeypress="return isNumberKey(event);" /></div>\
				<?php endif; if ( ! empty($sim_com['comp_col_6']) && ! empty($sim_com['comp_val_6'])): ?>
				<div class="form-group QusScore"><input type="text" class="form-control score swip" name="swip_ans_val6[]" id="swip_ans_val6" onkeypress="return isNumberKey(event);" /></div>\
				<?php endif; ?>
			</div>\
		</div>\
		<div class="form-group feed">\
			<h5>Feedback</h5>\
			<textarea class="form-control FeedForm inputtextWrap swip" name="swip_cfeedback[]" id="swip_cfeedback" placeholder="Feedback text goes here."></textarea>\
			<div class="swipf_assets_container_'+swipI+'" style="display:none;">\
				<div class="assets_data swipf_assets_data_'+swipI+'"><a class="view_assets" data-src="" href="javascript:;"></a></div>\
				<a href="javascript:void(0);" data-assets="" data-assets-id="" data-input-id="" data-path="<?php echo $uploadpath ?>" class="delete_assets" title="Delete"><i class="fa fa-times" aria-hidden="true"></i></a>\
			</div>\
			<ul class="QueBoxIcon feedQ">\
				<h5>Add assets</h5>\
				<li class="dropdown">\
					<button class="btn1 btn-secondary dropdown-toggle swipf_assets_'+swipI+'" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img class="img-fluid" src="img/list/audio.svg"></button>\
					<div class="dropdown-menu buttom" aria-labelledby="dropdownMenuButton">\
						<a data-toggle="modal" data-target="#swipfctts'+swipI+'" class="dropdown-item">\
							<div class="tooltip1">\
								<img class="img-fluid" src="img/list/text_speech.svg" />\
								<span class="tooltiptext">Text to Speech</span>\
							</div>\
						</a>\
						<a class="dropdown-item">\
							<div class="tooltip1 uploadicon">\
								<label for="file-input-swip-feedback-addAudio-'+swipI+'">\
									<img class="img-fluid" src="img/list/add_audio.svg" />\
									<span class="tooltiptext">Add Audio</span>\
								</label>\
								<input id="file-input-swip-feedback-addAudio-'+swipI+'" data-id="#swip_feedback_audio_'+swipI+'" data-assets="swipf_assets_'+swipI+'" class="uploadAudioFileVideoTemp" type="file" name="swip_feedback_Audio_'+swipI+'" />\
								<input type="hidden" name="swip_feedback_audio[]" id="swip_feedback_audio_'+swipI+'" />\
							</div>\
						</a>\
						<a class="dropdown-item">\
							<div class="tooltip1">\
								<img class="img-fluid rec-audio-videoq" data-input-id="swip_feedback_rec_audio_'+swipI+'" data-assets="swipf_assets_'+swipI+'" src="img/list/record_audio.svg" />\
								<span class="tooltiptext">Record Audio</span>\
							</div>\
							<input type="hidden" name="swip_feedback_rec_audio[]" id="swip_feedback_rec_audio_'+swipI+'" />\
						 </a>\
					</div>\
				</li>\
				<li class="dropdown">\
					<button class="btn1 btn-secondary dropdown-toggle swipf_assets_'+swipI+'" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img class="img-fluid" src="img/list/video.svg"></button>\
					<div class="dropdown-menu buttom" aria-labelledby="dropdownMenuButton">\
						<a class="dropdown-item">\
							<div class="tooltip1 uploadicon">\
								<label for="file-input-swip-feedback-addVideo-'+swipI+'">\
									<img class="img-fluid" src="img/list/add_video.svg">\
									<span class="tooltiptext">Add Video</span>\
								</label>\
								<input id="file-input-swip-feedback-addVideo-'+swipI+'" data-id="#swip_feedback_video_'+swipI+'" data-assets="swipf_assets_'+swipI+'" class="uploadFileVideoTemp" type="file" name="swip_feedback_Video_'+swipI+'" />\
								<input type="hidden" name="swip_feedback_video[]" id="swip_feedback_video_'+swipI+'" />\
							</div>\
						</a>\
						<a class="dropdown-item">\
							<div class="tooltip1">\
								<img class="img-fluid rec-video-videoq" data-input-id="swip_feedback_rec_video_'+swipI+'" data-assets="swipf_assets_'+swipI+'" src="img/qus_icon/rec_video.svg">\
								<span class="tooltiptext">Record Video</span>\
							 </div>\
							 <input type="hidden" name="swip_feedback_rec_video[]" id="swip_feedback_rec_video_'+swipI+'" />\
						</a>\
						<a class="dropdown-item">\
							<div class="tooltip1">\
								<img class="img-fluid rec-screen-videoq" data-input-id="swip_feedback_rec_screen_'+swipI+'" data-assets="swipf_assets_'+swipI+'" src="img/list/screen_record.svg">\
								<span class="tooltiptext">Record Screen</span>\
							 </div>\
							 <input type="hidden" name="swip_feedback_rec_screen[]" id="swip_feedback_rec_screen_'+swipI+'" />\
						</a>\
					</div>\
				</li>\
				<li>\
					<div class="tooltip uploadicon swipf_assets_'+swipI+'">\
						<label for="file-input-swip-feedback-addImg-'+swipI+'">\
							<img class="img-fluid" src="img/list/image.svg">\
							<span class="tooltiptext">Add Image</span>\
						</label>\
						<input id="file-input-swip-feedback-addImg-'+swipI+'" data-id="#swip_feedback_img_'+swipI+'" data-assets="swipf_assets_'+swipI+'" class="uploadImgFileVideo swipf_assets_'+swipI+'" type="file" name="swip_feedback_Img_'+swipI+'" />\
						<input type="hidden" name="swip_feedback_img[]" id="swip_feedback_img_'+swipI+'" />\
					</div>\
				</li>\
				<li>\
					<div class="tooltip uploadicon swipf_assets_'+swipI+'">\
						<label for="file-input-swip-feedback-addDoc-'+swipI+'">\
							<img class="img-fluid" src="img/list/add_document.svg" />\
							<span class="tooltiptext">Add Document</span>\
						</label>\
						<input id="file-input-swip-feedback-addDoc-'+swipI+'" data-id="#swip_feedback_doc_'+swipI+'" data-assets="swipf_assets_'+swipI+'" class="uploadDocFileVideo swipf_assets_'+swipI+'" type="file" name="swip_feedback_Doc_'+swipI+'" />\
						<input type="hidden" name="swip_feedback_doc[]" id="swip_feedback_doc_'+swipI+'" />\
					</div>\
				</li>\
			</ul>\
			<div class="form-popup draggable Ques-pop-style modal" id="swipfctts'+swipI+'">\
				<div class="popheading">Insert Text to Speech</div>\
				<div class="textarea">\
					<textarea type="text" class="form-control1 inputtextWrap" name="swip_feedback_text_to_speech[]" id="swip_feedback_text_to_speech_'+swipI+'"></textarea>\
				</div>\
				<div class="modal-footer">\
					<button type="button" class="btn1 submitbtn1" data-dismiss="modal">Insert</button>\
					<button type="button" class="btn1 submitbtn1" onclick="clearData("swip_feedback_text_to_speech_'+swipI+'");">Clear</button>\
					<button type="button" class="btn1 submitbtn1" data-dismiss="modal">Cancel</button>\
				</div>\
			</div>\
		</div>\
	</div></div>');
});

$('body').on('click', '.SwipRemove', function(){
	$(this).parent().parent().parent().remove();
	swipI--;
});

//--------------Delete-Options------------
$('body').on('click', '.removeOption', function(e) {
	e.preventDefault();
	var ans_id	= $(this).attr('data-remove-answer-id');
	var curr	= $(this);
	if (ans_id) {
		var dataString = 'delete_classic_answer='+ true +'&ans_id='+ ans_id;
		swal({
			title: "Are you sure?",
			text:  "Delete this Option",
			icon: "warning",
			buttons: [true, 'Delete'],
			dangerMode: true, }).then((willDelete) => { if (willDelete) {
				$.LoadingOverlay("show");
				$.ajax({
					url: "includes/process.php",
					type: "POST",
					data: dataString,
					cache: false,
					success: function(resdata) {
						var res = $.parseJSON(resdata);
						$.LoadingOverlay("hide");
						if (res.success == true) {
							swal(res.msg, { buttons: false, timer: 2000 });
							curr.parent().parent().remove();
						}
						else if (res.success == false) {
							swal(res.msg, { buttons: false, timer: 2000 });
						}
					},error: function() {
						$.LoadingOverlay("hide");
						swal("Error", 'Oops, something went wrong. Please try again later.?', "error");
					}
				});
			}
		});
	}
});

$('#upload').click(function() {
	$(this).attr('disabled', 'disabled').html('<i class="fa fa-refresh fa-spin fa-fw"></i><span class="sr-only">Loading...</span> Please wait....');
	var file_data = $('#file').prop('files')[0];
	var form_data = new FormData();
	form_data.append('file', file_data);
	form_data.append('scenario_media_file', true);
	var ftype   = file_data.type;
	var gettype = $('#scenario_media_file_type').val();
	var ext 	= ftype.split('/')[0];
	if (ext == gettype) {
		$.ajax({
			url: "includes/upload.php",
			type: "POST",
			data: form_data,
			contentType: false,
			cache: false,
			processData:false,
			success: function(resdata) {
				var res = $.parseJSON(resdata);
				if (res.success == true) {
					if (res.zip == true) {
						$('#ImgDelete').show();
						$('.delete_wb_file').attr('data-wb-file', res.file_name);
						$('#scenario_media_file').val(res.file_name);
						$('#upload').html('Upload').removeAttr('disabled', 'disabled');
					}
					else {
						var viewHtml = '<a href="<?php echo $uploadpath ?>'+res.file_name+'" target="_blank" title="View Scenario Media File"><i class="fa fa-eye" aria-hidden="true"></i></a>';
						$('#splashImg').show().html(viewHtml);
						$('#ImgDelete').show();
						$('.delete_scenario_media_file').attr('data-scenario-media-file', res.file_name);
						$('#scenario_media_file').val(res.file_name);
						$('#upload').html('Upload').removeAttr('disabled', 'disabled');
					}
					swal(res.msg, { buttons: false, timer: 1000 });
				}
				else if (res.success == false) {
					$('#scenario_media_file').val('');
					swal("Error", res.msg, "error");
					$('#splashImg').hide('slow');
					$('#upload').html('Upload').removeAttr('disabled', 'disabled');
				}
			},error: function() {
				$('#splashImg').hide('slow');
				swal("Error", 'Oops. something went wrong please try again.?', "error");
			}
		});
	} else { 
		swal("Warning", 'Please select valid file. accept only '+gettype+' file', "warning");
		$('#upload').html('Upload').removeAttr('disabled', 'disabled');
	}
});

$('.delete_scenario_media_file').click(function() {
	var file_name   = $(this).attr("data-scenario-media-file");
	var dataString	= 'delete='+ true +'&scenario_media_file='+ file_name;
	if (file_name) {
		swal({
			title: "Are you sure?",
			text: "Delete this file.",
			icon: "warning",
			buttons: [true, 'Delete'],
			dangerMode: true, }).then((willDelete) => { if (willDelete) {
				$.LoadingOverlay("show");
				$.ajax({
					url: "includes/delete-file.php",
					type: "POST",
					data: dataString,
					cache: false,
					success: function(resdata) {
						$.LoadingOverlay("hide");
						var res = $.parseJSON(resdata);
						if (res.success == false) {
							swal("Error", res.msg, "error");
							$('#splashImg').hide('slow');
						}
						else if (res.success == true) {
							$('#splashImg, #ImgDelete').hide('slow');
							$('#scenario_media_file').val('');
							swal(res.msg, { buttons: false, timer: 1000, });
						}
					},error: function() {
						swal("Error", 'Oops, something went wrong. Please try again later.?', "error");
					}
				});
			} else { swal("Your file is safe.!", { buttons: false, timer: 1000, }); }
		});
	}
});

$('.delete_wb_file').click(function() {
	var file_name   = $(this).attr("data-wb-file");
	var dataString	= 'delete='+ true + '&wb_file='+ file_name;
	if (file_name) {
		swal({
			title: "Are you sure?",
			text: "Delete Web Object file.",
			icon: "warning",
			buttons: [true, 'Delete'],
			dangerMode: true, }).then((willDelete) => { if (willDelete) {
				$.LoadingOverlay("show");
				$.ajax({
					url: "includes/delete-file.php",
					type: "POST",
					data: dataString,
					cache: false,
					success: function(resdata) {
						$.LoadingOverlay("hide");
						var res = $.parseJSON(resdata);
						if (res.success == false) {
							swal("Error", res.msg, "error");
							$('#splashImg').hide('slow');
						}
						else if (res.success == true) {
							$('#splashImg, #ImgDelete').hide('slow');
							$('#scenario_media_file').val('');
							swal(res.msg, { buttons: false, timer: 1000, });
						}
					},error: function() {
						swal("Error", 'Oops, something went wrong. Please try again later.?', "error");
					}
				});
			} else { swal("Your file is safe.!", { buttons: false, timer: 1000, }); }
		});
	}
});

/* Upload Web Object */
$('#upload_web').click(function() {
	$(this).attr('disabled', 'disabled').html('<i class="fa fa-refresh fa-spin fa-fw"></i><span class="sr-only">Loading...</span> Please wait....');
	var file_data = $('#web_file').prop('files')[0];
	var form_data = new FormData();
	form_data.append('file', file_data);
	form_data.append('web_object_file', true);
	var ftype = file_data.type;
	var type  = $('#web_object_type').val();
	var ext   = ftype.split('/')[0];
	if (ext == type) {
		$.ajax({
			url: "includes/upload.php",
			type: "POST",
			data: form_data,
			contentType: false,
			cache: false,
			processData: false,
			success: function(resdata) {
				var res = $.parseJSON(resdata);
				if (res.success == true) {
					$('#WbDelete').show();
					$('.delete_wb').attr('data-wb', res.file_name);
					$('#web_object').val(res.file_name);
					$('#upload_web').html('Upload').removeAttr('disabled', 'disabled');
					swal(res.msg, { buttons: false, timer: 1000, });
				}
				else if (res.success == false) {
					swal("Error", res.msg, "error");
					$('#web_object').val('');
					$('#WbDelete').hide('slow');
					$('#upload_web').html('Upload').removeAttr('disabled', 'disabled');
				}
			},error: function() {
				$('#WbDelete').hide('slow');
				swal("Error", 'Oops. something went wrong please try again.?', "error");
			}
		});
	} else { 
		swal("Warning", 'Please select valid file. accept only zip file', "warning");
		$('#upload_web').html('Upload').removeAttr('disabled', 'disabled');
	}
});

/* Delete Web Object */
$('.delete_wb').click(function() {
	var file_name   = $(this).attr("data-wb");
	var dataString	= 'delete='+ true + '&wb_file='+ file_name;
	if (file_name) {
		swal({
			title: "Are you sure?",
			text: "Delete Web Object file.",
			icon: "warning",
			buttons: [true, 'Delete'],
			dangerMode: true, }).then((willDelete) => { if (willDelete) {
				$.LoadingOverlay("show");
				$.ajax({
					url: "includes/delete-file.php",
					type: "POST",
					data: dataString,
					cache: false,
					success: function(resdata) {
						$.LoadingOverlay("hide");
						var res = $.parseJSON(resdata);
						if (res.success == false) {
							swal("Error", res.msg, "error");
							$('#WbDelete').hide('slow');
						}
						else if (res.success == true) {
							$('#WbDelete').hide('slow');
							$('#web_object').val('');
							swal(res.msg, { buttons: false, timer: 1000, });
						}
					},error: function() {
						swal("Error", 'Oops, something went wrong. Please try again later.?', "error");
					}
				});
			} else { swal("Your file is safe.!", { buttons: false, timer: 1000, }); }
		});
	}
});

/* Add Cover Image */
$('#upload_cover_img').click(function() {
	$(this).attr('disabled', 'disabled').html('<i class="fa fa-refresh fa-spin fa-fw"></i><span class="sr-only">Loading...</span> Please wait....');
	var file_data = $('#upload_cover_img_file').prop('files')[0];
	var form_data = new FormData();
	form_data.append('file', file_data);
	form_data.append('scenario_cover_file', true);
	var ftype   = file_data.type;
	var type	= $('#cover_img_file_type').val();
	var ext 	= ftype.split('/')[0];
	if (ext == type) {
		$.ajax({
			url: "includes/upload.php",
			type: "POST",
			data: form_data,
			contentType: false,
			cache: false,
			processData:false,
			success: function(resdata) {
				var res = $.parseJSON(resdata);
				if (res.success == true) {
					$('#scenario_cover_file').val(res.file_name);
					$('.sim_cover_img_data,.delete_cover_img_file').show();
					$('.sim_cover_img_data a').text(res.file_name);
					$('.sim_cover_img_data a').attr('data-src', assets_path + res.file_name);
					$('.cover_image_container a.delete_cover_img_file').attr('data-cover-img-file', res.file_name);
					$('#upload_cover_img').html('Upload').attr('disabled', 'disabled');
					$('.upload_cover_btn').attr('disabled', 'disabled');
					swal(res.msg, { buttons: false, timer: 1000 });
				}
				else if (res.success == false) {
					swal("Error", res.msg, "error");
					$('.sim_cover_img_data').hide('slow');
					$('#upload_cover_img').html('Upload').removeAttr('disabled', 'disabled');
					$('.upload_cover_btn').removeAttr('disabled', 'disabled');
				}
			},error: function() {
				swal("Error", 'Oops. something went wrong please try again.?', "error");
				$('#upload_cover_img').html('Upload').removeAttr('disabled', 'disabled');
				$('.upload_cover_btn').removeAttr('disabled', 'disabled');
			}
		});
	} else {
		swal("Warning", 'Please select valid file. accept only '+ type +' file', "warning");
		$('#upload_cover_img').html('Upload').removeAttr('disabled', 'disabled');
		$('.upload_cover_btn').removeAttr('disabled', 'disabled');
	}
});

/* Delete Cover Image */
$('.delete_cover_img_file').click(function() {
	var file_name   = $(this).attr("data-cover-img-file");
	var dataString	= 'delete='+ true + '&scenario_media_file='+ file_name;
	if (file_name) {
		swal({
			title: "Are you sure?",
			text: "Delete this file.",
			icon: "warning",
			buttons: [true, 'Delete'],
			dangerMode: true }).then((willDelete) => { if (willDelete) {
				$.LoadingOverlay("show");
				$.ajax({
					url: "includes/delete-file.php",
					type: "POST",
					data: dataString,
					cache: false,
					success: function(resdata) {
						$.LoadingOverlay("hide");
						var res = $.parseJSON(resdata);
						if (res.success == false) {
							swal("Error", res.msg, "error");
						}
						else if (res.success == true) {
							$('#scenario_cover_file').val('');
							$('.sim_cover_img_data,.delete_cover_img_file').hide('slow');
							$('.sim_cover_img_data a').text('');
							$('.sim_cover_img_data a').attr('data-src', '');
							$('.cover_image_container a.delete_cover_img_file').attr('data-cover-img-file', '');
							$('#upload_cover_img').html('Upload').removeAttr('disabled', 'disabled');
							$('.upload_cover_btn').removeAttr('disabled', 'disabled');
							swal(res.msg, { buttons: false, timer: 1000 });
						}
					},error: function() {
						swal("Error", 'Oops, something went wrong. Please try again later.?', "error");
					}
				});
			} else { swal("Your file is safe.!", { buttons: false, timer: 1000, }); }
		});
	}
});

$('.sim_page_desc').each(function(e){
	CKEDITOR.replace(this.id, { customConfig: "config-max-description.js" });
});

if ($('#sim_page_desc_0').length) {
	CKEDITOR.replace('sim_page_desc_0', { customConfig: "config-max-description.js" });
}

var page = <?php echo ( ! empty($pagei)) ? $pagei++ : 1; ?>;
$(".plus_page").click(function() {
	$(".plus_page_option:last").append('<div class="add_page_option appendSCE">\
	<div class="form-group">\
		<input type="text" name="sim_page_name[]" class="form-control" placeholder="Page name" required="required" />\
	</div>\
	<div class="form-group">\
		<textarea name="sim_page_desc[]" id="sim_page_desc_'+ page +'" class="form-control"></textarea>\
	</div>\
	 <div class="remove minusComp"><img src="img/icons/minus.png" title="Remove Page"></div></div>');
	 var page_des_id = 'sim_page_desc_'+ page;
	 CKEDITOR.replace(page_des_id, { customConfig : "config-max-description.js" });
	page++;
});

$('.plus_page_option').on('click', '.remove', function(e) {
	e.preventDefault();
	$(this).parent().remove();
	page--;
});

/* Close Video tab when select temp type. */
$('body').on('click', '.QusTempBanner .nav-QusTempTab li', function () {
	if ($(this).hasClass('act')) {
		$(this).removeClass('act');
		$('.panel-collapseV').slideDown(500);
		$('.Linr_mulVid_baner').removeClass('Linr_mulVid_baneradd');
	}
	else {
		$(this).addClass('act');
		$('.panel-collapseV').slideUp(500);
		$('.Linr_mulVid_baner').addClass('Linr_mulVid_baneradd');
	}
});

<?php if  ( ! empty($sim_data['bg_color'])) : ?>
$("#bg_color").spectrum("set", '<?php echo $sim_data['bg_color'] ?>');
<?php endif; 
if  ( ! empty($sim_brand['ques_bg'])) : ?>
$("#ques_bg_color").spectrum("set", '<?php echo $sim_brand['ques_bg'] ?>');
<?php endif; 
if  ( ! empty($sim_brand['option_bg'])) : ?>
$("#ques_option_bg_color").spectrum("set", '<?php echo $sim_brand['option_bg'] ?>');
<?php endif; 
if  ( ! empty($sim_brand['option_hover'])) : ?>
$("#ques_option_hover_color").spectrum("set", '<?php echo $sim_brand['option_hover'] ?>');
<?php endif; 
if  ( ! empty($sim_brand['option_selected'])) : ?>
$("#ques_option_select_color").spectrum("set", '<?php echo $sim_brand['option_selected'] ?>');
<?php endif; 
if  ( ! empty($sim_brand['btn_bg'])) : ?>
$("#btn_bg_color").spectrum("set", '<?php echo $sim_brand['btn_bg'] ?>');
<?php endif; 
if  ( ! empty($sim_brand['btn_hover'])) : ?>
$("#btn_hover_color").spectrum("set", '<?php echo $sim_brand['btn_hover'] ?>');
<?php endif; 
if  ( ! empty($sim_brand['btn_selected'])) : ?>
$("#btn_select_color").spectrum("set", '<?php echo $sim_brand['btn_selected'] ?>');
<?php endif; 
if  ( ! empty($sim_brand['font_color'])) : ?>
$("#font_color").spectrum("set", '<?php echo $sim_brand['font_color'] ?>');
<?php endif; 
if  ( ! empty($sim_brand['font_type'])) : ?>
$("#font_type").trigger("setFont", '<?php echo $sim_brand['font_type'] ?>');
<?php endif; ?>
document.onreadystatechange = function() {
	if (document.readyState == "complete") {
		$.LoadingOverlay("hide");
	}
};
</script>
<?php 
require_once 'includes/footer.php';
