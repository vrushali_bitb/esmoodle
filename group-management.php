<?php 
session_start();
if(isset($_SESSION['username'])){
    $user	= $_SESSION['username'];
	$role	= $_SESSION['role'];
	$userid	= $_SESSION['userId'];
}
else {
    header('location: index.php');
}
require_once 'includes/db.php';
require_once 'config/db.class.php';
require_once 'includes/formkey.class.php';
require_once 'includes/learners_menu.php';
require_once 'config/pagination.php';
$db 		= new DBConnection();
$formKey 	= new formKey();
$icon_path	= 'img/group_icon/';

if (isset($_GET['delete']) && ! empty($_GET['group_id'])):
	$id		= base64_decode($_GET['group_id']);
	$sql	= "DELETE t1.*, t2.* ";
	$sql   .= "FROM group_tbl t1 
				LEFT JOIN assignment_tbl t2 ON t1.group_id = t2.group_id 
				WHERE t1.group_id = ?";
    $q 	 = $db->prepare($sql);
	if ($q->execute(array($id))) $delete = 'Group delete successfully.!';
endif;

#----------Serch----------------
$search = '';
if (isset($_GET['search']) && ! empty($_GET['search'])):
	extract($_GET);
	$search = " AND ( group_name LIKE '%$search%' )";
endif;
#----------Sort by----------------
if (isset($_GET['orderby']) && $_GET['orderby'] == 'group'):
	$norderby = (( ! empty($search)) ? $search : " ")." ORDER BY g.group_name";
elseif (isset($_GET['orderby']) && $_GET['orderby'] == 'leader'):
	$norderby = (( ! empty($search)) ? $search : " ")." ORDER BY u.username";
else:
	$norderby = (( ! empty($search)) ? $search : " ")." ORDER BY g.group_id DESC";
endif;
$sqlr		= "SELECT g.*, u.username FROM group_tbl AS g LEFT JOIN users AS u ON g.group_leader = u.id WHERE 1=1 $norderby";
$q 			= $db->prepare($sqlr); $q->execute();
$rowCount	= $q->rowCount();
$count 		= ($sqlr) ? $rowCount : 0;
$page  		= (isset($_REQUEST['page']) && ! empty($_REQUEST['page'])) ? $_REQUEST['page'] : 1;
$show  		= 15;
$start 		= ($page - 1) * $show;
$pagination	= getPaginationString($count, $show, $page, 1, basename(__FILE__), '?page=');
$sqlr	   .= " LIMIT $start, $show";
$results	= $db->prepare($sqlr);
$msg 		= (isset($_GET['msg'])) ? $_GET['msg'] : '';
?>
<style>
.radio:after {
	display:none;
}
.multiselect-container>li>a>label {
    padding: 3px 20px 3px 30px !important;
}
.multiselect-container>li>a>label.checkbox, .multiselect-container>li>a>label.radio {
    margin-left: 10px;
}
input.form-control.multiselect-search {
    height: 34px;
}
.table-responsive {
    overflow: visible;
}
</style>
<div class="bottomheader">
    <div class="container-fluid">
        <ul class="breadcrumb">
			<?php echo $db->breadcrumbs(); ?>
         </ul>
     </div>
</div>
<div class="Group_managment">
    <div class="container-fluid">
		<div class="row">
			<div class="col-sm-12">
				<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
					<div class="panel panelCaollapse">
						<?php if ( ! empty($msg) && isset($_GET['add']) == 'true'): ?>
						<div class="alert alert-success alert-dismissable">Group added successfully.</div>
						<?php elseif ( ! empty($msg) && isset($_GET['exist']) == 'true'): ?>
						<div class="alert alert-danger alert-dismissable">Group already exists. please try another one.</div>
						<?php endif; ?>
						<?php if ( ! empty($delete)): ?>
						<div class="alert alert-success alert-dismissable"><?php echo $delete; ?></div>
						<?php endif; ?>
						<div class="col-sm-4 col-md-offset-2" id="showmsg" style="display:none;">
							<div class="alert alert-warning alert-dismissable" role="alert" id="msg"></div>
						</div>
						<div class="panel-heading" role="tab" id="headingOne">
							<h4 class="panel-title">
								<a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
									Create Group
								</a>
							</h4>
						</div>
						<div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
							<div class="panel-body">                    
							<form enctype="multipart/form" action="operations/manage_scenario.php" name="frmscenarioModal" method="POST">
								<?php echo $formKey->outputKey(); ?>
								<input type="hidden" name="frmscenario" id="frmscenario" value="<?php echo encryptString('modalCreateGroup', 'e'); ?>">
								<div class="usermanage-form">
									<div class="row">
										<div class="col-sm-6">
											<div class="form-group">
												<input type="text" class="form-control" name="group_name" id="group_name" placeholder="Group Name" required="required">
											</div>
										</div>
										<div class="col-sm-6">
											<div class="form-group form-select">
												<select class="form-control multiselect-ui selection" name="group_leader" id="group_leader" data-placeholder="Select Group Educator" required="required">
													<option value="0" selected="selected" disabled="disabled">Select Educator</option>
													<?php foreach ($db->getUserByRole("'educator'") as $educator): ?>
													<option value="<?php echo $educator['id'] ?>" title="<?php echo $educator['email'] ?>"><?php echo ucwords($educator['username']) ?> (<?php echo $educator['location'] ?>)</option>
													<?php endforeach; ?>
												</select>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="addsce-imgadm">
											<div class="col-sm-6">
												<div class="form-group">
													<input type="file" name="group_icon" id="group_icon" class="form-control file">
													<input type="text" class="form-control controls" disabled placeholder="Upload Image">
													<input type="hidden" name="group_icon_name" id="group_icon_name" />												
													<div class="browsebtn browsebtntmp">
														<span id="splashImg"></span> &nbsp;&nbsp; <span id="ImgDelete" style="display:none">
														<a href="javascript:void(0);" data-img-name="" class="delete_group_icon" title="Delete Icon"><i class="fa fa-times" aria-hidden="true"></i></a></span>
													</div>
												</div>
											</div>
											<div class="adimgbox browsebtn">
												<button class="browse btn btn-primary" type="button">Browse</button>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-sm-12">
											<div class="form-group">
												<textarea name="group_description" id="group_description" placeholder="Group Description" class="form-control"></textarea>
											</div>
										</div>
									</div>
								</div>
								<div class="usermanage-add clearfix">
									<input type="submit" class="btn btn-primary pull-right PullRcatagry" value="Add Group">
								</div>
							</form>
							</div>
						</div>
					</div>	
				</div>
				<div class="usermanage_main">
					<div class="table-box">
						<div class="tableheader clearfix">
						   <div class="tableheader_left">Add Users to Group</div>
						   <div class="tableheader_right">
								<div class="Searchbox">
									<form method="get">
										<input type="text" class="serchtext" name="search" placeholder="Search" required>
										<div class="serchBTN">
											<img class="img-responsive" src="img/dash_icon/search.svg">
										</div>
									</form>
								</div>
								<div class="btn-group btn-shortBY">
									<button type="button" class="btn btn-Transprate"><span class="sort">Sort by</span></button>
									<button type="button" class="btn btn-Transprate dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img  src="img/dash_icon/down_arrow.svg"></button>
									<div class="dropdown-menu shortBYMenu">
										<a class="dropdown-item" href="<?php echo $db->getBaseUrl($db->getFile()); ?>">Default Sorting</a>
										<a class="dropdown-item" href="<?php echo $db->getBaseUrl($db->getFile().'?orderby=group'); ?>">Group Name</a>
										<a class="dropdown-item" href="<?php echo $db->getBaseUrl($db->getFile().'?orderby=leader'); ?>">Educator Name</a>
									</div>
								</div>
							</div>
						</div>                
					</div>
					<div class="usermanage-form1">
					   <div class="table-responsive">        
							<table class="table table-striped">
								<thead class="Theader">
									<tr>
										<th>#</th>
										<th>Icon</th>
										<th>Group Name</th>
										<th>Leader Name</th>
										<th>Description</th>
										<th>Add User</th>
										<th>User Mail</th>
										<th>Edit</th>
										<th>Delete</th>                                                                           
									</tr>
								 </thead>
								<tbody>
								<?php if ($results->execute() && $rowCount > 0):
									$i = $start + 1;
									foreach ($results->fetchAll(PDO::FETCH_ASSOC) as $row):
									$icon = ( ! empty ($row['group_icon'])) ? $icon_path . $row['group_icon'] : ''; ?>
								<tr>
									<td><?php echo $i ?></td>
									<td><?php echo ( ! empty($icon)) ? '<img src="'.$icon.'" class="type_icon width40" width="40px" height="40px">' : ''; ?></td>
									<td><?php echo $row['group_name'] ?></td>
									<td><?php echo ucwords($row['username']); ?></td>
									<td><?php echo $row['group_des'] ?></td>
									<td><a class="add_users tooltiptop" data-group-id="<?php echo $row['group_id'] ?>" href="javascript:void(0);"><span class="tooltiptext">Add and Update User</span><img class="svg" src="img/list/add_user.svg"></a></td>
									<td><a class="user_mail tooltiptop" data-group-id="<?php echo md5($row['group_id']) ?>" href="javascript:void(0);"><span class="tooltiptext">Send New Account Mail</span><i class="fa fa-envelope-o fa-2x" aria-hidden="true"></i></a></td>
									<td><a class="edit tooltiptop" data-scenario-type-id="<?php echo md5($row['group_id']) ?>" href="javascript:void(0);"><span class="tooltiptext">Edit</span><img class="svg" src="img/list/edit.svg"></a></td>
									<td><a class="tooltiptop" href="<?php echo basename(__FILE__) ?>?delete=true&group_id=<?php echo base64_encode($row['group_id']) ?>" onclick="return confirm('Are you sure to delete this group.?');"><span class="tooltiptext">Delete</span><img class="svg" src="img/list/delete.svg"></a></td>
								</tr>
								<?php $i++; endforeach; else: ?>
								<tr><td colspan="8" style="text-align:center"><strong>No data available in database.</strong></td></tr>
								<?php endif; ?>
							   </tbody>
							</table>
						</div>
						<div class="pull-right">
							<nav aria-label="navigation"><?php echo ( ! empty($pagination) ) ? $pagination : '&nbsp;'; ?></nav>
						</div>
					</div>
				</div>
			</div>
		</div>
    </div>
</div>

<div id="load_popup_modal_show" class="modal fade" tabindex="-1"></div>

<script type="text/javascript">
	$('#group_leader').multiselect({
		includeSelectAllOption: false,
		filterPlaceholder: 'Search & select Educator',
		enableCaseInsensitiveFiltering : true,
		enableFiltering: true,
		buttonWidth: '250px',
		maxHeight: 250
	});
	$('.edit').click(function () {
		var id = $(this).attr('data-scenario-type-id');
		if (id != '') {
			$.LoadingOverlay("show");
			var $modal = $('#load_popup_modal_show');
			$modal.load('edit-group-modal.php', {'data-id': id }, function(res) {
				if (res != '') {
					$.LoadingOverlay("hide");
					$modal.modal('show');
				}
				else {
					$.LoadingOverlay("hide");
					swal("Error", 'Oops. something went wrong please try again.?', "error");
				}
			});
		}
	});
	$('.add_users').click(function () {
		var id = $(this).attr('data-group-id');
		if (id != '') {
			$.LoadingOverlay("show");
			var $modal = $('#load_popup_modal_show');
			$modal.load('add-user-group-modal.php', {'data-id': id }, function(res) {
				if (res != '') {
					$.LoadingOverlay("hide");
					$modal.modal('show');
				}
				else {
					$.LoadingOverlay("hide");
					swal("Error", 'Oops. something went wrong please try again.?', "error");
				}
			});
		}
	});
	$('.user_mail').click(function() {
		var gid = $(this).data('group-id');
		swal({
			title: "Are you sure?",
			text: "Sent New Account Mail",
			icon: "warning",
			buttons: [true, 'Send'],
			dangerMode: true, }).then((willDelete) => { if (willDelete) {
				$.LoadingOverlay("show");
				var dataString	= 'send_group_learner_mail='+ true + '&group_id='+ gid;
				$.ajax({
					url: 'includes/process.php',
					type: "POST",
					data: dataString,
					cache: false,
					success: function(resdata) {
						var res = $.parseJSON(resdata);
						if (res.success == true) {
							$.LoadingOverlay("hide");
							swal({text: res.msg, buttons: false, icon: "success", timer: 2000});
						}
						else if (res.success == false) {
							swal({text: res.msg, buttons: false, icon: "warning", timer: 2000});
							$.LoadingOverlay("hide");
						}
					}, error: function() {
						swal({text: 'Oops. something went wrong please try again.', buttons: false, icon: "error", timer: 2000});
						$.LoadingOverlay("hide");
					}
				});
			}
		});
	});
</script>
<?php 
require_once 'includes/footer.php';
