<?php
ob_start();
session_start();
if (isset($_SESSION['username'])) {
  $user   = $_SESSION['username'];
  $role   = $_SESSION['role'];
  $userid = $_SESSION['userId'];
  $cid    = (isset($_SESSION['client_id'])) ? $_SESSION['client_id'] : exit('Oops. something went wrong please try again');
} else {
  header('location: index.php');
}
require_once 'config/db.class.php';
require_once 'includes/learners_menu.php';
$db = new DBConnection(); ?>
<link rel="stylesheet" media="screen" href="content/css/report_page.css" />
<link rel="stylesheet" type="text/css" href="content/dataTables/jquery.dataTables.css">
<link rel="stylesheet" type="text/css" href="content/dataTables/jquery.dataTables.min.css">
<link rel="stylesheet" type="text/css" href="content/dataTables/buttons.dataTables.min.css">
<div class="bottomheader">
  <div class="container-fluid">
    <ul class="breadcrumb">
      <?php echo $db->breadcrumbs(); ?>
    </ul>
  </div>
</div>
<div class="admin-report GROUPpreFormancemain">
  <div class="bottomheader1">
    <div class="container-fluid">
      <div class="row">
        <div class="col-sm-4">
          <h3>My Group's Performance</h3>
        </div>
        <div class="col-sm-4">
          <div class="alert alert-warning alert-dismissible" role="alert" style="display: <?php echo (isset($_SESSION['msg'])) ? 'block' : 'none'; ?>">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <strong>Warning!</strong> <?php echo (isset($_SESSION['msg'])) ? $_SESSION['msg'] : ''; ?>
          </div>
        </div>
        <div class="col-sm-3 L-calender GPeducater usermanage-form">
          <select class="form-control" id="report">
            <option selected="selected" hidden>My Group Performance</option>
            <option value="1">Learner Report</option>
          </select>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-12">
          <form name="report_form" id="report_form" method="post">
            <input type="hidden" name="get_educator[]" id="get_educator">
            <div class="usermanage-form GrouPerformainbanner">
              <div class="row">
                <div class="col-sm-4">
                  <div class="form-group form-select">
                    <select name="category" id="category" class="form-control" data-placeholder="Category List" required="required">
                      <option value="0" selected="selected" disabled="disabled" hidden>Select Category</option>
                      <?php foreach ($db->getCategory() as $category) : ?>
                        <option value="<?php echo $category['cat_id'] ?>"><?php echo ucwords($category['category']) ?></option>
                      <?php endforeach; ?>
                    </select>
                  </div>
                </div>
                <div class="col-sm-4">
                  <div class="form-group form-select">
                    <select name="scenario[]" id="scenario" class="form-control multiselect-ui selection multiselection" data-placeholder="Simulation List" multiple="multiple" required="required">
                    </select>
                  </div>
                </div>
              </div>
              <div class="row GrouPerforbanner">
                <div class="col-sm-2">
                  <div class="Radio-box GrouPerfor">
                    <label class="radiostyle">
                      <input type="radio" class="checkbox1 filter_type" name="filter_type" id="System_Wise" value="1" checked="checked">
                      <span class="radiomark"></span>
                    </label>
                    <span>Groupwide</span>
                  </div>
                </div>
                <div class="col-sm-2">
                  <div class="Radio-box GrouPerfor">
                    <label class="radiostyle">
                      <input type="radio" class="checkbox1 filter_type other_filter" name="filter_type" id="filter_type" value="2" disabled="disabled">
                      <span class="radiomark"></span>
                    </label>
                    <div class="form-group form-select">
                      <select name="group[]" id="group" class="form-control multiselect-ui selection multiselection" multiple="multiple" data-placeholder="Group">
                      </select>
                    </div>
                  </div>
                </div>
                <div class="col-sm-2" style="display: none;">
                  <div class="form-group form-select">
                    <label>
                      <input type="radio" class="checkbox1 filter_type" name="filter_type" id="filter_learner" value="3" disabled="disabled"></label>
                    <div class="form-group form-select">
                      <select name="learner[]" id="learner" class="form-control multiselect-ui selection multiselection" multiple="multiple" data-placeholder="Learner">
                      </select>
                    </div>
                  </div>
                </div>
                <div class="col-sm-2">
                  <div class="Radio-box GrouPerfor">
                    <label class="radiostyle">
                      <input type="radio" class="checkbox1 filter_type other_filter" name="filter_type" value="4" disabled="disabled">
                      <span class="radiomark"></span>
                    </label>
                    <div class="form-group form-select">
                      <select name="unit[]" id="unit" class="form-control multiselect-ui selection multiselection" multiple="multiple" data-placeholder="Unit">
                      </select>
                    </div>
                  </div>
                </div>
                <div class="col-sm-2">
                  <div class="Radio-box GrouPerfor">
                    <label class="radiostyle">
                      <input type="radio" class="checkbox1 filter_type other_filter" name="filter_type" value="5" disabled="disabled">
                      <span class="radiomark"></span>
                    </label>
                    <div class="form-group form-select">
                      <select name="location[]" id="location" class="form-control multiselect-ui selection multiselection" multiple="multiple" data-placeholder="Location">
                      </select>
                    </div>
                  </div>
                </div>
                <div class="col-sm-2">
                  <div class="Radio-box GrouPerfor">
                    <label class="radiostyle">
                      <input type="radio" class="checkbox1 filter_type other_filter" name="filter_type" value="6" disabled="disabled">
                      <span class="radiomark"></span>
                    </label>
                    <div class="form-group form-select">
                      <select name="designation[]" id="designation" class="form-control multiselect-ui selection multiselection" multiple="multiple" data-placeholder="Designation">
                      </select>
                    </div>
                  </div>
                </div>
              </div>
              <div class="row GrouPerforbanner">
                  <div class="col-sm-3">
                    <div class="form-group">
                      <div class="input-group date form_datetime" data-date-format="yyyy-mm-dd" data-link-field="dtp_input1">
                          <input class="form-control" name="fdate" type="text" readonly placeholder="From Date" required />
                          <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                      </div>
                    </div>
                  </div>
                  <div class="col-sm-3">
                    <div class="form-group">
                      <div class="input-group date form_datetime" data-date-format="yyyy-mm-dd" data-link-field="dtp_input1">
                        <input class="form-control" name="tdate" type="text" readonly placeholder="To Date" required />
                        <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                      </div>
                    </div>
                  </div>
                </div>
              <div class="usermanage-add clearfix">
                <input type="hidden" name="export_type" value="2" />
                <input type="submit" name="submit" id="submit" class="btn btn-primary Search_btn" value="Search">
                <input type="submit" name="submit" id="submit" class="btn btn-primary Search_btn" formaction="<?php echo $db->getBaseUrl('includes/group-performance-report-excel-export.php'); ?>" value="XLS Export">
              </div>
            </div>
          </form>
          <?php
          if ($_SERVER['REQUEST_METHOD'] == 'POST' && ! empty($_POST['scenario'])):
            extract($_POST);
            $limit            =  2;
            $get_filter_type  = '';
            $show_filter_data = '';
            $get_sim_id       = $db->addMultiIds($scenario);
            if ($filter_type == 1):
              #----System-Wise-------
              $get_filter_type = 'Systemwide';
              $sql             = "SELECT GROUP_CONCAT(DISTINCT IF(group_id = '', null, group_id)) AS gid, GROUP_CONCAT(DISTINCT IF(learner_id = '', null, learner_id)) AS lid FROM assignment_tbl WHERE scenario_id IN ($get_sim_id)";
              $exc             = $db->prepare($sql); $exc->execute();
              $row             = $exc->fetch(PDO::FETCH_ASSOC);
              $gid             = ( ! empty($row['gid'])) ? rtrim($row['gid'], ',') : '';
              $lid             = ( ! empty($row['lid'])) ? rtrim($row['lid'], ',') : '';
              foreach ($db->getLearnerByGroup($gid, $userid) as $gdata):
                $getlearnerId[] = $gdata['learner'];
              endforeach;
              if ( ! empty($tdate) && ! empty($fdate)):
                $get_learner_id = $db->addMultiIds($getlearnerId);
                $date_sql = "SELECT GROUP_CONCAT(userid) AS learner FROM scenario_attempt_tbl WHERE scenario_id IN ($get_sim_id) AND userid IN ($get_learner_id) AND DATE(attempt_date) BETWEEN '". $fdate ."' AND '". $tdate ."'";
                $date_exc = $db->prepare($date_sql); $date_exc->execute();
                $date_row = $date_exc->fetch(PDO::FETCH_ASSOC);
                $get_learner = $date_row['learner'];
              elseif ( ! empty($tdate) && empty($fdate)):
                $get_learner_id = $db->addMultiIds($getlearnerId);
                $date_sql = "SELECT GROUP_CONCAT(userid) AS learner FROM scenario_attempt_tbl WHERE scenario_id IN ($get_sim_id) AND userid IN ($get_learner_id) AND DATE(attempt_date) = '". $tdate ."'";
                $date_exc = $db->prepare($date_sql); $date_exc->execute();
                $date_row = $date_exc->fetch(PDO::FETCH_ASSOC);
                $get_learner = $date_row['learner'];
              elseif ( ! empty($fdate) && empty($tdate)):
                $get_learner_id = $db->addMultiIds($getlearnerId);
                $date_sql = "SELECT GROUP_CONCAT(userid) AS learner FROM scenario_attempt_tbl WHERE scenario_id IN ($get_sim_id) AND userid IN ($get_learner_id) AND DATE(attempt_date) = '". $fdate ."'";
                $date_exc = $db->prepare($date_sql); $date_exc->execute();
                $date_row = $date_exc->fetch(PDO::FETCH_ASSOC);
                $get_learner = $date_row['learner'];
              else:
                $get_learner = $db->addMultiIds($getlearnerId);
              endif;
            elseif ($filter_type == 2):
              #----Group-------
              $get_filter_type  = 'Group';
              if ( ! empty($tdate) && ! empty($fdate)):
                $get_learner_id = $db->addMultiIds($group);
                $date_sql = "SELECT GROUP_CONCAT(userid) AS learner FROM scenario_attempt_tbl WHERE scenario_id IN ($get_sim_id) AND userid IN ($get_learner_id) AND DATE(attempt_date) BETWEEN '". $fdate ."' AND '". $tdate ."'";
                $date_exc = $db->prepare($date_sql); $date_exc->execute();
                $date_row = $date_exc->fetch(PDO::FETCH_ASSOC);
                $get_learner = $date_row['learner'];
              elseif ( ! empty($tdate) && empty($fdate)):
                $get_learner_id = $db->addMultiIds($group);
                $date_sql = "SELECT GROUP_CONCAT(userid) AS learner FROM scenario_attempt_tbl WHERE scenario_id IN ($get_sim_id) AND userid IN ($get_learner_id) AND DATE(attempt_date) = '". $tdate ."'";
                $date_exc = $db->prepare($date_sql); $date_exc->execute();
                $date_row = $date_exc->fetch(PDO::FETCH_ASSOC);
                $get_learner = $date_row['learner'];
              elseif ( ! empty($fdate) && empty($tdate)):
                $get_learner_id = $db->addMultiIds($group);
                $date_sql = "SELECT GROUP_CONCAT(userid) AS learner FROM scenario_attempt_tbl WHERE scenario_id IN ($get_sim_id) AND userid IN ($get_learner_id) AND DATE(attempt_date) = '". $fdate ."'";
                $date_exc = $db->prepare($date_sql); $date_exc->execute();
                $date_row = $date_exc->fetch(PDO::FETCH_ASSOC);
                $get_learner = $date_row['learner'];
              else:
                $get_learner = $db->addMultiIds($group);
              endif;
              $show_filter_data = $db->addMultiIds($get_educator);
            elseif ($filter_type == 3):
              #----Learner------
              $get_filter_type  = 'Learner';
              $get_learner      = $db->addMultiIds($learner);
            elseif ($filter_type == 4 || $filter_type == 5 || $filter_type == 6) :
              #----Unit------
              if ($filter_type == 4):
                $get_filter_type  = 'Unit';
                $get_filter_data  = implode("','", $unit);
                $show_filter_data = $db->addMultiIds($unit);
                $column           = 'department';
              elseif ($filter_type == 5):
                #----Location------
                $get_filter_type  = 'Location';
                $get_filter_data  = implode("','", $location);
                $show_filter_data = $db->addMultiIds($location);
                $column           = 'location';
              elseif ($filter_type == 6):
                #----Designation-----
                $get_filter_type  = 'Designation';
                $get_filter_data  = implode("','", $designation);
                $show_filter_data = $db->addMultiIds($designation);
                $column           = 'designation';
              endif;
              $sql = "SELECT GROUP_CONCAT(DISTINCT IF(group_id = '', null, group_id)) AS gid, GROUP_CONCAT(DISTINCT IF(learner_id = '', null, learner_id)) AS lid FROM assignment_tbl WHERE scenario_id IN ($get_sim_id)";
              $exc = $db->prepare($sql); $exc->execute();
              $row = $exc->fetch(PDO::FETCH_ASSOC);
              $gid = ( ! empty($row['gid'])) ? rtrim($row['gid'], ',') : '';
              $lid = ( ! empty($row['lid'])) ? rtrim($row['lid'], ',') : '';
              foreach ($db->getLearnerByGroup($gid, $userid) as $gdata) :
                $getlearnerId[] = $gdata['learner'];
              endforeach;
              $get_unit_learner   = $db->addMultiIds($getlearnerId);
              $cond               = "$column IN ('$get_filter_data')";
              $get_other_learner  = $db->GetUsersByFilter($get_unit_learner, 'id', $cond);
              if ( ! empty($tdate) && ! empty($fdate)):
                $get_learner_id = $db->addMultiIds($get_other_learner);
                $date_sql = "SELECT GROUP_CONCAT(userid) AS learner FROM scenario_attempt_tbl WHERE scenario_id IN ($get_sim_id) AND userid IN ($get_learner_id) AND DATE(attempt_date) BETWEEN '". $fdate ."' AND '". $tdate ."'";
                $date_exc = $db->prepare($date_sql); $date_exc->execute();
                $date_row = $date_exc->fetch(PDO::FETCH_ASSOC);
                $get_learner = $date_row['learner'];
              elseif ( ! empty($tdate) && empty($fdate)):
                $get_learner_id = $db->addMultiIds($get_other_learner);
                $date_sql = "SELECT GROUP_CONCAT(userid) AS learner FROM scenario_attempt_tbl WHERE scenario_id IN ($get_sim_id) AND userid IN ($get_learner_id) AND DATE(attempt_date) = '". $tdate ."'";
                $date_exc = $db->prepare($date_sql); $date_exc->execute();
                $date_row = $date_exc->fetch(PDO::FETCH_ASSOC);
                $get_learner = $date_row['learner'];
              elseif ( ! empty($fdate) && empty($tdate)):
                $get_learner_id = $db->addMultiIds($get_other_learner);
                $date_sql = "SELECT GROUP_CONCAT(userid) AS learner FROM scenario_attempt_tbl WHERE scenario_id IN ($get_sim_id) AND userid IN ($get_learner_id) AND DATE(attempt_date) = '". $fdate ."'";
                $date_exc = $db->prepare($date_sql); $date_exc->execute();
                $date_row = $date_exc->fetch(PDO::FETCH_ASSOC);
                $get_learner = $date_row['learner'];
              else:
                $get_learner = $db->addMultiIds($get_other_learner);
              endif;
            endif;
            $get_all_learner      = explode(',', $get_learner);
            $get_filter_learner   = array_unique($get_all_learner);
            $get_total_learner    = count($get_filter_learner); ?>
            <div class="row">
              <div class="col-sm-12">
                <div class="table-responsive AdminReportSys">
                  <table id="example" cellpadding="0" cellspacing="0" border="0" class="display nowrap table table-striped" width="100%">
                    <thead class="Theader">
                      <tr class="Reprttabheader">
                        <td>#</td>
                        <td>Full Name</td>
                        <td>User Name</td>
                        <td>Company</td>
                        <td>Unit</td>
                        <td>Location</td>
                        <td>Designation</td>
                        <?php foreach ($scenario as $sim) :
                          $sim_data = $db->getScenario(md5($sim)); ?>
                          <td><b><?php echo $sim_data['Scenario_title']; ?></b></td>
                        <?php endforeach; ?>
                        </td>
                    </thead>
                    <tbody>
                      <?php if ($get_total_learner > 0):
                      $get_all_learners = $db->addMultiIds($get_filter_learner);
                      $column   = "id, CONCAT(fname, ' ', lname) AS fullname, username, company, department, location, designation";
                      $userData = $db->getUsersColumns($get_all_learners, $column);
                      $sno      = 1;
                        foreach ($userData as $ldata): ?>
                          <tr>
                            <td><?php echo $sno ?></td>
                            <td><?php echo $ldata['fullname'] ?></td>
                            <td><strong><?php echo $ldata['username'] ?></strong></td>
                            <td><?php echo $ldata['company'] ?></td>
                            <td><?php echo $ldata['department'] ?></td>
                            <td><?php echo $ldata['location'] ?></td>
                            <td><?php echo $ldata['designation'] ?></td>
                            <?php foreach ($scenario as $sim): ?>
                            <td>
                              <div class="AReportFlex"><?php echo $db->analyticsReport($sim, $ldata['id'], $limit, $fdate, $tdate); ?></div>
                            </td>
                            <?php endforeach; ?>
                          </tr>
                      <?php $sno++; endforeach; endif; ?>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          <?php endif; ?>
        </div>
      </div>
    </div>
  </div>
</div>
<style type="text/css">
  @media screen and (-ms-high-contrast: active),
  (-ms-high-contrast: none) {
    p.circleChart_text {
      left: 350%;
    }
  }

  p.circleChart_text {
    font-size: 25px !important;
  }

  .comp-incomp-topbanner {
    background: #fff;
    min-height: 429px;
    border-radius: 0px;
    margin-bottom: 20px;
    padding: 20px;
    border: 1px solid #e2e2e2;
    position: relative;
  }

  .lernar-dashboard-boxes {
    padding: 10px;
  }

  .lernar-dashboard-boxes span {
    display: table-cell;
    font-size: 40px;
    vertical-align: middle;
  }

  .lernar-dashboard-boxes p {
    padding-left: 10px;
  }

  @media(max-width: 1450px) and (min-width: 1024px) {
    .lernar-dashboard-boxes p {
      padding-right: 0px;
    }
  }

  .wordwrap {
    white-space: pre-wrap;
    white-space: -moz-pre-wrap;
    white-space: -pre-wrap;
    white-space: -o-pre-wrap;
    word-wrap: break-word;
  }

  canvas.canvasjs-chart-canvas {
    padding-top: 0px;
  }
</style>
<script type="text/javascript" language="javascript" src="content/dataTables/jquery.dataTables.js"></script>
<script type="text/javascript" language="javascript" src="content/dataTables/jquery.dataTables.min.js"></script>
<script type="text/javascript" language="javascript" src="content/js/bootstrap-datetimepicker.js" charset="UTF-8"></script>
<script type="text/javascript">
  $(document).ready(function() {
    $('.form_datetime').datetimepicker({
      weekStart: 1,
      todayBtn:  0,
      autoclose: 1,
      todayHighlight: 1,
      startView: 2,
      forceParse: 0,
      minView: 2,
      showMeridian: 1,
      clearBtn: 1,
      endDate:new Date()
    });

    $('#example').DataTable({
      "dom": 'Bfrtip',
      "ordering": false,
      "lengthMenu": [ 20 ],
      "searching": false,
    });

    $('#group').on('change', function() {
      var selected    = $(this).find("option:selected");
      var arrSelected = [];
      selected.each(function() { arrSelected.push($(this).html()); });
      $('#get_educator').val(arrSelected);
    });

    $('#report').on('change', function() {
      var id = $(this).val();
      if (id == 1) window.location.href = 'educator-report.php'
    });
  });

  $('.multiselection').multiselect({
    disableIfEmpty: true,
    includeSelectAllOption: true,
    filterPlaceholder: 'Search & Select',
    enableCaseInsensitiveFiltering: true,
    enableFiltering: true,
    buttonWidth: '250px',
    maxHeight: 250
  });

  $('.filter_type').on('change', function() {
    var ftype = $(this).val();
    $('#group,#learner,#unit,#location,#designation').multiselect("deselectAll", false);
    $('#group,#learner,#unit,#location,#designation').multiselect('refresh');
    $('#group,#learner,#unit,#location,#designation').multiselect('disable');
    if (ftype == 1) {
      $('#group,#learner,#unit,#location,#designation').multiselect('disable');
    } else if (ftype == 2) {
      $('#group').multiselect('enable');
    } else if (ftype == 3) {
      $('#learner').multiselect('enable');
    } else if (ftype == 4) {
      $('#unit').multiselect('enable');
    } else if (ftype == 5) {
      $('#location').multiselect('enable');
    } else if (ftype == 6) {
      $('#designation').multiselect('enable');
    }
  });

  $('#category').on('change', function() {
    var catid = $(this).val();
    if (catid != '')
      $("#report_form").LoadingOverlay("show");
      $('#scenario').empty();
      $('#scenario').multiselect('refresh');
      $('#System_Wise').prop("checked", true);
      $('#filter_educator,#filter_learner,.other_filter').attr('disabled', true);
      $('#group,#learner,#unit,#location,#designation').empty();
      $('#group,#learner,#unit,#location,#designation').multiselect("deselectAll", false);
      $('#group,#learner,#unit,#location,#designation').multiselect('refresh');
      $('#group,#learner,#unit,#location,#designation').multiselect('disable');
      $.getJSON('includes/ajax.php?getEducatorScenario=true&catid=' + catid, function(res) {
        $("#report_form").LoadingOverlay("hide", true);
        $('#scenario').empty();
        if (res.success == true) {
          $('#scenario').multiselect('dataprovider', res.data);
        } else if (res.success == false) {
          $('#scenario').multiselect('rebuild');
          $('#scenario').multiselect('refresh');
        }
      });
  });

  $('#scenario').on('change', function(){
    var sid = $(this).val();
    if (sid != '') {
      $('#System_Wise').prop("checked", true);
      $('#group,#learner,#unit,#location,#designation').empty();
      $('#group,#learner,#unit,#location,#designation').multiselect("deselectAll", false);
      $('#group,#learner,#unit,#location,#designation').multiselect('refresh');
      $('#group,#learner,#unit,#location,#designation').multiselect('disable');
      $.getJSON('includes/ajax.php?group_performance_educator=true&sim_id=' + sid, function(res){
        if (res.success == true) {
          if (res.data) {
            $('#filter_educator,.other_filter').removeAttr('disabled', true);
            $('#filter_learner').attr('disabled', true);
            $('#group').multiselect('dataprovider', res.data);
            $('#unit').multiselect('dataprovider', res.unit);
            $('#location').multiselect('dataprovider', res.location);
            $('#designation').multiselect('dataprovider', res.designation);
          }
        } else {
          swal({text: 'Data not found.', buttons: false, icon: "warning", timer: 4000});
          $('#System_Wise').prop("checked", true);
          $('#filter_educator,#filter_learner,.other_filter').attr('disabled', true);
          $('#group,#learner,#unit,#location,#designation').multiselect("deselectAll", false);
          $('#group,#learner,#unit,#location,#designation').multiselect('refresh');
          $('#group,#learner,#unit,#location,#designation').multiselect('disable');
        }
      });
    } else {
      $('#System_Wise').prop("checked", true);
      $('#filter_educator,#filter_learner,.other_filter').attr('disabled', true);
      $('#group,#learner,#unit,#location,#designation').multiselect("deselectAll", false);
      $('#group,#learner,#unit,#location,#designation').multiselect('refresh');
      $('#group,#learner,#unit,#location,#designation').multiselect('disable');
    }
  });

  $('#group').on('change', function(){
    var gid = $(this).val();
    if (gid != '') {
      $('#learner,#unit,#location,#designation').empty();
      $('#learner,#unit,#location,#designation').multiselect("deselectAll", false);
      $('#learner,#unit,#location,#designation').multiselect('refresh');
      $('#learner,#unit,#location,#designation').multiselect('disable');
      $.getJSON('includes/ajax.php?group_performance_educator_other=true&group_id=' + gid, function(res){
        if (res.success == true) {
            $('#unit').multiselect('dataprovider', res.unit);
            $('#location').multiselect('dataprovider', res.location);
            $('#designation').multiselect('dataprovider', res.designation);
        } else {
          swal({text: 'Data not found.', buttons: false, icon: "warning", timer: 4000});
          $('#learner,#unit,#location,#designation').multiselect("deselectAll", false);
          $('#learner,#unit,#location,#designation').multiselect('refresh');
          $('#learner,#unit,#location,#designation').multiselect('disable');
        }
      });
    }
  });
</script>
<?php 
unset($_SESSION['msg']);
require_once 'includes/footer.php';
