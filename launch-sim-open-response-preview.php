<?php 
ob_start();
session_start();
if (isset($_SESSION['username'])):
	$user 	= $_SESSION['username'];
	$role 	= $_SESSION['role'];
	$userid = $_SESSION['userId'];
	$domain = (isset($_SESSION['domain_name'])) ? $_SESSION['domain_name'] : $_SERVER['HTTP_HOST'];
else:
    header('location: index.php');
endif;
require_once 'config/db.class.php';
require_once 'includes/header.php';
$db 		= new DBConnection();
$sim_id		= (isset($_GET['sim_id'])) ? $_GET['sim_id'] : die('Error');
$ques_id	= (isset($_GET['ques_id'])) ? base64_decode($_GET['ques_id']) : '';
$save		= (isset($_GET['save']) && $_GET['save'] == 'false') ? FALSE : TRUE;
$simData	= $db->getScenario($sim_id);
$simID 		= $simData['scenario_id'];
$brand		= $db->getSimBranding($simID);
$back_img	= ( ! empty($simData['sim_back_img'])) ? 'img/char_bg/'. $simData['sim_back_img'] : '';
$uploadpath	= '../scenario/upload/'.$domain.'/';
$path		= $db->getBaseUrl('scenario/upload/'.$domain.'/');
$aPoster	= $db->getBaseUrl('scenario/img/audio-poster.jpg');
$vtheme		= 'forest'; #city, fantasy, forest, sea
$left_char	= $right_char = $left_cls = '';
if ( ! empty($simData['sim_char_img'])):
	$left_char = $simData['sim_char_img'];
    $left_cls  = 'left_char';
elseif ( ! empty($simData['sim_char_left_img'])):
	$left_char = $simData['sim_char_left_img'];
	$left_cls  = 'left_char';
elseif ( ! empty($simData['char_own_choice']) && file_exists($root_path . $simData['char_own_choice'])):
	$left_char = $path . $simData['char_own_choice'];
	$left_cls  = 'left_char';
endif;
if ( ! empty($simData['sim_char_right_img'])):
	$right_char = 'img/char_bg/'. $simData['sim_char_right_img'];
endif;
if ( ! empty($brand['font_type'])):
$type = str_replace('+', ' ', $brand['font_type']); ?>
<style type="text/css">
@import url("https://fonts.googleapis.com/css2?family=<?php echo $brand['font_type'] ?>:wght@100;200;300;400;500;600;700;800;900&display=swap");
body { font-family: '<?php echo $type ?>' !important; }
h1,h2,h3,h4,h5,h6 { font-family: '<?php echo $type ?>' !important; }
p { font-family: '<?php echo $type ?>' !important; }
</style>
<?php endif; ?>
<script type="text/javascript">
	window.addEventListener('beforeunload', function (e) {
	  // Cancel the event
	  e.preventDefault(); // If you prevent default behavior in Mozilla Firefox prompt will always be shown
	  // Chrome requires returnValue to be set
	  e.returnValue = '';
	});
	
	function disableF5(e) { if ((e.which || e.keyCode) == 116 || (e.which || e.keyCode) == 82) e.preventDefault(); };
	
	$(document).ready(function(){
		$(document).on("keydown", disableF5);
	});
	
	$(document).bind("contextmenu", function(e){
		return false;
	});
	
	history.pushState(null, null, location.href);
    window.onpopstate = function() {
        history.go(1);
    };

	var simBrand = <?php echo json_encode($brand); ?>

	$.LoadingOverlay("show");
</script>
<style type="text/css">
<?php if ( ! empty($back_img)): ?>
.open_res {
	background: url("<?php echo $back_img ?>") center top no-repeat;
	background-size: cover;
	overflow:hidden;
	height:100vh;
	display:flex;
}
<?php elseif ( ! empty($simData['bg_color'])): ?>
.open_res { background: <?php echo $simData['bg_color']; ?>; display:flex;}
<?php endif; if ( ! empty($brand['option_bg'])): ?>
.questioninput:before { background: <?php echo $brand['option_bg']; ?>; }
.questioninput:after { background: <?php echo $brand['option_bg']; ?>; }
.rectangle1:after { background: <?php echo $brand['option_bg']; ?>; }
<?php endif; ?>
.tooltip { position: absolute; }
</style>
<div class="menu-banner-box">
	<div class="time-banner-box">
    	<button class="V_S-tetail" id="view_sim" data-sim-id="<?php echo $sim_id; ?>">
    		<img class="Deskview" src="img/list/view_scenario_b.svg" />
        	<img class="mobview" src="img/list/view_scenario.svg" />
        	View Scenario
    	</button>
  	</div>
    <!---SIM-Timer-->
    <div class="timer-banner">
    	<div class="time-text">
        	<span class="timer-pause" data-minutes-left="<?php echo $simData['duration'] ?>" style="pointer-events: none;"></span>
    	</div>
  	</div>
	<?php if ($save): ?>
    <div class="close-banner">
    	<img class="svg" src="img/list/close_learner.svg" />
    </div>
	<?php endif; ?>
</div>
<script type="text/javascript">
var timer = $('.timer-pause').startTimer({
  onComplete: function(element){
	<?php if ($save): ?>
		$('.signbtn1').show();
	<?php else: ?>
		swal({text: "Simulation time end. please relaunch.!", icon: "info"});
	<?php endif; ?>
  },
  allowPause: true
});
</script>
<div class="open_res">
	<input type="hidden" name="uid" id="uid" value="<?php echo $uid; ?>" />
	<input type="hidden" name="simId" id="simId" value="<?php echo $simID; ?>" />
	<input type="hidden" name="userid" id="userid" value="<?php echo $userid; ?>" />
	<input type="hidden" name="save" id="save" value="<?php echo $save; ?>" />
	<input type="hidden" name="apiurl" id="apiurl" value="<?php echo $db->getBaseUrl('api-process.php') ?>" />
	<?php if ( ! empty($left_char)): ?>
	<div class="CharFlex <?php echo ( ! empty($right_char)) ? 'CharFlextwo' : ''; ?>"><img class="img-fulid qusTempimg" src="<?php echo $db->getBaseUrl('img/char_bg/'. $left_char); ?>" /></div>
	<?php endif; ?>
	<div class="flexQTeMP <?php echo ( ! empty($right_char)) ? 'flexQTeMPtwochar' : ''; ?><?php echo ( ! empty($left_char) && empty($right_char)) ? 'flexQTeMPonechar' : ''; ?><?php echo (empty($left_char) && empty($right_char)) ? 'flexQTeMPnochar' : ''; ?>">
	<?php 
	$qSql = $db->prepare("SELECT * FROM question_tbl WHERE scenario_id = '". $simID ."' ORDER BY qorder = 0, qorder"); $qSql->execute();
	$totalQ = $qSql->rowCount();
	if ($totalQ > 0):
		$q = 1;
		$qdata = $qSql->fetchAll(PDO::FETCH_ASSOC);
		foreach ($qdata as $qrow):
		$qtype = $qrow['question_type'];
		if ( ! empty($ques_id) && $ques_id == $qrow['question_id']):
			$show = 'style="display:block"';
		elseif (empty($ques_id) && $q == 1):
			$show = 'style="display:block"';
		else:
			$show = 'style="display:none"';
		endif; ?>
		<div class="MatchingQusBannerlunch MatchingQusBanner_<?php echo $q; ?>" <?php echo $show; ?> id="option_<?php echo $q; ?>" data-qtype="<?php echo $qtype ?>">
			<div class="Matchingcontainer">
				<div class="Questmtf">
					<div class="MatchingQusTextmtf fade-in">
						<div class="qus-text openresponseQt">
							<h3>QUESTION <?php echo $q .'/'. $totalQ; ?></h3>
							<p class="more"><?php echo stripslashes($qrow['questions']); ?></p>
						</div>
						<div class="qusassestbox">
							<div class="Qusimg">
							<?php if ( ! empty($qrow['audio'])): ?>
							<audio id="qvideo-<?php echo $q ?>" class="video-js vjs-theme-<?php echo $vtheme ?>" controls preload="auto" width="300px" height="100px" data-setup="{}" data-type="audio" poster="<?php echo $aPoster ?>">
								<source src="<?php echo $path . $qrow['audio'] ?>" type="audio/<?php echo strtolower(pathinfo($qrow['audio'], PATHINFO_EXTENSION)); ?>" />
							</audio>
							<?php elseif ( ! empty($qrow['video'])): ?>
							<a href="<?php echo $path . $qrow['video']; ?>" data-fancybox data-caption="<?php echo stripslashes($qrow['questions']); ?>" class="qassets_icon">
								<video id="qvideo-<?php echo $q ?>" class="video-js vjs-theme-<?php echo $vtheme ?>" controls preload="auto" width="640" height="264" data-setup="{}" data-type="video">
									<source src="<?php echo $path . $qrow['video']; ?>" type="video/<?php echo strtolower(pathinfo($qrow['video'], PATHINFO_EXTENSION)); ?>" />
								</video>
							</a>
							<?php elseif ( ! empty($qrow['screen'])): ?>
							<a href="<?php echo $path . $qrow['screen']; ?>" data-fancybox data-caption="<?php echo stripslashes($qrow['questions']); ?>" class="qassets_icon">
								<video id="qvideo-<?php echo $q ?>" class="video-js vjs-theme-<?php echo $vtheme ?>" controls preload="auto" width="200" height="100" data-setup="{}" data-type="video">
									<source src="<?php echo $path . $qrow['screen']; ?>" type="video/<?php echo strtolower(pathinfo($qrow['screen'], PATHINFO_EXTENSION)); ?>" />
								</video>
							</a>
							<?php elseif ( ! empty($qrow['image'])): ?>
							<a href="<?php echo $path . $qrow['image']; ?>" data-fancybox data-caption="<?php echo stripslashes($qrow['questions']); ?>" class="qassets_icon">
								<img class="img-fluid" src="<?php echo $path . $qrow['image']; ?>">
							</a>
							<?php elseif ( ! empty($qrow['document'])): ?>
							<a href="javascript:;" data-src="<?php echo $path . $qrow['document']; ?>" data-fancybox data-type="iframe" data-caption="<?php echo stripslashes($qrow['questions']); ?>" class="document">
								<img class="img-fluid pdfclissicsIm" src="img/icons/pdf_icon.svg">
							</a>
							<?php elseif ( ! empty($qrow['speech_text'])): ?>
							<button onclick="speak('div.intro_<?php echo $q ?>')">
								<i id="playicon" class="fa fa-play playicon" aria-hidden="true"></i>
								<i id="pauseicon" class="fa fa-pause pauseicon" style="display:none" aria-hidden="true"></i></button>
								<div class="intro_<?php echo $q ?>" style="display:none;"><?php echo $qrow['speech_text'] ?></div>
							<?php endif; ?>
							</div>
						</div>
					</div>
					<div class="ORsubmitbox zoomIn fade-in" id="action_<?php echo $q; ?>">
						<div class="instance">Select the format you want to use to respond to the question.</div>
						<div class="ORgraaybox">
							<div class="ORbox ORbox1" data-toggle="tooltip" data-placement="top" title="Text only">
								<img class="img-fluid add-tts action" src="img/list/text_only.svg" />
							</div>
							<div class="ORbox ORbox2" data-toggle="tooltip" data-placement="top" title="Screen record">
								<img class="img-fluid rec-screen action" src="img/list/screen_record1.svg" />
							</div>
							<div class="ORbox ORbox3" data-toggle="tooltip" data-placement="top" title="Voice record">
								<img class="img-fluid rec-audio action" src="img/list/voice_record.svg" />
							</div>
							<div class="ORbox ORbox4" data-toggle="tooltip" data-placement="top" title="Video record">
								<img class="img-fluid rec-video action" src="img/list/video_record.svg" />
							</div>
						</div>
					</div>
					<div class="ORsubmitbox" id="open_answer_btn_<?php echo $q; ?>">
						<button type="button" id="btn_<?php echo $q; ?>" class="btn submitbtn fade-in " data-show-report="<?php echo ($totalQ == $q) ? 'true' : 'false'; ?>" data-target="<?php echo $q + 1 ?>" data-cur="<?php echo $q ?>">NEXT</button>
					</div>
				</div>
			</div>
		</div>
		<?php $q++; endforeach; endif; ?>
	</div>
	<?php if ( ! empty($right_char)): ?>
	<div class="CharFlextwo right">
		<img class="img-fulid qusTempimg" src="<?php echo $right_char; ?>">
	</div>
	<?php endif; ?>
</div>

<div id="load_popup_modal_show" class="modal fade openReSonceModel viewlunchModel" tabindex="-1" data-keyboard="false" data-backdrop="static"></div>

<script>
$(function(){
	$('[data-toggle="tooltip"]').tooltip();	
	$('.action').on('click', function(){
		swal({text: 'These options are active in Learner and Educator view only.', buttons: false, icon: "warning", timer: 3000});
	});
});
</script>
<script src="content/js/moreless.js"></script>
<script src="content/js/articulate.js"></script>
<script src="content/js/owlcarousel/owl.carousel.min.js"></script>
<script src="content/js/launch-sim.js?v=<?php echo time() ?>"></script>
<script src="content/js/launch-sim-open-response-preview.js"></script>
<script src="content/js/sim-branding.js?v=<?php echo time() ?>"></script>
<?php 
require_once 'includes/footer.php';
