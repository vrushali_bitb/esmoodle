<?php 
ob_start();
session_start();
if (isset($_SESSION['username'])) {
  $user     = $_SESSION['username'];
  $role     = $_SESSION['role'];
  $userid   = $_SESSION['userId'];
  $cid		= (isset($_SESSION['client_id'])) ? $_SESSION['client_id'] : exit('Oops. something went wrong please try again');
  $domain   = (isset($_SESSION['domain_name'])) ? $_SESSION['domain_name'] : $_SERVER['HTTP_HOST'];
} else {
  header('location: index.php');
}
require_once 'config/db.class.php';
require_once 'includes/header.php';
$db         = new DBConnection();
$sim_id		= (isset($_GET['review']) && ! empty($_GET['sim_id'])) ? $_GET['sim_id'] : '';
$sim_data   = $db->getScenario($sim_id);
$path		= 'scenario/upload/'.$domain.'/';
$noimg		= 'scenario/upload/noimage.png';
if ( ! empty($sim_data['sim_cover_img']) && file_exists($path . $sim_data['sim_cover_img'])):
    $assetsUrl = $path . $sim_data['sim_cover_img'];
else:
    $assetsUrl = $noimg;
endif; ?>
<input type="hidden" id="url" value="<?php echo base64_encode($db->getBaseUrl($db->getFile() .'?review=true&sim_id='. $sim_id)); ?>" />
<input type="hidden" id="scenario_id" value="<?php echo $sim_data['scenario_id'] ?>" />
<input type="hidden" id="user_id" value="<?php echo $userid ?>" />
<div class="rev-top-banner">
	<div class="rev-topLbanner">
        <a data-fancybox data-caption="<?php echo $sim_data['Scenario_title']; ?>" href="<?php echo $assetsUrl; ?>"><img class="img-fluid" src="<?php echo $assetsUrl; ?>"></a>
		<div class="revTopText">
            <h3 class="title" title="<?php echo $sim_data['Scenario_title']; ?>"><?php echo $db->truncateText($sim_data['Scenario_title'], 50); ?></h3>
			<p>Author:</p>
            <?php if ( ! empty($sim_data['assign_author'])): ?>
            <a data-toggle="popover" title="View Author" href="<?php echo $db->getBaseUrl('includes/ajax.php?getTooltipData=true&ids='. $sim_data['assign_author']); ?>" onclick="return false"><i class="fa fa-info-circle fa-2x" aria-hidden="true"></i></a>
            <?php endif; ?>
		</div>
	</div>
	<div class="rev-topRbanner">
		<div class="btn rev" id="share-button">GET SHAREABLE lINK</div>
		<?php if ($role == 'reviewer'): ?>
		<div class="btn rev" id="Notify" data-login-type="author">NOTITY AUTHOR</div>
		<?php elseif ($role == 'author'): ?>
		<div  class="btn rev" id="Notify" data-login-type="reviewer">NOTITY REVIEWER</div>
		<?php endif; ?>
		<div class="btn rev" onclick="getPDF('.show_comment')">EXPORT PDF</div>
		<div class="btn rev">REVIEW</div>
		<div class="btn rev">FEEDBACK</div>
	</div>
</div>
<div class="reviewfeedbackBanner">
	<div class="reviewfeedbox">
		<div class="QUES_F_S">QUESTION 1</div>
		<div class="jumbotron jumb-white">
			<div class="container">
				<div class="Feed_CONtainer">
					<div class="bulletComment">	
						<div class="commentbox">
							<div class="Replyname" style="background:#00bcd4">MD</div>
							<div class="comment-textbanner">							
								<div class="NameDate">
									<span>MD Author</span>
									<span class="Datetext">19-05-21, 04:48 PM</span>
								</div>
								<div class="comment-text">hello</div>
							</div>
						</div>
					</div>
					<div class="bulletComment">					
						<span class="remove_chat" data-comment-id="18" data-parent-comment-id="16"><i class="fa fa-times" aria-hidden="true"></i></span>
						<div class="commentbox">
							<div class="Replyname" style="background:#faab21">SW</div>
							<div class="comment-textbanner">							
								<div class="NameDate">
									<span>Shivani Walia,</span>
									<span class="Datetext">19-05-21, 04:48 PM</span>
								</div>
								<div class="comment-text">hello</div>
							</div>
						</div>
					</div>
					<form class="comment-form">
						<div class="Rev-replycomment">
							<input type="hidden" name="scenario_id" id="scenario_id" value="117">
							<input type="hidden" name="comment_add" value="1">
							<input type="hidden" name="chat_type" value="1">
							<input type="hidden" name="uid" value="11">
							<input type="hidden" name="comment_id" id="commentId" value="19">
							<input type="text" class="form-control comment" name="comment" required="required" placeholder="Add Reply..." aria-describedby="basic-addon2">
						</div>
						<ul class="msgpostbtn"><li class="msgpstgray"><input type="submit" value="Reply"></li></ul>
					</form>
				</div>
				<div class="Feed_CONtainer">
					<img class="img-fluid" src="img/img1.PNG">
				</div>
			</div>
		</div>
		<div class="QUES_F_S">QUESTION 2</div>
		<div class="jumbotron jumb-white">
			<div class="container">
				<div class="Feed_CONtainer">
					<div class="bulletComment">	
						<div class="commentbox">
							<div class="Replyname" style="background:#00bcd4">MD</div>
							<div class="comment-textbanner">							
								<div class="NameDate">
									<span>MD Author</span>
									<span class="Datetext">19-05-21, 04:48 PM</span>
								</div>
								<div class="comment-text">hello</div>
							</div>
						</div>
					</div>
					<div class="bulletComment">					
						<span class="remove_chat" data-comment-id="18" data-parent-comment-id="16"><i class="fa fa-times" aria-hidden="true"></i></span>
						<div class="commentbox">
							<div class="Replyname" style="background:#faab21">SW</div>
							<div class="comment-textbanner">							
								<div class="NameDate">
									<span>Shivani Walia,</span>
									<span class="Datetext">19-05-21, 04:48 PM</span>
								</div>
								<div class="comment-text">hello</div>
							</div>
						</div>
					</div>
					<form class="comment-form">
						<div class="Rev-replycomment">
							<input type="hidden" name="scenario_id" id="scenario_id" value="117">
							<input type="hidden" name="comment_add" value="1">
							<input type="hidden" name="chat_type" value="1">
							<input type="hidden" name="uid" value="11">
							<input type="hidden" name="comment_id" id="commentId" value="19">
							<input type="text" class="form-control comment" name="comment" required="required" placeholder="Add Reply..." aria-describedby="basic-addon2">
						</div>
						<ul class="msgpostbtn"><li class="msgpstgray"><input type="submit" value="Reply"></li></ul>
					</form>
				</div>
				<div class="Feed_CONtainer">
					<img class="img-fluid" src="img/img1.PNG">
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript" src="content/js/comment.js"></script>
<script type="text/javascript" src="content/js/tooltip.js"></script>
<script type="text/javascript" src="content/js/polyfill.min.js"></script>
<script type="text/javascript" src="content/js/jspdf.min.js"></script>
<script type="text/javascript" src="content/js/html2canvas.js"></script>
<script type="text/javascript">
	/* GET SHAREABLE LINK */
	var $temp = $("<input>");
	var $url = $(location).attr('href');
	$('#share-button').on('click', function() {
		$("body").append($temp);
		$temp.val($url).select();
		document.execCommand("copy");
		$temp.remove();
		$(this).text("Link copied.!");
	});
	/* GET COMMENT PDF */
	function getPDF(id){
		var HTML_Width = $(id).width();
		var HTML_Height = $(id).height();
		var top_left_margin = 15;
		var PDF_Width = HTML_Width+(top_left_margin*2);
		var PDF_Height = (PDF_Width*1.5)+(top_left_margin*2);
		var canvas_image_width = HTML_Width;
		var canvas_image_height = HTML_Height;		
		var totalPDFPages = Math.ceil(HTML_Height/PDF_Height)-1;
		html2canvas($(id)[0],{allowTaint:true}).then(function(canvas) {
			canvas.getContext('2d');
			//console.log(canvas.height+"  "+canvas.width);
			var imgData = canvas.toDataURL("image/jpeg", 1.0);
			//console.log(canvas);
			var pdf = new jsPDF('p', 'pt',  [PDF_Width, PDF_Height]);
		    pdf.addImage(imgData, 'JPG', top_left_margin, top_left_margin,canvas_image_width,canvas_image_height);
			for (var i = 1; i <= totalPDFPages; i++) {
				pdf.addPage(PDF_Width, PDF_Height);
				pdf.addImage(imgData, 'JPG', top_left_margin, -(PDF_Height*i)+(top_left_margin*4),canvas_image_width,canvas_image_height);
			}
			pdf.save("<?php echo $sim_data['Scenario_title'] ?>-comment.pdf");
        });
	};
</script>
<?php 
require_once 'includes/footer.php';
