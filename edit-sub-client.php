<?php 
ob_start();
session_start();
if (isset($_SESSION['username'])) {
	$client_id	= $_SESSION['client_id'];
	$userid		= $_SESSION['userId'];
    $user		= $_SESSION['username'];
	$role		= $_SESSION['role'];
}
else {
	header('location: index');
}
require_once 'config/db.class.php';
require_once 'includes/learners_menu.php';
$db   		= new DBConnection();

/* page access only client administrator */
($db->checkAccountType() != 2) ? header('location: index.php') : '';

$cid  		= ( ! empty($_GET['cid'])) ? base64_decode($_GET['cid']) : exit('Error');
$data 		= $db->getClient($cid);
$logo 		= $data['logo'];
$logo_path	= $db->getBaseUrl('img/logo/');
$get_domain = str_replace('.easysim.app', '', $data['domain_name']);
$columns 	= 'name, email, phone_code, phone';
$tbl_name	= 'client_contact_detail';
$condi 		= " WHERE client_id = '". $cid ."'";
$execond 	= $condi . " AND  type = 1";
$deccond 	= $condi . " AND  type = 2";
$billcond 	= $condi . " AND  type = 3";
$executive 	= @$db->getClientData($columns, $tbl_name, $execond)[0];
$decision  	= @$db->getClientData($columns, $tbl_name, $deccond)[0];
$billing  	= @$db->getClientData($columns, $tbl_name, $billcond)[0]; ?>
<div class="bottomheader">
  <div class="container-fluid">
    <ul class="breadcrumb">
      <?php echo $db->breadcrumbs(); ?>
    </ul>
  </div>
</div>
<div class="simulation_assignment super-client-manag">
	<div class="container-fluid">
    	<div class="row">
        	<div class="col-sm-12">
			<div class="client-backbtn"><button type="button" class="btn btn-primary" onclick="window.history.back();">Back</button></div>
            <form enctype="multipart/form" name="create_db_form" id="create_db_form" method="post">
				<input type="hidden" name="currency_code" id="currency_code" value="<?php echo ( ! empty($data['amount_cc'])) ? $data['amount_cc'] : '' ?>" />
				<input type="hidden" name="client_id" value="<?php echo $cid; ?>">
				<input type="hidden" name="old_domain" id="old_domain" value="<?php echo $get_domain; ?>">
				<div class="panel-body">
					<ul class="nav nav-tabs Screatenewadmin-tab">
						<li class="active"><a data-toggle="tab" href="#OrganizationDetail">Organization Detail</a></li>
						<li><a data-toggle="tab" href="#Contactdetail">Contact detail</a></li>
						<li><a data-toggle="tab" href="#LoginDetail">Login Detail</a></li>
						<li><a data-toggle="tab" href="#LicenseInformation">License Information</a></li>
						<li><a data-toggle="tab" href="#Hostingdetail">Hosting detail</a></li>
						<li><a data-toggle="tab" href="#ManageBranding">Manage Permissions</a></li>
						<li><a data-toggle="tab" href="#ManageTemp">Manage Template</a></li>
						<li><a data-toggle="tab" href="#PaymentDetail">Payment Detail</a></li>
					</ul>
					<div class="tab-content super-createnewadmin">
						<div id="OrganizationDetail" class="tab-pane fade in active">
							<div class="usermanage-form">
								<div class="row">
									<div class="col-sm-4">
										<div class="form-group">
											<input type="text" name="organization" id="organization" class="form-control" placeholder="Organization Name *" required="required" value="<?php echo $data['organization'] ?>" />
										</div>
									</div>
									<div class="col-sm-4">
										<div class="form-group">
											<input type="file" name="logo" id="logo" class="form-control file" />
											<input type="text" class="form-control controls" disabled placeholder="Upload Logo" value="<?php echo ( ! empty($logo)) ? $logo : ''; ?>" />
											<input type="hidden" name="logo_name" id="logo_name" value="<?php echo ( ! empty($logo)) ? $logo : ''; ?>" />										
											<div class="browsebtn browsebtntmp">
												<span id="splashImg">
												<?php if ( ! empty($logo)) : ?>
												<a href="<?php echo $logo_path . $logo ?>" target="_blank" title="View Logo"><i class="fa fa-eye" aria-hidden="true"></i></a>
												<?php endif; ?>
												</span>
												<span id="ImgDelete" <?php echo ( ! empty($logo)) ? '' : 'style="display:none"'; ?>>
												<a href="javascript:void(0);" data-img-name="<?php echo ( ! empty($logo)) ? $logo : ''; ?>" class="delete_logo" title="Delete Logo"><i class="fa fa-times" aria-hidden="true"></i></a>
												</span>
											</div>
										</div>
									</div>
									<div class="col-sm-2">
										<button class="browse btn btn-primary" type="button">Browse</button>
									</div>
									<div class="col-sm-2">
										<button type="button" name="upload" id="upload" class="btn btn-primary">Uplaod</button>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-4">
										<div class="form-group">
											<select name="company_country" id="company_country" class="form-control country" data-starget="company_state" data-ctarget="company_city" data-cid="<?php echo $data['country'] ?>" data-sid="<?php echo $data['state'] ?>" data-city="<?php echo $data['city'] ?>">
												<option value="" selected="" disabled="disabled">Select Country</option>
											</select>
										</div>
									</div>
									<div class="col-sm-4">
										<div class="form-group">
											<select name="company_state" id="company_state" class="form-control state" data-ctarget="company_city">
												<option value="" selected="" disabled="disabled">Select State</option>
											</select>
										</div>
									</div>
									<div class="col-sm-4">
										<div class="form-group">
											<select name="company_city" id="company_city" class="form-control city">
												<option value="" selected="" disabled="disabled">Select City</option>
											</select>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-4">
										<div class="form-group">
											<input type="text" class="form-control" name="tax_id" id="tax_id" placeholder="Tax ID" value="<?php echo $data['tax_id'] ?>" />
										</div>
									</div>
									<div class="col-sm-4">
										<div class="form-group">
											<textarea class="form-control" name="bill_add" id="bill_add" placeholder="Organization Address"><?php echo $data['address'] ?></textarea>
										</div>
									</div>
								</div>
							</div>
								<ul class="list-inline pull-right">
								<li><button type="button" class="btn btn-primary next-step">Continue</button></li>
							</ul>
						</div>
						<div id="Contactdetail" class="tab-pane fade">
							<div class="Contactinterhead">Executive Contact</div>								
								<div class="usermanage-form">
									<div class="row">
										<div class="col-sm-3">
											<div class="form-group">
												<input type="text" class="form-control" name="executive_name" id="executive_name" placeholder="Contact Name" value="<?php echo ( ! empty($executive['name'])) ? $executive['name'] : ''; ?>" />
											</div>
										</div>
										<div class="col-sm-3">
											<div class="form-group">
												<input type="email" class="form-control" name="executive_email" id="executive_email" placeholder="Email" value="<?php echo ( ! empty($executive['email'])) ? $executive['email'] : ''; ?>" />
											</div>
										</div>
										<div class="col-sm-3">
											<div class="form-group">
												<select name="executive_pcode" id="executive_pcode" class="form-control phonecode" data-target="executive_pcode" data-pcode="<?php echo ( ! empty($executive['phone_code'])) ? $executive['phone_code'] : ''; ?>">
													<option value="" selected="" disabled="disabled">Select Phone Code</option>
												</select>
											</div>
										</div>
										<div class="col-sm-3">
											<div class="form-group">
												<input type="tel" class="form-control" name="executive_mob" id="executive_mob" placeholder="Contact Ph Number" value="<?php echo ( ! empty($executive['phone'])) ? $executive['phone'] : ''; ?>" />
											</div>
										</div>
									</div>
								</div>
								
								<div class="Contactinterhead">Decision Maker</div>								
								<div class="usermanage-form">
									<div class="row">
										<div class="col-sm-3">
											<div class="form-group">
												<input type="text" class="form-control" name="decision_name" id="decision_name" placeholder="Contact Name" value="<?php echo ( ! empty($decision['name'])) ? $decision['name'] : ''; ?>" />
											</div>
										</div>
										<div class="col-sm-3">
											<div class="form-group">
												<input type="email" class="form-control" name="decision_email" id="decision_email" placeholder="Email" value="<?php echo ( ! empty($decision['email'])) ? $decision['email'] : ''; ?>" />
											</div>
										</div>
										<div class="col-sm-3">
											<div class="form-group">
												<select name="decision_pcode" id="decision_pcode" class="form-control phonecode" data-target="decision_pcode" data-pcode="<?php echo ( ! empty($decision['phone_code'])) ? $decision['phone_code'] : ''; ?>">
													<option value="" selected="" disabled="disabled">Select Phone Code</option>
												</select>
											</div>
										</div>
										<div class="col-sm-3">
											<div class="form-group">
												<input type="tel" class="form-control" name="decision_mob" id="decision_mob" placeholder="Contact Ph Number" value="<?php echo ( ! empty($decision['phone'])) ? $decision['phone'] : ''; ?>" />
											</div>
										</div>
									</div>
								</div>
					
								<div class="Contactinterhead">Billing Contact</div>								
								<div class="usermanage-form">
									<div class="row">
										<div class="col-sm-3">
											<div class="form-group">
												<input type="text" class="form-control" name="billing_name" id="billing_name" placeholder="Contact Name" value="<?php echo ( ! empty($billing['name'])) ? $billing['name'] : ''; ?>" />
											</div>
										</div>
										<div class="col-sm-3">
											<div class="form-group">
												<input type="email" class="form-control" name="billing_email" id="billing_email" placeholder="Email" value="<?php echo ( ! empty($billing['email'])) ? $billing['email'] : ''; ?>" />
											</div>
										</div>
										<div class="col-sm-3">
											<div class="form-group">
												<select name="billing_pcode" id="billing_pcode" class="form-control phonecode" data-target="billing_pcode" data-pcode="<?php echo ( ! empty($billing['phone_code'])) ? $billing['phone_code'] : ''; ?>">
													<option value="" selected="" disabled="disabled">Select Phone Code</option>
												</select>
											</div>
										</div>
										<div class="col-sm-3">
											<div class="form-group">
												<input type="tel" class="form-control" name="billing_mob" id="billing_mob" placeholder="Contact Ph Number" value="<?php echo ( ! empty($billing['phone'])) ? $billing['phone'] : ''; ?>" />
											</div>
										</div>
									</div>
								</div>
								<ul class="list-inline pull-right">
									<li><button type="button" class="btn btn-primary prev-step">Back</button></li>
									<li><button type="button" class="btn btn-primary next-step">Continue</button></li>
								</ul>
						</div>
						<div id="LoginDetail" class="tab-pane fade">
							<div class="usermanage-form">
								<div class="row">
									<div class="col-sm-3">
										<div class="input-group">
											<input type="text" name="domain_name" id="domain_name" class="form-control" placeholder="Subdomain Name *" autocomplete="off" required="required" title="Please give valid Subdomain name, no spaces or special characters allowed." value="<?php echo $get_domain ; ?>" disabled />
											<span class="input-group-addon">.easysim.app</span>
										</div>
									</div>
									<div class="col-sm-3">
										<div class="form-group form-select">
											<select class="form-control" name="orgtype" required="required" data-placeholder="Organization Type *">
												<option value="" selected="" disabled="disabled">Select Organization Type</option>
												<option value="Company" <?php echo ( ! empty($data['account_type']) && $data['account_type'] == 'Company') ? 'selected' : ''; ?>>Company</option>
												<option value="Indiviual" <?php echo ( ! empty($data['account_type']) && $data['account_type'] == 'Indiviual') ? 'selected' : ''; ?>>Indiviual</option>
												<option value="Reseller" <?php echo ( ! empty($data['account_type']) && $data['account_type'] == 'Reseller') ? 'selected' : ''; ?>>Reseller</option>
											</select>
										</div>
									</div>
									<div class="col-sm-3">
										<div class="form-group form-select">
											<?php $roles = $db->role;
											if (array_key_exists('clientAdmin', $roles)) {
												unset($roles['clientAdmin']);
											} ?>
											<select class="form-control multiselect-ui selection" multiple="multiple" name="role[]" id="role" required="required" data-placeholder="User Role *">
											<?php foreach ($roles as $roleKey => $roleVal): ?>
											<option value="<?php echo $roleKey; ?>"><?php echo $roleVal; ?></option>
											<?php endforeach; ?>
											</select>
										</div>
									</div>
								</div>
								<div class="Contactinterhead">SMTP Mail Configuration</div>
								<div class="row">
									<div class="col-sm-3">
										<div class="form-group">
											<input type="text" name="smtp_host_name" id="smtp_host_name" class="form-control" placeholder="SMTP Host Name" autocomplete="off" value="<?php echo ( ! empty($data['smtp_host_name'])) ? $data['smtp_host_name'] : '' ?>" />
										</div>
									</div>
									<div class="col-sm-3">
										<div class="form-group">
											<input type="text" name="smtp_user_name" id="smtp_user_name" class="form-control" placeholder="SMTP User Name" autocomplete="off" value="<?php echo ( ! empty($data['smtp_user_name'])) ? $data['smtp_user_name'] : '' ?>" />
										</div>
									</div>
									<div class="col-sm-3">
										<div class="form-group">
											<input type="text" name="smtp_pwd" id="smtp_pwd" class="form-control" placeholder="SMTP Password" autocomplete="off" value="<?php echo ( ! empty($data['smtp_pwd'])) ? $data['smtp_pwd'] : '' ?>" />
										</div>
									</div>
									<div class="col-sm-3">
										<div class="form-group">
											<input type="text" name="smtp_port" id="smtp_port" class="form-control" placeholder="SMTP PORT" autocomplete="off" value="<?php echo ( ! empty($data['smtp_port'])) ? $data['smtp_port'] : '' ?>" />
										</div>
									</div>
								</div>
							</div>
							<ul class="list-inline pull-right">
								<li><button type="button" class="btn btn-primary prev-step">Back</button></li>
								<li><button type="button" class="btn btn-primary next-step">Continue</button></li>
							</ul>
						</div>
						<div id="LicenseInformation" class="tab-pane fade">
							<div class="usermanage-form">
								<div class="row">
									<div class="col-sm-3">
										<div class="form-group">
											<div class="input-group date form_datetime" data-date-format="yyyy-mm-dd" data-link-field="dtp_input1">
												<input class="form-control" name="start_date" type="text" readonly placeholder="Start Date" value="<?php echo ($data['start_date'] != '0000-00-00') ? $data['start_date'] : ''; ?>">
												<span class="input-group-addon"><span class="fa fa-calendar"></span></span>
											</div>
										</div>
									</div>
									<div class="col-sm-3">
										<div class="form-group">
											<div class="input-group date form_datetime" data-date-format="yyyy-mm-dd" data-link-field="dtp_input1">
												<input class="form-control" name="end_date" type="text" readonly placeholder="End Date" value="<?php echo ($data['end_date'] != '0000-00-00') ? $data['end_date'] : ''; ?>">
												<span class="input-group-addon"><span class="fa fa-calendar"></span></span>
											</div>
										</div>
									</div>
								</div>
								<div class="row">	
									<div class="col-sm-12">											
										<div class="Catfield-banner">
											<div class="Categoriesfield">
												<input type="text" class="form-control Licensing_Info_Categories" name="no_cat" id="no_cat" placeholder="No. of Categories" value="<?php echo $data['category'] ?>" />
											</div>
											<div class="Catfield">
												<input type="text" class="form-control Licensing_Info_Categories" name="no_sim" id="no_sim" placeholder="No. of Simulation in each Category" value="<?php echo $data['sim'] ?>" />
											</div>
											<div class="Catfield">
												<input type="text" class="form-control Licensing_Info_Categories" name="no_admin" id="no_admin" placeholder="No. of Admin" value="<?php echo $data['admin'] ?>" />
											</div>
											<div class="Catfield">
												<input type="text" class="form-control Licensing_Info_Categories" name="no_auth" id="no_auth" placeholder="No. of Author" value="<?php echo $data['auth'] ?>" />
											</div>
											<div class="Catfield"> 
												<input type="text" class="form-control Licensing_Info_Categories" name="no_reviewer" id="no_reviewer" placeholder="No. of Reviewer" value="<?php echo $data['reviewer'] ?>" />
											</div>
											<div class="Catfield">
												<input type="text" class="form-control Licensing_Info_Categories" name="no_learner" id="no_learner" placeholder="No. of Learner" value="<?php echo $data['learner'] ?>" />
											</div>
											<div class="Catfield">
												<input type="text" class="form-control Licensing_Info_Categories" name="no_educator" id="no_educator" placeholder="No. of Educator" value="<?php echo $data['educator'] ?>" />
											</div>
										</div>
									</div>
								</div>
							</div>
							<ul class="list-inline pull-right">
								<li><button type="button" class="btn btn-primary prev-step">Back</button></li>
								<li><button type="button" class="btn btn-primary next-step">Continue</button></li>
							</ul>
						</div>
						<div id="Hostingdetail" class="tab-pane fade">
							<div class="usermanage-form">
								<div class="row">
									<div class="col-sm-5">
										<div class="form-group">
											<input type="text" class="form-control Licensing_Info_Categories" name="hosting_space" id="hosting_space" placeholder="Hosting Space (MB)" value="<?php echo $data['hosting_space'] ?>" />
										</div>
									</div>
								</div>
							</div>
							<ul class="list-inline pull-right">
								<li><button type="button" class="btn btn-primary prev-step">Back</button></li>
								<li><button type="button" class="btn btn-primary next-step">Continue</button></li>
							</ul>										
						</div>
						<div id="ManageBranding" class="tab-pane fade">
							<div class="usermanage-form">
								<div class="Check-box"><label class="checkstyle"><input type="checkbox" name="mhome" value="1" <?php echo ( ! empty($data['mhome'])) ? 'checked' : '' ?>><span class="checkmark"></span>Manage Home Page</label></div>
								<div class="Check-box"><label class="checkstyle"><input type="checkbox" name="mbrand" value="1" <?php echo ( ! empty($data['mbrand'])) ? 'checked' : '' ?>><span class="checkmark"></span>Manage Branding</label></div>
								<div class="Check-box"><label class="checkstyle"><input type="checkbox" name="proctor" value="1" <?php echo ( ! empty($data['proctor'])) ? 'checked' : '' ?>><span class="checkmark"></span>Manage Proctoring</label></div>
							</div>
							<ul class="list-inline pull-right">
								<li><button type="button" class="btn btn-primary prev-step">Back</button></li>
								<li><button type="button" class="btn btn-primary next-step">Continue</button></li>
							</ul>
						</div>
						<div id="ManageTemp" class="tab-pane fade SuperManageTemp">
							<?php $sim_temp = ( ! empty($data['templates'])) ? explode(',', $data['templates']) : []; ?>
							<div class="Check-box GrantCheckbox">
								<label class="checkstyle">
									<input type="checkbox" class="checked_all" />
									<span class="checkmark"></span>
									<span class="text-R">Grant All Template</span>
								</label>
							</div>
							<div class="edit-sim-box">
								<?php if (in_array(1, $sim_temp) || in_array(2, $sim_temp)): ?>
								<div class="user_text editSim">Classic Simulation - Linear Simulation</div>
								<div class="linersim">				
									<div class="edit-sim-radio Linerm">
										<?php if (in_array(1, $sim_temp)): ?>
										<div class="Check-box">
											<label class="checkstyle">
												<input type="checkbox" name="sim_tmp[]" value="1" class="linear_tmp tmp_type" <?php echo (in_array(1, $sim_temp)) ? 'checked' : ''; ?> />
												<span class="checkmark"></span>
												<span class="text-R">Classic Template</span>
											</label>
											<span class="smallT">
												<p class="question-type">(MCQ, Drag and Drop, Swiping)</p>
												<p class="question-type">(Multiple Feedback)</p>
											</span>
										</div>
										<?php endif; ?>
										<?php if (in_array(2, $sim_temp)): ?>
										<div class="Check-box">
											<label class="checkstyle">
												<input type="checkbox" name="sim_tmp[]" value="2" class="linear_tmp tmp_type" <?php echo (in_array(2, $sim_temp)) ? 'checked' : ''; ?> />
												<span class="checkmark"></span>
												<span class="text-R">Multiple Template</span>
											</label>
											<span class="smallT">
												<p class="question-type">(MTF, Sequence, Sorting, MMCQ, MCQ, Drag and Drop, Swiping)</p>
												<p class="question-type">(Single Feedback)</p>
											</span>
										</div>
										<?php endif; ?>
									</div>
								</div>
								<?php endif; ?>
								<?php if (in_array(4, $sim_temp) || in_array(5, $sim_temp)): ?>
								<div class="linersim linersimBotom">
									<div class="user_text editSim">Classic Simulation - Branching Simulation</div>					
									<div class="edit-sim-radio Linerm">
										<?php if (in_array(4, $sim_temp)): ?>
										<div class="Check-box">
											<label class="checkstyle">
												<input type="checkbox" name="sim_tmp[]" value="4" class="branch_tmp tmp_type" <?php echo (in_array(4, $sim_temp)) ? 'checked' : ''; ?> />
												<span class="checkmark"></span>
												<span class="text-R">Classic Template</span>
											</label>
											<span class="smallT">
												<p class="question-type">(MCQ, Drag and Drop,Swiping)</p>
												<p class="question-type">(Multiple Feedback)</p>
											</span>
										</div>
										<?php endif; ?>
										<?php if (in_array(5, $sim_temp)): ?>
										<div class="Check-box">
											<label class="checkstyle">
												<input type="checkbox" name="sim_tmp[]" value="5" class="branch_tmp tmp_type" <?php echo (in_array(5, $sim_temp)) ? 'checked' : ''; ?> />
												<span class="checkmark"></span>
												<span class="text-R">Multiple Template</span>
											</label>
											<span class="smallT">
												<p class="question-type">(MTF, Sorting, MMCQ, MCQ, Drag and Drop, Swiping)</p>
												<p class="question-type">(Single Feedback)</p>
											</span>
										</div>
										<?php endif; ?>
									</div>
								</div>
								<?php endif; ?>
								<?php if (in_array(7, $sim_temp)): ?>
								<div class="user_text editSim">Open Response Simulation</div>
								<div class="linersim linersimlast">
									<div class="Check-box">
										<label class="checkstyle">
											<input type="checkbox" name="sim_tmp[]" value="7" class="tmp_type" <?php echo (in_array(7, $sim_temp)) ? 'checked' : ''; ?> />
											<span class="checkmark"></span>
											<span class="text-R">Open Response</span>
										</label>
									</div>
								</div>
								<?php endif; ?>
								<?php if (in_array(8, $sim_temp)): ?>
								<div class="user_text editSim">Video Simulation</div>
								<div class="linersim">
									<div class="Check-box">
										<label class="checkstyle">
											<input type="checkbox" name="sim_tmp[]" value="8" class="tmp_type" <?php echo (in_array(8, $sim_temp)) ? 'checked' : ''; ?> />
											<span class="checkmark"></span>
											<span class="text-R">Single Video Simulation</span>
										</label>
									</div>
								</div>
								<?php endif; ?>
								<?php if (in_array(6, $sim_temp) || in_array(9, $sim_temp)): ?>
								<div class="linersim">
									<div class="user_text editSim">Linear Video Simulation</div>				
									<div class="edit-sim-radio Linerm">
										<?php if (in_array(6, $sim_temp)): ?>
										<div class="Check-box">
											<label class="checkstyle">
												<input type="checkbox" name="sim_tmp[]" value="6" class="linear_tmp tmp_type" <?php echo (in_array(6, $sim_temp)) ? 'checked' : ''; ?> />
												<span class="checkmark"></span>
												<span class="text-R">Classic Template</span>
											</label>
											<span class="smallT">
												<p class="question-type">(MCQ, Drag and Drop, Swiping)</p>
												<p class="question-type">(Multiple Feedback)</p>
											</span>
										</div>
										<?php endif; ?>
										<?php if (in_array(9, $sim_temp)): ?>
										<div class="Check-box">
											<label class="checkstyle">
												<input type="checkbox" name="sim_tmp[]" value="9" class="linear_tmp tmp_type" <?php echo (in_array(9, $sim_temp)) ? 'checked' : ''; ?> />
												<span class="checkmark"></span>
												<span class="text-R">Multiple Template</span>
											</label>
											<span class="smallT">
												<p class="question-type">(MTF, Sequence, Sorting, MMCQ, MCQ, Drag and Drop, Swiping)</p>
												<p class="question-type">(Single Feedback)</p>
											</span>
										</div>
										<?php endif; ?>
									</div>
								</div>
								<?php endif; ?>
								<?php if (in_array(10, $sim_temp) || in_array(11, $sim_temp)): ?>
								<div class="linersim linersimBotom">
									<div class="user_text editSim">Branching Video Simulation</div>				
									<div class="edit-sim-radio Linerm">
										<?php if (in_array(10, $sim_temp)): ?>
										<div class="Check-box">
											<label class="checkstyle">
												<input type="checkbox" name="sim_tmp[]" value="10" class="branch_tmp tmp_type" <?php echo (in_array(10, $sim_temp)) ? 'checked' : ''; ?> />
												<span class="checkmark"></span>
												<span class="text-R">Classic Template</span>
											</label>
											<span class="smallT">
												<p class="question-type">(MCQ, Drag and Drop,Swiping)</p>
												<p class="question-type">(Multiple Feedback)</p>
											</span>
										</div>
										<?php endif; ?>
										<?php if (in_array(11, $sim_temp)): ?>
										<div class="Check-box">
											<label class="checkstyle">
												<input type="checkbox" name="sim_tmp[]" value="11" class="branch_tmp tmp_type" <?php echo (in_array(11, $sim_temp)) ? 'checked' : ''; ?> />
												<span class="checkmark"></span>
												<span class="text-R">Multiple Template</span>
											</label>
											<span class="smallT">
												<p class="question-type">(MTF, Sorting, MMCQ, MCQ, Drag and Drop, Swiping)</p>
												<p class="question-type">(Single Feedback)</p>
											</span>
										</div>
										<?php endif; ?>
									</div>
								</div>
								<?php endif; ?>
							</div>
							<ul class="list-inline pull-right">
								<li><button type="button" class="btn btn-primary prev-step">Back</button></li>
								<li><button type="button" class="btn btn-primary next-step">Continue</button></li>
							</ul>
						</div>
						<div id="PaymentDetail" class="tab-pane fade">
							<div class="usermanage-form">
								<div class="row">
									<div class="col-sm-6">
										<div class="form-group">
											<input type="text" class="form-control" name="amount" id="amount" placeholder="Amount Per Year" value="<?php echo $data['amount'] ?>" />
										</div>
									</div>
									<div class="col-sm-6">
										<div class="form-group form-select">
											<select class="form-control selection" name="amount_type" id="amount_type">
											<option value="" selected="" disabled="disabled">Select Currency Type</option>
											</select>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-6">
										<div class="form-group">
											<label for="">Payment Received</label>
											<div class="checkbox radiobtn formcheckbox1">
												<label><input type="radio" class="checkbox1" name="amount_rec" value="1"  <?php echo ( ! empty($data['pay_received'])) ? 'checked="checked"' : ''; ?>><span>Yes</span></label>
											</div>
											<div class="checkbox radiobtn formcheckbox1">
												<label><input type="radio" class="checkbox1" name="amount_rec" value="0" <?php echo ( empty($data['pay_received'])) ? 'checked="checked"' : ''; ?>><span>No</span></label>
											</div>
										</div>
									</div>
								</div>
							</div>
							<ul class="list-inline pull-right">
								<li><button type="button" class="btn btn-primary prev-step">Back</button></li>
								<li><input type="hidden" name="update_sub_client_account" value="1" />
									<button type="submit" name="update_account" id="update_account" class="btn btn-primary">Update Account</button>
								</li>
							</ul>
						</div>
					</div>
                </div>
			</form>
	  	</div>
	</div>
</div>

<script type="text/javascript" src="content/js/inputmask/jquery.inputmask.js"></script>
<script type="text/javascript" src="content/js/bootstrap-datetimepicker.js" charset="UTF-8"></script>
<script type="text/javascript">
$(document).ready(function() {
	var amt_code = $('#currency_code').val();
	$('#amount_type').find("option:eq(0)").html("Please wait..");
	$.ajax({
		type: "Get",
		url: "includes/currencies.json",
		dataType: "json",
		success: function(data) {
			$('#amount_type').find("option:eq(0)").html("Select Currency Type");
			$.each(data, function (key, val) {				
				if (amt_code != '' && amt_code == val.cc) {
					var selected = 'selected';
				}
				else { var selected = ''; }
				var amt_type = val.name +' ('+ val.symbol +')';
				var option = '<option value="'+ amt_type +'"  '+ selected +'>'+ amt_type +'</option>';
				$('#amount_type').append(option);
			});
		},
		error: function(){ swal({text: 'Oops. something went wrong. currencies not load.', buttons: false, icon: "error", timer: 2000}); }
	});

	jQuery('body').on('change', '#amount_type', function() {
		var code = $(this).find(':selected').attr('data-currency-code');
		$('#currency_code').val(code);
	});
	
	$("#tax_id").inputmask("**-*********", {"clearIncomplete": true});

	$('.form_datetime').datetimepicker({
		weekStart: 1,
		todayBtn:  0,
		autoclose: 1,
		todayHighlight: 1,
		startView: 2,
		forceParse: 0,
		minView: 2,
		showMeridian: 1,
		clearBtn: 1
	});

	$('.multiselect-ui').multiselect({
		includeSelectAllOption: true,
		filterPlaceholder: 'Search & Select',
		enableCaseInsensitiveFiltering : true,
		enableFiltering: true,
		maxHeight: 400,
		buttonWidth: '280px',
	});
	
	$('.multiselect-ui').multiselect('select', <?php echo json_encode(explode(',', $data['role'])) ?>);

	$('[data-toggle="tooltip"]').tooltip();
	
	$("#domain_name").on('change', function(){
		var val = $(this).val();
		if ( ! (/^[a-zA-Z0-9_-]{3,25}$/i.test(val))) {
			$(this).val($('#old_domain').val());
			swal({text:'Please give valid Subdomain name, no spaces and special characters allowed.!', buttons: false, icon: "warning", timer: 3000});
		}
		return false;
	});
	
	$('#upload').click(function() {
		$(this).attr('disabled', 'disabled').html('<i class="fa fa-refresh fa-spin fa-fw"></i><span class="sr-only">Loading...</span> Please wait....');
		var file_data = $('#logo').prop('files')[0];
		var form_data = new FormData();
		form_data.append('logo', file_data);
		form_data.append('add_client_logo', true);
		$.ajax({
			url: "includes/upload.php",
			type: "POST",
			data: form_data,
			contentType: false,
			cache: false,
			processData:false,
			success: function(resdata) {
				var res = $.parseJSON(resdata);
				if (res.success == true) {
					var viewHtml = '<a href="img/logo/'+ res.logo +'" target="_blank" title="View Logo"><i class="fa fa-eye"></i></a>';
					$('#splashImg').show().html(viewHtml);
					$('#ImgDelete').show();
					$('.delete_logo').attr('data-img-name', res.logo);
					$('#logo_name').val(res.logo);
					$('#upload').html('Upload').removeAttr('disabled', 'disabled');
					swal({text: res.msg, buttons: false, timer: 1000});
				}
				else if (res.success == false) {
					$('#logo_name').val('');
					$('#splashImg').hide('slow');
					$('#upload').html('Upload').removeAttr('disabled', 'disabled');
					swal({text: res.msg, buttons: false, icon: "warning", timer: 1000});
				}
			},error: function() {
				$('#splashImg').hide('slow');
				swal({text: 'Oops. something went wrong please try again.', buttons: false, icon: "error", timer: 2000});
			}
		});
	});
	
	$('.delete_logo').click(function() {
		var file_name   = $(this).attr("data-img-name");
		var dataString	= 'delete='+ true + '&client_logo_file='+ file_name;
		if (file_name) {
			swal({
				title: "Are you sure?",
				text: "Delete this Logo.",
				icon: "warning",
				buttons: [true, 'Delete'],
				dangerMode: true, }).then((willDelete) => { if (willDelete) {
					$('#splashImg').show().html(' <img src="scenario/img/loader.gif"> Please wait....');
					$.ajax({
						url: "includes/delete-file.php",
						type: "POST",
						data: dataString,
						cache: false,
						success: function(resdata) {
							var res = $.parseJSON(resdata);
							if (res.success == false) {
								swal({text: res.msg, buttons: false, icon: "error", timer: 1000});
								$('#splashImg').hide('slow');
							}
							else if (res.success == true) {
								$('#splashImg, #ImgDelete').hide('slow');
								$('#logo_name').val('');
								swal({text: res.msg, buttons: false, timer: 1000});
							}
						},error: function() {
							swal({text: 'Oops. something went wrong please try again.', buttons: false, icon: "error", timer: 2000});
						}
					});
				} else { swal("Your file is safe.!", { buttons: false, timer: 1000, }); }
			});
		}
	});
	
	var cid 	= $('.country').data('cid');
	var sid 	= $('.country').data('sid');
	var cityid	= $('.country').data('city');
	var state	= $('.country').data('starget');
	var city	= $('.country').data('ctarget');
	
	$('.country').find("option:eq(0)").html("Please wait..");
	
	if (cid != '' && sid != '' && city != '') {
		$.getJSON('includes/main-process.php?type=getCountry', function (data) {
			$('.country').find("option:eq(0)").html("Select Country");
			$.each(data, function (key, val) {
				if (cid != '' && cid == val.id){
					var selected = 'selected';
				}
				else {
					var selected = '';
				}
				var option = '<option value="'+val.id+'" '+ selected +'>'+val.name+'</option>';
				$('.country').append(option);
			})
		});
		
		$.getJSON('includes/main-process.php?type=getState&countryId='+ cid, function (data) {
			$("#"+ state).find("option:eq(0)").html("Select State");
			$.each(data, function (key, val) {
				if (sid != '' && sid == val.id){
					var selected = 'selected';
				}
				else {
					var selected = '';
				}
				var option = '<option value="'+val.id+'" '+ selected +'>'+val.name+'</option>';
				$("#"+ state).append(option);
			})
		});
		
		$.getJSON('includes/main-process.php?type=getCity&stateId='+sid, function (data) {
			$("#"+ city).find("option:eq(0)").html("Select City");
			$.each(data, function (key, val) {
				if (cityid != '' && cityid == val.id){
					var selected = 'selected';
				}
				else {
					var selected = '';
				}
				var option = '<option value="'+val.id+'" '+ selected +'>'+val.name+'</option>';
				$("#"+ city).append(option);
			})
		});
	}
	else {
		$('.country').find("option:eq(0)").html("Please wait..");
		$.getJSON('includes/main-process.php?type=getCountry', function (data) {
			$('.country').find("option:eq(0)").html("Select Country");
			$.each(data, function (key, val) {
				var option = '<option value="'+val.id+'">'+val.name+'</option>';
				$('.country').append(option);
			})
		});
	}

	jQuery('body').on('change', '.country', function() {
		var cid		= $(this).val();
		var state	= $(this).data('starget');
		var city	= $(this).data('ctarget');
		if (cid != '') {
			$("#"+ state +" option:gt(0)").remove();
			$("#"+ city +" option:gt(0)").remove();
			$("#"+ state).find("option:eq(0)").html("Please wait..");
			$.getJSON('includes/main-process.php?type=getState&countryId='+cid, function (data) {
				$("#"+ state).find("option:eq(0)").html("Select State");
				$.each(data, function (key, val) {
					var option = '<option value="'+val.id+'">'+val.name+'</option>';
					$("#"+ state).append(option);
				})
			});
		} else { $("#"+ state +" option:gt(0)").remove(); $("#"+ city +" option:gt(0)").remove(); }
	});

	jQuery('body').on('change', '.state', function() {
		var sid	 = $(this).val();
		var city = $(this).data('ctarget');
		if (sid != '') {
			$("#"+ city +" option:gt(0)").remove();
			$("#"+ city).find("option:eq(0)").html("Please wait..");
			$.getJSON('includes/main-process.php?type=getCity&stateId='+sid, function (data) {
				$("#"+ city).find("option:eq(0)").html("Select City");
				$.each(data, function (key, val) {
					var option = '<option value="'+val.id+'">'+val.name+'</option>';
					$("#"+ city).append(option);
				})
			});
		}
		else { $("#"+ city +" option:gt(0)").remove(); }
	});

	$(".phonecode").each(function( index ) {
		var target = $(this).data('target');
		var pcode  = $(this).data('pcode');
		$('.phonecode').find("option:eq(0)").html("Please wait..");
		$.getJSON('includes/main-process.php?type=getCountry', function (data) {
			$('.phonecode').find("option:eq(0)").html("Select Phone Code");
			$.each(data, function (key, val) {
				if (pcode != '' && pcode == val.phonecode){
					var selected = 'selected';
				}
				else { var selected = ''; }
				var option = '<option value="'+val.phonecode+'" '+ selected +'>'+val.name +' (+'+ val.phonecode  +')</option>';
				$('#'+ target).append(option);
			});
		});
	});
	
	//Wizard
    $('a[data-toggle="tab"]').on('show.bs.tab', function(e) {
        var $target = $(e.target);
		if ($target.parent().hasClass('disabled')) {
            return false;
        }
    });

    $(".next-step").click(function (e) {
        var $active = $('.nav-tabs li.active');
        $active.next().removeClass('disabled');
        nextTab($active);
    });

    $(".prev-step").click(function (e) {
        var $active = $('.nav-tabs li.active');
        prevTab($active);
    });

	function nextTab(elem) {
		$(elem).next().find('a[data-toggle="tab"]').click();
	}
	
	function prevTab(elem) {
		$(elem).prev().find('a[data-toggle="tab"]').click();
	}
});
</script>
<?php 
require_once 'includes/footer.php';
