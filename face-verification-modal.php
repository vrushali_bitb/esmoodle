<?php 
session_start();
if (isset($_SESSION['username'])):
	$user 	= $_SESSION['username'];
	$role 	= $_SESSION['role'];
	$userid	= $_SESSION['userId'];
	$domain = (isset($_SESSION['domain_name'])) ? $_SESSION['domain_name'] : $_SERVER['HTTP_HOST'];
else:
    header('location: index.php');
endif;
require_once 'config/db.class.php';
$db		   = new DBConnection();
$user_data = ( ! empty($userid)) ? $db->getUsers(md5($userid)) : [];
$path	   = 'scenario/upload/'. $domain .'/';
$profile   = ( ! empty($user_data['img']) && file_exists($db->rootPath() . 'img/profile/' . $user_data['img'])) ? $user_data['img'] : '';
?>
<link href="videojs/node_modules/video.js/dist/video-js.min.css" rel="stylesheet">
<link href="videojs/dist/css/videojs.record.css" rel="stylesheet">

<script src="videojs/node_modules/video.js/dist/video.min.js"></script>
<script src="videojs/node_modules/recordrtc/RecordRTC.js"></script>
<script src="videojs/node_modules/webrtc-adapter/out/adapter.js"></script>
<script src="videojs/dist/videojs.record.js"></script>
<script src="videojs/examples/browser-workarounds.js"></script>
<style>
  /* change player background color */
  #myImage { background-color: #9ab87a; }
  .bar-success {  background-color: #5cb85c; }
  .bar-info { background-color: #5bc0de; }
  .bar-warning { background-color: #f0ad4e; }
  .bar-danger { background-color: #d9534f; }
  .progress { width: 55% !important; margin-top: -24px !important; height: 25px !important; margin-left: 265px !important;}
  .progress-bar { font-size: 22px !important; line-height: 20px !important;}
</style>
<div id="load_popup_modal_contant" class="temppoupMOdel" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close stopRecord" data-dismiss="modal">&times;</button>
                <h3 class="modal-title">Face Matching Progress</h3>
                <div class="progress progress-striped">
                    <div class="progress-bar progress-bar-success"></div>
                </div>
            </div>
            <div class="modal-body">
				<video id="myImage" playsinline class="video-js vjs-fantasy"></video>
				<input type="hidden" name="vfile_name" id="vfile_name" value="<?php echo $db->getBaseUrl($path . 'proctor/'. $userid .'.png'); ?>" />
				<input type="hidden" name="profile" id="profile" value="<?php echo ( ! empty($profile)) ? $db->getBaseUrl('img/profile/'. $profile) : ''; ?>" />
                <button type="button" class="btn1 submitbtn1 verify disabled" disabled="disabled">Verify</button>
                <button type="button" class="btn1 submitbtn1 stopRecord" data-dismiss="modal">Cancel</button>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript" src="content/js/sweetalert.min.js"></script>
<script type="text/javascript" src="content/js/proctor-api.js?v=<?php echo time() ?>"></script>
<script type="text/javascript" src="content/js/verification.js?v=<?php echo time() ?>"></script>