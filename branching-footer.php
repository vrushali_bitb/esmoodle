<div id="load_popup_modal_show" class="modal fade" tabindex="-1"></div>

<script src="content/js/jquery-ui-1.12.1/jquery-ui.js"></script>
<script src="content/js/jquery.fontselect.js"></script>
<script src="content/js/competency-validate.js"></script>
<script type="text/javascript">
$(function(){
	$(".draggable").draggable();
});

var assets_path = '<?php echo $path; ?>';

$('body').on('click', '.view_assets', function(e){
    e.preventDefault();
    var to = $(this).attr('data-src');
    $.fancybox.open({
        type: 'iframe',
        src: to,
        toolbar  : false,
        smallBtn : true,
		closeExisting: false,
		scrolling: 'no',
        iframe : {
            preload : true,
            scrolling: 'no',
			css:{
					width:'100%',
					height:'100%'
				}
        }
    });
});

$(".inputtextWrap").hover(function() {
	$(this).attr('title', $(this).val());
}, function() {
	$(this).css('cursor','auto');
});

/* Set Default Font Type */
$('.cl_ftype').click(function () {
	$('#font_type').trigger('setFont', '');
});

function isNumberKey(evt) {
	var charCode = (evt.which) ? evt.which : evt.keyCode
	return !(charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57));
}

function clearData(id){
	$('#'+ id).val('');
}
$('.update_next').on('click', function(){
	$('#submit_type').val(1);
});

$('.update_close').on('click', function(){
	$('#submit_type').val(2);
});

$('.type-color-on-page').spectrum({
	type: "component",
	togglePaletteOnly: "true",
	hideAfterPaletteSelect: "true",
	showInput: "true",
	showInitial: "true",
});

$('#font_type').fontselect({
	placeholder: 'Pick a font from the list',
	searchable: false,
	systemFonts: [],
	googleFonts: [<?php echo $db::googleFont; ?>]
});

/* Drag and Drop Selected Option */
$('.dragoption').on('click', function(){
	$('#seleted_drag_option').val($(this).val());
});

/* Backgrounds */
$.ajax({
	type: 'GET',
	url: 'includes/ajax.php',
	data: 'getCharBackground=true&type=1',
	success:function(resdata) {
		$.LoadingOverlay("hide");
		var res = $.parseJSON(resdata);
		if (res.success == true) {
			var bg_selected = '';
			$.each(res.data, function (key, val) {
				if (val.img_name == $('#get_bg_img').val()){
					bg_selected = 'checked="checked"';
				}
				else { bg_selected = ''; }
				if(key == 0){
					var option = '<div class="bacground_banner">\
				<div class="Radio-box MCQRadio"><label class="radiostyle"><input type="radio" checked="checked" name="sim_background_img" value="'+val.img_name+'" '+bg_selected+'><span class="radiomark"></span></label><a data-fancybox="gallery" href="img/char_bg/'+val.img_name+'"><img src="img/char_bg/'+val.img_name+'"></a>';
				}else{
					var option = '<div class="bacground_banner">\
				<div class="Radio-box MCQRadio"><label class="radiostyle"><input type="radio" name="sim_background_img" value="'+val.img_name+'" '+bg_selected+'><span class="radiomark"></span></label><a data-fancybox="gallery" href="img/char_bg/'+val.img_name+'"><img src="img/char_bg/'+val.img_name+'"></a>';
				}				
				<?php if (isset($role) && $role == 'admin'): ?>
				option += '<a href="javascript:void(0);" data-upload-id="'+val.upload_id+'" data-img-name="'+val.img_name+'" class="delete_char_bg_img" title="Delete File"><i class="fa fa-times" aria-hidden="true"></i></a></div>';
				<?php endif; ?>
				$('.BGIMG').append(option);
			});
		}
		else { swal({text: 'Background image not loading please try again later.', buttons: false, icon: "error", timer: 2000 }); }
	},error: function() {
		$.LoadingOverlay("hide");
		swal({text: 'Oops. something went wrong please try again.', buttons: false, icon: "error", timer: 2000 });
	}
});

/* One Character */
$.ajax({
	type: 'GET',
	url: 'includes/ajax.php',
	data: 'getCharBackground=true&type=2,3',
	success:function(resdata) {
		$.LoadingOverlay("hide");
		var res = $.parseJSON(resdata);
		if (res.success == true) {
			var one_char_selected = '';
			$.each(res.data, function (key, val) {
				if (val.img_name == $('#get_sim_char_img').val()){
					one_char_selected = 'checked="checked"';
				}
				else { one_char_selected = ''; }
				var option = '<div class="bacground_banner">\
				<div class="Radio-box MCQRadio"><label class="radiostyle"><input type="radio" name="sim_char_img" class="1char" value="'+val.img_name+'" '+one_char_selected+'><span class="radiomark"></span></label><a data-fancybox="gallery1" href="img/char_bg/'+val.img_name+'"><img src="img/char_bg/'+val.img_name+'"></a>';
				<?php if (isset($role) && $role == 'admin'): ?>
				option += '<a href="javascript:void(0);" data-upload-id="'+val.upload_id+'" data-img-name="'+val.img_name+'" class="delete_char_bg_img" title="Delete File"><i class="fa fa-times" aria-hidden="true"></i></a></div>';
				<?php endif; ?>
				$('.OneCharIMG').append(option);
			});
		}
		else { swal({text: 'Background image not loading please try again later.', buttons: false, icon: "error", timer: 2000 }); }
	},error: function() {
		$.LoadingOverlay("hide");
		swal({text: 'Oops. something went wrong please try again.', buttons: false, icon: "error", timer: 2000 });
	}
});

/* Two Characters */
$.ajax({
	type: 'GET',
	url: 'includes/ajax.php',
	data: 'getCharBackground=true&type=2,3',
	success:function(resdata) {
		$.LoadingOverlay("hide");
		var res = $.parseJSON(resdata);
		if (res.success == true) {
			var sim_char_left_img  = $('#sim_char_left_img').val();
			var sim_char_right_img = $('#sim_char_right_img').val();
			var char_left  = '';
			var char_right = '';
			$.each(res.data, function (key, val) {
				if (sim_char_left_img != '' && sim_char_left_img == val.img_name) {
					char_left = 'checked="checked"';
					get_value = 'value="'+val.img_name+'|L"'; 
				} else { char_left = ''; get_value = 'value="'+val.img_name+'"';  }
				
				if (sim_char_right_img != '' && sim_char_right_img == val.img_name) {
					char_right = 'checked="checked"';
					get_value = 'value="'+val.img_name+'|R"'; 
				} else { char_right = ''; get_value = 'value="'+val.img_name+'"';  }
				
				var option = '<div class="bacground_banner">\
				<div class="Check-box"><label class="checkstyle"><input type="checkbox" name="sim_two_char_img[]" class="2char" '+get_value+' '+char_left+' '+char_right+'>';
				if (char_left != ''){
					option += '<span class="leftIMG" title="Left side character">L</span>';
				}
				else if (char_right != ''){
					option += '<span class="RightIMG" title="Right side character">R</span>';
				}
				option += '<span class="checkmark"></span></label><a data-fancybox="gallery2" href="img/char_bg/'+val.img_name+'"><img src="img/char_bg/'+val.img_name+'"></a>';
				<?php if (isset($role) && $role == 'admin'): ?>
				option += '<a href="javascript:void(0);" data-upload-id="'+val.upload_id+'" data-img-name="'+val.img_name+'" class="delete_char_bg_img" title="Delete File"><i class="fa fa-times" aria-hidden="true"></i></a>';
				<?php endif; ?>
				$('.TwoCharIMG').append(option);
			});
		}
		else { swal({text: 'Background image not loading please try again later.', buttons: false, icon: "error", timer: 2000 }); }
	},error: function() {
		$.LoadingOverlay("hide");
		swal({text: 'Oops. something went wrong please try again.', buttons: false, icon: "error", timer: 2000 });
	}
});

/* Add New Question */
$('.AddIcon').on('click', function(e){
	e.preventDefault();
	$.LoadingOverlay("show");
	var simID = $('#sim_id').val();
	if (simID != '') {
		var dataString = 'add_Question='+ true +'&sim_id='+ simID;
		$.ajax({
			type: 'POST',
			url: 'includes/process.php',
			data: dataString,
			cache: false,
			success: function(resdata) {
				var res = $.parseJSON(resdata);
				if (res.success == true) {
					$.LoadingOverlay("hide");
					setTimeout(function() { window.location.reload(); }, time);
				}
				else if (res.success == false) {
					$.LoadingOverlay("hide");
					swal({text: res.msg, buttons: false, icon: "warning", timer: 2000});
				}
			}, error: function() {
				swal({text: 'Oops. something went wrong please try again.', buttons: false, icon: "error", timer: 2000 });
			}
		});
	}
});

/* Delete Question */
$('.DelIcon').on('click', function(e){
	e.preventDefault();
	var ques = $('#ques_id').val();
	if (ques) {
		$.LoadingOverlay("show");
		var dataString = 'del_Question='+ true +'&ques_id='+ ques;
		$.ajax({
			type: 'POST',
			url: 'includes/process.php',
			data: dataString,
			cache: false,
			success: function(resdata) {
				var res = $.parseJSON(resdata);
				if (res.success == true) {
					$.LoadingOverlay("hide");
					setTimeout(function() { window.location.reload(); }, time);
				}
				else if (res.success == false) {
					$.LoadingOverlay("hide");
					swal({text: res.msg, buttons: false, icon: "warning", timer: 2000});
				}
			}, error: function() {
				swal({text: 'Oops. something went wrong please try again.', buttons: false, icon: "error", timer: 2000 });
			}
		});
	} else {
		swal({text: 'Please select at-least one Question', buttons: false, icon: "warning", timer: 2000 });
	}
});

/* Clone Question */
$('.DuPIcon').on('click', function(e){
	e.preventDefault();
	var ques = $('#ques_id').val();
	if (ques) {
		$.LoadingOverlay("show");
		var dataString = 'clone_Question='+ true +'&ques_id='+ ques;
		$.ajax({
			type: 'POST',
			url: 'includes/process.php',
			data: dataString,
			cache: false,
			success: function(resdata) {
				var res = $.parseJSON(resdata);
				if (res.success == true) {
					$.LoadingOverlay("hide");
					setTimeout(function() { window.location.reload(); }, time);
				}
				else if (res.success == false) {
					$.LoadingOverlay("hide");
					swal({text: res.msg, buttons: false, icon: "warning", timer: 2000});
				}
			}, error: function() {
				swal({text: 'Oops. something went wrong please try again.', buttons: false, icon: "error", timer: 2000 });
			}
		});
	} else {
		swal({text: 'Please select one Question', buttons: false, icon: "warning", timer: 2000 });
	}
});

/* Template Type */
$('.qtype').on('click', function(){
	var type = $(this).data('qtype');
	var get_type = $('#get_question_type') .val();
	/* MTF */
	if (type == 1){
		$('.shuffle_off').attr("required", true);
	}
	else {
		$('.shuffle_off').removeAttr("required", true);
	}

	if (get_type == '' && type != '' && type == 1) {
		$('#question_type').val(type);
	}
	else if (get_type == '' && type != '' && type == 2) {
		$('#question_type').val(type);
	}
	else if (get_type == '' && type != '' && type == 3) {
		$('#question_type').val(type);
	}
	else if (get_type == '' && type != '' && type == 8) {
		$('#question_type').val(type);
	}
	else if (get_type == '' && type != '' && type == 4) {
		$('#question_type').val(type);
	}
	else if (get_type == '' && type != '' && type == 5) {
		$('#question_type').val(type);
	}
	else if (get_type == '' && type != '' && type == 7) {
		$('#question_type').val(type);
	}
});

$('.mtf_shuffle').on('click', function(){
	var mtf_check = $(this).val();
	if (mtf_check == 2) {
		$('.shuffle_off').removeAttr("required", true);
		$('.shuffle_on').attr("required", true);
	}
	else {
		$('.shuffle_on').removeAttr("required", true);
		$('.shuffle_off').attr("required", true);
	}
});

//------Competency-Score-------
$('#ques_val_1,#sq_ques_val_1,#sort_ques_val_1,#mcq_ques_val_1,#mmcq_ques_val_1,#swipq_ques_val_1').on('change', function(){
	var score = parseInt($(this).val());
	var maxallow = parseInt($('#comp_val_1').val());
	if (maxallow < score) {
		$(this).val('');
		swal('Maximum '+maxallow+' score', { buttons: false, timer: 1000 });
	}
});

$('#ques_val_2,#sq_ques_val_2,#sort_ques_val_2,#mcq_ques_val_2,#mmcq_ques_val_2,#swipq_ques_val_2').on('change', function(){
	var score = parseInt($(this).val());
	var maxallow = parseInt($('#comp_val_2').val());
	if (score > maxallow) {
		$(this).val('');
		swal('Maximum '+maxallow+' score', { buttons: false, timer: 1000 });
	}
});

$('#ques_val_3,#sq_ques_val_3,#sort_ques_val_3,#mcq_ques_val_3,#mmcq_ques_val_3,#swipq_ques_val_3').on('change', function(){
	var score = parseInt($(this).val());
	var maxallow = parseInt($('#comp_val_3').val());
	if (score > maxallow) {
		$(this).val('');
		swal('Maximum '+maxallow+' score', { buttons: false, timer: 1000 });
	}
});

$('#ques_val_4,#sq_ques_val_4,#sort_ques_val_4,#mcq_ques_val_4,#mmcq_ques_val_4,#swipq_ques_val_4').on('change', function(){
	var score = parseInt($(this).val());
	var maxallow = parseInt($('#comp_val_4').val());
	if (score > maxallow) {
		$(this).val('');
		swal('Maximum '+maxallow+' score', { buttons: false, timer: 1000 });
	}
});

$('#ques_val_5,#sq_ques_va_5,#sort_ques_va_5,#mcq_ques_val_5,#mmcq_ques_val_5,#swipq_ques_val_5').on('change', function(){
	var score = parseInt($(this).val());
	var maxallow = parseInt($('#comp_val_5').val());
	if (score > maxallow) {
		$(this).val('');
		swal('Maximum '+maxallow+' score', { buttons: false, timer: 1000 });
	}
});

$('#ques_val_6,#sq_ques_va_6,#sort_ques_va_6,#mcq_ques_val_6,#mmcq_ques_val_6,#swipq_ques_val_6').on('change', function(){
	var score = parseInt($(this).val());
	var maxallow = parseInt($('#comp_val_6').val());
	if (score > maxallow) {
		$(this).val('');
		swal('Maximum '+maxallow+' score', { buttons: false, timer: 1000 });
	}
});

//----------Audio-Upload-All-Template--------------------
$('body').on('change', '.uploadAudioFile', function(){
	$.LoadingOverlay("show");
	var input_id	= $(this).data('id');
	var assets_id	= $(this).data('assets');
	var file_data	= $(this).prop('files')[0];
	var form_data	= new FormData();
	form_data.append('file', file_data);
	form_data.append('qAudiofiles', '1');
	$.ajax({
		url: "includes/upload.php",
		type: "POST",
		data: form_data,
		contentType: false,
		cache: false,
		processData: false,
		success: function(resdata){
			var res = $.parseJSON(resdata);
			if (res.success == true){
				$(input_id).val(res.file_name);						
				$('.'+ assets_id).addClass("disabled").attr('disabled', true);
				$('.'+ assets_id + '_container').show();
				$('.'+ assets_id + '_data a').text(res.file_name);
				$('.'+ assets_id + '_data a').attr('data-src', assets_path + res.file_name);
				$('.'+ assets_id + '_container a.delete_assets').attr('data-assets', res.file_name);
				$('.'+ assets_id + '_container a.delete_assets').attr('data-assets-id', assets_id);
				$('.'+ assets_id + '_container a.delete_assets').attr('data-input-id', input_id);
				$('.'+ assets_id + '_container a.delete_assets').attr('data-path', res.path);
				swal({text: res.msg, buttons: false, timer: 1000});
			}
			else if (res.success == false){
				$(input_id).val('');
				swal({text: res.msg, buttons: false, icon: "warning", timer: 1000});
			}
			$.LoadingOverlay("hide");
		},error: function(){
			$.LoadingOverlay("hide");
			swal("Error", 'Oops, something went wrong. Please try again later.?', "error");
		}
	});
});

//-----------Video-Upload-All-Template-------------------
$('body').on('change', '.uploadVideoFile', function() {
	$.LoadingOverlay("show");
	var input_id	= $(this).data('id');
	var assets_id	= $(this).data('assets');
	var file_data	= $(this).prop('files')[0];
	var form_data	= new FormData();
	form_data.append('file', file_data);
	form_data.append('qVideofiles', '1');
	$.ajax({
		url: "includes/upload.php",
		type: "POST",
		data: form_data,
		contentType: false,
		cache: false,
		processData: false,
		success: function(resdata) {
			var res = $.parseJSON(resdata);
			if (res.success == true) {
				$(input_id).val(res.file_name);
				swal({text: res.msg, buttons: false, timer: 1000});
				$('.'+ assets_id).addClass("disabled").attr('disabled', true);
				$('.'+ assets_id + '_container').show();
				$('.'+ assets_id + '_data a').text(res.file_name);
				$('.'+ assets_id + '_data a').attr('data-src', assets_path + res.file_name);
				$('.'+ assets_id + '_container a.delete_assets').attr('data-assets', res.file_name);
				$('.'+ assets_id + '_container a.delete_assets').attr('data-assets-id', assets_id);
				$('.'+ assets_id + '_container a.delete_assets').attr('data-input-id', input_id);
				$('.'+ assets_id + '_container a.delete_assets').attr('data-path', res.path);
			}
			else if (res.success == false) {
				$(input_id).val('');
				swal({text: res.msg, buttons: false, icon: "warning", timer: 1000});
			}
			$.LoadingOverlay("hide");
		},error: function() {
			$.LoadingOverlay("hide");
			swal("Error", 'Oops, something went wrong. Please try again later.?', "error");
		}
	});
});

//-----------Image-Upload-All-Template-------------------
$('body').on('change', '.uploadImgFile', function() {
	$.LoadingOverlay("show");
	var cur 		= $(this);
	var input_id	= $(this).data('id');
	var assets_id	= $(this).data('assets');
	var file_data	= $(this).prop('files')[0];
	var form_data	= new FormData();
	form_data.append('file', file_data);
	form_data.append('qImgfiles', '1');
	$.ajax({
		url: "includes/upload.php",
		type: "POST",
		data: form_data,
		contentType: false,
		cache: false,
		processData: false,
		success: function(resdata) {
			var res = $.parseJSON(resdata);
			if (res.success == true) {
				$(input_id).val(res.file_name);
				swal({text: res.msg, buttons: false, timer: 1000});
				cur.closest('div').addClass("disabled");
				$('.'+ assets_id).addClass("disabled").attr('disabled', true);
				$('.'+ assets_id + '_container').show();
				$('.'+ assets_id + '_data a').text(res.file_name);
				$('.'+ assets_id + '_data a').attr('data-src', assets_path + res.file_name);
				$('.'+ assets_id + '_container a.delete_assets').attr('data-assets', res.file_name);
				$('.'+ assets_id + '_container a.delete_assets').attr('data-assets-id', assets_id);
				$('.'+ assets_id + '_container a.delete_assets').attr('data-input-id', input_id);
				$('.'+ assets_id + '_container a.delete_assets').attr('data-path', res.path);
			}
			else if (res.success == false) {
				$(input_id).val('');
				swal({text: res.msg, buttons: false, icon: "warning", timer: 1000});
			}
			$.LoadingOverlay("hide");
		},error: function() {
			$.LoadingOverlay("hide");
			swal("Error", 'Oops, something went wrong. Please try again later.?', "error");
		}
	});
});

//-----------Doc-Upload-All-Template---------------------
$('body').on('change', '.uploadDocFile', function() {
	$.LoadingOverlay("show");
	var cur 		= $(this);
	var input_id	= $(this).data('id');
	var assets_id	= $(this).data('assets');
	var file_data	= $(this).prop('files')[0];
	var form_data	= new FormData();
	form_data.append('file', file_data);
	form_data.append('qDocfiles', '1');
	$.ajax({
		url: "includes/upload.php",
		type: "POST",
		data: form_data,
		contentType: false,
		cache: false,
		processData: false,
		success: function(resdata) {
			var res = $.parseJSON(resdata);
			if (res.success == true) {
				$(input_id).val(res.file_name);
				swal({text: res.msg, buttons: false, timer: 1000});
				cur.closest('div').addClass("disabled");
				$('.'+ assets_id).addClass("disabled").attr('disabled', true);
				$('.'+ assets_id + '_container').show();
				$('.'+ assets_id + '_data a').text(res.file_name);
				$('.'+ assets_id + '_data a').attr('data-src', assets_path + res.file_name);
				$('.'+ assets_id + '_container a.delete_assets').attr('data-assets', res.file_name);
				$('.'+ assets_id + '_container a.delete_assets').attr('data-assets-id', assets_id);
				$('.'+ assets_id + '_container a.delete_assets').attr('data-input-id', input_id);
				$('.'+ assets_id + '_container a.delete_assets').attr('data-path', res.path);
			}
			else if (res.success == false) {
				$(input_id).val('');
				swal({text: res.msg, buttons: false, icon: "warning", timer: 1000});
			}
			$.LoadingOverlay("hide");
		},error: function() {
			$.LoadingOverlay("hide");
			swal("Error", 'Oops, something went wrong. Please try again later.?', "error");
		}
	});
});

//-----------Image-Upload-Sorting-Template---------------
$('body').on('change', '.uploadImgFileSort', function() {
	$.LoadingOverlay("show");
	var cur 		= $(this);
	var input_id	= $(this).data('id');
	var assets_id	= $(this).data('assets');
	var file_data	= $(this).prop('files')[0];
	var form_data	= new FormData();
	form_data.append('file', file_data);
	form_data.append('qImgfiles', '1');
	$.ajax({
		url: "includes/upload.php",
		type: "POST",
		data: form_data,
		contentType: false,
		cache: false,
		processData: false,
		success: function(resdata) {
			var res = $.parseJSON(resdata);
			if (res.success == true) {
				$(input_id).val(res.file_name);
				$('.sortq_item_assets'+ assets_id).addClass("disabled").attr('disabled', true);
				cur.closest('div').addClass("disabled");
				$('.sortq_items_assets_container_'+ assets_id).show();
				$('.sortq_items_assets_data_'+ assets_id + ' a.view_assets').text(res.file_name);
				$('.sortq_items_assets_data_'+ assets_id + ' a.view_assets').attr('data-src', assets_path + res.file_name);
				$('.sortq_items_assets_container_'+ assets_id +' a.delete_assets').attr('data-assets', res.file_name);
				$('.sortq_items_assets_container_'+ assets_id +' a.delete_assets').attr('data-assets-id', 'sortq_item_assets'+ assets_id);
				$('.sortq_items_assets_container_'+ assets_id +' a.delete_assets').attr('data-input-id', input_id);
				$('.sortq_items_assets_container_'+ assets_id +' a.delete_assets').attr('data-path', res.path);
				swal({text: res.msg, buttons: false, timer: 1000});
			}
			else if (res.success == false){
				$(input_id).val('');
				swal({text: res.msg, buttons: false, icon: "warning", timer: 1000});
			}
			$.LoadingOverlay("hide");
		},error: function() {
			$.LoadingOverlay("hide");
			swal("Error", 'Oops, something went wrong. Please try again later.?', "error");
		}
	});
});

//-----------Image-Upload-Swip-Template-----------------
$('body').on('change', '.uploadImgFileSwip', function() {
	$.LoadingOverlay("show");
	var cur 		= $(this);
	var input_id	= $(this).data('id');
	var assets_id	= $(this).data('assets');
	var file_data	= $(this).prop('files')[0];
	var form_data	= new FormData();
	form_data.append('file', file_data);
	form_data.append('qImgfiles', '1');
	$.ajax({
		url: "includes/upload.php",
		type: "POST",
		data: form_data,
		contentType: false,
		cache: false,
		processData: false,
		success: function(resdata) {
			var res = $.parseJSON(resdata);
			if (res.success == true) {
				$(input_id).val(res.file_name);
				cur.closest('div').addClass("disabled");
				$('.swipq_item_assets'+ assets_id).addClass("disabled").attr('disabled', true);
				$('.swipq_items_assets_container_'+ assets_id).show();
				$('.swipq_items_assets_data_'+ assets_id + ' a.view_assets').text(res.file_name);
				$('.swipq_items_assets_data_'+ assets_id + ' a.view_assets').attr('data-src', assets_path + res.file_name);
				$('.swipq_items_assets_container_'+ assets_id +' a.delete_assets').attr('data-assets', res.file_name);
				$('.swipq_items_assets_container_'+ assets_id +' a.delete_assets').attr('data-assets-id', 'swipq_item_assets'+ assets_id);
				$('.swipq_items_assets_container_'+ assets_id +' a.delete_assets').attr('data-input-id', input_id);
				$('.swipq_items_assets_container_'+ assets_id +' a.delete_assets').attr('data-path', res.path);
				swal({text: res.msg, buttons: false, timer: 1000});
			}
			else if (res.success == false){
				$(input_id).val('');
				swal({text: res.msg, buttons: false, icon: "warning", timer: 1000});
			}
			$.LoadingOverlay("hide");
		},error: function() {
			$.LoadingOverlay("hide");
			swal("Error", 'Oops, something went wrong. Please try again later.?', "error");
		}
	});
});

//-----------Video-Upload-Swip-Template-----------------
$('body').on('change', '.uploadVideoFileSwip', function() {
	$.LoadingOverlay("show");
	var cur 		= $(this);
	var input_id	= $(this).data('id');
	var assets_id	= $(this).data('assets');
	var file_data	= $(this).prop('files')[0];
	var form_data	= new FormData();
	form_data.append('file', file_data);
	form_data.append('qVideofiles', '1');
	$.ajax({
		url: "includes/upload.php",
		type: "POST",
		data: form_data,
		contentType: false,
		cache: false,
		processData: false,
		success: function(resdata) {
			var res = $.parseJSON(resdata);
			if (res.success == true) {
				$(input_id).val(res.file_name);
				cur.closest('div').addClass("disabled");
				$('.swipq_item_assets'+ assets_id).addClass("disabled").attr('disabled', true);
				$('.swipq_items_assets_container_'+ assets_id).show();
				$('.swipq_items_assets_data_'+ assets_id).text(res.file_name);
				$('.swipq_items_assets_container_'+ assets_id +' a').attr('data-assets', res.file_name);
				$('.swipq_items_assets_container_'+ assets_id +' a').attr('data-assets-id', 'swipq_item_assets'+ assets_id);
				$('.swipq_items_assets_container_'+ assets_id +' a').attr('data-input-id', input_id);
				$('.swipq_items_assets_container_'+ assets_id +' a').attr('data-path', res.path);
				swal({text: res.msg, buttons: false, timer: 1000});
			}
			else if (res.success == false){
				$(input_id).val('');
				swal({text: res.msg, buttons: false, icon: "warning", timer: 1000});
			}
			$.LoadingOverlay("hide");
		},error: function() {
			$.LoadingOverlay("hide");
			swal("Error", 'Oops, something went wrong. Please try again later.?', "error");
		}
	});
});

//-----------Image-Upload-Drag-Drop-Template-------------

	//--------------DROP-TARGET-----------------
$('body').on('change', '.uploadImgFileDDq', function() {
	$.LoadingOverlay("show");
	var cur 		= $(this);
	var input_id	= $(this).data('id');
	var assets_id	= $(this).data('assets');
	var file_data	= $(this).prop('files')[0];
	var form_data	= new FormData();
	form_data.append('file', file_data);
	form_data.append('qImgfiles', '1');
	$.ajax({
		url: "includes/upload.php",
		type: "POST",
		data: form_data,
		contentType: false,
		cache: false,
		processData: false,
		success: function(resdata) {
			var res = $.parseJSON(resdata);
			if (res.success == true) {
				$(input_id).val(res.file_name);
				cur.closest('div').addClass("disabled");
				$('.ddq_item_assets'+ assets_id).addClass("disabled").attr('disabled', true);
				$('.ddq_items_assets_container_'+ assets_id).show();
				$('.ddq_items_assets_data_'+ assets_id + ' a.view_assets').text(res.file_name);
				$('.ddq_items_assets_data_'+ assets_id + ' a.view_assets').attr('data-src', assets_path + res.file_name);
				$('.ddq_items_assets_container_'+ assets_id +' a.delete_assets').attr('data-assets', res.file_name);
				$('.ddq_items_assets_container_'+ assets_id +' a.delete_assets').attr('data-assets-id', 'ddq_item_assets'+ assets_id);
				$('.ddq_items_assets_container_'+ assets_id +' a.delete_assets').attr('data-input-id', input_id);
				$('.ddq_items_assets_container_'+ assets_id +' a.delete_assets').attr('data-path', res.path);
				swal({text: res.msg, buttons: false, timer: 1000});
			}
			else if (res.success == false){
				$(input_id).val('');
				swal({text: res.msg, buttons: false, icon: "warning", timer: 1000});
			}
			$.LoadingOverlay("hide");
		},error: function() {
			$.LoadingOverlay("hide");
			swal("Error", 'Oops, something went wrong. Please try again later.?', "error");
		}
	});
});

	//-------------- DRAG-ITEM-----------------
$('body').on('change', '.uploadImgFileDragDDq', function() {
	$.LoadingOverlay("show");
	var cur 		= $(this);
	var input_id	= $(this).data('id');
	var assets_id	= $(this).data('assets');
	var file_data	= $(this).prop('files')[0];
	var form_data	= new FormData();
	form_data.append('file', file_data);
	form_data.append('qImgfiles', '1');
	$.ajax({
		url: "includes/upload.php",
		type: "POST",
		data: form_data,
		contentType: false,
		cache: false,
		processData: false,
		success: function(resdata) {
			var res = $.parseJSON(resdata);
			if (res.success == true) {
				$(input_id).val(res.file_name);
				cur.closest('div').addClass("disabled");
				$('.ddq_drag_item_assets'+ assets_id).addClass("disabled").attr('disabled', true);
				$('.ddq_drag_items_assets_container_'+ assets_id).show();
				$('.ddq_drag_items_assets_data_'+ assets_id + ' a.view_assets').text(res.file_name);
				$('.ddq_drag_items_assets_data_'+ assets_id + ' a.view_assets').attr('data-src', assets_path + res.file_name);
				$('.ddq_drag_items_assets_container_'+ assets_id +' a.delete_assets').attr('data-assets', res.file_name);
				$('.ddq_drag_items_assets_container_'+ assets_id +' a.delete_assets').attr('data-assets-id', 'ddq_drag_item_assets'+ assets_id);
				$('.ddq_drag_items_assets_container_'+ assets_id +' a.delete_assets').attr('data-input-id', input_id);
				$('.ddq_drag_items_assets_container_'+ assets_id +' a.delete_assets').attr('data-path', res.path);
				swal({text: res.msg, buttons: false, timer: 1000});
			}
			else if (res.success == false){
				$(input_id).val('');
				swal({text: res.msg, buttons: false, icon: "warning", timer: 1000});
			}
			$.LoadingOverlay("hide");
		},error: function() {
			$.LoadingOverlay("hide");
			swal("Error", 'Oops, something went wrong. Please try again later.?', "error");
		}
	});
});

	//--------------DROP-TARGET-2----------------
$('body').on('change', '.uploadImgFileDDq2', function() {
	$.LoadingOverlay("show");
	var cur 		= $(this);
	var input_id	= $(this).data('id');
	var assets_id	= $(this).data('assets');
	var file_data	= $(this).prop('files')[0];
	var form_data	= new FormData();
	form_data.append('file', file_data);
	form_data.append('qImgfiles', '1');
	$.ajax({
		url: "includes/upload.php",
		type: "POST",
		data: form_data,
		contentType: false,
		cache: false,
		processData: false,
		success: function(resdata) {
			var res = $.parseJSON(resdata);
			if (res.success == true) {
				$(input_id).val(res.file_name);
				cur.closest('div').addClass("disabled");
				$('.ddq2_item_assets'+ assets_id).addClass("disabled").attr('disabled', true);
				$('.ddq2_items_assets_container_'+ assets_id).show();
				$('.ddq2_items_assets_data_'+ assets_id + ' a.view_assets').text(res.file_name);
				$('.ddq2_items_assets_data_'+ assets_id + ' a.view_assets').attr('data-src', assets_path + res.file_name);
				$('.ddq2_items_assets_container_'+ assets_id +' a.delete_assets').attr('data-assets', res.file_name);
				$('.ddq2_items_assets_container_'+ assets_id +' a.delete_assets').attr('data-assets-id', 'ddq2_item_assets'+ assets_id);
				$('.ddq2_items_assets_container_'+ assets_id +' a.delete_assets').attr('data-input-id', input_id);
				$('.ddq2_items_assets_container_'+ assets_id +' a.delete_assets').attr('data-path', res.path);
				swal({text: res.msg, buttons: false, timer: 1000});
			}
			else if (res.success == false){
				$(input_id).val('');
				swal({text: res.msg, buttons: false, icon: "warning", timer: 1000});
			}
			$.LoadingOverlay("hide");
		},error: function() {
			$.LoadingOverlay("hide");
			swal("Error", 'Oops, something went wrong. Please try again later.?', "error");
		}
	});
});

	//-------------- DRAG-ITEM-2----------------
$('body').on('change', '.uploadImgFileDragDDq2', function() {
	$.LoadingOverlay("show");
	var cur 		= $(this);
	var input_id	= $(this).data('id');
	var assets_id	= $(this).data('assets');
	var file_data	= $(this).prop('files')[0];
	var form_data	= new FormData();
	form_data.append('file', file_data);
	form_data.append('qImgfiles', '1');
	$.ajax({
		url: "includes/upload.php",
		type: "POST",
		data: form_data,
		contentType: false,
		cache: false,
		processData: false,
		success: function(resdata) {
			var res = $.parseJSON(resdata);
			if (res.success == true) {
				$(input_id).val(res.file_name);
				cur.closest('div').addClass("disabled");
				$('.ddq2_drag_item_assets'+ assets_id).addClass("disabled").attr('disabled', true);
				$('.ddq2_drag_items_assets_container_'+ assets_id).show();
				$('.ddq2_drag_items_assets_data_'+ assets_id + ' a.view_assets').text(res.file_name);
				$('.ddq2_drag_items_assets_data_'+ assets_id + ' a.view_assets').attr('data-src', assets_path + res.file_name);
				$('.ddq2_drag_items_assets_container_'+ assets_id +' a.delete_assets').attr('data-assets', res.file_name);
				$('.ddq2_drag_items_assets_container_'+ assets_id +' a.delete_assets').attr('data-assets-id', 'ddq2_drag_item_assets'+ assets_id);
				$('.ddq2_drag_items_assets_container_'+ assets_id +' a.delete_assets').attr('data-input-id', input_id);
				$('.ddq2_drag_items_assets_container_'+ assets_id +' a.delete_assets').attr('data-path', res.path);
				swal({text: res.msg, buttons: false, timer: 1000});
			}
			else if (res.success == false){
				$(input_id).val('');
				swal({text: res.msg, buttons: false, icon: "warning", timer: 1000});
			}
			$.LoadingOverlay("hide");
		},error: function() {
			$.LoadingOverlay("hide");
			swal("Error", 'Oops, something went wrong. Please try again later.?', "error");
		}
	});
});

//-----------Record Audio-All-Template------------------
$('body').on('click', '.rec-audio', function() {
	var input_id  = $(this).attr('data-input-id');
	var assets_id = $(this).attr('data-assets');
	if (input_id != '') {
		$.LoadingOverlay("show");
		var $modal = $('#load_popup_modal_show');
		$modal.load('rec-audio-modal.php', {'data-id': input_id, 'assets-id': assets_id}, function(res) {
			if (res != '') {
				$.LoadingOverlay("hide");
				$modal.modal('show', {backdrop: 'static', keyboard: false});
			}
			else {
				$.LoadingOverlay("hide");
				swal({text: 'Oops, something went wrong. Please try again later', buttons: false, icon: "error", timer: 1000});
			}
		});
	}
});

//-----------Record Video-All-Template------------------
$('body').on('click', '.rec-video', function() {
	var input_id  = $(this).attr('data-input-id');
	var assets_id = $(this).attr('data-assets');
	if (input_id != '') {
		$.LoadingOverlay("show");
		var $modal = $('#load_popup_modal_show');
		$modal.load('rec-video-modal.php', {'data-id': input_id, 'assets-id': assets_id}, function(res) {
			if (res != '') {
				$.LoadingOverlay("hide");
				$modal.modal('show', {backdrop: 'static', keyboard: false});
			}
			else {
				$.LoadingOverlay("hide");
				swal({text: 'Oops, something went wrong. Please try again later', buttons: false, icon: "error", timer: 1000});
			}
		});
	}
});

//-----------Record Screen-All-Template-----------------
$('body').on('click', '.rec-screen', function() {
	var input_id  = $(this).attr('data-input-id');
	var assets_id = $(this).attr('data-assets');
	if (input_id != '') {
		$.LoadingOverlay("show");
		var $modal = $('#load_popup_modal_show');
		$modal.load('rec-screen-modal.php', {'data-id': input_id, 'assets-id': assets_id}, function(res) {
			if (res != '') {
				$.LoadingOverlay("hide");
				$modal.modal('show', {backdrop: 'static', keyboard: false});
			}
			else {
				$.LoadingOverlay("hide");
				swal({text: 'Oops, something went wrong. Please try again later', buttons: false, icon: "error", timer: 1000});
			}
		});
	}
});

//--------------Delete-Assets-All-Templates------------
$('body').on('click', '.delete_assets', function() {
	var cur   		= $(this);
	var file_name	= cur.attr('data-assets');
	var assetsId	= cur.attr('data-assets-id');
	var inputId		= cur.attr('data-input-id');
	var path		= cur.attr('data-path');
	var divId		= cur.closest('div').attr('class');
	var dataString  = 'delete_assets='+ true +'&assets_path='+ path +'&file='+ file_name;
	if (file_name){
		swal({
			title: "Are you sure?",
			text: "Delete this Assets.",
			icon: "warning",
			buttons: [true, 'Delete'],
			dangerMode: true, }).then((willDelete) => { if (willDelete) {
				$.LoadingOverlay("show");
				$.ajax({
					url: "includes/delete-file.php",
					type: "POST",
					data: dataString,
					cache: false,
					success: function(resdata){
						var res = $.parseJSON(resdata);
						if (res.success == false) {
							$.LoadingOverlay("hide");
							swal({text: res.msg, buttons: false, icon: "error", timer: 2000});
						}
						else if (res.success == true) {
							$(inputId).val('');
							$('.'+ assetsId).removeClass("disabled").removeAttr('disabled', true);
							$('.'+ divId).hide();
							$.LoadingOverlay("hide");
							swal({text: res.msg, buttons: [false, 'OK'], closeOnClickOutside: false, closeOnEsc: false});
						}
					},error: function() {
						swal({text: 'Oops. something went wrong please try again.', buttons: false, icon: "error", timer: 1000});
					}
				});
			} else { swal({text: 'Your file is safe', buttons: false, timer: 1000}); }
		});
	}
});

var x = <?php echo ( ! empty($i)) ? $i : 1; ?>;
var maxField = 6;
$(".plus_comp").click(function() {
	if (x < maxField) {
		x++;
		$(".plus_comp_option").append('<div class="add_comp_option linaer">\
		<div class="form-group"><input type="text" class="form-control control1" placeholder="Competency Name" name="competency_name_'+x+'" autocomplete="off" required="required"></div>\
		<div class="form-group labelbox"><label>Competency Score</label><input type="text" class="form-control question-box" name="competency_score_'+x+'" onkeypress="return isNumberKey(event);" autocomplete="off" required="required"></div>\
		<div class="form-group labelbox"><label>Weightage</label><input type="text" class="form-control question-box" name="weightage_'+x+'" onkeypress="return isNumberKey(event);" autocomplete="off" required="required"></div><div class="remove minusComp"><img src="img/icons/minus.png" title="Remove"></div></div>');
		var getMax = $('.add_comp_option').length;
		if (getMax >= maxField) {
			$('.plus_comp').addClass('disabled');
		}
		else { $('.plus_comp').removeClass('disabled'); }
	}
	else {
		$('.plus_comp').addClass('disabled');
	}
});

$('.plus_comp_option').on('click', '.remove', function(e) {
	e.preventDefault();
	$(this).parent().remove();
	x--;
	$('.plus_comp').removeClass('disabled');
});

$('.suffleCheck').click(function() {
	if ($(".suffleCheck").is(":checked") == true) { 
		$('.AnsHeading.Mchoice').text('CORRECT CHOICE'); 
		$('.tooltiptop .tooltiptext').text('Shuffle ON'); 
		$('.tooltiptop .tooltiptext').css('display', 'block'); 
		$('.matchingchice').css('display', 'none');
		$('.matchingchicesuffle').css('display', 'block');
		
	} else { 
		$('.AnsHeading.Mchoice').text('MATCH'); 
		$('.tooltiptop .tooltiptext').text('Shuffle OFF'); 
		$('.tooltiptop .tooltiptext').delay(2000).fadeOut(300);
		$('.matchingchicesuffle').css('display', 'none')
		$('.matchingchice').css('display', 'block') 
	} 
});

$('.unsuffleCheck').click(function() {
	if ($(".unsuffleCheck").is(":checked") == true) { 		
		$('.AnsHeading.Mchoice').text('MATCH'); 
		$('.tooltiptop .tooltiptext').text('Shuffle OFF'); 
		$('.tooltiptop .tooltiptext').delay(2000).fadeOut(300);
		$('.matchingchicesuffle').css('display', 'none')
		$('.matchingchice').css('display', 'block') 
		
	} else { 
		$('.AnsHeading.Mchoice').text('CORRECT CHOICE'); 
		$('.tooltiptop .tooltiptext').text('Shuffle ON'); 
		$('.tooltiptop .tooltiptext').css('display', 'block'); 
		$('.matchingchice').css('display', 'none');
		$('.matchingchicesuffle').css('display', 'block');
	} 
});

$(".suffleCheck").click(function(){
	$('.AnsHeading.Mchoice').addClass('ANSCOrrectcho');
});

$(".closerightmenuA").click(function(e) {
	e.preventDefault();
	$("#menu-toggle").removeClass("changebtn");
	$("#wrapper").removeClass("toggled");
	$(".match-Q.tab-pane.fade").removeClass("tabpadding150");
	$(".main_sim_scroll").removeClass("TogglewidthQ100");		
	$(".widthQ100").removeClass("TogglewidthQ100");		
	$(".modal.Ques-pop-style.in ").removeClass("TogglewidthQ100");		
});

$("#menu-toggle").click(function(e) {
	e.preventDefault();
	$("#menu-toggle").toggleClass("changebtn");
	$("#wrapper").toggleClass("toggled");
	$(".match-Q.tab-pane.fade").toggleClass("tabpadding150");
	$(".main_sim_scroll").toggleClass("TogglewidthQ100");		
	$(".widthQ100").toggleClass("TogglewidthQ100");		
	$(".modal.Ques-pop-style.in ").toggleClass("TogglewidthQ100");		
});

$(".closeleftmenuA").click(function(e) {
	e.preventDefault();
	$("#list-toggle").addClass("changebtnleft");
	$("#wrapper").addClass("toggledlist");
	$(".match-Q.tab-pane.fade").addClass("tabpadding150");
	$(".Q-left").addClass("left150");
	$(".widthQ100").addClass("TogglelistwidthQ100");
	$(".main_sim_scroll").addClass("TogglelistwidthQ100");	
	$(".modal.Ques-pop-style.in ").addClass("TogglelistwidthQ100");
    $(".list-icon").addClass("Labsolute");			
});

$("#list-toggle").click(function(e) {
	e.preventDefault();
	$("#list-toggle").toggleClass("changebtnleft");
	$("#wrapper").toggleClass("toggledlist");
	$(".match-Q.tab-pane.fade").toggleClass("tabpadding150");
	$(".Q-left").toggleClass("left150");
	$(".widthQ100").toggleClass("TogglelistwidthQ100");	
	$(".main_sim_scroll").toggleClass("TogglelistwidthQ100");		
	$(".modal.Ques-pop-style.in ").toggleClass("TogglelistwidthQ100");
    $(".list-icon").toggleClass("Labsolute");			
});

$(".matchingQ").click(function(e) {
	e.preventDefault();
	$("#menu-toggle").removeClass("changebtn");
	$("#wrapper").removeClass("toggled");
	$(".match-Q.tab-pane.fade").removeClass("tabpadding150");
	$(".main_sim_scroll").removeClass("TogglewidthQ100");		
	$(".widthQ100").removeClass("TogglewidthQ100");		
	$(".modal.Ques-pop-style.in ").removeClass("TogglewidthQ100");		
});

$(".SequanceQ").click(function(e) {
	e.preventDefault();
	$("#menu-toggle").removeClass("changebtn");
	$("#wrapper").removeClass("toggled");
	$(".match-Q.tab-pane.fade").removeClass("tabpadding150");
	$(".main_sim_scroll").removeClass("TogglewidthQ100");		
	$(".widthQ100").removeClass("TogglewidthQ100");		
	$(".modal.Ques-pop-style.in ").removeClass("TogglewidthQ100");		
});

$(".DregDropQ").click(function(e) {
	e.preventDefault();
	$("#menu-toggle").removeClass("changebtn");
	$("#wrapper").removeClass("toggled");
	$(".match-Q.tab-pane.fade").removeClass("tabpadding150");
	$(".main_sim_scroll").removeClass("TogglewidthQ100");		
	$(".widthQ100").removeClass("TogglewidthQ100");		
	$(".modal.Ques-pop-style.in ").removeClass("TogglewidthQ100");		
});

$(".VideoQ").click(function(e) {
	e.preventDefault();
	$("#menu-toggle").removeClass("changebtn");
	$("#wrapper").removeClass("toggled");
	$(".match-Q.tab-pane.fade").removeClass("tabpadding150");
	$(".main_sim_scroll").removeClass("TogglewidthQ100");		
	$(".widthQ100").removeClass("TogglewidthQ100");		
	$(".modal.Ques-pop-style.in ").removeClass("TogglewidthQ100");		
});

$(".MCQQ").click(function(e) {
	e.preventDefault();
	$("#menu-toggle").removeClass("changebtn");
	$("#wrapper").removeClass("toggled");
	$(".match-Q.tab-pane.fade").removeClass("tabpadding150");
	$(".main_sim_scroll").removeClass("TogglewidthQ100");		
	$(".widthQ100").removeClass("TogglewidthQ100");		
	$(".modal.Ques-pop-style.in").removeClass("TogglewidthQ100");		
});

$(".MMCQQ").click(function(e) {
	e.preventDefault();
	$("#menu-toggle").removeClass("changebtn");
	$("#wrapper").removeClass("toggled");
	$(".match-Q.tab-pane.fade").removeClass("tabpadding150");
	$(".main_sim_scroll").removeClass("TogglewidthQ100");		
	$(".widthQ100").removeClass("TogglewidthQ100");		
	$(".modal.Ques-pop-style.in").removeClass("TogglewidthQ100");		
});

$(".SwapingQ").click(function(e) {
	e.preventDefault();
	$("#menu-toggle").removeClass("changebtn");
	$("#wrapper").removeClass("toggled");
	$(".match-Q.tab-pane.fade").removeClass("tabpadding150");
	$(".main_sim_scroll").removeClass("TogglewidthQ100");		
	$(".widthQ100").removeClass("TogglewidthQ100");		
	$(".modal.Ques-pop-style.in").removeClass("TogglewidthQ100");		
});

$(document).ready(function(){
	//----------------MTF----------------//
	$(".AddAnsICON").click(function(){
		$(".MatchOption-box1:last").after('<div class="MatchOption-box1">\
			<div class="col-sm-6">\
				<div class="matchbox">\
					<div class="form-group"><input type="text" name="mtf_choice[]" class="form-control inputtextWrap shuffle_off mtf" placeholder="Click here to enter choice" required="required"></div>\
					<div class="form-group QusScore"><input type="text" name="mtf_choice_order[]" class="form-control shuffle_off mtf" onkeypress="return isNumberKey(event);" required="required"></div>\
				</div>\
			</div>\
			<div class="col-sm-6">\
				<div class="matchbox">\
					<div class="form-group QusScore"><input type="text" name="mtf_match_order[]" class="form-control shuffle_off mtf" onkeypress="return isNumberKey(event);" required="required"></div>\
					<div class="form-group"><input type="text" name="mtf_match[]" class="form-control inputtextWrap shuffle_off mtf" placeholder="Click here to enter text" required="required"></div>\
				</div>\
			</div>\
			<div class="AddAns-Option RemoveAnsICON"><img class="img-fluid" src="img/list/delete_field.svg"></div></div>');
	});
	
	$('.ApendOption-box').on('click', '.AddAns-Option.RemoveAnsICON', function(e) {
		e.preventDefault();
		$(this).parent().remove();
	});
	
	//-----MTF-Shuffle-ON------------//
	$(".AddAnsICON1").click(function(){
		$(".MatchOption-box2:last").after('<div class="MatchOption-box2 MSuffle">\
		<div class="col-sm-6">\
			<div class="matchbox">\
				<div class="form-group">\
					<input type="text" name="mtf_choice2[]" class="form-control inputtextWrap shuffle_on mtf" placeholder="Click here to enter choice" required="required" />\
				</div>\
			</div>\
		</div>\
		<div class="col-sm-6">\
			<div class="matchbox">\
				<div class="form-group">\
					<input type="text" name="mtf_match2[]" class="form-control inputtextWrap shuffle_on mtf" placeholder="Click here to enter text" required="required" />\
				</div>\
			</div>\
		</div>\
		<div class="AddAns-Option RemoveAnsICON1"><img class="img-fluid" src="img/list/delete_field.svg"></div></div>');
	});
	
	$('.ApendOption-box').on('click', '.AddAns-Option.RemoveAnsICON1', function(e){
		e.preventDefault();
		$(this).parent().remove();
	});
	
	//------DNDQuestion1-----------//
	var dd2i = $('.DNDOption-box2').length;
	$(".DNDAddAnsICON1").click(function(){
		dd2i++;
		$(".DNDOption-box2:last").after('<div class="DNDOption-box2">\
			<div class="col-sm-12">\
				<div class="Radio-box">\
					<label class="radiostyle">\
						<input type="radio" name="ddq_true_option" value="'+dd2i+'" />\
						<span class="radiomark"></span>\
				   </label>\
				   <div class="col-sm-12">\
						<div class="choicebox">\
							<div class="form-group">\
								<ul class="QueBoxIcon">\
									<li>\
										<div class="tooltip uploadicon ddq2_drag_item_assets'+dd2i+'">\
											<label for="file-input-ddq2-drag-item-addImg'+dd2i+'">\
												<img class="img-fluid" src="img/list/image.svg" />\
												<span class="tooltiptext">Add Image</span>\
											</label>\
											<input id="file-input-ddq2-drag-item-addImg'+dd2i+'" data-id="#ddq2_drag_item_img'+dd2i+'" data-assets="'+dd2i+'" class="uploadImgFileDragDDq2 ddq2_drag_item_assets'+dd2i+'" type="file" name="ddq2_drag_Img'+dd2i+'" />\
											<input type="hidden" name="ddq2_drag_item_img[]" id="ddq2_drag_item_img'+dd2i+'" />\
										</div>\
									</li>\
								</ul>\
								<input type="text" name="ddq2_drag[]" class="form-control inputtextWrap" placeholder="Drag text goes here" />\
								<div class="ddq2_drag_items_assets_container_'+dd2i+'" id="ddq2_drag_items_assets" style="display:none;">\
									<div class="assets_data ddq2_drag_items_assets_data_'+dd2i+'"><a class="view_assets" data-src="" href="javascript:;"></a></div>\
									<a href="javascript:void(0);" data-assets="" data-assets-id="ddq2_drag_item_assets'+dd2i+'" data-input-id="#ddq2_drag_item_img'+dd2i+'" data-path="<?php echo $uploadpath ?>" class="delete_assets" title="Delete"><i class="fa fa-times" aria-hidden="true"></i></a>\
								</div>\
							</div>\
						</div>\
					</div>\
				   <div class="AddAns-Option RemoveAnsICON2"><img class="img-fluid" src="img/list/delete_field.svg"></div>\
				</div>\
			</div>\
		</div>');
	});
	
	$('.ApendOption-box').on('click', '.AddAns-Option.RemoveAnsICON2', function(){
		$(this).parent().parent().parent().remove();
		dd2i--;
	});
	
	//-------DNDQuestion---------------//
	var ddi = $('.DNDOption-box1').length;
	$(".DNDAddAnsICON").click(function(){
		ddi++;
		$(".DNDOption-box1:last").after('<div class="DNDOption-box1">\
			<div class="col-sm-6">\
				<div class="matchbox">\
					<span class="QuesNo">'+ddi+'</span>\
					<div class="form-group">\
						<ul class="QueBoxIcon">\
							<li>\
								<div class="tooltip uploadicon ddq_item_assets'+ddi+'">\
									<label for="file-input-ddq-item-addImg'+ddi+'">\
										<img class="img-fluid" src="img/list/image.svg" />\
										<span class="tooltiptext">Add Image</span>\
									</label>\
									<input id="file-input-ddq-item-addImg'+ddi+'" data-id="#ddq_item_img'+ddi+'" data-assets="'+ddi+'" class="uploadImgFileDDq ddq_item_assets'+ddi+'" type="file" name="ddq_item_Img'+ddi+'" />\
									<input type="hidden" name="ddq_item_img[]" id="ddq_item_img'+ddi+'" />\
								</div>\
							</li>\
						</ul>\
						<input type="text" name="ddq_drop[]" class="form-control inputtextWrap ddq" placeholder="Drop Target text goes here" />\
						<div class="ddq_items_assets_container_'+ddi+'" id="ddq_items_assets" style="display:none;">\
							<div class="assets_data ddq_items_assets_data_'+ddi+'"><a class="view_assets" data-src="" href="javascript:;"></a></div>\
							<a href="javascript:void(0);" data-assets="" data-assets-id="ddq_item_assets'+ddi+'" data-input-id="#ddq_item_img'+ddi+'" data-path="<?php echo $uploadpath ?>" class="delete_assets" title="Delete"><i class="fa fa-times" aria-hidden="true"></i></a>\
						</div>\
					</div>\
				</div>\
			</div>\
			<div class="col-sm-6">\
				<div class="matchbox">\
					<div class="form-group">\
						<ul class="QueBoxIcon">\
							<li>\
								<div class="tooltip uploadicon ddq_drag_item_assets'+ddi+'">\
									<label for="file-input-ddq-drag-item-addImg'+ddi+'">\
										<img class="img-fluid" src="img/list/image.svg" />\
										<span class="tooltiptext">Add Image</span>\
									</label>\
									<input id="file-input-ddq-drag-item-addImg'+ddi+'" data-id="#ddq_drag_item_img'+ddi+'" data-assets="'+ddi+'" class="uploadImgFileDragDDq ddq_drag_item_assets'+ddi+'" type="file" name="ddq_drag_Img'+ddi+'" />\
									<input type="hidden" name="ddq_drag_item_img[]" id="ddq_drag_item_img'+ddi+'" />\
								</div>\
							</li>\
						</ul>\
						<input type="text" name="ddq_drag[]" class="form-control inputtextWrap" placeholder="Drag text goes here" />\
						<div class="ddq_drag_items_assets_container_'+ddi+'" id="ddq_drag_items_assets" style="display:none;">\
							<div class="assets_data ddq_drag_items_assets_data_'+ddi+'"><a class="view_assets" data-src="" href="javascript:;"></a></div>\
							<a href="javascript:void(0);" data-assets="" data-assets-id="ddq_drag_item_assets'+ddi+'" data-input-id="#ddq_drag_item_img'+ddi+'" data-path="<?php echo $uploadpath ?>" class="delete_assets" title="Delete"><i class="fa fa-times" aria-hidden="true"></i></a>\
						</div>\
					</div>\
				</div>\
			</div><div class="AddAns-Option RemoveAnsICON"><img class="img-fluid" src="img/list/delete_field.svg"></div></div>');
	});
	
	$('.ApendOption-box').on('click', '.AddAns-Option.RemoveAnsICON', function(){
		$(this).parent().remove();
		ddi--;
	});
	
	//---SequanceQuestion-------//	
	$(".SeqAdd").click(function(){
		$(".Option-box:last").after('<div class="Option-box">\
		<div class="form-group formwidth">\
			<input type="text" name="sq_ans[]" class="form-control inputtextWrap sq" placeholder="Option text goes here" />\
		</div>\
		<div class="AddAns-Option"><img class="img-fluid SeqRemove" src="img/list/delete_field.svg"></div></div>');
	});
	
	$('.SeqApendOption').on('click', '.SeqRemove', function() {
		$(this).parent().parent().remove();
	});
	
	//----------Shorting-Item----------------//
	var sortingI = $('.ApendShortOption-box').length;
	$(".ShortAddICON").click(function(){
		dragitem = sortingI;
		sortingI++;
		$(".ApendShortOption-box:last").after('<div class="ApendShortOption-box">\
		<div class="shortbox2">\
			<span class="QuesNo">'+sortingI+'</span>\
			<div class="form-group">\
				<input type="text" name="sortq_sorting_items[]" id="sortq_sorting_items" class="form-control inputtextWrap sortq" placeholder="Sorting text goes here" />\
				<ul class="QueBoxIcon">\
					<li>\
						<div class="tooltip uploadicon sortq_item_assets'+sortingI+'">\
							<label for="file-input-sortq-item-addImg'+sortingI+'">\
								<img class="img-fluid" src="img/list/image.svg" />\
								<span class="tooltiptext">Add Image</span>\
							</label>\
							<input id="file-input-sortq-item-addImg'+sortingI+'" data-id="#sortq_item_img'+sortingI+'" data-assets="'+sortingI+'" class="uploadImgFileSort sortq_item_assets'+sortingI+'" type="file" name="sortq_item_Img'+sortingI+'" />\
							<input type="hidden" name="sortq_item_img[]" id="sortq_item_img'+sortingI+'" />\
						</div>\
					</li>\
			   </ul>\
			   <div class="sortq_items_assets_container_'+sortingI+'" id="sortq_items_assets" style="display:none;">\
			   		<div class="assets_data sortq_items_assets_data_'+sortingI+'"><a class="view_assets" data-src="" href="javascript:;"></a></div>\
					<a href="javascript:void(0);" data-assets="" data-assets-id="sortq_item_assets'+sortingI+'" data-input-id="#sortq_item_img'+sortingI+'" data-path="<?php echo $uploadpath ?>" class="delete_assets" title="Delete"><i class="fa fa-times" aria-hidden="true"></i></a>\
				</div>\
			</div>\
			<div class="AddAns-Option"><img class="img-fluid ShortdelICON" src="img/list/delete_field.svg" /></div>\
		</div>\
		<div class="SA-choicebox ShortOptions_'+sortingI+'">\
			<h3 class="AnsHeading dragH">Add Correct Drag Option</h3>\
			<div class="choicebox ShortAC">\
				<div class="form-group">\
					<input type="text" name="sortq_drag_items['+dragitem+'][option][]" id="sortq_drag_items" class="form-control inputtextWrap sortq" placeholder="Drag text goes here" />\
				</div>\
				<div class="AddAns-Option"><img class="img-fluid ShortDragAddICON" data-sortq-option="'+sortingI+'" data-sortq-suboption="'+dragitem+'" src="img/list/add_field.svg"></div>\
			</div>\
		</div>\
	 </div>');
	});
	
	$('.ApendShortOption-banner').on('click', '.ShortdelICON', function(){
		$(this).parent().parent().parent().remove();
		sortingI--;
	});
	
	$('body').on('click', '.ShortDragAddICON', function() {
		var option_id = $(this).data('sortq-option');
		var sub_option_id = $(this).data('sortq-suboption');
		$('.ShortOptions_'+ option_id).append('<div class="choicebox ShortAC ShortAC_'+option_id+'">\
			<div class="form-group">\
				<input type="text" name="sortq_drag_items['+sub_option_id+'][option][]" id="sortq_drag_items" class="form-control inputtextWrap sortq" placeholder="Drag text goes here" />\
			</div>\
			<div class="AddAns-Option">\
				<img class="img-fluid ShortDragdelICON" src="img/list/delete_field.svg" />\
			</div>\
		</div>');
	});
	
	$('.ApendShortOption-banner').on('click', '.ShortDragdelICON', function(){
		$(this).parent().parent().remove();
	});
	
	//--------------MCQQuestion----------------//
	var mcqI = $('.MCQApendOption .Radio-box').length;
	$(".MCQAdd").click(function(){
		mcqI++;
		$(".Radio-box.MCQRadio:last").after('<div class="Radio-box MCQRadio">\
		<label class="radiostyle">\
			<input type="radio" name="mcq_true_option" value="'+mcqI+'" /><span class="radiomark"></span>\
		 </label>\
		<div class="col-sm-12 mcwidth">\
			<div class="choicebox">\
				<div class="form-group">\
				   <input type="text" name="mcq_option[]" id="mcq_option" class="form-control inputtextWrap mcq" placeholder="Option text goes here" />\
				</div>\
			</div>\
		</div>\
		<div class="AddAns-Option MCQRemove"><img class="img-fluid" src="img/list/delete_field.svg"></div></div>');
	});

	$('.MCQApendOption').on('click', '.AddAns-Option.MCQRemove', function(){
		$(this).parent().remove();
		mcqI--;
	});
	
	//--------------MCQQuestion----------------//
	var mmcqI = $('.MMCQApendOption .Check-box').length;
	$(".MMCQAdd").click(function(){
		mmcqI++;
		$(".Check-box.MMCQCheck:last").after('<div class="Check-box MMCQCheck">\
		<label class="checkstyle">\
			<input type="checkbox" name="mmcq_true_option[]" value="'+mmcqI+'" />\
			<span class="checkmark"></span>\
		</label>\
		<div class="col-sm-12 mcwidth">\
			<div class="choicebox">\
				<div class="form-group">\
					<input type="text" name="mmcq_option[]" id="mmcq_option" class="form-control inputtextWrap mmcq" placeholder="Option text goes here" />\
				</div>\
			</div>\
		</div>\
		<div class="AddAns-Option MCCQRemove"><img class="img-fluid" src="img/list/delete_field.svg"></div></div>');
	});

	$('.MMCQApendOption').on('click', '.MCCQRemove', function(){
		$(this).parent().remove();
		mmcqI--;
	});
	
	//-------------SWIPING ----------------//
	var swipI = $('.Swa-box').length;
	$(".SwaAdd").click(function(){
		swipI++;
		$(".Swa-box:last").after('<div class="Swa-box">\
		<div class="Radio-box">\
			<label class="radiostyle">\
				<input type="radio" name="swipq_true_option" value="'+swipI+'" /><span class="radiomark"></span></label>\
				<div class="col-sm-12 mcwidth">\
					<div class="choicebox">\
						<div class="form-group">\
							<input type="text" name="swipq_option[]" id="swipq_option" class="form-control QuesForm1 inputtextWrap swipq" placeholder="Click here to enter question" />\
							<ul class="QueBoxIcon">\
								<li>\
									<div class="tooltip uploadicon swipq_item_assets'+swipI+'">\
										<label for="file-input-swipq-item-addImg'+swipI+'">\
											<img class="img-fluid" src="img/list/image.svg">\
											<span class="tooltiptext">Add Image</span>\
										</label>\
										<input id="file-input-swipq-item-addImg'+swipI+'" data-id="#swipq_item_img'+swipI+'" data-assets="'+swipI+'" class="uploadImgFileSwip swipq_item_assets'+swipI+'" type="file" name="swipq_item_Img'+swipI+'"/>\
										<input type="hidden" name="swipq_item_img[]" id="swipq_item_img'+swipI+'"/>\
									</div>\
								</li>\
							</ul>\
							<div class="swipq_items_assets_container_'+swipI+'" id="swipq_items_assets" style="display:none">\
								<div class="assets_data swipq_items_assets_data_'+swipI+'"><a class="view_assets" data-src="" href="javascript:;"></a></div>\
								<a href="javascript:void(0);" data-assets="" data-assets-id="" data-input-id="" data-path="" class="delete_assets" title="Delete">\
								<i class="fa fa-times" aria-hidden="true"></i></a>\
							</div>\
						</div>\
					</div>\
				</div>\
				<div class="AddAns-Option"><img class="img-fluid SWARemove" src="img/list/delete_field.svg"></div>\
			</div></div>');
	});
	
	$('.SWApendOption').on('click', '.SWARemove', function(){
		$(this).parent().parent().remove();
		swipI--;
	});
	
	$('.Radio-box.Dragradio.tab1').click(function(){
		$('.Dragrtab1').css('display' , 'block');
		$('.Dragrtab2').css('display' , 'none');
	});

	$('.Radio-box.Dragradio.tab2').click(function(){
		$('.Dragrtab2').css('display' , 'block');
		$('.Dragrtab1').css('display' , 'none');
	});

	//--------------Delete-Options------------
	$('body').on('click', '.removeOption', function(e) {
		e.preventDefault();
		var ans_id	= $(this).attr('data-remove-answer-id');
		var curr	= $(this);
		if (ans_id) {
			var dataString = 'delete_answer='+ true +'&ans_id='+ ans_id;
			swal({
				title: "Are you sure?",
				text:  "Delete this Option",
				icon: "warning",
				buttons: [true, 'Delete'],
				dangerMode: true, }).then((willDelete) => { if (willDelete) {
					$.LoadingOverlay("show");
					$.ajax({
						url: "includes/process.php",
						type: "POST",
						data: dataString,
						cache: false,
						success: function(resdata) {
							var res = $.parseJSON(resdata);
							$.LoadingOverlay("hide");
							if (res.success == true) {
								swal(res.msg, { buttons: false, timer: 2000 });
								curr.parent().remove();
								x--;
							}
							else if (res.success == false) {
								swal(res.msg, { buttons: false, timer: 2000 });
							}
						},error: function() {
							$.LoadingOverlay("hide");
							swal("Error", 'Oops, something went wrong. Please try again later.?', "error");
						}
					});
				}
			});
		}
	});

	//--------------Delete-SORTING-ITEM------------
	$('body').on('click', '.removeSortingOption', function(e) {
		e.preventDefault();
		var ans_id	= $(this).attr('data-remove-answer-id');
		var curr	= $(this);
		if (ans_id) {
			var dataString = 'delete_sorting_answer='+ true +'&ans_id='+ ans_id;
			swal({
				title: "Are you sure?",
				text:  "Delete this Option",
				icon: "warning",
				buttons: [true, 'Delete'],
				dangerMode: true, }).then((willDelete) => { if (willDelete) {
					$.LoadingOverlay("show");
					$.ajax({
						url: "includes/process.php",
						type: "POST",
						data: dataString,
						cache: false,
						success: function(resdata) {
							var res = $.parseJSON(resdata);
							$.LoadingOverlay("hide");
							if (res.success == true) {
								swal(res.msg, { buttons: false, timer: 2000 });
								curr.parent().parent().remove();
							}
							else if (res.success == false) {
								swal(res.msg, { buttons: false, timer: 2000 });
							}
						},error: function() {
							$.LoadingOverlay("hide");
							swal("Error", 'Oops, something went wrong. Please try again later.?', "error");
						}
					});
				}
			});
		}
	});

	//----------Delete-SORTING-DRAG-OPTION------------
	$('body').on('click', '.removeSortingSubOption', function(e) {
		e.preventDefault();
		var ans_id	= $(this).attr('data-remove-answer-id');
		var curr	= $(this);
		if (ans_id) {
			var dataString = 'delete_sorting_sub_answer='+ true +'&ans_id='+ ans_id;
			swal({
				title: "Are you sure?",
				text:  "Delete this Option",
				icon: "warning",
				buttons: [true, 'Delete'],
				dangerMode: true, }).then((willDelete) => { if (willDelete) {
					$.LoadingOverlay("show");
					$.ajax({
						url: "includes/process.php",
						type: "POST",
						data: dataString,
						cache: false,
						success: function(resdata) {
							var res = $.parseJSON(resdata);
							$.LoadingOverlay("hide");
							if (res.success == true) {
								swal(res.msg, { buttons: false, timer: 2000 });
								curr.parent().parent().remove();
							}
							else if (res.success == false) {
								swal(res.msg, { buttons: false, timer: 2000 });
							}
						},error: function() {
							$.LoadingOverlay("hide");
							swal("Error", 'Oops, something went wrong. Please try again later.?', "error");
						}
					});
				}
			});
		}
	});
});

$('#upload').click(function() {
	$(this).attr('disabled', 'disabled').html('<i class="fa fa-refresh fa-spin fa-fw"></i><span class="sr-only">Loading...</span> Please wait....');
	var file_data = $('#file').prop('files')[0];
	var form_data = new FormData();
	form_data.append('file', file_data);
	form_data.append('scenario_media_file', true);
	var ftype   = file_data.type;
	var gettype = $('#scenario_media_file_type').val();
	var ext 	= ftype.split('/')[0];
	if (ext == gettype) {
		$.ajax({
			url: "includes/upload.php",
			type: "POST",
			data: form_data,
			contentType: false,
			cache: false,
			processData:false,
			success: function(resdata) {
				var res = $.parseJSON(resdata);
				if (res.success == true) {
					if (res.zip == true) {
						$('#ImgDelete').show();
						$('.delete_wb_file').attr('data-wb-file', res.file_name);
						$('#scenario_media_file').val(res.file_name);
						$('#upload').html('Upload').removeAttr('disabled', 'disabled');
					}
					else {
						var viewHtml = '<a href="<?php echo $uploadpath ?>'+res.file_name+'" target="_blank" title="View Scenario Media File"><i class="fa fa-eye" aria-hidden="true"></i></a>';
						$('#splashImg').show().html(viewHtml);
						$('#ImgDelete').show();
						$('.delete_scenario_media_file').attr('data-scenario-media-file', res.file_name);
						$('#scenario_media_file').val(res.file_name);
						$('#upload').html('Upload').removeAttr('disabled', 'disabled');
					}
					swal(res.msg, { buttons: false, timer: 1000 });
				}
				else if (res.success == false) {
					$('#scenario_media_file').val('');
					swal("Error", res.msg, "error");
					$('#splashImg').hide('slow');
					$('#upload').html('Upload').removeAttr('disabled', 'disabled');
				}
			},error: function() {
				$('#splashImg').hide('slow');
				swal("Error", 'Oops. something went wrong please try again.?', "error");
			}
		});
	} else { 
		swal("Warning", 'Please select valid file. accept only '+gettype+' file', "warning");
		$('#upload').html('Upload').removeAttr('disabled', 'disabled');
	}
});

$('.delete_scenario_media_file').click(function() {
	var file_name   = $(this).attr("data-scenario-media-file");
	var dataString	= 'delete='+ true + '&scenario_media_file='+file_name;
	if (file_name) {
		swal({
			title: "Are you sure?",
			text: "Delete this file.",
			icon: "warning",
			buttons: [true, 'Delete'],
			dangerMode: true, }).then((willDelete) => { if (willDelete) {
				$.LoadingOverlay("show");
				$.ajax({
					url: "includes/delete-file.php",
					type: "POST",
					data: dataString,
					cache: false,
					success: function(resdata) {
						$.LoadingOverlay("hide");
						var res = $.parseJSON(resdata);
						if (res.success == false) {
							swal("Error", res.msg, "error");
							$('#splashImg').hide('slow');
						}
						else if (res.success == true) {
							$('#splashImg, #ImgDelete').hide('slow');
							$('#scenario_media_file').val('');
							swal(res.msg, { buttons: false, timer: 1000, });
						}
					},error: function() {
						swal("Error", 'Oops, something went wrong. Please try again later.?', "error");
					}
				});
			} else { swal("Your file is safe.!", { buttons: false, timer: 1000, }); }
		});
	}
});

$('.delete_wb_file').click(function() {
	var file_name   = $(this).attr("data-wb-file");
	var dataString	= 'delete='+ true + '&wb_file='+ file_name;
	if (file_name) {
		swal({
			title: "Are you sure?",
			text: "Delete Web Object file.",
			icon: "warning",
			buttons: [true, 'Delete'],
			dangerMode: true, }).then((willDelete) => { if (willDelete) {
				$.LoadingOverlay("show");
				$.ajax({
					url: "includes/delete-file.php",
					type: "POST",
					data: dataString,
					cache: false,
					success: function(resdata) {
						$.LoadingOverlay("hide");
						var res = $.parseJSON(resdata);
						if (res.success == false) {
							swal("Error", res.msg, "error");
							$('#splashImg').hide('slow');
						}
						else if (res.success == true) {
							$('#splashImg, #ImgDelete').hide('slow');
							$('#scenario_media_file').val('');
							swal(res.msg, { buttons: false, timer: 1000, });
						}
					},error: function() {
						swal("Error", 'Oops, something went wrong. Please try again later.?', "error");
					}
				});
			} else { swal("Your file is safe.!", { buttons: false, timer: 1000, }); }
		});
	}
});

/* Upload Web Object */
$('#upload_web').click(function() {
	$(this).attr('disabled', 'disabled').html('<i class="fa fa-refresh fa-spin fa-fw"></i><span class="sr-only">Loading...</span> Please wait....');
	var file_data = $('#web_file').prop('files')[0];
	var form_data = new FormData();
	form_data.append('file', file_data);
	form_data.append('web_object_file', true);
	var ftype = file_data.type;
	var type  = $('#web_object_type').val();
	var ext   = ftype.split('/')[0];
	if (ext == type) {
		$.ajax({
			url: "includes/upload.php",
			type: "POST",
			data: form_data,
			contentType: false,
			cache: false,
			processData: false,
			success: function(resdata) {
				var res = $.parseJSON(resdata);
				if (res.success == true) {
					$('#WbDelete').show();
					$('.delete_wb').attr('data-wb', res.file_name);
					$('#web_object').val(res.file_name);
					$('#upload_web').html('Upload').removeAttr('disabled', 'disabled');
					swal(res.msg, { buttons: false, timer: 1000, });
				}
				else if (res.success == false) {
					swal("Error", res.msg, "error");
					$('#web_object').val('');
					$('#WbDelete').hide('slow');
					$('#upload_web').html('Upload').removeAttr('disabled', 'disabled');
				}
			},error: function() {
				$('#WbDelete').hide('slow');
				swal("Error", 'Oops. something went wrong please try again.?', "error");
			}
		});
	} else { 
		swal("Warning", 'Please select valid file. accept only zip file', "warning");
		$('#upload_web').html('Upload').removeAttr('disabled', 'disabled');
	}
});

/* Delete Web Object */
$('.delete_wb').click(function() {
	var file_name   = $(this).attr("data-wb");
	var dataString	= 'delete='+ true + '&wb_file='+ file_name;
	if (file_name) {
		swal({
			title: "Are you sure?",
			text: "Delete Web Object file.",
			icon: "warning",
			buttons: [true, 'Delete'],
			dangerMode: true, }).then((willDelete) => { if (willDelete) {
				$.LoadingOverlay("show");
				$.ajax({
					url: "includes/delete-file.php",
					type: "POST",
					data: dataString,
					cache: false,
					success: function(resdata) {
						$.LoadingOverlay("hide");
						var res = $.parseJSON(resdata);
						if (res.success == false) {
							swal("Error", res.msg, "error");
							$('#WbDelete').hide('slow');
						}
						else if (res.success == true) {
							$('#WbDelete').hide('slow');
							$('#web_object').val('');
							swal(res.msg, { buttons: false, timer: 1000, });
						}
					},error: function() {
						swal("Error", 'Oops, something went wrong. Please try again later.?', "error");
					}
				});
			} else { swal("Your file is safe.!", { buttons: false, timer: 1000, }); }
		});
	}
});

$('#uploadBG').click(function() {
	$(this).attr('disabled', 'disabled').html('<i class="fa fa-refresh fa-spin fa-fw"></i><span class="sr-only">Loading...</span> Please wait....');
	var file_data = $('#bg_file').prop('files')[0];
	var form_data = new FormData();
	form_data.append('file', file_data);
	form_data.append('add_own_background', true);
	var ftype = file_data.type;
	var ext = ftype.split('/')[0];
	if (ext == 'image') {
		$.ajax({
			url: "includes/upload.php",
			type: "POST",
			data: form_data,
			contentType: false,
			cache: false,
			processData:false,
			success: function(resdata) {
				var res = $.parseJSON(resdata);
				if (res.success == true) {
					$('.delete_scenario_bg_file').attr('data-scenario-bg-file', res.file_name).show();
					$('#scenario_bg_file').val(res.file_name);
					$('.own-bg-prev').attr('src', '<?php echo $uploadpath ?>' + res.file_name);
					$('#uploadBG').html('Upload').removeAttr('disabled', 'disabled');
					swal(res.msg, { buttons: false, timer: 1000, });
				}
				else if (res.success == false) {
					$('#scenario_bg_file').val('');
					swal("Error", res.msg, "error");
					$('.own-bg-prev').attr('src', 'img/scenario-img.png');
					$('#uploadBG').html('Upload').removeAttr('disabled', 'disabled');
				}
			},error: function() {
				swal("Error", 'Oops. something went wrong please try again.?', "error");
			}
		});
	} else { 
		swal("Warning", 'Please select valid file. accept only jpeg, jpg, png file', "warning");
		$('#uploadBG').html('Upload').removeAttr('disabled', 'disabled');
	}
});

$('.delete_scenario_bg_file').click(function() {
	var file_name   = $(this).attr("data-scenario-bg-file");
	var dataString	= 'delete='+ true + '&scenario_media_file='+file_name;
	if (file_name) {
		swal({
			title: "Are you sure?",
			text: "Delete this file.",
			icon: "warning",
			buttons: [true, 'Delete'],
			dangerMode: true, }).then((willDelete) => { if (willDelete) {
				$.LoadingOverlay("show");
				$.ajax({
					url: "includes/delete-file.php",
					type: "POST",
					data: dataString,
					cache: false,
					success: function(resdata) {
						$.LoadingOverlay("hide");
						var res = $.parseJSON(resdata);
						if (res.success == false) {
							swal("Error", res.msg, "error");
						}
						else if (res.success == true) {
							$('#scenario_bg_file').val('');
							$('.delete_scenario_bg_file').hide();
							$('.own-bg-prev').attr('src', 'img/scenario-img.png');
							swal(res.msg, { buttons: false, timer: 1000, });
						}
					},error: function() {
						$.LoadingOverlay("hide");
						swal("Error", 'Oops, something went wrong. Please try again later.?', "error");
					}
				});
			} else { swal("Your file is safe.!", { buttons: false, timer: 1000, }); }
		});
	}
});

$('#uploadChar').click(function() {
	$(this).attr('disabled', 'disabled').html('<i class="fa fa-refresh fa-spin fa-fw"></i><span class="sr-only">Loading...</span> Please wait....');
	var file_data = $('#file_char').prop('files')[0];
	var form_data = new FormData();
	form_data.append('file', file_data);
	form_data.append('add_char', true);
	var ftype = file_data.type;
	var ext = ftype.split('/')[0];
	if (ext == 'image') {
		$.ajax({
			url: "includes/upload.php",
			type: "POST",
			data: form_data,
			contentType: false,
			cache: false,
			processData:false,
			success: function(resdata) {
				var res = $.parseJSON(resdata);
				if (res.success == true) {
					$('.delete_scenario_char_file').attr('data-scenario-char-file', res.file_name).show();
					$('#scenario_char_file').val(res.file_name);
					$('.sce-imgchar').attr('src', '<?php echo $uploadpath ?>' + res.file_name);
					$('#uploadChar').html('Upload').removeAttr('disabled', 'disabled');
					swal(res.msg, { buttons: false, timer: 1000, });
				}
				else if (res.success == false) {
					$('#scenario_char_file').val('');
					swal("Error", res.msg, "error");
					$('.sce-imgchar').attr('src', 'img/charbg.png');
					$('#uploadChar').html('Upload').removeAttr('disabled', 'disabled');
				}
			},error: function() {
				swal("Error", 'Oops. something went wrong please try again.?', "error");
			}
		});
	} else { 
		swal("Warning", 'Please select valid file. accept only jpeg, jpg, png file', "warning");
		$('#uploadChar').html('Upload').removeAttr('disabled', 'disabled');
	}
});

$('.delete_scenario_char_file').click(function() {
	var file_name   = $(this).attr("data-scenario-char-file");
	var dataString	= 'delete='+ true + '&scenario_media_file='+file_name;
	if (file_name) {
		swal({
			title: "Are you sure?",
			text: "Delete this file.",
			icon: "warning",
			buttons: [true, 'Delete'],
			dangerMode: true, }).then((willDelete) => { if (willDelete) {
				$.LoadingOverlay("show");
				$.ajax({
					url: "includes/delete-file.php",
					type: "POST",
					data: dataString,
					cache: false,
					success: function(resdata) {
						$.LoadingOverlay("hide");
						var res = $.parseJSON(resdata);
						if (res.success == false) {
							swal("Error", res.msg, "error");
						}
						else if (res.success == true) {
							$('#scenario_char_file').val('');
							$('.delete_scenario_char_file').hide();
							$('.sce-imgchar').attr('src', 'img/charbg.png');
							swal(res.msg, { buttons: false, timer: 1000, });
						}
					},error: function() {
						$.LoadingOverlay("hide");
						swal("Error", 'Oops, something went wrong. Please try again later.?', "error");
					}
				});
			} else { swal("Your file is safe.!", { buttons: false, timer: 1000, }); }
		});
	}
});

/* Add Cover Image */
$('#upload_cover_img').click(function() {
	$(this).attr('disabled', 'disabled').html('<i class="fa fa-refresh fa-spin fa-fw"></i><span class="sr-only">Loading...</span> Please wait....');
	var file_data = $('#upload_cover_img_file').prop('files')[0];
	var form_data = new FormData();
	form_data.append('file', file_data);
	form_data.append('scenario_cover_file', true);
	var ftype   = file_data.type;
	var type	= $('#cover_img_file_type').val();
	var ext 	= ftype.split('/')[0];
	if (ext == type) {
		$.ajax({
			url: "includes/upload.php",
			type: "POST",
			data: form_data,
			contentType: false,
			cache: false,
			processData:false,
			success: function(resdata) {
				var res = $.parseJSON(resdata);
				if (res.success == true) {
					$('#scenario_cover_file').val(res.file_name);
					$('.sim_cover_img_data,.delete_cover_img_file').show();
					$('.sim_cover_img_data a').text(res.file_name);
					$('.sim_cover_img_data a').attr('data-src', assets_path + res.file_name);
					$('.cover_image_container a.delete_cover_img_file').attr('data-cover-img-file', res.file_name);
					$('#upload_cover_img').html('Upload').attr('disabled', 'disabled');
					$('.upload_cover_btn').attr('disabled', 'disabled');
					swal(res.msg, { buttons: false, timer: 1000 });
				}
				else if (res.success == false) {
					swal("Error", res.msg, "error");
					$('.sim_cover_img_data').hide('slow');
					$('#upload_cover_img').html('Upload').removeAttr('disabled', 'disabled');
					$('.upload_cover_btn').removeAttr('disabled', 'disabled');
				}
			},error: function() {
				swal("Error", 'Oops. something went wrong please try again.?', "error");
				$('#upload_cover_img').html('Upload').removeAttr('disabled', 'disabled');
				$('.upload_cover_btn').removeAttr('disabled', 'disabled');
			}
		});
	} else {
		swal("Warning", 'Please select valid file. accept only '+ type +' file', "warning");
		$('#upload_cover_img').html('Upload').removeAttr('disabled', 'disabled');
		$('.upload_cover_btn').removeAttr('disabled', 'disabled');
	}
});

/* Delete Cover Image */
$('.delete_cover_img_file').click(function() {
	var file_name   = $(this).attr("data-cover-img-file");
	var dataString	= 'delete='+ true + '&scenario_media_file='+ file_name;
	if (file_name) {
		swal({
			title: "Are you sure?",
			text: "Delete this file.",
			icon: "warning",
			buttons: [true, 'Delete'],
			dangerMode: true }).then((willDelete) => { if (willDelete) {
				$.LoadingOverlay("show");
				$.ajax({
					url: "includes/delete-file.php",
					type: "POST",
					data: dataString,
					cache: false,
					success: function(resdata) {
						$.LoadingOverlay("hide");
						var res = $.parseJSON(resdata);
						if (res.success == false) {
							swal("Error", res.msg, "error");
						}
						else if (res.success == true) {
							$('#scenario_cover_file').val('');
							$('.sim_cover_img_data,.delete_cover_img_file').hide('slow');
							$('.sim_cover_img_data a').text('');
							$('.sim_cover_img_data a').attr('data-src', '');
							$('.cover_image_container a.delete_cover_img_file').attr('data-cover-img-file', '');
							$('#upload_cover_img').html('Upload').removeAttr('disabled', 'disabled');
							$('.upload_cover_btn').removeAttr('disabled', 'disabled');
							swal(res.msg, { buttons: false, timer: 1000 });
						}
					},error: function() {
						swal("Error", 'Oops, something went wrong. Please try again later.?', "error");
					}
				});
			} else { swal("Your file is safe.!", { buttons: false, timer: 1000, }); }
		});
	}
});

$('.sim_page_desc').each(function(e){
	CKEDITOR.replace(this.id, { customConfig: "config-max-description.js" });
});

if ($('#sim_page_desc_0').length) {
	CKEDITOR.replace('sim_page_desc_0', {
		customConfig : "config-max-description.js"
	});
}

var page = <?php echo ( ! empty($pagei)) ? $pagei++ : 1; ?>;
$(".plus_page").click(function() {
	$(".plus_page_option:last").append('<div class="add_page_option appendSCE">\
	<div class="form-group">\
		<input type="text" name="sim_page_name[]" class="form-control" placeholder="Page name" required="required" />\
	</div>\
	<div class="form-group">\
		<textarea name="sim_page_desc[]" id="sim_page_desc_'+ page +'" class="form-control"></textarea>\
	</div>\
	 <div class="remove minusComp"><img src="img/icons/minus.png" title="Remove Page"></div></div>');
	 var page_des_id = 'sim_page_desc_'+ page;
	 CKEDITOR.replace(page_des_id, { customConfig : "config-max-description.js" });
	page++;
});

$('.plus_page_option').on('click', '.remove', function(e) {
	e.preventDefault();
	$(this).parent().remove();
	page--;
});

$('input[name="bgoption"]').on('change', function (){
	var bg = $(this).val();
	if (bg == 1) {
		$('#scenario_bg_file').val('');
		$('input[name="sim_background_img"]').attr('checked', false);
	}
	else if (bg == 2) {
		$("#bg_color").spectrum("set", '');
		$('input[name="sim_background_img"]').attr('checked', false);
	}
	else if (bg == 3) {
		$("#bg_color").spectrum("set", '');
		$('#scenario_bg_file').val('');
	}
});

/**** MTF ****/
function matchopen1() {
  document.getElementById("match1").style.display = "block";
}
function matchclose1() {
  document.getElementById("match1").style.display = "none";
}

function mtf_feedback_tts() {
  document.getElementById("mtf_feedback_tts_model").style.display = "block";
}

function mtf_feedback_tts_close1() {
  document.getElementById("mtf_feedback_tts_model").style.display = "none";
}

function mtfi_feedback_tts() {
  document.getElementById("mtfi_feedback_tts_model").style.display = "block";
}

function mtfi_feedback_tts_close1() {
  document.getElementById("mtfi_feedback_tts_model").style.display = "none";
}

<?php if  ( ! empty($sim_data['bg_color'])) : ?>
$("#bg_color").spectrum("set", '<?php echo $sim_data['bg_color'] ?>');
<?php endif; 
if  ( ! empty($sim_brand['ques_bg'])) : ?>
$("#ques_bg_color").spectrum("set", '<?php echo $sim_brand['ques_bg'] ?>');
<?php endif; 
if  ( ! empty($sim_brand['option_bg'])) : ?>
$("#ques_option_bg_color").spectrum("set", '<?php echo $sim_brand['option_bg'] ?>');
<?php endif; 
if  ( ! empty($sim_brand['option_hover'])) : ?>
$("#ques_option_hover_color").spectrum("set", '<?php echo $sim_brand['option_hover'] ?>');
<?php endif; 
if  ( ! empty($sim_brand['option_selected'])) : ?>
$("#ques_option_select_color").spectrum("set", '<?php echo $sim_brand['option_selected'] ?>');
<?php endif; 
if  ( ! empty($sim_brand['btn_bg'])) : ?>
$("#btn_bg_color").spectrum("set", '<?php echo $sim_brand['btn_bg'] ?>');
<?php endif; 
if  ( ! empty($sim_brand['btn_hover'])) : ?>
$("#btn_hover_color").spectrum("set", '<?php echo $sim_brand['btn_hover'] ?>');
<?php endif; 
if  ( ! empty($sim_brand['btn_selected'])) : ?>
$("#btn_select_color").spectrum("set", '<?php echo $sim_brand['btn_selected'] ?>');
<?php endif; 
if  ( ! empty($sim_brand['font_color'])) : ?>
$("#font_color").spectrum("set", '<?php echo $sim_brand['font_color'] ?>');
<?php endif; 
if  ( ! empty($sim_brand['font_type'])) : ?>
$("#font_type").trigger("setFont", '<?php echo $sim_brand['font_type'] ?>');
<?php endif; ?>
document.onreadystatechange = function() {
	if (document.readyState == "complete"){
		$.LoadingOverlay("hide");
	}
};
//MTF
$('body').on('click', '#chkCorMTFEndSim', function() {
	if ($('#chkCorMTFEndSim').is(':checked')) {
		$("#mappedQuestionMtfCorrect").css("display", "none");		
	}else{
		$("#mappedQuestionMtfCorrect").css("display", "block");
	}
});
$('body').on('click', '#chkInMTFEndSim', function() {
	if ($('#chkInMTFEndSim').is(':checked')) {
		$("#mappedQuestionMtfIncorrect").css("display", "none");		
	}else{
		$("#mappedQuestionMtfIncorrect").css("display", "block");
	}
});
//sequence
$('body').on('click', '#chkCorSWQEndSim', function() {
	if ($('#chkCorSWQEndSim').is(':checked')) {
		$("#mappedQuestionSEQCorrect").css("display", "none");		
	}else{
		$("#mappedQuestionSEQCorrect").css("display", "block");
	}
});
$('body').on('click', '#chkInSWQEndSim', function() {
	if ($('#chkInSWQEndSim').is(':checked')) {
		$("#mappedQuestionSEQIncorrect").css("display", "none");		
	}else{
		$("#mappedQuestionSEQIncorrect").css("display", "block");
	}
});
//Sorting
$('body').on('click', '#chkCorSORTEndSim', function() {
	if ($('#chkCorSORTEndSim').is(':checked')) {
		$("#mappedQuestionSORTCorrect").css("display", "none");		
	}else{
		$("#mappedQuestionSORTCorrect").css("display", "block");
	}
});
$('body').on('click', '#chkInSORTEndSim', function() {
	if ($('#chkInSORTEndSim').is(':checked')) {
		$("#mappedQuestionSORTIncorrect").css("display", "none");		
	}else{
		$("#mappedQuestionSORTIncorrect").css("display", "block");
	}
});
//DND
$('body').on('click', '#chkCorDNDEndSim', function() {
	if ($('#chkCorDNDEndSim').is(':checked')) {
		$("#mappedQuestionDNDCorrect").css("display", "none");		
	}else{
		$("#mappedQuestionDNDCorrect").css("display", "block");
	}
});
$('body').on('click', '#chkInDNDEndSim', function() {
	if ($('#chkInDNDEndSim').is(':checked')) {
		$("#mappedQuestionDNDIncorrect").css("display", "none");		
	}else{
		$("#mappedQuestionDNDIncorrect").css("display", "block");
	}
});
//MCQ
$('body').on('click', '#chkCorMCQEndSim', function() {
	if ($('#chkCorMCQEndSim').is(':checked')) {
		$("#mappedQuestionMCQCorrect").css("display", "none");		
	}else{
		$("#mappedQuestionMCQCorrect").css("display", "block");
	}
});
$('body').on('click', '#chkInMCQEndSim', function() {
	if ($('#chkInMCQEndSim').is(':checked')) {
		$("#mappedQuestionMCQIncorrect").css("display", "none");		
	}else{
		$("#mappedQuestionMCQIncorrect").css("display", "block");
	}
});
//MMCQ
$('body').on('click', '#chkCorMMCQEndSim', function() {
	if ($('#chkCorMMCQEndSim').is(':checked')) {
		$("#mappedQuestionMMCQCorrect").css("display", "none");		
	}else{
		$("#mappedQuestionMMCQCorrect").css("display", "block");
	}
});
$('body').on('click', '#chkInMMCQEndSim', function() {
	if ($('#chkInMMCQEndSim').is(':checked')) {
		$("#mappedQuestionMMCQIncorrect").css("display", "none");		
	}else{
		$("#mappedQuestionMMCQIncorrect").css("display", "block");
	}
});
//swipe
$('body').on('click', '#chkCorSwipeEndSim', function() {
	if ($('#chkCorSwipeEndSim').is(':checked')) {
		$("#mappedQuestionSwipeCorrect").css("display", "none");		
	}else{
		$("#mappedQuestionSwipeCorrect").css("display", "block");
	}
});
$('body').on('click', '#chkInSwipeEndSim', function() {
	if ($('#chkInSwipeEndSim').is(':checked')) {
		$("#mappedQuestionSwipeIncorrect").css("display", "none");		
	}else{
		$("#mappedQuestionSwipeIncorrect").css("display", "block");
	}
});
</script>
<script id="code">
	function init() {
		var $ 		= go.GraphObject.make;  // for conciseness in defining templates
		myDiagram 	= $(go.Diagram, "myDiagramDiv", {
						// have mouse wheel events zoom in and out instead of scroll up and down
						"animationManager.initialAnimationStyle": go.AnimationManager.None,
						"InitialAnimationStarting": function(e) {
							var animation = e.subject.defaultAnimation;
							animation.easing = go.Animation.EaseOutExpo;
							animation.duration = 900;
							animation.add(e.diagram, 'scale', 0.1, 1);
							animation.add(e.diagram, 'opacity', 0, 1);
						},
						"toolManager.mouseWheelBehavior": go.ToolManager.WheelZoom,
						initialAutoScale: go.Diagram.Uniform,
						initialContentAlignment: go.Spot.Top,
						initialAutoScale: go.Diagram.UniformToFill,
						layout: $(go.LayeredDigraphLayout, { direction: 90 }),
						"undoManager.isEnabled": true,
					}
				);

       // This template is a Panel that is used to represent each item in a Panel.itemArray.
      // The Panel is data bound to the item object.
      // This template needs to be used by the FieldDraggingTool as well as the Diagram.nodeTemplate.
      var fieldTemplate = $(go.Panel, "TableRow",  // this Panel is a row in the containing Table
	  						new go.Binding("portId", "name"),  // this Panel is a "port"
							  { background: "transparent",  // so this port's background can be picked by the mouse
								fromSpot: go.Spot.Right,  // links only go from the right side to the left side
								toSpot: go.Spot.Left
							}, // allow drawing links from or to this port
							$(go.TextBlock,
								{ margin: new go.Margin(0, 2), column: 1 },
								new go.Binding("text", "name")
							));

      // the FieldDraggingTool needs a template for what to show while dragging
      myDiagram.toolManager.draggingTool.fieldTemplate = fieldTemplate;
	
	  // This template represents a whole "record".
		myDiagram.nodeTemplate = $(go.Node, "Auto", { movable: false, copyable: false, deletable: false, locationSpot: go.Spot.Center },
								 $(go.Shape, { fill: "#FFFF" }), // the content consists of a header and a list of items
								 $(go.Panel, "Vertical", // this is the header for the whole node
								 	$(go.Panel, "Auto", { stretch: go.GraphObject.Horizontal },  // as wide as the whole node
									 	$(go.Shape, { fill: "#000", stroke: null }),
										$("TreeExpanderButton", {
											margin: new go.Margin(0, 0, 15, 0),
											width: 14, height: 14,
											alignment: go.Spot.Right,
											"ButtonIcon.stroke": "white",
											"ButtonIcon.strokeWidth": 2,
											"ButtonBorder.fill": "goldenrod",
											"ButtonBorder.stroke": null,
											"ButtonBorder.figure": "Rectangle",
											"_buttonFillOver": "darkgoldenrod",
											"_buttonStrokeOver": null,
											"_buttonFillPressed": null
										}),
										$("HyperlinkText",
											function(node) { return node.data.url+"?add_data=true&sim_id="+ node.data.sim_id +"&ques_id="+ btoa(node.data.key); },
											function(node) { return node.data.text; },
											{ margin: new go.Margin(0, 0, 0, 0), maxSize: new go.Size(100, NaN), stroke: "whitesmoke", font: "12pt Segoe UI, sans-serif" },
										),
										{ toolTip: $("ToolTip", $(go.TextBlock, new go.Binding("text", "fullText"),
											{ width: 300, wrap: go.TextBlock.WrapFit, stroke: "black", background: "white" }))
										}
									),
									// this Panel holds a Panel for each item object in the itemArray;
									// each item Panel is defined by the itemTemplate to be a TableRow in this Table
									$(go.Panel, "Table", { name: "TABLE", padding: 1, minSize: new go.Size(100, 0), defaultStretch: go.GraphObject.Horizontal, itemTemplate: fieldTemplate },
										new go.Binding("itemArray", "fields"),
									),  // end Table Panel of items
								));
		myDiagram.linkTemplate = $(go.Link, $(go.Shape));
		load();
		layout();
	}

	function layout() {
		myDiagram.layoutDiagram(true);
	}

	function load() {
		myDiagram.model = go.Model.fromJson(document.getElementById("mySavedModel").value);
		//myDiagram.commandHandler.zoomToFit();
		zoomSlider = new ZoomSlider(myDiagram, { alignment: go.Spot.BottomLeft, alignmentFocus: go.Spot.BottomLeft, size:100});
	}
	window.addEventListener('DOMContentLoaded', init);
</script>
<?php 
require_once 'includes/footer.php';
