<?php 
ob_start();
session_start();
if (isset($_SESSION['username'])) {
    $user   = $_SESSION['username'];
    $role   = $_SESSION['role'];
    $userid = $_SESSION['userId'];
}
else {
    header('location: index.php');
}
require_once 'config/db.class.php';
require_once 'includes/learners_menu.php';
$db         = new DBConnection();
$rowCount   = 0;
$getGroups  = $db->getGroupByEducator("GROUP_CONCAT(DISTINCT group_id SEPARATOR ',') as col", $userid);
if ( ! empty($getGroups[0]['col'])):
    $sim_id = $db->getAssignSimByGroupId($getGroups[0]['col']);
	$q      = "SELECT DISTINCT scenario_id FROM scenario_master WHERE author_scenario_type != '0' AND sim_ques_type = '3' AND sim_temp_type = '7' AND scenario_id IN ($sim_id)";
	$qexe   = $db->prepare($q); $qexe->execute();
	$rowCount = $qexe->rowCount();
endif; ?>
<style type="text/css">
admin-dashboard.Ev2 .supDASHTOP { margin: 0px; }
.cls-1 { stroke: transparent !important; }
.flexRI.client.clients { display: flex; }
.hometab.active,  .menu1tab.active { display: flex !important; }
@media (min-width:320px) and (max-width: 767px) {
  .menu1tab.active { display: block !important; }
}
.flexItem .item { margin: 5px; }
</style>
<div class="admin-dashboard Ev2 revdashBoard EducDASHboard">
    <div class="container-fluid">
        <div class="flexItem flexItemsdMinDash AutHorDahBan">
          <div class="flextlistbaner hometab clearfix owl-carousel owl-theme active">
            <div class="item edu-tab1">
              <div class="flextlistitem">
                <div class="supDASHTOPtextB">
                  <div class="flexRI">
                    <p class="noofsubtextno"><?php echo $rowCount; ?></p>
                  </div>
                  <div class="flexRI client">
                    <p class="noofsubtext">OPEN RESPONSE SIMULATIONS</p>
                  </div>
                </div>
              </div>
            </div>
            <div class="item Rev4flextlistitem edu-tab3">
              <div class="usermanage-form educatorrepORT">
                <div class="row L-R-W20ROW">
                  <div class="L-R-W20">
                    <div class="form-group form-select">
                      <select name="category" id="category" class="form-control" data-placeholder="Category List" required="required">
                        <option value="0" selected="selected" disabled="disabled" hidden="">Select Category</option>
                        <?php foreach ($db->getCategory() as $category): ?>
                        <option value="<?php echo $category['cat_id'] ?>"><?php echo ucwords($category['category']) ?></option>
                        <?php endforeach; ?>
                      </select>
                    </div>
                  </div>
                  <div class="L-R-W20">
                    <div class="form-group form-select">
                      <select name="scenario" id="scenario" class="form-control" data-placeholder="Simulation List" required="required">
                        <option value="0" selected="selected" disabled="disabled">Select Simulation</option>
                      </select>
                    </div>
                  </div>
                  <div class="L-R-W20">
                    <div class="form-group form-select">
                      <select name="group" id="group" class="form-control" data-placeholder="Group List">
                        <option value="0" selected="selected" disabled="disabled">Select Group</option>
                      </select>
                    </div>
                  </div>
                </div>
              </div>
              <div class="flextlistitem Rev4flexbox group_data">
                <div class="col4SMgrid">
                  <div class="Rev4flexGheading">NUMBER OF LEARNERS</div>
                  <div class="score">
                      <div class="col-sm-12">
                          <span class="Score-Y" id="no_of_learners">0</span>
                    </div>
                  </div>
                </div>
                <div class="col4SMgrid">
                  <div class="Rev4flexGheading">%ATTEMPTED</div>
                  <div class="score">
                    <div class="col-sm-12">
                        <span class="Score-Y" id="attempted">0</span>
                    </div>
                  </div>
                </div>
                <div class="col4SMgrid">
                  <div class="Rev4flexGheading">%PASSED</div>
                  <div class="score">
                    <div class="col-sm-12">
                        <span class="Score-Y" id="passed">0</span>
                    </div>
                  </div>
                </div>
                <div class="col4SMgrid">
                  <div class="Rev4flexGheading">%REMEDIATION REQUIRED</div>
                  <div class="score">
                    <div class="col-sm-12">
                        <span class="Score-Y" id="rrequired">0</span>
                    </div>
                  </div>
                </div>
                <div class="col4SMgrid">
                  <div class="Rev4flexGheading">%JOB READINESS</div>
                  <div class="score">
                    <div class="col-sm-12">
                        <span class="Score-Y" id="job_readiness">0</span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="tab-content">
        <div id="home" class="tab-pane fade in active">
          <div class="adminT">
            <div class="col-sm-4"></div>
            <div class="col-sm-2"> <a href="<?php echo $db->getBaseUrl('educator-simulation.php') ?>">
              <div class="dashboard-box colorBoxW"> <img class="img-responsive width70 svg" src="img/dash_icon/evaluate_simulation.svg">
                <div class="dash-sub-title">Evaluate Simulations</div>
                <div class="dash-msg clearfix">
                  <div class="notino"><img class="img-responsive" src="img/massage.png"> <span class="notifi-icon"><?php echo $rowCount; ?></span></div>
                </div>
              </div>
              </a> </div>
            <div class="col-sm-2"><a href="<?php echo $db->getBaseUrl('educator-report.php') ?>">
              <div class="dashboard-box colorBoxW"> <img class="img-responsive width70 svg" src="img/dash_icon/view_report1.svg">
                <div class="dash-sub-title">View Report</div>
                <div class="dash-msg clearfix">
                  <div class="notino"><img class="img-responsive" src="img/massage.png"> <span class="notifi-icon">1</span></div>
                </div>
              </div>
              </a> </div>
          </div>
        </div>
      </div>
    </div>
<script type="text/javascript">
  $('.group_data').hide();
  $('#category').on('change', function() {
    var catid = $(this).val();
		if (catid != '')
      $('.group_data').hide();
      $('#scenario,#group').find("option:eq(0)").html("Please wait..");
      $.getJSON('includes/ajax.php?getEducatorScenario=true&catid='+ catid, function(res) {
        $('#scenario,#group').empty();
        if (res.success == true) {
          $('#scenario').append('<option value="" selected="selected" disabled="disabled">Select Simulation</option>');
          $('#group').append('<option value="" selected="selected" disabled="disabled">Select Group</option>');
          $.each(res.data, function (key, val) {
            var option = '<option value="'+ val.value +'">'+ val.label +'</option>';
            $('#scenario').append(option);
          });
        }
        else if (res.success == false) {
          $('#scenario').append('<option value="" selected="selected" disabled="disabled">Select Simulation</option>');
          $('#group').append('<option value="" selected="selected" disabled="disabled">Select Group</option>');
        }
      });
	});

	$('#scenario').on('change', function() {
		var sim_id = $(this).val();
		if (sim_id != '')
      $('#no_of_learners,#attempted,#passed,#rrequired,#job_readiness').html(0);
      $('#group').find("option:eq(0)").html("Please wait..");
			$.getJSON('includes/ajax.php?getEducatorScenarioGroup=true&sim_id='+ sim_id, function(res){
				$('#group').empty();
				if (res.success == true) {
					$('#group').append('<option value="" selected="selected" disabled="disabled">Select Group</option>');
					$.each(res.data, function (key, val) {
						var option = '<option value="'+ val.value +'">'+ val.label +'</option>';
						$('#group').append(option);
					});
				}
				else {
					$('#group').append('<option value="" selected="selected" disabled="disabled">Select Group</option>');
				}
			});
	});
  
  $('#group').on('change', function() {
    $('.group_data').show();
		var gid = $(this).val();
		var sid = $('#scenario').val();
		if (gid != '' && sid != '')
			$.getJSON('includes/ajax.php?getGroupReport=true&gid='+ gid + '&sid='+ sid, function(res) {
				if (res.success == true) {
          $('#no_of_learners').html(res.totalL);
          $('#attempted').html(res.attempted + '%');
          $('#passed').html(res.totalpass + '%');
          $('#rrequired').html(res.totalfail + '%');
          $('#job_readiness').html(res.jreadines + '%');
        }
				else { $('#no_of_learners,#attempted,#passed,#rrequired,#job_readiness').html(0); }
			});
	});
</script>
<?php 
require_once 'includes/footer.php';
