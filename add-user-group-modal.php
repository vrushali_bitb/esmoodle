<?php 
session_start();
if (isset($_SESSION['username'])) {
	$user	= $_SESSION['username'];
	$role   = $_SESSION['role'];
    $userid = $_SESSION['userId'];
}
else {
	header('location: index.php');
}
require_once 'config/db.class.php';
$db		 	= new DBConnection;
$group_id   = (isset($_POST['data-id'])) ? $_POST['data-id'] : FALSE;
$group 		= $db->getGroup(md5($group_id));
$learner 	= "'learner'";
$author	 	= "'author'";
$reviewer	= "'reviewer'";
$learnerId	= ( ! empty($group['learner'])) ? explode(',', $group['learner']) : '';
$authorId	= ( ! empty($group['author'])) ? explode(',', $group['author']) : '';
$reviewerId	= ( ! empty($group['reviewer'])) ? explode(',', $group['reviewer']) : '';
?>
<style>
.modal-footer {
   border-top: 0px solid #e5e5e5;
}
</style>
<div id="load_popup_modal_contant" class="" role="dialog" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><strong><?php echo $group['group_name'] ?></strong> » Add Users</h4>
            </div>
            <div class="modal-body">
            	<form enctype="multipart/form" name="edit_scenario_type_form" id="edit_scenario_type_form" method="post">
                    <input type="hidden" name="author[]" id="author[]" />
                    <input type="hidden" name="reviewer[]" id="reviewer[]" />
                	<div class="col-sm-12">
                        <?php /*
                        <div class="col-sm-4">
                            <label for="users">Author List</label>
                            <select name="author[]" id="author[]" class="form-control multiselect-ui selection" multiple="multiple">
                                <?php foreach ($db->getUserByRole($author, NULL) as $authors): ?>
                                <option value="<?php echo $authors['id'] ?>" <?php if ( ! empty($authorId) && array_search($authors['id'], $authorId) !== FALSE) { echo 'selected'; } ?>><?php echo ucwords($authors['username']) ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                        <div class="col-sm-4">
                            <label for="users">Reviewer List</label>
                            <select name="reviewer[]" id="reviewer[]" class="form-control multiselect-ui selection" multiple="multiple">
                                <?php foreach ($db->getUserByRole($reviewer, NULL) as $reviewers): ?>
                                <option value="<?php echo $reviewers['id'] ?>" <?php if ( ! empty($reviewerId) && array_search($reviewers['id'], $reviewerId) !== FALSE) { echo 'selected'; } ?>><?php echo ucwords($reviewers['username']) ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                        */ ?>
						<label for="users">Learner List</label>
                        <select name="learner[]" id="learner[]" class="form-control multiselect-ui selection" multiple="multiple">
                            <?php foreach ($db->getUserByRole($learner, NULL) as $learners): ?>
                            <option value="<?php echo $learners['id'] ?>" <?php if ( ! empty($learnerId) && array_search($learners['id'], $learnerId) !== FALSE) { echo 'selected'; } ?>><?php echo ucwords($learners['username']); ?> <?php echo ( ! empty($learners['department'])) ? '- '. $learners['department'] : ''; ?> <?php echo ( ! empty($learners['designation'])) ? ' -'. $learners['designation'] : ''; ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                	<div class="modal-footer">
                    	<input type="hidden" name="add_user_group" value="1" />
						<input type="hidden" name="group_id" value="<?php echo $group_id ?>" />
                        <button type="submit" name="editGroup" id="editGroup" class="btn btn-outline btn-primary" onClick="return confirm('Are you sure to add users in this group.?');">Add Users</button>
                    </div>
                </form>
            </div>
        </div>
     </div>
</div>
<script type="text/javascript">
$(function() {
	$('.multiselect-ui').multiselect({
        disableIfEmpty: true,
		includeSelectAllOption: true,
		enableCaseInsensitiveFiltering : true,
		enableFiltering: true,
        filterPlaceholder: 'Search & Select (Username - department - designation)',
        buttonContainer: '<div class="btn-group w-100" />',
		maxHeight: 250
	});
});
$("#edit_scenario_type_form").on('submit', (function(e) {
	e.preventDefault();
    $.LoadingOverlay("show");
	var form_data = $(this).serialize();
	$.ajax({
		url: "includes/process.php",
		type: "POST",
		data: form_data,
		success: function(result) {
			var res = $.parseJSON(result);
            $.LoadingOverlay("hide");
			if (res.success == true) {
				swal({text: res.msg, buttons: false, icon: "success", timer: 1000});
				setTimeout(function() { window.location.reload(); }, 1000);
			}
			else if (res.success == false) {
				swal({text: res.msg, buttons: false, icon: "error", timer: 1000});
			}
		},error: function(){
            $.LoadingOverlay("hide");
			swal({text: 'Oops, something went wrong. Please try again later', buttons: false, icon: "error", timer: 1000});
		}
	});
}));
</script>
<?php 
ob_end_flush();
