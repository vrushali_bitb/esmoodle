<?php
session_start();
if (isset($_SESSION['username'])) {
	$user	= $_SESSION['username'];
	$role	= $_SESSION['role'];
	$userid	= $_SESSION['userId'];
} else {
	header('location: index.php');
}
require_once 'config/db.class.php';
require_once 'includes/learners_menu.php';
$obj = new DBConnection;
$eid = (isset($_GET['eid'])) ? $_GET['eid'] : '';
$tmpData = $obj->getEmailTemplate($eid); ?>
<div class="bottomheader">
	<div class="container-fluid">
		<ul class="breadcrumb">
			<?php echo $db->breadcrumbs(); ?>
		</ul>
	</div>
</div>
<div class="admin-report">
	<div class="container-fluid">
		<div class="row">
			<div class="col-sm-6">
				<h3>Email Management</h3>
			</div>
			<div class="col-sm-6 alert alert-warning" role="alert">
				<i class="fa fa-info-circle fa-2x" aria-hidden="true"></i> 
				Please don't change { } variables.
			</div>
		</div>
	</div>
</div>
<?php if (isset($_SESSION['msg'])) : ?>
	<div class="alert alert-warning alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		<?php echo $_SESSION['msg']; ?>
	</div>
<?php endif; ?>
<form method="post" action="includes/process.php" enctype="multipart/form-data">
	<div class="container-fluid">
		<div class="row" style="margin-bottom:2%;">
			<div class="col-md-6">
				<font color="#2c3545">
					<h5 for="usr">Subject</h5>
				</font>
				<input type="text" name="email_subject" id="email_subject" class="form-control" required="required" value="<?php echo $tmpData['subject'] ?>" />
			</div>
			<div class="col-md-6">
				<font color="#2c3545">
					<h5 for="usr">Type</h5>
				</font>
				<input type="text" name="email_type" id="email_type" class="form-control" required="required" value="<?php echo $tmpData['type'] ?>" readonly="readonly" />
			</div>
			<div class="col-md-12 email_temp">
				<font color="#2c3545">
					<h5 for="usr">Template</h5>
				</font>
				<textarea class="editor1" id="editor1" name="tmp_des" style="width:100%" required="required"><?php echo $tmpData['tmp_des'] ?></textarea>
			</div>
			<div class="col-md-6 email_temp">
				<font color="#2c3545">
					<h5 for="usr">Comment</h5>
				</font>
				<textarea id="comment" class="form-control" name="comment" style="width:100%;height:34px;"><?php echo $tmpData['comment'] ?></textarea>
			</div>
			<div class="col-md-6 email_temp">
				<font color="#2c3545">
					<h5 for="usr">Status</h5>
				</font>
				<select name="status" id="status" class="form-control">
					<option value="1" <?php echo ($tmpData['status'] == 1) ? 'selected="selected"' : ''; ?>>Active</option>
					<option value="0" <?php echo ($tmpData['status'] == 0) ? 'selected="selected"' : ''; ?>>Inactive</option>
				</select>
			</div>
		</div>
	</div>
	<center>
		<div class="col-md-12">
			<input type="hidden" name="email_tmp_id" value="<?php echo $tmpData['eid'] ?>" />
			<input type="submit" name="update_email_tmp" value="Update" class="btn btn-outline btn-primary" style="width:155px;">
			<br /><br />
		</div>
	</center>
</form>
<script type="text/javascript">
	CKEDITOR.replace('editor1', {
		customConfig: 'ckeditor_config.js',
	});
</script>
<?php
unset($_SESSION['msg']);
require_once 'includes/footer.php';
