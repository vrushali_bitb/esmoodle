<?php 
ob_start();
session_start();
?>
<!doctype html>
<!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]><html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--><html class="no-js" lang="en"> <!--<![endif]-->
<head>
  <title>EasySIM: Login</title>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="shortcut icon" type="image/x-icon" href="favicon.ico" />
  <!-- Chrome, Firefox OS and Opera -->
  <meta name="theme-color" content="#2c3545">
  <!-- Windows Phone -->
  <meta name="msapplication-navbutton-color" content="#2c3545">
  <!-- iOS Safari -->
  <meta name="apple-mobile-web-app-status-bar-style" content="#2c3545">
  <link rel="stylesheet" media="screen" href="content/css/bootstrap.css" />
  <link rel="stylesheet" media="screen" href="content/css/bootstrap.min.css" />
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" media="screen" href="content/css/style.css?v=<?php echo time() ?>" />
  <link rel="stylesheet" media="screen" href="content/css/responsive.css?v=<?php echo time() ?>" />
  <script type="text/javascript" src="content/js/jquery-3.3.1.min.js"></script>
  <script type="text/javascript" src="content/js/bootstrap.3.3.7.min.js"></script>
  <script type="text/javascript" src="content/js/sweetalert.min.js"></script>
  <script type="text/javascript" src="content/js/loadingoverlay.min.js"></script>
  <script type="text/javascript" src="content/js/timezone.js"></script>
</head>
<body>
<?php 
require_once 'config/db.class.php';
require_once 'config/securimage/securimage.php';
$obj    = new DBConnection;
$image  = new Securimage();
$logo   = ( ! empty($obj->siteLogo)) ? $obj->siteLogo : 'EasySIM_logo.svg';
$brand  = $obj->webBranding();
# Check if already login then redirect
if (isset($_SESSION['username']) && isset($_SESSION['role'])) :
  $user   = $_SESSION['username'];
  $role   = $_SESSION['role'];
  $userid = $_SESSION['userId'];
  if (isset($role)) $obj->userRoleRedirect($role);
endif;
echo ( ! empty($brand)) ? $brand : ''; ?>
  <header>
    <div class="home_header">
      <ul class="logowhite">
        <a href="<?php echo $obj->getBaseUrl(); ?>">
          <li><img class="white-svg-logo" src="<?php echo $obj->getBaseUrl('img/logo/'. $logo); ?>"></li>
        </a>
        <li><img class="white-svg-logo1" src="<?php echo $obj->getBaseUrl('img/homepage/logo.svg'); ?>"></li>
      </ul>
    </div>
  </header>
  <div class="login-form">
    <div class="col-md-4"></div>
    <div class="loginbanner">
      <div class="col-md-4">
        <div class="logintext">Login</div>
        <div class="login_box">
          <form method="post" class="loginregerstion" id="loginregerstion">
            <input type="hidden" name="login" value="1">
            <div class="form-group">
              <div class="input-group"> <span class="loginicions"><img src="img/left_menu/user.svg"></span>
                <input type="text" class="form-control" placeholder="Username" name="username" autofocus required>
              </div>
            </div>
            <div class="form-group">
              <div class="input-group" id="show_hide_password"> <span class="loginicions"><img src="img/left_menu/password.svg"></span>
                <input type="password" class="form-control" placeholder="Password" name="password" required>
                <div class="input-group-addon loginaddon"><a href="javascript:void(0)"><i class="fa fa-eye-slash" aria-hidden="true"></i></a></div>
              </div>
            </div>
            <div class="form-group">
              <div class="input-group"> <span class="loginicions"><img src="img/left_menu/role.svg"></span>
                <select class="form-control" id="sel1" name="role">
                  <option disabled selected hidden>Role</option>
                  <?php foreach ($obj->role as $roleKey => $roleVal): ?>
                  <option value="<?php echo $roleKey; ?>"><?php echo $roleVal; ?></option>
                  <?php endforeach; ?>
                </select>
              </div>
            </div>
            <div class="form-group">
            <?php 
              $data = array(
                'input_text'          => '',
                'audio_icon_url'      => 'img/left_menu/capcha_audio.svg',
                'audio_icon_url2'     => 'img/left_menu/capcha_audio_hover.svg',
                'refresh_icon_url'    => 'img/left_menu/capcha_replay.svg',
                'refresh_icon_url2'   => 'img/left_menu/capcha_replay_hover.svg',
                'input_icon_url'      => 'img/left_menu/Capcha.svg',
                'icon_size'           => 32,
                'refresh_alt_text'    => 'Refresh Characters',
                'refresh_title_text'  => 'Refresh Characters',
                'input_attributes'    => array('class' => 'form-control', 'placeholder' => 'Enter Characters Displayed')
              );
              echo Securimage::getCaptchaHtml($data); ?>
            </div>
            <div class="form-group">
              <center>
                <button type="submit" class="regsterbtn" name="login_user">LOGIN</button>
              </center>
            </div>
            <div class="form-group forgetcrearttext">
              <div class="createacount"><a href="<?php echo $obj->getBaseUrl('register'); ?>">Create an Account</a></div>
              <div class="Rpassword"> <a href="<?php echo $obj->getBaseUrl('forgotten'); ?>">Forgot Password?</a> </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
  <div class="footergray yellow"></div>
  <script type="text/javascript" src="content/js/script.js?v=<?php echo time() ?>"></script>
  <script type="text/javascript" src="content/js/app.js?v=<?php echo time() ?>"></script>
  
