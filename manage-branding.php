<?php 
ob_start();
session_start();
if (isset($_SESSION['username'])) {
	$client_id	= $_SESSION['client_id'];
	$userid		= $_SESSION['userId'];
	$user		= $_SESSION['username'];
	$role		= $_SESSION['role'];
} else {
	header('location: index.php');
}
require_once 'config/db.class.php';
require_once 'includes/learners_menu.php';
$db = new DBConnection();
/* page access only when permission */
(empty($clientData['mbrand'])) ? header('location: index.php') : '';
$brand = $db->webBranding($client_id); ?>
<div class="bottomheader">
	<div class="container-fluid">
		<ul class="breadcrumb">
			<?php echo $db->breadcrumbs(); ?>
		</ul>
	</div>
</div>
<link rel="stylesheet" type="text/css" href="content/css/template-updated.css" />
<link rel="stylesheet" type="text/css" href="content/css/jquery.fontselect.css" />
<script type="text/javascript" src="content/js/spectrum/spectrum.min.js"></script>
<link rel="stylesheet" type="text/css" href="content/js/spectrum/spectrum.min.css" />
<style>
	.brandmanag-box {
		display: flex;
		margin-bottom: 20px;
		margin-top: 20px;
	}
	.pull-right {
		padding: 0px 10px 0px !important;
	}

	.brandmanagl {
		width: 100%;
	}

	.brandmanag-flex {
		display: flex;
		padding: 10px 0;
	}
</style>
<div class="simulation_assignment super-client-manag">
	<div class="container-fluid">
		<div class="row">
			<div class="col-sm-12">
				<form method="post" class="form-inline" name="superAdmin_form" id="superAdmin_form">
					<div class="brandingmanagment">
						<div class="branding-color">
							<div class="branding-flex brandingbox">
								<div class="brandmanag-box MBboXborder">
									<div class="brandmanagl">
										<div class="user_text colorp">Primary Colors</div>
										<div class="branding-flex"><input class="type-color-on-page" name="btn_color" id="btn_color" autocomplete="off" /></div>
										<div class="branding-flex"><input class="type-color-on-page" name="btn_hover_color" id="btn_hover_color" autocomplete="off" /></div>
										<!-- <div class="branding-flex"><input class="type-color-on-page" name="icon_color" id="icon_color" autocomplete="off" /></div> -->
										<!-- <div class="branding-flex"><input class="type-color-on-page" name="icon_hover_color" id="icon_hover_color" autocomplete="off" /></div> -->
									</div>
								</div>
								<div class="brandmanag-box">
									<div class="brandmanagl">
										<div class="user_text colorp">Secondary Colors</div>
										<div class="branding-flex"><input class="type-color-on-page" autocomplete="off" /></div>
										<div class="branding-flex"><input class="type-color-on-page" autocomplete="off" /></div>
										<div class="branding-flex"><input class="type-color-on-page" autocomplete="off" /></div>
										<div class="branding-flex"><input class="type-color-on-page" autocomplete="off" /></div>
										<div class="branding-flex"><input class="type-color-on-page" autocomplete="off" /></div>										
										<div class="branding-flex"><input class="type-color-on-page" autocomplete="off" /></div>
									</div>
								</div>
							</div>
						</div>
						<div class="user_text fonts">Font</div>
						<div class="brandmanag-font">
							<div class="brandmanag-flex">
								<span class="colorb">Font Type</span>
								<input id="font_type" name="font_type" type="text" />
								<?php if ( ! empty($brand['font_type'])): ?>
                                <div class="col-sm-2"><button class="cl_ftype btn btn-primary" type="button">Set Default Font Type</button></div>
								<?php endif; ?>
							</div>
							<div class="brandmanag-flex">
								<span class="colorb">Font Color</span>
								<input class="type-color-on-page" name="font_color" id="font_color" autocomplete="off" />
							</div>
							<div class="brandmanag-flex"><span class="colorb">Font Size</span>
								<select name="font_size" id="font_size" class="form-control fontSiZE">
									<option selected="selected" value="0">Select font size</option>
									<option value="10px" <?php echo ( ! empty($brand['font_size']) && $brand['font_size'] == '10px') ? 'selected="selected"' : '' ?>>10px</option>
									<option value="12px" <?php echo ( ! empty($brand['font_size']) && $brand['font_size'] == '12px') ? 'selected="selected"' : '' ?>>12px</option>
									<option value="16px" <?php echo ( ! empty($brand['font_size']) && $brand['font_size'] == '16px') ? 'selected="selected"' : '' ?>>16px</option>
									<option value="18px" <?php echo ( ! empty($brand['font_size']) && $brand['font_size'] == '18px') ? 'selected="selected"' : '' ?>>18px</option>
									<option value="20px" <?php echo ( ! empty($brand['font_size']) && $brand['font_size'] == '20px') ? 'selected="selected"' : '' ?>>20px</option>
									<option value="24px" <?php echo ( ! empty($brand['font_size']) && $brand['font_size'] == '24px') ? 'selected="selected"' : '' ?>>24px</option>
									<option value="26px" <?php echo ( ! empty($brand['font_size']) && $brand['font_size'] == '26px') ? 'selected="selected"' : '' ?>>26px</option>
								</select>
							</div>
						</div>
					</div>
					<div class="usermanage-add clearfix">
						<input type="hidden" name="update_branding" value="1">
						<input type="hidden" name="client_id" value="<?php echo $client_id ?>">
						<input type="submit" name="update_branding_btn" class="btn btn-primary pull-right" value="Update" />
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
<script src="content/js/jquery-ui-1.12.1/jquery-ui.js"></script>
<script src="content/js/jquery.fontselect.js"></script>
<script type="text/javascript">
$(document).ready(function() {
	/* Set Default Font Type */
	$('.cl_ftype').click(function() {
		$('#font_type').trigger('setFont', '');
	});

	$('.type-color-on-page').spectrum({
		type: "component",
		togglePaletteOnly: "true",
		hideAfterPaletteSelect: "true",
		showInput: "true",
		showInitial: "true",
	});

	$('#font_type').fontselect({
		placeholder: 'Pick a font from the list',
		searchable: false,
		systemFonts: [],
		googleFonts: [<?php echo $db::googleFont; ?>]
	});
	
	<?php if ( ! empty($brand['btn_color'])): ?>
		$("#btn_color").spectrum("set", '<?php echo $brand['btn_color'] ?>');
	<?php endif;
	if ( ! empty($brand['btn_hcolor'])): ?>
		$("#btn_hover_color").spectrum("set", '<?php echo $brand['btn_hcolor'] ?>');
	<?php endif;
	if ( ! empty($brand['icon_color'])): ?>
		$("#icon_color").spectrum("set", '<?php echo $brand['icon_color'] ?>');
	<?php endif;
	if ( ! empty($brand['icon_hcolor'])): ?>
		$("#icon_hover_color").spectrum("set", '<?php echo $brand['icon_hcolor'] ?>');
	<?php endif;
	if ( ! empty($brand['font_color'])): ?>
		$("#font_color").spectrum("set", '<?php echo $brand['font_color'] ?>');
	<?php endif;
	if ( ! empty($brand['font_type'])): ?>
		$("#font_type").trigger("setFont", '<?php echo $brand['font_type'] ?>');
	<?php endif; ?>
});
</script>
<?php 
require_once 'includes/footer.php';
