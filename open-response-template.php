<?php 
session_start();
if (isset($_SESSION['username'])) {
    $user   = $_SESSION['username'];
    $role   = $_SESSION['role'];
    $userid = $_SESSION['userId'];
	$domain = (isset($_SESSION['domain_name'])) ? $_SESSION['domain_name'] : $_SERVER['HTTP_HOST'];
}
else {
    header('location: index.php');
}
require_once 'config/db.class.php';
require_once 'includes/learners_menu.php';
$db 		= new DBConnection();
$sim_id		= (isset($_GET['sim_id']) && ! empty($_GET['sim_id'])) ? $_GET['sim_id'] : '';
$ques_id	= (isset($_GET['ques_id'])) ? base64_decode($_GET['ques_id']) : '';
$sim_data 	= $db->getScenario($sim_id);
$sim_com 	= $db->getScenarioCompetency($sim_id);
$sim_page 	= $db->getSimPage($sim_data['scenario_id']);
$sim_brand 	= $db->getSimBranding($sim_data['scenario_id']);
$sim_type	= $db->getMultiScenarioType($sim_data['author_scenario_type']);
$path		= $db->getBaseUrl('scenario/upload/'.$domain.'/');
$uploadpath	= '../scenario/upload/'.$domain.'/';
$root_path	= $db->rootPath() . 'scenario/upload/'.$domain.'/';
$prelink 	= 'launch-sim-open-response-preview.php?preview=true&save=false&sim_id='. $sim_id;
$preview    = ( ! empty($ques_id)) ? $prelink . '&ques_id='. base64_encode($ques_id) : $prelink;
$type_name	= $sim_type[0]['type_name'];
$type 		= strtolower($type_name);
$gettype 	= '';
if ($type == 'image and text'):
	$gettype = 'image';
else:
	$gettype = $type;
endif;
$upload_file_name = '';
if ( ! empty($sim_data['image_url']) && file_exists($root_path . $sim_data['image_url'])):
	$upload_file_name = $sim_data['image_url'];
elseif ( ! empty($sim_data['video_url']) && file_exists($root_path . $sim_data['video_url'])):
	$upload_file_name = $sim_data['video_url'];
elseif ( ! empty($sim_data['audio_url']) && file_exists($root_path . $sim_data['audio_url'])):
	$upload_file_name = $sim_data['audio_url'];
elseif ( ! empty($sim_data['web_object_file'])):
	$upload_file_name = $sim_data['web_object_file'];
endif;
$web_object = '';
if ( ! empty($sim_data['web_object'])):
	$web_object = $sim_data['web_object'];
endif;
$ques_val1 = $ques_val2 = $ques_val3 = $ques_val4 = $ques_val5 = $ques_val6 = 0;
if ( ! empty($ques_id)):
	$qsql = "SELECT * FROM question_tbl WHERE question_id = '". $ques_id ."'";
	try {
		$qstmt	 = $db->prepare($qsql); $qstmt->execute();
		$qrow	 = $qstmt->rowCount();
		$qresult = ($qrow > 0) ? $qstmt->fetch() : '';
	}
	catch(PDOException $e) {
		echo "Oops. something went wrong. ".$e->getMessage(); exit;
	}
	$tqsql  = "SELECT * FROM question_tbl WHERE MD5(scenario_id) = '". $sim_id ."'";
	$tqstmt = $db->prepare($tqsql); $tqstmt->execute();
	$tqresult = $tqstmt->fetchAll();
	foreach ($tqresult as $tqueSocre):
		if ( ! empty($tqueSocre['ques_val_1'])) $ques_val1 += $tqueSocre['ques_val_1'];
		if ( ! empty($tqueSocre['ques_val_2'])) $ques_val2 += $tqueSocre['ques_val_2'];
		if ( ! empty($tqueSocre['ques_val_3'])) $ques_val3 += $tqueSocre['ques_val_3'];
		if ( ! empty($tqueSocre['ques_val_4'])) $ques_val4 += $tqueSocre['ques_val_4'];
		if ( ! empty($tqueSocre['ques_val_5'])) $ques_val5 += $tqueSocre['ques_val_5'];
		if ( ! empty($tqueSocre['ques_val_6'])) $ques_val6 += $tqueSocre['ques_val_6'];
	endforeach;
endif; ?>
<link rel="stylesheet" type="text/css" href="content/css/owlcarousel/owl.carousel.min.css" />
<link rel="stylesheet" type="text/css" href="content/css/qustemplate.css" />
<link rel="stylesheet" type="text/css" href="content/css/questionresponsive.css" />
<link rel="stylesheet" type="text/css" href="content/css/template-updated.css" />
<link rel="stylesheet" type="text/css" href="content/css/jquery.fontselect.css">
<script type="text/javascript" src="content/js/inputmask/jquery.inputmask.js"></script>
<script type="text/javascript" src="content/js/spectrum/spectrum.min.js"></script>
<link rel="stylesheet" type="text/css" href="content/js/spectrum/spectrum.min.css" />
<link rel="stylesheet" type="text/css" href="content/fancybox/jquery.fancybox.min.css" />
<script type="text/javascript" src="content/fancybox/jquery.fancybox.min.js"></script>
<script type="text/javascript">
$.LoadingOverlay("show");
</script>
<style type="text/css">	
	body{
		overflow-x:hidden;
		background:#efefef;
	}
	.header { z-index: 999; }
	[data-title] {
		font-size: 16px;
		position: relative;
		cursor: help;
	}  
	[data-title]:hover::before {
		content: attr(data-title);
		position: absolute;
		bottom: -20px;
		padding: 0px;
		color: #2c3545;
		font-size: 14px;
		white-space: nowrap;
	}
	span.truncateTxt {
		width: 30px;
		white-space: nowrap;
		overflow: hidden;
		text-overflow: ellipsis;
		float: left;
	}
	:required  {
		background: url('img/list/star.png') no-repeat !important;
		background-position:right top !important;
		background-color: #fff !important;
	}
	.required  {
		background: url('img/list/star.png') no-repeat !important;
		background-position:right top !important;
		background-color: #fff !important;
	}
</style>
<form method="post" class="form-inline" name="add_sim_linear_multiple_details_form" id="add_sim_linear_multiple_details_form">
	<input type="hidden" name="update_open_response_sim" value="1" />
    <input type="hidden" name="sim_id" id="sim_id" value="<?php echo $sim_data['scenario_id'] ?>" />
    <input type="hidden" name="gettype" value="<?php echo $gettype ?>" />
    <input type="hidden" name="character_type" id="character_type" value="" />
    <input type="hidden" name="submit_type" id="submit_type" />
    <input type="hidden" name="ques_id" id="ques_id" value="<?php echo $ques_id; ?>" />
    <input type="hidden" name="question_type" id="question_type" value="<?php echo ( ! empty($qresult['question_type'])) ? $qresult['question_type'] : 9 ?>" />
    <input type="hidden" name="location" id="location" value="<?php echo $db->getBaseUrl($db->getFile().'?add_data=true&sim_id='. $sim_id); ?>" />
    <div class="sim-side-menu">
        <ul class="side-menu-list nav nav-tabs">
			<li class="Add_Web <?php echo (empty($ques_id)) ? 'active' : ''; ?>"><a data-toggle="tab" href="#Add_Web"><div class="tooltipmenu"><img class="svg addScenario" src="img/list/Add_Learning.svg"/><span class="tooltiptext">Add Learning</span></div></a></li>
			<li class="Add_Scenario"><a data-toggle="tab" href="#Add_Scenario"><div class="tooltipmenu"><img class="svg addScenario" src="img/list/add_Scenario.svg"/><span class="tooltiptext">Add Scenario</span></div></a></li>
            <li class="image_menu"><a data-toggle="tab" href="#image_menu" onclick="openNav1()"><div class="tooltipmenu"><img class="svg add_bg_w" src="img/list/add_background.svg"/><span class="tooltiptext">Add Backgrounds</span></div></a></li>
            <li class="char_menu"><a data-toggle="tab" href="#char_menu" onclick="openNav2()"><div class="tooltipmenu"><img class="svg add_char_w" src="img/list/ad_character.svg"/><span class="tooltiptext">Add Character</span></div></a></li>
            <li class="Add_Branding"><a data-toggle="tab" href="#Add_Branding"><div class="tooltipmenu"><img class="detail_w svg" src="img/list/add_branding.svg"/><span class="tooltiptext">Add Branding</span></div></a></li>
			<li class="main_menu"><a data-toggle="tab" href="#main_menu"><div class="tooltipmenu"><img class="svg add_qa" src="img/list/add_detail.svg"/><span class="tooltiptext">Simulation Details</span></div></a></li>
			<li class="QUeS_menu main_menu <?php echo ( ! empty($ques_id)) ? 'active' : ''; ?>"><a data-toggle="tab" href="#questiontamp"><div class="tooltipmenu"><img class="svg add_qa" src="img/list/add_question1.svg"/><span class="tooltiptext">Question Template</span></div></a></li>
        </ul>
    </div>
    <div class="question_tree_banner tab-content clearfix">
		<!--Add-Web-Object-->
		<div class="bg_selection clearfix tab-pane fade <?php echo (empty($ques_id)) ? 'in active' : ''; ?>" id="Add_Web">
			<div class="question_tree_menu NoBorder" id="question_tree_menu1">
				<div class="Simulation_det stopb clearfix">
					<h3>Add Learning</h3>
					<div class="previewQus Sbtn">
						<a href="<?php echo $preview ?>" target="_blank" title="Preview"><img class="img-fluid" src="img/qus_icon/preview.svg"></a>
					</div>
			    </div>
            	<div class="Group_managment">
					<div class="container">
						<div class="row">
							<div class="col-sm-12">
								<div class="usermanage_main">
									<div class="usermanage-form">
										<div class="row">
											<div class="tille">
												<div class="add_page_option appendSCE">
													<div class="form-group">
														<input type="text" name="web_object_title" class="form-control" placeholder="Title name" autocomplete="off" value="<?php echo ( ! empty($sim_data['web_object_title'])) ? $sim_data['web_object_title'] : ''; ?>" />
													</div>
												</div>
											</div>
											<div class="tilleobj">
												<div class="addsce-img">
													<div class="adimgbox">
														<div class="form-group">
															<input type="file" name="web_file" id="web_file" class="form-control file" />
															<input type="text" class="form-control controls" disabled placeholder="Select Web Object file" value="<?php echo $web_object; ?>" />
															<input type="hidden" name="web_object" id="web_object" value="<?php echo $web_object; ?>" />
															<input type="hidden" name="web_object_type" id="web_object_type" value="application" />
															<div class="browsebtn browsebtntmp">
																<span id="WbDelete" <?php echo ( ! empty($web_object)) ? '' : 'style="display:none"'; ?>>
																	<a href="javascript:void(0);" data-wb="<?php echo ( ! empty($web_object)) ? $web_object : ''; ?>" class="delete_wb" title="Delete File"><i class="fa fa-times" aria-hidden="true"></i></a>
																</span>
															</div>
														</div>
													</div>
													<div class="adimgbox browsebtn"><button class="browse btn btn-primary" type="button">Browse</button></div>
													<div class="adimgbox browsebtn"><button type="button" name="upload_web" id="upload_web" class="btn btn-primary">Upload</button></div>
												</div>
											</div>
                                         </div>
									</div>
								</div>
							</div>
						</div>
					</div>
                </div>
            </div>
        </div>
		<!--Add-Simulation-->
        <div class="bg_selection clearfix tab-pane fade" id="Add_Scenario">
            <div class="question_tree_menu NoBorder" id="question_tree_menu1">
				<div class="Simulation_det stopb clearfix">
					<h3>Add Scenario</h3>
					<div class="previewQus Sbtn">
						<a href="<?php echo $preview ?>" target="_blank" title="Preview"><img class="img-fluid" src="img/qus_icon/preview.svg"></a>
					</div>
			    </div>
            	<div class="Group_managment">
					<div class="container">
						<div class="row">
							<div class="col-sm-12">
								<div class="usermanage_main">
									<div class="usermanage-form">
										<div class="row">
                                        	<?php if ($gettype != 'text only' && $gettype != 'web object'): ?>
                                            <div class="addsce-img">
                                            	<div class="adimgbox">
                                                    <div class="form-group">
                                                        <input type="file" name="file" id="file" class="form-control file">
                                                        <input type="text" id="scenario_media" class="form-control controls" disabled placeholder="Select <?php echo $gettype; ?> file" value="<?php echo $upload_file_name; ?>">
                                                        <input type="hidden" name="scenario_media_file" id="scenario_media_file" value="<?php echo $upload_file_name; ?>" />
                                                        <input type="hidden" name="scenario_media_file_type" id="scenario_media_file_type" value="<?php echo $gettype; ?>" />														
														<div class="browsebtn browsebtntmp">
															<span id="splashImg"><?php if ( ! empty($upload_file_name)) : ?><a href="<?php echo $uploadpath . $upload_file_name ?>" target="_blank" title="View Scenario Media File"><i class="fa fa-eye" aria-hidden="true"></i></a><?php endif; ?></span>
															<span id="ImgDelete" <?php echo ( ! empty($upload_file_name)) ? '' : 'style="display:none"'; ?>><a href="javascript:void(0);" data-scenario-media-file="<?php echo ( ! empty($upload_file_name)) ? $upload_file_name : ''; ?>" class="delete_scenario_media_file" title="Delete File"><i class="fa fa-times" aria-hidden="true"></i></a></span>
														</div>
                                                    </div>
                                                </div>
                                                <div class="adimgbox browsebtn"><button class="browse btn btn-primary" type="button">Browse</button></div>
                                                <div class="adimgbox browsebtn"><button type="button" name="upload" id="upload" class="btn btn-primary">Upload</button></div>
                                            </div>
											<?php elseif ($gettype == 'web object'): ?>
											<div class="addsce-img">
												<div class="adimgbox">
													<div class="form-group">
                                                        <input type="file" name="file" id="file" class="form-control file" />
                                                        <input type="text" class="form-control controls" disabled placeholder="Select <?php echo $gettype; ?> file" value="<?php echo $upload_file_name; ?>" />
                                                        <input type="hidden" name="scenario_media_file" id="scenario_media_file" value="<?php echo $upload_file_name; ?>" />
                                                        <input type="hidden" name="scenario_media_file_type" id="scenario_media_file_type" value="application" />
														<div class="browsebtn browsebtntmp"><span id="ImgDelete" <?php echo ( ! empty($upload_file_name)) ? '' : 'style="display:none"'; ?>><a href="javascript:void(0);" data-wb-file="<?php echo ( ! empty($upload_file_name)) ? $upload_file_name : ''; ?>" class="delete_wb_file" title="Delete File"><i class="fa fa-times" aria-hidden="true"></i></a></span></div>
                                                    </div>
                                                </div>
												<div class="adimgbox browsebtn"><button class="browse btn btn-primary" type="button">Browse</button></div>
                                                <div class="adimgbox browsebtn"><button type="button" name="upload" id="upload" class="btn btn-primary">Upload</button></div>
                                            </div>
                                            <?php endif; ?>
											<div class="user_text addtext">Add Tabs</div>
											<div class="col-sm-12">
												<?php if ( ! empty($sim_page)):
												$pagei = 1; foreach ($sim_page as $pdata): ?>
                                                <div class="plus_page_option">
                                                    <div class="add_page_option appendSCE">
                                                        <div class="form-group">
                                                            <input type="text" name="sim_page_name[]" class="form-control" placeholder="Page name" required="required" value="<?php echo $pdata['sim_page_name'] ?>" />
                                                        </div>
                                                        <div class="form-group">
                                                            <textarea name="sim_page_desc[]" id="sim_page_desc_<?php echo $pagei ?>" class="form-control sim_page_desc"><?php echo $pdata['sim_page_desc'] ?></textarea>
                                                        </div>
                                                        <?php if ($pagei == 1): ?>
                                                         <div class="plus_page"><img src="img/icons/plus.png" title="Add More Page" /></div>
                                                         <?php else: ?>
                                                         <div class="remove minusComp"><img src="img/icons/minus.png" title="Remove Page"></div>
                                                         <?php endif; ?>
                                                    </div>                                               
                                                </div>
                                                <?php $pagei++; endforeach; else: ?>
                                            	<div class="plus_page_option">
                                                    <div class="add_page_option appendSCE">
                                                        <div class="form-group">
                                                            <input type="text" name="sim_page_name[]" class="form-control required" placeholder="Page name" />
                                                        </div>
                                                        <div class="form-group">
                                                            <textarea name="sim_page_desc[]" id="sim_page_desc_0" class="form-control"></textarea>
                                                        </div>
                                                         <div class="plus_page"><img src="img/icons/plus.png" title="Add More Page" /></div>
                                                    </div>                                               
                                                </div>
                                                <?php endif; ?>
											</div>
                                         </div>
									</div>
								</div>
							</div>
						</div>
					</div>
                </div>
            </div>
        </div>
		<!--Add-Backgrounds-->
    	<div class="bg_selection clearfix tab-pane fade in" id="image_menu">
            <div class="question_tree_menu NoBorder" id="question_tree_menu1">
				<div class="Simulation_det stopb clearfix">
					<h3>Add Backgrounds</h3>
					<div class="previewQus Sbtn">
						<a href="<?php echo $preview ?>" target="_blank" title="Preview"><img class="img-fluid" src="img/qus_icon/preview.svg"></a>
					</div>
			    </div>
				<div class="Group_managment Adddbackgroundbg">
				<div class="row back-margin">
					<div class="Radio-box MCQRadio">
                    	<label class="radiostyle">
                        	<input type="radio" name="bgoption" value="1" <?php echo ( ! empty($sim_data['bg_color'])) ? 'checked="checked"' : '' ?> />
                            <span class="radiomark white"></span>
                            <div class="user_text">Add background color</div>
                       </label>
                    </div>
					<div class="branding-flex colorbgbrand">
                    	<span class="colorb">Color</span>
                        <input class="type-color-on-page" name="bg_color" id="bg_color" />
                    </div>
				</div>
				<div class="row back-margin">
                    <div class="Radio-box MCQRadio">
                        <label class="radiostyle">
                        	<input type="radio" name="bgoption" value="2" <?php echo ( ! empty($sim_data['bg_own_choice'])) ? 'checked="checked"' : '' ?> />
                        	<span class="radiomark white"></span>
                        	<div class="user_text">Add background of your own choice</div>
                        </label>
                    </div>
                    <div class="addsce-img bgsi">
                        <!--div class="Scen-preview bgsi"><img class="sce-img own-bg-prev" src="<?php echo ( ! empty($sim_data['bg_own_choice'])) ? $uploadpath . $sim_data['bg_own_choice'] : 'img/scenario-img.png' ?>" /></div-->
                        <div class="imgpath bgbtn">
                            <div class="form-group">
                                <input type="file" name="file" id="bg_file" class="form-control file">
                                <input type="text" class="form-control controls" value="<?php echo ( ! empty($sim_data['bg_own_choice'])) ? $sim_data['bg_own_choice'] : '' ?>" disabled placeholder="Select File" />
                                <input type="hidden" name="scenario_bg_file" id="scenario_bg_file" value="<?php echo ( ! empty($sim_data['bg_own_choice'])) ? $sim_data['bg_own_choice'] : '' ?>" />
                             </div>
                        </div>
                        <div class="browsebtn bgbtn">
                        	<a href="javascript:void(0);" style=" <?php echo ( ! empty($sim_data['bg_own_choice'])) ? '' : 'display:none' ?> " data-scenario-bg-file="<?php echo ( ! empty($sim_data['bg_own_choice'])) ? $sim_data['bg_own_choice'] : '' ?>" class="delete_scenario_bg_file" title="Delete File"><i class="fa fa-times" aria-hidden="true"></i></a>
                        </div>
                        <div class="browsebtn bgbtn"><button class="browse btn btn-primary" type="button">Browse & Select</button></div>
                        <div class="browsebtn bgbtn"><button type="button" name="upload" id="uploadBG" class="btn btn-primary">Upload</button></div>
                    </div>
				</div>
				<div class="row back-margin">
                    <div class="Radio-box MCQRadio">
                        <label class="radiostyle">
                        <input type="radio" name="bgoption" value="3" <?php echo ( ! empty($sim_data['sim_back_img'])) ? 'checked="checked"' : '' ?> />
                            <span class="radiomark white"></span>
                            <div class="user_text">Add background Library</div>
                        </label>
                   </div>
				</div>
                <input type="hidden" id="get_bg_img" value="<?php echo $sim_data['sim_back_img'] ?>" />
                <div class="checkbox bluecheckbox BGIMG"></div>
               </div>
			</div>
        </div>
        <!--Add-Character-->
        <div class="char_bg_selection clearfix tab-pane fade" id="char_menu">
            <div class="question_tree_menu NoBorder" id="question_tree_menu2">
				<div class="Simulation_det stopb clearfix">
					<h3>Add Character</h3>
					<div class="previewQus Sbtn">
						<a href="<?php echo $preview ?>" target="_blank" title="Preview"><img class="img-fluid" src="img/qus_icon/preview.svg"></a>
					</div>
			    </div>	
				<div class="Group_managment">				
                    <ul class="nav-char-tab mb-3">
                        <li class="checkbox radiobtn">
                            <div class="Radio-box">
                            <label class="radiostyle">
                            <input type="radio" class="NoCharTab" name="radio-group" value="1" / >
                            <span class="radiomark SelC"></span></label>
                            <label for="test1">No Character</label></div>
                        </li>
                        <li class="checkbox radiobtn">
                            <div class="Radio-box">
                            <label class="radiostyle">
                            <input type="radio" class="OneCharTab" name="radio-group" value="2" <?php echo ( ! empty($sim_data['sim_char_img']) || ! empty($sim_data['char_own_choice'])) ? 'checked="checked"' : ''; ?> />
                            <span class="radiomark SelC"></span></label>
                            <label for="test2">One Character</label></div>
                        </li>
                        <li class="checkbox radiobtn">
                            <div class="Radio-box">
                            	<label class="radiostyle">
                            	<input type="radio" class="TwoCharTab" name="radio-group" <?php echo ( ! empty($sim_data['sim_char_left_img']) || ! empty($sim_data['sim_char_right_img'])) ? 'checked="checked"' : ''; ?> /><span class="radiomark SelC"></span></label>
                            	<label for="test3">Two Characters</label>
                            </div>
                        </li>
                    </ul>
                    <div class="row back-margin onec">
                        <div class="Radio-box MCQRadio">
                        	<label class="radiostyle">
                            	<input type="radio" name="charoption" value="1" <?php echo ( ! empty($sim_data['char_own_choice'])) ? 'checked="checked"' : ''; ?>>
                                <span class="radiomark white"></span>
                                <div class="user_text">Add your own character</div>
                           	</label>
                        </div>
                        <div class="addsce-img charboxm">
                            <!--div class="Scen-preview"><div class="Scen-preview bgsi"><img class="sce-imgchar" src="<?php echo ( ! empty($sim_data['char_own_choice'])) ? $uploadpath . $sim_data['char_own_choice'] : 'img/charbg.png' ?>" /></div></div-->
                            <div class="imgpath charbtn">
                                <div class="form-group">
                                    <input type="file" name="file" id="file_char" class="form-control file">
                                    <input type="text" class="form-control controls" value="<?php echo ( ! empty($sim_data['char_own_choice'])) ? $sim_data['char_own_choice'] : '' ?>" disabled placeholder="Select File" />
                                    <input type="hidden" name="scenario_char_file" id="scenario_char_file" value="<?php echo ( ! empty($sim_data['char_own_choice'])) ? $sim_data['char_own_choice'] : '' ?>" />
                                </div>
                            </div>
                            <div class="browsebtn charbtn">
                                <a href="javascript:void(0);" style=" <?php echo ( ! empty($sim_data['char_own_choice'])) ? '' : 'display:none' ?> " data-scenario-char-file="<?php echo ( ! empty($sim_data['char_own_choice'])) ? $sim_data['char_own_choice'] : '' ?>" class="delete_scenario_char_file" title="Delete File"><i class="fa fa-times" aria-hidden="true"></i></a>
                            </div>
                            <div class="browsebtn charbtn"><button class="browse btn btn-primary" type="button">Browse & Select</button></div>
                            <div class="browsebtn charbtn"><button type="button" name="upload" id="uploadChar" class="btn btn-primary">Upload</button></div>
                        </div>
                    </div>
                    <div class="tab-content">
                        <div class="row back-margin onec">
                            <div class="Radio-box MCQRadio">
                            	<label class="radiostyle">
                                	<input type="radio" name="charoption" value="2" <?php echo ( ! empty($sim_data['sim_char_img'])) ? 'checked="checked"' : ''; ?> <?php echo ( ! empty($sim_data['sim_char_left_img']) || ! empty($sim_data['sim_char_right_img'])) ? 'checked="checked"' : ''; ?> />
                                    <span class="radiomark white"></span>
                                    <div class="user_text">Add character form the Library</div>
                               	</label>
                            </div>
                        </div>
                        <div class="OneChar">
                            <input type="hidden" id="get_sim_char_img" value="<?php echo $sim_data['sim_char_img'] ?>"/>
                            <div class="checkbox radiobtn bluecheckbox OneCharIMG"></div>
                        </div>
                        <div class="TwoChar">
                            <input type="hidden" id="sim_char_left_img" value="<?php echo $sim_data['sim_char_left_img'] ?>"/>
                            <input type="hidden" id="sim_char_right_img" value="<?php echo $sim_data['sim_char_right_img'] ?>"/>
                            <div class="checkbox bluecheckbox TwoCharIMG"></div>
                        </div>
                    </div>
            	</div>
			</div>
        </div>
        <!--Add-Branding-->
		<div class="main tab-pane fade" id="Add_Branding">
            <div class="main_sim question_tree_menu">
                <div class="main_sim_scroll">
					<div class="Simulation_det stopb clearfix">
						<h3>Adding Branding</h3>
						<div class="previewQus Sbtn">
							<a href="<?php echo $preview ?>" target="_blank" title="Preview"><img class="img-fluid" src="img/qus_icon/preview.svg"></a>
						</div>
					</div>
					<div class="Group_managment">
						<div class="container">
                        	<div class="branding-color">
								<div class="branding-flex brandingbox">
									<div class="coloredittool-box">
										<div class="coloredittool">
											<div class="user_text colorp">Question Background</div>
											<div class="branding-flex"><span class="colorb">Color</span><input class="type-color-on-page" name="ques_bg_color" id="ques_bg_color" /></div>
										</div>
										<div class="coloredittool">
											<div class="user_text colorp">Option Background</div>
											<div class="branding-flex"><span class="colorb">Color</span><input class="type-color-on-page" name="ques_option_bg_color" id="ques_option_bg_color" /></div>
										</div>
										<div class="coloredittool">
											<div class="user_text colorp">Option Hover</div>
											<div class="branding-flex"><span class="colorb">Color</span><input class="type-color-on-page" name="ques_option_hover_color" id="ques_option_hover_color" /></div>
										</div>
										<div class="coloredittool">
											<div class="user_text colorp">Option Selected</div>
											<div class="branding-flex"><span class="colorb">Color</span><input class="type-color-on-page" name="ques_option_select_color" id="ques_option_select_color" /></div>
										</div>
									</div>
									<div class="coloredittool-box">
										<div class="coloredittool">
											<div class="user_text colorp">Submit button Background</div>
											<div class="branding-flex"><span class="colorb">Color</span><input class="type-color-on-page" name="btn_bg_color" id="btn_bg_color" /></div>
										</div>
										<div class="coloredittool">
											<div class="user_text colorp">Submit button Hover</div>
											<div class="branding-flex"><span class="colorb">Color</span><input class="type-color-on-page" name="btn_hover_color" id="btn_hover_color" /></div>
										</div>
										<div class="coloredittool">
											<div class="user_text colorp">Submit button Selected</div>
											<div class="branding-flex"><span class="colorb">Color</span><input class="type-color-on-page" name="btn_select_color" id="btn_select_color" /></div>
										</div>
									</div>
								</div>
							</div>
							<div class="user_text fonts">Font</div>
							<div class="branding-font">
								<div class="branding-flex">
                                	<span class="colorb">Font Type</span>
									<input id="font_type" name="font_type" type="text" />
									<?php if  ( ! empty($sim_brand['font_type'])) : ?>
									<div class="col-sm-2"><button class="cl_ftype btn btn-primary" type="button">Set Default Font Type</button></div>
									<?php endif; ?>
								</div>
								<div class="branding-flex">
                                	<span class="colorb">Font Color</span>
                                    <input class="type-color-on-page" name="font_color" id="font_color" />
                                </div>
                                <div class="branding-flex"><span class="colorb">Font Size</span>
                                  <select name="font_size" id="font_size" class="form-control">
								    <option selected="selected" value="0">Select font size</option>
                                  	<option value="10px" <?php echo ( ! empty($sim_brand['font_size']) && $sim_brand['font_size'] == '10px') ? 'selected="selected"' : '' ?>>10px</option>
                                    <option value="12px" <?php echo ( ! empty($sim_brand['font_size']) && $sim_brand['font_size'] == '12px') ? 'selected="selected"' : '' ?>>12px</option>
                                    <option value="16px" <?php echo ( ! empty($sim_brand['font_size']) && $sim_brand['font_size'] == '16px') ? 'selected="selected"' : '' ?>>16px</option>
                                    <option value="18px" <?php echo ( ! empty($sim_brand['font_size']) && $sim_brand['font_size'] == '18px') ? 'selected="selected"' : '' ?>>18px</option>
                                    <option value="20px" <?php echo ( ! empty($sim_brand['font_size']) && $sim_brand['font_size'] == '20px') ? 'selected="selected"' : '' ?>>20px</option>
                                    <option value="24px" <?php echo ( ! empty($sim_brand['font_size']) && $sim_brand['font_size'] == '24px') ? 'selected="selected"' : '' ?>>24px</option>
                                    <option value="26px" <?php echo ( ! empty($sim_brand['font_size']) && $sim_brand['font_size'] == '26px') ? 'selected="selected"' : '' ?>>26px</option>
                                  </select>
                              </div>
							</div>
						</div>
					</div>
                </div>
            </div>
        </div>
        <!--Add-Sim-Details-->
        <div class="main tab-pane fade" id="main_menu">
            <div class="main_sim question_tree_menu">              
                <div class="Simulation_det stopb clearfix">
                    <h3>Simulation Details</h3>
                    <div class="previewQus Sbtn">
                        <a href="<?php echo $preview ?>" target="_blank" title="Preview"><img class="img-fluid" src="img/qus_icon/preview.svg"></a>
                    </div>
                </div>
                <div class="Group_managment CreateSIMCOMPTBAN">
					<div class="row" style="margin-top: 20px;">
						<div class="col-sm-12">
							<div class="form-group branching ">
								<label class="branching-text">Simulation Name</label>
								<input type="text" name="sim_title" id="sim_title" placeholder="Simulation Name" value="<?php echo $sim_data['Scenario_title'] ?>" class="form-control required" />
							</div>
							<div class="form-group branching">
								<label class="branching-text">Duration(min)</label>
								<input type="text" class="form-control question-box required" name="sim_duration" id="sim_duration" onkeypress="return isNumberKey(event);" autocomplete="off" value="<?php echo $sim_data['duration'] ?>" />
							</div>
							<div class="form-group branching">
								<label class="branching-text">Passing Score(%)</label>
								<input type="text" class="form-control question-box required" name="passing_marks" id="passing_marks" onkeypress="return isNumberKey(event);" autocomplete="off" value="<?php echo $sim_data['passing_marks'] ?>" />
							</div>
						</div>
					</div>
					<?php $sim_cover_img  = '';
					if ( ! empty($sim_data['sim_cover_img']) && file_exists($root_path . $sim_data['sim_cover_img'])):
						$sim_cover_img = $sim_data['sim_cover_img'];
					endif; ?>
					<div class="edit-sim-box editSimAUTH">
						<div class="Simulation_det VreACom" style="margin-bottom: 20px;">
							<h3>Add Cover Image</h3>
						</div>
						<div class="addsce-img">
							<div class="imgpath imgpathAUT">
								<div class="form-group">
									<input type="file" name="upload_cover_img_file" id="upload_cover_img_file" class="form-control file" />
									<input type="text" class="form-control controls" disabled placeholder="Select File" />
									<input type="hidden" name="scenario_cover_file" id="scenario_cover_file" value="<?php echo ( ! empty($sim_cover_img)) ? $sim_cover_img : ''; ?>" />
									<input type="hidden" name="cover_img_file_type" id="cover_img_file_type" value="image" />
									<div class="sim_cover_img_data"><a class="view_assets" data-src="<?php echo $path . $sim_cover_img; ?>" href="javascript:;"><?php echo $sim_cover_img; ?></a></div>
								</div>
							</div>
							<div class="browsebtn">
								<div class="cover_image_container" <?php echo ( ! empty($sim_cover_img)) ? '' : 'style="display:none;"'; ?>>
									<a href="javascript:void(0);" data-cover-img-file="<?php echo ( ! empty($sim_cover_img)) ? $sim_cover_img : ''; ?>" class="delete_cover_img_file" title="Delete File"><i class="fa fa-times" aria-hidden="true"></i></a>
								</div>
							</div>
							<div class="browsebtn"><button class="browse btn btn-primary upload_cover_btn" type="button" <?php echo ( ! empty($sim_cover_img)) ? 'disabled' : ''; ?>>Browse & Select</button></div>
							<div class="browsebtn"><button type="button" name="upload_cover_img" id="upload_cover_img" class="btn btn-primary" <?php echo ( ! empty($sim_cover_img)) ? 'disabled' : ''; ?>>Upload</button></div>
						</div>
					</div>
					
					<div class="Simulation_det VreACom" style="margin-top: 20px;">
						<h3>Create Competency</h3>
					</div>
					<div class="row" style="margin-top: 50px;">
					<?php if ( ! empty($sim_com)): ?>
						<div class="plus_comp_option linaer">
						<?php $i = 1; if ( ! empty($sim_com['comp_col_1'])): ?>
						<div class="add_comp_option">
							<div class="form-group">
								<input type="text" placeholder="Competency Name" class="form-control" name="competency_name_1" value="<?php echo $sim_com['comp_col_1'] ?>" required="required" autocomplete="off" />
							</div>
							<div class="form-group labelbox">
								<label>Competency Score</label>
								<input type="text" class="form-control question-box" name="competency_score_1" onkeypress="return isNumberKey(event);" value="<?php echo $sim_com['comp_val_1'] ?>" required="required" />
							</div>
							<div class="form-group labelbox">
								<label>Weightage</label>
								<input type="text" class="form-control question-box" name="weightage_1" onkeypress="return isNumberKey(event);" value="<?php echo $sim_com['weightage_1'] ?>" required="required" />
							</div>
							<div class="plus_comp"><img src="img/icons/plus.png" title="Add More"></div>
						</div>
						<?php endif;
						if ( ! empty($sim_com['comp_col_2'])): $i += 1; ?>
						<div class="add_comp_option">
							<div class="form-group">
								<input type="text" placeholder="Competency Name" class="form-control" name="competency_name_2" value="<?php echo $sim_com['comp_col_2'] ?>" required="required" autocomplete="off" />
							</div>
							<div class="form-group labelbox">
								<label>Competency Score</label>
								<input type="text" class="form-control question-box" name="competency_score_2" onkeypress="return isNumberKey(event);" value="<?php echo $sim_com['comp_val_2'] ?>" required="required" />
							</div>
							<div class="form-group labelbox">
								<label>Weightage</label>
								<input type="text" class="form-control question-box" name="weightage_2" onkeypress="return isNumberKey(event);" value="<?php echo $sim_com['weightage_2'] ?>" required="required" />
							</div>
							<div class="remove minusComp"><img src="img/icons/minus.png" title="Remove"></div>
						</div>
						<?php endif;
						if ( ! empty($sim_com['comp_col_3'])): $i += 1; ?>
						<div class="add_comp_option">
							<div class="form-group">
								<input type="text" placeholder="Competency Name" class="form-control" name="competency_name_3" value="<?php echo $sim_com['comp_col_3'] ?>" required="required" autocomplete="off" />
							</div>
							<div class="form-group labelbox">
								<label>Competency Score</label>
								<input type="text" class="form-control question-box" name="competency_score_3" onkeypress="return isNumberKey(event);" value="<?php echo $sim_com['comp_val_3'] ?>" required="required" />
							</div>
							<div class="form-group labelbox">
								<label>Weightage</label>
								<input type="text" class="form-control question-box" name="weightage_3" onkeypress="return isNumberKey(event);" value="<?php echo $sim_com['weightage_3'] ?>" required="required" />
							</div>
							<div class="remove minusComp"><img src="img/icons/minus.png" title="Remove"></div>
						</div>
						<?php endif;
						if ( ! empty($sim_com['comp_col_4'])): $i += 1; ?>
						<div class="add_comp_option">
							<div class="form-group">
								<input type="text" placeholder="Competency Name" class="form-control" name="competency_name_4" value="<?php echo $sim_com['comp_col_4'] ?>" required="required" autocomplete="off" />
							</div>
							<div class="form-group labelbox">
								<label>Competency Score</label>
								<input type="text" class="form-control question-box" name="competency_score_4" onkeypress="return isNumberKey(event);" value="<?php echo $sim_com['comp_val_4'] ?>" required="required" />
							</div>
							<div class="form-group labelbox">
								<label>Weightage</label>
								<input type="text" class="form-control question-box" name="weightage_4" onkeypress="return isNumberKey(event);" value="<?php echo $sim_com['weightage_4'] ?>" required="required" />
							</div>
							<div class="remove minusComp"><img src="img/icons/minus.png" title="Remove"></div>
						</div>
						<?php endif;
						if ( ! empty($sim_com['comp_col_5'])): $i += 1; ?>
						<div class="add_comp_option">
							<div class="form-group">
								<input type="text" placeholder="Competency Name" class="form-control" name="competency_name_5" value="<?php echo $sim_com['comp_col_5'] ?>" required="required" autocomplete="off" />
							</div>
							<div class="form-group labelbox">
								<label>Competency Score</label>
								<input type="text" class="form-control question-box" name="competency_score_5" onkeypress="return isNumberKey(event);" value="<?php echo $sim_com['comp_val_5'] ?>" required="required" />
							</div>
							<div class="form-group labelbox">
								<label>Weightage</label>
								<input type="text" class="form-control question-box" name="weightage_5" onkeypress="return isNumberKey(event);" value="<?php echo $sim_com['weightage_5'] ?>" required="required" />
							</div>
							<div class="remove minusComp"><img src="img/icons/minus.png" title="Remove"></div>
						</div>
						<?php endif;
						if ( ! empty($sim_com['comp_col_6'])): $i += 1; ?>
						<div class="add_comp_option">
							<div class="form-group">
								<input type="text" placeholder="Competency Name" class="form-control" name="competency_name_6" value="<?php echo $sim_com['comp_col_6'] ?>" required="required" autocomplete="off" />
							</div>
							<div class="form-group labelbox">
								<label>Competency Score</label>
								<input type="text" class="form-control question-box" name="competency_score_6" onkeypress="return isNumberKey(event);" value="<?php echo $sim_com['comp_val_6'] ?>" required="required" />
							</div>
							<div class="form-group labelbox">
								<label>Weightage</label>
								<input type="text" class="form-control question-box" name="weightage_6" onkeypress="return isNumberKey(event);" value="<?php echo $sim_com['weightage_6'] ?>" required="required" />
							</div>
							<div class="remove minusComp"><img src="img/icons/minus.png" title="Remove"></div>
						</div>
						<?php endif; ?>
						</div>
						<?php else: ?>
						<div class="plus_comp_option linaer">
							<div class="add_comp_option">
								<div class="form-group">
									<input type="text" placeholder="Competency Name" class="form-control required" name="competency_name_1" autocomplete="off" />
								</div>
								<div class="form-group labelbox">
									<label>Competency Score</label>
									<input type="text" class="form-control question-box required" name="competency_score_1" onkeypress="return isNumberKey(event);" />
								</div>
								<div class="form-group labelbox">
									<label>Weightage</label>
									<input type="text" class="form-control question-box required" name="weightage_1" onkeypress="return isNumberKey(event);" />
								</div>
								<div class="plus_comp"><img src="img/icons/plus.png" title="Add More"></div>
							</div>
						</div>
						<?php endif; ?>
					</div>
            	</div>
           </div>
        </div>
        <!--Question-Temp-->
		<div class="Questio-template OpenRsponcetempl tab-pane fade <?php echo ( ! empty($ques_id)) ? 'in active' : ''; ?>" id="questiontamp">
            <div class="">
                <div class="main_sim_scroll">
                    <div class="Simulation_det stopb clearfix unsetp">
                        <div class="skilQue">
							<?php if ( ! empty($sim_com['comp_col_1']) && ! empty($sim_com['comp_val_1'])): ?>
                            <div class="skills">
                                <label data-title="<?php echo $sim_com['comp_col_1'] ?>"><span class="truncateTxt"><?php echo $sim_com['comp_col_1'] ?></span></label>
                                <input type="text" class="form-control" id="comp_val_1" readonly="readonly" value="<?php echo abs($sim_com['comp_val_1'] - $ques_val1); ?>" />
                            </div>
                            <?php endif; if ( ! empty($sim_com['comp_col_2']) && ! empty($sim_com['comp_val_2'])): ?>
                            <div class="skills">
                                <label data-title="<?php echo $sim_com['comp_col_2'] ?>"><span class="truncateTxt"><?php echo $sim_com['comp_col_2'] ?></span></label>
                                <input type="text" class="form-control" id="comp_val_2" readonly="readonly" value="<?php echo abs($sim_com['comp_val_2'] - $ques_val2); ?>" />
                            </div>
                            <?php endif; if ( ! empty($sim_com['comp_col_3']) && ! empty($sim_com['comp_val_3'])): ?>
                            <div class="skills">
                                <label data-title="<?php echo $sim_com['comp_col_3'] ?>"><span class="truncateTxt"><?php echo $sim_com['comp_col_3'] ?></span></label>
                                <input type="text" class="form-control" id="comp_val_3" readonly="readonly" value="<?php echo abs($sim_com['comp_val_3'] - $ques_val3); ?>" />
                            </div>
                            <?php endif; if ( ! empty($sim_com['comp_col_4']) && ! empty($sim_com['comp_val_4'])): ?>
                            <div class="skills">
                                <label data-title="<?php echo $sim_com['comp_col_4'] ?>"><span class="truncateTxt"><?php echo $sim_com['comp_col_4'] ?></span></label>
                                <input type="text" class="form-control" id="comp_val_4" readonly="readonly" value="<?php echo abs($sim_com['comp_val_4'] - $ques_val4); ?>" />
                            </div>
                            <?php endif; if ( ! empty($sim_com['comp_col_5']) && ! empty($sim_com['comp_val_5'])): ?>
                            <div class="skills">
                                <label data-title="<?php echo $sim_com['comp_col_5'] ?>"><span class="truncateTxt"><?php echo $sim_com['comp_col_5'] ?></span></label>
                                <input type="text" class="form-control" id="comp_val_5" readonly="readonly" value="<?php echo abs($sim_com['comp_val_5'] - $ques_val5); ?>" />
                            </div>
                            <?php endif; if ( ! empty($sim_com['comp_col_6']) && ! empty($sim_com['comp_val_6'])): ?>
                            <div class="skills">
                                <label data-title="<?php echo $sim_com['comp_col_6'] ?>"><span class="truncateTxt"><?php echo $sim_com['comp_col_6'] ?></span></label>
                                <input type="text" class="form-control" id="comp_val_6" readonly="readonly" value="<?php echo abs($sim_com['comp_val_6'] - $ques_val6); ?>" />
                            </div>
                            <?php endif; ?>
                        </div>
                        <div class="previewQus Sbtn">
                            <a href="<?php echo $preview ?>" target="_blank" title="Preview"><img class="img-fluid" src="img/qus_icon/preview.svg"></a>
                        </div>
                    </div>
                    <div id="wrapper">
                    	<div class="width12" id="left-wrapper">
							<div class="topbannericon"><img class="img-fluid closeleftmenuA" src="img/list/left_arrow.svg"></div>
                            <div class="question-list-banner">
                                <!--Start-Load-Question-List-->
                                <ul class="question-list" id="sortable"></ul>
                                <!--End-->
                                <div class="list-icon">
                                    <ul>
                                        <li class="DuPIcon"><img class="img-fluid" src="img/list/duplicate_question.svg"/></li>
                                        <li class="AddIcon"><img class="img-fluid" src="img/list/add_question.svg"/></li>
                                        <li class="DelIcon"><img class="img-fluid" src="img/list/delete_question.svg"/></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                       <?php $ques_type = ( ! empty($qresult['question_type'])) ? $qresult['question_type'] : ''; ?>
                       <div id="page-content-wrapper" class="OpenResp">
                            <div class="container-fluid">
                                <div class="row">
                                    <div class="tab-content">
                                        <div class="Q-left">
											<a href="#list-toggle" id="list-toggle" class="hideQTmp"><img class="close_panel" src="img/list/questiion_list.svg" /></a>
										</div>
                                        <?php 
										#---Questions
										$ques_att = '';
										$qatt_type = '';
										if ( ! empty($qresult['qaudio']) && file_exists($root_path . $qresult['qaudio'])):
											$qatt_type = 'qa';
											$ques_att = $qresult['qaudio'];
										endif;
										
										#--Questions-Assets
										$ques_assets = '';
										$assets_type = '';
										if ( ! empty($qresult['audio']) && file_exists($root_path . $qresult['audio'])):
											$assets_type = 'a';
											$ques_assets = $qresult['audio'];
										elseif ( ! empty($qresult['video']) && file_exists($root_path . $qresult['video'])):
											$assets_type = 'v';
											$ques_assets = $qresult['video'];
										elseif ( ! empty($qresult['screen']) && file_exists($root_path . $qresult['screen'])):
											$assets_type = 's';
											$ques_assets = $qresult['screen'];
										elseif ( ! empty($qresult['image']) && file_exists($root_path . $qresult['image'])):
											$assets_type = 'i';
											$ques_assets = $qresult['image'];
										elseif ( ! empty($qresult['document']) && file_exists($root_path . $qresult['document'])):
											$assets_type = 'd';
											$ques_assets = $qresult['document'];
										endif; ?>
                                        <!--Question-->
                                        <div id="MCQQuestion" class="tab-pane <?php echo ( ! empty($ques_id)) ? 'in active' : ''; ?>">
                                            <div class="container-fluid">
                                                <div class="widthQ100 classic-template educaterAuthTemp">
                                                    <div class="tab-content educater-banner_Qus">
                                                        <div id="match-Q" class="match-Q tab-pane fade in active">
                                                            <div class="quesbox CQtmp">
                                                                <div class="row">
                                                                    <div class="col-sm-12 Questiontmpbanner">
																		<div class="critical_div">
																			<h3 class="qusHeading">QUESTION</h3>
																			<div class="CIRtiCaltext">
																				Critical Question: <input type="checkbox" name="mcq_criticalQ" id="mcq_criticalQ" class="form-control criticalQ" value="1" <?php echo ( ! empty($qresult['critical'])) ? 'checked="checked"' : ''; ?> />
																			</div>
																		</div>
                                                                        <div class="quesbox-flex">
                                                                            <div class="form-group">
                                                                            	<input type="text" name="mcq" id="mcq" class="form-control QuesForm inputtextWrap mcq" placeholder="Click here to enter question" value="<?php echo ( ! empty($qresult['questions'])) ? stripslashes($qresult['questions']) : ''; ?>" />
                                                                                <div class="mcqq_assets_container" <?php echo ( ! empty($ques_att)) ? '' : 'style="display:none;"'; ?>>
                                                                                    <div class="assets_data mcqq_assets_data"><a class="view_assets" data-src="<?php echo $path . $ques_att; ?>" href="javascript:;">	</a></div>
                                                                                    <a href="javascript:void(0);" data-assets="<?php echo $ques_att; ?>" data-assets-id="mcqq_assets" data-input-id="#mcqq_qaudio,#mcqq_rec_audio" data-path="<?php echo $uploadpath ?>" class="delete_assets" title="Delete"><i class="fa fa-times" aria-hidden="true"></i></a>
                                                                                </div>
                                                                                <ul class="QueBoxIcon">
                                                                                    <li class="dropdown">
                                                                                        <button class="btn1 btn-secondary dropdown-toggle mcqq_assets <?php echo ( ! empty($ques_att)) ? 'disabled' : ''; ?>" <?php echo ( ! empty($ques_att)) ? 'disabled="disabled"' : ''; ?> type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img class="img-fluid" src="img/list/audio.svg"></button>
                                                                                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                                                                            <a class="dropdown-item">
                                                                                                <div class="tooltip1 uploadicon">
                                                                                                    <label for="file-input-mcqq-addAudio"><img class="img-fluid" src="img/list/add_audio.svg" /><span class="tooltiptext">Add Audio</span></label>
                                                                                                    <input id="file-input-mcqq-addAudio" data-id="#mcqq_qaudio" data-assets="mcqq_assets" class="uploadAudioFile" type="file" name="mcqq_Audio" />
                                                                                                    <input type="hidden" name="mcqq_qaudio" id="mcqq_qaudio" value="<?php echo ($qatt_type == 'qa') ? $ques_att : ''; ?>" />
                                                                                                </div>
                                                                                            </a>
                                                                                            <a class="dropdown-item">
                                                                                                <div class="tooltip1">
                                                                                                    <img class="img-fluid rec-audio" data-input-id="mcqq_rec_audio" data-assets="mcqq_assets" src="img/list/record_audio.svg" />
                                                                                                    <span class="tooltiptext">Record Audio</span>
                                                                                                </div>
                                                                                                <input type="hidden" name="mcqq_rec_audio" id="mcqq_rec_audio" />
                                                                                             </a>
                                                                                            <a data-toggle="modal" data-target="#mcqqtts" class="dropdown-item">
                                                                                                <div class="tooltip1">
                                                                                                    <img class="img-fluid" src="img/list/text_speech.svg" />
                                                                                                    <span class="tooltiptext">Text to Speech</span>
                                                                                                </div>
                                                                                             </a>
                                                                                         </div>
                                                                                    </li>
                                                                                </ul>
                                                                            </div>
                                                                        </div>
                                                                        <div class="q-icon-banner clearfix">
                                                                            <ul class="QueBoxIcon QueBoxIcon1">
																				<h5>Add assets</h5>
                                                                                <li class="dropdown">
                                                                                    <button class="btn1 btn-secondary dropdown-toggle mcq_assets <?php echo ( ! empty($ques_assets)) ? 'disabled' : ''; ?>" <?php echo ( ! empty($ques_assets)) ? 'disabled="disabled"' : ''; ?> type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img class="img-fluid" src="img/list/audio.svg" /></button>
                                                                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                                                                        <a data-toggle="modal" data-target="#mcqtts" class="dropdown-item">
                                                                                            <div class="tooltip1">
                                                                                                <img class="img-fluid" src="img/list/text_speech.svg">
                                                                                                <span class="tooltiptext">Text to Speech</span>
                                                                                            </div>
                                                                                        </a>
                                                                                        <a class="dropdown-item">
                                                                                            <div class="tooltip1 uploadicon">
                                                                                                <label for="file-input-mcq-addAudio">
                                                                                                    <img class="img-fluid" src="img/list/add_audio.svg" />
                                                                                                    <span class="tooltiptext">Add Audio</span>
                                                                                                </label>
                                                                                                <input id="file-input-mcq-addAudio" data-id="#mcq_audio" data-assets="mcq_assets" class="uploadAudioFile" type="file" name="mcq_Audio"/>
                                                                                                <input type="hidden" name="mcq_audio" id="mcq_audio" value="<?php echo ($assets_type == 'a') ? $ques_assets : ''; ?>" />
                                                                                            </div>
                                                                                        </a>
                                                                                        <a class="dropdown-item">
                                                                                            <div class="tooltip1">
                                                                                                <img class="img-fluid rec-audio" data-input-id="mcq_rec_audio" data-assets="mcq_assets" src="img/list/record_audio.svg" />
                                                                                                <span class="tooltiptext">Record Audio</span>
                                                                                            </div>
                                                                                            <input type="hidden" name="mcq_rec_audio" id="mcq_rec_audio" />
                                                                                         </a>
                                                                                    </div>
                                                                                </li>
                                                                                <li class="dropdown">
                                                                                    <button class="btn1 btn-secondary dropdown-toggle mcq_assets <?php echo ( ! empty($ques_assets)) ? 'disabled' : ''; ?>" <?php echo ( ! empty($ques_assets)) ? 'disabled="disabled"' : ''; ?> type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img class="img-fluid" src="img/list/video.svg"></button>
                                                                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                                                                        <a class="dropdown-item">
                                                                                            <div class="tooltip1 uploadicon">
                                                                                                <label for="file-input-mcq-addVideo">
                                                                                                    <img class="img-fluid" src="img/list/add_video.svg" />
                                                                                                    <span class="tooltiptext">Add Video</span>
                                                                                                </label>
                                                                                                <input id="file-input-mcq-addVideo" data-id="#mcq_video" data-assets="mcq_assets" class="uploadVideoFile" type="file" name="mcq_Video" />
                                                                                                <input type="hidden" name="mcq_video" id="mcq_video" value="<?php echo ($assets_type == 'v') ? $ques_assets : ''; ?>" />
                                                                                            </div>
                                                                                        </a>
                                                                                        <a class="dropdown-item">
                                                                                            <div class="tooltip1">
                                                                                                <img class="img-fluid rec-video" data-input-id="mcq_rec_video" data-assets="mcq_assets" src="img/qus_icon/rec_video.svg" />
                                                                                                <span class="tooltiptext">Record Video</span>
                                                                                             </div>
                                                                                             <input type="hidden" name="mcq_rec_video" id="mcq_rec_video" />
                                                                                        </a>
                                                                                        <a class="dropdown-item">
                                                                                            <div class="tooltip1">
                                                                                                <img class="img-fluid rec-screen" data-input-id="mcq_rec_screen" data-assets="mcq_assets" src="img/list/screen_record.svg" />
                                                                                                <span class="tooltiptext">Record Screen</span>
                                                                                             </div>
                                                                                             <input type="hidden" name="mcq_rec_screen" id="mcq_rec_screen" value="<?php echo ($assets_type == 's') ? $ques_assets : ''; ?>" />
                                                                                        </a>
                                                                                    </div>
                                                                                </li>
                                                                                <li>
                                                                                    <div class="tooltip uploadicon mcq_assets <?php echo ( ! empty($ques_assets)) ? 'disabled' : ''; ?>" <?php echo ( ! empty($ques_assets)) ? 'disabled="disabled"' : ''; ?>>
                                                                                        <label for="file-input-mcq-addImg">
                                                                                            <img class="img-fluid" src="img/list/image.svg" />
                                                                                            <span class="tooltiptext">Add Image</span>
                                                                                        </label>
                                                                                        <input id="file-input-mcq-addImg" data-id="#mcq_img" data-assets="mcq_assets" class="uploadImgFile mcq_assets" type="file" name="mcq_Img" <?php echo ( ! empty($ques_assets)) ? 'disabled="disabled"' : ''; ?>/>
                                                                                        <input type="hidden" name="mcq_img" id="mcq_img" value="<?php echo ($assets_type == 'i') ? $ques_assets : ''; ?>" />
                                                                                    </div>
                                                                                </li>
                                                                                <li>
                                                                                    <div class="tooltip uploadicon mcq_assets <?php echo ( ! empty($ques_assets)) ? 'disabled' : ''; ?>" <?php echo ( ! empty($ques_assets)) ? 'disabled="disabled"' : ''; ?>>
                                                                                        <label for="file-input-mcq-addDoc">
                                                                                            <img class="img-fluid" src="img/list/add_document.svg" />
                                                                                            <span class="tooltiptext">Add Document</span>
                                                                                        </label>
                                                                                        <input id="file-input-mcq-addDoc" data-id="#mcq_doc" data-assets="mcq_assets" class="uploadDocFile mcq_assets" type="file" name="mcq_Doc" <?php echo ( ! empty($ques_assets)) ? 'disabled="disabled"' : ''; ?> />
                                                                                        <input type="hidden" name="mcq_doc" id="mcq_doc" value="<?php echo ($assets_type == 'd') ? $ques_assets : ''; ?>" />
                                                                                    </div>
                                                                                </li>
                                                                            </ul>                                                               
                                                                            <div class="Ques-comp">
                                                                                <h5>Add Competency score</h5>
																				<?php if ( ! empty($sim_com['comp_col_1']) && ! empty($sim_com['comp_val_1'])): ?>
                                                                                <div class="form-group QusScore">
                                                                                    <input type="text" class="form-control mcq" name="mcq_ques_val_1" id="mcq_ques_val_1" onkeypress="return isNumberKey(event);" value="<?php echo $qresult['ques_val_1'] ?>" />
                                                                                </div>
                                                                                <input type="hidden" class="form-control" id="mcq_ques_val_1h" value="<?php echo $qresult['ques_val_1'] ?>" />
                                                                                <?php else: ?>
                                                                                <input type="hidden" class="form-control" name="mcq_ques_val_1" value="0" />
                                                                                <?php endif; ?>
                                                                                
                                                                                <?php if ( ! empty($sim_com['comp_col_2']) && ! empty($sim_com['comp_val_2'])): ?>
                                                                                <div class="form-group QusScore">
                                                                                    <input type="text" class="form-control mcq" name="mcq_ques_val_2" id="mcq_ques_val_2" onkeypress="return isNumberKey(event);" value="<?php echo $qresult['ques_val_2'] ?>" />
                                                                                </div>
                                                                                <input type="hidden" class="form-control" id="mcq_ques_val_2h" value="<?php echo $qresult['ques_val_2'] ?>" />
                                                                                <?php else: ?>
                                                                                <input type="hidden" class="form-control" name="mcq_ques_val_2" value="0" />
                                                                                <?php endif; ?>
                                                                                
                                                                                <?php if ( ! empty($sim_com['comp_col_3']) && ! empty($sim_com['comp_val_3'])): ?>
                                                                                <div class="form-group QusScore">
                                                                                    <input type="text" class="form-control mcq" name="mcq_ques_val_3" id="mcq_ques_val_3" onkeypress="return isNumberKey(event);" value="<?php echo $qresult['ques_val_3'] ?>" />
                                                                                </div>
                                                                                <input type="hidden" class="form-control" id="mcq_ques_val_3h" value="<?php echo $qresult['ques_val_3'] ?>" />
                                                                                <?php else: ?>
                                                                                <input type="hidden" class="form-control" name="mcq_ques_val_3" value="0" />
                                                                                <?php endif; ?>
                                                                                
                                                                                <?php if ( ! empty($sim_com['comp_col_4']) && ! empty($sim_com['comp_val_4'])): ?>
                                                                                <div class="form-group QusScore">
                                                                                    <input type="text" class="form-control mcq" name="mcq_ques_val_4" id="mcq_ques_val_4" onkeypress="return isNumberKey(event);" value="<?php echo $qresult['ques_val_4'] ?>" />
                                                                                </div>
                                                                                <input type="hidden" class="form-control" id="mcq_ques_val_4h" value="<?php echo $qresult['ques_val_4'] ?>" />
                                                                                <?php else: ?>
                                                                                <input type="hidden" class="form-control" name="mcq_ques_val_4" value="0" />
                                                                                <?php endif; ?>
                                                                                
                                                                                <?php if ( ! empty($sim_com['comp_col_5']) && ! empty($sim_com['comp_val_5'])): ?>
                                                                                <div class="form-group QusScore">
                                                                                    <input type="text" class="form-control mcq" name="mcq_ques_val_5" id="mcq_ques_val_5" onkeypress="return isNumberKey(event);" value="<?php echo $qresult['ques_val_5'] ?>" />
                                                                                </div>
                                                                                <input type="hidden" class="form-control" id="mcq_ques_val_5h" value="<?php echo $qresult['ques_val_5'] ?>" />
                                                                                <?php else: ?>
                                                                                <input type="hidden" class="form-control" name="mcq_ques_val_5h" value="0" />
                                                                                <?php endif; ?>
                                                                                
                                                                                <?php if ( ! empty($sim_com['comp_col_6']) && ! empty($sim_com['comp_val_6'])): ?>
                                                                                <div class="form-group QusScore">
                                                                                    <input type="text" class="form-control mcq" name="mcq_ques_val_6" id="mcq_ques_val_6" onkeypress="return isNumberKey(event);" value="<?php echo $qresult['ques_val_6'] ?>" />
                                                                                </div>
                                                                                <input type="hidden" class="form-control" id="mcq_ques_val_6h" value="<?php echo $qresult['ques_val_6'] ?>" />
                                                                                <?php else: ?>
                                                                                <input type="hidden" class="form-control" name="mcq_ques_val_6" value="0" />
                                                                                <?php endif; ?>
                                                                            </div>
                                                                        </div>
                                                                        <div class="mcq_assets_container" <?php echo ( ! empty($ques_assets)) ? '' : 'style="display:none;"'; ?>>
                                                                            <div class="mcq_assets_data"><a class="view_assets" data-src="<?php echo $path . $ques_assets; ?>" href="javascript:;"><?php echo $ques_assets; ?></a></div>
                                                                            <a href="javascript:void(0);" data-assets="<?php echo $ques_assets ?>" data-assets-id="mcq_assets" data-input-id="#mcq_audio,#mcq_rec_audio,#mcq_video,#mcq_rec_video,#mcq_rec_screen,#mcq_img,#mcq_doc" data-path="<?php echo $uploadpath ?>" class="delete_assets" title="Delete"><i class="fa fa-times" aria-hidden="true"></i></a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-popup draggable Ques-pop-style modal" id="mcqqtts">
                                                                <div class="popheading">Insert Text to Speech</div>
                                                                <div class="textarea">
                                                                    <textarea type="text" class="form-control1 inputtextWrap" name="mcqq_text_to_speech" id="mcqq_text_to_speech"><?php echo ( ! empty($qresult['qspeech_text'])) ? stripslashes($qresult['qspeech_text']) : ''; ?></textarea>
                                                                </div>
                                                                <div class="modal-footer">
                                                                    <button type="button" class="btn1 submitbtn1" data-dismiss="modal">Insert</button>
                                                                    <button type="button" class="btn1 submitbtn1" onclick="clearData('mcqq_text_to_speech');">Clear</button>
                                                                    <button type="button" class="btn1 submitbtn1" data-dismiss="modal">Cancel</button>
                                                                </div>
                                                            </div>
                                                            <div class="form-popup draggable Ques-pop-style modal" id="mcqtts">
                                                                <div class="popheading">Insert Text to Speech</div>
                                                                <div class="textarea">
                                                                    <textarea type="text" class="form-control1 inputtextWrap" name="mcq_text_to_speech" id="mcq_text_to_speech"><?php echo ( ! empty($qresult['speech_text'])) ? stripslashes($qresult['speech_text']) : ''; ?></textarea>
                                                                </div>
                                                                <div class="modal-footer">
                                                                    <button type="button" class="btn1 submitbtn1" data-dismiss="modal">Insert</button>
                                                                    <button type="button" class="btn1 submitbtn1" onclick="clearData('mcq_text_to_speech');">Clear</button>
                                                                    <button type="button" class="btn1 submitbtn1" data-dismiss="modal">Cancel</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>		
				</div>
			</div>
		</div>
	</div>
    <div class="sim_bluebanner">
    	<ul class="Ques_linear_save">
        	<li><button type="submit" class="submitques update_next"><img src="img/list/save-t.svg" />SAVE & CONTINUE</button></li>
            <li><button type="submit" class="submitques update_close"><img src="img/list/close.svg" />SAVE & CLOSE</button></li>
        </ul> 
    </div>
</form>

<div id="load_popup_modal_show" class="modal fade" tabindex="-1"></div>

<script src="content/js/jquery-ui-1.12.1/jquery-ui.js"></script>
<script src="content/js/jquery.fontselect.js"></script>
<script src="content/js/competency-validate.js"></script>
<script type="text/javascript">
$(function(){
	$(".draggable").draggable();
});

var assets_path = '<?php echo $path; ?>';

$('body').on('click', '.view_assets', function(e){
    e.preventDefault();
    var to = $(this).attr('data-src');
    $.fancybox.open({
        type: 'iframe',
        src: to,
        toolbar  : false,
        smallBtn : true,
		closeExisting: false,
		scrolling: 'no',
        iframe : {
            preload : true,
            scrolling: 'no',
			css:{
					width:'100%',
					height:'100%'
				}
        }
    });
});

$(".inputtextWrap").hover(function() {
	$(this).attr('title', $(this).val());
}, function() {
	$(this).css('cursor','auto');
});

/* Set Default Font Type */
$('.cl_ftype').click(function () {
	$('#font_type').trigger('setFont', '');
});

function isNumberKey(evt) {
	var charCode = (evt.which) ? evt.which : evt.keyCode
	return !(charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57));
}

function clearData(id){
	$('#'+ id).val('');
}

$("#sortable").sortable({
	update: function() {
		serial = $('#sortable').sortable('serialize');
		$.LoadingOverlay("show");
		$.ajax({
            data: serial + '&qorder=true&sim_id=<?php echo $sim_id ?>',
            type: 'POST',
            url: 'includes/process.php',
			success:function(resdata) {
				getQuesList();
				$.LoadingOverlay("hide");
			},error: function() {
				$.LoadingOverlay("hide");
				swal({text: 'Oops. something went wrong please try again.', buttons: false, icon: "error", timer: 2000 });
			}
        });
	}
});

/* Get Questions List */
function getQuesList() {
	$.ajax({
		type: 'GET',
		url: 'includes/process.php',
		data: 'getQuesList=true&sim_id=<?php echo $sim_id ?>',
		success:function(resdata) {
			$.LoadingOverlay("hide");
			var res = $.parseJSON(resdata);
			if (res.success == true) {
				var base 	= $('#location').val();
				var current	= window.location.href;
				var qurl 	= '';
				var qclass	= '';
				var qno		= 1;
				$('.question-list').empty();
				$.each(res.data, function (key, val) {
					qurl = base + '&ques_id='+ val.ques_id;
					if (current == qurl){
						qclass = 'active';
					} else {
						qclass = '';
					}
					var qlist = '<label class="ui-state-default '+ qclass +'" id="question-'+ val.qid +'"><span class="QNum">'+ qno +'</span><input type="checkbox" name="ques_no[]" class="form-control ques_no ui-state-CheCK" value="'+ val.qid +'" /><a class="ui-state-text" href="'+ qurl +'"><img class="updownQues" src="img/list/up-down.svg"><li>'+ val.qname +'</li></a></label>';
					$('.question-list').append(qlist);
					qno++;
				});
			} else {
				swal({text: 'Questions list not load please try again later.', buttons: false, icon: "error", timer: 2000 });
			}
		},error: function() {
			$.LoadingOverlay("hide");
			swal({text: 'Oops. something went wrong please try again.', buttons: false, icon: "error", timer: 2000 });
		}
	});
}

getQuesList();

$('.update_next').on('click', function(){
	$('#submit_type').val(1);
});

$('.update_close').on('click', function(){
	$('#submit_type').val(2);
});

$('.type-color-on-page').spectrum({
	type: "component",
	togglePaletteOnly: "true",
	hideAfterPaletteSelect: "true",
	showInput: "true",
	showInitial: "true",
});

$('#font_type').fontselect({
	placeholder: 'Pick a font from the list',
	searchable: false,
	systemFonts: [],
	googleFonts: [<?php echo $db::googleFont; ?>]
});

/* Drag and Drop Selected Option */
$('.dragoption').on('click', function(){
	$('#seleted_drag_option').val($(this).val());
});

/* Backgrounds */
$.ajax({
	type: 'GET',
	url: 'includes/ajax.php',
	data: 'getCharBackground=true&type=1',
	success:function(resdata) {
		$.LoadingOverlay("hide");
		var res = $.parseJSON(resdata);
		if (res.success == true) {
			var bg_selected = '';
			$.each(res.data, function (key, val) {
				if (val.img_name == $('#get_bg_img').val()){
					bg_selected = 'checked="checked"';
				}
				else { bg_selected = ''; }
				var option = '<div class="bacground_banner">\
				<div class="Radio-box MCQRadio"><label class="radiostyle"><input type="radio" name="sim_background_img" value="'+val.img_name+'" '+bg_selected+'><span class="radiomark"></span></label><a data-fancybox="gallery" href="img/char_bg/'+val.img_name+'"><img src="img/char_bg/'+val.img_name+'"></a>';
				<?php if (isset($role) && $role == 'admin'): ?>
				option += '<a href="javascript:void(0);" data-upload-id="'+val.upload_id+'" data-img-name="'+val.img_name+'" class="delete_char_bg_img" title="Delete File"><i class="fa fa-times" aria-hidden="true"></i></a></div>';
				<?php endif; ?>
				$('.BGIMG').append(option);
			});
		}
		else { swal({text: 'Background image not loading please try again later.', buttons: false, icon: "error", timer: 2000 }); }
	},error: function() {
		$.LoadingOverlay("hide");
		swal({text: 'Oops. something went wrong please try again.', buttons: false, icon: "error", timer: 2000 });
	}
});

/* One Character */
$.ajax({
	type: 'GET',
	url: 'includes/ajax.php',
	data: 'getCharBackground=true&type=2,3',
	success:function(resdata) {
		$.LoadingOverlay("hide");
		var res = $.parseJSON(resdata);
		if (res.success == true) {
			var one_char_selected = '';
			$.each(res.data, function (key, val) {
				if (val.img_name == $('#get_sim_char_img').val()){
					one_char_selected = 'checked="checked"';
				}
				else { one_char_selected = ''; }
				var option = '<div class="bacground_banner">\
				<div class="Radio-box MCQRadio"><label class="radiostyle"><input type="radio" name="sim_char_img" class="1char" value="'+val.img_name+'" '+one_char_selected+'><span class="radiomark"></span></label><a data-fancybox="gallery1" href="img/char_bg/'+val.img_name+'"><img src="img/char_bg/'+val.img_name+'"></a>';
				<?php if (isset($role) && $role == 'admin'): ?>
				option += '<a href="javascript:void(0);" data-upload-id="'+val.upload_id+'" data-img-name="'+val.img_name+'" class="delete_char_bg_img" title="Delete File"><i class="fa fa-times" aria-hidden="true"></i></a></div>';
				<?php endif; ?>
				$('.OneCharIMG').append(option);
			});
		}
		else { swal({text: 'Background image not loading please try again later.', buttons: false, icon: "error", timer: 2000 }); }
	},error: function() {
		$.LoadingOverlay("hide");
		swal({text: 'Oops. something went wrong please try again.', buttons: false, icon: "error", timer: 2000 });
	}
});

/* Two Characters */
$.ajax({
	type: 'GET',
	url: 'includes/ajax.php',
	data: 'getCharBackground=true&type=2,3',
	success:function(resdata) {
		$.LoadingOverlay("hide");
		var res = $.parseJSON(resdata);
		if (res.success == true) {
			var sim_char_left_img  = $('#sim_char_left_img').val();
			var sim_char_right_img = $('#sim_char_right_img').val();
			var char_left  = '';
			var char_right = '';
			$.each(res.data, function (key, val) {
				if (sim_char_left_img != '' && sim_char_left_img == val.img_name) {
					char_left = 'checked="checked"';
					get_value = 'value="'+val.img_name+'|L"'; 
				} else { char_left = ''; get_value = 'value="'+val.img_name+'"';  }
				
				if (sim_char_right_img != '' && sim_char_right_img == val.img_name) {
					char_right = 'checked="checked"';
					get_value = 'value="'+val.img_name+'|R"'; 
				} else { char_right = ''; get_value = 'value="'+val.img_name+'"';  }
				
				var option = '<div class="bacground_banner"><div class="Check-box"><label class="checkstyle"><input type="checkbox" name="sim_two_char_img[]" class="2char" '+get_value+' '+char_left+' '+char_right+'>';
				if (char_left != ''){
					option += '<span class="leftIMG" title="Left side character">L</span>';
				}
				else if (char_right != ''){
					option += '<span class="RightIMG" title="Right side character">R</span>';
				}
				option += '<span class="checkmark"></span></label><a data-fancybox="gallery2" href="img/char_bg/'+val.img_name+'"><img src="img/char_bg/'+val.img_name+'"></a>';
				<?php if (isset($role) && $role == 'admin'): ?>
				option += '<a href="javascript:void(0);" data-upload-id="'+val.upload_id+'" data-img-name="'+val.img_name+'" class="delete_char_bg_img" title="Delete File"><i class="fa fa-times" aria-hidden="true"></i></a>';
				<?php endif; ?>
				$('.TwoCharIMG').append(option);
			});
		}
		else { swal({text: 'Background image not loading please try again later.', buttons: false, icon: "error", timer: 2000 }); }
	},error: function() {
		$.LoadingOverlay("hide");
		swal({text: 'Oops. something went wrong please try again.', buttons: false, icon: "error", timer: 2000 });
	}
});

/* Add New Question */
$('.AddIcon').on('click', function(e){
	e.preventDefault();
	$.LoadingOverlay("show");
	var simID = $('#sim_id').val();
	if (simID != '') {
		var dataString = 'add_Question='+ true +'&sim_id='+ simID;
		$.ajax({
			type: 'POST',
			url: 'includes/process.php',
			data: dataString,
			cache: false,
			success: function(resdata) {
				var res = $.parseJSON(resdata);
				if (res.success == true) {
					$.LoadingOverlay("hide");
					getQuesList();
				}
				else if (res.success == false) {
					$.LoadingOverlay("hide");
					swal({text: res.msg, buttons: false, icon: "warning", timer: 2000});
				}
			}, error: function() {
				swal({text: 'Oops. something went wrong please try again.', buttons: false, icon: "error", timer: 2000 });
			}
		});
	}
});

/* Delete Question */
$('.DelIcon').on('click', function(e){
	e.preventDefault();
	var ques = $('.ques_no:checked').map(function(){
        return this.value;
    }).get();
	if (ques.length > 0) {
		$.LoadingOverlay("show");
		var dataString = 'del_Question='+ true +'&ques_id='+ ques;
		$.ajax({
			type: 'POST',
			url: 'includes/process.php',
			data: dataString,
			cache: false,
			success: function(resdata) {
				var res = $.parseJSON(resdata);
				if (res.success == true) {
					$.LoadingOverlay("hide");
					getQuesList();
				}
				else if (res.success == false) {
					$.LoadingOverlay("hide");
					swal({text: res.msg, buttons: false, icon: "warning", timer: 2000});
				}
			}, error: function() {
				swal({text: 'Oops. something went wrong please try again.', buttons: false, icon: "error", timer: 2000 });
			}
		});
	} else {
		swal({text: 'Please select at-least one Question', buttons: false, icon: "warning", timer: 2000 });
	}
});

/* Clone Question */
$('.DuPIcon').on('click', function(e){
	e.preventDefault();
	var ques = $('.ques_no:checked').map(function(){ return this.value; }).get();
	if (ques.length == 1) {
		$.LoadingOverlay("show");
		var dataString = 'clone_Question='+ true +'&ques_id='+ ques;
		$.ajax({
			type: 'POST',
			url: 'includes/process.php',
			data: dataString,
			cache: false,
			success: function(resdata) {
				$.LoadingOverlay("hide");
				var res = $.parseJSON(resdata);
				if (res.success == true) {
					swal({text: res.msg, buttons: false, icon: "success", timer: 2000});
					getQuesList();
				}
				else if (res.success == false) {
					swal({text: res.msg, buttons: false, icon: "warning", timer: 2000});
				}
			}, error: function() {
				$.LoadingOverlay("hide");
				swal({text: 'Oops. something went wrong please try again.', buttons: false, icon: "error", timer: 2000 });
			}
		});
	} else {
		swal({text: 'Please select one Question', buttons: false, icon: "warning", timer: 2000 });
	}
});

//------Competency-Score-------
$('#ques_val_1,#sq_ques_val_1,#sort_ques_val_1,#mcq_ques_val_1,#mmcq_ques_val_1,#swipq_ques_val_1').on('change', function(){
	var score = parseInt($(this).val());
	var maxallow = parseInt($('#comp_val_1').val());
	if (maxallow < score) {
		$(this).val('');
		swal('Maximum '+maxallow+' score', { buttons: false, timer: 1000 });
	}
});

$('#ques_val_2,#sq_ques_val_2,#sort_ques_val_2,#mcq_ques_val_2,#mmcq_ques_val_2,#swipq_ques_val_2').on('change', function(){
	var score = parseInt($(this).val());
	var maxallow = parseInt($('#comp_val_2').val());
	if (score > maxallow) {
		$(this).val('');
		swal('Maximum '+maxallow+' score', { buttons: false, timer: 1000 });
	}
});

$('#ques_val_3,#sq_ques_val_3,#sort_ques_val_3,#mcq_ques_val_3,#mmcq_ques_val_3,#swipq_ques_val_3').on('change', function(){
	var score = parseInt($(this).val());
	var maxallow = parseInt($('#comp_val_3').val());
	if (score > maxallow) {
		$(this).val('');
		swal('Maximum '+maxallow+' score', { buttons: false, timer: 1000 });
	}
});

$('#ques_val_4,#sq_ques_val_4,#sort_ques_val_4,#mcq_ques_val_4,#mmcq_ques_val_4,#swipq_ques_val_4').on('change', function(){
	var score = parseInt($(this).val());
	var maxallow = parseInt($('#comp_val_4').val());
	if (score > maxallow) {
		$(this).val('');
		swal('Maximum '+maxallow+' score', { buttons: false, timer: 1000 });
	}
});

$('#ques_val_5,#sq_ques_va_5,#sort_ques_va_5,#mcq_ques_val_5,#mmcq_ques_val_5,#swipq_ques_val_5').on('change', function(){
	var score = parseInt($(this).val());
	var maxallow = parseInt($('#comp_val_5').val());
	if (score > maxallow) {
		$(this).val('');
		swal('Maximum '+maxallow+' score', { buttons: false, timer: 1000 });
	}
});

$('#ques_val_6,#sq_ques_va_6,#sort_ques_va_6,#mcq_ques_val_6,#mmcq_ques_val_6,#swipq_ques_val_6').on('change', function(){
	var score = parseInt($(this).val());
	var maxallow = parseInt($('#comp_val_6').val());
	if (score > maxallow) {
		$(this).val('');
		swal('Maximum '+maxallow+' score', { buttons: false, timer: 1000 });
	}
});

//----------Audio-Upload-All-Template--------------------
$('body').on('change', '.uploadAudioFile', function(){
	$.LoadingOverlay("show");
	var input_id	= $(this).data('id');
	var assets_id	= $(this).data('assets');
	var file_data	= $(this).prop('files')[0];
	var form_data	= new FormData();
	form_data.append('file', file_data);
	form_data.append('qAudiofiles', '1');
	$.ajax({
		url: "includes/upload.php",
		type: "POST",
		data: form_data,
		contentType: false,
		cache: false,
		processData: false,
		success: function(resdata){
			var res = $.parseJSON(resdata);
			if (res.success == true){
				$(input_id).val(res.file_name);						
				$('.'+ assets_id).addClass("disabled").attr('disabled', true);
				$('.'+ assets_id + '_container').show();
				$('.'+ assets_id + '_data a').text(res.file_name);
				$('.'+ assets_id + '_data a').attr('data-src', assets_path + res.file_name);
				$('.'+ assets_id + '_container a.delete_assets').attr('data-assets', res.file_name);
				$('.'+ assets_id + '_container a.delete_assets').attr('data-assets-id', assets_id);
				$('.'+ assets_id + '_container a.delete_assets').attr('data-input-id', input_id);
				$('.'+ assets_id + '_container a.delete_assets').attr('data-path', res.path);
				swal({text: res.msg, buttons: false, timer: 1000});
			}
			else if (res.success == false){
				$(input_id).val('');
				swal({text: res.msg, buttons: false, icon: "warning", timer: 1000});
			}
			$.LoadingOverlay("hide");
		},error: function(){
			$.LoadingOverlay("hide");
			swal("Error", 'Oops, something went wrong. Please try again later.?', "error");
		}
	});
});

//-----------Video-Upload-All-Template-------------------
$('body').on('change', '.uploadVideoFile', function() {
	$.LoadingOverlay("show");
	var input_id	= $(this).data('id');
	var assets_id	= $(this).data('assets');
	var file_data	= $(this).prop('files')[0];
	var form_data	= new FormData();
	form_data.append('file', file_data);
	form_data.append('qVideofiles', '1');
	$.ajax({
		url: "includes/upload.php",
		type: "POST",
		data: form_data,
		contentType: false,
		cache: false,
		processData: false,
		success: function(resdata) {
			var res = $.parseJSON(resdata);
			if (res.success == true) {
				$(input_id).val(res.file_name);
				swal({text: res.msg, buttons: false, timer: 1000});
				$('.'+ assets_id).addClass("disabled").attr('disabled', true);
				$('.'+ assets_id + '_container').show();
				$('.'+ assets_id + '_data a').text(res.file_name);
				$('.'+ assets_id + '_data a').attr('data-src', assets_path + res.file_name);
				$('.'+ assets_id + '_container a.delete_assets').attr('data-assets', res.file_name);
				$('.'+ assets_id + '_container a.delete_assets').attr('data-assets-id', assets_id);
				$('.'+ assets_id + '_container a.delete_assets').attr('data-input-id', input_id);
				$('.'+ assets_id + '_container a.delete_assets').attr('data-path', res.path);
			}
			else if (res.success == false) {
				$(input_id).val('');
				swal({text: res.msg, buttons: false, icon: "warning", timer: 1000});
			}
			$.LoadingOverlay("hide");
		},error: function() {
			$.LoadingOverlay("hide");
			swal("Error", 'Oops, something went wrong. Please try again later.?', "error");
		}
	});
});

//-----------Image-Upload-All-Template-------------------
$('body').on('change', '.uploadImgFile', function() {
	$.LoadingOverlay("show");
	var cur 		= $(this);
	var input_id	= $(this).data('id');
	var assets_id	= $(this).data('assets');
	var file_data	= $(this).prop('files')[0];
	var form_data	= new FormData();
	form_data.append('file', file_data);
	form_data.append('qImgfiles', '1');
	$.ajax({
		url: "includes/upload.php",
		type: "POST",
		data: form_data,
		contentType: false,
		cache: false,
		processData: false,
		success: function(resdata) {
			var res = $.parseJSON(resdata);
			if (res.success == true) {
				$(input_id).val(res.file_name);
				swal({text: res.msg, buttons: false, timer: 1000});
				cur.closest('div').addClass("disabled");
				$('.'+ assets_id).addClass("disabled").attr('disabled', true);
				$('.'+ assets_id + '_container').show();
				$('.'+ assets_id + '_data a').text(res.file_name);
				$('.'+ assets_id + '_data a').attr('data-src', assets_path + res.file_name);
				$('.'+ assets_id + '_container a.delete_assets').attr('data-assets', res.file_name);
				$('.'+ assets_id + '_container a.delete_assets').attr('data-assets-id', assets_id);
				$('.'+ assets_id + '_container a.delete_assets').attr('data-input-id', input_id);
				$('.'+ assets_id + '_container a.delete_assets').attr('data-path', res.path);
			}
			else if (res.success == false) {
				$(input_id).val('');
				swal({text: res.msg, buttons: false, icon: "warning", timer: 1000});
			}
			$.LoadingOverlay("hide");
		},error: function() {
			$.LoadingOverlay("hide");
			swal("Error", 'Oops, something went wrong. Please try again later.?', "error");
		}
	});
});

//-----------Doc-Upload-All-Template---------------------
$('body').on('change', '.uploadDocFile', function() {
	$.LoadingOverlay("show");
	var cur 		= $(this);
	var input_id	= $(this).data('id');
	var assets_id	= $(this).data('assets');
	var file_data	= $(this).prop('files')[0];
	var form_data	= new FormData();
	form_data.append('file', file_data);
	form_data.append('qDocfiles', '1');
	$.ajax({
		url: "includes/upload.php",
		type: "POST",
		data: form_data,
		contentType: false,
		cache: false,
		processData: false,
		success: function(resdata) {
			var res = $.parseJSON(resdata);
			if (res.success == true) {
				$(input_id).val(res.file_name);
				swal({text: res.msg, buttons: false, timer: 1000});
				cur.closest('div').addClass("disabled");
				$('.'+ assets_id).addClass("disabled").attr('disabled', true);
				$('.'+ assets_id + '_container').show();
				$('.'+ assets_id + '_data a').text(res.file_name);
				$('.'+ assets_id + '_data a').attr('data-src', assets_path + res.file_name);
				$('.'+ assets_id + '_container a.delete_assets').attr('data-assets', res.file_name);
				$('.'+ assets_id + '_container a.delete_assets').attr('data-assets-id', assets_id);
				$('.'+ assets_id + '_container a.delete_assets').attr('data-input-id', input_id);
				$('.'+ assets_id + '_container a.delete_assets').attr('data-path', res.path);
			}
			else if (res.success == false) {
				$(input_id).val('');
				swal({text: res.msg, buttons: false, icon: "warning", timer: 1000});
			}
			$.LoadingOverlay("hide");
		},error: function() {
			$.LoadingOverlay("hide");
			swal("Error", 'Oops, something went wrong. Please try again later.?', "error");
		}
	});
});

//-----------Record Audio-All-Template------------------
$('body').on('click', '.rec-audio', function() {
	var input_id  = $(this).attr('data-input-id');
	var assets_id = $(this).attr('data-assets');
	if (input_id != '') {
		$.LoadingOverlay("show");
		var $modal = $('#load_popup_modal_show');
		$modal.load('rec-audio-modal.php', {'data-id': input_id, 'assets-id': assets_id}, function(res) {
			if (res != '') {
				$.LoadingOverlay("hide");
				$modal.modal('show', {backdrop: 'static', keyboard: false});
			}
			else {
				$.LoadingOverlay("hide");
				swal({text: 'Oops, something went wrong. Please try again later', buttons: false, icon: "error", timer: 1000});
			}
		});
	}
});

//-----------Record Video-All-Template------------------
$('body').on('click', '.rec-video', function() {
	var input_id  = $(this).attr('data-input-id');
	var assets_id = $(this).attr('data-assets');
	if (input_id != '') {
		$.LoadingOverlay("show");
		var $modal = $('#load_popup_modal_show');
		$modal.load('rec-video-modal.php', {'data-id': input_id, 'assets-id': assets_id}, function(res) {
			if (res != '') {
				$.LoadingOverlay("hide");
				$modal.modal('show', {backdrop: 'static', keyboard: false});
			}
			else {
				$.LoadingOverlay("hide");
				swal({text: 'Oops, something went wrong. Please try again later', buttons: false, icon: "error", timer: 1000});
			}
		});
	}
});

//-----------Record Screen-All-Template-----------------
$('body').on('click', '.rec-screen', function() {
	var input_id  = $(this).attr('data-input-id');
	var assets_id = $(this).attr('data-assets');
	if (input_id != '') {
		$.LoadingOverlay("show");
		var $modal = $('#load_popup_modal_show');
		$modal.load('rec-screen-modal.php', {'data-id': input_id, 'assets-id': assets_id}, function(res) {
			if (res != '') {
				$.LoadingOverlay("hide");
				$modal.modal('show', {backdrop: 'static', keyboard: false});
			}
			else {
				$.LoadingOverlay("hide");
				swal({text: 'Oops, something went wrong. Please try again later', buttons: false, icon: "error", timer: 1000});
			}
		});
	}
});

//--------------Delete-Assets-All-Templates------------
$('body').on('click', '.delete_assets', function() {
	var cur   		= $(this);
	var file_name	= cur.attr('data-assets');
	var assetsId	= cur.attr('data-assets-id');
	var inputId		= cur.attr('data-input-id');
	var path		= cur.attr('data-path');
	var divId		= cur.closest('div').attr('class');
	var dataString  = 'delete_assets='+ true +'&assets_path='+ path +'&file='+ file_name;
	if (file_name){
		swal({
			title: "Are you sure?",
			text: "Delete this Assets.",
			icon: "warning",
			buttons: [true, 'Delete'],
			dangerMode: true, }).then((willDelete) => { if (willDelete) {
				$.LoadingOverlay("show");
				$.ajax({
					url: "includes/delete-file.php",
					type: "POST",
					data: dataString,
					cache: false,
					success: function(resdata){
						var res = $.parseJSON(resdata);
						if (res.success == false) {
							$.LoadingOverlay("hide");
							swal({text: res.msg, buttons: false, icon: "error", timer: 2000});
						}
						else if (res.success == true) {
							$(inputId).val('');
							$('.'+ assetsId).removeClass("disabled").removeAttr('disabled', true);
							$('.'+ divId).hide();
							$.LoadingOverlay("hide");
							swal({text: res.msg, buttons: [false, 'OK'], closeOnClickOutside: false, closeOnEsc: false});
						}
					},error: function() {
						swal({text: 'Oops. something went wrong please try again.', buttons: false, icon: "error", timer: 1000});
					}
				});
			} else { swal({text: 'Your file is safe', buttons: false, timer: 1000}); }
		});
	}
});

var x = <?php echo ( ! empty($i)) ? $i : 1; ?>;
var maxField = 6;
$(".plus_comp").click(function() {
	if (x < maxField) {
		x++;
		$(".plus_comp_option").append('<div class="add_comp_option linaer">\
		<div class="form-group"><input type="text" class="form-control control1" placeholder="Competency Name" name="competency_name_'+x+'" autocomplete="off" required="required"></div>\
		<div class="form-group labelbox"><label>Competency Score</label><input type="text" class="form-control question-box" name="competency_score_'+x+'" onkeypress="return isNumberKey(event);" autocomplete="off" required="required"></div>\
		<div class="form-group labelbox"><label>Weightage</label><input type="text" class="form-control question-box" name="weightage_'+x+'" onkeypress="return isNumberKey(event);" autocomplete="off" required="required"></div><div class="remove minusComp"><img src="img/icons/minus.png" title="Remove"></div></div>');
		var getMax = $('.add_comp_option').length;
		if (getMax >= maxField) {
			$('.plus_comp').addClass('disabled');
		}
		else { $('.plus_comp').removeClass('disabled'); }
	}
	else {
		$('.plus_comp').addClass('disabled');
	}
});

$('.plus_comp_option').on('click', '.remove', function(e) {
	e.preventDefault();
	$(this).parent().remove();
	x--;
	$('.plus_comp').removeClass('disabled');
});

$("#menu-toggle").click(function(e) {
	e.preventDefault();
	$("#menu-toggle").toggleClass("changebtn");
	$("#wrapper").toggleClass("toggled");
	$(".match-Q.tab-pane.fade").toggleClass("tabpadding150");
	$(".widthQ100").toggleClass("TogglewidthQ100");		
	$(".modal.Ques-pop-style.in").toggleClass("TogglewidthQ100");		
});

$(".closeleftmenuA").click(function(e) {
	e.preventDefault();
	$("#list-toggle").addClass("changebtnleft");
	$("#wrapper").addClass("toggledlist");
	$(".match-Q.tab-pane.fade").addClass("tabpadding150");
	$(".Q-left").addClass("left150");
	$(".widthQ100").addClass("TogglelistwidthQ100");
	$(".main_sim_scroll").addClass("TogglelistwidthQ100");	
	$(".modal.Ques-pop-style.in").addClass("TogglelistwidthQ100");
    $(".list-icon").addClass("Labsolute");			
});

$("#list-toggle").click(function(e) {
	e.preventDefault();
	$("#list-toggle").removeClass("changebtnleft");
	$("#wrapper").removeClass("toggledlist");
	$(".match-Q.tab-pane.fade").removeClass("tabpadding150");
	$(".Q-left").removeClass("left150");
	$(".widthQ100").removeClass("TogglelistwidthQ100");	
	$(".main_sim_scroll").removeClass("TogglelistwidthQ100");		
	$(".modal.Ques-pop-style.in").removeClass("TogglelistwidthQ100");
    $(".list-icon").removeClass("Labsolute");
});

$('#upload').click(function() {
	$(this).attr('disabled', 'disabled').html('<i class="fa fa-refresh fa-spin fa-fw"></i><span class="sr-only">Loading...</span> Please wait....');
	var file_data = $('#file').prop('files')[0];
	var form_data = new FormData();
	form_data.append('file', file_data);
	form_data.append('scenario_media_file', true);
	var ftype   = file_data.type;
	var gettype = $('#scenario_media_file_type').val();
	var ext 	= ftype.split('/')[0];
	if (ext == gettype) {
		$.ajax({
			url: "includes/upload.php",
			type: "POST",
			data: form_data,
			contentType: false,
			cache: false,
			processData:false,
			success: function(resdata) {
				var res = $.parseJSON(resdata);
				if (res.success == true) {
					if (res.zip == true) {
						$('#ImgDelete').show();
						$('.delete_wb_file').attr('data-wb-file', res.file_name);
						$('#scenario_media_file').val(res.file_name);
						$('#upload').html('Upload').removeAttr('disabled', 'disabled');
					}
					else {
						var viewHtml = '<a href="<?php echo $uploadpath ?>'+res.file_name+'" target="_blank" title="View Scenario Media File"><i class="fa fa-eye" aria-hidden="true"></i></a>';
						$('#splashImg').show().html(viewHtml);
						$('#ImgDelete').show();
						$('.delete_scenario_media_file').attr('data-scenario-media-file', res.file_name);
						$('#scenario_media_file').val(res.file_name);
						$('#upload').html('Upload').removeAttr('disabled', 'disabled');
					}
					swal(res.msg, { buttons: false, timer: 1000 });
				}
				else if (res.success == false) {
					$('#scenario_media_file').val('');
					swal("Error", res.msg, "error");
					$('#splashImg').hide('slow');
					$('#upload').html('Upload').removeAttr('disabled', 'disabled');
				}
			},error: function() {
				$('#splashImg').hide('slow');
				swal("Error", 'Oops. something went wrong please try again.?', "error");
			}
		});
	} else {
		$('#scenario_media_file,#scenario_media').val('');
		swal("Warning", 'Please select valid file. accept only '+gettype+' file', "warning");
		$('#upload').html('Upload').removeAttr('disabled', 'disabled');
	}
});

$('.delete_scenario_media_file').click(function() {
	var file_name   = $(this).attr("data-scenario-media-file");
	var dataString	= 'delete='+ true + '&scenario_media_file='+file_name;
	if (file_name) {
		swal({
			title: "Are you sure?",
			text: "Delete this file.",
			icon: "warning",
			buttons: [true, 'Delete'],
			dangerMode: true, }).then((willDelete) => { if (willDelete) {
				$.LoadingOverlay("show");
				$.ajax({
					url: "includes/delete-file.php",
					type: "POST",
					data: dataString,
					cache: false,
					success: function(resdata) {
						$.LoadingOverlay("hide");
						var res = $.parseJSON(resdata);
						if (res.success == false) {
							swal("Error", res.msg, "error");
							$('#splashImg').hide('slow');
						}
						else if (res.success == true) {
							$('#splashImg, #ImgDelete').hide('slow');
							$('#scenario_media_file,#scenario_media').val('');
							swal(res.msg, { buttons: false, timer: 1000 });
						}
					},error: function() {
						swal("Error", 'Oops, something went wrong. Please try again later.?', "error");
					}
				});
			} else { swal("Your file is safe.!", { buttons: false, timer: 1000, }); }
		});
	}
});

/* Upload Web Object */
$('#upload_web').click(function() {
	$(this).attr('disabled', 'disabled').html('<i class="fa fa-refresh fa-spin fa-fw"></i><span class="sr-only">Loading...</span> Please wait....');
	var file_data = $('#web_file').prop('files')[0];
	var form_data = new FormData();
	form_data.append('file', file_data);
	form_data.append('web_object_file', true);
	var ftype = file_data.type;
	var type  = $('#web_object_type').val();
	var ext   = ftype.split('/')[0];
	if (ext == type) {
		$.ajax({
			url: "includes/upload.php",
			type: "POST",
			data: form_data,
			contentType: false,
			cache: false,
			processData: false,
			success: function(resdata) {
				var res = $.parseJSON(resdata);
				if (res.success == true) {
					$('#WbDelete').show();
					$('.delete_wb').attr('data-wb', res.file_name);
					$('#web_object').val(res.file_name);
					$('#upload_web').html('Upload').removeAttr('disabled', 'disabled');
					swal(res.msg, { buttons: false, timer: 1000, });
				}
				else if (res.success == false) {
					swal("Error", res.msg, "error");
					$('#web_object').val('');
					$('#WbDelete').hide('slow');
					$('#upload_web').html('Upload').removeAttr('disabled', 'disabled');
				}
			},error: function() {
				$('#WbDelete').hide('slow');
				swal("Error", 'Oops. something went wrong please try again.?', "error");
			}
		});
	} else { 
		swal("Warning", 'Please select valid file. accept only zip file', "warning");
		$('#upload_web').html('Upload').removeAttr('disabled', 'disabled');
	}
});

/* Delete Web Object */
$('.delete_wb').click(function() {
	var file_name   = $(this).attr("data-wb");
	var dataString	= 'delete='+ true + '&wb_file='+ file_name;
	if (file_name) {
		swal({
			title: "Are you sure?",
			text: "Delete Web Object file.",
			icon: "warning",
			buttons: [true, 'Delete'],
			dangerMode: true, }).then((willDelete) => { if (willDelete) {
				$.LoadingOverlay("show");
				$.ajax({
					url: "includes/delete-file.php",
					type: "POST",
					data: dataString,
					cache: false,
					success: function(resdata) {
						$.LoadingOverlay("hide");
						var res = $.parseJSON(resdata);
						if (res.success == false) {
							swal("Error", res.msg, "error");
							$('#WbDelete').hide('slow');
						}
						else if (res.success == true) {
							$('#WbDelete').hide('slow');
							$('#web_object').val('');
							swal(res.msg, { buttons: false, timer: 1000, });
						}
					},error: function() {
						swal("Error", 'Oops, something went wrong. Please try again later.?', "error");
					}
				});
			} else { swal("Your file is safe.!", { buttons: false, timer: 1000, }); }
		});
	}
});

$('#uploadBG').click(function() {
	$(this).attr('disabled', 'disabled').html('<i class="fa fa-refresh fa-spin fa-fw"></i><span class="sr-only">Loading...</span> Please wait....');
	var file_data = $('#bg_file').prop('files')[0];
	var form_data = new FormData();
	form_data.append('file', file_data);
	form_data.append('add_own_background', true);
	var ftype = file_data.type;
	var ext = ftype.split('/')[0];
	if (ext == 'image') {
		$.ajax({
			url: "includes/upload.php",
			type: "POST",
			data: form_data,
			contentType: false,
			cache: false,
			processData:false,
			success: function(resdata) {
				var res = $.parseJSON(resdata);
				if (res.success == true) {
					$('.delete_scenario_bg_file').attr('data-scenario-bg-file', res.file_name).show();
					$('#scenario_bg_file').val(res.file_name);
					$('.own-bg-prev').attr('src', '<?php echo $uploadpath ?>' + res.file_name);
					$('#uploadBG').html('Upload').removeAttr('disabled', 'disabled');
					swal(res.msg, { buttons: false, timer: 1000, });
				}
				else if (res.success == false) {
					$('#scenario_bg_file').val('');
					swal("Error", res.msg, "error");
					$('.own-bg-prev').attr('src', 'img/scenario-img.png');
					$('#uploadBG').html('Upload').removeAttr('disabled', 'disabled');
				}
			},error: function() {
				swal("Error", 'Oops. something went wrong please try again.?', "error");
			}
		});
	} else { 
		swal("Warning", 'Please select valid file. accept only jpeg, jpg, png file', "warning");
		$('#uploadBG').html('Upload').removeAttr('disabled', 'disabled');
	}
});

$('.delete_scenario_bg_file').click(function() {
	var file_name   = $(this).attr("data-scenario-bg-file");
	var dataString	= 'delete='+ true + '&scenario_media_file='+file_name;
	if (file_name) {
		swal({
			title: "Are you sure?",
			text: "Delete this file.",
			icon: "warning",
			buttons: [true, 'Delete'],
			dangerMode: true, }).then((willDelete) => { if (willDelete) {
				$.LoadingOverlay("show");
				$.ajax({
					url: "includes/delete-file.php",
					type: "POST",
					data: dataString,
					cache: false,
					success: function(resdata) {
						$.LoadingOverlay("hide");
						var res = $.parseJSON(resdata);
						if (res.success == false) {
							swal("Error", res.msg, "error");
						}
						else if (res.success == true) {
							$('#scenario_bg_file').val('');
							$('.delete_scenario_bg_file').hide();
							$('.own-bg-prev').attr('src', 'img/scenario-img.png');
							swal(res.msg, { buttons: false, timer: 1000, });
						}
					},error: function() {
						$.LoadingOverlay("hide");
						swal("Error", 'Oops, something went wrong. Please try again later.?', "error");
					}
				});
			} else { swal("Your file is safe.!", { buttons: false, timer: 1000, }); }
		});
	}
});

$('#uploadChar').click(function() {
	$(this).attr('disabled', 'disabled').html('<i class="fa fa-refresh fa-spin fa-fw"></i><span class="sr-only">Loading...</span> Please wait....');
	var file_data = $('#file_char').prop('files')[0];
	var form_data = new FormData();
	form_data.append('file', file_data);
	form_data.append('add_char', true);
	var ftype = file_data.type;
	var ext = ftype.split('/')[0];
	if (ext == 'image') {
		$.ajax({
			url: "includes/upload.php",
			type: "POST",
			data: form_data,
			contentType: false,
			cache: false,
			processData:false,
			success: function(resdata) {
				var res = $.parseJSON(resdata);
				if (res.success == true) {
					$('.delete_scenario_char_file').attr('data-scenario-char-file', res.file_name).show();
					$('#scenario_char_file').val(res.file_name);
					$('.sce-imgchar').attr('src', '<?php echo $uploadpath ?>' + res.file_name);
					$('#uploadChar').html('Upload').removeAttr('disabled', 'disabled');
					swal(res.msg, { buttons: false, timer: 1000, });
				}
				else if (res.success == false) {
					$('#scenario_char_file').val('');
					swal("Error", res.msg, "error");
					$('.sce-imgchar').attr('src', 'img/charbg.png');
					$('#uploadChar').html('Upload').removeAttr('disabled', 'disabled');
				}
			},error: function() {
				swal("Error", 'Oops. something went wrong please try again.?', "error");
			}
		});
	} else { 
		swal("Warning", 'Please select valid file. accept only jpeg, jpg, png file', "warning");
		$('#uploadChar').html('Upload').removeAttr('disabled', 'disabled');
	}
});

$('.delete_scenario_char_file').click(function() {
	var file_name   = $(this).attr("data-scenario-char-file");
	var dataString	= 'delete='+ true + '&scenario_media_file='+file_name;
	if (file_name) {
		swal({
			title: "Are you sure?",
			text: "Delete this file.",
			icon: "warning",
			buttons: [true, 'Delete'],
			dangerMode: true, }).then((willDelete) => { if (willDelete) {
				$.LoadingOverlay("show");
				$.ajax({
					url: "includes/delete-file.php",
					type: "POST",
					data: dataString,
					cache: false,
					success: function(resdata) {
						$.LoadingOverlay("hide");
						var res = $.parseJSON(resdata);
						if (res.success == false) {
							swal("Error", res.msg, "error");
						}
						else if (res.success == true) {
							$('#scenario_char_file').val('');
							$('.delete_scenario_char_file').hide();
							$('.sce-imgchar').attr('src', 'img/charbg.png');
							swal(res.msg, { buttons: false, timer: 1000, });
						}
					},error: function() {
						$.LoadingOverlay("hide");
						swal("Error", 'Oops, something went wrong. Please try again later.?', "error");
					}
				});
			} else { swal("Your file is safe.!", { buttons: false, timer: 1000, }); }
		});
	}
});

/* Add Cover Image */
$('#upload_cover_img').click(function() {
	$(this).attr('disabled', 'disabled').html('<i class="fa fa-refresh fa-spin fa-fw"></i><span class="sr-only">Loading...</span> Please wait....');
	var file_data = $('#upload_cover_img_file').prop('files')[0];
	var form_data = new FormData();
	form_data.append('file', file_data);
	form_data.append('scenario_cover_file', true);
	var ftype   = file_data.type;
	var type	= $('#cover_img_file_type').val();
	var ext 	= ftype.split('/')[0];
	if (ext == type) {
		$.ajax({
			url: "includes/upload.php",
			type: "POST",
			data: form_data,
			contentType: false,
			cache: false,
			processData:false,
			success: function(resdata) {
				var res = $.parseJSON(resdata);
				if (res.success == true) {
					$('#scenario_cover_file').val(res.file_name);
					$('.sim_cover_img_data,.delete_cover_img_file').show();
					$('.sim_cover_img_data a').text(res.file_name);
					$('.sim_cover_img_data a').attr('data-src', assets_path + res.file_name);
					$('.cover_image_container a.delete_cover_img_file').attr('data-cover-img-file', res.file_name);
					$('#upload_cover_img').html('Upload').attr('disabled', 'disabled');
					$('.upload_cover_btn').attr('disabled', 'disabled');
					swal(res.msg, { buttons: false, timer: 1000 });
				}
				else if (res.success == false) {
					swal("Error", res.msg, "error");
					$('.sim_cover_img_data').hide('slow');
					$('#upload_cover_img').html('Upload').removeAttr('disabled', 'disabled');
					$('.upload_cover_btn').removeAttr('disabled', 'disabled');
				}
			},error: function() {
				swal("Error", 'Oops. something went wrong please try again.?', "error");
				$('#upload_cover_img').html('Upload').removeAttr('disabled', 'disabled');
				$('.upload_cover_btn').removeAttr('disabled', 'disabled');
			}
		});
	} else {
		swal("Warning", 'Please select valid file. accept only '+ type +' file', "warning");
		$('#upload_cover_img').html('Upload').removeAttr('disabled', 'disabled');
		$('.upload_cover_btn').removeAttr('disabled', 'disabled');
	}
});

/* Delete Cover Image */
$('.delete_cover_img_file').click(function() {
	var file_name   = $(this).attr("data-cover-img-file");
	var dataString	= 'delete='+ true + '&scenario_media_file='+ file_name;
	if (file_name) {
		swal({
			title: "Are you sure?",
			text: "Delete this file.",
			icon: "warning",
			buttons: [true, 'Delete'],
			dangerMode: true }).then((willDelete) => { if (willDelete) {
				$.LoadingOverlay("show");
				$.ajax({
					url: "includes/delete-file.php",
					type: "POST",
					data: dataString,
					cache: false,
					success: function(resdata) {
						$.LoadingOverlay("hide");
						var res = $.parseJSON(resdata);
						if (res.success == false) {
							swal("Error", res.msg, "error");
						}
						else if (res.success == true) {
							$('#scenario_cover_file').val('');
							$('.sim_cover_img_data,.delete_cover_img_file').hide('slow');
							$('.sim_cover_img_data a').text('');
							$('.sim_cover_img_data a').attr('data-src', '');
							$('.cover_image_container a.delete_cover_img_file').attr('data-cover-img-file', '');
							$('#upload_cover_img').html('Upload').removeAttr('disabled', 'disabled');
							$('.upload_cover_btn').removeAttr('disabled', 'disabled');
							swal(res.msg, { buttons: false, timer: 1000 });
						}
					},error: function() {
						swal("Error", 'Oops, something went wrong. Please try again later.?', "error");
					}
				});
			} else { swal("Your file is safe.!", { buttons: false, timer: 1000, }); }
		});
	}
});

$('.sim_page_desc').each(function(e){
	CKEDITOR.replace(this.id, { customConfig: "config-max-description.js" });
});

if ($('#sim_page_desc_0').length) {
	CKEDITOR.replace('sim_page_desc_0', {
		customConfig : "config-max-description.js"
	});
}

var page = <?php echo ( ! empty($pagei)) ? $pagei++ : 1; ?>;
$(".plus_page").click(function() {
	$(".plus_page_option:last").append('<div class="add_page_option appendSCE">\
	<div class="form-group">\
		<input type="text" name="sim_page_name[]" class="form-control" placeholder="Page name" required="required" />\
	</div>\
	<div class="form-group">\
		<textarea name="sim_page_desc[]" id="sim_page_desc_'+ page +'" class="form-control"></textarea>\
	</div>\
	 <div class="remove minusComp"><img src="img/icons/minus.png" title="Remove Page"></div></div>');
	 var page_des_id = 'sim_page_desc_'+ page;
	 CKEDITOR.replace(page_des_id, {
		customConfig : "config-max-description.js",
	});
	page++;
});

$('.plus_page_option').on('click', '.remove', function(e) {
	e.preventDefault();
	$(this).parent().remove();
	page--;
});

$('input[name="bgoption"]').on('change', function (){
	var bg = $(this).val();
	if (bg == 1) {
		$('#scenario_bg_file').val('');
		$('input[name="sim_background_img"]').attr('checked', false);
	}
	else if (bg == 2) {
		$("#bg_color").spectrum("set", '');
		$('input[name="sim_background_img"]').attr('checked', false);
	}
	else if (bg == 3) {
		$("#bg_color").spectrum("set", '');
		$('#scenario_bg_file').val('');
	}
});

<?php if  ( ! empty($sim_data['bg_color'])) : ?>
$("#bg_color").spectrum("set", '<?php echo $sim_data['bg_color'] ?>');
<?php endif; 
if  ( ! empty($sim_brand['ques_bg'])) : ?>
$("#ques_bg_color").spectrum("set", '<?php echo $sim_brand['ques_bg'] ?>');
<?php endif; 
if  ( ! empty($sim_brand['option_bg'])) : ?>
$("#ques_option_bg_color").spectrum("set", '<?php echo $sim_brand['option_bg'] ?>');
<?php endif; 
if  ( ! empty($sim_brand['option_hover'])) : ?>
$("#ques_option_hover_color").spectrum("set", '<?php echo $sim_brand['option_hover'] ?>');
<?php endif; 
if  ( ! empty($sim_brand['option_selected'])) : ?>
$("#ques_option_select_color").spectrum("set", '<?php echo $sim_brand['option_selected'] ?>');
<?php endif; 
if  ( ! empty($sim_brand['btn_bg'])) : ?>
$("#btn_bg_color").spectrum("set", '<?php echo $sim_brand['btn_bg'] ?>');
<?php endif; 
if  ( ! empty($sim_brand['btn_hover'])) : ?>
$("#btn_hover_color").spectrum("set", '<?php echo $sim_brand['btn_hover'] ?>');
<?php endif; 
if  ( ! empty($sim_brand['btn_selected'])) : ?>
$("#btn_select_color").spectrum("set", '<?php echo $sim_brand['btn_selected'] ?>');
<?php endif; 
if  ( ! empty($sim_brand['font_color'])) : ?>
$("#font_color").spectrum("set", '<?php echo $sim_brand['font_color'] ?>');
<?php endif; 
if  ( ! empty($sim_brand['font_type'])) : ?>
$("#font_type").trigger("setFont", '<?php echo $sim_brand['font_type'] ?>');
<?php endif; ?>
document.onreadystatechange = function() {
	if (document.readyState == "complete"){
		$.LoadingOverlay("hide");
	}
};
</script>
<?php 
require_once 'includes/footer.php';
