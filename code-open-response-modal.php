<?php 
session_start();
if (isset($_SESSION['username'])) {
    $user   = $_SESSION['username'];
    $role   = $_SESSION['role'];
    $userid = $_SESSION['userId'];
	$domain = (isset($_SESSION['domain_name'])) ? $_SESSION['domain_name'] : $_SERVER['HTTP_HOST'];
}
else { header('location: index.php'); }
$path	= 'scenario/upload/'. $domain .'/';
$box_id	= (isset($_POST['box_id'])) ? $_POST['box_id'] : '';
$qid	= (isset($_POST['qid'])) ? $_POST['qid'] : '';
$type	= (isset($_POST['type'])) ? $_POST['type'] : ''; ?>
<div id="load_popup_modal_contant" class="OPTText openReSonceModel" role="dialog">
	<div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h3 class="modal-title">WRITE CODE</h3>
            </div>
            <div class="modal-body">
            	<div class="textarea">
                	<textarea type="text" class="form-control1 inputtextWrap" id="code_data"></textarea>
                </div>
                <button type="button" class="btn1 submitbtn1 upload" data-dismiss="modal">Insert</button>
                <button type="button" class="btn1 submitbtn1" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </div>
     </div>
</div>
<script type="text/javascript" src="content/ckeditor/editor/ckeditor.js"></script>
<script type="text/javascript">
var inputId = '<?php echo $box_id; ?>';
$('.upload').on('click', function(){
	for (instance in CKEDITOR.instances) { CKEDITOR.instances[instance].updateElement(); }
	var data = $('#code_data').val();
	if (data) {
		$('#file_name_'+ inputId).val('');
		$('#tts_data_'+ inputId).val('');
		$('#code_data_'+ inputId).val(data);
		$('#action_'+ inputId).hide();
		$('#open_res_answer_'+ inputId).show();
		$('#open_answer_btn_'+ inputId).show();
		$('#open_res_answer_'+ inputId + ' .delete_tts').show();
		$('#open_res_answer_'+ inputId + ' .delete_assets').hide();
		$('#open_res_answer_'+ inputId + ' .ORAudiobox').hide();
		$('#open_res_answer_'+ inputId + ' .ORVideobox').hide();
		$('#open_res_answer_'+ inputId + ' .ORTextbox').show().html(data);
		swal({text: 'Code added successfully.', buttons: false, icon: "success", timer: 1000});
	}
});
CKEDITOR.replace('code_data', { customConfig: "config-open-response.js" });
</script>
