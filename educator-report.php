<?php 
session_start();
if (isset($_SESSION['username'])) {
	$user 	= $_SESSION['username'];
	$role 	= $_SESSION['role'];
	$userid	= $_SESSION['userId'];
}
else {
    header('location: index.php');
}
require_once 'config/db.class.php';
require_once 'includes/learners_menu.php';
$db 		= new DBConnection();
$icon_path	= 'img/list/'; ?>
<link rel="stylesheet" media="screen" href="content/css/report_page.css" />
<link rel="stylesheet" type="text/css" href="content/dataTables/jquery.dataTables.css" />
<link rel="stylesheet" type="text/css" href="content/dataTables/jquery.dataTables.min.css" />
<link rel="stylesheet" type="text/css" href="content/dataTables/buttons.dataTables.min.css" />
<style type="text/css">
.table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th {
    padding: 10px;
}
button.multiselect.dropdown-toggle {
    height: 35px;
    margin-top: 10px;
}
input.form-control.multiselect-search {
    margin-top: 0px;
    margin-bottom: 0px;
    height: 35px;
}
button.btn.btn-default.multiselect-clear-filter {
    height: 35px;
}
.L-R-W20 .form-control {
    width: 100%;
    margin-top: 0px;
    margin-bottom: 0px;
}
button.multiselect.dropdown-toggle {
    margin-top: 0px;
    margin-bottom: 0px;
}
.L-R-W20.educaterusermanage {
    margin-top: 15px;
    text-align: right;
    padding-right: 5px;
}
</style>
<div class="bottomheader">
  <div class="container-fluid">
    <ul class="breadcrumb">
      <?php echo $db->breadcrumbs(); ?>
    </ul>
  </div>
</div>
<div class="admin-report edcuaterRePort">
	<div class="bottomheader1">
		<div class="container-fluid">
			<div class="row">
				<div class="col-sm-8"><h3>Learner Report</h3></div>
				<div class="col-sm-1"></div>
				<div class="col-sm-3 L-calender GPeducater usermanage-form">
					<select class="form-control" id="report">
						<option selected="selected" hidden>Learner Report</option>
						<option value="1">My Group Performance</option>
					</select>
				</div>
			</div>
		</div>
	</div>
	<div class="container-fluid">
		<div class="row">
			<div class="col-sm-12">
				<form name="report_form" id="report_form" method="post">
					<div class="usermanage-form educatorrepORT">
						<div class="row L-R-W20ROW">
							<div class="L-R-W20">
								<div class="form-group form-select">
								<select name="category" id="category" class="form-control" data-placeholder="Category List" required="required">
                                <option value="0" selected="selected" disabled="disabled" hidden="">Select Category</option>
                                <?php foreach ($db->getCategory() as $category): ?>
                                <option value="<?php echo $category['cat_id'] ?>"><?php echo ucwords($category['category']) ?></option>
                                <?php endforeach; ?>
								</select>
								</div>
							</div>
							<div class="L-R-W20">
								<div class="form-group form-select">
								<select name="scenario" id="scenario" class="form-control" data-placeholder="Simulation List" required="required">
									<option value="0" selected="selected" disabled="disabled">Select Simulation</option>
								</select>
								</div>
							</div>
                            <div class="L-R-W20">
                                <div class="form-group form-select">
                                <select name="group" id="group" class="form-control" data-placeholder="Select Group">
									<option value="0" selected="selected" disabled="disabled">Select Group</option>
                                </select>
                                </div>
                            </div>
							<div class="L-R-W20">
								<div class="form-group form-select">
									<select name="learner" id="learner" class="form-control multiselect-ui selection" data-placeholder="Learner List *">
									</select>
								</div>
							</div>								
						</div>
						<div class="L-R-W20 educaterusermanage">
							<div class="usermanage-add clearfix">
								<input type="submit" name="submit" id="submit" class="btn btn-primary Search_btn" value="Search">
							</div>	
						</div>						
					</div>
				</form>
			</div>
		</div>
		<?php if ($_SERVER['REQUEST_METHOD'] == 'POST' && ! empty($_POST['learner'])):
		extract($_POST);
		$sim_data = ( ! empty($scenario)) ? $db->getScenario(md5($scenario)) : exit('error'); ?>
        <div class="print_data">
			<div class="row">
                <div class="col-sm-12">
                    <div class="usermanage_main">
                        <div class="table-box">
                            <div class="tableheader clearfix">
                                <div class="tableheader_left">Simulation Name: <?php echo $sim_data['Scenario_title'] ?></div>
                                <div class="tableH_right">Learner Name: <?php $userData = $db->getUsers(md5($learner)); echo $userData['fname'] .' '. $userData['lname'] . ' ('. $userData['username'].')'; ?>, Group Name: <?php echo $db->getGroup(md5($group))['group_name']; ?></div>
                            </div>
                        </div>
                        <div class="usermanage-form1">
                            <div class="table-responsive">
                                <table id="example" cellpadding="0" cellspacing="0" border="0" class="display nowrap table table-striped" width="100%">
                                    <thead class="Theader">
                                        <tr>
                                        	<th>#</th>
                                        	<th>Date of Attempt</th>
                                            <th>View Report</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php 
									$sim_temp_type = $sim_data['sim_temp_type'];
									$sql = "SELECT *, DATE_FORMAT(attempt_date, '%e-%b-%Y %l:%i %p') AS atdate FROM scenario_attempt_tbl WHERE 
											scenario_id = '". $scenario ."' AND userid = '". $learner ."' AND (uid IN (SELECT uid FROM score_tbl) OR uid IN (SELECT uid FROM multi_score_tbl) OR uid IN (SELECT uid FROM open_res_tbl)) ORDER BY aid DESC";
									$q	 = $db->prepare($sql); $q->execute();
									if ($q->rowCount() > 0):
										$i = 1;
										foreach ($q->fetchAll(PDO::FETCH_ASSOC) as $row):
										if ( ! empty($row['uid'])): ?>
                                   	 	<tr>
                                        	<td><?php echo $i; ?></td>
											<td><?php echo $row['atdate']; ?></td>
											<?php $url = $db->educatorReportPage($sim_temp_type); ?>
											<td><a title="View Report" href="<?php echo $url ?>?report=true&sid=<?php echo md5($row['scenario_id']); ?>&uid=<?php echo $row['uid']; ?>&lid=<?php echo $row['userid']; ?>" target="_blank"><img class="img" src="<?php echo $icon_path; ?>view_report.svg" /></a></td>
                                    	</tr>
                                    	<?php endif; $i++; endforeach; endif; ?>
									</tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php endif; ?>
	</div>
</div>
<style type="text/css">
@media screen and (-ms-high-contrast: active), (-ms-high-contrast: none) {
	p.circleChart_text { left: 350%; }
}
p.circleChart_text {
    font-size: 25px !important;
}
.comp-incomp-topbanner {
    background: #fff;
    min-height: 429px;
    border-radius: 0px;
    margin-bottom: 20px;
	padding:20px;
	border: 1px solid #e2e2e2;
	position:relative;
}
.lernar-dashboard-boxes {
	padding: 10px;
}
.lernar-dashboard-boxes span{
	display:table-cell;
	font-size:40px;
	vertical-align:middle;
}
.lernar-dashboard-boxes p{
	padding-left:10px;
}
@media(max-width: 1450px) and (min-width: 1024px){
	.lernar-dashboard-boxes p {
		padding-right:0px;
	}
}
.wordwrap { 
   white-space: pre-wrap;
   white-space: -moz-pre-wrap;
   white-space: -pre-wrap;
   white-space: -o-pre-wrap;
   word-wrap: break-word;
}
canvas.canvasjs-chart-canvas {
    padding-top: 0px;
}
</style>
<script type="text/javascript" language="javascript" src="content/dataTables/jquery.dataTables.js"></script>
<script type="text/javascript" language="javascript" src="content/dataTables/jquery.dataTables.min.js"></script>
<script type="text/javascript" language="javascript" src="content/dataTables/dataTables.buttons.min.js"></script>
<script type="text/javascript" language="javascript" src="content/dataTables/buttons.flash.min.js"></script>
<script type="text/javascript" language="javascript" src="content/dataTables/jszip.min.js"></script>
<script type="text/javascript" language="javascript" src="content/dataTables/pdfmake.min.js"></script>
<script type="text/javascript" language="javascript" src="content/dataTables/vfs_fonts.js"></script>
<script type="text/javascript" language="javascript" src="content/dataTables/buttons.html5.min.js"></script>
<script type="text/javascript" language="javascript" src="content/dataTables/buttons.print.min.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
		$('#report').on('change', function() {
			var id = $(this).val();
			if (id == 1) window.location.href = 'group-performance-educator.php';
		});
		$('#example').DataTable({
			"searching": false,
			scrollY: "300px",
			scrollX: true,
			scrollCollapse: true,
			fixedColumns: true,
			"lengthChange": false
		});
	});
	
	$('#learner').multiselect({
		includeSelectAllOption: false,
		filterPlaceholder: 'Search & select learner',
		enableCaseInsensitiveFiltering : true,
		enableFiltering: true,
		buttonWidth: '250px',
		maxHeight: 250
	});

	$('#category').on('change', function() {
		var catid = $(this).val();
		if (catid != '')
		$('#scenario,#learner,#group').find("option:eq(0)").html("Please wait..");
		$.getJSON('includes/ajax.php?getEducatorScenario=true&catid='+ catid, function(res) {
			$('#scenario,#learner,#group').empty();
			if (res.success == true) {
				$('#scenario').append('<option value="" selected="selected" disabled="disabled">Select Simulation</option>');
				$('#group').append('<option value="" selected="selected" disabled="disabled">Select Group</option>');
				$('#learner').append('<option value="" selected="selected">Select Learner</option>');
				$.each(res.data, function (key, val) {
					var option = '<option value="'+ val.value +'">'+ val.label +'</option>';
					$('#scenario').append(option);
				});
			}
			else if (res.success == false) {
				$('#scenario').append('<option value="" selected="selected" disabled="disabled">Select Simulation</option>');
				$('#learner').append('<option value="" selected="selected">Select Learner</option>');
				$('#group').append('<option value="" selected="selected" disabled="disabled">Select Group</option>');
			}
		});
	})

	$('#scenario').on('change', function() {
		var sim_id = $(this).val();
		if (sim_id != '')
			$('#learner,#group').find("option:eq(0)").html("Please wait..");
			$.getJSON('includes/ajax.php?getEducatorScenarioGroup=true&sim_id='+ sim_id, function(res){
				$('#learner,#group').empty();
				if (res.success == true) {
					$('#group').append('<option value="" selected="selected" disabled="disabled">Select Group</option>');
					$('#learner').append('<option value="" selected="selected">Select Learner</option>');
					$.each(res.data, function (key, val) {
						var option = '<option value="'+ val.value +'">'+ val.label +'</option>';
						$('#group').append(option);
					});
				}
				else {
					$('#group').append('<option value="" selected="selected" disabled="disabled">Select Group</option>');
					$('#learner').append('<option value="" selected="selected">Select Learner</option>');
				}
			});
	})

	$('#group').on('change', function(){
		var gid = $(this).val();
		if (gid != '')
			$('#learner').empty();
			$('#learner').multiselect('refresh');
			$.getJSON('includes/ajax.php?getLearnerByGroup=true&group_id='+ gid, function(res) {
				if (res.success == true && res.data != null) {
					$('#learner').multiselect('dataprovider', res.data);
				}
				else if (res.success == false) {
					$('#learner').multiselect('rebuild');
					$('#learner').multiselect('refresh');
				}
			});
	});
</script>
<?php 
require_once 'includes/footer.php';
