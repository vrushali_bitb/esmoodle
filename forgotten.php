<?php 
ob_start();
session_start();
?>
<!doctype html>
<!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]><html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--><html class="no-js" lang="en"> <!--<![endif]-->
<head>
  <title>Forgot Password | Can't Log In | EasySIM</title>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="shortcut icon" type="image/x-icon" href="favicon.ico" />
  <!-- Chrome, Firefox OS and Opera -->
  <meta name="theme-color" content="#2c3545">
  <!-- Windows Phone -->
  <meta name="msapplication-navbutton-color" content="#2c3545">
  <!-- iOS Safari -->
  <meta name="apple-mobile-web-app-status-bar-style" content="#2c3545">
  <link rel="stylesheet" media="screen" href="content/css/bootstrap.css" />
  <link rel="stylesheet" media="screen" href="content/css/bootstrap.min.css" />
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" media="screen" href="content/css/style.css?v=<?php echo time() ?>" />
  <link rel="stylesheet" media="screen" href="content/css/responsive.css?v=<?php echo time() ?>" />
  <script type="text/javascript" src="content/js/jquery-3.3.1.min.js"></script>
  <script type="text/javascript" src="content/js/bootstrap.3.3.7.min.js"></script>
  <script type="text/javascript" src="content/js/sweetalert.min.js"></script>
  <script type="text/javascript" src="content/js/loadingoverlay.min.js"></script>
  <script type="text/javascript" src="content/js/timezone.js"></script>
</head>
<body>
<?php 
require_once 'config/db.class.php';
$obj    = new DBConnection;
$logo   = ( ! empty($obj->siteLogo)) ? $obj->siteLogo : 'EasySIM_logo.svg';
$brand  = $obj->webBranding();
# Check if already login then redirect
if (isset($_SESSION['username']) && isset($_SESSION['role'])) :
  $user   = $_SESSION['username'];
  $role   = $_SESSION['role'];
  $userid = $_SESSION['userId'];
  if (isset($role)) $obj->userRoleRedirect($role);
endif;
echo ( ! empty($brand)) ? $brand : ''; ?>
  <header>
    <div class="home_header">
      <ul class="logowhite">
        <a href="<?php echo $obj->getBaseUrl(); ?>">
          <li><img class="white-svg-logo" src="<?php echo $obj->getBaseUrl('img/logo/'. $logo); ?>"></li>
        </a>
        <li><img class="white-svg-logo1" src="<?php echo $obj->getBaseUrl('img/homepage/logo.svg'); ?>"></li>
      </ul>
    </div>
  </header>
  <div class="login-form">
    <div class="col-md-4"></div>
    <div class="forget_pswd">
      <div class="col-md-4">
        <div class="logintext forgettext">Forgot Password</div>
        <div class="login_box">
          <label class="reset_text">To reset your password, please enter your Username and a link will be emailed to you.</label>
          <form method="post" name="ForgetFrom" id="ForgetFrom">
            <input type="hidden" name="Forget_pwd" id="Forget_pwd" value="1" />
            <div class="form-group">
              <div class="input-group"> <span class="loginicions"><img src="img/left_menu/user.svg"></span>
                <input type="text" class="form-control" id="username" placeholder="Enter your Username" name="username" autofocus>
              </div>
            </div>
            <center>
              <div class="form-group">
                <div class="input-group">
                  <button type="submit" class="regsterbtn" name="forgot_pwd_send">Send</button>
                </div>
              </div>
              <a href="<?php echo $obj->getBaseUrl('login'); ?>">
                <div class="createacount">Log in</div>
              </a>
            </center>
          </form>
        </div>
      </div>
    </div>
  </div>
<?php 
require_once 'includes/footer.php';
