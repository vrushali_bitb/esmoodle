<?php 
session_start();
if (isset($_SESSION['username'])):
	$user = $_SESSION['username'];
else:
	die;
endif;
require_once 'config/db.class.php';
$db		 = new DBConnection;
$getid	 = (isset($_POST['data-id'])) ? $_POST['data-id'] : FALSE;
$getdata = ( ! empty($getid)) ? $db->getCategory($getid) : FALSE; ?>
<div id="load_popup_modal_contant" class="" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h3 class="modal-title">Update Category</h3>
            </div>
            <div class="modal-body">
            	<form enctype="multipart/form" name="edit_Category_form" id="edit_Category_form" method="post">
                	<div class="usermanage-form">
                    	<div class="row">
                    		<div class="col-sm-12">
                                <div class="form-group">
                                	<input type="text" class="form-control" name="category" id="category" placeholder="New Category" value="<?php echo $getdata['category'] ?>">
                                </div>
                    		</div>
                   		</div>
                    </div>
                	<div class="modal-footer">
                    	<input type="hidden" name="category_update" value="1" />
						<input type="hidden" name="cat_id" value="<?php echo $getid; ?>" />
                        <input type="hidden" name="old_cat" value="<?php echo $getdata['category'] ?>" />
                        <button type="submit" name="editScenarioStatus" id="editScenarioStatus" class="btn btn-primary" onClick="return confirm('Are you sure to update this category.?');">Update</button>
						<button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                    </div>
                </form>
            </div>
        </div>
     </div>
</div>
<script type="text/javascript">
$("#edit_Category_form").on('submit', (function(e) {
	e.preventDefault();
	var form_data = $(this).serialize();
	var post_url  = 'includes/process.php';
	$.LoadingOverlay("show");
	$.ajax({
		url: post_url,
		type: "POST",
		data: form_data,
		success: function(result) {
			var res = $.parseJSON(result);
			$.LoadingOverlay("hide");
			if (res.success == true) {
				swal({text: res.msg, buttons: false, icon: "success", timer: 1000});
				setTimeout(function() { window.location.reload(); }, 500);
			}
			else if (res.success == false) {
				swal({text: res.msg, buttons: false, icon: "error", timer: 1000});
				setTimeout(function() { window.location.reload(); }, 500);
			}
		},error: function() {
			swal({text: 'Oops, something went wrong. Please try again later', buttons: false, icon: "error", timer: 1000});
		}
	});
}));
</script>
<?php 
ob_end_flush();
