<?php 
if(!isset($_SESSION)) 
{ 
session_start();
}
if (isset($_SESSION['username'])):
	$user 	= $_SESSION['username'];
	$role 	= $_SESSION['role'];
	$userid	= $_SESSION['userId'];
	$domain = (isset($_SESSION['domain_name'])) ? $_SESSION['domain_name'] : $_SERVER['HTTP_HOST'];
	$client_id = (isset($_SESSION['client_id'])) ? $_SESSION['client_id'] : '';
else:
    header('location: index.php');
endif;
require_once 'config/db.class.php';
require_once 'includes/learners_menu.php';
$sid		= (isset($_GET['sid'])) ? $_GET['sid'] : exit('error');
$db			= new DBConnection();
$result		= $db->getScenario($sid);
$simPage	= $db->getSimPage($result['scenario_id']);
$sim_type	= $result['sim_ques_type'];
$temp_type	= $result['sim_temp_type'];
$path		= 'scenario/upload/'.$domain.'/';
$proctor	= $db->checkProctorPermission($client_id);
$profile	= ( ! empty($getUsers['img']) && file_exists($db->rootPath().'img/profile/'.$getUsers['img'])) ? TRUE : FALSE;
?>
<!-- Owl Stylesheets -->
<link rel="stylesheet" href="content/css/owlcarousel/owl.carousel.min.css" />
<link rel="stylesheet" href="content/css/qustemplate.css" />
<link rel="stylesheet" href="content/css/questionresponsive.css" />
<link rel="stylesheet" href="videojs/node_modules/video.js/dist/video-js.min.css" />
<link rel="stylesheet" href="videojs/node_modules/video.js/dist/themes/forest/index.css" />
<script type="text/javascript" src="videojs/node_modules/video.js/dist/video.min.js"></script>
<style type="text/css">
	.text-hint-banner.webframeobj{
		text-align: center;
		width: 100%;
	}
	.nav-tabs {
		border-bottom: none;
	}
	.owl-item {
		text-align: center;
	}
	.owl-item li {
		list-style: none;
	}
	ul, ol {
		padding: revert;
		margin: revert;
		list-style: unset !important;
	}
	.owl-stage {
		margin: 0 auto;
	}
	.owl-theme .owl-nav{
		margin-top:0;
	}
	.owl-carousel .owl-item {
		padding: 10px;
	}
	button.owl-prev {
		position: absolute;
		left: -80px;
		top: 37%;
		transform: translateY(-50%);
		background-color: #049805!important;
		width: 20px;
		height: 20px;
		border-radius: 100%!important;
		color: #fff!important;
	}
	
	button.owl-next {
		position: absolute;
		right: -80px;
		top: 37%;
		transform: translateY(-50%);
		background-color: #049805!important;
		width: 20px;
		height: 20px;
		border-radius: 100%!important;
		color: #fff!important;
	}
  
	.admin-dashboard.Ev2 .nav {
		padding-top: 5px;
		padding: 0 95px;
	}
	.owl-dots {
		display: none;
	}
  
	/*------------------phone-------------*/
	@media (min-width:320px)   and (max-width: 767px) {
		button.owl-prev {
			left: -20px !important;
		}
		.admin-dashboard.Ev2 .nav {
			padding: 0 25px !important;
		}
	
		button.owl-next {
			right: -20px !important;
		}
	}
</style>
<div class="admin-dashboard Ev2 launchSce launchScev2">
	<div class="container-fuild linearassestBG">
		<h2 class="launchhead"><?php echo ( ! empty($result['Scenario_title'])) ? $result['Scenario_title'] : ''; ?></h2>
		<div class="text-hint-banner">
			<?php if (empty($result['video_url']) && ! empty($result['audio_url']) && file_exists($path . $result['audio_url'])): ?>
				<audio id="audio_example" class="video-js vjs-forest" controls preload="auto" poster="scenario/img/audio-poster.jpg" data-setup='{}'>
					<source src="<?php echo $path . $result['audio_url'] ?>" type="audio/<?php echo strtolower(pathinfo($result['audio_url'], PATHINFO_EXTENSION)); ?>" />
				</audio>
			<?php elseif ( ! empty($result['video_url']) && file_exists($path . $result['video_url'])): ?>
				<video id="video_example" class="video-js vjs-forest" controls preload="auto" height="300px" data-setup="{}" data-type="video">
					<source src="<?php echo $path . $result['video_url'] ?>" type="video/<?php echo strtolower(pathinfo($result['video_url'], PATHINFO_EXTENSION)); ?>" />
				</video>
			<?php elseif ( ! empty($result['image_url']) && file_exists($path . $result['image_url'])): ?>
				<img src="<?php echo $path . $result['image_url'] ?>" class="img-responsive img-fluid"/>
			<?php elseif ( ! empty($result['web_object_file']) && file_exists($path . $result['web_object_file'])):
			$wofn = '';
			if (file_exists($path . $result['web_object_file'] . '/story.html')):
				$wofn = 'story.html';
			elseif (file_exists($path . $result['web_object_file'] . '/index.html')):
				$wofn = 'index.html';
			elseif (file_exists($path . $result['web_object_file'] . '/a001index.html')):
				$wofn = 'a001index.html';
			endif; ?>
			<iframe src="<?php echo $path . $result['web_object_file'] . '/' . $wofn; ?>" width="1000px" height="600px" frameborder="0"></iframe>
			<script type="text/javascript">
			$('.text-hint-banner').addClass('webframeobj');
			</script>
			<?php endif; ?>
		</div>
		<ul class="nav nav-tabs">
			<div class="owl_1 owl-carousel">
			<?php if ($simPage): $l = 0;
				foreach ($simPage as $plink): ?>
				<div class="item">
				<li <?php echo ($l == 0) ? 'class="active"' : ''; ?>><a data-toggle="tab" href="#<?php echo $plink['sim_page_id'] ?>"><?php echo $plink['sim_page_name'] ?></a></li></div>
			<?php $l++; endforeach; endif; ?>
			</div>
		</ul>
    </div>
    <div class="tab-content">
	<?php if ($simPage): $p = 0;
	foreach ($simPage as $pdata): ?>
	<div id="<?php echo $pdata['sim_page_id'] ?>" class="tab-pane fade <?php echo ($p == 0) ? 'in active' : ''; ?>">
		<div class="Lrn-Banner linearlunch1"><?php echo $pdata['sim_page_desc'] ?></div>
	</div>
	<?php $p++; endforeach; endif; ?>
    </div>
</div>
<?php 
$launchUrl = '';
if ($sim_type == 1):
	if ($temp_type == 1):
		$launchUrl = 'launch-sim-linear-classic.php';
	elseif ($temp_type == 2):
		$launchUrl = 'launch-sim-linear-multiple.php';
	elseif ($temp_type == 3):
		$launchUrl = 'launch-sim-linear-video.php';
	endif;
elseif ($sim_type == 2):
	if ($temp_type == 4):
		$launchUrl = 'launch-sim-branching-classic.php';
	elseif ($temp_type == 5):
		$launchUrl = 'launch-sim-branching-multiple.php';
	elseif ($temp_type == 6):
		$launchUrl = 'launch-sim-branching-video.php';
	endif;
elseif ($sim_type == 3):
	if ($temp_type == 7):
		$launchUrl = 'launch-sim-open-response.php';
	endif;
elseif ($sim_type == 4):
	if ($temp_type == 8):
		$launchUrl = 'launch-single-video.php';
	endif;
elseif($sim_type == 5):
	if ($temp_type == 6):
		$launchUrl = 'launch-sim-linear-classic-video.php';
	elseif ($temp_type == 9):
		$launchUrl = 'launch-sim-linear-multiple-video.php';
	endif;
elseif ($sim_type == 6):
	if ($temp_type == 11):
		$launchUrl = 'launch-branching-video-multiple.php';
	endif;
	if ($temp_type == 10):
		$launchUrl = 'launch-branching-video-classic.php';
	endif;
endif; ?>
<div class="footergray Launchbtn linnerlaunch">
	<a href="<?php echo $launchUrl; ?>?preview=true&sim_id=<?php echo $sid; ?>" id="launch_btn" <?php echo ($proctor == TRUE) ? 'style="display: none;"' : ''; ?>><button class="btn btn-primary">LAUNCH SIMULATION</button></a>
	<?php if ($proctor == TRUE): if ($profile): ?>
		<a href="javascript:void(0);" id="Verify"><button class="btn btn-primary">FACE VERIFICATION</button></a>
	<?php else: ?>
		<a href="profile.php"><button class="btn btn-primary">First upload your profile picture</button></a>
	<?php endif; endif; ?>
</div>
<div id="load_popup_modal_show" class="modal fade" data-keyboard="false" data-backdrop="static"></div>
<script src="content/js/owlcarousel/owl.carousel.min.js"></script>
<script type="text/javascript">
$('.owl_1').owlCarousel({
    loop:false,
	mouseDrag: false,
    margin:20,
	responsiveClass:true,
	autoplayHoverPause:true,
    autoplay:false,
	slideSpeed: 400,
	paginationSpeed: 400,
	autoplayTimeout:3000,
    responsive:{
        0:{
            items:2,
            nav:true,
              loop:false
        },
        600:{
            items:3,
            nav:true,
              loop:false
        },
        1000:{
            items:6,
            nav:true,
            loop:false
        }
    }
});
$(document) .ready(function(){
	var li =  $(".owl-item li");
	$(".owl-item li").click(function(){
		li.removeClass('active');
	});
});
//-----------View-SIM-----------------
$('body').on('click', '#Verify', function(){
	var sim_id = $(this).attr('data-sim-id');
	$.LoadingOverlay("show");
	var $modal = $('#load_popup_modal_show');
	$modal.load('face-verification-modal.php', function(res){
		$.LoadingOverlay("hide");
		if (res != ''){
			$modal.modal('show', {backdrop: 'static', keyboard: false});
		}
		else {
			swal({text: "Oops. something went wrong please try again.", buttons: false, icon: "error", timer: 3000});
		}
	});
});
</script>
<?php 
require_once 'includes/footer.php';
