<?php 
session_start();
if (isset($_SESSION['username'])):
	$user 	= $_SESSION['username'];
	$role 	= $_SESSION['role'];
	$userid	= $_SESSION['userId'];
	$domain = (isset($_SESSION['domain_name'])) ? $_SESSION['domain_name'] : $_SERVER['HTTP_HOST'];
else:
    header('location: index.php');
endif;
require_once 'config/db.class.php';
require_once 'includes/learners_menu.php';
$db 		= new DBConnection();
$path		= 'scenario/upload/'.$domain.'/';
$noimg		= 'scenario/upload/noimage.png';
$iconpath	= 'img/type_icon/';
$imgpath	= 'img/list/';
$curdate	= strtotime(date('Y-m-d H:i:s'));
$getSim		= ( ! empty($db->getAssignSimData($userid))) ? $db->getAssignSimData($userid) : 0; ?>
<!-- Owl Stylesheets -->
<link rel="stylesheet" href="content/css/owlcarousel/owl.carousel.min.css">
<link rel="stylesheet" href="content/css/owlcarousel/owl.theme.default.css">
<script type="text/javascript">
$.LoadingOverlay("show");
</script>
<div class="bottomheader">
    <div class="container-fluid">
        <ul class="breadcrumb">
			<li><a href="<?php echo $db->getBaseUrl() ?>">Home</a></li>
            <li>My Incomplete Simulations</li>
         </ul>
         <div class="btn-group btn-shortBYLernar shortright">
            <button type="button" class="btn btn-Transprate"><span class="sort">Sort by</span></button>
            <button type="button" class="btn btn-Transprate dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img  src="img/down_arrow.png"></button>
            <div class="dropdown-menu shortBYMenu">
                <a class="dropdown-item" href="<?php echo $db->getBaseUrl($db->getFile()); ?>">Default Sorting</a>
                <a class="dropdown-item" href="<?php echo $db->getBaseUrl($db->getFile().'?orderby=category'); ?>">Category</a>
            </div>
        </div>         
     </div>
</div>
<?php 
#----------Sort by----------------
$corderby = '';
$orderby = '';
if (isset($_GET['orderby']) && $_GET['orderby'] == 'category'):
	$corderby = " ORDER BY category";
endif;
$catSql = "SELECT * FROM `category_tbl` WHERE category != '' AND cat_id IN (SELECT scenario_category FROM scenario_master WHERE scenario_id IN ($getSim) AND scenario_id NOT IN (SELECT scenario_id FROM scenario_attempt_tbl WHERE userid = '". $userid ."')) $corderby";
$catRes = $db->prepare($catSql); $catRes->execute();
$p = 1; foreach ($catRes->fetchAll(PDO::FETCH_ASSOC) as $crow): ?>
<div class="cardcontainer">
	<div class="maintitle">
    	<div class="container-fluid"><?php echo ucwords($crow['category']); ?></div>
    </div>
    <div class="container-fluid">
    	<div class="row">
			<div class="col-sm-12">
				<div class="maincardbox">
					<div class="owl-carousel owl-theme cat_<?php echo $p ?>">
					<?php 
					$sql = "SELECT DISTINCT scenario_id, sim_ques_type, sim_temp_type, Scenario_title, duration, image_url, video_url, audio_url, scenario_type, scenario_category, author_scenario_type, sim_cover_img, web_object FROM scenario_master WHERE scenario_category = '". $crow['cat_id'] ."' AND scenario_id IN ($getSim) AND scenario_id NOT IN (SELECT scenario_id FROM scenario_attempt_tbl WHERE userid = '". $userid ."')";
					$results = $db->prepare($sql);
					if ($results->execute()):
						foreach ($results->fetchAll(PDO::FETCH_ASSOC) as $row):
							$sim_id 		= $row['scenario_id'];
							$sim_type_icon	= $db->getScenarioTypes(md5($row['author_scenario_type']));
							$totalAttempt	= $db->getTotalAttempt($sim_id, $userid);
							$assignData		= $db->getAssignSimOtherData($sim_id, $userid)[0];
							$totalQuestions = $db->getTotalQuestionsByScenario(md5($sim_id));
							$start_date		= ($assignData['start_date'] != '0000-00-00 00:00:00') ? strtotime($assignData['start_date']) : '';
							$end_date		= ($assignData['end_date'] != '0000-00-00 00:00:00') ? strtotime($assignData['end_date']) : '';
							$branching		= $multiple = FALSE;
							if ($row['sim_temp_type'] == 7):
								/* Open Response */
								$timeSum 		= $db->getOpenResTotalTime($sim_id, $userid);
								$getLastAttempt = $db->getLastAttemptOpenResp($sim_id, $userid);
								$table			= 'open_res_tbl';
								$column			= 'open_res_id';
								$report_page	= 'open-response-learner-progress.php';
							elseif ($row['sim_temp_type'] == 2 || $row['sim_temp_type'] == 9):
								/* Classic :: Multiple */
								$timeSum 		= $db->getMultiTotalTime($sim_id, $userid);
								$getLastAttempt = $db->getLastAttemptMulti($sim_id, $userid);
								$table			= 'multi_score_tbl';
								$column			= 'mid';
								$report_page	= 'learner-progress-linear-multiple.php';
							elseif ($row['sim_temp_type'] == 4):
								/* Branching :: Classic */
								$timeSum 		= $db->getTotalTime($sim_id, $userid);
								$getLastAttempt = $db->getLastAttempt($sim_id, $userid);
								$table			= 'score_tbl';
								$column			= 'sid';
								$report_page	= 'learner-progress-branching-classic.php';
								$branching		= TRUE;
							elseif ($row['sim_temp_type'] == 5 || $row['sim_temp_type'] == 11):
								/* Branching :: Multiple */
								$timeSum 		= $db->getMultiTotalTime($sim_id, $userid);
								$getLastAttempt = $db->getLastAttemptMulti($sim_id, $userid);
								$table			= 'multi_score_tbl';
								$column			= 'mid';
								$report_page	= 'learner-progress-branching-multiple.php';
								$branching		= TRUE;
								$multiple		= TRUE;
							else:
								/* Linear :: Classic || Single Video */
								$timeSum 		= $db->getTotalTime($sim_id, $userid);
								$getLastAttempt = $db->getLastAttempt($sim_id, $userid);
								$table			= 'score_tbl';
								$column			= 'sid';
								if ($row['sim_temp_type'] == 1):
									$report_page = 'learner-progress.php';
								elseif ($row['sim_temp_type'] == 8):
									$report_page = 'learner-progress-single-video.php';
								elseif ($row['sim_temp_type'] == 6):
									$report_page = 'learner-progress-report-video-classic.php';
								elseif ($row['sim_temp_type'] == 10):
									$report_page = 'learner-progress-branching-classic-video.php';
								else:
									$report_page = '';
								endif;
							endif;
							if ( ! empty($getLastAttempt['uid']) && $branching == FALSE):
								$satisfaction = $db->getCustomerSatisfaction($getLastAttempt['uid'], $sim_id, $column, $table);
							elseif ( ! empty($getLastAttempt['uid']) && $branching == TRUE && $multiple == FALSE):
								$satisfaction = $db->getBranchingSatisfaction($getLastAttempt['uid'], $sim_id, $column, $table);
							elseif ( ! empty($getLastAttempt['uid']) && $branching == TRUE && $multiple == TRUE):
								$satisfaction = $db->getBranchingSatisfaction($getLastAttempt['uid'], $sim_id, $column, $table, TRUE);
							else:
								$satisfaction = 0;
							endif;
							$percentage = ( ! empty($satisfaction)) ? $db->get_percentage($totalQuestions, $satisfaction) : 0;
							if ( ! empty($row['web_object']) && file_exists($path . $row['web_object'])):
								$url = 'learning.php';
							else:
								$url = 'launch.php';
							endif; ?>
							<div class="item">
								<div>
									<div class="cardbox">
										<div class="scenarioimg">
											<?php $type_icon = ( ! empty($sim_type_icon['type_icon'])) ? $iconpath . $sim_type_icon['type_icon'] : ''; ?>
											<img class="img right" src="<?php echo $type_icon; ?>" title="<?php echo $sim_type_icon['type_name'] ?>" />
										</div>
										<div class="video_img CMVimg"><img class="imgbox img-responsive" src="<?php echo ( ! empty($row['sim_cover_img'])) ? $path . $row['sim_cover_img'] : $noimg; ?>" /></div>
										<div class="gridcard">
											<div class="card-body">
												<h4 class="title" title="<?php echo $row['Scenario_title']; ?>"><?php echo $db->truncateText($row['Scenario_title'], 50); ?></h4>
											</div>
											<?php if ( ! empty($start_date) && ! empty($end_date) && $curdate >= $start_date && $curdate <= $end_date): ?>
											<div class="cardfooter clearfix">
												<div class="footerleft">
												<?php if ( ! empty($assignData['no_attempts']) && $totalAttempt >= $assignData['no_attempts']):
													else: ?>
														<a title="Launch" href="<?php echo $url ?>?view=true&sid=<?php echo md5($sim_id); ?>"><img class="img" src="<?php echo $imgpath; ?>launch.svg" /></a>
												<?php endif; ?>
												</div>
												<div class="footerright">
												<?php if ( ! empty($getLastAttempt['uid'])): ?>
													<div title="Progress" class="circleChart" data-value="<?php echo $percentage; ?>" data-text="<?php echo $percentage; ?>%"></div>
													<img src="<?php echo $imgpath; ?>duration.svg" alt="Duration" data-html="true" data-toggle="tooltipas" data-placement="top" title="Total: <?php echo ( ! empty($row['duration'])) ? $row['duration'] : 0 ?> (min) <br/> Attempted: <?php echo $timeSum['timeSum']; ?>" />
													<a title="View Report" href="<?php echo ( ! empty($report_page)) ? $report_page . '?report=true&sid='. md5($sim_id) : 'JavaScript:void(0);'; ?>" target="_blank"><img class="img" src="<?php echo $imgpath; ?>report.svg" /></a>
													<?php endif; ?>
												</div>
											</div>
											<?php elseif (empty($start_date) && empty($end_date)): ?>
											<div class="cardfooter clearfix">
												<div class="footerleft">
												<?php if ( ! empty($assignData['no_attempts']) && $totalAttempt >= $assignData['no_attempts']):
													else: ?>
														<a title="Launch" href="<?php echo $url ?>?view=true&sid=<?php echo md5($sim_id); ?>"><img class="img" src="<?php echo $imgpath; ?>launch.svg" /></a>
												<?php endif; ?>
												</div>
												<div class="footerright">
												<?php if ( ! empty($getLastAttempt['uid'])): ?>
													<div title="Progress" class="circleChart" data-value="<?php echo $percentage; ?>" data-text="<?php echo $percentage; ?>%"></div>
													<img src="<?php echo $imgpath; ?>duration.svg" alt="Duration" data-html="true" data-toggle="tooltipas" data-placement="top" title="Total: <?php echo ( ! empty($row['duration'])) ? $row['duration'] : 0 ?> (min) <br/> Attempted: <?php echo $timeSum['timeSum']; ?>" />
													<a title="View Report" href="<?php echo ( ! empty($report_page)) ? $report_page . '?report=true&sid='. md5($sim_id) : 'JavaScript:void(0);'; ?>" target="_blank"><img class="img" src="<?php echo $imgpath; ?>report.svg" /></a>
													<?php endif; ?>
												</div>
											</div>
											<?php else: ?>
												Sorry, this assessment is not yet available.
											<?php endif; ?>
										</div>
									</div>
								</div>
							</div>
						<?php endforeach; endif; ?>
					</div>
				</div>
            </div>
        </div>
    </div>
 </div>
<script type="text/javascript">
$(document).ready(function() {
  var owl = $('.cat_<?php echo $p ?>');
  owl.owlCarousel({
		loop:false,
		nav:true,
		margin:20,
		responsive:{
			0:{items:1},
			600:{items:2},
			960:{items:3},
			1200:{items:5},
			1400:{items:6}
		}
	});
})
</script>
<?php $p++; endforeach; ?>
<script src="content/js/owlcarousel/owl.carousel.min.js"></script>
<script src="content/js/polyfill.min.js"></script>
<script src="content/js/circleChart/circleChart.es5.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {
	$(".circleChart").circleChart({
		color				: "#edb007",
		backgroundColor		: "#455a64",
		background			: true,
		size				: 28,
		animate				: true,
	});
	$('[data-toggle="tooltipas"]').tooltip({template: '<div class="tooltiptop tooltiptopArroW"><div class="tooltip-head"><h5><span class="glyphicon glyphicon-info-sign"></span> Duration</h5></div><div class="tooltip-inner"></div></div>'});
});
document.onreadystatechange = function() {
	if (document.readyState == "complete"){
		$.LoadingOverlay("hide");
	}
};
</script>
<?php 
require_once 'includes/footer.php';
