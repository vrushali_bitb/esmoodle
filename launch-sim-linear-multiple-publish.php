<?php 
ob_start();
session_start();
if (isset($_SESSION['username'])):
	$user 	= $_SESSION['username'];
	$role 	= $_SESSION['role'];
	$userid = $_SESSION['userId'];
	$domain = (isset($_SESSION['domain_name'])) ? $_SESSION['domain_name'] : $_SERVER['HTTP_HOST'];
else:
    header('location: index.php');
endif;
require_once 'config/db.class.php';
$db 		= new DBConnection();
require_once 'includes/scorm-header.php';
$scorm_head	= ob_get_contents();
$sim_id		= (isset($_GET['sim_id'])) ? $_GET['sim_id'] : die('Error');
$type		= (isset($_GET['type'])) ? $_GET['type'] : '';
$simData	= $db->getScenario($sim_id);
$simID 		= $simData['scenario_id'];
$title  	= $simData['Scenario_title'];
$simPage  	= $db->getSimPage($simID);
$brand		= $db->getSimBranding($simID);
$back_img	= ( ! empty($simData['sim_back_img'])) ? 'img/char_bg/'. $simData['sim_back_img'] : '';
$path		= $db->getBaseUrl('scenario/upload/'. $domain .'/');
$root_path	= $db->rootPath().'scenario/upload/'. $domain .'/';
$aPoster	= $db->getBaseUrl('scenario/img/audio-poster.jpg');
$noImage	= $db->getBaseUrl('scenario/img/palceholder.jpg');
$vtheme		= 'forest'; #city, fantasy, forest, sea
$left_char	= $right_char = '';

/* COMPETENCY SCORE */
$sqlcom	= "SELECT (SUM(comp_val_1) + SUM(comp_val_2) + SUM(comp_val_3) + SUM(comp_val_4) + SUM(comp_val_5) + SUM(comp_val_6)) AS totalCompval FROM competency_tbl WHERE scenario_id = '". $simID ."'";
$comexc	= $db->prepare($sqlcom); $comexc->execute();
$comRes	= $comexc->fetch(PDO::FETCH_ASSOC);
$totalCompval = ( ! empty($comRes['totalCompval'])) ? $comRes['totalCompval'] : 0;
/* SUB-ANSWER LIST */
function getSubOptions($ans_id) {
	global $db;
	$ansSql = "SELECT answer_id AS aid, sub_options_id AS sid FROM 
			   sub_options_tbl WHERE answer_id IN ($ans_id)";
	$ansexe = $db->prepare($ansSql); $ansexe->execute();
	if ($ansexe->rowCount() > 0):
		foreach ($ansexe->fetchAll(PDO::FETCH_ASSOC) as $ansrow):
			$ansData[] = $ansrow;
		endforeach;
		return $ansData;
	endif;
}

/* ANSWER LIST */
function getAnsData($qid) {
	global $db;
	$ansSql = "SELECT answer_id AS aid, true_option AS trueAns FROM 
			   answer_tbl WHERE question_id = '". $qid ."'";
	$ansexe = $db->prepare($ansSql); $ansexe->execute();
	if ($ansexe->rowCount() > 0):
		foreach ($ansexe->fetchAll(PDO::FETCH_ASSOC) as $ansrow):
			$ansData[] = $ansrow;
			$subAnsData[] = getSubOptions($ansrow['aid']);
		endforeach;
		return ( ! empty($subAnsData[0])) ? [$ansData, 'sub_ans' => $subAnsData] : $ansData;
	else:
		return [];
	endif;
}
/* QUESTION LIST */
$questionSql = "SELECT question_id AS qid, question_type AS qtype, true_options AS trueQ, (ques_val_1 + ques_val_2 + ques_val_3 + ques_val_4 + ques_val_5 + ques_val_6) AS QScore FROM 
				question_tbl WHERE scenario_id = '". $simID ."'";
$questionexe = $db->prepare($questionSql); $questionexe->execute();
if ($questionexe->rowCount() > 0):
	foreach ($questionexe->fetchAll(PDO::FETCH_ASSOC) as $row):
		$jsondata[] = ['ques' => $row, 'ans' => getAnsData($row['qid'])];
	endforeach;
else:
	$jsondata = [];
endif;
#echo '<pre/>'; print_r($jsondata); exit;
if ( ! empty($simData['sim_char_img'])):
	$left_char = 'img/char_bg/'. $simData['sim_char_img'];
elseif ( ! empty($simData['sim_char_left_img'])):
	$left_char = 'img/char_bg/'. $simData['sim_char_left_img'];
	elseif ( ! empty($simData['char_own_choice']) && file_exists($root_path . $simData['char_own_choice'])):
	$left_char = $path . $simData['char_own_choice'];
endif;
if ( ! empty($simData['sim_char_right_img'])):
	$right_char = 'img/char_bg/'. $simData['sim_char_right_img'];
endif;
if ( ! empty($brand['font_type'])):
$ftype = str_replace('+', ' ', $brand['font_type']); ?>
<style type="text/css">
@import url("https://fonts.googleapis.com/css2?family=<?php echo $brand['font_type'] ?>:wght@100;200;300;400;500;600;700;800;900&display=swap");
body { font-family: '<?php echo $ftype ?>' !important; }
h1,h2,h3,h4,h5,h6 { font-family: '<?php echo $ftype ?>' !important; }
p { font-family: '<?php echo $ftype ?>' !important; }
</style>
<?php endif; ?>
<?php if ($type == 'scrom'): ?>
<script src="SCORMGeneric.js" type="text/javascript" language="JavaScript"></script>
<script type="text/javascript" language="JavaScript">
    function init(score, status) {
		if (status == 1) {
			if (SCOInitialize() == "true") {
				objAPI.LMSSetValue("cmi.core.lesson_status", "completed");
			}
			objAPI.LMSSetValue("cmi.core.lesson_status", "completed");
			objAPI.LMSSetValue("cmi.core.lesson_status", "passed");
			objAPI.LMSSetValue("cmi.core.score.raw", score);
			finish();
		}
		else if (status == 2) {
			if (SCOInitialize() == "true") {
				objAPI.LMSSetValue("cmi.core.lesson_status", "incomplete");
			}
			objAPI.LMSSetValue("cmi.core.lesson_status", "incomplete");
			objAPI.LMSSetValue("cmi.core.lesson_status", "failed");
			objAPI.LMSSetValue("cmi.core.score.raw", score);
			Failed();
		}
		else if (status == 3) {
			if (SCOInitialize() == "true") {
				objAPI.LMSSetValue("cmi.core.lesson_status", "completed");
			}
			objAPI.LMSSetValue("cmi.core.lesson_status", "completed");
			objAPI.LMSSetValue("cmi.core.lesson_status", "failed");
			objAPI.LMSSetValue("cmi.core.score.raw", score);
			Failed();
		}
    }
    function finish() {
		if (objAPI != null) {
            objAPI.LMSFinish("cmi.objectives.n.status", "passed");
        }
        objAPI.LMSFinish("cmi.objectives.n.status", "passed");
    }
	function Failed() {
		if (objAPI != null) {
            objAPI.LMSFinish("cmi.objectives.n.status", "failed");
        }
        objAPI.LMSFinish("cmi.objectives.n.status", "failed");
    }
</script>
<?php endif; ?>
<script type="text/javascript">
	window.addEventListener('beforeunload', function(e) {
	  // Cancel the event
	  e.preventDefault(); // If you prevent default behavior in Mozilla Firefox prompt will always be shown
	  // Chrome requires returnValue to be set
	  e.returnValue = '';
	});
	
	function disableF5(e) { if ((e.which || e.keyCode) == 116 || (e.which || e.keyCode) == 82) e.preventDefault(); };
	
	$(document).ready(function() {
		$(document).on("keydown", disableF5);
	});
	
	$(document).bind("contextmenu", function(e){
		return false;
	});

	history.pushState(null, null, location.href);

    window.onpopstate = function() { history.go(1); };
	
	$.LoadingOverlay("show");
	
	var simBrand = <?php echo json_encode($brand); ?> 
	var quesJson = <?php echo json_encode($jsondata); ?> 
</script>
<style type="text/css">
	.close-banner img {
		width: 30px;
	}
	.feedback_box:after {
		content: '';
		width: 100%;
		height: 100vh;
		background: black;
		position: absolute;
		top: 0;
		opacity: 0.6;
		z-index: -1;
		right: 0;
	}
	.modal-open .modal {
		overflow-x: hidden;
		overflow-y: hidden;
	}
	.Dflex .multiDBDbox {
        height: auto !important;
    }
<?php if ( ! empty($back_img)): ?>
	.linear_classic {
		background: url("<?php echo $db->getBaseUrl($back_img); ?>") center top no-repeat;
		background-size: cover;
		overflow: hidden;
		height: 100vh;
		display: flex;
	}
	.bordered {
		background: #abdd89;
	}
	.swappable {
		cursor:pointer;
	}
<?php elseif ( ! empty($simData['bg_color'])): ?>
	.linear_classic { background: <?php echo $simData['bg_color']; ?>; display:flex; }
<?php endif; ?>
<?php if ( ! empty($brand['option_bg'])): ?>
	.questioninput:before { background: <?php echo $brand['option_bg']; ?>; }
	.questioninput:after { background: <?php echo $brand['option_bg']; ?>; }
	.rectangle1:after { background: <?php echo $brand['option_bg']; ?>; }
<?php endif; ?>
</style>
<div class="menu-banner-box">
	<div class="time-banner-box">
		<button class="V_S-tetail view_scenario" data-fancybox data-type="iframe" data-src="index.html">
    		<img class="Deskview" src="<?php echo $db->getBaseUrl('img/list/view_scenario_b.svg'); ?>" />
        	<img class="mobview" src="<?php echo $db->getBaseUrl('img/list/view_scenario.svg'); ?>" />
        	View Scenario
    	</button>
  	</div>
    <!----SIM-Timer---->
    <div class="timer-banner">
    	<div class="time-text">
        	<span class="timer-pause" data-minutes-left="<?php echo $simData['duration'] ?>" style="pointer-events: none;"></span>
    	</div>
  	</div>
</div>
<script type="text/javascript">
var timer = $('.timer-pause').startTimer({
	onComplete: function(element) {
		swal({text: "Simulation time end. please relaunch.!", icon: "info"}); },
		allowPause: true
	});
</script>
<div class="linear_classic">
	<?php if ( ! empty($left_char)): ?>
	<div class="CharFlex <?php echo ( ! empty($right_char)) ? 'CharFlextwo' : ''; ?>"><img class="img-fulid qusTempimg" src="<?php echo $db->getBaseUrl($left_char); ?>" /></div>
	<?php endif; ?>
	<div class="flexQTeMP <?php echo ( ! empty($right_char)) ? 'flexQTeMPtwochar' : ''; ?><?php echo ( ! empty($left_char) && empty($right_char)) ? 'flexQTeMPonechar' : ''; ?><?php echo (empty($left_char) && empty($right_char)) ? 'flexQTeMPnochar' : ''; ?>">
	<?php 
	$qSql = $db->prepare("SELECT * FROM question_tbl WHERE scenario_id = '". $simID ."' ORDER BY qorder = 0, qorder"); $qSql->execute();
	$totalQ = $qSql->rowCount();
	if ($totalQ > 0):
		$q = 1;
		$qdata = $qSql->fetchAll(PDO::FETCH_ASSOC);
		foreach ($qdata as $qrow):
		$question_id	= $qrow['question_id'];
		$shuffle		= ( ! empty($qrow['shuffle'])) ? TRUE : FALSE;
		$ansData		= $db->getAnswersByQuestionId(md5($question_id), $shuffle);
		$qtype	 		= $qrow['question_type'];
		$show	 		= ($q == 1) ? 'style="display:block"' : 'style="display:none"'; ?>
		<div class="MatchingQusBannerlunch MatchingQusBanner_<?php echo $q; ?> multimatchingB" <?php echo $show; ?>>
			<div class="Matchingcontainer">
				<div class="Questmtf">
					<div class="MatchingQusText fade-in">
						<div class="qus-text">
							<h3>QUESTION <?php echo $q .'/'. $totalQ; ?></h3>
							<p class="more"><?php echo stripslashes($qrow['questions']); ?></p>
						</div>
						<div class="qusassestbox">
							<div class="Qusimg">
							<?php if ( ! empty($qrow['audio']) && file_exists($root_path . $qrow['audio'])): ?>
							<audio id="qvideo-<?php echo $q ?>" class="video-js vjs-theme-<?php echo $vtheme ?>" controls preload="auto" width="300px" height="100px" data-setup="{}" data-type="audio" poster="<?php echo $aPoster ?>">
								<source src="<?php echo $path . $qrow['audio'] ?>" type="audio/<?php echo strtolower(pathinfo($qrow['audio'], PATHINFO_EXTENSION)); ?>" />
							</audio>
							<?php elseif ( ! empty($qrow['video']) && file_exists($root_path . $qrow['video'])): ?>
							<a href="<?php echo $path . $qrow['video']; ?>" data-fancybox data-caption="<?php echo stripslashes($qrow['questions']); ?>" class="qassets_icon">
								<video id="qvideo-<?php echo $q ?>" class="video-js vjs-theme-<?php echo $vtheme ?>" controls preload="auto" width="640" height="264" data-setup="{}" data-type="video">
									<source src="<?php echo $path . $qrow['video']; ?>" type="video/<?php echo strtolower(pathinfo($qrow['video'], PATHINFO_EXTENSION)); ?>" />
								</video>
							</a>
							<?php elseif ( ! empty($qrow['screen']) && file_exists($root_path . $qrow['screen'])): ?>
							<a href="<?php echo $path . $qrow['screen']; ?>" data-fancybox data-caption="<?php echo stripslashes($qrow['questions']); ?>" class="qassets_icon">
								<video id="qvideo-<?php echo $q ?>" class="video-js vjs-theme-<?php echo $vtheme ?>" controls preload="auto" width="200" height="100" data-setup="{}" data-type="video">
									<source src="<?php echo $path . $qrow['screen']; ?>" type="video/<?php echo strtolower(pathinfo($qrow['screen'], PATHINFO_EXTENSION)); ?>" />
								</video>
							</a>
							<?php elseif ( ! empty($qrow['image']) && file_exists($root_path . $qrow['image'])): ?>
							<a href="<?php echo $path . $qrow['image']; ?>" data-fancybox data-caption="<?php echo stripslashes($qrow['questions']); ?>" class="qassets_icon">
								<img class="img-fluid" src="<?php echo $path . $qrow['image']; ?>">
							</a>
							<?php elseif ( ! empty($qrow['document']) && file_exists($root_path . $qrow['document'])): ?>
							<a href="javascript:;" data-src="<?php echo $path . $qrow['document']; ?>" data-fancybox data-type="iframe" data-caption="<?php echo stripslashes($qrow['questions']); ?>" class="document">
								<img class="img-fluid pdfclissicsIm" src="<?php echo $db->getBaseUrl('img/icons/pdf_icon.svg'); ?>" />
							</a>
							<?php elseif ( ! empty($qrow['speech_text'])): ?>
							<button onclick="speak('div.intro_<?php echo $q ?>')">
								<i id="playicon" class="fa fa-play playicon" aria-hidden="true"></i>
								<i id="pauseicon" class="fa fa-pause pauseicon" style="display:none" aria-hidden="true"></i></button>
								<div class="intro_<?php echo $q ?>" style="display:none;"><?php echo $qrow['speech_text'] ?></div>
							<?php endif; ?>
							</div>
						</div>
					</div>
				</div>
				<?php #--------MTF--------
				if ($qtype == 1): ?>
				<div class="DestmatchingDrop">
					<div class="Drag_Drop Drag_Dropmultitext" id="option_<?php echo $q; ?>" data-qtype="<?php echo $qtype ?>">
						<div class="row desktop_multiple_DND">
							<div class="fixingHeight">
								<div class="col-sm-4 fade-in">
								<?php 
								$mtf_choice_sql = "SELECT answer_id, question_id, choice_option, match_option FROM answer_tbl WHERE question_id = '". $qrow['question_id'] ."' ORDER BY (CASE WHEN choice_order_no != 0 THEN choice_order_no WHEN choice_order_no = 0 THEN RAND() END)";
								$mtf_choice = $db->prepare($mtf_choice_sql); $mtf_choice->execute();
								if ($mtf_choice->rowCount() > 0):
									$mtf_choice_data = $mtf_choice->fetchAll(PDO::FETCH_ASSOC);
									foreach ($mtf_choice_data as $mtf_choice_row): ?>
									<div class="flexheight33 questioninput option_brand">
										<div class="textwraping dtwraping option_brand"><?php echo stripslashes($mtf_choice_row['choice_option']); ?></div>
									</div>
								<?php endforeach; endif; ?>
								</div>
								<div class="col-sm-4 fade-in">
									<?php $mtf_choice->execute(); 
									if ($mtf_choice->rowCount() > 0): $mtf1 = 1;
									foreach ($mtf_choice_data as $mtf_choice_row): ?>
									<div class="flexheight33 FHdrop mtf_ordered_list">
										<div href="tgt<?php echo $mtf1.'_'.$q ?>" id="tgt<?php echo $mtf1.'_'.$q ?>" class="dragBox option_brand drop dragBox<?php echo $mtf1.'_'.$q ?>" data-qtype="<?php echo $qtype ?>" data-qid="<?php echo $mtf_choice_row['question_id'] ?>" data-aid="<?php echo $mtf_choice_row['answer_id'] ?>" data-choice-option="<?php echo $mtf_choice_row['answer_id'] ?>"></div>
									</div>
									<?php $mtf1++; endforeach; endif; ?>
								</div>
								<div class="col-sm-4 fade-in">
								<?php $mtf_match_sql = "SELECT answer_id, match_option FROM answer_tbl WHERE question_id = '". $qrow['question_id'] ."' ORDER BY (CASE WHEN match_order_no != 0 THEN match_order_no WHEN match_order_no = 0 THEN RAND() END)";
								$mtf_match = $db->prepare($mtf_match_sql); $mtf_match->execute();
								if ($mtf_match->rowCount() > 0): $mtf2 = 1;
								foreach ($mtf_match->fetchAll(PDO::FETCH_ASSOC) as $mtf_match_row): ?>
								<div class="flexheight33 Optioninput">
									<div class="rectangle1 dragdrop option_brand" id="drg<?php echo $mtf2.'_'.$q ?>" data-match-option="<?php echo $mtf_match_row['answer_id'] ?>">
										<div class="dndopt"><?php echo stripslashes($mtf_match_row['match_option']); ?></div>
									</div>
								</div>
								<?php $mtf2++; endforeach; endif; ?>
								</div>
							</div>
							<div class="submitMTFcenter col-sm-12">
								<button type="button" id="btn_<?php echo $q; ?>" class="btn submitbtn fade-in" data-show-report="<?php echo ($totalQ == $q) ? 'true' : 'false'; ?>" data-target="<?php echo $q + 1 ?>" data-cur="<?php echo $q ?>" data-sim-id="<?php echo $sim_id ?>" data-qid="<?php echo $qrow['question_id'] ?>">SUBMIT</button>
							</div>
						</div>
					</div>
				</div>
				<?php #------SEQUENCE--------
				elseif ($qtype == 2): ?>
				<div class="MatchingOption" id="option_<?php echo $q; ?>" data-qtype="<?php echo $qtype ?>">
					<ul id="MatchingOptiondragble" class="Matchingdstyle advertise_container fade-in active">
					<?php $sq_sql = "SELECT answer_id, question_id, choice_option FROM answer_tbl WHERE question_id = '". $qrow['question_id'] ."' ORDER BY RAND()";
					$sq_choice = $db->prepare($sq_sql); $sq_choice->execute();
					if ($sq_choice->rowCount() > 0):
					$sq_choice_data = $sq_choice->fetchAll(PDO::FETCH_ASSOC);
					foreach ($sq_choice_data as $sq_choice_row): ?>
					<li class="draglistbox option_brand" data-qtype="<?php echo $qtype ?>" data-qid="<?php echo $sq_choice_row['question_id'] ?>" data-aid="<?php echo $sq_choice_row['answer_id'] ?>">
						<div class="dragbanner">
							<div class="dragIconContainer"><img src="<?php echo $db->getBaseUrl('img/qus_icon/white_dots.svg') ?>" class="dragIcon" /></div>
							<div class="seqoptions"><?php echo stripslashes($sq_choice_row['choice_option']); ?></div>
						</div>
					</li>
					<?php endforeach; endif; ?>
					</ul>
					<div class="submitMTFcenter col-sm-12">
						<button type="button" id="btn_<?php echo $q; ?>" class="btn submitbtn fade-in" data-show-report="<?php echo ($totalQ == $q) ? 'true' : 'false'; ?>" data-target="<?php echo $q + 1 ?>" data-cur="<?php echo $q ?>" data-sim-id="<?php echo $sim_id ?>" data-qid="<?php echo $qrow['question_id'] ?>">SUBMIT</button>
					</div>
				</div>
				<?php #-----SORTING--------
				elseif ($qtype == 3): ?>
				<div class="Drag_Drop IMGMATHCHDD" id="option_<?php echo $q; ?>" data-qtype="<?php echo $qtype ?>">
					<div class="row desktop_SORTING">
						<div class="DestmatchingDrop fade-in">
							<div class="Dflex-banner">
								<?php $sort_choice_sql = "SELECT answer_id, question_id, choice_option, image FROM answer_tbl WHERE question_id = '". $qrow['question_id'] ."'";
								$sort_choice = $db->prepare($sort_choice_sql); $sort_choice->execute();
								if ($sort_choice->rowCount() > 0):
								$sort_choice_data = $sort_choice->fetchAll(PDO::FETCH_ASSOC);
								foreach ($sort_choice_data as $sort_choice_row): ?>
								<div class="Dflex option_brand">
									<div class="multiDBDbanner">
										<div class="multiDBDbox option_brand">
											<?php if ( ! empty($sort_choice_row['image']) && file_exists($root_path . $sort_choice_row['image'])): ?>
											<a href="<?php echo $path . $sort_choice_row['image']; ?>" data-fancybox data-caption="<?php echo $sort_choice_row['choice_option']; ?>" class="qassets_icon"><img class="feedimage" src="<?php echo $path . $sort_choice_row['image'] ?>" /></a>									
											<?php endif; ?>
											<div class="textwraping"><?php echo $sort_choice_row['choice_option']; ?></div>
										</div>
									</div>
								</div>
								<?php endforeach; endif; ?>
							</div>
							<div class="Dflex-banner Dflex-bannerDG">
								<?php $sort_choice->execute(); foreach ($sort_choice_data as $sort_choice_row): ?>
								<div class="multiDropbox drop1 option_brand" data-qid="<?php echo $sort_choice_row['question_id'] ?>" data-aid="<?php echo $sort_choice_row['answer_id'] ?>" data-choice-option="<?php echo $sort_choice_row['answer_id'] ?>"></div>
								<?php endforeach; ?>
							</div>
							<div class="dropareabox col-sm-12">
								<?php $sort_option_sql = "SELECT sub.sub_options_id, sub.sub_option FROM sub_options_tbl AS sub LEFT JOIN answer_tbl AS ans ON sub.answer_id = ans.answer_id WHERE ans.question_id = '". $qrow['question_id'] ."' ".(( ! empty($shuffle)) ? "ORDER BY RAND() ": "");
								$sort_option = $db->prepare($sort_option_sql); $sort_option->execute();
								if ($sort_option->rowCount() > 0): $q4 = 1;
								$sort_option_data = $sort_option->fetchAll(PDO::FETCH_ASSOC);
								foreach ($sort_option_data as $sort_option_row): ?>
								<div id="multidrag<?php echo $q4 ?>" class="box navy dragdrop1 sorting_dragdrop option_brand" data-match-option="<?php echo $sort_option_row['sub_options_id'] ?>">
									<?php echo stripslashes($sort_option_row['sub_option']); ?>
								</div>
								<?php $q4++; endforeach; endif; ?>
							</div>
						</div>
						<div class="submitMTFcenter col-sm-12">
							<button type="button" id="btn_<?php echo $q; ?>" class="btn submitbtn fade-in" data-show-report="<?php echo ($totalQ == $q) ? 'true' : 'false'; ?>" data-target="<?php echo $q + 1 ?>" data-cur="<?php echo $q ?>" data-sim-id="<?php echo $sim_id ?>" data-qid="<?php echo $qrow['question_id'] ?>">SUBMIT</button>
						</div>
					</div>
				</div>
				<?php #------MCQ--------
				elseif ($qtype == 4): ?>
				<div class="MCQOption bannermcq" id="option_<?php echo $q; ?>" data-qtype="<?php echo $qtype ?>">
					<?php foreach ($ansData as $mcq_data): ?>
					<div class="row options option_brand advertise_container fade-in" data-qtype="<?php echo $qtype ?>" data-qid="<?php echo $mcq_data['question_id'] ?>" data-aid="<?php echo $mcq_data['answer_id'] ?>" data-box-id="<?php echo $q; ?>">
						<div class="multiQusChecK"><div class="Check-boxQB Check-boxMCQ"><i class="fa fa-circle" aria-hidden="true"></i></div></div>
						<div class="multiQustext"><?php echo stripslashes($mcq_data['choice_option']); ?></div>
					</div>
					<?php endforeach; ?>
					<div class="submitMTFcenter col-sm-12">
						<button type="button" id="btn_<?php echo $q; ?>" class="btn submitbtn fade-in" data-show-report="<?php echo ($totalQ == $q) ? 'true' : 'false'; ?>" data-target="<?php echo $q + 1 ?>" data-cur="<?php echo $q ?>" data-sim-id="<?php echo $sim_id ?>" data-qid="<?php echo $qrow['question_id'] ?>">SUBMIT</button>
					</div>
				</div>
				<?php #------MMCQ--------
				elseif ($qtype == 5): ?>
				<div class="MMCQOption bannermcq" id="option_<?php echo $q; ?>" data-qtype="<?php echo $qtype ?>">
					<?php foreach ($ansData as $mmcq_data): ?>
					<div class="row mmcq_options option_brand advertise_container fade-in" data-qtype="<?php echo $qtype ?>" data-qid="<?php echo $mmcq_data['question_id'] ?>" data-aid="<?php echo $mmcq_data['answer_id'] ?>" data-box-id="<?php echo $q; ?>">
						<div class="multiQusChecK"><div class="Check-boxQB"><i class="fa fa-check" aria-hidden="true"></i></div></div>
						<div class="multiQustext"><?php echo stripslashes($mmcq_data['choice_option']); ?></div>                     
					</div>
					<?php endforeach; ?>
					<div class="submitMTFcenter col-sm-12">
						<button type="button" id="btn_<?php echo $q; ?>" class="btn submitbtn fade-in" data-show-report="<?php echo ($totalQ == $q) ? 'true' : 'false'; ?>" data-target="<?php echo $q + 1 ?>" data-cur="<?php echo $q ?>" data-sim-id="<?php echo $sim_id ?>" data-qid="<?php echo $qrow['question_id'] ?>">SUBMIT</button>
					</div>
				</div>
				<?php #------SWIPING------
				elseif ($qtype == 7): ?>
				<div class="MCQOption" id="option_<?php echo $q; ?>" data-qtype="<?php echo $qtype ?>">
					<div class="home-demo">
						<div class="row CoursalsTYbullet">
							<div class="large-12 columns">
								<div class="owl-carousel fade-in swipingcoursal">
								<?php foreach ($ansData as $swipe_data): ?>
								<div class="swipeoptions option_brand" data-qtype="<?php echo $qtype ?>" data-qid="<?php echo $swipe_data['question_id'] ?>" data-aid="<?php echo $swipe_data['answer_id'] ?>">
									<div class="multiDBDboxAllIGN">
										<?php $swip_cls = 'dd_twrap'; if ( ! empty($swipe_data['image']) && file_exists($root_path . $swipe_data['image'])): $swip_cls = ''; ?>
										<a href="<?php echo $path . $swipe_data['image']; ?>" data-fancybox data-caption="<?php echo stripslashes($swipe_data['choice_option']); ?>" class="qassets_icon"><img class="feedimage" src="<?php echo $path . $swipe_data['image'] ?>" /></a>
										<?php endif; ?>
										<div class="textwraping <?php echo $swip_cls; ?>"><?php echo stripslashes($swipe_data['choice_option']); ?></div>
									</div>
								</div>
								<?php endforeach; ?>
								</div>
								<div class="submitMTFcenter col-sm-12">
									<button type="button" id="btn_<?php echo $q; ?>" class="btn submitbtn fade-in" data-show-report="<?php echo ($totalQ == $q) ? 'true' : 'false'; ?>" data-target="<?php echo $q + 1 ?>" data-cur="<?php echo $q ?>" data-sim-id="<?php echo $sim_id ?>" data-qid="<?php echo $qrow['question_id'] ?>">SUBMIT</button>
								</div>
							</div>
						</div>
					</div>
				</div>
				<?php #------Single-D&D------
				elseif ($qtype == 8 && $qrow['seleted_drag_option'] == 2): ?>
				<div class="Drag_Drop imgDNDS Drag_Dropmultiimgtext" id="option_<?php echo $q; ?>" data-qtype="<?php echo $qtype ?>" data-drag-option="<?php echo $qrow['seleted_drag_option'] ?>">
					<div class="row desktop_multiple_DND">
						<div class="flex33 DCbanner fade-in">
						<?php $dnd_choice_sql = "SELECT question_id, sub_option, image FROM sub_options_tbl WHERE question_id = '". $qrow['question_id'] ."'";
						$dnd_choice = $db->prepare($dnd_choice_sql); $dnd_choice->execute();
						if ($dnd_choice->rowCount() > 0):
						$dnd_choice_data = $dnd_choice->fetchAll(PDO::FETCH_ASSOC);
						foreach ($dnd_choice_data as $dnd_choice_row): ?>
						<div class="flexheight33 multiDBDbanner DDaling DDaling1">
							<div class="multiDBDbox option_brand">
								<div class="multiDBDboxAllIGN">
									<?php if ( ! empty($dnd_choice_row['image']) && file_exists($root_path . $dnd_choice_row['image'])): ?>
									<a href="<?php echo $path . $dnd_choice_row['image']; ?>" data-fancybox data-caption="<?php echo stripslashes($dnd_choice_row['sub_option']); ?>" class="qassets_icon"><img src="<?php echo $path . $dnd_choice_row['image']; ?>" /></a>                                
									<?php endif; ?>
									<div class="textwraping <?php echo (empty($dnd_choice_row['image'])) ? 'dd_twrap' : ''; ?>"><?php echo stripslashes($dnd_choice_row['sub_option']); ?></div>
								</div>
							</div>
						</div>
						<?php endforeach; endif; ?>
						<div class="flexheight33 multiDBDbanner DROPAREA1">
							<?php $dndi = 1; foreach ($dnd_choice_data as $data): ?>
							<div class="dragBox option_brand drop dragBox<?php echo $dndi.'_'.$q ?> dragdrop2" data-qtype="<?php echo $qtype ?>" data-qid="<?php echo $data['question_id'] ?>"></div>
							<?php $dndi++; endforeach; ?>
						</div>
						</div>
						<div class="flex33 DROPclassicbanner fade-in">
							<?php if (count($ansData) > 0):
							foreach ($ansData as $dnd_match_row): ?>
							<div class="multiDBDbox dragdrop DDrop_multi option_brand" data-aid="<?php echo $dnd_match_row['answer_id'] ?>">
								<div class="multiDBDboxAllIGN">
								<?php if ( ! empty($dnd_match_row['image']) && file_exists($root_path . $dnd_match_row['image'])): ?>
								<a href="<?php echo $path . $dnd_match_row['image']; ?>" data-fancybox data-caption="<?php echo stripslashes($dnd_match_row['choice_option']); ?>" class="qassets_icon">
								<img src="<?php echo $path . $dnd_match_row['image']; ?>" /></a>
								<?php endif; ?>
								<div class="textwraping <?php echo (empty($dnd_match_row['image'])) ? 'dd_twrap' : ''; ?>"><?php echo stripslashes($dnd_match_row['choice_option']); ?></div>
								</div>
							</div>
						<?php endforeach; endif; ?>
						</div>
						<div class="submitMTFcenter col-sm-12">
							<button type="button" id="btn_<?php echo $q; ?>" class="btn submitbtn fade-in" data-show-report="<?php echo ($totalQ == $q) ? 'true' : 'false'; ?>" data-target="<?php echo $q + 1 ?>" data-cur="<?php echo $q ?>" data-sim-id="<?php echo $sim_id ?>" data-qid="<?php echo $qrow['question_id'] ?>">SUBMIT</button>
						</div>
					</div>
				</div>
				<?php #-----Multi-D&D------
				elseif ($qtype == 8 && $qrow['seleted_drag_option'] == 1): ?>
				<div class="Drag_Drop imgDNDS Drag_Dropmultiimgtext" id="option_<?php echo $q; ?>" data-qtype="<?php echo $qtype ?>" data-drag-option="<?php echo $qrow['seleted_drag_option'] ?>">
					<div class="row desktop_multiple_DND">
						<div class="DestmatchingDrop MATCHDNDDesktop">
							<div class="col-sm-3 fade-in">
							<?php 
							$dnd2_choice_sql = "SELECT answer_id, question_id, image, choice_option FROM answer_tbl WHERE question_id = '". $qrow['question_id'] ."' ".(($shuffle == TRUE) ? "ORDER BY RAND()" : "");
							$dnd2_choice_exe = $db->prepare($dnd2_choice_sql); $dnd2_choice_exe->execute();
							if ($dnd2_choice_exe->rowCount() > 0):
								$dnd2_choice_data = $dnd2_choice_exe->fetchAll(PDO::FETCH_ASSOC);
								foreach ($dnd2_choice_data as $dnd2_choice_row): ?>
								<div class="flexheight33 multiDBDbanner QTbAnnEr option_brand">
									<div class="multiDBDbox multiDBDboxAllIGN option_brand">
										<?php if ( ! empty($dnd2_choice_row['image']) && file_exists($root_path . $dnd2_choice_row['image'])): ?>
										<img src="<?php echo $path . $dnd2_choice_row['image']; ?>" />
										<?php endif; ?>
										<div class="textwraping <?php echo (empty($dnd2_choice_row['image'])) ? 'dd_twrap' : ''; ?>"><?php echo stripslashes($dnd2_choice_row['choice_option']); ?></div>
									</div>
								</div>
								<?php endforeach; endif; ?>
							</div>
							<div class="col-sm-3 fade-in droppoint">
								<?php foreach ($dnd2_choice_data as $dnd2_choice_row): ?>
								<div class="flexheight33 multiDBDbanner">
									<div href="tgt1" class="dragBox option_brand dragBox<?php echo $dnd2_choice_row['answer_id'] ?>  drop dragdrop_multi" data-qid="<?php echo $dnd2_choice_row['question_id'] ?>" data-aid="<?php echo $dnd2_choice_row['answer_id'] ?>" data-choice-option="<?php echo $dnd2_choice_row['answer_id'] ?>"></div>
								</div>
								<?php endforeach; ?>
							</div>
							<div class="col-sm-3 fade-in">
								<?php foreach ($dnd2_choice_data as $dnd2_choice_row): ?>
									<div class="flexheight33 multiDBDbanner"></div>
								<?php endforeach; ?>
							</div>
							<div class="col-sm-3 fade-in pickpoint">
								<?php $sql2dnd = "SELECT answer_id, image2, match_option FROM answer_tbl WHERE question_id = '". $qrow['question_id'] ."' ".(($shuffle == TRUE) ? "ORDER BY RAND()" : "");
								$dnd2exe = $db->prepare($sql2dnd); $dnd2exe->execute();
								if ($dnd2exe->rowCount() > 0):
								foreach ($dnd2exe->fetchAll(PDO::FETCH_ASSOC) as $dnd2data): ?>
								<div class="flexheight33 multiDBDbanner BGMUltTicolor">
									<div class="multiDBDbox maTcHingdrop MultiMatchRB swappable notswap option_brand" id="drg<?php echo $dnd2data['answer_id'] ?>" data-match-option="<?php echo $dnd2data['answer_id'] ?>">
										<div class="multiDBDboxAllIGN">
											<?php if ( ! empty($dnd2data['image2']) && file_exists($root_path . $dnd2data['image2'])): ?>
											<img src="<?php echo $path . $dnd2data['image2']; ?>" />
											<?php endif; ?>
											<div class="textwraping <?php echo (empty($dnd2data['image2'])) ? 'dd_twrap' : ''; ?>"><?php echo stripslashes($dnd2data['match_option']); ?></div>
										</div>
									</div>
								</div><?php endforeach; endif; ?>
							</div>
						</div>
						<div class="submitMTFcenter col-sm-12">
							<button type="button" id="btn_<?php echo $q; ?>" class="btn submitbtn fade-in" data-show-report="<?php echo ($totalQ == $q) ? 'true' : 'false'; ?>" data-target="<?php echo $q + 1 ?>" data-cur="<?php echo $q ?>" data-sim-id="<?php echo $sim_id ?>" data-qid="<?php echo $qrow['question_id'] ?>">SUBMIT</button>
						</div>
					</div>
				</div>
				<?php endif; ?>
			</div>
		</div>
		<!--FEEDBACK-->
		<?php $feedback = $db->getFeedback($question_id, NULL, NULL, NULL, TRUE);
		if ( ! empty($feedback)): $mtf = 1; foreach ($feedback as $feed_data): ?>
		<div class="modal feedback_box fade" role="dialog" id="Feedback_<?php echo $q ?>_<?php echo $feed_data['feedback_type'] ?>" data-keyboard="false" data-backdrop="static">
			<div class="modal-dialog modal-dialog-centered">
				<div class="modal-content">
					<div class="modal-header feedback-header">
						<button type="button" class="feedclose" data-show-report="<?php echo ($totalQ == $q) ? 'true' : 'false'; ?>" data-target="<?php echo $q + 1 ?>" data-cur="<?php echo $q ?>" data-dismiss="modal">&times;</button>
						<h3 class="modal-title" id="exampleModalLabel">FEEDBACK</h3>
					</div>
					<div class="modal-body">
						<div class="row feedtextarea">
							<div class="row feed">
								<div class="flexfeed">
									<div class="flexfeed img-videobox">
										<?php if ( ! empty($feed_data['feed_audio']) && file_exists($root_path . $feed_data['feed_audio'])): ?>
										<audio id="fvideo-1" class="video-js vjs-theme-<?php echo $vtheme ?>" controls preload="auto" width="300px" height="100px" data-setup="{}" data-type="audio" poster="<?php echo $aPoster ?>">
											<source src="<?php echo $path . $feed_data['feed_audio'] ?>" type="audio/<?php echo strtolower(pathinfo($feed_data['feed_audio'], PATHINFO_EXTENSION)); ?>" />
										</audio>
										<?php elseif ( ! empty($feed_data['feed_video']) && file_exists($root_path . $feed_data['feed_video'])): ?>
										<video id="fvideo-1" class="video-js vjs-theme-<?php echo $vtheme ?>" controls preload="auto" width="640" height="264" data-setup="{}" data-type="video">
											<source src="<?php echo $path . $feed_data['feed_video'] ?>" type="video/<?php echo strtolower(pathinfo($feed_data['feed_video'], PATHINFO_EXTENSION)); ?>" />
										</video>
										<?php elseif ( ! empty($feed_data['feed_screen']) && file_exists($root_path . $feed_data['feed_screen'])): ?>
										<video id="fvideo-1" class="video-js vjs-theme-<?php echo $vtheme ?>" controls preload="auto" width="640" height="264" data-setup="{}" data-type="video">
											<source src="<?php echo $path . $feed_data['feed_screen'] ?>" type="video/<?php echo strtolower(pathinfo($feed_data['feed_screen'], PATHINFO_EXTENSION)); ?>" />
										</video>
										<?php elseif ( ! empty($feed_data['feed_image']) && file_exists($root_path . $feed_data['feed_image'])): ?>
										<a href="<?php echo $path . $feed_data['feed_image']; ?>" data-fancybox data-caption="<?php echo stripslashes($feed_data['feedback']); ?>">
											<img class="img-fluid" src="<?php echo $path . $feed_data['feed_image']; ?>" />
										</a>
										<?php elseif ( ! empty($feed_data['feed_document']) && file_exists($root_path . $feed_data['feed_document'])): ?>
										<a href="javascript:;" data-src="<?php echo $path . $feed_data['feed_document']; ?>" data-fancybox data-type="iframe" data-caption="<?php echo stripslashes($feed_data['feedback']); ?>" class="document">
											<img class="img-fluid pdfclissicsIm" src="<?php echo $db->getBaseUrl('img/icons/pdf_icon.svg'); ?>">
										</a>
										<?php elseif ( ! empty($feed_data['feed_speech_text'])): ?>
											<button onclick="speak_feedback('div.feedback_intro_<?php echo $mtf ?>')"><i id="playicon_feedback" class="fa fa-play playicon" aria-hidden="true"></i><i id="pauseicon_feedback" class="fa fa-pause pauseicon" style="display:none" aria-hidden="true"></i></button>
											<div class="feedback_intro_<?php echo $mtf ?>" style="display:none;"><?php echo $feed_data['feed_speech_text'] ?></div>
										<?php endif; ?>
									</div>
									<div class="feedbacktext"><?php echo ( ! empty($feed_data['feedback'])) ? stripslashes($feed_data['feedback']) : ''; ?></div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<?php $mtf++; endforeach; endif; $q++; endforeach; endif; ?>
	</div>
	<?php if ( ! empty($right_char)): ?>
	<div class="CharFlextwo right">
		<img class="img-fulid qusTempimg" src="<?php echo $db->getBaseUrl($right_char); ?>">
	</div>
	<?php endif; ?>
	<div class="bttn_form scorm_publishs">
    	<form method="post">
        	<input type="hidden" name="sim_name" value="<?php echo str_replace(' ', '-', trim($title)); ?>" />
            <?php if ($type == 'zip'): ?>
        	<input type="submit" class="btn btn-primary" name="web_obj_publish" id="web_obj_publish" value="Publish as Web Object" />
            <?php elseif ($type == 'scrom'): ?>
			<input type="hidden" name="scorm_type" value="linear_multi" />
            <input type="submit" class="btn btn-primary" name="scorm_publish" id="scorm_publish" value="SCORM Publish" />
            <?php endif; ?>
        </form>
    </div>
</div>

<div id="load_popup_modal_show" class="modal fade" tabindex="-1" data-keyboard="false" data-backdrop="static"></div>

<script src="<?php echo $db->getBaseUrl('content/js/moreless.js'); ?>"></script>
<script src="<?php echo $db->getBaseUrl('content/js/articulate.js'); ?>"></script>
<script src="<?php echo $db->getBaseUrl('content/js/owlcarousel/owl.carousel.min.js'); ?>"></script>
<script src="<?php echo $db->getBaseUrl('content/js/sim-branding.js'); ?>"></script>
<script type="text/javascript">
	$('.CharFlex.CharFlextwo').insertBefore('.Questmtf');

	$('.CharFlextwo.right').insertAfter('.Questmtf');
	
	$('a.qassets_icon').fancybox({
		'titleShow': true,
		'transitionIn': 'elastic',
		'transitionOut': 'elastic',
		'easingIn': 'easeOutBack',
		'easingOut': 'easeInBack'
	});

	$('.document').fancybox({
		openEffect: 'elastic',
		closeEffect: 'elastic',
		autoSize: true,
		type: 'iframe',
		iframe: { preload: false }
	});

	$('body').on('click', '.view_scenario', function(e){
		e.preventDefault();
		var to = $(this).attr('data-src');
		var iframe = $.fancybox.open({
			type: 'iframe',
			src: to,
			toolbar  : false,
			smallBtn : true,
			closeExisting: false,
			scrolling: 'no',
			iframe : {
				preload : true,
				scrolling: 'no',
				css:{
					width:'100%',
					height:'100%'
				}
			},
			afterLoad:function(){
				$('.fancybox-iframe').contents().find('.footergray').hide();
			}
		});
	});

	var owl = $('.owl-carousel');
	owl.owlCarousel({
		margin: 10,
		loop: false,
		center: true,
		responsive: {
			0: { items: 1 },
			600: { items: 2 },
			1000: { items: 4 }
		}
	});

	function speak(obj) {
		var speaking = $().articulate('isSpeaking');
		var paused = $().articulate('isPaused');

		// This is how you can use one button for a speak/pause toggle
		// Is browser speaking? (This returns 'true' even when paused)
		// If it is, is speaking paused? If paused, then resume; otherwise pause
		// If it isn't, then initiate speaking

		if (speaking) {
			if (paused) {
				$().articulate('resume');
			} else {
				$().articulate('pause');
			}
		} else {
			$(obj).articulate('speak');
		};

		var playicon = document.getElementById("playicon");
		var pauseicon = document.getElementById("pauseicon");

		if (playicon.style.display === 'none') {
			playicon.style.display = "block";
			pauseicon.style.display = "none";
		} else {
			playicon.style.display = "none";
			pauseicon.style.display = "block";
		}
	};

	function speak_feedback(obj) {
		var speaking = $().articulate('isSpeaking');
		var paused = $().articulate('isPaused');

		// This is how you can use one button for a speak/pause toggle
		// Is browser speaking? (This returns 'true' even when paused)
		// If it is, is speaking paused? If paused, then resume; otherwise pause
		// If it isn't, then initiate speaking

		if (speaking) {
			if (paused) {
				$().articulate('resume');
			} else {
				$().articulate('pause');
			}
		} else {
			$(obj).articulate('speak');
		};

		var playicon_feedback = document.getElementById("playicon_feedback");
		var pauseicon_feedback = document.getElementById("pauseicon_feedback");

		if (playicon_feedback.style.display === 'none') {
			playicon_feedback.style.display = "block";
			pauseicon_feedback.style.display = "none";
		} else {
			playicon_feedback.style.display = "none";
			pauseicon_feedback.style.display = "block";
		}
	};

	var selectedDiv = null;
	var swappableIndex = 0;
	$('.pickpoint .swappable').each(function() {
		$(this).attr('data-pos', swappableIndex);
		swappableIndex++;
	})
	$('.pickpoint .swappable').bind('click',function(div){
		$('.pickpoint .swappable').removeClass('bordered');    
		selectedDiv=$(this).attr('id');
		$(this).addClass('bordered');
	});
	$('.droppoint .dragBox').bind('click',function(div){
		//$(this).before($("#" + selectedDiv));
		$(this).append($("#" + selectedDiv)).find('.bordered').removeClass('bordered');
		selectedDiv = null;
	});
	$('body').on('click', '.droppoint .dragBox .swappable',function(){
		var prePos = $(this).attr('data-pos');
		$('.pickpoint .flexheight33').eq(prePos).append($(this));
	});

	$(function() {
		$('ul.Matchingdstyle').sortable({
			update: function(evt) {
				//console.log(JSON.stringify($(this).sortable('serialize')));
			}
		});
		
		//draggable
		$('.dragdrop').draggable({
			revert: true,
			placeholder: true,
			droptarget: '.drop',
			scroll:true,
			drag: function(evt){+
				console.log(evt);
			},
			drop: function(evt, droptarget) {
				//$(this).appendTo(droptarget).draggable();
				if ($(droptarget).find('.dragdrop').length == 0){
					//== empty drop target, simply drop
					$(this).appendTo(droptarget).draggable();
				} else {
					//== swapping drops
					var d1 = this;
					var d2 = $(droptarget).find('.dragdrop').eq(0);
					$(d2).appendTo($(d1).parent()).draggable();
					$(d1).appendTo(droptarget).draggable();
				}
			}
		});
		
		//draggable
		$('.dragdrop1').draggable({
			revert: true,
			placeholder: true,
			droptarget: '.drop1',
			drop: function(evt, droptarget) {
				$(this).appendTo(droptarget).draggable();
			}
		});
	});
	
	$('.options').click(function() {
		var aid = $(this).data('aid');
		var bid = $(this).data('box-id');
		$('#btn_'+ bid).attr('data-aid', aid);
		$('.options').removeClass('press');
		$(this).addClass('press');
		$('.btn').removeAttr('disabled', true);
	});
	
	$('.mmcq_options').click(function() {
		var aid = $(this).data('aid');
		var bid = $(this).data('box-id');
		$('#btn_'+ bid).attr('data-aid', aid);
		$(this).toggleClass("press").checked = true;
		$('.btn').removeAttr('disabled', true);
	});
	
	function onDragStart(event) {
		event.dataTransfer.setData('text/plain', event.target.id);
		event.currentTarget.style.backgroundColor = '#fff';
	}
	
	function onDragOver(event) {
	  event.preventDefault();
	}
	
	function onDrop(event) {
		const id = event.dataTransfer.getData('text');
	 	const draggableElement = document.getElementById(id);
		const dropzone = event.target;
		dropzone.appendChild(draggableElement);
		event.dataTransfer.clearData();
	}
	
	//Function handleDragStart(), Its purpose is to store the id of the draggable element.
	function handleDragStart(e) {
		e.dataTransfer.setData("text", this.id); //note: using "this" is the same as using: e.target.
	}//end function


	//The dragenter event fires when dragging an object over the target. 
	//The css class "drag-enter" is append to the targets object.
	function handleDragEnterLeave(e) {
		if(e.type == "dragenter") {
			this.className = "drag-enter" 
		} else {
			this.className = "" //Note: "this" referces to the target element where the "dragenter" event is firing from.
		}
	}//end function

	//Function handles dragover event eg.. moving your source div over the target div element.
	//If drop event occurs, the function retrieves the draggable element’s id from the DataTransfer object.
	function handleOverDrop(e) {
		e.preventDefault(); 
  		//Depending on the browser in use, not using the preventDefault() could cause any number of strange default behaviours to occur.
		if (e.type != "drop") {
			return; //Means function will exit if no "drop" event is fired.
		}
		//Stores dragged elements ID in var draggedId
		var draggedId = e.dataTransfer.getData("text");
		//Stores referrence to element being dragged in var draggedEl
		var draggedEl = document.getElementById(draggedId);

		//if the event "drop" is fired on the dragged elements original drop target e.i..  it's current parentNode, 
		//then set it's css class to ="" which will remove dotted lines around the drop target and exit the function.
		if (draggedEl.parentNode == this) {
			this.className = "";
			return; //note: when a return is reached a function exits.
		}
		//Otherwise if the event "drop" is fired from a different target element, detach the dragged element node from it's
		//current drop target (i.e current perantNode) and append it to the new target element. Also remove dotted css class. 
		draggedEl.parentNode.removeChild(draggedEl);
		this.appendChild(draggedEl); //Note: "this" references to the current target div that is firing the "drop" event.
		this.className = "";
	}//end Function

	//Retrieve two groups of elements, those that are draggable and those that are drop targets:
	var draggable = document.querySelectorAll('[draggable]')
	var targets = document.querySelectorAll('[data-drop-target]');
	//Note: using the document.querySelectorAll() will aquire every element that is using the attribute defind in the (..)

	//Register event listeners for the"dragstart" event on the draggable elements:
	for(var i = 0; i < draggable.length; i++) {
		draggable[i].addEventListener("dragstart", handleDragStart);
	}

	//Register event listeners for "dragover", "drop", "dragenter" & "dragleave" events on the drop target elements.
	for(var i = 0; i < targets.length; i++) {
		targets[i].addEventListener("dragover", handleOverDrop);
		targets[i].addEventListener("drop", handleOverDrop);
		targets[i].addEventListener("dragenter", handleDragEnterLeave);
		targets[i].addEventListener("dragleave", handleDragEnterLeave);
	}		
	
	//-----DRAG-DROP--------
	function allowDrop(ev) {
		ev.preventDefault();
	}
	
	function drag(ev) {
		ev.dataTransfer.setData("text", ev.target.id);
	}
	
	function drop(ev) {
		ev.preventDefault();
		var data = ev.dataTransfer.getData("text");
		var nodeName = ev.target.getAttribute('href');
		if (nodeName == null) {
			console.log("failed");
		}
		else {
			ev.target.appendChild(document.getElementById(data));
			$("div[href$="+nodeName+"]").addClass('success');
			var match_option = $('#'+ data).data('match-option');
			$('#'+ nodeName).attr('data-match-option', match_option);
		}
	}
	
	var SearchTag = function (qid) {
		var i = null;
		for (i = 0; quesJson.length > i; i += 1) {
			if (quesJson[i].ques.qid === qid) {
				return quesJson[i];
			}
		}
		return null;
	};

	var total_com_score = <?php echo $totalCompval ?>;

	var score = 0;

	//-----------Feedback-----------
	$('body').on('click', '.submitbtn', function(event) {
		event.preventDefault();
		$.LoadingOverlay("show");
		var curData	= $(this);
		var cur  	= curData.data('cur');
		var option	= $('#option_' + cur);
		var qtype 	= option.data('qtype');
		var report	= $(this).data('show-report');
		var target	= $(this).data('target');
		var current	= $(this).data('cur');
		var qid		= $(this).data('qid');
		var aid		= $(this).data('aid');
		var drag	= option.data('drag-option');
		var $modal_correct 	 = $('#Feedback_'+ cur +'_'+ 1);
		var $modal_incorrect = $('#Feedback_'+ cur +'_'+ 2);
		var score_add = '';
		/* MTF */
		if (qtype == 1) {
			$.LoadingOverlay("hide");
			var correct	= SearchTag(qid);
			$('#option_'+ cur +' .mtf_ordered_list').each(function(){
				var choice_option_id = $(this).children('div').data('choice-option');
				var match_option_id = $(this).children('div').children('div').data('match-option');
				if (choice_option_id != match_option_id) {
					$modal_incorrect.modal('show', { backdrop:'static', keyboard:false});
					return false;
				}
				else {
					score_add = true;
					$modal_correct.modal('show', { backdrop:'static', keyboard:false});
				}
			});
			if (score_add) {
				score += parseInt(correct.ques.QScore);
			}
			else {
				score += parseInt(0);
			}
		}
		/* SEQUENCE */
		else if (qtype == 2) {
			$.LoadingOverlay("hide");
			var correct = SearchTag(qid);
			var correct_sq = [];
			$.each(correct.ans, function(i, obj) {
				correct_sq.push({'aid': obj.aid});
			});
			var sq_data = [];
			$('#option_'+ cur +' ul.Matchingdstyle li').each(function(){
				sq_data.push({"aid": $(this).data('aid')});
			});
			if (JSON.stringify(correct_sq) == JSON.stringify(sq_data)){
				score += parseInt(correct.ques.QScore);
				$modal_correct.modal('show', { backdrop:'static', keyboard:false });
			} else {
				$modal_incorrect.modal('show', { backdrop:'static', keyboard:false });
				return false;
			}
		}
		/* SORTING */
		else if (qtype == 3) {
			$.LoadingOverlay("hide");
			var sort_data = [];
			$('#option_'+ cur +' div.sorting_dragdrop').each(function(){
				var sort_choice = $(this).parent('div');
				sort_data.push({"aid": sort_choice.data('choice-option'), "sid": $(this).data('match-option')});
			});
			var correct = SearchTag(qid);
			correctData = correct.ans['sub_ans'];
			correctIds  = [];
			$.each(correctData, function(index, value){
				$.each(value, function(indexi, valuei){
					correctIds.push({"aid": valuei['aid'], "sid": valuei['sid']});
				});
			});
			if (JSON.stringify(correctIds) == JSON.stringify(sort_data)){
				score += parseInt(correct.ques.QScore);
				$modal_correct.modal('show', { backdrop:'static', keyboard:false });
			} else {
				$modal_incorrect.modal('show', { backdrop:'static', keyboard:false });
				return false;
			}
		}
		/* MCQ */
		else if (qtype == 4) {
			$.LoadingOverlay("hide");
			var mcqdata = $('#option_'+ cur +' .press');
			var aid = mcqdata.data('aid');
			var mcq_check = '';
			var correct = SearchTag(qid);
			$.each(correct.ans, function(i, obj) {
				if (aid == obj.aid && obj.trueAns == 1) {
					mcq_check = true;
					return false;
				}
				else { mcq_check = false; }
			});
			if (mcq_check) {
				score += parseInt(correct.ques.QScore);
				$modal_correct.modal('show', { backdrop:'static', keyboard:false});
			} else {
				$modal_incorrect.modal('show', { backdrop:'static', keyboard:false});
			}
		}
		/* MMCQ */
		else if (qtype == 5) {
			$.LoadingOverlay("hide");
			var mmcq_data = SearchTag(qid);
			var correct   = mmcq_data.ques.trueQ;
			var nameArr   = correct.split(',');
			var correct_mmcq = [];
			$.each(nameArr, function(i, obj) {
				if (mmcq_data.ans[obj-1]) {
					correct_mmcq.push({'aid': mmcq_data.ans[obj-1]['aid']});
				}
			});
			var post_mmcq_data = [];
			$('#option_'+ cur +' .press').each(function() {
				post_mmcq_data.push({"aid": $(this).data('aid')});
			});
			if (JSON.stringify(correct_mmcq) == JSON.stringify(post_mmcq_data)){
				score += parseInt(mmcq_data.ques.QScore);
				$modal_correct.modal('show', { backdrop:'static', keyboard:false });
			} else {
				$modal_incorrect.modal('show', { backdrop:'static', keyboard:false });
				return false;
			}
		}
		/* SWIPING */
		else if (qtype == 7) {
			$.LoadingOverlay("hide");
			var swip = $('#option_'+ cur +' .owl-stage .center .swipeoptions');
			var aid	 = swip.data('aid');
			var swip_check = '';
			var correct = SearchTag(qid);
			$.each(correct.ans, function(i, obj) {
				if (aid == obj.aid && obj.trueAns == 1) {
					swip_check = true;
					return false;
				}
				else { swip_check = false; }
			});
			if (swip_check) {
				score += parseInt(correct.ques.QScore);
				$modal_correct.modal('show', { backdrop:'static', keyboard:false});
			} else {
				$modal_incorrect.modal('show', { backdrop:'static', keyboard:false});
			}
		}
		/* D&D Multiple */
		else if (qtype == 8 && drag == 1) {
			$.LoadingOverlay("hide");
			var correct	= SearchTag(qid);
			$('#option_'+ cur +' div.dragdrop_multi').each(function(){
				var choice_option_id = $(this).data('choice-option');
				var match_option_id = $(this).children('div').data('match-option');
				if (choice_option_id != match_option_id) {
					$modal_incorrect.modal('show', { backdrop:'static', keyboard:false});
					return false;
				}
				else {
					score_add = true;
					$modal_correct.modal('show', { backdrop:'static', keyboard:false});
				}
			});
			if (score_add) {
				score += parseInt(correct.ques.QScore);
			}
			else {
				score += parseInt(0);
			}
		}
		/* D&D Single */
		else if (qtype == 8 && drag == 2) {
			$.LoadingOverlay("hide");
			var dd_data = $('#option_'+ cur +' .dragdrop2');
			var aid = dd_data.children('div').data('aid');
			var dd_check = '';
			var correct = SearchTag(qid);
			$.each(correct.ans, function(i, obj) {
				if (aid == obj.aid && obj.trueAns == 1) {
					dd_check = true;
					return false;
				}
				else { dd_check = false; }
			});
			if (dd_check) {
				score += parseInt(correct.ques.QScore);
				$modal_correct.modal('show', { backdrop:'static', keyboard:false});
			} else {
				$modal_incorrect.modal('show', { backdrop:'static', keyboard:false});
			}
		}

		<?php if ($type == 'scrom'): ?> 
		if (report && score == total_com_score) {
			/* console.log('comp' + score); */
			init(score, 1);
		}
		else if (report && score < total_com_score) {
			/* console.log('comp-fail' + score); */
			init(score, 3);
		}
		else {
			/* console.log('incomp-fail' + score); */
			init(score, 2);
		}
		<?php endif; ?>
	});

	$('.feedclose').on('click', function(){
		var curData	= $(this);
		var cur  	= curData.data('cur');
		var next 	= curData.data('target');
		var report	= curData.data('show-report');
		if (report) {
			timer.trigger('click');
			swal({title: "Good job!", text: "You've attempted all questions.", buttons: false, icon: "success", closeOnClickOutside: false,  closeOnEsc: false});
		}
		else {
			$('.MatchingQusBanner_'+ cur).fadeOut();
			$('.MatchingQusBanner_'+ next).fadeIn();
		}
	});

	document.onreadystatechange = function () {
		if (document.readyState == "complete") {
			$.LoadingOverlay("hide");
		}
	};
</script>
<?php 
require_once 'includes/sim-page.php';
require_once 'includes/scorm-zip.php';
require_once 'includes/zip.php';
