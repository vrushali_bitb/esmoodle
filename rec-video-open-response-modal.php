<?php 
session_start();
if (isset($_SESSION['username'])) {
    $user   = $_SESSION['username'];
    $role   = $_SESSION['role'];
    $userid = $_SESSION['userId'];
	$domain = (isset($_SESSION['domain_name'])) ? $_SESSION['domain_name'] : $_SERVER['HTTP_HOST'];
}
else {
    header('location: index.php');
}
$path	= 'scenario/upload/'.$domain.'/';
$box_id	= (isset($_POST['box_id'])) ? $_POST['box_id'] : '';
$qid	= (isset($_POST['qid'])) ? $_POST['qid'] : '';
$type	= (isset($_POST['type'])) ? $_POST['type'] : '';
?>

<link href="videojs/node_modules/video.js/dist/video-js.min.css" rel="stylesheet">
<link href="videojs/dist/css/videojs.record.css" rel="stylesheet">

<script src="videojs/node_modules/video.js/dist/video.min.js"></script>
<script src="videojs/node_modules/recordrtc/RecordRTC.js"></script>
<script src="videojs/node_modules/webrtc-adapter/out/adapter.js"></script>
<script src="videojs/dist/videojs.record.js"></script>
<script src="videojs/examples/browser-workarounds.js"></script>
<style>
  /* change player background color */
  #myVideo {
      background-color: #9ab87a;
  }

  </style>
<div id="load_popup_modal_contant" class="OPTText openReSonceModel" role="dialog">
	<div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close stopRecord" data-dismiss="modal">&times;</button>
                <h3 class="modal-title">RECORD VIDEO</h3>
            </div>
            <div class="modal-body">
				<video id="myVideo" playsinline class="video-js vjs-default-skin"></video>
                <button type="button" class="btn1 submitbtn1 upload action" disabled="disabled">Insert</button>
                <button type="button" class="btn1 submitbtn1 stopRecord" data-dismiss="modal">Cancel</button>
                </div>                
            </div>
        </div>
     </div>
</div>
<script type="text/javascript">
var inputId  = '<?php echo $box_id; ?>';
/* eslint-disable */
var options = {
    controls: true,
	loop: false,
   	width: 565,
    height: 300,
    fluid: false,
    plugins: {
        record: {
            audio: true,
            video: true,
            maxLength: 10,
            debug: true,
			audioEngine: 'recordrtc',
			videoMimeType: 'video/webm;codecs=vp8,opus'
        }
    }
};

// apply some workarounds for opera browser
applyVideoWorkaround();

var player = videojs('myVideo', options, function() {
    // print version information at startup
    var msg = 'Using video.js ' + videojs.VERSION +
        ' with videojs-record ' + videojs.getPluginVersion('record') +
        ' and recordrtc ' + RecordRTC.version;
    videojs.log(msg);
});

// error handling
player.on('deviceError', function() {
    console.log('device error:', player.deviceErrorCode);
});

player.on('error', function(element, error) {
    console.error(error);
});

// user clicked the record button and started recording
player.on('startRecord', function() {
    console.log('started recording!');
	$('.upload').removeAttr('disabled', true);
});

// stop device stream only
$('.stopRecord').on('click', function(){
	player.record().stopStream();
});

// user completed recording and stream is available
player.on('finishRecord', function() {
    // the blob object contains the recorded data that
    // can be downloaded by the user, stored on server etc.
    // console.log('finished recording: ', player.recordedData);
	// Create an instance of FormData and append the video parameter that
    // will be interpreted in the server as a file
	
	$('.action').removeAttr('disabled', true);
	
	$('.upload').on('click', function(e) {
		e.preventDefault();
		e.stopImmediatePropagation();
		var data = player.recordedData;
		$('.upload').attr('disabled', true);
		var serverUrl	= 'includes/upload.php';
    	var formData	= new FormData();
    	formData.append('file', data, data.name);
    	formData.append('rec_video', '1');
		console.log('uploading recording:', data.name);
		player.record().stopStream();
		$.ajax({
			url: serverUrl,
            method: 'POST',
            data: formData,
            cache: false,
            processData: false,
            contentType: false,
            success: function (resdata) {
				$('#load_action_popup_modal_show').modal('hide');
				var res = $.parseJSON(resdata);
				if (res.success == true) {
					$('#file_name_'+ inputId).val(res.file_name);
					$('#action_'+ inputId).hide();
					$('#open_res_answer_'+ inputId).show();
					$('#open_answer_btn_'+ inputId).show();
					$('#open_res_answer_'+ inputId + ' .ORyouranswer a').attr('data-assets', res.file_name);
					$('#open_res_answer_'+ inputId + ' .ORyouranswer a').attr('data-path', res.path);
					$('#open_res_answer_'+ inputId + ' .ORAudiobox').hide();
					$('#open_res_answer_'+ inputId + ' .ORTextbox').hide();
					$('#open_res_answer_'+ inputId + ' .ORVideobox').show().html('<video width="100%" height="auto" controls muted><source src="<?php echo $path ?>'+ res.file_name +'" class="img-responsive img-fluid"></video>');
					$('#open_res_answer_'+ inputId + ' .delete_assets').show();
					$('#open_res_answer_'+ inputId + ' .delete_tts').hide();
					swal({text: res.msg, buttons: false, icon: "success", timer: 1000});
				}
				else if (res.success == false) {
					swal({text: res.msg, buttons: false, icon: "warning", timer: 1000});
				}
			},error: function() {
				swal({text: 'Oops. something went wrong please try again.', buttons: false, icon: "error", timer: 2000 });
			}
        });
	});
	
	// stop device stream only
	$('.stopRecord').on('click', function(){
		player.record().stopStream();
	});
});

$('.delete_rec_video').on('click', function () {
	var file_name	= $(this).attr("data-rec-video");
	var dataString	= 'delete='+true+'&rec_video='+file_name;
	if (file_name) {
		swal({
			title: "Are you sure?",
			text: "Delete this Record Video.",
			icon: "warning",
			buttons: [true, 'Delete'],
			dangerMode: true, }).then((willDelete) => { if (willDelete) {
				$.LoadingOverlay("show");
				$.ajax({
					url: "includes/delete-file.php",
					type: "POST",
					data: dataString,
					cache: false,
					success: function(resdata) {
						var res = $.parseJSON(resdata);
						if (res.success == false) {
							$.LoadingOverlay("hide");
							swal({text: res.msg, buttons: false, icon: "error", timer: 2000});
						}
						else if (res.success == true) {
							$('#file_name_'+ inputId).val('');
							$('.action').attr('disabled', true);
							$('.delete_rec_screen').attr('data-rec-screen', '');
							$('#open_res_answer_'+ inputId).hide();
							$('#open_res_answer_'+ inputId + ' .ORyouranswer a').attr('data-assets', '');
							$('#open_res_answer_'+ inputId + ' .ORyouranswer a').attr('data-path', '');
							$('#open_res_answer_'+ inputId + ' .ORAudiobox').hide().html('');
							$('.delete_rec_screen').attr('data-rec-screen', '');
							$.LoadingOverlay("hide");
							swal({text: res.msg, buttons: false, icon: "success", timer: 2000});
						}
					},error: function() {
						swal({text: 'Oops. something went wrong please try again.', buttons: false, icon: "error", timer: 2000});
					}
				});
			} else { swal({text: 'Your Record Video is safe', buttons: false, timer: 2000}); }
		});
	}
});
</script>
