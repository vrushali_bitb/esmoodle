<?php 
session_start();
if (isset($_SESSION['username'])) {
	$user	= $_SESSION['username'];
	$role	= $_SESSION['role'];
	$userid	= $_SESSION['userId'];
}
else {
    header('location: index.php');
}
require_once 'config/db.class.php';
require_once 'includes/learners_menu.php';
?>
<style type="text/css">
.password-strength {
  box-shadow: 0 4px 5px 0 rgba(0, 0, 0, 0.14), 0 1px 10px 0 rgba(0, 0, 0, 0.12), 0 2px 4px -1px rgba(0, 0, 0, 0.3);
}
.js-hidden {
  display: none;
}
</style>
<div class="bottomheader">
    <div class="container-fluid">
        <ul class="breadcrumb">
			<?php echo $db->breadcrumbs(); ?>
         </ul>
     </div>
</div>
<div class="user_managment changepassbanner">
    <div class="container-fluid">
		<div class="row">
			<div class="col-sm-12">
				<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
					<div class="panel panelCaollapse">
						<div class="panel-heading" role="tab" id="headingTwo">
							<h4 class="panel-title">
								<a class="" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
									Change Password
								</a>
							</h4>
						</div>
						<div id="collapseTwo" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingTwo" aria-expanded="true" style="">
							<div class="panel-body">				
							<form id="change_pwd_form" name="change_pwd_form" method="post" class="smart-form client-form password-strength">
							<input type="hidden" name="change_pwd" value="1" />
							<div class="usermanage-form">
								<div class="row">
									<div class="col-sm-6">
										<div class="form-group">
											<label for="password-input">New Password: <span class="d-inline-block" tabindex="0" data-toggle="tooltip" title="Add 8 charachters or more, lowercase letters, uppercase letters, numbers and symbols to make the password really strong!"><i class="fa fa-info-circle" aria-hidden="true"></i></span></label>
											<div class="register_password1">
												<input class="password-strength__input form-control" type="password" id="npwd" name="npwd" placeholder="Password" required="" autocomplete="off">
												<div class="input-group-append">
													<button class="password-strength__visibility btn btn-outline-secondary" type="button" style="margin-top: 8px;">
													<span class="password-strength__visibility-icon" data-visible="hidden"><i class="fa fa-eye-slash"></i></span>
													<span class="password-strength__visibility-icon js-hidden" data-visible="visible"><i class="fa fa-eye"></i></span></button>
												</div>
											</div>
											<!-- <input class="password-strength__input form-control" type="password" id="npwd" name="npwd" required autocomplete="off" />
											<div class="input-group-append">
											<button class="password-strength__visibility btn btn-outline-secondary" type="button">
											<span class="password-strength__visibility-icon" data-visible="hidden"><i class="fa fa-eye-slash"></i></span>
											<span class="password-strength__visibility-icon js-hidden" data-visible="visible"><i class="fa fa-eye"></i></span></button>
											</div> -->
											<small class="password-strength__error text-danger js-hidden">This symbol is not allowed!</small>
										</div>
										<div class="password-strength__bar-block progress1 mb-4">
										<div class="password-strength__bar progress-bar bg-danger" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
										</div>
									</div>
								</div>
								<div class="row">
								<div class="col-sm-6">
										<div class="form-group">
											<label for="password-input">Confirm password: <span class="d-inline-block" tabindex="0" data-toggle="tooltip" title="Add 8 charachters or more, lowercase letters, uppercase letters, numbers and symbols to make the password really strong!"><i class="fa fa-info-circle" aria-hidden="true"></i></span></label>
											<div class="input-group" id="show_hide_password1">
												<input type="password" class="form-control" id="rpwd" name="rpwd" required autocomplete="off" />
												<div class="input-group-addon">
													<a href="javascript:void(0);"><i class="fa fa-eye-slash" aria-hidden="true"></i></a>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div> 
							<div class="usermanage-add clearfix">
								<button type="submit" name="reg_user" class="btn btn-primary password-strength__submit btn" disabled="disabled">Submit</button>
							</div>
							</form>  
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
     </div>
</div>
<script src="content/js/pwd-script.js"></script>
<script type="text/javascript">
$(function () {
  $('[data-toggle="tooltip"]').tooltip();
})
$("#change_pwd_form").on('submit', (function(e) {
	e.preventDefault();
	$.LoadingOverlay("show");
	$.ajax({
		url: 'includes/process.php',
		type: "POST",
		data: new FormData(this),
		contentType: false,
		cache: false,
		processData:false,
		success: function(resdata) {
			var res = $.parseJSON(resdata);
			if (res.success == true) {
				$.LoadingOverlay("hide");
				swal({text: res.msg, buttons: false, icon: "success", timer: msgtimer});
				setTimeout(function() { window.location.href = res.return_url; }, time);
			}
			else if (res.success == false) {
				$.LoadingOverlay("hide");
				swal({text: res.msg, buttons: false, icon: "error", timer: msgtimer});
			}
		}, error: function() {
			$.LoadingOverlay("hide");
			swal({text: oopsmsg, buttons: false, icon: "error", timer: msgtimer });
		}
	});
}));
</script>
<?php 
require_once 'includes/footer.php';
