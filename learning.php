<?php 
session_start();
if (isset($_SESSION['username'])):
	$user 	= $_SESSION['username'];
	$role 	= $_SESSION['role'];
	$userid	= $_SESSION['userId'];
	$domain = (isset($_SESSION['domain_name'])) ? $_SESSION['domain_name'] : $_SERVER['HTTP_HOST'];
else:
    header('location: index.php');
endif;
require_once 'config/db.class.php';
require_once 'includes/learners_menu.php';
$sid	= (isset($_GET['sid'])) ? $_GET['sid'] : exit('error');
$db		= new DBConnection();
$result	= $db->getScenario($sid);
$path	= 'scenario/upload/'. $domain .'/'; ?>
<style type="text/css">
	body {
		overflow: hidden;
	}
	.text-hint-banner {
		text-align: center;
		width: 100%;
		margin-top:20px;
		height: calc(100vh - 125px);
    	overflow: hidden;
	}
	button.owl-prev {
		position: absolute;
		left: -80px;
		top: 37%;
		transform: translateY(-50%);
		background-color: #049805 !important;
		width: 20px;
		height: 20px;
		border-radius: 100% !important;
		color: #fff !important;
	}	
	button.owl-next {
		position: absolute;
		right: -80px;
		top: 37%;
		transform: translateY(-50%);
		background-color: #049805 !important;
		width: 20px;
		height: 20px;
		border-radius: 100% !important;
		color: #fff !important;
	}  
	.admin-dashboard.Ev2 .nav {
		padding-top: 5px;
		padding: 0 95px;
	}
	.owl-dots { display: none; }

	/*------------------phone-------------*/
	@media (min-width:320px) and (max-width: 767px) {
		button.owl-prev { left: -20px !important; }
		.admin-dashboard.Ev2 .nav { padding: 0 25px !important; }
		button.owl-next { right: -20px !important; }
	}
</style>
<div class="admin-dashboard Ev2 launchSce launchScev2">	
    <div class="container-fuild linearassestBG" style="position: relative;">
		<h2 class="launchhead"><?php echo ( ! empty($result['web_object_title'])) ? $result['web_object_title'] : ''; ?></h2>
        <a style="position: absolute; top: 10px; right: 15px;" href="launch.php?view=true&sid=<?php echo $sid; ?>"><button class="btn btn-primary">VIEW SCENARIO</button></a>
		<div class="text-hint-banner">
		<?php if ( ! empty($result['web_object']) && file_exists($path . $result['web_object'])):
			$wofn = '';
			if (file_exists($path . $result['web_object'] . '/story.html')):
				$wofn = 'story.html';
			elseif (file_exists($path . $result['web_object'] . '/index.html')):
				$wofn = 'index.html';
			elseif (file_exists($path . $result['web_object'] . '/a001index.html')):
				$wofn = 'a001index.html';
			endif; ?>
            <iframe src="<?php echo $path . $result['web_object'] . '/' . $wofn; ?>" width="100%" height="100%" frameborder="0"></iframe>
			<?php endif; ?>
		</div>
    </div>
</div>
<?php 
require_once 'includes/footer.php';
