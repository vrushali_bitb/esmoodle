<?php 
session_start();
if (isset($_SESSION['username'])) {
	$user = $_SESSION['username'];
	$role   = $_SESSION['role'];
    $userid = $_SESSION['userId'];
}
else {
	header('location: index.php');
}
require_once 'config/db.class.php';
$db		= new DBConnection;
$gid	= (isset($_POST['data-id'])) ? $_POST['data-id'] : FALSE;
$data	= ( ! empty($gid)) ? $db->getGroup($gid) : FALSE;
$path	= 'img/group_icon/'; ?>
<style>
input.form-control.multiselect-search {
    height: 34px;
}
.grid1 {
	padding-left:0px;
}
.grid2 {
	padding-right:0px;
}
#group_name {
	height: 47px;
}
.form-group.edit_group_txt {
    margin-bottom: 0;
}
.SplashImAGE{
    margin-bottom: 10px;
    display: block;
}
</style>
<div id="load_popup_modal_contant" class="" role="dialog">
    <div class="modal-dialog" style="margin-top:100px">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" style="color:#2C3545">UPDATE GROUP</h4>
            </div>
            <div class="modal-body">
            	<form enctype="multipart/form" name="edit_scenario_type_form" id="edit_scenario_type_form" method="post">
                    <div class="edit_group_div">
                        <div class="row">
							<div class="col-sm-6 grid1">
								<h5 for="scenario_name" style="margin-top: -6px;color:#2C3545;">Group Name *</h5>
								<input type="text" class="form-control" name="group_name" id="group_name" value="<?php echo $data['group_name'] ?>" required="required">
							</div>
							<div class="col-sm-6 grid2">
								<h5 for="scenario_name" style="margin-top: -6px;color:#2C3545;">Educator *</h5>
                                <div class="form-group form-select">
                                	<select class="form-control multiselect-ui selection" name="group_leaders" id="group_leaders" data-placeholder="Select Group Educator" required="required">
                                        <option value="0" selected="selected" disabled="disabled">Select Educator</option>
                                        <?php foreach ($db->getUserByRole("'educator'") as $educator): ?>
                                        <option value="<?php echo $educator['id'] ?>" <?php echo ( ! empty($data['group_leader']) && $data['group_leader'] == $educator['id']) ? 'selected="selected"' : ''; ?> title="<?php echo $educator['email'] ?>"><?php echo ucwords($educator['username']) ?> (<?php echo $educator['location'] ?>)</option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>
						</div>
                        <div class="row">
							<div class="form-group edit_group_txt">
								<input type="file" name="file" id="file" class="form-control file" />
								<input type="text" class="form-control controls" style="border-radius: 0px;" disabled placeholder="Upload Image">
							</div>
							<label class="SplashImAGE" for="Splash Image"> 
								<span id="splashImg_edit"><?php if ( ! empty($data['group_icon'] )): ?><a href="<?php echo $path.$data['group_icon'] ?>" target="_blank" title="View Icon"><?php echo $data['group_icon'] ?></a><?php endif; ?></span>
								<span id="EditGroupDelete" <?php echo (empty($data['group_icon'])) ? ' style="display:none;"' : ''; ?>><a href="javascript:void(0);" data-edit-group-name="<?php echo $data['group_icon'] ?>" class="edit_group_image" title="Delete Icon"><i class="fa fa-times" aria-hidden="true"></i></a></span>
							</label>
							<button class="browse btn btn-primary edit_group" type="button">Browse</button>
							<button type="button" name="upload" id="upload" class="btn btn-primary edit_group">Upload</button>
						</div>
                        
                        <input type="hidden" name="group_edit_image_name" id="group_edit_image_name" />
                        <input type="hidden" name="group_old_image_name" id="group_old_image_name" value="<?php echo $data['group_icon'] ?>" />
                    <div>
                    <h5 for="scenario_description" style="color:#2C3545;">Group Description</h5>
                    	<textarea name="group_description" id="group_description" class="form-control" style="border-radius:0px;"><?php echo $data['group_des'] ?></textarea>
                    </div>
                    <div class="modal-footer" style="text-align:center;">
						<input type="hidden" name="update_group_id" value="<?php echo $data['group_id'] ?>" />
                        <button type="submit" name="editGroup" id="editGroup" class="btn btn-outline btn-primary" style="width:134px" onClick="return confirm('Are you sure to update this group.?');">Update Group</button>
                    </div>
                </form>
            </div>
        </div>
     </div>
</div>
<script type="text/javascript">
$('#group_leaders').multiselect({
	includeSelectAllOption: false,
	filterPlaceholder: 'Search & select group',
	enableCaseInsensitiveFiltering : true,
	enableFiltering: true,
	buttonWidth: '250px',
	maxHeight: 250
});

$('#upload').on('click', function() {
	$('#splashImg_edit').show().html(' <img src="scenario/img/loader.gif"> Please wait....');
	var file_data = $('#file').prop('files')[0];
	var form_data = new FormData();
	form_data.append('file', file_data);
	form_data.append('group_icon_img', true);
	$.ajax({
		url: "includes/upload.php",
		type: "POST",
		data: form_data,
		contentType: false,
		cache: false,
		processData:false,
		success: function(resdata) {
			var res = $.parseJSON(resdata);
			if (res.success == true) {
				var viewHtml = '<a href="<?php echo $path ?>'+res.img_name+'" target="_blank" title="View Icon">'+res.img_name+'</a>';
				$('#splashImg_edit').html(viewHtml);
				$('#group_edit_image_name').val(res.img_name);
				$('#EditGroupDelete').show();
				$('.edit_group_image').attr('data-edit-group-name', res.img_name);
				swal({text: res.msg, buttons: false, icon: "success", timer: 1000});
			}
			else if (res.success == false) {
				$('#group_edit_image_name').val('');
				$('#splashImg_edit').hide('slow');
				swal({text: res.msg, buttons: false, icon: "error", timer: 1000});
			}
		},error: function() {
			$('#splashImg_edit').hide('slow');
			swal({text: 'Oops, something went wrong. Please try again later', buttons: false, icon: "error", timer: 1000 });
		}
	});
});
	
$('.edit_group_image').on('click', function () {
	var img_name	= $(this).attr("data-edit-group-name");
	var dataString	= 'delete='+true+'&group_icon='+img_name;
	if (img_name) {
		swal({
			title: "Are you sure?",
			text: "Delete this Icon.",
			icon: "warning",
			buttons: [true, 'Delete'],
			dangerMode: true, }).then((willDelete) => { if (willDelete) {
				$('#splashImg_edit').show().html(' <img src="scenario/img/loader.gif"> Please wait....');
				$.ajax({
					url: "includes/delete-file.php",
					type: "POST",
					data: dataString,
					cache: false,
					success: function(resdata) {
						var res = $.parseJSON(resdata);
						if (res.success == false) {
							swal({text: res.msg, buttons: false, icon: "error", timer: 1000});
							$('#splashImg_edit').hide('slow');
						}
						else if (res.success == true) {
							$('#splashImg_edit, #EditGroupDelete').hide('slow');
							$('#group_edit_image_name').val('');
							$('#group_old_image_name').val('');
							swal({text: res.msg, buttons: false, icon: "success", timer: 1000});
						}
					},error: function() {
						swal({text: 'Oops, something went wrong. Please try again later', buttons: false, icon: "error", timer: 1000 });
					}
				});
			} else { swal({text: 'Your Icon is safe', buttons: false, timer: 1000 }); }
		});
	}
});
		
$("#edit_scenario_type_form").on('submit', (function(e) {
	e.preventDefault();
	var form_data = $(this).serialize();
	$('#editScenarioType').attr('disabled', 'disabled').html('<img src="scenario/img/loader.gif"> Please wait....');
	$.ajax({
		url: "includes/process.php",
		type: "POST",
		data: form_data,
		success: function(result) {
			var res = $.parseJSON(result);
			if (res.success == true) {
				swal({text: res.msg, buttons: false, icon: "success", timer: 1000});
				setTimeout(function() { window.location.reload(); }, 2000);
			}
			else if (res.success == false) {
				swal({text: res.msg, buttons: false, icon: "error", timer: 1000});
				setTimeout(function() { window.location.reload(); }, 2000);
			}
		},error: function() { swal({text: 'Oops, something went wrong. Please try again later', buttons: false, icon: "error", timer: 1000 }); }
	});
}));
</script>
<?php 
ob_end_flush();
