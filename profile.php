<?php 
session_start();
if(isset($_SESSION['username'])) {
    $user 	= $_SESSION['username'];
	$role 	= $_SESSION['role'];
	$userid	= $_SESSION['userId'];
}
else {
    header('location: index.php');
}
require_once 'config/db.class.php';
require_once 'includes/learners_menu.php';
$obj	= new DBConnection;
$data	= $obj->getUsers(md5($userid));
$path	= 'img/profile/';
$img	= ( ! empty($data['img']) && file_exists($path.$data['img'])) ? $path.$data['img'] : $path.'edit-profile-img.jpg'; ?>
<div class="User-edit_profile">
    <div class="container-fluid">        
        <div class="edit_profile-banner">
            <div class="row">
				<div class="col-sm-12">
					<form enctype="multipart/form" name="profile_form" id="profile_form" method="post">
						<input type="hidden" name="update_profile" value="1" />
						<input type="hidden" name="update_profile_id" value="<?php echo $data['id'] ?>" />
						<input type="hidden" name="old_profile_img" value="<?php echo $data['img'] ?>" />
						<input type="hidden" name="old_profile_img1" value="<?php echo $data['img_thumb_50'] ?>" />
						<input type="hidden" name="old_profile_img2" value="<?php echo $data['img_thumb_125'] ?>" />
						<div class="edit-profilemainbanner">
							<div class="edit-profile-img">
								<div class="previewimgshow">
									<img class="img-fluid" id="uploadPreview" src="<?php echo $img; ?>">
									<input type="file" id="proFileImg" name="proFileImg" class="proImgName">
									<input type="hidden" name="proImgName" id="proImgName" value="" />
									<input type="hidden" name="proImgName1" id="proImgName1" value="" />
									<input type="hidden" name="proImgName2" id="proImgName2" value="" />
									<img class="edit-profile-icon" src="img/edit-profile.png" />
								</div> 
								<div class="previewtextshow">
									<div><span class="previewtextbold">User Name</span>: <span><?php echo $data['username'] ?></span></div>
									<div><span class="previewtextbold">Role</span>: <span><?php echo $data['role'] ?></span></div>
									<div><span class="previewtextbold">Registration Date</span>: <span><?php echo ($data['date'] != '0000-00-00') ? $obj->dateFormat($data['date'], 'd-m-Y') : '' ?></span></div>
								</div>	
								
								<ul class="ProfilesocalMedia">
									<li><a href="<?php echo ( ! empty($data['facebook'])) ? $data['facebook'] : 'javascript:void(0);'; ?>" target="_blank"><img src="img/facebook.png"></a></li>
									<li><a href="<?php echo ( ! empty($data['twitter'])) ? $data['twitter'] : 'javascript:void(0);'; ?>" target="_blank"><img src="img/twiter.png"></a></li>
									<li><a href="<?php echo ( ! empty($data['linkedIn'])) ? $data['linkedIn'] : 'javascript:void(0);'; ?>" target="_blank"><img src="img/linkdin.png"></a></li>
								</ul>							
							</div>
							<div class="edit-profilefield">
								<h3>Profile</h3>
								<div class="form-group edit-textentry">
									<div class="edit-field">
										<input type="text" class="form-control designation" name="fname" placeholder="First name" value="<?php echo $data['fname'] ?>" required="required">
									</div>
								</div>
								<div class="form-group edit-textentry">
									<div class="edit-field">
										<input type="text" class="form-control designation" name="lname" placeholder="Last name" value="<?php echo $data['lname'] ?>" required="required">
									</div>
								</div>
								<div class="form-group edit-textentry">
									<div class="edit-field">
										<input class="form-control designation" name="location" id="location" type="text" placeholder="Location goes here" value="<?php echo $data['location'] ?>">
									</div>
								</div>	
								<div class="form-group edit-textentry">
									<div class="edit-field">
										<input class="form-control designation" name="organization" id="organization" type="text" placeholder="Organization name goes here" value="<?php echo $data['company'] ?>" required="required">
									</div>
								</div>	
								<div class="form-group edit-textentry">
									<div class="edit-field">
										<input class="form-control designation" name="department" id="department" type="text" placeholder="Department name goes here" value="<?php echo $data['department'] ?>" required="required">
									</div>
								</div>
								<div class="form-group edit-textentry">
									<div class="edit-field">
										<input type="tel" class="form-control designation" name="mob" id="mob" max="10"  placeholder="Mobile number goes here" onkeypress="return isNumberKey(event);" value="<?php echo $data['mob'] ?>" />
									</div>
								</div>
								<div class="form-group edit-textentry">
									<div class="edit-field">
										<input type="email" class="form-control designation" name="email" placeholder="Email address goes here" id="email" value="<?php echo $data['email'] ?>" required="required" />
									</div>
								</div>
								<div class="form-group edit-textentry">
									<div class="edit-field">
										<input type="text" class="form-control designation" name="webpage" id="webpage" value="<?php echo $data['webpage'] ?>" placeholder="Enter the link to your Webpage">
									</div>
								</div>
								<div class="form-group edit-textentry">
									<div class="edit-field">
										<input type="url" class="form-control designation" name="facebook" id="facebook" value="<?php echo $data['facebook'] ?>" placeholder="Enter the link to your Facebook profile page">
									</div>
								</div>
								<div class="form-group edit-textentry">
									<div class="edit-field">
										<input type="url" class="form-control designation" name="twitter" id="twitter" value="<?php echo $data['twitter'] ?>" placeholder="Enter the link to your Twitter profile page">
									</div>
								</div>
								<div class="form-group edit-textentry">
									<div class="edit-field">
										<input type="url" class="form-control designation" name="linkedIn" id="linkedIn" value="<?php echo $data['linkedIn'] ?>" placeholder="Enter the link to your LinkedIn profile page">
									</div>
								</div> 
								<div class="edit-social-icon">
									<div class="profile-update-btn"> 
										<button type="submit" class="btn btn-primary update_profile">Update and Close</button>
									</div>
								</div>
							</div>
						</div>
					</form>
				</div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="content/js/bootstrap-datetimepicker.js" charset="UTF-8"></script>
<script type="text/javascript">	
	$('.date').datetimepicker({
		weekStart: 1,
		todayBtn: 0,
		autoclose: 1,
		todayHighlight: 1,
		startView: 2,
		forceParse: 0,
		minView: 2,
		showMeridian: 1,
		clearBtn: 1
	});
	function isNumberKey(evt) {
		var charCode = (evt.which) ? evt.which : evt.keyCode
		return !(charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57));
	}
	var path	= '<?php echo $path ?>';
	var noimg	= '<?php echo $path ?>picture-not-available.jpg';
	function validate(id) {
		var file_size = $(id)[0].files[0].size;
		if (file_size > 5242880) {
			swal("Error", "File size must be less than 5 MB.!", "error");
			return false;
		}
		return true;
	}
	//----------image-one--------------------------
	$('#proFileImg').change(function() {
		if (validate(this) != false) {
			$.LoadingOverlay("show");
			var file_data = $('#proFileImg').prop('files')[0];
			var form_data = new FormData();
			form_data.append('file', file_data);
			$.ajax({
				url: "upload-profile.php",
				type: "POST",
				data: form_data,
				contentType: false,
				cache: false,
				processData:false,
				success: function(resdata) {
					var res = $.parseJSON(resdata);
					if (res.success == true) {
						$('#proImgName').val(res.img_name);
						$('#proImgName1').val(res.img_name1);
						$('#proImgName2').val(res.img_name2);
						$('#uploadPreview').attr('src', path + res.img_name);
						$.LoadingOverlay("hide");
					}
					else if (res.success == false) {
						$('#proImgName').val('');
						swal("Error", res.msg, "error");
						$.LoadingOverlay("hide");
					}
				},error: function() {
					$.LoadingOverlay("hide");
					swal("Error", 'Oops, something went wrong. Please try again later.?', "error");
				}
			});
		}
	});
</script>
<?php 
require_once 'includes/footer.php';
