<?php 
session_start();
if (isset($_SESSION['username'])) {
	$user = $_SESSION['username'];
	$role   = $_SESSION['role'];
    $userid = $_SESSION['userId'];
}
else {
	header('location: index.php');
}
require_once 'config/db.class.php';
$db		 = new DBConnection;
$getid	 = (isset($_POST['data-id'])) ? $_POST['data-id'] : FALSE;
$getdata = ( ! empty($getid)) ? $db->getScenarioTypes($getid) : FALSE;
?>
<div id="load_popup_modal_contant" class="" role="dialog">
    <div class="modal-dialog" style="margin-top:100px">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h3 class="modal-title" style="color:#115961">Update Scenario Type</h3>
            </div>
            <div class="modal-body">
            	<form enctype="multipart/form" name="edit_scenario_type_form"  id="edit_scenario_type_form" method="post">
                	<div>
                        <label for="scenario_name">Scenario Type Name *</label>
                        <input type="text" class="form-control" name="scenario_name" id="txtname" value="<?php echo $getdata['type_name'] ?>" required="required">
                    </div>
                    <div>
                        <label for="Splash Image">Icon &nbsp;&nbsp;<span id="splashImg_edit">
                        <?php if ( ! empty($getdata['type_icon'] )): ?>
                        <a href="img/type_icon/<?php echo $getdata['type_icon'] ?>" target="_blank" title="View Icon"><i class="fa fa-eye" aria-hidden="true"></i></a>
                        <?php endif; ?></span> &nbsp;&nbsp; 
                        <span id="EditIconDelete" <?php echo (empty($getdata['type_icon'])) ? ' style="display:none;"' : ''; ?>><a href="javascript:void(0);" data-edit-icon-name="<?php echo $getdata['type_icon'] ?>" class="edit_icon_image" title="Delete Icon"><i class="fa fa-times" aria-hidden="true"></i></a></span></label>
                        <input class="form-control" type="file" name="icon_image_edit" id="icon_image_edit">
                        <input type="hidden" name="icon_edit_image_name" id="icon_edit_image_name" />
                        <input type="hidden" name="icon_old_image_name" id="icon_old_image_name" value="<?php echo $getdata['type_icon'] ?>" />
                    </div>
                    <div>
                    	<label for="scenario_description">Scenario Type Description</label>
                        <textarea name="scenario_description" id="scenario_description" class="form-control"><?php echo $getdata['type_des'] ?></textarea>
                    </div>
                    <div class="modal-footer">
						<input type="hidden" name="update_scenario_type_id" value="<?php echo $getdata['st_id'] ?>" />
                        <button type="submit" name="editScenarioType" id="editScenarioType" class="btn btn-outline btn-success" onClick="return confirm('Are you sure to update this Scenario Type.?');">Update</button>
						<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </form>
            </div>
        </div>
     </div>
</div>
<script type="text/javascript">
	$('#icon_image_edit').change(function() {
		$('#splashImg_edit').show().html(' <img src="scenario/img/loader.gif"> Please wait....');
		var file_data = $('#icon_image_edit').prop('files')[0];
		var form_data = new FormData();
		form_data.append('file', file_data);
		form_data.append('type_icon_img', true);
		$.ajax({
			url: "includes/upload.php",
			type: "POST",
			data: form_data,
			contentType: false,
			cache: false,
			processData:false,
			success: function(resdata) {
				var res = $.parseJSON(resdata);
				if (res.success == true) {
					var viewHtml = '<a href="img/type_icon/'+res.img_name+'" target="_blank" title="View Icon"><i class="fa fa-eye" aria-hidden="true"></i></a>';
					$('#splashImg_edit').html(viewHtml);
					$('#icon_edit_image_name').val(res.img_name);
					$('#EditIconDelete').show();
					$('.edit_icon_image').attr('data-edit-icon-name', res.img_name);
					swal({text: res.msg, buttons: false, icon: "success", timer: 1000});
				}
				else if (res.success == false) {
					$('#icon_edit_image_name').val('');					
					$('#splashImg_edit').hide('slow');
					swal({text: res.msg, buttons: false, icon: "error", timer: 1000});
				}
			}, error: function() {
				$('#splashImg_edit').hide('slow');
				swal({text: 'Oops, something went wrong. Please try again later', buttons: false, icon: "error", timer: 1000 });
			}
		});
	});
	
	$('.edit_icon_image').on('click', function () {
		var img_name	= $(this).attr("data-edit-icon-name");
		var dataString	= 'delete='+ true + '&type_icon='+img_name;
		if (img_name) {
			swal({
				title: "Are you sure?",
				text: "Delete this Icon.",
				icon: "warning",
				buttons: [true, 'Delete'],
				dangerMode: true, }).then((willDelete) => { if (willDelete) {
					$('#splashImg_edit').show().html(' <img src="scenario/img/loader.gif"> Please wait....');
					$.ajax({
						url: "includes/delete-file.php",
						type: "POST",
						data: dataString,
						cache: false,
						success: function(resdata) {
							var res = $.parseJSON(resdata);
							if (res.success == false) {									
								$('#splashImg_edit').hide('slow');
								swal({text: res.msg, buttons: false, icon: "error", timer: 1000});
							}
							else if (res.success == true) {
								$('#splashImg_edit, #EditIconDelete').hide('slow');
								$('#icon_old_image_name').val('');
								$('#icon_edit_image_name').val('');
								swal({text: res.msg, buttons: false, icon: "success", timer: 1000});
							}
						},error: function() {
							swal({text: 'Oops, something went wrong. Please try again later', buttons: false, icon: "error", timer: 1000});
						}
					});
				} else { swal({text: 'Your Icon is safe', buttons: false, timer: 1000 }); }
			});
		}
	})
		
	$("#edit_scenario_type_form").on('submit', (function(e) {
		e.preventDefault();
		var form_data = $(this).serialize();
		$('#editScenarioType').attr('disabled', 'disabled').html('<img src="scenario/img/loader.gif"> Please wait....');
		$.ajax({
			url: "includes/process.php",
			type: "POST",
			data: form_data,
			success: function(result) {
				var res = $.parseJSON(result);
				if (res.success == true) {
					$('#editScenarioType').removeAttr('disable', true);
					$('#editScenarioType').html('Update');
					swal({text: res.msg, buttons: false, icon: "success", timer: 1000});
					setTimeout(function() { window.location.reload(); }, 500);
				}
				else if (res.success == false) {						
					$('#editScenarioType').removeAttr('disable', true);
					$('#editScenarioType').html('Update');
					swal({text: res.msg, buttons: false, icon: "error", timer: 1000});
					setTimeout(function() { window.location.reload(); }, 500);
				}
			}
		});
	}));
</script>
<?php 
ob_end_flush();
