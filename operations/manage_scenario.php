<?php
/**
 * This is the CRUD operations file
 * 
 * PHP version 7
 * 
 * LICENSE: this is KS property
 * 
 * @category  Crud
 * @package   KS_Easysim
 * @author    Anita Kumar <anita2kumar@yahoo.co.in>
 * @copyright 2018-2019 The KS Orga
 * @license   http://www.php.net/license/3_01.txt  PHP License 3.01
 * @link      http://pear.php.net/package/PackageName
 */

/**
 * This is CRUD operations file for Authors.
 */

session_start();
//Require the class
require_once '../includes/formkey.class.php';
require_once '../config/db.class.php';
require_once '../includes/db.php';

//Start the class
$formKey	= new formKey();
$error 	 	= "No error";
$action 	= encryptString($_POST['frmscenario'], 'd');
$userId 	= $_SESSION['userId'];
$role   	= $_SESSION['role'];

if ($role == 'reviewer'):
	$redirect_url = '../reviewer-dashboard.php';
elseif ($role == 'admin'):
	$redirect_url = '../admin-dashboard.php';
elseif ($role == 'author'):
	$redirect_url = '../author-dashboard.php';
endif;

if ($action == 'modalScenarioType') {
    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
        $formValidation = $formKey->validate();
        if ($formValidation==='false') {
            echo $error = 'invalid';
            header('location: '. $redirect_url .'?error='.encryptString($_POST['frmscenario'], 'e'));
        } else {
			extract($_POST);
            $createdby = $userId;
            $createdon = date("Y-m-d H:i:s");
			$db = new DBConnection();
			if ( $db->scenarioTypeExist($scenario_name) == FALSE && ! empty($scenario_name)):
				try {
					$sql  = "INSERT INTO `scenario_type` (`type_name`, `type_icon`, `type_des`, `user_id`, `type_add_date`) values (:type, :icon, :des, :uid, :createdon)";
					$stmt = $db->prepare($sql);
					$stmt->execute(array(
						'type'		=> addslashes($scenario_name),
						'icon'		=> ( ! empty($type_icon_name)) ? $type_icon_name : '',
						'des'		=> addslashes($scenario_description),
						'uid'		=> $createdby,
						'createdon'	=> $createdon));
					$lastInsertedId = encryptString($db->lastInsertId(), 'e');
					if ( ! empty($lastInsertedId)):
						$url = '../scenario-type.php?msg=true&add=true';
						header('location:'. $url);
					endif;
				}
				catch (PDOException $e) {
					echo 'There is some problem in connection: ' . $e->getMessage();
				}
			else:
				$url = '../scenario-type.php?msg=true&exist=true';
				header('location:'. $url);
			endif;
        }
    }
}
elseif ($action == 'modalScenarioStatus') {
    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
        $formValidation = $formKey->validate();
        if ($formValidation==='false') {
            echo $error = 'invalid';
            header('location: '. $redirect_url .'?error='.encryptString($_POST['frmscenario'], 'e'));
        } else {
			extract($_POST);
            $createdby = $userId;
            $createdon = date("Y-m-d H:i:s");
			$db = new DBConnection();
			if ( $db->scenarioStatusExist($status_name) == FALSE && ! empty($status_name)):
				try {
					$sql = "INSERT INTO `scenario_status` (`status_name`, `status_des`,`user_id`, `status_date`) values (:sname, :des, :uid, :createdon)";
					$stmt = $db->prepare($sql);
					$stmt->execute(array(
						'sname'		=> addslashes($status_name),
						'des'		=> addslashes($status_description),
						'uid'		=> $createdby,
						'createdon'	=> $createdon));
					$lastInsertedId = encryptString($db->lastInsertId(), 'e');
					if ( ! empty($lastInsertedId)):
						$url = '../scenario-stage.php?msg=true&add=true';
					endif;
					header('location:'. $url);
				}
				catch (PDOException $e){
					echo 'There is some problem in connection: ' . $e->getMessage();
				}
			else:
				$url = '../scenario-stage.php?msg=true&exist=true';
				header('location:'. $url);
			endif;
        }
    }
}
elseif ($action == 'modalCreateGroup') {
    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
        $formValidation = $formKey->validate();
        if ($formValidation==='false') {
            echo $error = 'invalid';
            header('location: '.$redirect_url.'?error='.encryptString($_POST['frmscenario'], 'e'));
        } else {
			extract($_POST);
            $createdby = $userId;
            $createdon = date("Y-m-d H:i:s");
			$db = new DBConnection();
			if ( ! empty($group_name) && $db->groupExist($group_name) == FALSE):
				try {
					$sql = "INSERT INTO `group_tbl` (`group_name`, `group_icon`, `group_des`, `group_leader`, `uid`, `status`, `group_date`) 
					values (:group_name, :icon, :des, :gleader, :uid, :status, :createdon)";
					$stmt = $db->prepare($sql);
					$stmt->execute(array(
						'group_name'	=> addslashes($group_name),
						'icon'			=> ( ! empty($group_icon_name)) ? $group_icon_name : '',
						'des'			=> addslashes($group_description),
						'gleader'		=> $group_leader,
						'uid'			=> $createdby,
						'status'		=> 1,
						'createdon'		=> $createdon));
					$lastInsertedId = encryptString($db->lastInsertId(), 'e');
					if ( ! empty($lastInsertedId)):
						$url = '../group-management.php?msg=true&add=true';
						header('location:'. $url);
					endif;
				}
				catch (PDOException $e) {
					echo 'There is some problem in connection: ' . $e->getMessage();
				}
			else:
				$url = '../group-management.php?msg=true&exist=true';
				header('location:'. $url);
			endif;
        }
    }
}
