-- phpMyAdmin SQL Dump
-- version 4.7.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: May 04, 2021 at 12:41 PM
-- Server version: 5.5.68-MariaDB
-- PHP Version: 7.2.34

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `proddb_easysimapp_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `answer_tbl`
--

CREATE TABLE `answer_tbl` (
  `answer_id` int(11) NOT NULL,
  `question_id` int(11) NOT NULL,
  `choice_option` text,
  `choice_order_no` smallint(2) NOT NULL,
  `match_order_no` smallint(2) NOT NULL,
  `match_option` text,
  `match_sorting_item` int(11) NOT NULL,
  `nextQid` varchar(100) NOT NULL,
  `End_Sim` int(10) NOT NULL,
  `image` varchar(100) DEFAULT NULL,
  `image2` varchar(200) DEFAULT NULL,
  `video` varchar(100) DEFAULT NULL,
  `ans_val1` int(11) NOT NULL,
  `ans_val2` int(11) NOT NULL,
  `ans_val3` int(11) NOT NULL,
  `ans_val4` int(11) NOT NULL,
  `ans_val5` int(11) NOT NULL,
  `ans_val6` int(11) NOT NULL,
  `true_option` smallint(6) NOT NULL,
  `status` int(11) NOT NULL,
  `uid` int(11) NOT NULL,
  `cur_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='the answer and feedback tables with relation to questions';

-- --------------------------------------------------------

--
-- Table structure for table `assignment_tbl`
--

CREATE TABLE `assignment_tbl` (
  `assignment_id` int(11) NOT NULL,
  `scenario_id` varchar(200) NOT NULL,
  `group_id` int(11) NOT NULL,
  `learner_id` varchar(200) CHARACTER SET latin1 NOT NULL,
  `start_date` datetime NOT NULL,
  `end_date` datetime NOT NULL,
  `no_attempts` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `add_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `category_tbl`
--

CREATE TABLE `category_tbl` (
  `cat_id` int(11) NOT NULL,
  `category` varchar(200) DEFAULT NULL,
  `status` int(11) NOT NULL,
  `uid` int(11) NOT NULL,
  `datetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cms_page_tbl`
--

CREATE TABLE `cms_page_tbl` (
  `cms_page_id` int(11) NOT NULL,
  `bg_img` varchar(100) DEFAULT NULL,
  `page_content` text,
  `status` int(11) NOT NULL,
  `added_date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cms_page_tbl`
--

INSERT INTO `cms_page_tbl` (`cms_page_id`, `bg_img`, `page_content`, `status`, `added_date_time`) VALUES
(1, '', '<h1>CONNECTING BRANCHED MINDS</h1>\r\n\r\n<p>Easy<em>SIM</em>&reg;is a KPI-driven comprehensive Simulated Learning and Performance Cloud (SLPC) designed to disrupt the norm of plateaued job performances at your organization.<br />\r\nIts branched decision tree model puts your employees behind the wheel and lets them carve out a learning path of their own. It allows them to practice performing in real-life work scenarios, and its proprietary multi-competency mapping algorithm provides deep analysis of the employee&rsquo;s knowledge and behavior and the optimum utilization of their competencies.</p>\r\n', 1, '2021-03-01 06:42:43');

-- --------------------------------------------------------

--
-- Table structure for table `comments_tbl`
--

CREATE TABLE `comments_tbl` (
  `comment_id` int(11) NOT NULL,
  `parent_comment_id` int(11) NOT NULL,
  `scenario_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `comment` text,
  `type` int(11) NOT NULL,
  `cdate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `competency_tbl`
--

CREATE TABLE `competency_tbl` (
  `comp_id` int(11) NOT NULL,
  `scenario_id` int(11) NOT NULL,
  `comp_col_1` varchar(200) DEFAULT NULL,
  `comp_val_1` int(11) NOT NULL,
  `comp_col_2` varchar(200) DEFAULT NULL,
  `comp_val_2` int(11) NOT NULL,
  `comp_col_3` varchar(200) DEFAULT NULL,
  `comp_val_3` int(11) NOT NULL,
  `comp_col_4` varchar(200) DEFAULT NULL,
  `comp_val_4` int(11) NOT NULL,
  `comp_col_5` varchar(200) DEFAULT NULL,
  `comp_val_5` int(11) NOT NULL,
  `comp_col_6` varchar(200) DEFAULT NULL,
  `comp_val_6` int(11) NOT NULL,
  `weightage_1` int(11) NOT NULL,
  `weightage_2` int(11) NOT NULL,
  `weightage_3` int(11) NOT NULL,
  `weightage_4` int(11) NOT NULL,
  `weightage_5` int(11) NOT NULL,
  `weightage_6` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `uid` int(11) NOT NULL,
  `cur_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `email_template_tbl`
--

CREATE TABLE `email_template_tbl` (
  `eid` int(11) NOT NULL,
  `subject` varchar(200) DEFAULT NULL,
  `type` varchar(100) DEFAULT NULL,
  `tmp_des` text,
  `comment` varchar(200) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `status` smallint(6) NOT NULL,
  `edate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `email_template_tbl`
--

INSERT INTO `email_template_tbl` (`eid`, `subject`, `type`, `tmp_des`, `comment`, `user_id`, `status`, `edate`) VALUES
(1, 'New Simulation Assigned for Review', 'Reviewer', '<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"border-collapse:separate; mso-table-lspace:0pt; mso-table-rspace:0pt; width:100%\">\r\n	<tbody>\r\n		<tr>\r\n			<td style=\"vertical-align:top\">\r\n			<p style=\"margin-left:0px; margin-right:0px\">Hello {name},</p>\r\n\r\n			<p style=\"margin-left:0px; margin-right:0px\">There is a comment in the {simname} simulation under your review. Click on the link below to view the comment.</p>\r\n\r\n			<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"btn btn-primary\" style=\"border-collapse:separate; box-sizing:border-box; mso-table-lspace:0pt; mso-table-rspace:0pt; width:100%\">\r\n				<tbody>\r\n					<tr>\r\n						<td style=\"vertical-align:top\">\r\n						<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"border-collapse:separate; mso-table-lspace:0pt; mso-table-rspace:0pt; width:auto\">\r\n							<tbody>\r\n								<tr>\r\n									<td style=\"background-color:#3498db; text-align:center; vertical-align:top\"><a href=\"{url}\" style=\"display: inline-block; color: #ffffff; background-color: #3498db; border: solid 1px #3498db; border-radius: 5px; box-sizing: border-box; cursor: pointer; text-decoration: none; font-size: 14px; font-weight: bold; margin: 0; padding: 12px 25px; text-transform: capitalize; border-color: #3498db;\" target=\"_blank\">Review Simulation</a></td>\r\n								</tr>\r\n							</tbody>\r\n						</table>\r\n						</td>\r\n					</tr>\r\n				</tbody>\r\n			</table>\r\n\r\n			<p style=\"margin-left:0px; margin-right:0px\">Please do not hesitate to contact us at <a href=\"mailto:{cmail}\">{cmail}</a> if you have any questions.</p>\r\n\r\n			<p style=\"margin-left:0px; margin-right:0px\">Thank you!</p>\r\n			</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n', 'Reviewer email template', 1, 1, '2021-03-22 11:56:57'),
(2, 'Forgot Password', 'reset-password', '<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"border-collapse:separate; mso-table-lspace:0pt; mso-table-rspace:0pt; width:100%\">\n	<tbody>\n		<tr>\n			<td style=\"vertical-align:top\">\n			<p style=\"margin-left:0px; margin-right:0px\">Hi {name},</p>\n\n			<p style=\"margin-left:0px; margin-right:0px\">Your New Login Info:</p>\n\n			<p style=\"margin-left:0px; margin-right:0px\">Username: {name},&nbsp; New-Password: {pwd}</p>\n\n			<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"btn btn-primary\" style=\"border-collapse:separate; box-sizing:border-box; mso-table-lspace:0pt; mso-table-rspace:0pt; width:100%\">\n				<tbody>\n					<tr>\n						<td style=\"vertical-align:top\">\n						<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"border-collapse:separate; mso-table-lspace:0pt; mso-table-rspace:0pt; width:auto\">\n							<tbody>\n								<tr>\n									<td style=\"background-color:#3498db; text-align:center; vertical-align:top\"><a href=\"{url}\" style=\"display: inline-block; color: #ffffff; background-color: #3498db; border: solid 1px #3498db; border-radius: 5px; box-sizing: border-box; cursor: pointer; text-decoration: none; font-size: 14px; font-weight: bold; margin: 0; padding: 12px 25px; text-transform: capitalize; border-color: #3498db;\" target=\"_blank\">Manage your account</a></td>\n								</tr>\n							</tbody>\n						</table>\n						</td>\n					</tr>\n				</tbody>\n			</table>\n\n			<p style=\"margin-left:0px; margin-right:0px\">If you have any questions, please feel free to contact us at <a href=\"mailto:{cmail}\">{cmail}</a></p>\n\n			<p style=\"margin-left:0px; margin-right:0px\">Thank you!</p>\n			</td>\n		</tr>\n	</tbody>\n</table>\n', 'Forgot Password', 3, 1, '2019-10-11 05:11:15'),
(3, 'New Account Info.', 'new_user', '<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"border-collapse:separate; mso-table-lspace:0pt; mso-table-rspace:0pt; width:100%\">\r\n	<tbody>\r\n		<tr>\r\n			<td style=\"vertical-align:top\">\r\n			<p style=\"margin-left:0px; margin-right:0px\">Welcome {full_name}, to Lee Health&rsquo;s EngageSIM&trade; annual nurse assessments.</p>\r\n\r\n			<p style=\"margin-left:0px; margin-right:0px\">EngageSIM&trade; provides you a chance to demostrate your competency and job readiness.</p>\r\n\r\n			<p style=\"margin-left:0px; margin-right:0px\">Please use these login credentials to get started on your learning path.</p>\r\n\r\n			<p style=\"margin-left:0px; margin-right:0px\">Username: {name}</p>\r\n\r\n			<p style=\"margin-left:0px; margin-right:0px\">Password: {pwd}</p>\r\n\r\n			<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"btn btn-primary\" style=\"border-collapse:separate; box-sizing:border-box; mso-table-lspace:0pt; mso-table-rspace:0pt; width:100%\">\r\n				<tbody>\r\n					<tr>\r\n						<td style=\"vertical-align:top\">\r\n						<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"border-collapse:separate; mso-table-lspace:0pt; mso-table-rspace:0pt; width:auto\">\r\n							<tbody>\r\n								<tr>\r\n									<td style=\"background-color:#3498db; text-align:center; vertical-align:top\"><a href=\"{url}\" style=\"display: inline-block; color: #ffffff; background-color: #3498db; border: solid 1px #3498db; border-radius: 5px; box-sizing: border-box; cursor: pointer; text-decoration: none; font-size: 14px; font-weight: bold; margin: 0; padding: 12px 25px; text-transform: capitalize; border-color: #3498db;\" target=\"_blank\">Manage your account</a></td>\r\n								</tr>\r\n							</tbody>\r\n						</table>\r\n						</td>\r\n					</tr>\r\n				</tbody>\r\n			</table>\r\n\r\n			<p style=\"margin-left:0px; margin-right:0px\">Please do not hesitate to contact us at <a href=\"mailto:{cmail}\">{cmail}</a> if you have any questions.</p>\r\n\r\n			<p style=\"margin-left:0px; margin-right:0px\">Thank you!</p>\r\n			</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n', '', 3, 1, '2020-06-02 16:16:57'),
(4, 'New Simulation Assigned for Review', 'notify_author', '<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"border-collapse:separate; mso-table-lspace:0pt; mso-table-rspace:0pt; width:100%\">\r\n	<tbody>\r\n		<tr>\r\n			<td style=\"vertical-align:top\">\r\n			<p style=\"margin-left:0px; margin-right:0px\">Hello {name},</p>\r\n            <p style=\"margin-left:0px; margin-right:0px\">There is a comment in the {simname} simulation under your review. Click on the link below to view the comment.</p>\r\n            <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"btn btn-primary\" style=\"border-collapse:separate; box-sizing:border-box; mso-table-lspace:0pt; mso-table-rspace:0pt; width:100%\">\r\n				<tbody>\r\n					<tr>\r\n						<td style=\"vertical-align:top\">\r\n						<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"border-collapse:separate; mso-table-lspace:0pt; mso-table-rspace:0pt; width:auto\">\r\n							<tbody>\r\n								<tr>\r\n									<td style=\"background-color:#3498db; text-align:center; vertical-align:top\"><a href=\"{url}\" style=\"display: inline-block; color: #ffffff; background-color: #3498db; border: solid 1px #3498db; border-radius: 5px; box-sizing: border-box; cursor: pointer; text-decoration: none; font-size: 14px; font-weight: bold; margin: 0; padding: 12px 25px; text-transform: capitalize; border-color: #3498db;\" target=\"_blank\">Manage your account</a></td>\r\n								</tr>\r\n							</tbody>\r\n						</table>\r\n						</td>\r\n					</tr>\r\n				</tbody>\r\n			</table>\r\n\r\n			<p style=\"margin-left:0px; margin-right:0px\">Please do not hesitate to contact us at <a href=\"mailto:{cmail}\">{cmail}</a> if you have any questions.</p>\r\n\r\n			<p style=\"margin-left:0px; margin-right:0px\">Thank you!</p>\r\n			</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n', '', 3, 1, '2020-06-09 18:00:32'),
(5, 'Support', 'support', '<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"border-collapse:separate; mso-table-lspace:0pt; mso-table-rspace:0pt; width:100%\">\r\n	<tbody>\r\n		<tr>\r\n			<td style=\"vertical-align:top\">\r\n			<p style=\"margin-left:0px; margin-right:0px\">Hi Admin,</p>\r\n\r\n			<p style=\"margin-left:0px; margin-right:0px\">From:</p>\r\n\r\n			<p style=\"margin-left:0px; margin-right:0px\">Login Username: {name}, Email: {email}, Role: {role}</p>\r\n\r\n			<p style=\"margin-left:0px; margin-right:0px\">{msg}</p>\r\n\r\n			<p style=\"margin-left:0px; margin-right:0px\">Thank you!</p>\r\n			</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n', 'For Support mail', 3, 1, '2020-06-01 20:34:57'),
(6, 'Learner Report', 'group_leader', '<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"border-collapse:separate; mso-table-lspace:0pt; mso-table-rspace:0pt; width:100%\">\r\n	<tbody>\r\n		<tr>\r\n			<td style=\"vertical-align:top\">\r\n			<p style=\"margin-left:0px; margin-right:0px\">Hello {name},</p>\r\n\r\n			<p style=\"margin-left:0px; margin-right:0px\">I have successfully completed {simname} simulation. Please find attached my performance report for your records.</p>\r\n\r\n			<p style=\"margin-left:0px; margin-right:0px\">Learner Name: {learner_name}</p>\r\n\r\n			<p style=\"margin-left:0px; margin-right:0px\">{date}, {time}</p>\r\n\r\n			<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"btn btn-primary\" style=\"border-collapse:separate; box-sizing:border-box; mso-table-lspace:0pt; mso-table-rspace:0pt; width:100%\">\r\n				<tbody>\r\n					<tr>\r\n						<td style=\"vertical-align:top\">\r\n						<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"border-collapse:separate; mso-table-lspace:0pt; mso-table-rspace:0pt; width:auto\">\r\n							<tbody>\r\n								<tr>\r\n									<td style=\"background-color:#3498db; text-align:center; vertical-align:top\"><a href=\"{url}\" style=\"display: inline-block; color: #ffffff; background-color: #3498db; border: solid 1px #3498db; border-radius: 5px; box-sizing: border-box; cursor: pointer; text-decoration: none; font-size: 14px; font-weight: bold; margin: 0; padding: 12px 25px; text-transform: capitalize; border-color: #3498db;\" target=\"_blank\">Manage your account</a></td>\r\n								</tr>\r\n							</tbody>\r\n						</table>\r\n						</td>\r\n					</tr>\r\n				</tbody>\r\n			</table>\r\n\r\n			<p style=\"margin-left:0px; margin-right:0px\">Please do not hesitate to contact us at <a href=\"mailto:{cmail}\">{cmail}</a> if you have any questions.</p>\r\n\r\n			<p style=\"margin-left:0px; margin-right:0px\">Thank you!</p>\r\n			</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n', 'For learners report send to group leader.', 3, 1, '2020-06-10 14:59:19');

-- --------------------------------------------------------

--
-- Table structure for table `feedback_tbl`
--

CREATE TABLE `feedback_tbl` (
  `feed_id` int(11) NOT NULL,
  `question_id` int(11) NOT NULL,
  `nextQid` int(10) NOT NULL,
  `EndSim` int(10) NOT NULL,
  `answer_id` int(11) NOT NULL,
  `sub_options_id` int(11) NOT NULL,
  `feedback` text,
  `feedback_type` smallint(2) NOT NULL,
  `feed_speech_text` mediumtext,
  `feed_audio` varchar(100) DEFAULT NULL,
  `feed_video` varchar(100) DEFAULT NULL,
  `feed_screen` varchar(100) DEFAULT NULL,
  `feed_image` varchar(100) DEFAULT NULL,
  `feed_document` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `group_tbl`
--

CREATE TABLE `group_tbl` (
  `group_id` int(11) NOT NULL,
  `group_name` varchar(200) DEFAULT NULL,
  `group_icon` varchar(200) DEFAULT NULL,
  `group_des` text,
  `learner` text,
  `author` varchar(200) DEFAULT NULL,
  `reviewer` varchar(200) DEFAULT NULL,
  `group_leader` int(11) NOT NULL,
  `uid` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `group_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `group_tbl`
--

INSERT INTO `group_tbl` (`group_id`, `group_name`, `group_icon`, `group_des`, `learner`, `author`, `reviewer`, `group_leader`, `uid`, `status`, `group_date`) VALUES
(1, 'Group 1', NULL, NULL, NULL, NULL, NULL, 0, 0, 0, '2021-04-20 10:05:57');

-- --------------------------------------------------------

--
-- Table structure for table `multi_score_tbl`
--

CREATE TABLE `multi_score_tbl` (
  `mid` int(11) NOT NULL,
  `uid` varchar(200) NOT NULL,
  `scenario_id` int(11) NOT NULL,
  `question_type` int(11) NOT NULL,
  `question_id` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `userid` int(11) NOT NULL,
  `cur_date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `open_res_tbl`
--

CREATE TABLE `open_res_tbl` (
  `open_res_id` int(11) NOT NULL,
  `uid` varchar(200) DEFAULT NULL,
  `scenario_id` int(11) NOT NULL,
  `question_type` int(11) NOT NULL,
  `question_id` int(11) NOT NULL,
  `file_name` varchar(200) DEFAULT NULL,
  `tts_data` text,
  `tts_data_feedback` text,
  `file_name_feedback` varchar(200) DEFAULT NULL,
  `code` text,
  `code_feedback` text,
  `ans_val1` int(11) NOT NULL,
  `ans_val2` int(11) NOT NULL,
  `ans_val3` int(11) NOT NULL,
  `ans_val4` int(11) NOT NULL,
  `ans_val5` int(11) NOT NULL,
  `ans_val6` int(11) NOT NULL,
  `userid` int(11) NOT NULL,
  `cur_date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `question_tbl`
--

CREATE TABLE `question_tbl` (
  `question_id` int(11) NOT NULL,
  `scenario_id` int(11) DEFAULT NULL,
  `option_id` int(10) NOT NULL,
  `map_ques_1` int(10) NOT NULL,
  `ref_question_id` int(10) NOT NULL,
  `question_type` smallint(6) NOT NULL,
  `questions` text,
  `qaudio` varchar(100) DEFAULT NULL,
  `qspeech_text` mediumtext,
  `speech_text` mediumtext,
  `audio` varchar(100) DEFAULT NULL,
  `video` varchar(100) DEFAULT NULL,
  `screen` varchar(100) DEFAULT NULL,
  `image` varchar(100) DEFAULT NULL,
  `document` varchar(100) DEFAULT NULL,
  `true_options` varchar(50) DEFAULT NULL,
  `videoq_media_file` varchar(100) DEFAULT NULL COMMENT 'video question temp.',
  `videoq_cue_point` varchar(50) DEFAULT NULL,
  `seleted_drag_option` smallint(6) NOT NULL,
  `qorder` int(11) NOT NULL,
  `shuffle` int(11) NOT NULL,
  `ques_val_1` int(11) NOT NULL,
  `ques_val_2` int(11) NOT NULL,
  `ques_val_3` int(11) NOT NULL,
  `ques_val_4` int(11) NOT NULL,
  `ques_val_5` int(11) NOT NULL,
  `ques_val_6` int(11) NOT NULL,
  `critical` smallint(6) NOT NULL,
  `status` int(11) NOT NULL,
  `uid` int(11) NOT NULL,
  `cur_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='list of questions';

-- --------------------------------------------------------

--
-- Table structure for table `scenario_attempt_tbl`
--

CREATE TABLE `scenario_attempt_tbl` (
  `aid` int(11) NOT NULL,
  `uid` varchar(200) NOT NULL,
  `scenario_id` int(11) NOT NULL,
  `userid` int(11) DEFAULT NULL,
  `attempt_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `scenario_master`
--

CREATE TABLE `scenario_master` (
  `scenario_id` int(11) NOT NULL,
  `Scenario_title` varchar(250) NOT NULL,
  `scenario_type` varchar(45) DEFAULT NULL,
  `sim_ques_type` int(11) NOT NULL,
  `sim_temp_type` int(11) NOT NULL,
  `author_scenario_type` int(11) NOT NULL,
  `scenario_category` int(11) NOT NULL,
  `duration` int(11) NOT NULL,
  `passing_marks` int(11) NOT NULL,
  `image_url` varchar(200) DEFAULT NULL,
  `video_url` varchar(200) DEFAULT NULL,
  `audio_url` varchar(200) DEFAULT NULL,
  `web_object_file` varchar(200) DEFAULT NULL,
  `web_object` varchar(200) DEFAULT NULL,
  `web_object_title` varchar(200) DEFAULT NULL,
  `assign_start_date` date NOT NULL,
  `assign_end_date` date NOT NULL,
  `assign_author` varchar(200) DEFAULT NULL,
  `assign_reviewer` varchar(100) DEFAULT NULL,
  `assign_group` varchar(200) NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `bg_color` varchar(50) DEFAULT NULL,
  `bg_own_choice` varchar(200) DEFAULT NULL,
  `sim_back_img` varchar(200) DEFAULT NULL,
  `sim_char_img` varchar(200) DEFAULT NULL,
  `sim_char_left_img` varchar(200) DEFAULT NULL,
  `sim_char_right_img` varchar(200) DEFAULT NULL,
  `char_own_choice` varchar(200) DEFAULT NULL,
  `sim_cover_img` varchar(200) DEFAULT NULL,
  `update_sim` int(11) NOT NULL,
  `scenario_xm_file` varchar(100) DEFAULT NULL,
  `status` int(11) NOT NULL,
  `modified_by` int(11) NOT NULL,
  `modified_on` datetime DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `created_on` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='meta details of the scenarios';

-- --------------------------------------------------------

--
-- Table structure for table `scenario_status`
--

CREATE TABLE `scenario_status` (
  `sid` int(11) NOT NULL,
  `status_name` varchar(200) DEFAULT NULL,
  `status_des` mediumtext,
  `user_id` int(11) NOT NULL,
  `status_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `scenario_status`
--

INSERT INTO `scenario_status` (`sid`, `status_name`, `status_des`, `user_id`, `status_date`) VALUES
(2, 'Draft', '', 5, '2018-07-09 12:16:56'),
(5, 'Ready to Publish', '', 5, '2018-08-07 08:51:51'),
(6, 'Preview', '', 5, '2018-08-09 08:24:32'),
(7, 'Ready to Review', '', 5, '2018-09-17 09:07:44'),
(8, 'Published', '', 4, '2019-05-02 01:31:10'),
(9, 'Assigned', '', 4, '2019-05-02 01:35:16'),
(10, 'Unassigned', '', 4, '2019-05-02 01:35:23'),
(11, 'Ready to Author', '', 4, '2019-05-02 03:07:00'),
(12, 'Archive', 'archive simulation', 2, '2019-05-02 04:03:49'),
(13, 'Assigne to Learner', '', 2, '2019-05-02 04:17:10'),
(14, 'In Review', '', 3, '2019-11-25 12:23:11'),
(15, 'In Fixes', '', 3, '2019-11-25 12:23:17'),
(16, 'In Verification', '', 3, '2019-11-25 12:23:23'),
(17, 'Notify Reviewer', '', 3, '2019-11-26 08:16:33'),
(18, 'Notify Author', '', 3, '2019-11-26 08:16:38');

-- --------------------------------------------------------

--
-- Table structure for table `scenario_type`
--

CREATE TABLE `scenario_type` (
  `st_id` int(11) NOT NULL,
  `type_name` varchar(200) DEFAULT NULL,
  `type_icon` varchar(200) DEFAULT NULL,
  `type_des` mediumtext,
  `user_id` int(11) NOT NULL,
  `type_add_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `scenario_type`
--

INSERT INTO `scenario_type` (`st_id`, `type_name`, `type_icon`, `type_des`, `user_id`, `type_add_date`) VALUES
(2, 'Video', 'video_sce73.svg', '', 5, '2019-05-03 05:24:45'),
(3, 'Audio', 'audio_sce66.svg', '', 5, '2019-05-03 05:24:25'),
(4, 'Image and text', 'image_sce78.svg', '', 5, '2019-05-03 05:24:02'),
(5, 'Text only', 'text_sce56.svg', '', 5, '2019-05-03 05:23:31'),
(6, 'Web Object', 'web-object451624610745.svg', '', 1, '2021-06-25 05:16:06');

-- --------------------------------------------------------

--
-- Table structure for table `score_tbl`
--

CREATE TABLE `score_tbl` (
  `sid` int(11) NOT NULL,
  `uid` varchar(200) DEFAULT NULL,
  `scenario_id` int(11) NOT NULL,
  `question_type` int(11) NOT NULL,
  `question_id` int(11) NOT NULL,
  `choice_option_id` int(11) NOT NULL,
  `match_option_id` int(11) NOT NULL,
  `userid` int(11) DEFAULT NULL,
  `cur_date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sim_branding_tbl`
--

CREATE TABLE `sim_branding_tbl` (
  `branding_id` int(11) NOT NULL,
  `scenario_id` int(11) NOT NULL,
  `ques_bg` varchar(100) DEFAULT NULL,
  `ques_bg_transparency` int(11) NOT NULL,
  `option_bg` varchar(100) DEFAULT NULL,
  `option_bg_transparency` int(11) NOT NULL,
  `option_hover` varchar(100) DEFAULT NULL,
  `option_hover_transparency` int(11) NOT NULL,
  `option_selected` varchar(100) DEFAULT NULL,
  `option_selected_transparency` int(11) NOT NULL,
  `btn_bg` varchar(100) DEFAULT NULL,
  `btn_bg_transparency` int(11) NOT NULL,
  `btn_hover` varchar(100) DEFAULT NULL,
  `btn_hover_transparency` int(11) NOT NULL,
  `btn_selected` varchar(100) DEFAULT NULL,
  `btn_selected_transparency` int(11) NOT NULL,
  `font_type` varchar(100) DEFAULT NULL,
  `font_color` varchar(100) DEFAULT NULL,
  `font_size` varchar(50) NOT NULL,
  `user_id` int(11) NOT NULL,
  `added_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sim_pages`
--

CREATE TABLE `sim_pages` (
  `sim_page_id` int(11) NOT NULL,
  `scenario_id` int(11) NOT NULL,
  `sim_page_name` varchar(200) DEFAULT NULL,
  `sim_page_desc` text,
  `userid` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `added_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sub_options_tbl`
--

CREATE TABLE `sub_options_tbl` (
  `sub_options_id` int(11) NOT NULL,
  `question_id` int(11) NOT NULL,
  `answer_id` int(11) NOT NULL,
  `sub_option` text,
  `image` varchar(200) DEFAULT NULL,
  `ans_val1` int(11) NOT NULL,
  `ans_val2` int(11) NOT NULL,
  `ans_val3` int(11) NOT NULL,
  `ans_val4` int(11) NOT NULL,
  `ans_val5` int(11) NOT NULL,
  `ans_val6` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `support_tbl`
--

CREATE TABLE `support_tbl` (
  `support_id` int(11) NOT NULL,
  `subject` varchar(200) DEFAULT NULL,
  `des` text,
  `attachment` varchar(200) DEFAULT NULL,
  `status` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `datetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `upload_tbl`
--

CREATE TABLE `upload_tbl` (
  `upload_id` int(11) NOT NULL,
  `img_name` varchar(200) DEFAULT NULL,
  `des` varchar(200) DEFAULT NULL,
  `type` int(11) NOT NULL COMMENT '1=bg,2=char',
  `uid` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `add_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `upload_tbl`
--

INSERT INTO `upload_tbl` (`upload_id`, `img_name`, `des`, `type`, `uid`, `status`, `add_date`) VALUES
(1, 'sel_bg1.jpg', '', 1, 5, 1, '2019-08-16 03:49:50'),
(2, 'sel_bg2.jpg', '', 1, 5, 1, '2019-08-16 03:52:23'),
(3, 'sel_bg3.jpg', '', 1, 5, 1, '2019-08-16 03:52:38'),
(5, 'char3.png', '', 2, 5, 1, '2019-08-16 03:54:39'),
(6, 'char2.png', '', 2, 5, 1, '2019-08-16 03:54:53'),
(7, 'char1.png', '', 2, 5, 1, '2019-08-16 03:55:05'),
(18, 'abstract-913a69.jpg', '', 1, 3, 1, '2019-09-18 10:19:03'),
(24, 'UpsellingBG37.jpg', '', 1, 5, 1, '2019-09-27 10:49:20'),
(26, 'BG122.jpg', '', 1, 5, 1, '2019-09-27 11:05:44'),
(37, '0177.jpg', '', 1, 5, 1, '2019-09-30 05:37:47'),
(38, '0284.jpg', '', 1, 5, 1, '2019-09-30 05:38:00'),
(39, '0388.jpg', '', 1, 5, 1, '2019-09-30 05:38:10'),
(40, '0450.jpg', '', 1, 5, 1, '2019-09-30 05:38:25'),
(50, 'avatar0114.png', '', 2, 5, 1, '2019-09-30 05:44:32'),
(51, 'avatar0239.png', '', 2, 5, 1, '2019-09-30 05:44:42'),
(52, 'avatar035.png', '', 2, 5, 1, '2019-09-30 05:44:56'),
(53, 'avatar0457.png', '', 2, 5, 1, '2019-09-30 05:45:17'),
(54, 'avatar0544.png', '', 2, 5, 1, '2019-09-30 05:45:34'),
(55, 'avatar0653.png', '', 2, 5, 1, '2019-09-30 05:45:49'),
(56, 'avatar0756.png', '', 2, 5, 1, '2019-09-30 05:46:02'),
(57, 'avatar0849.png', '', 2, 5, 1, '2019-09-30 05:46:20'),
(58, 'avatar0979.png', '', 2, 5, 1, '2019-09-30 05:46:31'),
(60, 'avatar1081.png', '', 2, 5, 1, '2019-09-30 06:06:22'),
(61, 'avatar1170.png', '', 2, 5, 1, '2019-09-30 06:06:33'),
(62, '0519.jpg', '', 1, 5, 1, '2019-09-30 06:26:25'),
(64, '0614.jpg', '', 1, 5, 1, '2019-09-30 06:27:28'),
(65, '0782.jpg', '', 1, 5, 1, '2019-09-30 06:27:40'),
(66, '0814.jpg', '', 1, 5, 1, '2019-09-30 06:27:51'),
(67, '0924.jpg', '', 1, 5, 1, '2019-09-30 06:28:01'),
(69, '1037.jpg', '', 1, 5, 1, '2019-09-30 06:28:22'),
(70, '1112.jpg', '', 1, 5, 1, '2019-09-30 06:28:43'),
(71, '1250.jpg', '', 1, 5, 1, '2019-09-30 06:28:53'),
(72, '1365.jpg', '', 1, 5, 1, '2019-09-30 06:29:07'),
(73, '1436.jpg', '', 1, 5, 1, '2019-09-30 06:29:20'),
(74, '1581.jpg', '', 1, 5, 1, '2019-09-30 06:29:32'),
(76, '1778.jpg', '', 1, 5, 1, '2019-09-30 06:30:00'),
(77, '1868.jpg', '', 1, 5, 1, '2019-09-30 06:30:18'),
(78, '1989.jpg', '', 1, 5, 1, '2019-09-30 06:30:38'),
(79, '2026.jpg', '', 1, 5, 1, '2019-09-30 06:30:49'),
(80, '2188.jpg', '', 1, 5, 1, '2019-09-30 06:31:08'),
(81, '2213.jpg', '', 1, 5, 1, '2019-09-30 06:31:30'),
(82, 'CouplelisteningtoPolicy46.png', '', 1, 21, 1, '2019-10-20 20:04:10'),
(83, 'CountryDrive34.png', '', 1, 21, 1, '2019-10-20 20:15:06'),
(85, 'Car31.png', '', 1, 21, 1, '2019-10-21 05:37:24'),
(86, 'Optimized-CarHouse40.png', '', 1, 21, 1, '2019-10-21 05:40:14'),
(87, 'MB0183.jpg', '', 1, 5, 1, '2019-10-21 07:56:41'),
(89, 'MB_avatar0548.png', '', 2, 5, 1, '2019-10-21 08:05:40'),
(91, 'Optimized-fadedclinic86.jpg', '', 1, 21, 1, '2019-10-30 07:49:59'),
(92, 'avatar0251573017541.png', '', 3, 5, 1, '2019-11-06 05:19:01'),
(93, 'avatar03281573017555.png', '', 3, 5, 1, '2019-11-06 05:19:15'),
(94, 'avatar04121573017567.png', '', 3, 5, 1, '2019-11-06 05:19:27'),
(95, 'avatar05141573017580.png', '', 3, 5, 1, '2019-11-06 05:19:40'),
(96, 'avatar0661573017592.png', '', 3, 5, 1, '2019-11-06 05:19:52'),
(97, 'avatar07711573017616.png', '', 3, 5, 1, '2019-11-06 05:20:16'),
(99, 'avatar08681573017653.png', '', 3, 5, 1, '2019-11-06 05:20:53'),
(100, 'avatar09261573017669.png', '', 3, 5, 1, '2019-11-06 05:21:09'),
(101, 'avatar10281573017683.png', '', 3, 5, 1, '2019-11-06 05:21:23'),
(102, 'avatar11961573017705.png', '', 3, 5, 1, '2019-11-06 05:21:45');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) UNSIGNED NOT NULL,
  `username` varchar(100) NOT NULL,
  `fname` varchar(200) DEFAULT NULL,
  `lname` varchar(100) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `mob` bigint(10) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  `role` varchar(50) DEFAULT NULL,
  `location` varchar(200) DEFAULT NULL,
  `company` varchar(200) DEFAULT NULL,
  `department` varchar(200) DEFAULT NULL,
  `designation` varchar(200) DEFAULT NULL,
  `webpage` varchar(200) DEFAULT NULL,
  `facebook` varchar(200) DEFAULT NULL,
  `twitter` varchar(200) DEFAULT NULL,
  `linkedIn` varchar(200) DEFAULT NULL,
  `date_of_join` date NOT NULL,
  `img` varchar(200) DEFAULT NULL,
  `img_thumb_50` varchar(200) DEFAULT NULL,
  `img_thumb_125` varchar(200) DEFAULT NULL,
  `status` smallint(2) DEFAULT NULL,
  `email_status` int(11) NOT NULL,
  `modified_date` date NOT NULL,
  `last_change_pwd` date NOT NULL,
  `last_login` timestamp NULL DEFAULT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for table `answer_tbl`
--
ALTER TABLE `answer_tbl`
  ADD PRIMARY KEY (`answer_id`),
  ADD KEY `question_id` (`question_id`);

--
-- Indexes for table `assignment_tbl`
--
ALTER TABLE `assignment_tbl`
  ADD PRIMARY KEY (`assignment_id`),
  ADD KEY `scenario_id` (`scenario_id`),
  ADD KEY `group_id` (`group_id`);

--
-- Indexes for table `category_tbl`
--
ALTER TABLE `category_tbl`
  ADD PRIMARY KEY (`cat_id`);

--
-- Indexes for table `cms_page_tbl`
--
ALTER TABLE `cms_page_tbl`
  ADD PRIMARY KEY (`cms_page_id`);

--
-- Indexes for table `comments_tbl`
--
ALTER TABLE `comments_tbl`
  ADD PRIMARY KEY (`comment_id`),
  ADD KEY `scenario_id` (`scenario_id`);

--
-- Indexes for table `competency_tbl`
--
ALTER TABLE `competency_tbl`
  ADD PRIMARY KEY (`comp_id`),
  ADD KEY `scenario_id` (`scenario_id`);

--
-- Indexes for table `email_template_tbl`
--
ALTER TABLE `email_template_tbl`
  ADD PRIMARY KEY (`eid`);

--
-- Indexes for table `feedback_tbl`
--
ALTER TABLE `feedback_tbl`
  ADD PRIMARY KEY (`feed_id`),
  ADD KEY `question_id` (`question_id`),
  ADD KEY `answer_id` (`answer_id`),
  ADD KEY `sub_options_id` (`sub_options_id`);

--
-- Indexes for table `group_tbl`
--
ALTER TABLE `group_tbl`
  ADD PRIMARY KEY (`group_id`);

--
-- Indexes for table `multi_score_tbl`
--
ALTER TABLE `multi_score_tbl`
  ADD PRIMARY KEY (`mid`),
  ADD KEY `scenario_id` (`scenario_id`),
  ADD KEY `question_type` (`question_type`),
  ADD KEY `question_id` (`question_id`);

--
-- Indexes for table `open_res_tbl`
--
ALTER TABLE `open_res_tbl`
  ADD PRIMARY KEY (`open_res_id`),
  ADD KEY `scenario_id` (`scenario_id`),
  ADD KEY `question_id` (`question_id`);

--
-- Indexes for table `question_tbl`
--
ALTER TABLE `question_tbl`
  ADD PRIMARY KEY (`question_id`),
  ADD KEY `scenario_id` (`scenario_id`);

--
-- Indexes for table `scenario_attempt_tbl`
--
ALTER TABLE `scenario_attempt_tbl`
  ADD PRIMARY KEY (`aid`),
  ADD KEY `userid` (`userid`),
  ADD KEY `scenario_id` (`scenario_id`);

--
-- Indexes for table `scenario_master`
--
ALTER TABLE `scenario_master`
  ADD PRIMARY KEY (`scenario_id`);

--
-- Indexes for table `scenario_status`
--
ALTER TABLE `scenario_status`
  ADD PRIMARY KEY (`sid`);

--
-- Indexes for table `scenario_type`
--
ALTER TABLE `scenario_type`
  ADD PRIMARY KEY (`st_id`);

--
-- Indexes for table `score_tbl`
--
ALTER TABLE `score_tbl`
  ADD PRIMARY KEY (`sid`),
  ADD KEY `uid` (`uid`),
  ADD KEY `scenario_id` (`scenario_id`),
  ADD KEY `question_id` (`question_id`),
  ADD KEY `choice_option_id` (`choice_option_id`),
  ADD KEY `match_option_id` (`match_option_id`);

--
-- Indexes for table `sim_branding_tbl`
--
ALTER TABLE `sim_branding_tbl`
  ADD PRIMARY KEY (`branding_id`),
  ADD KEY `scenario_id` (`scenario_id`);

--
-- Indexes for table `sim_pages`
--
ALTER TABLE `sim_pages`
  ADD PRIMARY KEY (`sim_page_id`),
  ADD KEY `scenario_id` (`scenario_id`);

--
-- Indexes for table `sub_options_tbl`
--
ALTER TABLE `sub_options_tbl`
  ADD PRIMARY KEY (`sub_options_id`),
  ADD KEY `question_id` (`question_id`),
  ADD KEY `answer_id` (`answer_id`);

--
-- Indexes for table `support_tbl`
--
ALTER TABLE `support_tbl`
  ADD PRIMARY KEY (`support_id`);

--
-- Indexes for table `upload_tbl`
--
ALTER TABLE `upload_tbl`
  ADD PRIMARY KEY (`upload_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `answer_tbl`
--
ALTER TABLE `answer_tbl`
  MODIFY `answer_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `assignment_tbl`
--
ALTER TABLE `assignment_tbl`
  MODIFY `assignment_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `category_tbl`
--
ALTER TABLE `category_tbl`
  MODIFY `cat_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cms_page_tbl`
--
ALTER TABLE `cms_page_tbl`
  MODIFY `cms_page_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `comments_tbl`
--
ALTER TABLE `comments_tbl`
  MODIFY `comment_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `competency_tbl`
--
ALTER TABLE `competency_tbl`
  MODIFY `comp_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `email_template_tbl`
--
ALTER TABLE `email_template_tbl`
  MODIFY `eid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `feedback_tbl`
--
ALTER TABLE `feedback_tbl`
  MODIFY `feed_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `group_tbl`
--
ALTER TABLE `group_tbl`
  MODIFY `group_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `multi_score_tbl`
--
ALTER TABLE `multi_score_tbl`
  MODIFY `mid` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `open_res_tbl`
--
ALTER TABLE `open_res_tbl`
  MODIFY `open_res_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `question_tbl`
--
ALTER TABLE `question_tbl`
  MODIFY `question_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `scenario_attempt_tbl`
--
ALTER TABLE `scenario_attempt_tbl`
  MODIFY `aid` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `scenario_master`
--
ALTER TABLE `scenario_master`
  MODIFY `scenario_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `scenario_status`
--
ALTER TABLE `scenario_status`
  MODIFY `sid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `scenario_type`
--
ALTER TABLE `scenario_type`
  MODIFY `st_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `score_tbl`
--
ALTER TABLE `score_tbl`
  MODIFY `sid` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sim_branding_tbl`
--
ALTER TABLE `sim_branding_tbl`
  MODIFY `branding_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sim_pages`
--
ALTER TABLE `sim_pages`
  MODIFY `sim_page_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sub_options_tbl`
--
ALTER TABLE `sub_options_tbl`
  MODIFY `sub_options_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `support_tbl`
--
ALTER TABLE `support_tbl`
  MODIFY `support_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `upload_tbl`
--
ALTER TABLE `upload_tbl`
  MODIFY `upload_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=103;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
