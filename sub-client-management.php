<?php 
ob_start();
session_start();
if (isset($_SESSION['username'])) {
  $client_id  = $_SESSION['client_id'];
  $userid     = $_SESSION['userId'];
  $user       = $_SESSION['username'];
  $role       = $_SESSION['role'];
} else {
  header('location: index.php');
}

require_once 'config/db.class.php';
require_once 'includes/learners_menu.php';
require_once 'config/pagination.php';
$db = new DBConnection();

/* page access only client administrator */
($db->checkAccountType() != 2) ? header('location: index.php') : '';

$permissions = $db->getClient($client_id);

/* Connect main DB */
try {
  $main_conn = new PDO("mysql:host=$db->main_servername;dbname=$db->main_db_name", $db->main_username, $db->main_password);
  $main_conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch (PDOException $e) {
  echo "Main Connection failed:" . $e->getMessage();
  exit;
}
$sqlr       = "SELECT c.*, DATE_FORMAT(c.create_date_time, '%d-%m-%y %h:%i %p') AS create_date FROM 
			   mapping_tbl AS m LEFT JOIN 
			   client_tbl AS c ON m.sub_client_id = c.client_id 
               WHERE m.client_id = '" . $client_id . "' ORDER BY c.client_id DESC";
$q          = $main_conn->prepare($sqlr); $q->execute();
$rowCount   = $q->rowCount();
$count      = ($sqlr) ? $rowCount : 0;
$page       = (isset($_REQUEST['page']) && !empty($_REQUEST['page'])) ? $_REQUEST['page'] : 1;
$show       = 12;
$start      = ($page - 1) * $show;
$pagination = getPaginationString($count, $show, $page, 1, basename(__FILE__), '?page=');
$sql        = $sqlr . " LIMIT $start, $show";
$results    = $main_conn->prepare($sql); ?>
<div class="bottomheader">
  <div class="container-fluid">
    <ul class="breadcrumb">
      <?php echo $db->breadcrumbs(); ?>
    </ul>
  </div>
</div>
<div class="simulation_assignment super-client-manag">
	<div class="container-fluid">
    	<div class="row">
        	<div class="col-sm-12">
            <form enctype="multipart/form" name="create_db_form" id="create_db_form" method="post">
				<input type="hidden" name="currency_code" id="currency_code" />
            	<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                	<div class="panel panelCaollapse">
                    	<div class="panel-heading" role="tab" id="headingOne">
                        	<h4 class="panel-title"><a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne"> Add New Sub Client</a></h4>
				  		</div>
                        <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                        	<div class="panel-body">
                                <ul class="nav nav-tabs Screatenewadmin-tab">
                                    <li class="active"><a data-toggle="tab" href="#OrganizationDetail">Organization Detail</a></li>
                                    <li><a data-toggle="tab" href="#Contactdetail">Contact detail</a></li>
                                    <li><a data-toggle="tab" href="#LoginDetail">Login Detail</a></li>
                                    <li><a data-toggle="tab" href="#LicenseInformation">License Information</a></li>
                                    <li><a data-toggle="tab" href="#Hostingdetail">Hosting detail</a></li>
									<li><a data-toggle="tab" href="#ManageBranding">Manage Permissions</a></li>
									<li><a data-toggle="tab" href="#ManageTemp">Manage Template</a></li>
                                    <li><a data-toggle="tab" href="#PaymentDetail">Payment Detail</a></li>
                                </ul>
                                <div class="tab-content super-createnewadmin">
                                    <div id="OrganizationDetail" class="tab-pane fade in active">
                                        <div class="usermanage-form">
                                            <div class="row">
                                                <div class="col-sm-4">
                                                    <div class="form-group">
														<input type="text" name="organization" id="organization" class="form-control" placeholder="Organization Name *" required="required" />
                                                    </div>
                                                </div>
                                                <div class="col-sm-4">
                                                    <div class="form-group">
														<input type="file" name="logo" id="logo" class="form-control file" />
														<input type="text" class="form-control controls" disabled placeholder="Upload Logo" />
														<input type="hidden" name="logo_name" id="logo_name" />														
														<div class="browsebtn browsebtntmp">
															<span id="splashImg"></span> <span id="ImgDelete" style="display:none">
															<a href="javascript:void(0);" data-img-name="" class="delete_logo" title="Delete Logo" />
															<i class="fa fa-times" aria-hidden="true"></i></a></span>
														</div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-2">
                                                    <button class="browse btn btn-primary" type="button">Browse</button>
                                                </div>
												<div class="col-sm-2">
													<button type="button" name="upload" id="upload" class="btn btn-primary">Uplaod</button>
                                                </div>
                                            </div>
                                            <div class="row">
												<div class="col-sm-4">
													<div class="form-group">
														<select name="company_country" id="company_country" class="form-control country" data-starget="company_state" data-ctarget="company_city">
															<option value="" selected="" disabled="disabled">Select Country</option>
														</select>
													</div>
												</div>
												<div class="col-sm-4">
													<div class="form-group">
														<select name="company_state" id="company_state" class="form-control state" data-ctarget="company_city">
															<option value="" selected="" disabled="disabled">Select State</option>
														</select>
													</div>
												</div>
												<div class="col-sm-4">
													<div class="form-group">
														<select name="company_city" id="company_city" class="form-control city">
															<option value="" selected="" disabled="disabled">Select City</option>
														</select>
													</div>
												</div>
											</div>
											<div class="row">
												<div class="col-sm-4">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control" name="tax_id" id="tax_id" placeholder="Tax ID" />
                                                    </div>
                                                </div>
												<div class="col-sm-4">
                                                    <div class="form-group">
														<textarea class="form-control" name="bill_add" id="bill_add" placeholder="Organization Address"></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
										 <ul class="list-inline pull-right">
											<li><button type="button" class="btn btn-primary next-step">Continue</button></li>
										</ul>
									</div>
                                    <div id="Contactdetail" class="tab-pane fade">
                                        <div class="Contactinterhead">Executive Contact</div>
                                            <div class="usermanage-form">
                                                <div class="row">
                                                    <div class="col-sm-3">
                                                        <div class="form-group">
                                                            <input type="text" class="form-control" name="executive_name" id="executive_name" placeholder="Contact Name" />
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <div class="form-group">
                                                            <input type="email" class="form-control" name="executive_email" id="executive_email" placeholder="Email" />
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-3">
														<div class="form-group">
															<select name="executive_pcode" id="executive_pcode" class="form-control phonecode">
																<option value="" selected="" disabled="disabled">Select Phone Code</option>
															</select>
														</div>
													</div>
													<div class="col-sm-3">
                                                        <div class="form-group">
                                                            <input type="tel" class="form-control" name="executive_mob" id="executive_mob" placeholder="Contact Ph Number" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            
                                            <div class="Contactinterhead">Decision Maker</div>
                                            <div class="usermanage-form">
                                                <div class="row">
                                                    <div class="col-sm-3">
                                                        <div class="form-group">
                                                            <input type="text" class="form-control" name="decision_name" id="decision_name" placeholder="Contact Name" />
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <div class="form-group">
                                                            <input type="email" class="form-control" name="decision_email" id="decision_email" placeholder="Email" />
                                                        </div>
                                                    </div>
													<div class="col-sm-3">
														<div class="form-group">
															<select name="decision_pcode" id="decision_pcode" class="form-control phonecode">
																<option value="" selected="" disabled="disabled">Select Phone Code</option>
															</select>
														</div>
													</div>
                                                    <div class="col-sm-3">
                                                        <div class="form-group">
                                                            <input type="tel" class="form-control" name="decision_mob" id="decision_mob" placeholder="Contact Ph Number" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                
                                            <div class="Contactinterhead">Billing Contact</div>
                                            <div class="usermanage-form">
                                                <div class="row">
                                                    <div class="col-sm-3">
                                                        <div class="form-group">
                                                            <input type="text" class="form-control" name="billing_name" id="billing_name" placeholder="Contact Name" />
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <div class="form-group">
                                                            <input type="email" class="form-control" name="billing_email" id="billing_email" placeholder="Email" />
                                                        </div>
                                                    </div>
													<div class="col-sm-3">
														<div class="form-group">
															<select name="billing_pcode" id="billing_pcode" class="form-control phonecode">
																<option value="" selected="" disabled="disabled">Select Phone Code</option>
															</select>
														</div>
													</div>
                                                    <div class="col-sm-3">
                                                        <div class="form-group">
                                                            <input type="tel" class="form-control" name="billing_mob" id="billing_mob" placeholder="Contact Ph Number" />
                                                        </div>
                                                    </div>
                                            	</div>
                                        	</div>
											<ul class="list-inline pull-right">
												<li><button type="button" class="btn btn-primary prev-step">Back</button></li>
												<li><button type="button" class="btn btn-primary next-step">Continue</button></li>
											</ul>
                                    </div>
                                    <div id="LoginDetail" class="tab-pane fade">
                                        <div class="usermanage-form">
											<div class="row">
												<div class="col-sm-3">
													<div class="input-group">
													    <input type="text" name="domain_name" id="domain_name" class="form-control" placeholder="Subdomain Name *" autocomplete="off" required="required" title="Please give valid Subdomain name, no spaces or special characters allowed." />
													    <span class="input-group-addon">.easysim.app</span>
													</div>
												</div>
												<div class="col-sm-3">
													<div class="input-group">
														<input type="text" name="db_name" id="db_name" class="form-control noSpace" placeholder="Enter Database Name like es_client_db_name *" autocomplete="off" required="required" />
														<span class="input-group-addon"><span class="d-inline-block" tabindex="0" data-toggle="tooltip" title="Enter database name like es_client_db"><i class="fa fa-info-circle" aria-hidden="true"></i></span></span>
													</div>
												</div>
												<div class="col-sm-3">
													<div class="form-group form-select">
														<select class="form-control" name="orgtype" required="required" data-placeholder="Organization Type *">
                                                            <option value="" selected="" disabled="disabled">Select Organization Type</option>
                                                            <option value="Company">Company</option>
                                                            <option value="Indiviual">Indiviual</option>
                                                            <option value="Reseller">Reseller</option>
														</select>
													</div>
												</div>
												
												<div class="col-sm-3">
													<div class="form-group form-select">
														<?php $roles = $db->role;
														if (array_key_exists('clientAdmin', $roles)) {
															unset($roles['clientAdmin']);
														} ?>
														<select class="form-control multiselect-ui selection" multiple="multiple" name="role[]" id="role" required="required" data-placeholder="User Role *">
														<?php foreach ($roles as $roleKey => $roleVal): ?>
														<option value="<?php echo $roleKey; ?>"><?php echo $roleVal; ?></option>
														<?php endforeach; ?>
														</select>
													</div>
												</div>
												<div class="col-sm-3">
													<div class="form-group">
														<input type="text" name="userName" id="userName" class="form-control" placeholder="Login User Name *" required="required" />
													</div>
												</div>
												<div class="col-sm-3">
													<div class="input-group" id="show_hide_password">
														<input type="password" name="password" id="password" class="form-control" placeholder="Login Password *" required="required" />
														<div class="input-group-addon eye-slash"><a href=""><i class="fa fa-eye-slash" aria-hidden="true"></i></a></div>
													</div>
												</div>
												<div class="col-sm-3">
													<div class="form-group form-select">
														<select class="form-control" name="loginType" id="loginType" required="required" data-placeholder="Login Type *">
														    <option value="" selected="" disabled="disabled">Select Login Type</option>
														    <option value="admin">Admin</option>
														</select>
													</div>
												</div>
											</div>
											<div class="Contactinterhead">SMTP Mail Configuration</div>
											<div class="row">
												<div class="col-sm-4">
													<div class="input-group">
														<input type="text" name="smtp_host_name" id="smtp_host_name" class="form-control" placeholder="SMTP Host Name" autocomplete="off" />
													</div>
												</div>
												<div class="col-sm-3">
													<div class="input-group">
														<input type="text" name="smtp_user_name" id="smtp_user_name" class="form-control" placeholder="SMTP User Name" autocomplete="off" />
													</div>
												</div>
												<div class="col-sm-3">
													<div class="input-group">
														<input type="text" name="smtp_pwd" id="smtp_pwd" class="form-control" placeholder="SMTP Password" autocomplete="off" />
													</div>
												</div>
												<div class="col-sm-2">
													<div class="input-group">
														<input type="text" name="smtp_port" id="smtp_port" class="form-control" placeholder="SMTP PORT" autocomplete="off" />
													</div>
												</div>
											</div>
										</div>
										<ul class="list-inline pull-right">
											<li><button type="button" class="btn btn-primary prev-step">Back</button></li>
											<li><button type="button" class="btn btn-primary next-step">Continue</button></li>
										</ul>
									</div>
                                    <div id="LicenseInformation" class="tab-pane fade">
                                        <div class="usermanage-form">
                                            <div class="row">
												<div class="col-sm-3">
													<div class="form-group">
														<div class="input-group date form_datetime" data-date-format="yyyy-mm-dd" data-link-field="dtp_input1">
															<input class="form-control" name="start_date" type="text" readonly placeholder="Start Date">
															<span class="input-group-addon"><span class="fa fa-calendar"></span></span>
														</div>
													</div>
												</div>
												<div class="col-sm-3">
													<div class="form-group">
														<div class="input-group date form_datetime" data-date-format="yyyy-mm-dd" data-link-field="dtp_input1">
															<input class="form-control" name="end_date" type="text" readonly placeholder="End Date">
															<span class="input-group-addon"><span class="fa fa-calendar"></span></span>
														</div>
													</div>
												</div>
                                            </div>
											<div class="row">	
												<div class="col-sm-12">											
													<div class="Catfield-banner">
														<div class="Categoriesfield">
															<input type="text" class="form-control Licensing_Info_Categories" name="no_cat" id="no_cat" placeholder="No. of Categories" />
														</div>
														<div class="Catfield">
															<input type="text" class="form-control Licensing_Info_Categories" name="no_sim" id="no_sim" placeholder="No. of Simulation in each Category" />
														</div>
														<div class="Catfield">
															<input type="text" class="form-control Licensing_Info_Categories" name="no_admin" id="no_admin" placeholder="No. of Admin" />
														</div>
														<div class="Catfield">
															<input type="text" class="form-control Licensing_Info_Categories" name="no_auth" id="no_auth" placeholder="No. of Author" />
														</div>
														<div class="Catfield"> 
															<input type="text" class="form-control Licensing_Info_Categories" name="no_reviewer" id="no_reviewer" placeholder="No. of Reviewer" />
														</div>
														<div class="Catfield">
															<input type="text" class="form-control Licensing_Info_Categories" name="no_learner" id="no_learner" placeholder="No. of Learner" />
														</div>
														<div class="Catfield">
															<input type="text" class="form-control Licensing_Info_Categories" name="no_educator" id="no_educator" placeholder="No. of Educator" />
														</div>
													</div>
                                                </div>
											</div>
                                        </div>
										<ul class="list-inline pull-right">
											<li><button type="button" class="btn btn-primary prev-step">Back</button></li>
											<li><button type="button" class="btn btn-primary next-step">Continue</button></li>
										</ul>
								   </div>
                                    <div id="Hostingdetail" class="tab-pane fade">
                                        <div class="usermanage-form">
                                            <div class="row">
                                                <div class="col-sm-5">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control Licensing_Info_Categories" name="hosting_space" id="hosting_space" placeholder="Hosting Space (MB)" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
										<ul class="list-inline pull-right">
											<li><button type="button" class="btn btn-primary prev-step">Back</button></li>
											<li><button type="button" class="btn btn-primary next-step">Continue</button></li>
										</ul>										
                                    </div>
									<div id="ManageBranding" class="tab-pane fade">
										<div class="usermanage-form">
											<div class="Check-box"><label class="checkstyle"><input type="checkbox" name="mhome" value="1"><span class="checkmark"></span>Manage Home Page</label></div>
											<div class="Check-box"><label class="checkstyle"><input type="checkbox" name="mbrand" value="1"><span class="checkmark"></span>Manage Branding</label></div>
											<div class="Check-box"><label class="checkstyle"><input type="checkbox" name="proctor" value="1"><span class="checkmark"></span>Manage Proctoring</label></div>
										</div>
										<ul class="list-inline pull-right">
											<li><button type="button" class="btn btn-primary prev-step">Back</button></li>
											<li><button type="button" class="btn btn-primary next-step">Continue</button></li>
										</ul>
									</div>
									<div id="ManageTemp" class="tab-pane fade SuperManageTemp">
										<?php $sim_temp = ( ! empty($permissions['templates'])) ? explode(',', $permissions['templates']) : []; ?>
										<div class="Check-box GrantCheckbox">
											<label class="checkstyle">
												<input type="checkbox" class="checked_all" />
												<span class="checkmark"></span>
												<span class="text-R">Grant All Template</span>
											</label>
										</div>
										<div class="edit-sim-box">
											<?php if (in_array(1, $sim_temp) || in_array(2, $sim_temp)): ?>
											<div class="user_text editSim">Classic Simulation - Linear Simulation</div>
											<div class="linersim">				
												<div class="edit-sim-radio Linerm">
													<?php if (in_array(1, $sim_temp)): ?>
													<div class="Check-box">
														<label class="checkstyle">
															<input type="checkbox" name="sim_tmp[]" value="1" class="linear_tmp tmp_type" />
															<span class="checkmark"></span>
															<span class="text-R">Classic Template</span>
														</label>
														<span class="smallT">
															<p class="question-type">(MCQ, Drag and Drop, Swiping)</p>
															<p class="question-type">(Multiple Feedback)</p>
														</span>
													</div>
													<?php endif; ?>
													<?php if (in_array(2, $sim_temp)): ?>
													<div class="Check-box">
														<label class="checkstyle">
															<input type="checkbox" name="sim_tmp[]" value="2" class="linear_tmp tmp_type" />
															<span class="checkmark"></span>
															<span class="text-R">Multiple Template</span>
														</label>
														<span class="smallT">
															<p class="question-type">(MTF, Sequence, Sorting, MMCQ, MCQ, Drag and Drop, Swiping)</p>
															<p class="question-type">(Single Feedback)</p>
														</span>
													</div>
													<?php endif; ?>
												</div>
											</div>
											<?php endif; ?>
											<?php if (in_array(4, $sim_temp) || in_array(5, $sim_temp)): ?>
											<div class="linersim linersimBotom">
												<div class="user_text editSim">Classic Simulation - Branching Simulation</div>						
												<div class="edit-sim-radio Linerm">
													<?php if (in_array(4, $sim_temp)): ?>
													<div class="Check-box">
														<label class="checkstyle">
															<input type="checkbox" name="sim_tmp[]" value="4" class="branch_tmp tmp_type" />
															<span class="checkmark"></span>
															<span class="text-R">Classic Template</span>
														</label>
														<span class="smallT">
															<p class="question-type">(MCQ, Drag and Drop,Swiping)</p>
															<p class="question-type">(Multiple Feedback)</p>
														</span>
													</div>
													<?php endif; ?>
													<?php if (in_array(5, $sim_temp)): ?>
													<div class="Check-box">
														<label class="checkstyle">
															<input type="checkbox" name="sim_tmp[]" value="5" class="branch_tmp tmp_type" />
															<span class="checkmark"></span>
															<span class="text-R">Multiple Template</span>
														</label>
														<span class="smallT">
															<p class="question-type">(MTF, Sorting, MMCQ, MCQ, Drag and Drop, Swiping)</p>
															<p class="question-type">(Single Feedback)</p>
														</span>
													</div>
													<?php endif; ?>
												</div>
											</div>
											<?php endif; ?>
											<?php if (in_array(7, $sim_temp)): ?>
											<div class="user_text editSim">Open Response Simulation</div>
											<div class="linersim linersimlast">
												<div class="Check-box">
													<label class="checkstyle">
														<input type="checkbox" name="sim_tmp[]" value="7" class="tmp_type" />
														<span class="checkmark"></span>
														<span class="text-R">Open Response</span>
													</label>
												</div>
											</div>
											<?php endif; ?>
											<?php if (in_array(8, $sim_temp)): ?>
											<div class="user_text editSim">Video Simulation</div>
											<div class="linersim">
												<div class="Check-box">
													<label class="checkstyle">
														<input type="checkbox" name="sim_tmp[]" value="8" class="tmp_type" />
														<span class="checkmark"></span>
														<span class="text-R">Single Video Simulation</span>
													</label>
												</div>
											</div>
											<?php endif; ?>
											<?php if (in_array(6, $sim_temp) || in_array(9, $sim_temp)): ?>
											<div class="linersim">
												<div class="user_text editSim">Linear Video Simulation</div>				
												<div class="edit-sim-radio Linerm">
													<?php if (in_array(6, $sim_temp)): ?>
													<div class="Check-box">
														<label class="checkstyle">
															<input type="checkbox" name="sim_tmp[]" value="6" class="linear_tmp tmp_type" />
															<span class="checkmark"></span>
															<span class="text-R">Classic Template</span>
														</label>
														<span class="smallT">
															<p class="question-type">(MCQ, Drag and Drop, Swiping)</p>
															<p class="question-type">(Multiple Feedback)</p>
														</span>
													</div>
													<?php endif; ?>
													<?php if (in_array(9, $sim_temp)): ?>
													<div class="Check-box">
														<label class="checkstyle">
															<input type="checkbox" name="sim_tmp[]" value="9" class="linear_tmp tmp_type" />
															<span class="checkmark"></span>
															<span class="text-R">Multiple Template</span>
														</label>
														<span class="smallT">
															<p class="question-type">(MTF, Sequence, Sorting, MMCQ, MCQ, Drag and Drop, Swiping)</p>
															<p class="question-type">(Single Feedback)</p>
														</span>
													</div>
													<?php endif; ?>
												</div>
											</div>
											<?php endif; ?>
											<?php if (in_array(10, $sim_temp) || in_array(11, $sim_temp)): ?>
											<div class="linersim linersimBotom">
												<div class="user_text editSim">Branching Video Simulation</div>						
												<div class="edit-sim-radio Linerm">
													<?php if (in_array(10, $sim_temp)): ?>
													<div class="Check-box">
														<label class="checkstyle">
															<input type="checkbox" name="sim_tmp[]" value="10" class="branch_tmp tmp_type" />
															<span class="checkmark"></span>
															<span class="text-R">Classic Template</span>
														</label>
														<span class="smallT">
															<p class="question-type">(MCQ, Drag and Drop,Swiping)</p>
															<p class="question-type">(Multiple Feedback)</p>
														</span>
													</div>
													<?php endif; ?>
													<?php if (in_array(11, $sim_temp)): ?>
													<div class="Check-box">
														<label class="checkstyle">
															<input type="checkbox" name="sim_tmp[]" value="11" class="branch_tmp tmp_type" />
															<span class="checkmark"></span>
															<span class="text-R">Multiple Template</span>
														</label>
														<span class="smallT">
															<p class="question-type">(MTF, Sorting, MMCQ, MCQ, Drag and Drop, Swiping)</p>
															<p class="question-type">(Single Feedback)</p>
														</span>
													</div>
													<?php endif; ?>
												</div>
											</div>
											<?php endif; ?>
										</div>
										<ul class="list-inline pull-right">
											<li><button type="button" class="btn btn-primary prev-step">Back</button></li>
											<li><button type="button" class="btn btn-primary next-step">Continue</button></li>
										</ul>
									</div>
                                    <div id="PaymentDetail" class="tab-pane fade">
                                        <div class="usermanage-form">
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control" name="amount" id="amount" placeholder="Amount Per Year" />
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group form-select">
                                                        <select class="form-control selection" name="amount_type" id="amount_type">
														<option value="" selected="" disabled="disabled">Select Currency Type</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label for="">Payment Received</label>
														<div class="checkbox radiobtn formcheckbox1">
                                                        	<label><input type="radio" class="checkbox1" name="amount_rec" value="1"><span>Yes</span></label>
                                                    	</div>
                                                    	<div class="checkbox radiobtn formcheckbox1">
                                                        	<label><input type="radio" class="checkbox1" name="amount_rec" value="0"><span>No</span></label>
                                                    	</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <ul class="list-inline pull-right">
											<li><button type="button" class="btn btn-primary prev-step">Back</button></li>
											<li><input type="hidden" name="create_sub_client_account" value="1" />
												<button type="submit" name="create_db_btn" id="create_db_btn" class="btn btn-primary">Create Account</button>
											</li>
										</ul>
                                    </div>
                                </div>
                        	</div>
                      	</div>
					</div>
			  	</div>
			</form>
			<div class="usermanage_main">
            	<div class="table-box">
                	<div class="tableheader clearfix">
                    	<div class="tableheader_left">List of Sub Client's</div>
                        <div class="tableheader_right">
                        	<div class="Searchbox">
                            	<form method="get">
                                <input type="text" class="serchtext" name="search" placeholder="Search" required>
                                <div class="serchBTN"><img class="img-responsive" src="img/dash_icon/search.svg"></div>
                                </form>
                            </div>
                          </div>
						</div>
			  		</div>
					<div class="usermanage-form1">
                        <div class="table-responsive">
                          <table class="table table-striped">
                            <thead class="Theader">
                              <tr>
                                <th>#</th>
								<th>Created Date</th>
                                <th>Organization</th>
                                <th>Domain</th>
								<th>Role</th>
                                <th>Start Date</th>
                                <th>End Date</th>
                                <th>Hosting Space (MB)</th>
                                <th>Used Space (MB)</th>
                                <th>Amount Per Year</th>
								<th>Payment Received</th>
                                <th>Status</th>
                                <th>View</th>
                                <th>Edit</th>
                                <th>Delete</th>
                              </tr>
                            </thead>
                            <tbody>
                            <?php if ($results->execute() && $rowCount > 0):
								$i = $start + 1;
                                foreach ($results->fetchAll(PDO::FETCH_ASSOC) as $row):
								$domain_path = 'scenario/upload/'. $row['domain_name']; ?>
                              <tr>
                                <td><?php echo $i ?></td>
								<td><?php echo $row['create_date'] ?></td>
								<td><?php echo ( ! empty($row['organization'])) ? $row['organization'] : '--' ?></td>
                                <td><a href="https://<?php echo $row['domain_name'] ?>" target="_blank"><?php echo $row['domain_name'] ?></a></td>
								<td><span data-toggle="tooltip" data-placement="top" title="<?php echo ( ! empty($row['role'])) ? $row['role'] : '--' ?>"><i class="fa fa-info-circle fa-2x" aria-hidden="true"></i></span></td>
								<td><?php echo ( ! empty($row['sdate'])) ? $row['sdate'] : '--' ?></td>
                                <td><?php echo ( ! empty($row['edate'])) ? $row['edate'] : '--' ?></td>
                                <td><?php echo ( ! empty($row['hosting_space'])) ? $row['hosting_space'] : '--' ?></td>
                                <td><?php echo $db->folderSize($domain_path); ?></td>
                                <td><?php echo ( ! empty($row['amount'])) ? $row['amount'] .' '. $row['amount_type'] : '--' ?></td>
                                <td><?php echo ( ! empty($row['pay_received'])) ? 'YES' : 'NO' ?></td>
								<th><?php echo (empty($row['status'])) ? '<button class="alert alert-success inactive_client" data-client-id="'. $row['client_id'] .'">Active</button>' : '<button class="alert alert-danger active_client" data-client-id="'. $row['client_id'] .'">Inactive</button>'; ?></th>
								<td><a href="<?php echo $db->getBaseUrl('view-client.php?view=true&cid='. base64_encode($row['client_id'])); ?>" class="view"><img class="svg" src="img/list/review.svg" /></a></td>
								<td><a href="<?php echo $db->getBaseUrl('edit-sub-client.php?edit=true&cid='. base64_encode($row['client_id'])); ?>"><img class="svg" src="img/list/edit.svg" /></a></td>
                                <td><a href="javascript:void(0);" data-cid="<?php echo $row['client_id'] ?>" class="delete"><img class="svg" src="img/list/delete.svg" /></a></td>
                              </tr>
                              <?php $i++; endforeach; else: ?>
                              <tr><td colspan="15" align="center"><strong>No data available in database.</strong></td></tr>
                              <?php endif; ?>
                            </tbody>
                          </table>
                        </div>
                        <div class="pull-right">
                          <nav aria-label="navigation"><?php echo ( ! empty($pagination) ) ? $pagination : '&nbsp;'; ?></nav>
                        </div>
                  	</div>
                </div>
		  	</div>
	  	</div>
	</div>
</div>

<script type="text/javascript" src="content/js/inputmask/jquery.inputmask.js"></script>
<script type="text/javascript" src="content/js/bootstrap-datetimepicker.js" charset="UTF-8"></script>
<script type="text/javascript">
$(document).ready(function() {
	$('.checked_all').on('change', function() {
		$('.tmp_type').prop('checked', $(this).prop("checked"));
	});

	$('.tmp_type').change(function() {
		if ($('.tmp_type:checked').length == $('.tmp_type').length) {
			$('.checked_all').prop('checked', true);
		}
		else {
			$('.checked_all').prop('checked', false);
		}
	});

	$('#amount_type').find("option:eq(0)").html("Please wait..");
	$.ajax({
		type: "Get",
		url: "includes/currencies.json",
		dataType: "json",
		success: function(data) {
			$('#amount_type').find("option:eq(0)").html("Select Currency Type");
			$.each(data, function (key, val) {
				var option = '<option value="'+ val.name +' ('+ val.symbol +')" data-currency-code="'+val.cc+'">'+ val.name +' ('+ val.symbol +')</option>';
				$('#amount_type').append(option);
			});
		},
		error:function(){ swal({text: 'Oops. something went wrong. currencies not load.', buttons: false, icon: "error", timer: 2000}); }
	});
	
	jQuery('body').on('change', '#amount_type', function() {
		var code = $(this).find(':selected').attr('data-currency-code');
		$('#currency_code').val(code);
	});
	
	$('.country').find("option:eq(0)").html("Please wait..");
	$.getJSON('includes/main-process.php?type=getCountry', function (data) {
		$('.country').find("option:eq(0)").html("Select Country");
		$.each(data, function (key, val) {
			var option = '<option value="'+val.id+'">'+val.name+'</option>';
			$('.country').append(option);
		})
	});

	jQuery('body').on('change', '.country', function() {
		var cid		= $(this).val();
		var state	= $(this).data('starget');
		var city	= $(this).data('ctarget');
		if (cid != '') {
			$("#"+ state +" option:gt(0)").remove();
			$("#"+ city +" option:gt(0)").remove();
			$("#"+ state).find("option:eq(0)").html("Please wait..");
			$.getJSON('includes/main-process.php?type=getState&countryId='+cid, function (data) {
				$("#"+ state).find("option:eq(0)").html("Select State");
				$.each(data, function (key, val) {
					var option = '<option value="'+val.id+'">'+val.name+'</option>';
					$("#"+ state).append(option);
				})
			});
		} else { $("#"+ state +" option:gt(0)").remove(); $("#"+ city +" option:gt(0)").remove(); }
	})

	jQuery('body').on('change', '.state', function() {
		var sid	 = $(this).val();
		var city = $(this).data('ctarget');
		if (sid != '') {
			$("#"+ city +" option:gt(0)").remove();
			$("#"+ city).find("option:eq(0)").html("Please wait..");
			$.getJSON('includes/main-process.php?type=getCity&stateId='+sid, function (data) {
				$("#"+ city).find("option:eq(0)").html("Select City");
				$.each(data, function (key, val) {
					var option = '<option value="'+val.id+'">'+val.name+'</option>';
					$("#"+ city).append(option);
				})
			});
		}
		else { $("#"+ city +" option:gt(0)").remove(); }
	})

	$('.phonecode').find("option:eq(0)").html("Please wait..");
	$.getJSON('includes/main-process.php?type=getCountry', function (data) {
		$('.phonecode').find("option:eq(0)").html("Select Phone Code");
		$.each(data, function (key, val) {
			var option = '<option value="'+val.phonecode+'">'+val.name +' (+'+ val.phonecode  +')</option>';
			$('.phonecode').append(option);
		});
	});

	$("#tax_id").inputmask("**-*********", {"clearIncomplete": true});

	$("#executive_mob,#decision_mob,#billing_mob").inputmask("9999999999", {"clearIncomplete": true});

	$('.form_datetime').datetimepicker({
		weekStart: 1,
		todayBtn:  0,
		autoclose: 1,
		todayHighlight: 1,
		startView: 2,
		forceParse: 0,
		minView: 2,
		showMeridian: 1,
		clearBtn: 1
	});

	$('.noSpace').keyup(function() {
		this.value = this.value.replace(/\s/g, '');
	});
	
	$('#role').multiselect({
		includeSelectAllOption: true,
		filterPlaceholder: 'Search & Select',
		enableCaseInsensitiveFiltering : true,
		enableFiltering: true,
		maxHeight: 400,
		buttonWidth: '280px'
	});

	$('[data-toggle="tooltip"]').tooltip();
	
	$('#db_name').keydown(function(event){
		var key = event.charCode || event.keyCode || event.which;
		var dbname = $(this).val();
		if (dbname.length <= 8 && key === 8 || key === 46 || key === 17 || key === 32) {
			return false;
		}
		else { return true; }
	});
	
	$("#domain_name").on('change', function(){
		var val = $(this).val();
		if ( ! (/^[a-zA-Z0-9_-]{3,25}$/i.test(val))) {
			$(this).val('');
			swal({text:'Please give valid Subdomain name, no spaces and special characters allowed.!', buttons: false, icon: "warning", timer: 3000});
		}
		return false;
	});
	
	$('#upload').click(function() {
		$(this).attr('disabled', 'disabled').html('<i class="fa fa-refresh fa-spin fa-fw"></i><span class="sr-only">Loading...</span> Please wait....');
		var file_data = $('#logo').prop('files')[0];
		var form_data = new FormData();
		form_data.append('logo', file_data);
		form_data.append('add_client_logo', true);
		$.ajax({
			url: "includes/upload.php",
			type: "POST",
			data: form_data,
			contentType: false,
			cache: false,
			processData:false,
			success: function(resdata) {
				var res = $.parseJSON(resdata);
				if (res.success == true) {
					var viewHtml = '<a href="img/logo/'+res.logo+'" target="_blank" title="View Logo"><i class="fa fa-eye"></i></a>';
					$('#splashImg').show().html(viewHtml);
					$('#ImgDelete').show();
					$('.delete_logo').attr('data-img-name', res.logo);
					$('#logo_name').val(res.logo);
					$('#upload').html('Upload').removeAttr('disabled', 'disabled');
					swal({text: res.msg, buttons: false, timer: 1000});
				}
				else if (res.success == false) {
					$('#logo_name').val('');
					$('#splashImg').hide('slow');
					$('#upload').html('Upload').removeAttr('disabled', 'disabled');
					swal({text: res.msg, buttons: false, icon: "warning", timer: 1000});
				}
			},error: function() {
				$('#splashImg').hide('slow');
				swal({text: 'Oops. something went wrong please try again.', buttons: false, icon: "error", timer: 2000});
			}
		});
	});
	
	$('.delete_logo').click(function() {
		var file_name   = $(this).attr("data-img-name");
		var dataString	= 'delete='+ true + '&client_logo_file='+ file_name;
		if (file_name) {
			swal({
				title: "Are you sure?",
				text: "Delete this Logo.",
				icon: "warning",
				buttons: [true, 'Delete'],
				dangerMode: true, }).then((willDelete) => { if (willDelete) {
					$('#splashImg').show().html(' <img src="scenario/img/loader.gif"> Please wait....');
					$.ajax({
						url: "includes/delete-file.php",
						type: "POST",
						data: dataString,
						cache: false,
						success: function(resdata) {
							var res = $.parseJSON(resdata);
							if (res.success == false) {
								swal({text: res.msg, buttons: false, icon: "error", timer: 1000});
								$('#splashImg').hide('slow');
							}
							else if (res.success == true) {
								$('#splashImg, #ImgDelete').hide('slow');
								$('#logo_name').val('');
								swal({text: res.msg, buttons: false, timer: 1000});
							}
						},error: function() {
							swal({text: 'Oops. something went wrong please try again.', buttons: false, icon: "error", timer: 2000});
						}
					});
					} else { swal("Your file is safe.!", { buttons: false, timer: 1000, }); }
				});
			}
	});

	$('.delete').click(function () {
		var id = $(this).attr('data-cid');
		var dataString	= 'delete_client='+ true +'&client_id='+ id;
		if (id != '') {
			swal({
				title: "Are you sure?",
				text: "Delete this client account with all data.",
				icon: "warning",
				buttons: [true, 'Delete'],
				dangerMode: true, }).then((willDelete) => { if (willDelete) {
					$.LoadingOverlay("show");
					$.ajax({
						url: "includes/main-process.php",
						type: "POST",
						data: dataString,
						cache: false,
						success: function(resdata) {
							var res = $.parseJSON(resdata);
							if (res.success == false) {
								swal({text: res.msg, buttons: false, icon: "error", timer: 1000});
								$.LoadingOverlay("hide");
							}
							else if (res.success == true) {
								$.LoadingOverlay("hide");
								swal({text: res.msg, buttons: false, timer: 1000});
								setTimeout(function() { window.location.reload(); }, 2000);
							}
						},error: function() {
							swal({text: 'Oops. something went wrong please try again.', buttons: false, icon: "error", timer: 2000});
						}
					});
				}
			});
		}
	});

	$('.inactive_client').on('click', function(){
		var cid = $(this).data('client-id');
		swal({
			title: "Are you sure?",
			text: "Inactive this account.",
			icon: "warning",
			buttons: [true, 'Inactive'],
			dangerMode: true, }).then((willDelete) => { if (willDelete) {
			$.LoadingOverlay("show");
			$.ajax({
				url: 'includes/main-process.php',
				type: "POST",
				data: 'inactive_client='+ true + '&client_id='+ cid,
				cache: false,
				success: function(resdata) {
					$.LoadingOverlay("hide");
					var res = $.parseJSON(resdata);
					if (res.success == true) {
						swal({text: res.msg, buttons: false, icon: "success", timer: 2000});
						setTimeout(function() { window.location.reload(); }, 3000);
					}
					else if (res.success == false) {
						swal({text: res.msg, buttons: false, icon: "error", timer: 2000});
					}
				},error: function() {
					$.LoadingOverlay("hide");
					swal({text: 'Oops. something went wrong please try again.', buttons: false, icon: "error", timer: 2000});
				}
			});
		}});
	});

	$('.active_client').on('click', function(){
		var cid = $(this).data('client-id');
		swal({
			title: "Are you sure?",
			text: "Active this account.",
			icon: "warning",
			buttons: [true, 'Active'],
			dangerMode: true, }).then((willDelete) => { if (willDelete) {
			$.LoadingOverlay("show");
			$.ajax({
				url: 'includes/main-process.php',
				type: "POST",
				data: 'active_client='+ true + '&client_id='+ cid,
				cache: false,
				success: function(resdata) {
					$.LoadingOverlay("hide");
					var res = $.parseJSON(resdata);
					if (res.success == true) {
						swal({text: res.msg, buttons: false, icon: "success", timer: 2000});
						setTimeout(function() { window.location.reload(); }, 3000);
					}
					else if (res.success == false) {
						swal({text: res.msg, buttons: false, icon: "error", timer: 2000});
					}
				},error: function() {
					$.LoadingOverlay("hide");
					swal({text: 'Oops. something went wrong please try again.', buttons: false, icon: "error", timer: 2000});
				}
			});
		}});
	});
	
	//Wizard
    $('a[data-toggle="tab"]').on('show.bs.tab', function(e) {
        var $target = $(e.target);
		if ($target.parent().hasClass('disabled')) {
            return false;
        }
    });

    $(".next-step").click(function (e) {
        var $active = $('#collapseOne .nav-tabs li.active');
        $active.next().removeClass('disabled');
        nextTab($active);
    });

    $(".prev-step").click(function (e) {
        var $active = $('#collapseOne .nav-tabs li.active');
        prevTab($active);
    });

	function nextTab(elem) {
		$(elem).next().find('a[data-toggle="tab"]').click();
	}
	
	function prevTab(elem) {
		$(elem).prev().find('a[data-toggle="tab"]').click();
	}
});
</script>
<?php
require_once 'includes/footer.php';
