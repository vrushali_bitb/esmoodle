<?php 
session_start();
if(isset($_SESSION['username'])){
	$userid	= $_SESSION['userId'];
    $user = $_SESSION['username'];
	$role = $_SESSION['role'];
}
else {
    exit;
}
require_once 'config/db.class.php';
$db  = new DBConnection();
$gid = (isset($_POST['gid'])) ? $_POST['gid'] : '';
if ( ! empty($gid))
	$data = $db->getLearnerByGroup($gid);
	$i = 0;
	foreach ($data as $learner):
		if ( ! empty($learner['learner'])) $learners[$data[$i]['group_name']] = $db->getLearnerMultiId($learner['learner']);
		$i++;
	endforeach; if ( ! empty($learners)): ?>
    <select class="form-control" data-placeholder="Learner List">
    <?php $l = 0; foreach ($learners as $k => $v): ?>
    <optgroup label="<?php echo ucwords($k); ?>">
	<?php $u = 0; foreach ($v as $username): ?>
	<option value=""><?php echo ucwords($username['username']); ?></option>
	<?php endforeach; ?>
    </optgroup>
    <?php $l++; endforeach; ?>
    </select>
    <?php else: ?>
    <select class="form-control" data-placeholder="Learner List">
    <option>Learner not found.</option>
    </select>
<?php endif; ?>
