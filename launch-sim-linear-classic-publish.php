<?php 
ob_start();
session_start();
if (isset($_SESSION['username'])):
	$user 	= $_SESSION['username'];
	$role 	= $_SESSION['role'];
	$userid = $_SESSION['userId'];
	$domain = (isset($_SESSION['domain_name'])) ? $_SESSION['domain_name'] : $_SERVER['HTTP_HOST'];
else:
    header('location: index.php');
endif;
require_once 'config/db.class.php';
$db 		= new DBConnection();
require_once 'includes/scorm-header.php';
$scorm_head	= ob_get_contents();
$sim_id		= (isset($_GET['sim_id'])) ? $_GET['sim_id'] : die('Error');
$type		= (isset($_GET['type'])) ? $_GET['type'] : '';
$simData	= $db->getScenario($sim_id);
$simID 		= $simData['scenario_id'];
$title  	= $simData['Scenario_title'];
$simPage  	= $db->getSimPage($simID);
$brand		= $db->getSimBranding($simID);
$back_img	= ( ! empty($simData['sim_back_img'])) ? 'img/char_bg/'. $simData['sim_back_img'] : '';
$path		= $db->getBaseUrl('scenario/upload/'. $domain .'/');
$root_path	= $db->rootPath().'scenario/upload/'. $domain .'/';
$aPoster	= $db->getBaseUrl('scenario/img/audio-poster.jpg');
$noImage	= $db->getBaseUrl('scenario/img/palceholder.jpg');
$vtheme		= 'forest'; #city, fantasy, forest, sea
$left_char	= $right_char = '';

/* COMPETENCY SCORE */
$sqlcom	= "SELECT (SUM(comp_val_1) + SUM(comp_val_2) + SUM(comp_val_3) + SUM(comp_val_4) + SUM(comp_val_5) + SUM(comp_val_6)) AS totalCompval FROM competency_tbl WHERE scenario_id = '". $simID ."'";
$comexc	= $db->prepare($sqlcom); $comexc->execute();
$comRes	= $comexc->fetch(PDO::FETCH_ASSOC);
$totalCompval = ( ! empty($comRes['totalCompval'])) ? $comRes['totalCompval'] : 0;

if ( ! empty($simData['sim_char_img'])):
	$left_char = 'img/char_bg/'. $simData['sim_char_img'];
elseif ( ! empty($simData['sim_char_left_img'])):
	$left_char = 'img/char_bg/'. $simData['sim_char_left_img'];
	elseif ( ! empty($simData['char_own_choice']) && file_exists($root_path . $simData['char_own_choice'])):
	$left_char = $path . $simData['char_own_choice'];
endif;
if ( ! empty($simData['sim_char_right_img'])):
	$right_char = 'img/char_bg/'. $simData['sim_char_right_img'];
endif;
if ( ! empty($brand['font_type'])):
$ftype = str_replace('+', ' ', $brand['font_type']); ?>
<style type="text/css">
@import url("https://fonts.googleapis.com/css2?family=<?php echo $brand['font_type'] ?>:wght@100;200;300;400;500;600;700;800;900&display=swap");
body { font-family: '<?php echo $ftype ?>' !important; }
h1,h2,h3,h4,h5,h6 { font-family: '<?php echo $ftype ?>' !important; }
p { font-family: '<?php echo $ftype ?>' !important; }
</style>
<?php endif;
if ($type == 'scrom'): ?>
<script src="SCORMGeneric.js" type="text/javascript" language="JavaScript"></script>
<script type="text/javascript" language="JavaScript">
    function init(score, status) {
		if (status == 1) {
			if (SCOInitialize() == "true") {
				objAPI.LMSSetValue("cmi.core.lesson_status", "completed");
			}
			objAPI.LMSSetValue("cmi.core.lesson_status", "completed");
			objAPI.LMSSetValue("cmi.core.lesson_status", "passed");
			objAPI.LMSSetValue("cmi.core.score.raw", score);
			finish();
		}
		else if (status == 2) {
			if (SCOInitialize() == "true") {
				objAPI.LMSSetValue("cmi.core.lesson_status", "incomplete");
			}
			objAPI.LMSSetValue("cmi.core.lesson_status", "incomplete");
			objAPI.LMSSetValue("cmi.core.lesson_status", "failed");
			objAPI.LMSSetValue("cmi.core.score.raw", score);
			Failed();
		}
		else if (status == 3) {
			if (SCOInitialize() == "true") {
				objAPI.LMSSetValue("cmi.core.lesson_status", "completed");
			}
			objAPI.LMSSetValue("cmi.core.lesson_status", "completed");
			objAPI.LMSSetValue("cmi.core.lesson_status", "failed");
			objAPI.LMSSetValue("cmi.core.score.raw", score);
			Failed();
		}
    }
    function finish() {
		if (objAPI != null) {
            objAPI.LMSFinish("cmi.objectives.n.status", "passed");
        }
        objAPI.LMSFinish("cmi.objectives.n.status", "passed");
    }
	function Failed() {
		if (objAPI != null) {
            objAPI.LMSFinish("cmi.objectives.n.status", "failed");
        }
        objAPI.LMSFinish("cmi.objectives.n.status", "failed");
    }
</script>
<?php endif; ?>
<script type="text/javascript">
	window.addEventListener('beforeunload', function(e) {
	  // Cancel the event
	  e.preventDefault(); // If you prevent default behavior in Mozilla Firefox prompt will always be shown
	  // Chrome requires returnValue to be set
	  e.returnValue = '';
	});
	
	function disableF5(e) { if ((e.which || e.keyCode) == 116 || (e.which || e.keyCode) == 82) e.preventDefault(); };
	
	$(document).ready(function(){
		$(document).on("keydown", disableF5);
	});
	
	$(document).bind("contextmenu", function(e){
		return false;
	});

	history.pushState(null, null, location.href);

    window.onpopstate = function() { history.go(1); };
	
	$.LoadingOverlay("show");
	
	var simBrand = <?php echo json_encode($brand); ?> 
</script>
<style type="text/css">
	.close-banner img {
		width: 30px;
	}
	.feedback_box:after {
		content: '';
		width: 100%;
		height: 100vh;
		background: black;
		position: absolute;
		top: 0;
		opacity: 0.6;
		z-index: -1;
		right: 0;
	}
	.modal-open .modal {
		overflow-x: hidden;
		overflow-y: hidden;
	}
<?php if ( ! empty($back_img)): ?>
	.linear_classic {
		background: url("<?php echo $db->getBaseUrl($back_img); ?>") center top no-repeat;
		background-size: cover;
		overflow: hidden;
		height: 100vh;
		display: flex;
	}
<?php elseif ( ! empty($simData['bg_color'])): ?>
	.linear_classic { background: <?php echo $simData['bg_color']; ?>; display:flex; }
<?php endif; ?>
<?php if ( ! empty($brand['option_bg'])): ?>
	.questioninput:before { background: <?php echo $brand['option_bg']; ?>; }
	.questioninput:after { background: <?php echo $brand['option_bg']; ?>; }
	.rectangle1:after { background: <?php echo $brand['option_bg']; ?>; }
<?php endif; ?>
</style>
<div class="menu-banner-box">
	<div class="time-banner-box">
		<button class="V_S-tetail view_scenario" data-fancybox data-type="iframe" data-src="index.html">
    		<img class="Deskview" src="<?php echo $db->getBaseUrl('img/list/view_scenario_b.svg'); ?>" />
        	<img class="mobview" src="<?php echo $db->getBaseUrl('img/list/view_scenario.svg'); ?>" />
        	View Scenario
    	</button>
  	</div>
    <!----SIM-Timer---->
    <div class="timer-banner">
    	<div class="time-text">
        	<span class="timer-pause" data-minutes-left="<?php echo $simData['duration'] ?>" style="pointer-events: none;"></span>
    	</div>
  	</div>
</div>
<script type="text/javascript">
var timer = $('.timer-pause').startTimer({
	onComplete: function(element) {
		swal({text: "Simulation time end. please relaunch.!", icon: "info"}); },
		allowPause: true
	});
</script>
<div class="linear_classic">
	<?php if ( ! empty($left_char)): ?>
	<div class="CharFlex <?php echo ( ! empty($right_char)) ? 'CharFlextwo' : ''; ?>"><img class="img-fulid qusTempimg" src="<?php echo $db->getBaseUrl($left_char); ?>" /></div>
	<?php endif; ?>
	<div class="flexQTeMP <?php echo ( ! empty($right_char)) ? 'flexQTeMPtwochar' : ''; ?><?php echo ( ! empty($left_char) && empty($right_char)) ? 'flexQTeMPonechar' : ''; ?><?php echo (empty($left_char) && empty($right_char)) ? 'flexQTeMPnochar' : ''; ?>">
	<?php 
	$qSql = $db->prepare("SELECT * FROM question_tbl WHERE scenario_id = '". $simID ."' ORDER BY qorder = 0, qorder"); $qSql->execute();
	$totalQ = $qSql->rowCount();
	if ($totalQ > 0):
		$q = 1;
		$qdata = $qSql->fetchAll(PDO::FETCH_ASSOC);
		foreach ($qdata as $qrow):
		$question_id	= $qrow['question_id'];
		$shuffle		= ( ! empty($qrow['shuffle'])) ? TRUE : FALSE;
		$ansData		= $db->getAnswersByQuestionId(md5($qrow['question_id']), $shuffle);
		$qtype	 		= $qrow['question_type'];
		$show	 		= ($q == 1) ? 'style="display:block"' : 'style="display:none"'; ?>
		<div class="MatchingQusBannerlunch MatchingQusBanner_<?php echo $q; ?> multimatchingB" <?php echo $show; ?>>
			<div class="Matchingcontainer">
				<div class="Questmtf">
					<div class="MatchingQusText fade-in">
						<div class="qus-text">
							<h3>QUESTION <?php echo $q .'/'. $totalQ; ?></h3>
							<p class="more"><?php echo stripslashes($qrow['questions']); ?></p>
						</div>
						<div class="qusassestbox">
							<div class="Qusimg">
							<?php if ( ! empty($qrow['audio']) && file_exists($root_path . $qrow['audio'])): ?>
							<audio id="qvideo-<?php echo $q ?>" class="video-js vjs-theme-<?php echo $vtheme ?>" controls preload="auto" width="300px" height="100px" data-setup="{}" data-type="audio" poster="<?php echo $aPoster ?>">
								<source src="<?php echo $path . $qrow['audio'] ?>" type="audio/<?php echo strtolower(pathinfo($qrow['audio'], PATHINFO_EXTENSION)); ?>" />
							</audio>
							<?php elseif ( ! empty($qrow['video']) && file_exists($root_path . $qrow['video'])): ?>
							<a href="<?php echo $path . $qrow['video']; ?>" data-fancybox data-caption="<?php echo stripslashes($qrow['questions']); ?>" class="qassets_icon">
								<video id="qvideo-<?php echo $q ?>" class="video-js vjs-theme-<?php echo $vtheme ?>" controls preload="auto" width="640" height="264" data-setup="{}" data-type="video">
									<source src="<?php echo $path . $qrow['video']; ?>" type="video/<?php echo strtolower(pathinfo($qrow['video'], PATHINFO_EXTENSION)); ?>" />
								</video>
							</a>
							<?php elseif ( ! empty($qrow['screen']) && file_exists($root_path . $qrow['screen'])): ?>
							<a href="<?php echo $path . $qrow['screen']; ?>" data-fancybox data-caption="<?php echo stripslashes($qrow['questions']); ?>" class="qassets_icon">
								<video id="qvideo-<?php echo $q ?>" class="video-js vjs-theme-<?php echo $vtheme ?>" controls preload="auto" width="200" height="100" data-setup="{}" data-type="video">
									<source src="<?php echo $path . $qrow['screen']; ?>" type="video/<?php echo strtolower(pathinfo($qrow['screen'], PATHINFO_EXTENSION)); ?>" />
								</video>
							</a>
							<?php elseif ( ! empty($qrow['image']) && file_exists($root_path . $qrow['image'])): ?>
							<a href="<?php echo $path . $qrow['image']; ?>" data-fancybox data-caption="<?php echo stripslashes($qrow['questions']); ?>" class="qassets_icon">
								<img class="img-fluid" src="<?php echo $path . $qrow['image']; ?>">
							</a>
							<?php elseif ( ! empty($qrow['document']) && file_exists($root_path . $qrow['document'])): ?>
							<a href="javascript:;" data-src="<?php echo $path . $qrow['document']; ?>" data-fancybox data-type="iframe" data-caption="<?php echo stripslashes($qrow['questions']); ?>" class="document">
								<img class="img-fluid pdfclissicsIm" src="<?php echo $db->getBaseUrl('img/icons/pdf_icon.svg'); ?>" />
							</a>
							<?php elseif ( ! empty($qrow['speech_text'])): ?>
							<button onclick="speak('div.intro_<?php echo $q ?>')">
								<i id="playicon" class="fa fa-play playicon" aria-hidden="true"></i>
								<i id="pauseicon" class="fa fa-pause pauseicon" style="display:none" aria-hidden="true"></i></button>
								<div class="intro_<?php echo $q ?>" style="display:none;"><?php echo $qrow['speech_text'] ?></div>
							<?php endif; ?>
							</div>
						</div>
					</div>
				</div>
				<?php #------MCQ------
				if ($qtype == 4): ?>
				<div class="MCQOption bannermcq" id="option_<?php echo $q; ?>" data-qtype="<?php echo $qtype ?>">
				<?php foreach ($ansData as $mcq_data):
					$mtf_feedback[] = $db->getFeedback($question_id, $mcq_data['answer_id']); ?>
					<div class="row options option_brand advertise_container fade-in" data-aid="<?php echo $mcq_data['answer_id'] ?>" data-feed-show="true" data-com1="<?php echo $mcq_data['ans_val1'] ?>" data-com2="<?php echo $mcq_data['ans_val2'] ?>" data-com3="<?php echo $mcq_data['ans_val3'] ?>" data-com4="<?php echo $mcq_data['ans_val4'] ?>" data-com5="<?php echo $mcq_data['ans_val5'] ?>" data-com6="<?php echo $mcq_data['ans_val6'] ?>">
						<div class="multiQusChecK"><div class="Check-boxQB Check-boxMCQ"><i class="fa fa-circle" aria-hidden="true"></i></div></div>
						<div class="multiQustext"><?php echo stripslashes($mcq_data['choice_option']); ?></div>
					</div>
					<?php endforeach; ?>
					<!--MTF-FEEDBACK-->
					<?php if ( ! empty($mtf_feedback)): $mtf = 1; foreach ($mtf_feedback as $mtf_feed_data): ?>
					<div class="modal feedback_box fade" role="dialog" id="Feedback_<?php echo $mtf_feed_data['answer_id'] ?>" data-keyboard="false" data-backdrop="static">
						<div class="modal-dialog modal-dialog-centered">
							<div class="modal-content">
								<div class="modal-header feedback-header">
									<button type="button" class="feedclose" data-show-report="<?php echo ($totalQ == $q) ? 'true' : 'false'; ?>" data-target="<?php echo $q + 1 ?>" data-cur="<?php echo $q ?>" data-dismiss="modal">&times;</button>
									<h3 class="modal-title" id="exampleModalLabel">FEEDBACK</h3>
								</div>
								<div class="modal-body">
									<div class="row feedtextarea">
										<div class="row feed">
											<div class="flexfeed">
												<div class="flexfeed img-videobox">
													<?php if ( ! empty($mtf_feed_data['feed_audio']) && file_exists($root_path . $mtf_feed_data['feed_audio'])): ?>
													<audio id="fvideo-1" class="video-js vjs-theme-<?php echo $vtheme ?>" controls preload="auto" width="300px" height="100px" data-setup="{}" data-type="audio" poster="<?php echo $aPoster ?>">
														<source src="<?php echo $path . $mtf_feed_data['feed_audio'] ?>" type="audio/<?php echo strtolower(pathinfo($mtf_feed_data['feed_audio'], PATHINFO_EXTENSION)); ?>" />
													</audio>
													<?php elseif ( ! empty($mtf_feed_data['feed_video']) && file_exists($root_path . $mtf_feed_data['feed_video'])): ?>
													<video id="fvideo-1" class="video-js vjs-theme-<?php echo $vtheme ?>" controls preload="auto" width="640" height="264" data-setup="{}" data-type="video">
														<source src="<?php echo $path . $mtf_feed_data['feed_video'] ?>" type="video/<?php echo strtolower(pathinfo($mtf_feed_data['feed_video'], PATHINFO_EXTENSION)); ?>" />
													</video>
													<?php elseif ( ! empty($mtf_feed_data['feed_screen']) && file_exists($root_path . $mtf_feed_data['feed_screen'])): ?>
													<video id="fvideo-1" class="video-js vjs-theme-<?php echo $vtheme ?>" controls preload="auto" width="640" height="264" data-setup="{}" data-type="video">
														<source src="<?php echo $path . $mtf_feed_data['feed_screen'] ?>" type="video/<?php echo strtolower(pathinfo($mtf_feed_data['feed_screen'], PATHINFO_EXTENSION)); ?>" />
													</video>
													<?php elseif ( ! empty($mtf_feed_data['feed_image']) && file_exists($root_path . $mtf_feed_data['feed_image'])): ?>
													<a href="<?php echo $path . $mtf_feed_data['feed_image']; ?>" data-fancybox data-caption="<?php echo stripslashes($mtf_feed_data['feedback']); ?>">
														<img class="img-fluid" src="<?php echo $path . $mtf_feed_data['feed_image']; ?>" />
													</a>
													<?php elseif ( ! empty($mtf_feed_data['feed_document']) && file_exists($root_path . $mtf_feed_data['feed_document'])): ?>
													<a href="javascript:;" data-src="<?php echo $path . $mtf_feed_data['feed_document']; ?>" data-fancybox data-type="iframe" data-caption="<?php echo stripslashes($mtf_feed_data['feedback']); ?>" class="document">
														<img class="img-fluid pdfclissicsIm" src="<?php echo $db->getBaseUrl('img/icons/pdf_icon.svg'); ?>">
													</a>
													<?php elseif ( ! empty($mtf_feed_data['feed_speech_text'])): ?>
														<button onclick="speak_feedback('div.feedback_intro_<?php echo $mtf ?>')"><i id="playicon_feedback" class="fa fa-play playicon" aria-hidden="true"></i><i id="pauseicon_feedback" class="fa fa-pause pauseicon" style="display:none" aria-hidden="true"></i></button>
														<div class="feedback_intro_<?php echo $mtf ?>" style="display:none;"><?php echo $mtf_feed_data['feed_speech_text'] ?></div>
													<?php endif; ?>
												</div>
												<div class="feedbacktext"><?php echo ( ! empty($mtf_feed_data['feedback'])) ? stripslashes($mtf_feed_data['feedback']) : ''; ?></div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<?php $mtf++; endforeach; endif; ?>
					<div class="submitMTFcenter col-sm-12">
						<button type="button" id="btn_<?php echo $q; ?>" class="btn submitbtn fade-in" data-cur="<?php echo $q ?>" data-show-report="<?php echo ($totalQ == $q) ? 'true' : 'false'; ?>">SUBMIT</button>
					</div>
				</div>
				<?php #------SWIPING------
				elseif ($qtype == 7): ?>
				<div class="MCQOption" id="option_<?php echo $q; ?>" data-qtype="<?php echo $qtype ?>">
					<div class="home-demo">
						<div class="row row CoursalsTYbullet">
							<div class="large-12 columns">
								<div class="owl-carousel fadeIn swipingcoursal">
								<?php foreach ($ansData as $swipe_data):
								$swip_feedback[] = $db->getFeedback($question_id, $swipe_data['answer_id']); ?>
								<div class="swipeoptions option_brand" data-aid="<?php echo $swipe_data['answer_id'] ?>" data-feed-show="true" data-com1="<?php echo $swipe_data['ans_val1'] ?>" data-com2="<?php echo $swipe_data['ans_val2'] ?>" data-com3="<?php echo $swipe_data['ans_val3'] ?>" data-com4="<?php echo $swipe_data['ans_val4'] ?>" data-com5="<?php echo $swipe_data['ans_val5'] ?>" data-com6="<?php echo $swipe_data['ans_val6'] ?>">
									<div class="multiDBDboxAllIGN">
										<?php $swip_cls = 'dd_twrap'; if ( ! empty($swipe_data['image']) && file_exists($root_path . $swipe_data['image'])): $swip_cls = ''; ?>
										<a href="<?php echo $path . $swipe_data['image']; ?>" data-fancybox data-caption="<?php echo stripslashes($swipe_data['choice_option']); ?>" class="qassets_icon"><img class="feedimage" src="<?php echo $path . $swipe_data['image'] ?>" /></a>
										<?php endif; ?>
										<div class="textwraping <?php echo $swip_cls; ?>"><?php echo stripslashes($swipe_data['choice_option']); ?></div>
									</div>
								</div>
								<?php endforeach; ?>
								</div>
								<!--SWIP-FEEDBACK-->
								<?php if ( ! empty($swip_feedback)): $swip = 1; foreach ($swip_feedback as $swip_feed_data): ?>
								<div class="modal feedback_box fade" role="dialog" id="Feedback_<?php echo $swip_feed_data['answer_id'] ?>" data-keyboard="false" data-backdrop="static">
									<div class="modal-dialog modal-dialog-centered">
										<div class="modal-content">
											<div class="modal-header feedback-header">
												<button type="button" class="feedclose" data-show-report="<?php echo ($totalQ == $q) ? 'true' : 'false'; ?>" data-target="<?php echo $q + 1 ?>" data-cur="<?php echo $q ?>" data-dismiss="modal">&times;</button>
												<h3 class="modal-title" id="exampleModalLabel">FEEDBACK</h3>
											</div>
											<div class="modal-body">
												<div class="row feedtextarea">
													<div class="row feed">
														<div class="flexfeed">
															<div class="flexfeed img-videobox">
																<?php if ( ! empty($swip_feed_data['feed_audio']) && file_exists($root_path . $swip_feed_data['feed_audio'])): ?>
																<audio id="fvideo-1" class="video-js vjs-theme-<?php echo $vtheme ?>" controls preload="auto" width="300px" height="100px" data-setup="{}" data-type="audio" poster="<?php echo $aPoster ?>">
																	<source src="<?php echo $path . $swip_feed_data['feed_audio'] ?>" type="audio/<?php echo strtolower(pathinfo($swip_feed_data['feed_audio'], PATHINFO_EXTENSION)); ?>" />
																</audio>
																<?php elseif ( ! empty($swip_feed_data['feed_video']) && file_exists($root_path . $swip_feed_data['feed_video'])): ?>
																<video id="fvideo-1" class="video-js vjs-theme-<?php echo $vtheme ?>" controls preload="auto" width="640" height="264" data-setup="{}" data-type="video">
																	<source src="<?php echo $path . $swip_feed_data['feed_video'] ?>" type="video/<?php echo strtolower(pathinfo($swip_feed_data['feed_video'], PATHINFO_EXTENSION)); ?>" />
																</video>
																<?php elseif ( ! empty($swip_feed_data['feed_screen']) && file_exists($root_path . $swip_feed_data['feed_screen'])): ?>
																<video id="fvideo-1" class="video-js vjs-theme-<?php echo $vtheme ?>" controls preload="auto" width="640" height="264" data-setup="{}" data-type="video">
																	<source src="<?php echo $path . $swip_feed_data['feed_screen'] ?>" type="video/<?php echo strtolower(pathinfo($swip_feed_data['feed_screen'], PATHINFO_EXTENSION)); ?>" />
																</video>
																<?php elseif ( ! empty($swip_feed_data['feed_image']) && file_exists($root_path . $swip_feed_data['feed_image'])): ?>
																<a href="<?php echo $path . $swip_feed_data['feed_image']; ?>" data-fancybox data-caption="<?php echo stripslashes($swip_feed_data['feedback']); ?>">
																	<img class="img-fluid" src="<?php echo $path . $swip_feed_data['feed_image']; ?>" />
																</a>
																<?php elseif ( ! empty($swip_feed_data['feed_document']) && file_exists($root_path . $swip_feed_data['feed_document'])): ?>
																<a href="javascript:;" data-src="<?php echo $path . $swip_feed_data['feed_document']; ?>" data-fancybox data-type="iframe" data-caption="<?php echo stripslashes($swip_feed_data['feedback']); ?>" class="document">
																	<img class="img-fluid pdfclissicsIm" src="<?php echo $db->getBaseUrl('img/icons/pdf_icon.svg'); ?>">
																</a>
																<?php elseif ( ! empty($swip_feed_data['feed_speech_text'])): ?>
																	<button onclick="speak_feedback('div.feedback_intro_<?php echo $swip ?>')"><i id="playicon_feedback" class="fa fa-play playicon" aria-hidden="true"></i><i id="pauseicon_feedback" class="fa fa-pause pauseicon" style="display:none" aria-hidden="true"></i></button>
																	<div class="feedback_intro_<?php echo $swip ?>" style="display:none;"><?php echo $swip_feed_data['feed_speech_text'] ?></div>
																<?php endif; ?>
															</div>
															<div class="feedbacktext"><?php echo ( ! empty($swip_feed_data['feedback'])) ? stripslashes($swip_feed_data['feedback']) : ''; ?></div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<?php $swip++; endforeach; endif; ?>
								<div class="submitMTFcenter col-sm-12">
									<button type="button" id="btn_<?php echo $q; ?>" class="btn submitbtn fadeIn" data-cur="<?php echo $q ?>" data-show-report="<?php echo ($totalQ == $q) ? 'true' : 'false'; ?>">SUBMIT</button>
								</div>
							</div>
						</div>
					</div>
				</div>
				<?php #------D&D------
				elseif ($qtype == 8): ?>
				<div class="Drag_Drop imgDNDS" id="option_<?php echo $q; ?>" data-qtype="<?php echo $qtype ?>">
					<div class="row">
						<div class="flex33 DCbanner DCbannerClassic fade-in">
							<?php $dnd_choice_sql = "SELECT question_id, sub_option, image FROM sub_options_tbl WHERE question_id = '". $qrow['question_id'] ."'";
							$dnd_choice = $db->prepare($dnd_choice_sql); $dnd_choice->execute();
							if ($dnd_choice->rowCount() > 0):
							$dnd_choice_data = $dnd_choice->fetchAll(PDO::FETCH_ASSOC);
							foreach ($dnd_choice_data as $dnd_choice_row): ?>
							<div class="flexheight33 multiDBDbanner option_brand">
								<div class="multiDBDbox">
								<?php if ( ! empty($dnd_choice_row['image']) && file_exists($root_path . $dnd_choice_row['image'])): ?>
								<a href="<?php echo $path . $dnd_choice_row['image']; ?>" data-fancybox data-caption="<?php echo stripslashes($dnd_choice_row['sub_option']); ?>" class="qassets_icon"><img src="<?php echo $path . $dnd_choice_row['image']; ?>" /></a>
								<?php endif; ?>
								<div class="textwraping <?php echo (empty($dnd_choice_row['image'])) ? 'dd_twrap' : ''; ?>"><?php echo stripslashes($dnd_choice_row['sub_option']); ?></div>
								</div>
							</div>
							<?php endforeach; endif; ?>
							<div class="flexheight33 multiDBDbanner BGMUltTrans">
								<?php $dndi = 1; foreach ($dnd_choice_data as $data): ?>
								<div class="dragBox drop option_brand dragBox<?php echo $dndi.'_'.$q ?>"></div>
								<?php $dndi++; endforeach; ?>
							</div>
						</div>
						<div class="flex33 DROPclassicbanner DROPclassicA fade-in">
							<?php if (count($ansData) > 0): foreach ($ansData as $dnd_match_row):
							$dd_feedback[] = $db->getFeedback($question_id, $dnd_match_row['answer_id']); ?>
							<div class="multiDBDbox dragdrop option_brand" data-aid="<?php echo $dnd_match_row['answer_id'] ?>"  data-com1="<?php echo $dnd_match_row['ans_val1'] ?>" data-com2="<?php echo $dnd_match_row['ans_val2'] ?>" data-com3="<?php echo $dnd_match_row['ans_val3'] ?>" data-com4="<?php echo $dnd_match_row['ans_val4'] ?>" data-com5="<?php echo $dnd_match_row['ans_val5'] ?>" data-com6="<?php echo $dnd_match_row['ans_val6'] ?>">
								<?php if ( ! empty($dnd_match_row['image']) && file_exists($root_path . $dnd_match_row['image'])): ?>
								<a href="<?php echo $path . $dnd_match_row['image']; ?>" data-fancybox data-caption="<?php echo stripslashes($dnd_match_row['choice_option']); ?>" class="qassets_icon"><img src="<?php echo $path . $dnd_match_row['image']; ?>" /></a>
								<?php endif; ?>
								<div class="textwraping"><?php echo stripslashes($dnd_match_row['choice_option']); ?></div>
							</div>
							<?php endforeach; endif; ?>
						</div>
						<!--DD-FEEDBACK-->
						<?php if ( ! empty($dd_feedback)): $dd = 1; foreach ($dd_feedback as $dd_feed_data): ?>
						<div class="modal feedback_box fade" role="dialog" id="Feedback_<?php echo $dd_feed_data['answer_id'] ?>" data-keyboard="false" data-backdrop="static">
							<div class="modal-dialog modal-dialog-centered">
								<div class="modal-content">
									<div class="modal-header feedback-header">
										<button type="button" class="feedclose" data-show-report="<?php echo ($totalQ == $q) ? 'true' : 'false'; ?>" data-target="<?php echo $q + 1 ?>" data-cur="<?php echo $q ?>" data-dismiss="modal">&times;</button>
										<h3 class="modal-title" id="exampleModalLabel">FEEDBACK</h3>
									</div>
									<div class="modal-body">
										<div class="row feedtextarea">
											<div class="row feed">
												<div class="flexfeed">
													<div class="flexfeed img-videobox">
														<?php if ( ! empty($dd_feed_data['feed_audio']) && file_exists($root_path . $dd_feed_data['feed_audio'])): ?>
														<audio id="fvideo-1" class="video-js vjs-theme-<?php echo $vtheme ?>" controls preload="auto" width="300px" height="100px" data-setup="{}" data-type="audio" poster="<?php echo $aPoster ?>">
															<source src="<?php echo $path . $dd_feed_data['feed_audio'] ?>" type="audio/<?php echo strtolower(pathinfo($dd_feed_data['feed_audio'], PATHINFO_EXTENSION)); ?>" />
														</audio>
														<?php elseif ( ! empty($dd_feed_data['feed_video']) && file_exists($root_path . $dd_feed_data['feed_video'])): ?>
														<video id="fvideo-1" class="video-js vjs-theme-<?php echo $vtheme ?>" controls preload="auto" width="640" height="264" data-setup="{}" data-type="video">
															<source src="<?php echo $path . $dd_feed_data['feed_video'] ?>" type="video/<?php echo strtolower(pathinfo($dd_feed_data['feed_video'], PATHINFO_EXTENSION)); ?>" />
														</video>
														<?php elseif ( ! empty($dd_feed_data['feed_screen']) && file_exists($root_path . $dd_feed_data['feed_screen'])): ?>
														<video id="fvideo-1" class="video-js vjs-theme-<?php echo $vtheme ?>" controls preload="auto" width="640" height="264" data-setup="{}" data-type="video">
															<source src="<?php echo $path . $dd_feed_data['feed_screen'] ?>" type="video/<?php echo strtolower(pathinfo($dd_feed_data['feed_screen'], PATHINFO_EXTENSION)); ?>" />
														</video>
														<?php elseif ( ! empty($dd_feed_data['feed_image']) && file_exists($root_path . $dd_feed_data['feed_image'])): ?>
														<a href="<?php echo $path . $dd_feed_data['feed_image']; ?>" data-fancybox data-caption="<?php echo stripslashes($dd_feed_data['feedback']); ?>">
															<img class="img-fluid" src="<?php echo $path . $dd_feed_data['feed_image']; ?>" />
														</a>
														<?php elseif ( ! empty($dd_feed_data['feed_document']) && file_exists($root_path . $dd_feed_data['feed_document'])): ?>
														<a href="javascript:;" data-src="<?php echo $path . $dd_feed_data['feed_document']; ?>" data-fancybox data-type="iframe" data-caption="<?php echo stripslashes($dd_feed_data['feedback']); ?>" class="document">
															<img class="img-fluid pdfclissicsIm" src="<?php echo $db->getBaseUrl('img/icons/pdf_icon.svg'); ?>">
														</a>
														<?php elseif ( ! empty($dd_feed_data['feed_speech_text'])): ?>
															<button onclick="speak_feedback('div.feedback_intro_<?php echo $dd ?>')"><i id="playicon_feedback" class="fa fa-play playicon" aria-hidden="true"></i><i id="pauseicon_feedback" class="fa fa-pause pauseicon" style="display:none" aria-hidden="true"></i></button>
															<div class="feedback_intro_<?php echo $dd ?>" style="display:none;"><?php echo $dd_feed_data['feed_speech_text'] ?></div>
														<?php endif; ?>
													</div>
													<div class="feedbacktext"><?php echo ( ! empty($dd_feed_data['feedback'])) ? stripslashes($dd_feed_data['feedback']) : ''; ?></div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<?php $dd++; endforeach; endif; ?>
						<div class="submitMTFcenter col-sm-12">
							<button type="button" id="btn_<?php echo $q; ?>" class="btn submitbtn fade-in" data-cur="<?php echo $q ?>" data-show-report="<?php echo ($totalQ == $q) ? 'true' : 'false'; ?>">SUBMIT</button>
						</div>
					</div>
				</div>
				<?php endif; ?>
			</div>
		</div>
		<?php $q++; endforeach; endif; ?>
	</div>
	<?php if ( ! empty($right_char)): ?>
	<div class="CharFlextwo right">
		<img class="img-fulid qusTempimg" src="<?php echo $db->getBaseUrl($right_char); ?>">
	</div>
	<?php endif; ?>
	<div class="bttn_form scorm_publishs">
    	<form method="post">
        	<input type="hidden" name="sim_name" value="<?php echo str_replace(' ', '-', trim($title)); ?>" />
            <?php if ($type == 'zip'): ?>
        	<input type="submit" class="btn btn-primary" name="web_obj_publish" id="web_obj_publish" value="Publish as Web Object" />
            <?php elseif ($type == 'scrom'): ?>
            <input type="submit" class="btn btn-primary" name="scorm_publish" id="scorm_publish" value="SCORM Publish" />
            <?php endif; ?>
        </form>
    </div>
</div>

<div id="load_popup_modal_show" class="modal fade" tabindex="-1" data-keyboard="false" data-backdrop="static"></div>

<script src="<?php echo $db->getBaseUrl('content/js/moreless.js'); ?>"></script>
<script src="<?php echo $db->getBaseUrl('content/js/articulate.js'); ?>"></script>
<script src="<?php echo $db->getBaseUrl('content/js/owlcarousel/owl.carousel.min.js'); ?>"></script>
<script src="<?php echo $db->getBaseUrl('content/js/sim-branding.js'); ?>"></script>
<script type="text/javascript">
	$('.CharFlex.CharFlextwo').insertBefore('.Questmtf');

	$('.CharFlextwo.right').insertAfter('.Questmtf');
	
	$('a.qassets_icon').fancybox({
		'titleShow': true,
		'transitionIn': 'elastic',
		'transitionOut': 'elastic',
		'easingIn': 'easeOutBack',
		'easingOut': 'easeInBack'
	});

	$('.document').fancybox({
		openEffect: 'elastic',
		closeEffect: 'elastic',
		autoSize: true,
		type: 'iframe',
		iframe: { preload: false }
	});

	$('body').on('click', '.view_scenario', function(e){
		e.preventDefault();
		var to = $(this).attr('data-src');
		var iframe = $.fancybox.open({
			type: 'iframe',
			src: to,
			toolbar  : false,
			smallBtn : true,
			closeExisting: false,
			scrolling: 'no',
			iframe : {
				preload : true,
				scrolling: 'no',
				css:{
					width:'100%',
					height:'100%'
				}
			},
			afterLoad:function(){
				$('.fancybox-iframe').contents().find('.footergray').hide();
			}
		});
	});

	var owl = $('.owl-carousel');
	owl.owlCarousel({
		margin: 10,
		loop: false,
		center: true,
		responsive: {
			0: { items: 1 },
			600: { items: 2 },
			1000: { items: 4 }
		}
	});

	function speak(obj) {
		var speaking = $().articulate('isSpeaking');
		var paused = $().articulate('isPaused');

		// This is how you can use one button for a speak/pause toggle
		// Is browser speaking? (This returns 'true' even when paused)
		// If it is, is speaking paused? If paused, then resume; otherwise pause
		// If it isn't, then initiate speaking

		if (speaking) {
			if (paused) {
				$().articulate('resume');
			} else {
				$().articulate('pause');
			}
		} else {
			$(obj).articulate('speak');
		};

		var playicon = document.getElementById("playicon");
		var pauseicon = document.getElementById("pauseicon");

		if (playicon.style.display === 'none') {
			playicon.style.display = "block";
			pauseicon.style.display = "none";
		} else {
			playicon.style.display = "none";
			pauseicon.style.display = "block";
		}
	};

	function speak_feedback(obj) {
		var speaking = $().articulate('isSpeaking');
		var paused = $().articulate('isPaused');

		// This is how you can use one button for a speak/pause toggle
		// Is browser speaking? (This returns 'true' even when paused)
		// If it is, is speaking paused? If paused, then resume; otherwise pause
		// If it isn't, then initiate speaking

		if (speaking) {
			if (paused) {
				$().articulate('resume');
			} else {
				$().articulate('pause');
			}
		} else {
			$(obj).articulate('speak');
		};

		var playicon_feedback = document.getElementById("playicon_feedback");
		var pauseicon_feedback = document.getElementById("pauseicon_feedback");

		if (playicon_feedback.style.display === 'none') {
			playicon_feedback.style.display = "block";
			pauseicon_feedback.style.display = "none";
		} else {
			playicon_feedback.style.display = "none";
			pauseicon_feedback.style.display = "block";
		}
	};

	$('.options').click(function() {
		var aid = $(this).data('aid');
		var bid = $(this).data('box-id');
		$('#btn_'+ bid).attr('data-aid', aid);
		$('.options').removeClass('press');
		$(this).addClass('press');
		$('.btn').removeAttr('disabled', true);
	});
	
	//-----DRAG-AND-DROP----
	$(function() {
		//draggable
        $('.dragdrop').draggable({
          revert: true,
          placeholder: true,
          droptarget: '.drop',
          drop: function(evt, droptarget) {
			if ($(droptarget).find('.dragdrop').length == 0){
				//== empty drop target, simply drop
				$(this).appendTo(droptarget).draggable();
				$(".flexheight33.multiDBDbanner.BGMUltTrans").addClass("BGMUltTrans1");
			}
			else {
				//== swapping drops
				var d1 = this;
				var d2 = $(droptarget).find('.dragdrop').eq(0);
				$(d2).appendTo($(d1).parent()).draggable();
				$(d1).appendTo(droptarget).draggable();
			}
          }
        });
	});
	
	var total_com_score = <?php echo $totalCompval ?>;

	var total_score = 0; var get_score1 = 0; var get_score2 = 0; var get_score3 = 0; var get_score4 = 0; var get_score5 = 0; var get_score6 = 0;
	//-----------Feedback-----------
	$('body').on('click', '.submitbtn', function(){
		var cur		= $(this);
		var id  	= cur.data('cur');
		var option	= $('#option_' + id);
		var qtype 	= option.data('qtype');
		var report	= cur.data('show-report');

		$.LoadingOverlay("show");		
		/* MCQ */
		if (qtype == 4) {
			var mcqdata = $('#option_'+ id +' .press');
			var aid 	= mcqdata.data('aid');
			var score1	= mcqdata.data('com1');
			var score2	= mcqdata.data('com2');
			var score3	= mcqdata.data('com3');
			var score4	= mcqdata.data('com4');
			var score5	= mcqdata.data('com4');
			var score6	= mcqdata.data('com5');
		}
		/* SWIPING */
		else if (qtype == 7) {
			var swip = $('#option_'+ id +' .owl-stage .center .swipeoptions');
			var aid	 = swip.data('aid');
			var score1	= swip.data('com1');
			var score2	= swip.data('com2');
			var score3	= swip.data('com3');
			var score4	= swip.data('com4');
			var score5	= swip.data('com4');
			var score6	= swip.data('com5');
		}
		/* D&D */
		else if (qtype == 8) {
			var dd_data	= $('#option_'+ id +' .dragBox');
			var aid 	= dd_data.children('div').data('aid');
			var score1	= dd_data.children('div').data('com1');
			var score2	= dd_data.children('div').data('com2');
			var score3	= dd_data.children('div').data('com3');
			var score4	= dd_data.children('div').data('com4');
			var score5	= dd_data.children('div').data('com4');
			var score6	= dd_data.children('div').data('com5');
		}

		get_score1 += parseInt(score1);
		get_score2 += parseInt(score2);
		get_score3 += parseInt(score3);
		get_score4 += parseInt(score4);
		get_score5 += parseInt(score5);
		get_score6 += parseInt(score6);

		total_score = get_score1 + get_score2 + get_score3 + get_score4 + get_score5 + get_score6;
	
	<?php if ($type == 'scrom'): ?> 
		if (report && total_score == total_com_score) {
			init(total_score, 1);
		}
		else if (report && total_score < total_com_score) {
			init(total_score, 3);
		}
		else {
			init(total_score, 2);
		}
	<?php endif; ?>
	
	var $modal = $('#Feedback_'+ aid);
		$.LoadingOverlay("hide");
		$modal.modal('show', { backdrop:'static', keyboard:false });
	});

	$('.feedclose').on('click', function(){
		var curData	= $(this);
		var cur  	= curData.data('cur');
		var next 	= curData.data('target');
		var report	= curData.data('show-report');
		if (report) {
			timer.trigger('click');
			swal({title: "Good job!", text: "You've attempted all questions.", buttons: false, icon: "success", closeOnClickOutside: false,  closeOnEsc: false});
		}
		else {
			$('.MatchingQusBanner_'+ cur).fadeOut();
			$('.MatchingQusBanner_'+ next).fadeIn();
		}
	});

	document.onreadystatechange = function () {
	if (document.readyState == "complete") {
		$.LoadingOverlay("hide");
	}
};
</script>
<?php 
require_once 'includes/sim-page.php';
require_once 'includes/scorm-zip.php';
require_once 'includes/zip.php';
