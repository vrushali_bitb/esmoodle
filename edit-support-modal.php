<?php 
session_start();
if (isset($_SESSION['username'])):
	$user	= $_SESSION['username'];
    $role   = $_SESSION['role'];
    $userid = $_SESSION['userId'];
else:
	die;
endif;
require_once 'config/db.class.php';
$db   = new DBConnection;
$sid  = (isset($_POST['data-id'])) ? $_POST['data-id'] : '';
$data = $db->GetSupport($sid); ?>
<div id="load_popup_modal_contant" class="" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h3 class="modal-title">Update Status</h3>
      </div>
      <div class="modal-body">
        <form enctype="multipart/form" name="edit_Simulation_form" id="edit_Simulation_form" method="post">
          <div class="usermanage-form">
            <div class="row">
              <div class="col-sm-12">
                <div class="form-group form-select">
                  <select class="form-control" name="supportStatus" required="required" data-placeholder="Status *">
                    <option value="0" selected>Select Status *</option>
                    <?php foreach ($db->SupportStatus() as $k => $v): ?>
                    <option value="<?php echo $k ?>" <?php echo ($k == $data['status']) ? 'selected="selected"' : ''; ?>><?php echo $v ?></option>
                    <?php endforeach; ?>
                  </select>
                </div>
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <input type="hidden" name="update_Support_Status" value="1" />
            <input type="hidden" name="support_id" value="<?php echo $data['support_id']; ?>" />
            <button type="submit" class="btn btn-primary">Update</button>
            <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
$("#edit_Simulation_form").on('submit', (function(e) {
	e.preventDefault();
	var form_data = $(this).serialize();
	var post_url  = 'includes/support-process.php';
	$.LoadingOverlay("show");
	$.ajax({
			url: post_url,
			type: "POST",
			data: form_data,
			success: function(result) {
				var res = $.parseJSON(result);
				$.LoadingOverlay("hide");
				if (res.success == true) {
					swal({text: res.msg, buttons: false, icon: "success", timer: 1000});
					setTimeout(function() { window.location.reload(); }, 2000);
				}
				else if (res.success == false) {
					swal({text: res.msg, buttons: false, icon: "error", timer: 1000});
					setTimeout(function() { window.location.reload(); }, 2000);
				}
			},error: function() {
				swal({text: 'Oops, something went wrong. Please try again later', buttons: false, icon: "error", timer: 1000});
			}
		});
	}));
</script>
<?php 
ob_end_flush();
