<?php 
session_start();
if(isset($_SESSION['username'])) {
    $user 	= $_SESSION['username'];
	$role 	= $_SESSION['role'];
	$userid	= $_SESSION['userId'];
}
else {
    header('location: index.php');
}
require_once 'config/db.class.php';
require_once 'includes/learners_menu.php';
$obj	= new DBConnection;
$uid	= (isset($_GET['uid'])) ? $_GET['uid'] : die('Error');
$data	= $obj->getUsers($uid); ?>
<style type="text/css">
.password-strength {
    box-shadow: 0 0px 0px 0 transparent;
}
.js-hidden {
    display: none;
}
.col-sm-6.first-side {
    padding-left: 0;
    padding-right: 20px;
}
.col-sm-6.second-side {
    padding-right: 0;
    padding-left: 20px;
}
.usermanage-form {
    background: transparent;
    padding: 0px 0px 0px;
}
.usermanage-form .form-control {
    height: 35px;
}
.input-group {
    width: 100%;
}
.input-group-append button {
    margin-top: 0px;
}
.tooltip{
		position: absolute;
	}
</style>
<div class="User-edit_profile">
    <div class="container-fluid">
        <div class="edit_info-banner EdiTUser">
            <div class="row">
              <div class="col-sm-12">
                <form enctype="multipart/form" name="profile_form" id="profile_form" method="post" class="smart-form client-form password-strength">
                    <h4 class="row edit_info_label">EDIT INFORMATION</h4>
                    <div class="row">
                        <div class="col-sm-6 first-side">
                            <div style="font-weight: bold; font-size: 18px; margin-bottom: 15px; margin-top: 15px;">
                                ACCOUNT INFORMATION</div>
                            <div class="edit-field">
                                <input type="text" class="form-control edit-info-entrybox" name="fname"
                                    placeholder="First name" value="<?php echo $data['fname'] ?>">
                            </div>
                            <div class="edit-field">
                                <input type="text" class="form-control edit-info-entrybox" name="lname"
                                    placeholder="Last name" value="<?php echo $data['lname'] ?>">
                            </div>
                            <div class="edit-field">
                                <input type="text" class="form-control edit-info-entrybox" name="username"
                                    value="<?php echo $data['username'] ?>" readonly="readonly">
                            </div>
                            <div class="edit-field">
                                <?php 
                                $roles  = $db->role;
                                $cadmin = FALSE;
                                if (array_key_exists('clientAdmin', $roles) && $data['role'] != 'clientAdmin') {
                                    unset($roles['clientAdmin']);
                                    $cadmin = TRUE;
                                } ?>
                                <select class="form-control" name="edit_role" <?php echo ($data['role'] == 'clientAdmin') ? 'disabled' : ''; ?>>
                                    <option selected="selected" disabled="disabled" hidden value="">Select Role</option>
                                    <?php foreach ($roles as $roleKey => $roleVal): ?>
                                    <option value="<?php echo $roleKey; ?>"
                                        <?php echo ($roleKey == $data['role']) ? 'selected' : ''; ?>>
                                        <?php echo $roleVal; ?></option>
                                    <?php endforeach; ?>
                                </select>
                                <?php if ($data['role'] == 'clientAdmin'): ?>
                                <input type="hidden" name="edit_role" value="clientAdmin" />
                                <?php endif; ?>
                            </div>
                            <div class="edit-field">
                                <input type="email" class="form-control edit-info-entrybox" name="email" id="email"
                                    placeholder="EMAIL GOES HERE" value="<?php echo $data['email'] ?>"
                                    required="required" />
                            </div>
                            <div class="edit-field">
                                <input type="tel" class="form-control edit-info-entrybox" name="mob" id="mob" max="10"
                                    onkeypress="return isNumberKey(event);" value="<?php echo $data['mob'] ?>">
                            </div>
                            <div class="edit-field">
                                <input class="form-control edit-info-entrybox" type="text" placeholder="Date of Join"
                                    value="<?php echo ($data['date'] != '0000-00-00') ? $obj->dateFormat($data['date'], 'd-m-Y') : '' ?>">
                            </div>
                            <div class="edit-field">
                                <select class="form-control" id="status" name="status" required>
                                    <option value="1" <?php echo ($data['status'] == 1) ? 'selected' : ''; ?>>
                                        Active</option>
                                    <option value="0" <?php echo (empty($data['status'])) ? 'selected' : ''; ?>>
                                        Inactive</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-6 second-side">
                            <div style="font-weight: bold; font-size: 18px; margin-bottom: 15px; margin-top: 15px;">PERSONAL INFORMATION</div>
                            <div class="row">
                                <div class="edit-field">
                                    <input class="form-control edit-info-entrybox" name="location" id="location" type="text" placeholder="LOCATION GOES HERE" value="<?php echo $data['location'] ?>" required="required">
                                </div>
                                <div class="edit-field">
                                    <input class="form-control edit-info-entrybox" name="organization" id="organization" type="text" placeholder="ORGANIZATION NAME GOES HERE" value="<?php echo $data['company'] ?>" required="required">
                                </div>
                                <div class="edit-field">
                                    <input class="form-control edit-info-entrybox" name="department" id="department" type="text" placeholder="DEPARTMENT NAME GOES HERE"  value="<?php echo $data['department'] ?>" required="required">
                                </div>
                                <div style="font-weight: bold; font-size: 18px; margin-bottom: 15px; margin-top: 15px;">GENERATE PASSWORD</div>
                                <div class="usermanage-form">
                                  <div class="form-group">
                                      <label for="password-input">New Password: <span class="d-inline-block" tabindex="0" data-toggle="tooltip" title="Add 8 charachters or more, lowercase letters, uppercase letters, numbers and symbols to make the password really strong!"><i class="fa fa-info-circle" aria-hidden="true"></i></span></label>
                                      <div class="register_password1">
                                          <input class="password-strength__input form-control" type="password" id="npwd" name="npwd" placeholder="Password" autocomplete="off">
                                          <div class="input-group-append">
                                              <button class="password-strength__visibility btn btn-outline-secondary" type="button">
                                              <span class="password-strength__visibility-icon" data-visible="hidden"><i  class="fa fa-eye-slash"></i></span>
                                              <span  class="password-strength__visibility-icon js-hidden" data-visible="visible"><i class="fa fa-eye"></i></span></button>
                                          </div>
                                      </div>
                                      <small class="password-strength__error text-danger js-hidden">This symbol is not allowed!</small>
                                  </div>
                                  <div class="password-strength__bar-block progress1 mb-4">
                                      <div class="password-strength__bar progress-bar bg-danger" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
                                  </div>
                                </div>
                                <div class="form-group">
                                    <label for="password-input">Confirm password: <span class="d-inline-block" tabindex="0" data-toggle="tooltip"  title="Add 8 charachters or more, lowercase letters, uppercase letters, numbers and symbols to make the password really strong!"><i class="fa fa-info-circle"  aria-hidden="true"></i></span></label>
                                    <div class="input-group" id="show_hide_password1">
                                        <input type="password" class="form-control" id="rpwd" name="rpwd" autocomplete="off" />
                                        <div class="input-group-addon CoNfpass"> <a href="javascript:void(0);"><i class="fa fa-eye-slash" aria-hidden="true"></i></a></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <input type="hidden" name="update_user_id" value="<?php echo $data['id']; ?>">
                        <input type="hidden" name="update_user_profile" value="1" />
                        <div class="EdiTGroUPmodel"><button type="submit" class="edit_info-update btn btn-primary">Update and Close</button></div>
                    </div>
                </form>
               </div>
            </div>
        </div>
    </div>
</div>
<script src="content/js/pwd-script.js"></script>
<script type="text/javascript" src="content/js/bootstrap-datetimepicker.js" charset="UTF-8"></script>
<script type="text/javascript">
function isNumberKey(evt) {
    var charCode = (evt.which) ? evt.which : evt.keyCode
    return !(charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57));
}
$(function() {
    $('[data-toggle="tooltip"]').tooltip();
})
$('.date').datetimepicker({
    weekStart: 1,
    todayBtn: 0,
    autoclose: 1,
    todayHighlight: 1,
    startView: 2,
    forceParse: 0,
    minView: 2,
    showMeridian: 1,
    clearBtn: 1
});
</script>
<?php 
require_once 'includes/footer.php';