<?php
session_start();
if (isset($_SESSION['username'])) :
  $user   = $_SESSION['username'];
  $role   = $_SESSION['role'];
  $userid  = $_SESSION['userId'];
  $domain = (isset($_SESSION['domain_name'])) ? $_SESSION['domain_name'] : $_SERVER['HTTP_HOST'];
else :
  header('location: index.php');
endif;
require_once 'config/db.class.php';
$sim_id   = (isset($_POST['data-id'])) ? $_POST['data-id'] : exit('error');
$db       = new DBConnection();
$simData  = $db->getScenario($sim_id);
$simPage  = $db->getSimPage($simData['scenario_id']);
$path     = 'scenario/upload/'. $domain .'/'; ?>
<style type="text/css">
  .nav-tabs { border-bottom: none; }
  .owl-item { text-align: center; }
  .owl-item li { list-style: none; }
  .owl-stage { margin: 0 auto; }
  .owl-theme .owl-nav { margin-top: 0; }
  .owl-carousel .owl-item { padding: 10px; }
  button.owl-prev {
    position: absolute;
    left: -80px;
    top: 37%;
    transform: translateY(-50%);
    background-color: #049805 !important;
    width: 20px;
    height: 20px;
    border-radius: 100% !important;
    color: #fff !important;
  }
  button.owl-next {
    position: absolute;
    right: -80px;
    top: 37%;
    transform: translateY(-50%);
    background-color: #049805 !important;
    width: 20px;
    height: 20px;
    border-radius: 100% !important;
    color: #fff !important;
  }
  .admin-dashboard.Ev2 .nav {
    padding-top: 5px;
    padding: 0 95px;
  }
  .owl-dots { display: none; }
  /*------------------phone-------------*/
  @media (min-width:320px) and (max-width: 767px) {
    button.owl-prev { left: -20px !important; }
    .admin-dashboard.Ev2 .nav { padding: 0 25px !important; }
    button.owl-next { right: -20px !important; }
  }
</style>
<link rel="stylesheet" href="videojs/node_modules/video.js/dist/video-js.min.css" />
<link rel="stylesheet" href="videojs/node_modules/video.js/dist/themes/forest/index.css" />
<script type="text/javascript" src="videojs/node_modules/video.js/dist/video.min.js"></script>
<div id="load_popup_modal_contant" class="" role="dialog">
  <div class="modal-dialog modal-lg ViewSEnArIo">
    <div class="modal-content">
      <div class="modal-header">
        <h2 class="launchhead" id="exampleModalLabel"><?php echo ( ! empty($simData['Scenario_title'])) ? $simData['Scenario_title'] : ''; ?></h2>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      </div>
      <div class="modal-body">
        <div class="text-hint-banner">
          <?php if (empty($simData['video_url']) && ! empty($simData['audio_url']) && file_exists($path . $simData['audio_url'])) : ?>
            <audio id="audio_example" class="video-js vjs-forest" controls preload="auto" poster="scenario/img/audio-poster.jpg" data-setup='{}'>
            <source src="<?php echo $path . $simData['audio_url'] ?>" type="audio/<?php echo strtolower(pathinfo($simData['audio_url'], PATHINFO_EXTENSION)); ?>" />
          </audio>
          <?php elseif ( ! empty($simData['video_url']) && file_exists($path . $simData['video_url'])) : ?>
            <video id="video_example" class="video-js vjs-forest" controls preload="auto" height="300px" data-setup="{}" data-type="video">
              <source src="<?php echo $path . $simData['video_url'] ?>" type="video/<?php echo strtolower(pathinfo($simData['video_url'], PATHINFO_EXTENSION)); ?>" />
            </video>
          <?php elseif ( ! empty($simData['image_url']) && file_exists($path . $simData['image_url'])) : ?>
            <img src="<?php echo $path . $simData['image_url'] ?>" class="img-responsive img-fluid" />
          <?php elseif ( ! empty($simData['web_object_file']) && file_exists($path . $simData['web_object_file'])):
            $wofn = '';
            if (file_exists($path . $simData['web_object_file'] . '/story.html')):
              $wofn = 'story.html';
            elseif (file_exists($path . $simData['web_object_file'] . '/index.html')):
              $wofn = 'index.html';
            elseif (file_exists($path . $simData['web_object_file'] . '/a001index.html')):
                $wofn = 'a001index.html';
            endif; ?>
            <iframe src="<?php echo $path . $simData['web_object_file'] . '/' . $wofn; ?>" width="100%" height="500px"></iframe>
          <?php endif; ?>
        </div>
        <ul class="nav nav-tabs">
          <div class="owl_1 owl-carousel owl-theme">
            <?php if ($simPage) : $l = 0;
              foreach ($simPage as $plink) : ?>
              <div class="item">
                <li <?php echo ($l == 0) ? 'class="active"' : ''; ?>><a data-toggle="tab" href="#<?php echo $plink['sim_page_id'] ?>"><?php echo $plink['sim_page_name'] ?></a></li>
              </div>
            <?php $l++; endforeach; endif; ?>
          </div>
        </ul>
      </div>
      <div class="tab-content">
        <?php if ($simPage) : $p = 0;
          foreach ($simPage as $pdata) : ?>
          <div id="<?php echo $pdata['sim_page_id'] ?>" class="tab-pane fade <?php echo ($p == 0) ? 'in active' : ''; ?>">
            <div class="Lrn-Banner"><?php echo $pdata['sim_page_desc'] ?></div>
          </div>
        <?php $p++; endforeach; endif; ?>
      </div>
    </div>
  </div>
  <script src="content/js/owlcarousel/owl.carousel.min.js"></script>
  <script type="text/javascript">
  $('.owl_1').owlCarousel({
      loop: false,
      margin: 0,
      mouseDrag: false,
      responsiveClass: true,
      autoplayHoverPause: true,
      autoplay: false,
      slideSpeed: 400,
      paginationSpeed: 400,
      autoplayTimeout: 3000,
      responsive: {
        0: {
          items: 2,
          nav: true,
          loop: false
        },
        600: {
          items: 3,
          nav: true,
          loop: false
        },
        1000: {
          items: 6,
          nav: true,
          loop: false
        }
      }
    });
    $(document).ready(function() {
      var li = $(".owl-item li");
      $(".owl-item li").click(function() {
        li.removeClass('active');
      });
    });
  </script>