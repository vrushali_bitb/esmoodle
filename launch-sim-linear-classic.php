<?php 
ob_start();
session_start();
if (isset($_SESSION['username'])):
	$user 	= $_SESSION['username'];
	$role 	= $_SESSION['role'];
	$userid = $_SESSION['userId'];
	$domain = (isset($_SESSION['domain_name'])) ? $_SESSION['domain_name'] : $_SERVER['HTTP_HOST'];
	$client_id = (isset($_SESSION['client_id'])) ? $_SESSION['client_id'] : '';
else:
    header('location: index.php');
endif;
require_once 'config/db.class.php';
require_once 'includes/header.php';
$db 		= new DBConnection();
$sim_id		= (isset($_GET['sim_id'])) ? $_GET['sim_id'] : die('Error');
$ques_id	= (isset($_GET['ques_id'])) ? base64_decode($_GET['ques_id']) : '';
$save		= (isset($_GET['save']) && $_GET['save'] == 'false') ? FALSE : TRUE;
$simData	= $db->getScenario($sim_id);
$simID 		= $simData['scenario_id'];
$brand		= $db->getSimBranding($simID);
$proctor	= $db->checkProctorPermission($client_id);
$uid 		= $db->UID();
if ($save):
	$tAttempt = $db->getTotalAttempt($simID, $userid);
	$attempt  = $db->getAssignSimOtherData($simID, $userid)[0];
	if ( ! empty($attempt['no_attempts']) && $tAttempt >= $attempt['no_attempts']):
		echo "<script>alert('Number of attempts of this simulation is completed.');</script>";
		echo "<script>window.location.href='learner-dashboard.php';</script>";
		exit;
	endif;
endif;
$back_img	= ( ! empty($simData['sim_back_img'])) ? 'img/char_bg/'. $simData['sim_back_img'] : '';
$path		= $db->getBaseUrl('scenario/upload/'.$domain.'/');
$root_path	= $db->rootPath().'scenario/upload/'.$domain.'/';
$aPoster	= $db->getBaseUrl('scenario/img/audio-poster.jpg');
$noImage	= $db->getBaseUrl('scenario/img/palceholder.jpg');
$vtheme		= 'forest'; #city, fantasy, forest, sea
$user_data = ( ! empty($userid)) ? $db->getUsers(md5($userid)) : [];
$profile   = ( ! empty($user_data['img']) && file_exists($db->rootPath() . 'img/profile/'. $user_data['img'])) ? $user_data['img'] : '';
$left_char	= $right_char = '';
if ( ! empty($simData['sim_char_img'])):
	$left_char = 'img/char_bg/'. $simData['sim_char_img'];
elseif ( ! empty($simData['sim_char_left_img'])):
	$left_char = 'img/char_bg/'. $simData['sim_char_left_img'];
	elseif ( ! empty($simData['char_own_choice']) && file_exists($root_path . $simData['char_own_choice'])):
	$left_char = $path . $simData['char_own_choice'];
endif;
if ( ! empty($simData['sim_char_right_img'])):
	$right_char = 'img/char_bg/'. $simData['sim_char_right_img'];
endif;
if ( ! empty($brand['font_type'])):
$type = str_replace('+', ' ', $brand['font_type']); ?>
<style type="text/css">
@import url("https://fonts.googleapis.com/css2?family=<?php echo $brand['font_type'] ?>:wght@100;200;300;400;500;600;700;800;900&display=swap");
body { font-family: '<?php echo $type ?>' !important; }
h1,h2,h3,h4,h5,h6 { font-family: '<?php echo $type ?>' !important; }
p { font-family: '<?php echo $type ?>' !important; }
</style>
<?php endif; ?>
<script type="text/javascript">
	window.addEventListener('beforeunload', function (e) {
	  // Cancel the event
	  e.preventDefault(); // If you prevent default behavior in Mozilla Firefox prompt will always be shown
	  // Chrome requires returnValue to be set
	  e.returnValue = '';
	});
	
	function disableF5(e) { if ((e.which || e.keyCode) == 116 || (e.which || e.keyCode) == 82) e.preventDefault(); };
	
	$(document).ready(function(){
		$(document).on("keydown", disableF5);
	});
	
	$(document).bind("contextmenu", function(e){
		return false;
	});
	
	history.pushState(null, null, location.href);
    window.onpopstate = function() {
        history.go(1);
    };
	
	var simBrand = <?php echo json_encode($brand); ?>

	$.LoadingOverlay("show");
</script>
<style type="text/css">
.Dflex .multiDBDbox {
	height: auto !important;
}
<?php if ( ! empty($back_img)): ?>
.linear_classic {
	background: url("<?php echo $back_img ?>") center top no-repeat;
	background-size: cover;
	overflow:hidden;
	height:100vh;
	display: flex;
}
<?php elseif ( ! empty($simData['bg_color'])): ?>
.linear_classic { background: <?php echo $simData['bg_color']; ?>; display:flex;}
<?php endif; if ( ! empty($brand['option_bg'])): ?>
.questioninput:before { background: <?php echo $brand['option_bg']; ?>; }
.questioninput:after { background: <?php echo $brand['option_bg']; ?>; }
.rectangle1:after { background: <?php echo $brand['option_bg']; ?>; }
<?php endif; ?>
</style>
<script src="videojs/node_modules/recordrtc/RecordRTC.js"></script>
<script src="videojs/node_modules/webrtc-adapter/out/adapter.js"></script>
<script src="videojs/dist/videojs.record.js"></script>
<script src="videojs/dist/plugins/videojs.record.ts-ebml.js"></script>
<script src="videojs/examples/browser-workarounds.js"></script>
<div class="menu-banner-box">
	<div class="time-banner-box">
    	<button class="V_S-tetail" id="view_sim" data-sim-id="<?php echo $sim_id; ?>">
    		<img class="Deskview" src="img/list/view_scenario_b.svg" />
        	<img class="mobview" src="img/list/view_scenario.svg" />
        	View Scenario
    	</button>
  	</div>
    <!---SIM-Timer-->
    <div class="timer-banner">
    	<div class="time-text">
        	<span class="timer-pause" data-minutes-left="<?php echo $simData['duration'] ?>" style="pointer-events: none;"></span>
    	</div>
  	</div>
	<?php if ($save): ?>
    <div class="close-banner">
    	<img class="svg" src="img/list/close_learner.svg" />
    </div>
	<?php endif; ?>
</div>
<script type="text/javascript">
var timer = $('.timer-pause').startTimer({
	onComplete: function(element){
		<?php if ($save): ?>
		$('.signbtn1').show();
		<?php else: ?>
		swal({text: "Simulation time end. please relaunch.!", icon: "info"});
		<?php endif; ?>
	},
	allowPause: true
});
</script>
<div class="linear_classic">
	<input type="hidden" name="uid" id="uid" value="<?php echo $uid; ?>" />
	<input type="hidden" name="simId" id="simId" value="<?php echo $simID; ?>" />
	<input type="hidden" name="userid" id="userid" value="<?php echo $userid; ?>" />
	<input type="hidden" name="save" id="save" value="<?php echo $save; ?>" />
	<input type="hidden" name="apiurl" id="apiurl" value="<?php echo $db->getBaseUrl('api-process.php') ?>" />
	<input type="hidden" name="vfile_name" id="vfile_name" value="<?php echo $userid .'_'. $uid; ?>" />
	<input type="hidden" name="base_path" id="base_path" value="<?php echo $path; ?>" />
	<input type="hidden" name="profile" id="profile" value="<?php echo ( ! empty($profile)) ? $db->getBaseUrl('img/profile/'. $profile) : $db->getBaseUrl($path . 'proctor/'. $userid .'.png'); ?>" />
	<?php if ( ! empty($left_char)): ?>
	<div class="CharFlex <?php echo ( ! empty($right_char)) ? 'CharFlextwo' : ''; ?>"><img class="img-fulid qusTempimg" src="<?php echo $left_char; ?>" /></div>
	<?php endif; ?>
	<div class="flexQTeMP <?php echo ( ! empty($right_char)) ? 'flexQTeMPtwochar' : ''; ?><?php echo ( ! empty($left_char) && empty($right_char)) ? 'flexQTeMPonechar' : ''; ?><?php echo (empty($left_char) && empty($right_char)) ? 'flexQTeMPnochar' : ''; ?>">
	<?php 
	$qSql = $db->prepare("SELECT * FROM question_tbl WHERE scenario_id = '". $simID ."' ORDER BY qorder = 0, qorder"); $qSql->execute();
	$totalQ = $qSql->rowCount();
	if ($totalQ > 0):$q = 1;
		$qdata = $qSql->fetchAll(PDO::FETCH_ASSOC);
		foreach ($qdata as $qrow):
		$shuffle = ( ! empty($qrow['shuffle'])) ? TRUE : FALSE;
		$ansData = $db->getAnswersByQuestionId(md5($qrow['question_id']), $shuffle);
		$qtype	 = $qrow['question_type'];
		if ( ! empty($ques_id) && $ques_id == $qrow['question_id']):
			$show = 'style="display:block"';
		elseif (empty($ques_id) && $q == 1):
			$show = 'style="display:block"';
		else:
			$show = 'style="display:none"';
		endif; ?>
		<div class="MatchingQusBannerlunch MatchingQusBanner_<?php echo $q; ?> multimatchingB" <?php echo $show; ?>>
			<div class="Matchingcontainer">
				<div class="Questmtf">
					<div class="MatchingQusText fade-in">
						<div class="qus-text">
							<h3>QUESTION <?php echo $q .'/'. $totalQ; ?></h3>
							<p class="more"><?php echo stripslashes($qrow['questions']); ?></p>
						</div>
						<div class="qusassestbox">
							<div class="Qusimg">
							<?php if ( ! empty($qrow['audio']) && file_exists($root_path . $qrow['audio'])): ?>
							<audio id="qvideo-<?php echo $q ?>" class="video-js vjs-theme-<?php echo $vtheme ?>" controls preload="auto" width="300px" height="100px" data-setup="{}" data-type="audio" poster="<?php echo $aPoster ?>">
								<source src="<?php echo $path . $qrow['audio'] ?>" type="audio/<?php echo strtolower(pathinfo($qrow['audio'], PATHINFO_EXTENSION)); ?>" />
							</audio>
							<?php elseif ( ! empty($qrow['video']) && file_exists($root_path . $qrow['video'])): ?>
							<a href="<?php echo $path . $qrow['video']; ?>" data-fancybox data-caption="<?php echo stripslashes($qrow['questions']); ?>" class="qassets_icon">
								<video id="qvideo-<?php echo $q ?>" class="video-js vjs-theme-<?php echo $vtheme ?>" controls preload="auto" width="640" height="264" data-setup="{}" data-type="video">
									<source src="<?php echo $path . $qrow['video']; ?>" type="video/<?php echo strtolower(pathinfo($qrow['video'], PATHINFO_EXTENSION)); ?>" />
								</video>
							</a>
							<?php elseif ( ! empty($qrow['screen']) && file_exists($root_path . $qrow['screen'])): ?>
							<a href="<?php echo $path . $qrow['screen']; ?>" data-fancybox data-caption="<?php echo stripslashes($qrow['questions']); ?>" class="qassets_icon">
								<video id="qvideo-<?php echo $q ?>" class="video-js vjs-theme-<?php echo $vtheme ?>" controls preload="auto" width="200" height="100" data-setup="{}" data-type="video">
									<source src="<?php echo $path . $qrow['screen']; ?>" type="video/<?php echo strtolower(pathinfo($qrow['screen'], PATHINFO_EXTENSION)); ?>" />
								</video>
							</a>
							<?php elseif ( ! empty($qrow['image']) && file_exists($root_path . $qrow['image'])): ?>
							<a href="<?php echo $path . $qrow['image']; ?>" data-fancybox data-caption="<?php echo stripslashes($qrow['questions']); ?>" class="qassets_icon">
								<img class="img-fluid" src="<?php echo $path . $qrow['image']; ?>">
							</a>
							<?php elseif ( ! empty($qrow['document']) && file_exists($root_path . $qrow['document'])): ?>
							<a href="javascript:;" data-src="<?php echo $path . $qrow['document']; ?>" data-fancybox data-type="iframe" data-caption="<?php echo stripslashes($qrow['questions']); ?>" class="document">
								<img class="img-fluid pdfclissicsIm" src="img/icons/pdf_icon.svg" />
							</a>
							<?php elseif ( ! empty($qrow['speech_text'])): ?>
							<button onclick="speak('div.intro_<?php echo $q ?>')">
								<i id="playicon" class="fa fa-play playicon" aria-hidden="true"></i>
								<i id="pauseicon" class="fa fa-pause pauseicon" style="display:none" aria-hidden="true"></i></button>
								<div class="intro_<?php echo $q ?>" style="display:none;"><?php echo $qrow['speech_text'] ?></div>
							<?php endif; ?>
							</div>
						</div>
					</div>
				</div>
				<?php #------MCQ------
				if ($qtype == 4): ?>
				<div class="MCQOption bannermcq" id="option_<?php echo $q; ?>" data-qtype="<?php echo $qtype ?>">
				<?php foreach ($ansData as $mcq_data): ?>
					<div class="row options option_brand advertise_container fade-in" data-qtype="<?php echo $qtype ?>" data-qid="<?php echo $mcq_data['question_id'] ?>" data-aid="<?php echo $mcq_data['answer_id'] ?>" data-box-id="<?php echo $q; ?>">						
						<div class="multiQusChecK"><div class="Check-boxQB Check-boxMCQ"><i class="fa fa-circle" aria-hidden="true"></i></div></div>
						<div class="multiQustext"><?php echo stripslashes($mcq_data['choice_option']); ?></div>
					</div>
					<?php endforeach; ?>
					<div class="submitMTFcenter col-sm-12">
						<button type="button" id="btn_<?php echo $q; ?>" class="btn submitbtn fade-in" data-show-report="<?php echo ($totalQ == $q) ? 'true' : 'false'; ?>" data-target="<?php echo $q + 1 ?>" data-cur="<?php echo $q ?>" data-sim-id="<?php echo $sim_id ?>" data-qid="<?php echo $qrow['question_id'] ?>">SUBMIT</button>
					</div>
				</div>
				<?php #------SWIPING------
				elseif ($qtype == 7): ?>
				<div class="MCQOption" id="option_<?php echo $q; ?>" data-qtype="<?php echo $qtype ?>">
					<div class="home-demo">
						<div class="row row CoursalsTYbullet">
							<div class="large-12 columns">
								<div class="owl-carousel fadeIn swipingcoursal">
								<?php foreach ($ansData as $swipe_data): ?>
								<div class="swipeoptions option_brand" data-qtype="<?php echo $qtype ?>" data-qid="<?php echo $swipe_data['question_id'] ?>" data-aid="<?php echo $swipe_data['answer_id'] ?>">
									<div class="multiDBDboxAllIGN">
									<?php $swip_cls = 'dd_twrap'; if ( ! empty($swipe_data['image']) && file_exists($root_path . $swipe_data['image'])): $swip_cls = ''; ?>
										<a href="<?php echo $path . $swipe_data['image']; ?>" data-fancybox data-caption="<?php echo stripslashes($swipe_data['choice_option']); ?>" class="qassets_icon"><img class="feedimage" src="<?php echo $path . $swipe_data['image'] ?>" /></a>
										<?php endif; ?>
										<div class="textwraping <?php echo $swip_cls; ?>"><?php echo stripslashes($swipe_data['choice_option']); ?></div>
									</div>
								</div>
								<?php endforeach; ?>
								</div>
								<div class="submitMTFcenter col-sm-12">
									<button type="button" id="btn_<?php echo $q; ?>" class="btn submitbtn fadeIn" data-show-report="<?php echo ($totalQ == $q) ? 'true' : 'false'; ?>" data-target="<?php echo $q + 1 ?>" data-cur="<?php echo $q ?>" data-sim-id="<?php echo $sim_id ?>" data-qid="<?php echo $qrow['question_id'] ?>">SUBMIT</button>
								</div>
							</div>
						</div>
					</div>
				</div>
				<?php #------D&D------
				elseif ($qtype == 8): ?>
				<div class="Drag_Drop imgDNDS" id="option_<?php echo $q; ?>" data-qtype="<?php echo $qtype ?>">
					<div class="row">
						<div class="flex33 DCbanner DCbannerClassic fade-in">
							<?php $dnd_choice_sql = "SELECT question_id, sub_option, image FROM sub_options_tbl WHERE question_id = '". $qrow['question_id'] ."'";
							$dnd_choice = $db->prepare($dnd_choice_sql); $dnd_choice->execute();
							if ($dnd_choice->rowCount() > 0):
							$dnd_choice_data = $dnd_choice->fetchAll(PDO::FETCH_ASSOC);
							foreach ($dnd_choice_data as $dnd_choice_row): ?>
							<div class="flexheight33 multiDBDbanner option_brand">
								<div class="multiDBDbox">
								<?php if ( ! empty($dnd_choice_row['image']) && file_exists($root_path . $dnd_choice_row['image'])): ?>
								<a href="<?php echo $path . $dnd_choice_row['image']; ?>" data-fancybox data-caption="<?php echo stripslashes($dnd_choice_row['sub_option']); ?>" class="qassets_icon"><img src="<?php echo $path . $dnd_choice_row['image']; ?>" /></a>
								<?php endif; ?>
								<div class="textwraping <?php echo (empty($dnd_choice_row['image'])) ? 'dd_twrap' : ''; ?>"><?php echo stripslashes($dnd_choice_row['sub_option']); ?></div>
								</div>
							</div>
							<?php endforeach; endif; ?>
							<div class="flexheight33 multiDBDbanner BGMUltTrans">
								<?php $dndi = 1; foreach ($dnd_choice_data as $data): ?>
								<div class="dragBox drop option_brand dragBox<?php echo $dndi.'_'.$q ?>" data-qtype="<?php echo $qtype ?>" data-qid="<?php echo $data['question_id'] ?>"></div>
								<?php $dndi++; endforeach; ?>
							</div>
						</div>
						<div class="flex33 DROPclassicbanner DROPclassicA fade-in">
							<?php if (count($ansData) > 0): foreach ($ansData as $dnd_match_row): ?>
							<div class="multiDBDbox dragdrop option_brand" data-aid="<?php echo $dnd_match_row['answer_id'] ?>">
								<?php if ( ! empty($dnd_match_row['image']) && file_exists($root_path . $dnd_match_row['image'])): ?>
								<a href="<?php echo $path . $dnd_match_row['image']; ?>" data-fancybox data-caption="<?php echo stripslashes($dnd_match_row['choice_option']); ?>" class="qassets_icon"><img src="<?php echo $path . $dnd_match_row['image']; ?>" /></a>
								<?php endif; ?>
								<div class="textwraping"><?php echo stripslashes($dnd_match_row['choice_option']); ?></div>
							</div>
							<?php endforeach; endif; ?>
						</div>
						<div class="submitMTFcenter col-sm-12">
							<button type="button" id="btn_<?php echo $q; ?>" class="btn submitbtn fade-in" data-show-report="<?php echo ($totalQ == $q) ? 'true' : 'false'; ?>" data-target="<?php echo $q + 1 ?>" data-cur="<?php echo $q ?>" data-sim-id="<?php echo $sim_id ?>" data-qid="<?php echo $qrow['question_id'] ?>">SUBMIT</button>
						</div>	
					</div>
				</div>
				<?php endif; ?>
			</div>
		</div>
		<?php if ($totalQ == $q): ?>
		<span class="report-view">
			<div class="signbtn1 signbtnRepot" style="display:none;">
				<div class="repotView"><a href="learner-progress.php?report=true&sid=<?php echo $sim_id; ?>" target="_blank">
					<img src="img/list/report_simicon.svg" />VIEW REPORT</a>
				</div>
			</div>
		</span>
		<?php endif; $q++; endforeach; endif; ?>
	</div>
	<?php if ( ! empty($right_char)): ?>
	<div class="CharFlextwo right">
		<img class="img-fulid qusTempimg" src="<?php echo $right_char; ?>">
	</div>
	<?php endif; ?>
</div>

<div id="load_popup_modal_show" class="modal fade" tabindex="-1" data-keyboard="false" data-backdrop="static"></div>

<script src="content/js/moreless.js"></script>
<script src="content/js/articulate.js"></script>
<script src="content/js/owlcarousel/owl.carousel.min.js"></script>
<script src="content/js/launch-sim.js?v=<?php echo time() ?>"></script>
<script src="content/js/sim-branding.js?v=<?php echo time() ?>"></script>
<!-- For Proctor Video Recording -->
<?php if ($proctor == TRUE): ?>
<video id="myVideo" playsinline class="video-js vjs-default-skin" style="display: none;"></video>
<script src="content/js/proctor-api.js"></script>
<script src="content/js/recording.js"></script>
<?php endif; ?>
<script type="text/javascript">
	$('.options').click(function() {
		var aid = $(this).data('aid');
		var bid = $(this).data('box-id');
		$('#btn_'+ bid).attr('data-aid', aid);
		$('.options').removeClass('press');
		$(this).addClass('press');
		$('.btn').removeAttr('disabled', true);
	});
	
	//-----DRAG AND DROP----	
	$(function() {
		//draggable
        $('.dragdrop').draggable({
          revert: true,
          placeholder: true,
          droptarget: '.drop',
          drop: function(evt, droptarget) {
			if ($(droptarget).find('.dragdrop').length == 0){
				//== empty drop target, simply drop
				$(this).appendTo(droptarget).draggable();
				$(".flexheight33.multiDBDbanner.BGMUltTrans").addClass("BGMUltTrans1");
			}
			else {
				//== swapping drops
				var d1 = this;
				var d2 = $(droptarget).find('.dragdrop').eq(0);
				$(d2).appendTo($(d1).parent()).draggable();
				$(d1).appendTo(droptarget).draggable();
			}
          }
        });
	});
	
	//-----------Feedback-----------------
	$('body').on('click', '.submitbtn', function() {
		var cur		= $(this);
		var id  	= cur.data('cur');
		var option	= $('#option_' + id);
		var qtype 	= option.data('qtype');
		var report	= cur.data('show-report');
		var target	= cur.data('target');
		var current	= cur.data('cur');
		var sid		= cur.data('sim-id');
		var url 	= 'view-feedback-modal.php';
		var $modal	= $('#load_popup_modal_show');		
		$.LoadingOverlay("show");		
		/* MCQ */
		if (qtype == 4) {
			var mcqdata = $('#option_'+ id +' .press');
			if (mcqdata.data('aid')) {
				$modal.load(url, {'report': report, 'target': target, 'current': current, 'sid': sid, 'qid': mcqdata.data('qid'), 'aid': mcqdata.data('aid')}, function(res) {
					$.LoadingOverlay("hide");
					if (res != 'error') {
						$modal.modal('show', {backdrop: 'static', keyboard: false});
					}
					else { swal({text: 'Oops, something went wrong. Please try again later', buttons: false, icon: "error", timer: 1000}); }
				});
			}
			else { swal({text: 'Oops, something went wrong. Please try again later', buttons: false, icon: "error", timer: 1000}); }
		}
		/* SWIPING */
		else if (qtype == 7) {
			var swip = $('#option_'+ id +' .owl-stage .center .swipeoptions');
			if (swip.data('aid')) {
				$modal.load(url, {'report': report, 'target': target, 'current': current, 'sid': sid, 'qid': swip.data('qid'), 'aid': swip.data('aid')}, function(res) {
					$.LoadingOverlay("hide");
					if (res != 'error') {
						$modal.modal('show', {backdrop: 'static', keyboard: false});
					}
					else { swal({text: 'Oops, something went wrong. Please try again later', buttons: false, icon: "error", timer: 1000}); }
				});
			}
			else { swal({text: 'Oops, something went wrong. Please try again later', buttons: false, icon: "error", timer: 1000}); }
		}
		/* D&D */
		else if (qtype == 8) {
			var dd_data = $('#option_'+ id +' .dragBox');
			if (dd_data.children('div').data('aid')){
				$modal.load(url, {'report': report, 'target': target, 'current': current, 'sid': sid, 'qid': dd_data.data('qid'), 'aid': dd_data.children('div').data('aid')}, function(res) {
					$.LoadingOverlay("hide");
					if (res != 'error') {
						$modal.modal('show', {backdrop: 'static', keyboard: false});
					}
					else { swal({text: 'Oops, something went wrong. Please try again later', buttons: false, icon: "error", timer: 1000}); }
				});
			}
			else { swal({text: 'Oops, something went wrong. Please try again later', buttons: false, icon: "error", timer: 1000}); }
		}
	});
</script>
<?php 
require_once 'includes/footer.php';
