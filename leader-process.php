<?php 
session_start();
if(isset($_SESSION['username'])) {
    $user 	= $_SESSION['username'];
	$role 	= $_SESSION['role'];
	$userid	= $_SESSION['userId'];
}
else {
    exit;
}
require_once 'config/db.class.php';
require_once 'includes/mailer/Send_Mail.php';
$db 		= new DBConnection();
$sim_id		= (isset($_POST['sim_id']) && ! empty($_POST['sim_id'])) ? $_POST['sim_id'] : '';
$pdf_data	= (isset($_FILES['pdf_data']) && ! empty($_FILES['pdf_data'])) ? $_FILES['pdf_data'] : '';
$attachment	= $pdf_data['tmp_name'];
$filename	= $user.'-learner-report.pdf';
$sim_data	= $db->getScenario($sim_id);
$get_groups	= $db->getAssignSimGroup($sim_data['scenario_id']);
$curdate	= date('Y-m-d');
$curtime	= date('H:i a');
if ( ! empty($get_groups)):
	$sql = "SELECT g.group_name, g.group_leader, CONCAT(u.fname, ' ', u.lname) as name, u.username, u.email FROM group_tbl g LEFT JOIN users u ON g.group_leader = u.id WHERE g.group_leader != '' AND u.email != '' AND g.group_id IN (". $get_groups .") AND FIND_IN_SET(". $userid .", g.learner)";
	$q = $db->prepare($sql);
	if ($q->execute() && $q->rowCount() > 0):
		$res = '';
		$getTempData = $db->getEmailTemplate(NULL, 'group_leader');
		$template	 = $getTempData['tmp_des'];
		$subject	 = $getTempData['subject'];
		$report_link = $db->getBaseUrl('view-learner-progress-report.php?view-report=true&sid='.$sim_id.'&uid='.base64_encode($userid));
		foreach ($q->fetchAll(PDO::FETCH_ASSOC) as $row):
			$to = trim($row['email']);
			if ( ! empty($to)):
				$values = array('name'			=> ( ! empty($row['name'])) ? $row['name'] : $row['username'],
								'simname'		=> $sim_data['Scenario_title'],
								'learner_name'	=> $user,
								'date'			=> $curdate,
								'time'			=> $curtime,
								'logo'			=> TEMPLATE_HEADER_LOGO,
								'url'			=> LOGIN_URL,
								'cmail'			=> CONTACT_EMAIL,
								'tname'			=> FTITLE,
								'subject'		=> $subject,
								'report_link'	=> $report_link);
				$res = sendAttachmentMail($to, $template, $values, $subject, $attachment, $filename);
			endif;
		endforeach;
		if ($res):
			$resdata = array('success' => TRUE, 'msg' => 'Report sent to group leader successfully.');
		else:
			$resdata = array('success' => FALSE, 'msg' => 'Report not sent to group leader.');
		endif;
	else:
		$resdata = array('success' => FALSE, 'msg' => 'Report not sent to group leader.');
	endif;
	echo json_encode($resdata);
endif;
