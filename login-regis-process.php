<?php
//echo "test";die;
ob_start();
session_start();
require_once 'config/db.class.php';
require_once 'config/securimage/securimage.php';
require_once 'includes/mailer/Send_Mail.php';

header('Access-Control-Allow-Origin: *');


$db		= new DBConnection;
$image	= new Securimage();
$cdate	= date('Y-m-d H:i:s a');
//echo $_SERVER['REQUEST_METHOD'] ;die;
//echo isset($_REQUEST['log']) ? $_REQUEST['log'] . $_REQUEST['keyfirst']  : 'no key' ;die;
//echo isset($_REQUEST['keyfirst']) ? $_REQUEST['keyfirst'] : "no";die;
//echo isset($_POST['key']) ? $_POST['key'] : $_POST['key'];die;
//echo isset($_GET['email']) ? $_GET['email'] : $_GET['email'];die;
#------------------------------------------Login ------------------------
if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['login'])):
	extract($_POST);
	$error = FALSE;
	if (empty($username) || empty($password) || empty($role) || empty($captcha_code)):
		$resdata = array('success' => FALSE, 'msg' => 'Please enter all required fields.');
		$error = TRUE;
	elseif ( ! empty($captcha_code) && $image->check($captcha_code) == FALSE):
		$resdata = array('success' => FALSE, 'msg' => 'Please enter valid captcha code. try another.');
		$error = TRUE;
	elseif ( ! empty($username) && ! empty($password) && ! empty($role) && $db->checkUserBlock($username, $password, $role) == TRUE):
		$resdata = array('success' => FALSE, 'msg' => 'Account has been temporarily blocked. please contact to administrator.');
		$error = TRUE;
	elseif ( ! empty($username) && ! empty($password) && ! empty($role) && $db->checkUser($username, $password, $role) == FALSE):
		$resdata = array('success' => FALSE, 'msg' => 'Invalid credentials. Please try again.');
		$error = TRUE;
	elseif ($error == FALSE):		
		$sql = "SELECT id, username, role FROM users WHERE username = '". $username ."' AND password = '". md5($password) ."' AND role = '". $role ."' AND status = '1'";
		$q = $db->prepare($sql); $q->execute();
		if ($q->rowCount() > 0):
			$results = $q->fetch(PDO::FETCH_ASSOC);
			$_SESSION['username']	= $results['username'];
			$_SESSION['role']		= $results['role'];
			$_SESSION['userId']		= $results['id'];
			$url = '';
			switch ($results['role']) {
				case 'superAdmin':
					$url = 'super-admin-dashboard.php';
					break;
				case 'clientAdmin':
					$url = 'client-admin-dashboard.php';
					break;
				case 'admin':
					$url = 'admin-dashboard.php';
					break;
				case 'reviewer':
					$url = 'reviewer-dashboard.php';
					break;
				case 'author':
					$url = 'author-dashboard.php';
					break;
				case 'learner':
					$url = 'learner-dashboard.php';
					break;
				case 'educator':
                	$url = 'educator-dashboard.php';
                	break;
				default:
					return FALSE;
			}
			$psql = "UPDATE users SET last_login = '". $cdate ."' WHERE id = '". $results['id'] ."'";
			$pq = $db->prepare($psql); $pq->execute();
			$resdata = array('success' => TRUE, 'msg' => 'Login Successful.!', 'url' => $url);
		endif;
	endif;
	echo json_encode($resdata);
endif;



if (isset($_REQUEST['log']) && isset($_REQUEST['key']) ):
//echo $_REQUEST['key'];die;
	$unm = validateGenKey( urldecode($_REQUEST['key']));
$username = '';
	if($unm){
			$username		= $unm;
	}
//echo $username . "     tttt";die;

	$sql = "SELECT id, username, role FROM users WHERE username = '". $username ."'  AND role = 'learner' AND status = '1'";
	
	$q = $db->prepare($sql); $q->execute();
	//print_r($q);die;
//	echo $q->rowCount() ;die;
	if ($q->rowCount() > 0):

		$results = $q->fetch(PDO::FETCH_ASSOC);
	//	print_r($results);die;
		$_SESSION['username']	= $results['username'];
		$_SESSION['role']	= 'learner';
		$_SESSION['userId']		= $results['id'];
		$_SESSION['client_id'] = '26';
		$_SESSION['domain_name'] = 'localhost:8081';
	//	print_r($_SESSION);die;
		//echo $results['id'];die;
		$url = 'learner-dashboard.php';
		if(isset($_REQUEST['redirecturl'])){
			$url = urldecode($_REQUEST['redirecturl']);			
		}
		$_SESSION['autologin'] = 	 serialize(
			['key' => isset($_REQUEST['key']) ? $_REQUEST['key'] : '']
		); 
	else:
		/////////////Register/////////////////
		$autoGenPwd = 'ES-'. $db->get_random_string(6);
		$userData = array('uname'	=> trim($username),
						  'email'	=> $email,
						  'pwd'		=> md5($autoGenPwd),
						  'status'	=> '1',
						  'estatus'	=> 1,
						  'pwd_date'=> date('Y-m-d'),
							'role' => 'learner',
						  'date'	=> $cdate);
		$sql = "INSERT INTO `users` (`username`, `email`, `password`, `status`, `email_status`, `last_change_pwd`,`role`, `date`) 
				VALUES (:uname, :email, :pwd, :status, :estatus, :pwd_date, :role, :date)";
		$results = $db->prepare($sql);
			if ($results->execute($userData)):
				$uID = $db->lastInsertId();
				$getTempData = $db->getEmailTemplate(NULL, 'new_user');
				$template	 = $getTempData['tmp_des'];
				$subject	 = $getTempData['subject'];
				$values	   	 = array('full_name'	=> $username,
									'name'			=> $username,
									'pwd'			=> $autoGenPwd,
									'logo'			=> TEMPLATE_HEADER_LOGO,
									'url'			=> LOGIN_URL,
									'cmail'		=> CONTACT_EMAIL,
									'tname'		=> FTITLE,
									'subject'		=> $subject);
				$_SESSION['username']	= $username;
				$_SESSION['role']	= 'learner';
				$_SESSION['userId']	= $uID;
				$_SESSION['client_id'] = '26';
				$_SESSION['domain_name'] = 'localhost:8081';
			//	echo  isset($_REQUEST['key']) ? $_REQUEST['key'] : 'noooooo';
				$_SESSION['autologin'] = 	 serialize(
					['key' => isset($_REQUEST['key']) ? $_REQUEST['key'] : '']
				); 
				//sendMail($email, $template, $values, $subject);
			endif;
		endif;

	if(isset($_REQUEST['redirecturl'])){
		$url = urldecode($_REQUEST['redirecturl']);
		if(strpos($url,'?') > 0){
			$url = explode('?',$url);	
			$params = explode('&',$url[1]);
			$url = $url[0];

			foreach($params as $p){
				$p= explode('=',$p,2);
				$_REQUEST[$p[0]] = $p[1];
			}

		}	
	}else{
		$url = 'learner-dashboard.php';
	}
print_r($_SESSION);

	require_once($url);
	exit;
endif;





if (isset($_REQUEST['call']) && $_REQUEST['keyfirst'] == md5('moodle')):
//echo "<b>hi test</b>";die;
$username		= ( ! empty($_REQUEST['username'])) ? $_REQUEST['username'] : '';
//echo $username;die;
$email		= ( ! empty($_REQUEST['email'])) ? $_REQUEST['email'] : '';
	extract($_REQUEST);

	$sql = "SELECT id, username, role FROM users WHERE username = '". $username ."'  AND role = 'learner' AND status = '1'";
	$q = $db->prepare($sql); $q->execute();
	if ($q->rowCount() > 0):
		$results = $q->fetch(PDO::FETCH_ASSOC);
		$_SESSION['username']	= $username;
		$_SESSION['role']	= 'learner';
		$_SESSION['userId']		= $results['id'];
		$_SESSION['client_id'] = '26';
		$_SESSION['domain_name'] = 'localhost:8081';
		//echo $results['id'];die;
		$url = 'learner-dashboard.php';
		if(isset($_REQUEST['redirecturl'])){
			$url = urldecode($_REQUEST['redirecturl']);			
		}
		$_SESSION['autologin'] = 	 serialize(
			['key' => isset($_REQUEST['key']) ? $_REQUEST['key'] : '']
		); 
	else:
		/////////////Register/////////////////
		$autoGenPwd = 'ES-'. $db->get_random_string(6);
		$userData = array('uname'	=> trim($username),
						  'email'	=> $email,
						  'pwd'		=> md5($autoGenPwd),
						  'status'	=> '1',
						  'estatus'	=> 1,
						  'pwd_date'=> date('Y-m-d'),
							'role' => 'learner',
						  'date'	=> $cdate);
		$sql = "INSERT INTO `users` (`username`, `email`, `password`, `status`, `email_status`, `last_change_pwd`,`role`, `date`) 
				VALUES (:uname, :email, :pwd, :status, :estatus, :pwd_date, :role, :date)";
		$results = $db->prepare($sql);
			if ($results->execute($userData)):
				$uID = $db->lastInsertId();
				$getTempData = $db->getEmailTemplate(NULL, 'new_user');
				$template	 = $getTempData['tmp_des'];
				$subject	 = $getTempData['subject'];
				$values	   	 = array('full_name'	=> $username,
									'name'			=> $username,
									'pwd'			=> $autoGenPwd,
									'logo'			=> TEMPLATE_HEADER_LOGO,
									'url'			=> LOGIN_URL,
									'cmail'		=> CONTACT_EMAIL,
									'tname'		=> FTITLE,
									'subject'		=> $subject);
	$_SESSION['username']	= $username;
	$_SESSION['role']	= 'learner';
	$_SESSION['userId']	= $uID;
	$_SESSION['client_id'] = '26';
	$_SESSION['domain_name'] = 'localhost:8081';
	$url = 'learner-dashboard.php';
	$_SESSION['autologin'] = 	 serialize(
		['key' => isset($_REQUEST['key']) ? $_REQUEST['key'] : '']
	); 
				//sendMail($email, $template, $values, $subject);
			endif;  
	endif;

echo json_encode(['key' => genkey($username)]);die;
//	require_once($url);
//	exit;
endif;

function genkey($unm){              
	$u_arr = str_split($unm);
	$u_digits = '';
	$u_convr = '';
	foreach($u_arr as $u){
			$asc = (int)ord($u);
			$asc = $asc - 97 + 1;            
			$u_digits .= $asc;
			$u_convr .= chr($asc+97);
	}

	$hash = $u_convr .  $u_digits . $unm;        
$p_hash = password_hash($hash, PASSWORD_ARGON2I);
	$key = $u_convr . "&" . $p_hash ;
$key = urlencode($key);
return $key;
	die;
}

function validateGenKey($key){
	
	$pos1 = strpos($key, "&");        
	$u_convr = substr($key, 0 , $pos1);
	$u_arr = str_split($u_convr);
	$u_digits = '';
	$unm = '';
	
	foreach($u_arr as $u){
			$asc = ord($u);            
			$asc = $asc - 97;
			$u_digits .= $asc;
			$asc = $asc - 1;                                             
			$unm .= chr($asc + 97); 
	}
	
	$key1 = substr($key, $pos1+1);        
	$hash =  $u_convr .  $u_digits . $unm;
	
  $p_hash = password_verify($hash, $key1);
	if($p_hash){
		return $unm;
	}else{
		return false;
	}
//	return $p_hash;
}

