<?php 
session_start();
if (isset($_SESSION['username'])) {
	$user		= $_SESSION['username'];
	$role		= $_SESSION['role'];
	$userid		= $_SESSION['userId'];
	$client_id	= $_SESSION['client_id'];
}
else {
    header('location: index.php');
}
require_once 'config/db.class.php';
require_once 'config/upload.class.php';
$obj 			= new DBConnection();
$up 			= new Uploads;
$fieldseparator	= ",";
$lineseparator	= "\n";
$uploadpath		= 'user_data_file/';
$valid_exts		= array('csv');
$permission		= $obj->getClientPermission($client_id);
$allow_user		= ( ! empty($permission['totalUsers'])) ? $permission['totalUsers'] : 0;
$totalUser 		= $obj->getTotalUsers();

if ( ! is_dir($uploadpath)) mkdir($uploadpath, 0777, TRUE);

if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_FILES)):
	$file	= (isset($_FILES['file'])) ? $_FILES['file'] : '';
	$error	= FALSE;
	if (empty($file['name'])):
		$resdata = array('success' => FALSE, 'msg' => 'Please select csv file.');
		$error = TRUE;
	elseif ( ! empty($file) && ! in_array(strtolower(pathinfo($file['name'], PATHINFO_EXTENSION)), $valid_exts)):
		$resdata = array('success' => FALSE, 'msg' => 'Please select valid file. accept only csv file.');
		$error = TRUE;
	elseif ($error == FALSE && ! empty($file) && $file['size'] > 2097152):
		$resdata = array('success' => FALSE, 'msg' => 'Please select file max size 2 MB.');
		$error = TRUE;
	elseif ( ! empty($allow_user) && $totalUser >= $allow_user):
		$resdata = array('success' => FALSE, 'msg' => 'User creation limit full.');
		$error = TRUE;
	elseif ($error == FALSE):
		$csvfile		= $up->uploadFile($file['name'], $file['tmp_name'], $uploadpath);
		$nfile			= $uploadpath . $csvfile;
		$affectedRows	= $obj->exec('LOAD DATA LOCAL INFILE "'. $nfile .'" INTO TABLE users FIELDS TERMINATED by \',\' OPTIONALLY ENCLOSED BY \'"\' LINES TERMINATED BY \'\n\' IGNORE 1 LINES (username, fname, lname, email, mob, @var1, role, location, company, department, designation, date_of_join, status, @last_change_pwd) SET password = MD5(@var1), last_change_pwd = CURDATE()');
		$resdata 		= ($affectedRows) ? array('success' => TRUE, 'msg' => 'Records have been successfully added to the table!') : array('success' => FALSE, 'msg' => 'Duplicate username not applicable. try again later.!');
		@unlink($nfile);
	endif;
	echo json_encode($resdata);
endif;
