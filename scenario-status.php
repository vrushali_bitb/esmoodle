<?php
session_start();
if (isset($_SESSION['username'])) {
    $user     = $_SESSION['username'];
    $role     = $_SESSION['role'];
    $userid   = $_SESSION['userId'];
} else {
    header('location: index.php');
}

require_once 'config/db.class.php';
require_once 'includes/db.php';
require_once 'includes/formkey.class.php';
require_once 'includes/learners_menu.php';
require_once 'config/pagination.php';

$formKey = new formKey();
$db = new DBConnection();

if (isset($_GET['delete']) && !empty($_GET['status_id'])) :
    $id     = base64_decode($_GET['status_id']);
    $sql = "DELETE FROM scenario_status WHERE sid = ?";
    $q   = $db->prepare($sql);
    $response = $q->execute(array($id));
    if ($response == 1) :
        $delete = 'Scenario status delete successfully.!';
    endif;
endif;

$sqlr = "SELECT * FROM scenario_status ORDER BY sid DESC";
$q = $db->prepare($sqlr); $q->execute();
$rowCount = $q->rowCount();
$count = ($sqlr) ? $rowCount : 0;
$page  = (isset($_REQUEST['page']) && !empty($_REQUEST['page'])) ? $_REQUEST['page'] : 1;
$show  = 15;
$start = ($page - 1) * $show;
$pagination = getPaginationString($count, $show, $page, 1, basename(__FILE__), '?page=');
$sql = "SELECT * FROM scenario_status ORDER BY sid DESC LIMIT $start, $show";
$results = $db->prepare($sql);
$msg = (isset($_GET['msg'])) ? $_GET['msg'] : ''; ?>
<div class="header1">
    <img src="img/creat_scenario.png" style="max-width:100%; height:auto; margin-left:5px">
    <b style="margin-left:17px"> Scenario Status</b>
</div>
<br><br><br><br>
<div class='container-field' style='padding-left:5%; padding-right:5%'>
    <div class='row'>
        <button type="button" class="btn btn-info btn-lg pull-left" data-toggle="modal" data-target="#StatusModal">Add Scenario Status</button>
    </div>
</div>
<?php if (!empty($msg) && isset($_GET['add']) == 'true') : ?>
    <div class="alert alert-success alert-dismissable">Scenario status added successfully.!</div>
<?php elseif (!empty($msg) && isset($_GET['exist']) == 'true') : ?>
    <div class="alert alert-danger alert-dismissable">Scenario status already exists. please try another one.!</div>
<?php endif; ?>
<?php if (!empty($delete)) : ?>
    <div class="alert alert-success alert-dismissable"><?php echo $delete; ?></div>
<?php endif; ?>
<div class="col-sm-4 col-md-offset-2" id="showmsg" style="display:none;">
    <div class="alert alert-warning alert-dismissable" role="alert" id="msg"></div>
</div>
<div id="StatusModal" class="modal fade" role="dialog">
    <div class="modal-dialog" style="margin-top:100px">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h3 class="modal-title" style="color:#74CDD6">Create Scenario Status</h3>
            </div>
            <div class="modal-body">
                <form enctype="multipart/form" action="operations/manage_scenario.php" name="frmscenarioModal" method="post">
                    <?php echo $formKey->outputKey(); ?>
                    <input type="hidden" name="frmscenario" id="frmscenario" value="<?php echo encryptString('modalScenarioStatus', 'e'); ?>">
                    <div>
                        <label for="scenario_name">Scenario Status Name</label>
                        <input type="text" class="form-control" name="status_name" id="txtname" required="required">
                    </div>
                    <div>
                        <label for="scenario_description">Scenario Status Description</label>
                        <textarea name="status_description" id="status_description" class="form-control"></textarea>
                    </div>
                    <div class="modal-footer">
                        <input type="submit" class="btn btn-default" value="Submit">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div class='container-field' style='padding-left:5%; padding-right:5%'>
    <div class='row'>
        <div style="border:1px solid #c5c6c6;  margin-top:10px; box-shadow: 2px 2px white;"></div>
        <div style="overflow-x:auto;">
            <table>
                <tr>
                    <th>#</th>
                    <th>Status Name</th>
                    <th>Description</th>
                    <th>Created Date</th>
                    <th>Actions</th>
                </tr>
                <?php if ($results->execute() && $rowCount > 0) :
                    $i = 1;
                    foreach ($results->fetchAll(PDO::FETCH_ASSOC) as $row) : ?>
                        <tr>
                            <td><?php echo $i ?> </td>
                            <td><?php echo $row['status_name'] ?></td>
                            <td><?php echo $row['status_des'] ?></td>
                            <td><?php echo $db->dateFormat($row['status_date'], 'd-M-y'); ?></td>
                            <td><a class="edit" title="Edit" data-scenario-status-id="<?php echo md5($row['sid']) ?>" href="javascript:void(0);">
                                    <img src="img/img_edit.png" class="icoctc">
                                </a>
                            </td>
                        </tr>
                    <?php $i++;
                    endforeach;
                else : ?>
                    <tr>
                        <td colspan="5" style="text-align:center"><strong>No data available in database.</strong></td>
                    </tr>
                <?php endif; ?>
            </table>
        </div>
        <div class="pull-right">
            <nav aria-label="navigation"><?php echo (!empty($pagination)) ? $pagination : '&nbsp;'; ?></nav>
        </div>
    </div>
</div>

<div id="load_popup_modal_show" class="modal fade" tabindex="-1"></div>

<script type="text/javascript">
    $('.edit').click(function() {
        var id = $(this).attr('data-scenario-status-id');
        if (id != '') {
            $.LoadingOverlay("show");
            var $modal = $('#load_popup_modal_show');
            $modal.load('edit-scenario-status-modal.php', {
                'data-id': id
            }, function(res) {
                if (res != '') {
                    $.LoadingOverlay("hide");
                    $modal.modal('show');
                } else {
                    $.LoadingOverlay("hide");
                    swal("Error", 'Oops. something went wrong please try again.?', "error");
                }
            });
        }
    });
</script>
<?php
require_once 'includes/footer.php';
