<?php 
ob_start();
session_start();
if (isset($_SESSION['username'])) {
  $user     = $_SESSION['username'];
  $role     = $_SESSION['role'];
  $userid   = $_SESSION['userId'];
  $cid		= (isset($_SESSION['client_id'])) ? $_SESSION['client_id'] : exit('Oops. something went wrong please try again');
} else {
  header('location: index.php');
}

require_once 'config/db.class.php';
require_once 'includes/learners_menu.php';
require_once 'config/pagination.php';
$db				= new DBConnection();
$type_path		= 'img/type_icon/';
$action_path    = 'img/list/'; ?>
<div class="bottomheader">
    <div class="container-fluid">
        <ul class="breadcrumb">
			<?php echo $db->breadcrumbs(); ?>
        </ul>
     </div>
</div>
<style>
    body{
        overflow-x:hidden;
    }
</style>
<div class="Group_managment reViwerlistBanner">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
            	<div class="usermanage_main">
               		<div class="table-box">
                    	<div class="tableheader clearfix">
                        	<div class="tableheader_left">Review Simulation </div>
                            <div class="tableheader_right">
                            	<div class="Searchbox">
                                	<form method="get">
                                        <input type="serch" class="serchtext" name="searchkey" placeholder="Search" required>
                                        <div class="serchBTN"><img class="img-responsive" src="img/dash_icon/search.svg"></div>
                                    </form>
                                </div>
                                <div class="btn-group btn-shortBY">
                                    <button type="button" class="btn btn-Transprate"><span class="sort">Sort by</span></button>
                                    <button type="button" class="btn btn-Transprate dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img  src="img/down_arrow_white.png"></button>
                                    <div class="dropdown-menu shortBYMenu">
                                    <a class="dropdown-item" href="<?php echo $db->getBaseUrl($db->getFile()); ?>">Default Sorting</a>
                                    <a class="dropdown-item" href="<?php echo $db->getBaseUrl($db->getFile().'?orderby=type'); ?>">Type</a>
                                    <a class="dropdown-item" href="<?php echo $db->getBaseUrl($db->getFile().'?orderby=status'); ?>">Status</a>
                                  </div>
                                </div>
                           </div>
                    	</div>
                  	</div>
                    <div class="usermanage-form1">
                    	<div class="table-responsive">
						<?php 
							#----------Serch----------------
							$searchText     = '';
							if (isset($_GET['searchkey']) && ! empty($_GET['searchkey'])):
								extract($_GET);
								$searchText = " AND ( c.category LIKE '%$searchkey%' OR s.Scenario_title LIKE '%$searchkey%' )";
							endif;
							#----------Sort by----------------
							if (isset($_GET['orderby']) && $_GET['orderby'] == 'type'):
								$orderby    = "".(( ! empty($searchText)) ? $searchText : " ")." ORDER BY s.author_scenario_type";
							elseif (isset($_GET['orderby']) && $_GET['orderby'] == 'status'):
								$orderby    = "".(( ! empty($searchText)) ? $searchText : " ")." ORDER BY s.status";
							else:
								$orderby    = "".(( ! empty($searchText)) ? $searchText : " ")." ORDER BY s.scenario_id DESC";
							endif;
							$sqlr           = "SELECT DISTINCT s.scenario_id, s.Scenario_title, s.duration, s.scenario_type, s.scenario_category, s.author_scenario_type, s.assign_author, s.assign_start_date, s.assign_end_date, s.sim_ques_type, s.sim_temp_type, s.created_on, s.update_sim, c.category, st.status_name, t.type_name, t.type_icon FROM 
                                                scenario_master s INNER JOIN 
                                                group_tbl g LEFT JOIN 
                                                category_tbl c ON c.cat_id = s.scenario_category LEFT JOIN 
                                                scenario_status st ON st.sid = s.status LEFT JOIN 
                                                scenario_type t ON t.st_id = s.author_scenario_type WHERE 
                                                s.author_scenario_type != '0' AND (CASE WHEN s.assign_reviewer != '' THEN FIND_IN_SET('". $userid ."', s.assign_reviewer) WHEN s.assign_group != '' THEN FIND_IN_SET('". $userid ."', g.reviewer) END) $orderby";
							$q              = $db->prepare($sqlr); $q->execute();
                            $rowCount       = $q->rowCount();
                            $count	        = ($sqlr) ? $rowCount : 0;
                            $page           = (isset($_REQUEST['page']) && ! empty($_REQUEST['page'])) ? $_REQUEST['page'] : 1;
                            $show  	        = 15;
                            $start 	        = ($page - 1) * $show;
                            $npagination    = getPaginationString($count, $show, $page, 1, basename(__FILE__), '?page=');
							$sql 	        = $sqlr ." LIMIT $start, $show";
							$results        = $db->prepare($sql); ?>
                            <table class="table table-striped">
                                <thead class="Theader">
                                    <tr>
                                        <th>Category Name</th>
                                        <th>Simulation Title</th>
                                        <th>Creation Date</th>
                                        <th>Start Date</th>
                                        <th>End Date</th>
                                        <th>Duration in min</th>
                                        <th>Total Question</th>
                                        <th>Authors Name</th>
                                        <th>Simulation Type</th>
                                        <th>Stage</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
								<?php if ($results->execute() && $rowCount > 0):
									foreach ($results->fetchAll(PDO::FETCH_ASSOC) as $nrow): ?>
                                    <tr>
                                        <td><?php echo $nrow['category']; ?></td>
                                        <td title="<?php echo $nrow['Scenario_title']; ?>"><?php echo $db->truncateText($nrow['Scenario_title'], 40); ?></td>
                                        <td><?php echo $db->dateFormat($nrow['created_on'], 'd-M-y'); ?></td>
                                        <td><?php echo ($nrow['assign_start_date'] != '0000-00-00') ? $db->dateFormat($nrow['assign_start_date'], 'd-M-y') : '--' ?></td>
                                        <td><?php echo ($nrow['assign_end_date'] != '0000-00-00') ? $db->dateFormat($nrow['assign_end_date'], 'd-M-y') : '--' ?></td>
                                        <td><?php echo ( ! empty($nrow['duration'])) ? $nrow['duration'] : 0; ?></td>
                                        <td><?php echo $db->getTotalQuestionsByScenario(md5($nrow['scenario_id'])); ?></td>
                                        <td><?php if ( ! empty($nrow['assign_author'])): ?>
                                            <a data-toggle="popover" title="View Author" href="<?php echo $db->getBaseUrl('includes/ajax.php?getTooltipData=true&ids='. $nrow['assign_author']); ?>" onclick="return false"><i class="fa fa-info-circle fa-2x" aria-hidden="true"></i></a>
                                        <?php endif; ?></td>
                                        <td><img src="<?php echo $type_path . $nrow['type_icon'] ?>" class="type_icon" title="<?php echo $nrow['type_name'] ?>" /></td>
                                        <td><strong><?php echo $nrow['status_name']; ?></strong></td>
                                        <td><a class="update_stage tooltiptop" data-scenario-id="<?php echo md5($nrow['scenario_id']); ?>" href="javascript:void(0);"><span class="tooltiptext">Update Stage</span><img class="svg web_icon" src="<?php echo $action_path; ?>update_stage.svg"></a></td>
                                        <td><a href="review-simulation-details.php?review=true&sim_id=<?php echo md5($nrow['scenario_id']); ?>" target="_blank" class="tooltiptop"><span class="tooltiptext">Review</span><img class="svg" src="<?php echo $action_path; ?>review_list.svg"></a></td>
                                    </tr>
                                    <?php endforeach; else: ?>
                                    <tr><td colspan="10" style="text-align:center"><strong>No data available in database.</strong></td></tr>
                                    <?php endif; ?>
                                </tbody>
                            </table>
                        </div>
                        <div class="pull-right">
                            <nav aria-label="navigation"><?php echo ( ! empty($npagination)) ? $npagination : '&nbsp;'; ?></nav>
                        </div>
                    </div>
              	</div>
            </div>
        </div>
    </div>
</div>
<style>
a.disabled {
	color: currentColor;
	cursor: not-allowed;
	opacity: 0.2;
	text-decoration: none;
}
.table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th {
    padding: 10px;
}
</style>

<div id="load_popup_modal_show" class="modal fade" tabindex="-1"></div>

<script type="text/javascript">
$(function () {
  $('[data-toggle="tooltip"]').tooltip()
});

$('.update_stage').click(function () {
    var id = $(this).attr('data-scenario-id');
    if (id != '') {
        $.LoadingOverlay("show");
        var $modal = $('#load_popup_modal_show');
        $modal.load('edit-scenario-stage-modal.php', {'data-id': id }, function(res) {
            if (res != '') {
                $.LoadingOverlay("hide");
                $modal.modal('show');
            }
            else {
                $.LoadingOverlay("hide");
                swal("Error", 'Oops. something went wrong please try again.?', "error");
            }
        });
    }
});
</script>
<script src="content/js/tooltip.js"></script>
<?php 
require_once 'includes/footer.php';
