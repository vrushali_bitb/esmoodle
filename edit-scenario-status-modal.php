<?php 
session_start();
if (isset($_SESSION['username'])):
	$user = $_SESSION['username'];
else:
	die;
endif;
require_once 'config/db.class.php';
$db		 = new DBConnection;
$getid	 = (isset($_POST['data-id'])) ? $_POST['data-id'] : FALSE;
$getdata = ( ! empty($getid)) ? $db->getScenarioStatus($getid) : FALSE; ?>
<div id="load_popup_modal_contant" class="" role="dialog">
    <div class="modal-dialog" style="margin-top:100px">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h3 class="modal-title" style="color:#115961">Update Scenario Status</h3>
            </div>
            <div class="modal-body">
            	<form enctype="multipart/form" name="edit_scenario_status_form"  id="edit_scenario_status_form" method="post">
                	<div>
                        <label for="scenario_name">Scenario Status Name *</label>
                        <input type="text" class="form-control" name="status_name" id="txtname" value="<?php echo $getdata['status_name'] ?>" required="required">
                    </div>
                    <div>
                        <label for="scenario_description">Scenario Status Description</label>
                        <textarea name="status_description" id="status_description" class="form-control"><?php echo $getdata['status_des'] ?></textarea>
                    </div>                    
                    <div class="modal-footer">
						<input type="hidden" name="update_scenario_status_id" value="<?php echo $getdata['sid'] ?>" />
                        <button type="submit" name="editScenarioStatus" id="editScenarioStatus" class="btn btn-outline btn-success" onClick="return confirm('Are you sure to update this Scenario Status.?');">Update</button>
						<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </form>
            </div>
        </div>
     </div>
</div>
<script type="text/javascript">
$("#edit_scenario_status_form").on('submit', (function(e) {
	e.preventDefault();
	var form_data = $(this).serialize();
	$('#editScenarioStatus').attr('disabled', 'disabled').html('<img src="scenario/img/loader.gif"> Please wait....');
	$.ajax({
		url: "includes/process.php",
		type: "POST",
		data: form_data,
		success: function(result) {
			var res = $.parseJSON(result);
			if (res.success == true) {
				swal({text: res.msg, buttons: false, icon: "success", timer: 1000});
				setTimeout(function() { window.location.reload(); }, 500);
			}
			else if (res.success == false) {
				swal({text: res.msg, buttons: false, icon: "error", timer: 1000});
				setTimeout(function() { window.location.reload(); }, 500);
			}
		},error: function() {
			swal({text: 'Oops, something went wrong. Please try again later', buttons: false, icon: "error", timer: 1000});
		}
	});
}));
</script>
<?php 
ob_end_flush();
