<?php 
ob_start();
session_start();
if (isset($_SESSION['username'])) {
  $user   = $_SESSION['username'];
  $role   = $_SESSION['role'];
  $userid = $_SESSION['userId'];
  $cid		= (isset($_SESSION['client_id'])) ? $_SESSION['client_id'] : exit('Oops. something went wrong please try again');
} else {
  header('location: index.php');
}

require_once 'config/db.class.php';
require_once 'includes/learners_menu.php';
$db = new DBConnection();

/* page access only client administrator */
($db->checkAccountType() != 2) ? header('location: index') : '';

/* Get clients hosting data */
$hdata = $db->getUsedHostingData($cid); ?>
<style>
  body{
    overflow:hidden;
  }
  admin-dashboard.Ev2 .supDASHTOP {
      margin: 0px;
  }
  .cls-1 {
      stroke: transparent !important;
  }
  .flexRI.client.clients {
      display: flex;
  }
  .ClinetSelF {
      font-size: 24px;
  }
  .flexItem .item {
    margin: 5px;
}
.hometab.active,
.menu1tab.active{
    display: flex !important;
}
canvas.canvasjs-chart-canvas {
    width: 100%;
}
 </style>
<link rel="stylesheet" href="content/css/owlcarousel/owl.carousel.min.css" />
<link rel="stylesheet" href="content/js/daterangepicker/daterangepicker.css" />
<div class="admin-dashboard Ev2">
  <div class="container-fluid">
    <div class="L-calender">
      <div class="input-group calenderclient">
        <input type="text" id="reportrange" class="form-control" readonly="readonly" />
        <span class="input-group-addon"><span class="fa fa-calendar"></span></span> </div>
    </div>
    <div class="flexItem carouselWraper">
      <div class="flextlistbaner hometab clearfix owl-theme active">
        <div class="item">
          <div class="flextlistitem">
            <div class="textFlex CAtextflex">
              <div class="clienttextbox">
                <div class="CTUser">USER</div>
                <div class="CTUserNo"><?php echo $db->getTotalUsers(); ?></div>
                <div class="CTGroupBanneR">
                  <div class="CTGroup">Group</div>
                  <div class="CTGroupNo"><?php echo $db->getTotalGroup(); ?></div>
                </div>
              </div>
              <ul class="flexLI clientflexLI">
                <li>Admin <span><?php echo $db->getTotalUsers('admin'); ?></span></li>
                <li>Author <span><?php echo $db->getTotalUsers('author'); ?></span></li>
                <li>Reviewer <span><?php echo $db->getTotalUsers('reviewer'); ?></span></li>
                <li>Educator <span><?php echo $db->getTotalUsers('educator'); ?></span></li>
                <li>Learner <span><?php echo $db->getTotalUsers('learner'); ?></span></li>
              </ul>
            </div>
          </div>
        </div>
        <div class="item">
          <div class="flextlistitem SIMulationT1">
            <div class="textFlex CAtextflex2">
              <div class="col-sm-6">
                <div class="Tright"><?php echo $db->getTotalCategory(); ?></div>
                <div class="A16F">SIMULATION<br>CATEGORIES</div>
              </div>
              <div class="col-sm-6">
                <div class="Tright"><?php echo $db->getTotalScenario(); ?></div>
                <div class="A16F">SIMULATIONS</div>
              </div>
            </div>
          </div>
        </div>
        <div class="item">
          <div class="flextlistitem">
            <div id="top_learner" style="height: 170px; width: 100%;"></div>
          </div>
        </div>
        <div class="item">
          <div class="flextlistitem T1">
            <div id="lead_group" style="height: 170px; width: 100%;"></div>
          </div>
        </div>
        <div class="item">
          <div class="flextlistitem">
            <div id="trand_sim" style="height: 170px; width: 100%;"></div>
          </div>
        </div>
        <div class="item">
          <div class="flextlistitem T1">
            <div id="top_comp" style="height: 170px; width: 100%;"></div>
          </div>
        </div>
      </div>

      <div class="col-container flextlistbaner flextlistbanertab2 menu1tab clearfix owl-theme">
        <div class="item">
          <div class="supDASHTOP supDASHTOPtab2">
            <div class="supDASHTOPtextB">
              <div class="flexRI client">
                <p class="noofsubtext">Number of <br>Sub-Client</p>
              </div>
              <div class="flexRI">
                <p class="noofsubtextno" id="clientAdmin_sub_client">0</p>
              </div>
            </div>
          </div>
        </div>
        <div class="item">
        <div class="supDASHTOP supDASHTOPtab2">          
          <div class="superadhtable-baaner">
              <ul class="superadhtable manAGEYurself ">
                <li>Group <span id="clientAdmin_group">0</span></li>
                <li>Admin <span id="clientAdmin_admin">0</span></li>
                <li>Author <span id="clientAdmin_author">0</span></li>
                <li>Reviewer <span id="clientAdmin_reviewer">0</span></li>
                <li>Educator <span id="clientAdmin_educator">0</span></li>
                <li>Learner <span id="clientAdmin_learner">0</span></li>
              </ul>
            </div>
          </div>
        </div>
        <div class="item item900" style="width:900px">
          <div class="supDASHTOP supDASHTOPtab2 supDASHTOPta2_1">
            <div class="flextlistitem">
              <div class="textFlex">
                <div class="usermanage-form">
                  <select class="form-control multiselect-ui selection" name="client_id" id="client_id" data-placeholder="Select Sub-Client" multiple="multiple">
                    <option value="0" selected="selected" disabled="disabled">Select Sub-Client</option>
                  </select>
                </div>
                <div class="ShowhostingBanner">
                  <div class="Showhostingitem">
                    <div class="hosting_space">
                      <div class="A24F">HOSTING SPACE</div>
                      <div class="hosting_no" id="hosting_space">0</div>
                    </div>
                  </div>
                  <div class="Showhostingitem">
                    <p class="A24F">START DATE <span id="sdate"></span</p>
                    <p class="A24F">END DATE <span id="edate"></span<</p>
                  </div>
                  <div class="Showhostingitem">
                    <div class="superadhtable-baaner">
                      <ul class="superadhtable HOSTstartban">
                        <li>Group <span id="group">0</span></li>
                        <li>Admin <span id="admin">0</span></li>
                        <li>Author <span id="author">0</span></li>
                        <li>Reviewer <span id="reviewer">0</span></li>
                        <li>Educator <span id="educator">0</span></li>
                        <li>Learner <span id="learner">0</span></li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="item">
          <div class="supDASHTOP supDASHTOPtab2">
            <div id="UsedHostingData"></div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <ul class="nav nav-tabs">
    <li class="homeclick active"><a data-toggle="tab" href="#home">Manage Yourself</a></li>
    <li class="menuclick"><a data-toggle="tab" href="#menu1">Manage Your Sub Client </a></li>
  </ul>
  <div class="tab-content">
    <div id="home" class="tab-pane fade in active">
      <div class="adminT">
        <div class="col-sm-2"> <a href="<?php echo $db->getBaseUrl('user-management.php') ?>">
          <div class="dashboard-box colorBoxW"> <img class="img-responsive width70 svg" src="img/dash_icon/manage_user.svg">
            <div class="dash-sub-title">Manage Users</div>
            <div class="dash-msg clearfix">
              <div class="notino"><img class="img-responsive" src="img/massage.png"> <span class="notifi-icon"> <?php echo $db->getTotalNewUsers(); ?></span></div>
            </div>
          </div>
          </a> </div>
        <div class="col-sm-2"> <a href="<?php echo $db->getBaseUrl('group-management.php') ?>">
          <div class="dashboard-box colorBoxW"> <img class="img-responsive width70 svg" src="img/dash_icon/manage_group.svg">
            <div class="dash-sub-title">Manage Groups</div>
            <div class="dash-msg clearfix">
              <div class="notino"><img class="img-responsive" src="img/massage.png"> <span class="notifi-icon"> <?php echo $db->getTotalGroup(); ?></span></div>
            </div>
          </div>
          </a> </div>
        <div class="col-sm-2"> <a href="<?php echo $db->getBaseUrl('manage-simulations.php') ?>">
          <div class="dashboard-box colorBoxW"> <img class="img-responsive width70 svg" src="img/dash_icon/manage_sim.svg">
            <div class="dash-sub-title">Manage Simulations</div>
            <div class="dash-msg clearfix">
              <div class="notino"> <img class="img-responsive" src="img/massage.png"> <span class="notifi-icon"> <?php echo $db->getDraftScenario(2); ?></span></div>
            </div>
          </div>
          </a> </div>
        <div class="col-sm-2"> <a href="<?php echo $db->getBaseUrl('simulation-assignment.php') ?>">
          <div class="dashboard-box colorBoxW"> <img class="img-responsive width70 svg" src="img/dash_icon/assign_sim.svg">
            <div class="dash-sub-title">Assign Simulations</div>
            <div class="dash-msg clearfix">
              <div class="notino"><img class="img-responsive" src="img/massage.png"> <span class="notifi-icon"> <?php echo $db->getAssignScenario(); ?></span></div>
            </div>
          </div>
          </a> </div>
        <div class="col-sm-2"> <a href="<?php echo $db->getBaseUrl('email-management.php') ?>">
          <div class="dashboard-box colorBoxW"> <img class="img-responsive width70 svg" src="img/dash_icon/manage_template.svg">
            <div class="dash-sub-title">Manage Templates</div>
            <div class="dash-msg clearfix">
              <div class="notino"><img class="img-responsive" src="img/massage.png"> <span class="notifi-icon"><?php echo $db->getTotalTemplate(); ?></span> </div>
            </div>
          </div>
          </a></div>
        <div class="col-sm-2">
          <div class="dashboard-box colorBoxW"> <img class="img-responsive width70 svg" src="img/dash_icon/library.svg">
            <div class="dash-sub-title">Manage Library</div>
            <div class="dash-msg clearfix"><div class="notino"><img class="img-responsive" src="img/massage.png"> <span class="notifi-icon">1</span> </div></div>
          </div>
        </div>
        <div class="col-sm-2">
          <a href="<?php echo $db->getBaseUrl('manage-branding.php') ?>">
            <div class="dashboard-box colorBoxW"> <img class="img-responsive width70 svg" src="img/dash_icon/branding.svg">
              <div class="dash-sub-title">Manage Theme</div>
              <div class="dash-msg clearfix"><div class="notino"><img class="img-responsive" src="img/massage.png"><span class="notifi-icon">1</span></div></div>
            </div>
          </a>
        </div>
        <div class="col-sm-2">
          <div class="dashboard-box colorBoxW"> <img class="img-responsive width70 svg" src="img/dash_icon/view_report1.svg">
            <div class="dash-sub-title">View Report</div>
            <div class="dash-msg clearfix">
              <div class="notino"><img class="img-responsive" src="img/massage.png"><span class="notifi-icon">1</span></div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div id="menu1" class="tab-pane fade">
      <div class="adminT">
        <div class="col-sm-2"><a href="<?php echo $db->getBaseUrl('sub-client-management.php') ?>">
          <div class="dashboard-box colorBoxW"> <img class="img-responsive width70 svg" src="img/dash_icon/manage_subclient.svg">
            <div class="dash-sub-title">Manage Sub Client</div>
            <div class="dash-msg clearfix">
              <div class="notino"> <img class="img-responsive" src="img/massage.png"> <span class="notifi-icon">1</span> </div>
            </div>
          </div>
          </a> </div>
        <div class="col-sm-2"><a href="#">
          <div class="dashboard-box colorBoxW"> <img class="img-responsive width70 svg" src="img/dash_icon/view_report1.svg">
            <div class="dash-sub-title">View Report</div>
            <div class="dash-msg clearfix">
              <div class="notino"><img class="img-responsive" src="img/massage.png"> <span class="notifi-icon">1</span></div>
            </div>
          </div>
          </a></div>
      </div>
    </div>
  </div>
</div>
<script src="content/js/daterangepicker/moment.min.js"></script>
<script src="content/js/daterangepicker/daterangepicker.js"></script>
<script src="content/js/canvasjs.min.js"></script>      
<script src="content/js/owlcarousel/owl.carousel.min.js"></script>
<script>
  $(document).ready(function() {
    $(".menuclick").click(function(){
      $('.hometab').removeClass('active');
      $('.menu1tab').addClass('active');
      $(".L-calender").css('visibility', 'hidden')
  });
  $(".homeclick").click(function(){
    $('.hometab').addClass('active');
    $('.menu1tab').removeClass('active');
    $(".L-calender").css('visibility', 'visible')
  });

  var start = moment().subtract(29, 'days');
  var end   = moment();
  
  function cb(start, end) {
    $('#reportrange span').html(start.format('YYYY-MM-DD') + ' - ' + end.format('YYYY-MM-DD'));
  }
  
  $('#reportrange').daterangepicker({
    startDate: start,
    endDate: end,
    locale: { format: 'YYYY-MM-DD' },
    ranges: {
      'Today': [moment(), moment()],
      'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
      'Last 7 Days': [moment().subtract(6, 'days'), moment()],
      'Last 30 Days': [moment().subtract(29, 'days'), moment()],
      'This Month': [moment().startOf('month'), moment().endOf('month')],
      'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
    } }, cb);
    cb(start, end);
    
  function defaultData(){
    $.LoadingOverlay("show");
    $.ajax({
      url: 'includes/main-process.php',
      type: "POST",
      data: 'getClientAdminDefaultData='+ true,
      cache: false,
      success: function(resdata) {
        $.LoadingOverlay("hide");
        var res = $.parseJSON(resdata);
        if (res.success == true) {
          $('#clientAdmin_organization').html(res.organization);
          $('#clientAdmin_sub_client').html(res.subclient);
          $('#clientAdmin_group').html(res.data.group);
          $('#clientAdmin_admin').html(res.data.admin);
          $('#clientAdmin_author').html(res.data.auth);
          $('#clientAdmin_reviewer').html(res.data.reviewer);
          $('#clientAdmin_learner').html(res.data.learner);
          $('#clientAdmin_educator').html(res.data.educator);
        }
        else {
          swal({text: 'Oops. something went wrong please try again.', buttons: false, icon: "error", timer: 2000});
        }
      },error: function() {
        $.LoadingOverlay("hide");
        swal({text: 'Oops. something went wrong please try again.', buttons: false, icon: "error", timer: 2000});
      }
    });
  }

  defaultData();

  var maxCount = 1;
  $('.multiselect-ui').multiselect({
    disableIfEmpty: true,
    filterPlaceholder: 'Search & Select Sub-Client',
    enableCaseInsensitiveFiltering: true,
    enableFiltering: true,
    maxHeight: 250,
    numberDisplayed: 1,
    includeSelectAllOption: false,
    onChange: function(option, checked, select) {
      //Get selected count
      var selectedCount = $("#client_id").find("option:selected").length;
      
      //If the selected count is equal to the max allowed count, then disable any unchecked boxes
      if (selectedCount >= maxCount) {
        $("#client_id option:not(:selected)").prop('disabled', true);
        $("#client_id").multiselect('refresh');
        var cid = $("#client_id").find("option:selected").val();
        $.LoadingOverlay("show");
        $.ajax({
          url: 'includes/main-process.php',
          type: "POST",
          data: 'getClientData='+ true + '&client_id='+ cid,
          cache: false,
          success: function(resdata) {
            $.LoadingOverlay("hide");
            var res = $.parseJSON(resdata);
            if (res.success == true) {
              if (res.data.client != false) {
                $('#clients_data').html(res.data.client);
              }
              else {
                $('#clients_data').html(0);
              }
              $('#organization').html(res.organization);
              $('#sdate').html(res.start_date);
              $('#edate').html(res.end_date);
              $('#hosting_space').html(res.used_space + ' / ' + res.space);
              $('#group').html(res.data.group);
              $('#admin').html(res.data.admin);
              $('#author').html(res.data.auth);
              $('#reviewer').html(res.data.reviewer);
              $('#learner').html(res.data.learner);
              $('#educator').html(res.data.educator);
            }
            else if (res.success == false) {
              swal({text: res.msg, buttons: false, icon: "error", timer: 2000});
            }
          },error: function() {
            $.LoadingOverlay("hide");
            swal({text: 'Oops. something went wrong please try again.', buttons: false, icon: "error", timer: 2000});
          }
        });
      }
      //If the selected count is less than the max allowed count, then set all boxes to enabled
      else {
        $("#client_id option:disabled").prop('disabled', false);
        $("#client_id").multiselect('refresh');
        $('#group,#admin,#author,#reviewer,#learner,#educator,#clients_data,#hosting_space').html(0);
        $('#organization,#sdate,#edate').html('&nbsp;');
      }
    }
  });
	
	$('#client_id').empty();
	$('#client_id').multiselect('refresh');
	$.getJSON('includes/main-process.php?getClients=true&getSubClients=true', function(res) {
		if (res.success == true) {
			$('#client_id').multiselect('dataprovider', res.data);
		}
		else if (res.success == false) {
			$('#client_id').multiselect('rebuild');
			$('#client_id').multiselect('refresh');
		}
	});
  
  CanvasJS.addColorSet("colorcode", ["#6d3073", "#3b602d", "#c07744", "#678087", "#678087"]);

  function defaultGraphData(date){
    $.ajax({
      type: "POST",
      url: 'includes/client-admin-dash-grid-data.php',
      data: 'getClientAdminDashData='+ true + '&date='+ date,
      cache: false,
      success: function (resdata) {
        $.LoadingOverlay("hide");
        var res = $.parseJSON(resdata);
        if (res.success == true) {
          /* TRENDING SIM */
          if (res.data.sim.length) {
            var trendingSimChart = new CanvasJS.Chart("trand_sim", {
                colorSet: "colorcode",
                title: { text: "TRENDING SIMULATIONS"},
                animationEnabled: true,
                theme: "light2",
                backgroundColor: '#fff',
                axisX: { gridThickness: 0, tickLength: 0, lineThickness: 0, labelFontColor: "black", labelFormatter: function() { return " "; } },
                axisY: { gridThickness: 0, tickLength: 0, lineThickness: 0, labelFormatter: function() { return " "; } },
                toolTip: { content: "<div class='wordwrap'>{label}: {y}</div>" },
                data: [{
                  type: "area",
                  indexLabel: "{y}",
                  indexLabelPlacement: "inside",
                  indexLabelFontWeight: "bolder",
                  indexLabelFontColor: "black",
                  dataPoints: res.data.sim
                }]
              });
              trendingSimChart.render();
          }
          /* TOP LEARNER */
          if (res.data.learner.length) {
            var learnerChart = new CanvasJS.Chart("top_learner", {
                colorSet: "colorcode",
                title: { text: "TOP 5 LEARNERS"},
                animationEnabled: true,
                theme: "light2",
                backgroundColor: '#fff',
                axisX: { gridThickness: 0, tickLength: 0, lineThickness: 0, labelFontColor: "black", labelFormatter: function() { return " "; } },
                axisY: { gridThickness: 0, tickLength: 0, lineThickness: 0, labelFormatter: function() { return " "; } },
                toolTip: { content: "<div class='wordwrap'>{label}: {s}: {y} / {p}</div>" },
                data: [{
                  type: "column",
                  indexLabel: "{y}",
                  indexLabelPlacement: "inside",
                  indexLabelFontWeight: "bolder",
                  indexLabelFontColor: "white",
                  dataPoints: res.data.learner
                }]
              });
              learnerChart.render();
          }
          /* LEADING GROUPS */
          if (res.data.group.length) {
            var groupChart = new CanvasJS.Chart("lead_group", {
                colorSet: "colorcode",
                title: { text: "LEADING GROUPS"},
                animationEnabled: true,
                theme: "light2",
                backgroundColor: '#fff',
                axisX: { gridThickness: 0, tickLength: 0, lineThickness: 0, labelFontColor: "black", labelFormatter: function() { return " "; } },
                axisY: { gridThickness: 0, tickLength: 0, lineThickness: 0, labelFormatter: function() { return " "; } },
                toolTip: { content: "<div class='wordwrap'>{name}: {y}</div>" },
                data: [{
                  type: "line",
                  indexLabel: "{y}",
                  indexLabelPlacement: "inside",
                  indexLabelFontWeight: "bolder",
                  indexLabelFontColor: "black",
                  dataPoints: res.data.group
                }]
              });
              groupChart.render();
          }
          /* TOP COMPETENCE */
          if (res.data.competency.length) {
            var topCompChart = new CanvasJS.Chart("top_comp", {
                colorSet: "colorcode",
                title: { text: "TOP COMPETENCE"},
                animationEnabled: true,
                theme: "light2",
                backgroundColor: '#fff',
                axisX: { gridThickness: 0, tickLength: 0, lineThickness: 0, labelFontColor: "black", labelFormatter: function() { return " "; } },
                axisY: { gridThickness: 0, tickLength: 0, lineThickness: 0, labelFormatter: function() { return " "; } },
                toolTip: { content: "<div class='wordwrap'>{name}: {y}</div>" },
                data: [{
                  type: "doughnut",
                  indexLabel: "{y}",
                  indexLabelPlacement: "inside",
                  indexLabelFontWeight: "bolder",
                  indexLabelFontColor: "white",
                  dataPoints: res.data.competency
                }]
              });
              topCompChart.render();
          }
        }
      },error: function() {
        $.LoadingOverlay("hide");
        swal({text: 'Oops. something went wrong please try again.', buttons: false, icon: "error", timer: 2000});
      }
    });
  }
  
  var date = $('#reportrange').val();

  defaultGraphData(date);
  
  $('#reportrange').on('change', function() {
	  var date = $(this).val();
	  $.LoadingOverlay("show");
	  defaultGraphData(date);
  });
});
var UsedHostingData = new CanvasJS.Chart("UsedHostingData", {
      colorSet: "colorcode",
      backgroundColor: "",
      animationEnabled: true,
      theme: "light2", // "light1", "light2", "dark1", "dark2"
      data: [{
        type: "doughnut",
        showInLegend: false,
        toolTipContent: "{legendText}",
        indexLabelFontSize: 10,
        dataPoints: <?php echo json_encode($hdata['dataPoints'], JSON_NUMERIC_CHECK); ?>
      }]
    });
    UsedHostingData.render();
    var total = '<?php echo $hdata['total_size'] ?>';
    UsedHostingData.set("title", {text: total, verticalAlign: "center"});
</script>
<?php 
require_once 'includes/footer.php';
