<?php 
session_start();
if (isset($_SESSION['username'])):
	$user	= $_SESSION['username'];
    $role   = $_SESSION['role'];
    $userid = $_SESSION['userId'];
else:
	die;
endif;
require_once 'config/db.class.php';
$db			= new DBConnection;
$getid		= (isset($_POST['data-id'])) ? $_POST['data-id'] : '';
$getdata	= ( ! empty($getid)) ? $db->getScenario($getid) : '';
$simType	= ( ! empty($getdata['scenario_type'])) ? explode(',', $getdata['scenario_type']) : '';
$authorId	= ( ! empty($getdata['assign_author'])) ? explode(',', $getdata['assign_author']) : '';
$reviewerId	= ( ! empty($getdata['assign_reviewer'])) ? explode(',', $getdata['assign_reviewer']) : '';
?>
<div id="load_popup_modal_contant" class="" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Update Simulation</h4>
            </div>
            <div class="modal-body">
            	<form enctype="multipart/form" name="edit_Simulation_form" id="edit_Simulation_form" method="post">
                	<div class="usermanage-form">
                    	<div class="row">
                    		<div class="col-sm-12">
                                <div class="col-sm-4">
                                	<div class="form-group">
                                    	<input type="text" class="form-control" name="scenario_name" id="scenario_name" placeholder="Simulation Name" required="required" value="<?php echo $getdata['Scenario_title'] ?>">
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group  form-select">
                                        <select class="form-control" name="scenario_category" id="scenario_category" required="required" data-placeholder="Scenario Category *">
                                        <option value="0" selected>Select Category *</option>
                                        <?php foreach ($db->getCategory() as $category): ?>
                                        <option value="<?php echo $category['cat_id'] ?>" <?php echo ($category['cat_id'] == $getdata['scenario_category']) ? 'selected="selected"' : ''; ?>><?php echo $category['category'] ?></option>
                                        <?php endforeach; ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group form-select">
                                        <select class="form-control multiselect-ui selection" multiple="multiple" name="scenario_type[]" id="scenario_type" required="required" data-placeholder="Scenario Type *">
                                        <?php foreach ($db->getScenarioTypes() as $types): ?>
                                        <option value="<?php echo $types['st_id'] ?>" <?php if ( ! empty($simType) && array_search($types['st_id'], $simType) !== FALSE) { echo 'selected'; } ?>><?php echo $types['type_name'] ?></option>
                                        <?php endforeach; ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group form-select">
                                        <select class="form-control multiselect-ui selection" name="author[]" id="author" multiple="multiple" data-placeholder="Author Name List">
                                        <?php foreach ($db->getUserByRole("'author'", NULL) as $author): ?>
                                        <option value="<?php echo $author['id'] ?>" <?php if ( ! empty($authorId) && array_search($author['id'], $authorId) !== FALSE) { echo 'selected'; } ?>><?php echo $author['username'] ?></option>
                                        <?php endforeach; ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group form-select">
                                        <select class="form-control multiselect-ui selection" name="reviewer[]" id="reviewer" multiple="multiple" data-placeholder="Reviewer Name List">
                                        <?php foreach ($db->getUserByRole("'reviewer'", NULL) as $reviewer): ?>
                                        <option value="<?php echo $reviewer['id'] ?>" <?php if ( ! empty($reviewerId) && array_search($reviewer['id'], $reviewerId) !== FALSE) { echo 'selected'; } ?>><?php echo $reviewer['username'] ?></option>
                                        <?php endforeach; ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <div class="input-group date form_datetime" data-date-format="yyyy-mm-dd" data-link-field="dtp_input1">
                                            <input class="form-control" name="start_date" type="text" readonly placeholder="Start Date" value="<?php echo ($getdata['assign_start_date'] != '0000-00-00') ? $getdata['assign_start_date'] : '' ?>">
                                            <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <div class="input-group date form_datetime" data-date-format="yyyy-mm-dd" data-link-field="dtp_input1">
                                            <input class="form-control" name="end_date" type="text" readonly placeholder="End Date" value="<?php echo ($getdata['assign_end_date'] != '0000-00-00') ? $getdata['assign_end_date'] : '' ?>">
                                            <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                        </div>
                                    </div>
                                </div>
                    		</div>
                   		</div>
                    </div>
                	<div class="modal-footer">
                    	<input type="hidden" name="simulation_update" value="1" />
						<input type="hidden" name="sim_id" value="<?php echo $getdata['scenario_id']; ?>" />
                        <button type="submit" name="editScenarioStatus" id="editScenarioStatus" class="btn btn-primary" onClick="return confirm('Are you sure to update this Simulation.?');">Update</button>
						<button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                    </div>
                </form>
            </div>
        </div>
     </div>
</div>
<script type="text/javascript">
$(function() {
	$('.multiselect-ui').multiselect({
		includeSelectAllOption: true,
		filterPlaceholder: 'Search & select users',
		enableCaseInsensitiveFiltering : true,
		enableFiltering: true,
		buttonWidth: '250px',
		maxHeight: 250
	})
	$('.form_datetime').datetimepicker({
		weekStart: 1,
		todayBtn:  0,
		autoclose: 1,
		todayHighlight: 1,
		startView: 2,
		forceParse: 0,
		minView: 2,
		showMeridian: 1,
		clearBtn: 1
	});
});
$("#edit_Simulation_form").on('submit', (function(e) {
	e.preventDefault();
	var form_data = $(this).serialize();
	var post_url  = 'includes/process.php';
	$.LoadingOverlay("show");
	$.ajax({
			url: post_url,
			type: "POST",
			data: form_data,
			success: function(result) {
				var res = $.parseJSON(result);
				$.LoadingOverlay("hide");
				if (res.success == true) {
					swal({text: res.msg, buttons: false, icon: "success", timer: 1000});
					setTimeout(function() { window.location.reload(); }, 2000);
				}
				else if (res.success == false) {
					swal({text: res.msg, buttons: false, icon: "error", timer: 1000});
					setTimeout(function() { window.location.reload(); }, 2000);
				}
			},error: function() {
				swal({text: 'Oops, something went wrong. Please try again later', buttons: false, icon: "error", timer: 1000});
			}
		});
	}));
</script>
<?php 
ob_end_flush();
