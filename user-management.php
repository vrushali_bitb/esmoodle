<?php 
session_start();
if (isset($_SESSION['username'])) {
	$user		= $_SESSION['username'];
	$role		= $_SESSION['role'];
	$userid		= $_SESSION['userId'];
	$client_id	= $_SESSION['client_id'];
}
else {
    header('location: index.php');
}
require_once 'config/db.class.php';
require_once 'config/pagination.php';
require_once 'includes/learners_menu.php';
require_once 'includes/mailer/Send_Mail.php';
$db  		= new DBConnection();
$msg 		= '';
$success 	= '';
$curdate	= date('Y-m-d H:i:s a');

if (isset($_GET['delete']) && ! empty($_GET['uid'])):
	$id   = (isset($_GET['uid'])) ? $_GET['uid'] : '';
	$sql  = "DELETE t1.*, t2.*, t3.*, t4.*, t5.* ";
	$sql .= "FROM users t1 
			 LEFT JOIN scenario_attempt_tbl t2 ON t1.id = t2.userid 
			 LEFT JOIN score_tbl t3 ON t2.uid = t3.uid 
			 LEFT JOIN multi_score_tbl t4 ON t2.uid = t4.uid 
			 LEFT JOIN open_res_tbl t5 ON t2.uid = t5.uid 
			 WHERE MD5(t1.id) = ?";
    $q = $db->prepare($sql);
	if ($q->execute(array($id))):
		$msg = 'User deleted successfully.';
	endif;
endif;

if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['reg_user'])):
	extract($_POST);
	$permission	= $db->getClientPermission($client_id);
	$totalUser 	= $db->getTotalUsers($role);
	$allow_user	= '';
		
	if ( ! empty($role) && $role == 'admin'):
		$allow_user = ( ! empty($permission['admin'])) ? $permission['admin'] : '';
	elseif ( ! empty($role) && $role == 'author'):
		$allow_user = ( ! empty($permission['auth'])) ? $permission['auth'] : '';
	elseif ( ! empty($role) && $role == 'reviewer'):
		$allow_user = ( ! empty($permission['reviewer'])) ? $permission['reviewer'] : '';
	elseif ( ! empty($role) && $role == 'learner'):
		$allow_user = ( ! empty($permission['learner'])) ? $permission['learner'] : '';
	elseif ( ! empty($role) && $role == 'educator'):
		$allow_user = ( ! empty($permission['educator'])) ? $permission['educator'] : '';
	endif;

	if (empty($username) || empty($email) || empty($role)):
		$msg 	 = 'please enter all required fields.';
		$success = 'false';
	elseif ( ! empty($username) && ! empty($role) && $db->checkUserExist($username, $role) == TRUE):
		$msg 	 = 'User with role already exists. try another.';
		$success = 'false';
	elseif ( ! empty($email) && $db->checkEmail($email) == FALSE):
		$msg 	 = 'Please enter valid email-id.';
		$success = 'false';
	elseif ( ! empty($allow_user) && $totalUser >= $allow_user):
		$msg 	 = 'User with role creation limit full.';
		$success = 'false';
	elseif (empty($msg)):
		$npwd = 'ES-'. $db->get_random_string(6);
		$data = array('uname'			=> $username,
					  'email'			=> $email,
					  'pwd'				=> md5($npwd),
					  'role' 			=> $role,
					  'location'		=> ( ! empty ($location)) ? $location : '',
					  'company' 		=> ( ! empty ($company)) ? $company : '',
					  'department'		=> ( ! empty ($department)) ? $department : '',
					  'date_of_join'	=> ( ! empty ($join_date)) ? $join_date : '',
					  'status'			=> $status,
					  'pwd_date'		=> date('Y-m-d'),
					  'date'			=> $curdate);
		$sql = "INSERT INTO `users` (`username`, `email`, `password`, `role`, `location`, `company`, `department`, `date_of_join`, `status`, `last_change_pwd`, `date`) VALUES (:uname, :email, :pwd, :role, :location, :company, :department, :date_of_join, :status, :pwd_date, :date)";
		$results = $db->prepare($sql);
		if ($results->execute($data)):
			$getTempData = $db->getEmailTemplate(NULL, 'new_user');
			#-----Email-template----------------
			$template = $getTempData['tmp_des'];
			$subject  = $getTempData['subject'];
			$values   = array('full_name'	=> ucwords($username),
							  'name'		=> $username,
							  'pwd'			=> $npwd,
							  'logo'		=> TEMPLATE_HEADER_LOGO,
							  'url'			=> LOGIN_URL,
							  'cmail'		=> CONTACT_EMAIL,
							  'tname'		=> FTITLE,
							  'subject'		=> $subject);
			sendMail($email, $template, $values, $subject);
			$msg 	 = 'User registered successfully. please check mail for system generated password.';
			$success = 'true';
		else:
			$msg 	 = 'User not registered. please try again later.';
			$success = 'false';
		endif;
	endif;
endif;

#----------Serch----------------
$search = '';
if (isset($_GET['search']) && ! empty($_GET['search'])):
	extract($_GET);
	$search = " AND ( username LIKE '%$search%' OR fname LIKE '%$search%' OR lname LIKE '%$search%' OR email LIKE '%$search%' OR mob LIKE '%$search%' OR role LIKE '%$search%' OR location LIKE '%$search%' OR company LIKE '%$search%' OR department LIKE '%$search%' OR designation LIKE '%$search%' )";
endif;
#----------Sort by----------------
if (isset($_GET['orderby']) && $_GET['orderby'] == 'role'):
	$norderby =	(( ! empty($search)) ? $search : " ")." ORDER BY role";
elseif (isset($_GET['orderby']) && $_GET['orderby'] == 'status'):
	$norderby =	(( ! empty($search)) ? $search : " ")." ORDER BY status";
else:
	$norderby =	(( ! empty($search)) ? $search : " ")." ORDER BY last_login, id DESC";
endif;
$sqlr 		= "SELECT * FROM users WHERE 1=1 $norderby";
$q 			= $db->prepare($sqlr); $q->execute();
$rowCount	= $q->rowCount();
$count 		= ($sqlr) ? $rowCount : 0;
$page  		= (isset($_REQUEST['page']) && ! empty($_REQUEST['page'])) ? $_REQUEST['page'] : 1;
$show  		= 15;
$start 		= ($page - 1) * $show;
$pagination	= getPaginationString($count, $show, $page, 1, basename(__FILE__), '?page=');
$sqlr 	   .= " LIMIT $start, $show";
$results = $db->prepare($sqlr); ?>
<div class="bottomheader">
    <div class="container-fluid">
        <ul class="breadcrumb">
			<?php echo $db->breadcrumbs(); ?>
         </ul>
     </div>
</div>
<div class="user_managment">
    <div class="container-fluid">
		<div class="row">
			<div class="col-sm-12">
			<?php if ( ! empty($msg)): ?>
			<div class="alert <?php echo ($success == 'true') ? 'alert-success' : 'alert-danger' ?> alert-dismissable"><?php echo $msg; ?></div>
			<?php endif; ?>
			<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
				<div class="panel panelCaollapse">
					<div class="panel-heading" role="tab" id="headingOne">
						<h4 class="panel-title">
							<a  class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
								Import Bulk User 
							</a>
						</h4>
					</div>
					<div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
						<div class="panel-body">
							<div class="usermanage-form">
								<div class="row">
									<div class="addsce-img">
										<div class="adimgbox">                               
											<div class="form-group">
												<input type="file" name="file" id="file" class="form-control file">
												<input type="text" class="form-control controls" disabled placeholder="Upload CSV file">
											</div>
											<a href="<?php echo $db->getBaseUrl('user_data_file/users-import-file-example.csv'); ?>"> Download Sample CSV File</a>
										</div>
										<div class="adimgbox browsebtn">
											<button class="browse btn btn-primary" type="button">Browse</button>
										</div>
										<div class="adimgbox browsebtn">
											<button type="button" name="upload" id="upload" class="btn btn-primary">Uplaod</button>
										</div>
									</div>
								</div>                        
							</div>
						</div>
					</div>
				</div>
				
				<div class="panel panelCaollapse">
					<div class="panel-heading" role="tab" id="headingTwo">
						<h4 class="panel-title">
							<a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
								Add User
							</a>
						</h4>
					</div>
					<div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
						<div class="panel-body">
							<form method="post">
								<div class="usermanage-form">
									<div class="row">
										<div class="col-sm-6">
											<div class="form-group">
												<input type="text" class="form-control" id="username" onchange="validateUserName();" name="username" placeholder="User Name" required value="<?php echo @$username; ?>">
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-sm-3">  
											<div class="form-group">
												<input type="text" class="form-control" id="location" placeholder="Enter location" name="location" required value="<?php echo @$location; ?>">
											</div>
										</div>
										<div class="col-sm-3">
											<div class="form-group">
												<input type="text" class="form-control" id="company" placeholder="Enter company" name="company" required value="<?php echo @$company; ?>">
											</div>
										</div>
										<div class="col-sm-3">
											<div class="form-group">
												<input type="text" class="form-control" id="department" placeholder="Enter unit" name="department" required value="<?php echo @$department; ?>">
											</div>
										</div>
										<div class="col-sm-3">
											<div class="form-group form-select">
												<?php 
												$roles = $db->role;
												if (array_key_exists('clientAdmin', $roles)) {
													unset($roles['clientAdmin']);
												} ?>
												<select class="form-control" name="role" id="role" data-placeholder="Role">
													<option value="" selected="selected" hidden="">Select Role</option>
													<?php foreach ($roles as $roleKey => $roleVal): ?>
													 <option value="<?php echo $roleKey; ?>"><?php echo $roleVal; ?></option>
													<?php endforeach; ?>
												</select>
											</div>
										</div>
										<div class="col-sm-3">
											<div class="form-group">
												<div class="input-group date form_datetime" data-date-format="yyyy-mm-dd" data-link-field="dtp_input1">
													<input class="form-control" name="join_date" type="text" readonly placeholder="Date of Registration">
													<span class="input-group-addon"><span class="fa fa-calendar"></span></span>
												</div>
											</div>
										</div>
										<div class="col-sm-3">
											<div class="form-group">
												<input type="email" class="form-control" id="email" placeholder="Enter email" name="email" required value="<?php echo @$email; ?>">
											</div>
										</div>
										<div class="col-sm-3">
											<div class="form-group form-select">
												<select class="form-control" name="status">
													<option selected="selected" hidden="">Status</option>
													<option value="1">Active</option>
													<option value="0">Inactive</option>
												</select>
											</div>
										</div>
									</div>
								</div>
								<div class="usermanage-add clearfix"><button type="submit" name="reg_user" id="reg_user" class="btn btn-primary">Add User</button></div>
							</form>
						</div>
					</div>
				</div>
			</div>
			<div class="usermanage_main">
				<div class="table-box">
					<div class="tableheader clearfix">
					   <div class="tableheader_left">List of Users</div>
						<div class="tableheader_right">
							<div class="Searchbox">
								<form method="get">
									<input type="text" class="serchtext" name="search" placeholder="Search" required>
									<div class="serchBTN">
										<img class="img-responsive" src="img/dash_icon/search.svg">
									</div>
								</form>
							</div>
							<div class="btn-group btn-shortBY">
								<button type="button" class="btn btn-Transprate"><span class="sort">Sort by</span></button>
								<button type="button" class="btn btn-Transprate dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img  src="img/dash_icon/down_arrow.svg"></button>
								<div class="dropdown-menu shortBYMenu">
									<a class="dropdown-item" href="<?php echo $db->getBaseUrl($db->getFile()); ?>">Default Sorting</a>
									<a class="dropdown-item" href="<?php echo $db->getBaseUrl($db->getFile().'?orderby=role'); ?>">Role</a>
									<a class="dropdown-item" href="<?php echo $db->getBaseUrl($db->getFile().'?orderby=status'); ?>">Status</a>
								</div>
							</div>
						</div>
					</div>                
				</div>
				<div class="usermanage-form1">
				   <div class="table-responsive">
						<form name="user_form" id="user_form">
						<table class="table table-striped">
							<thead class="Theader">
								<tr>
									<th><label><input class="checked_all" type="checkbox"> All</label></th>
									<th>#</th>
									<th>Username</th>
									<th>Location</th>
									<th>Company</th>
									<th>Unit</th>
									<th>Role</th>
									<th>Designation</th>
									<th>Date of Registration</th>
									<th>Email</th>
									<th>Email Status</th>
									<th>Status</th>
									<th>Edit</th>    
									<th>Delete</th>                                                                                 
								</tr>
							</thead>
							<tbody>
							<?php if ($results->execute() && $rowCount > 0):
							$i = $start + 1;
							foreach ($results->fetchAll(PDO::FETCH_ASSOC) as $row): ?>
							<tr <?php echo (empty($row['last_login'])) ? 'style="background-color:#fae7b4"' : ''; ?>>
								<td><?php if ($row['role'] != 'admin' && $userid != $row['id']): ?>
									<input type="checkbox" name="id[]" class="checkbox" value="<?php echo $row['id'] ?>" />
									<?php endif; ?>
								</td>
								<td><?php echo $i ?></td>
								<td><?php echo $row['username'] ?></td>
								<td><?php echo ( ! empty($row['location'])) ? $row['location'] : '--' ?></td>
								<td><?php echo ( ! empty($row['company'])) ? $row['company'] : '--' ?></td>
								<td><?php echo ( ! empty($row['department'])) ? $row['department'] : '--' ?></td>
								<td><strong><?php echo $row['role'] ?></strong></td>
								<td><?php echo ( ! empty($row['department'])) ? $row['department'] : '--' ?></td>
								<td><?php echo ($row['date_of_join'] != '0000-00-00') ? $db->dateFormat($row['date_of_join'], 'd-M-y') : '--'; ?></td>
								<td><?php echo $row['email'] ?></td>
								<td><?php echo ( ! empty($row['email_status'])) ? '<i class="fa fa-check-circle-o fa-2x" aria-hidden="true" title="Mail sent"></i>' : '<i class="fa fa-times-circle-o fa-2x" title="Mail not sent" aria-hidden="true"></i>'; ?></td>
								<td><?php echo ( ! empty($row['status'])) ? 'Active' : 'Inactive'; ?></td>
								<td><a class="tooltiptop edit" href="edit-user.php?edit=true&uid=<?php echo md5($row['id']) ?>">
									<span class="tooltiptext">Edit</span>
									<img class="svg" src="img/list/edit.svg"></a></td>
								<td><?php if ($row['role'] != 'admin' && $userid != $row['id']): ?>
									<a href="<?php echo basename(__FILE__) ?>?delete=true&uid=<?php echo md5($row['id']) ?>" class="tooltiptop" onclick="return confirm('Are you sure to delete.?');">
									<span class="tooltiptext">Delete</span>
									<img class="svg" src="img/list/delete.svg"></a>
									<?php else: ?>
									<a class="disabled" href="JavaScript:void(0);"><img class="svg" src="img/list/delete.svg"></a>
									<?php endif; ?>
								</td>
							</tr>
							<?php $i++; endforeach; else: ?>
							<tr>
								<td colspan="13" style="text-align:center"><strong>No data available in database.</strong></td>
							</tr>
							<?php endif; ?>
							</tbody>
						</table>
						<div class="usErManAge">			
							<button type="button" id="delete_Multiple_Users" class="btn btn-primary">Delete Multiple Users</button>
							<button type="button" id="send_mail_Multiple_Users" class="btn btn-primary">Send New Account Mail</button>
						</div>
						</form>
				   </div>
				   <div class="pull-right">
					<nav aria-label="navigation"><?php echo ( ! empty($pagination) ) ? $pagination : '&nbsp;'; ?></nav>
				   </div>
				</div>
		   </div>
			</div>
		</div>
	</div>
</div>
<style>
a.disabled {
	color: currentColor;
	cursor: not-allowed;
	opacity: 0.2;
	text-decoration: none;
}
.table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th {
    padding: 10px;
}
</style>
<script type="text/javascript" src="content/js/bootstrap-datetimepicker.js" charset="UTF-8"></script>
<script type="text/javascript">
function validateUserName() {
    var val = document.getElementById('username').value;
	if ( ! val.match(/^[a-zA-Z0-9]+$/)) {
       swal({text: 'Only alphabets and numbers are allowed.', buttons: false, icon: "warning", timer: 2000});
	   $('#reg_user').attr("disabled", true);
	   $('#username').focus();
    }
	else {
		$('#reg_user').attr("disabled", false);
	}
}

$('.checked_all').on('change', function() {
	$('.checkbox').prop('checked', $(this).prop("checked"));
});

$('.checkbox').change(function() {
	if ($('.checkbox:checked').length == $('.checkbox').length) {
		$('.checked_all').prop('checked', true);
	}
	else {
		$('.checked_all').prop('checked', false);
	}
});

$('.form_datetime').datetimepicker({
	weekStart: 1,
	todayBtn:  0,
	autoclose: 1,
	todayHighlight: 1,
	startView: 2,
	forceParse: 0,
	minView: 2,
	showMeridian: 1,
	clearBtn: 1
});

$('#upload').click(function() {
	$(this).attr('disabled', 'disabled').html('<i class="fa fa-refresh fa-spin fa-fw"></i><span class="sr-only">Loading...</span> Please wait....');
	var file_data = $('#file').prop('files')[0];
	var form_data = new FormData();
	form_data.append('file', file_data);
	form_data.append('import_bulk_user', true);
	$.ajax({
		url: "import-csv-file.php",
		type: "POST",
		data: form_data,
		contentType: false,
		cache: false,
		processData:false,
		success: function(resdata) {
			var res = $.parseJSON(resdata);
			if (res.success == true) {
				swal({text: res.msg, buttons: false, icon: "success", timer: 1000});
				$('#upload').html('Upload').removeAttr('disabled', 'disabled');
				setTimeout(function() { window.location.reload(); }, 500);
			}
			else if (res.success == false) {
				swal({text: res.msg, buttons: false, icon: "error", timer: 1000});
				$('#upload').html('Upload').removeAttr('disabled', 'disabled');
			}
		},error: function() {
			swal({text: 'Oops, something went wrong. Please try again later', buttons: false, icon: "error", timer: 1000 });
		}
	});
});
	
$('#delete_Multiple_Users').click(function() {
	swal({
		title: "Are you sure?",
		text: "Delete user.",
		icon: "warning",
		buttons: [true, 'Delete'],
		dangerMode: true, }).then((willDelete) => { if (willDelete) {
			$.LoadingOverlay("show");
			var formData = new FormData(document.getElementsByName('user_form')[0]);
			formData.append('multiple_user_type', 1);
			$.ajax({
				url: 'includes/process.php',
				type: "POST",
				data: formData,
				contentType: false,
				cache: false,
				processData:false,
				success: function(resdata) {
					var res = $.parseJSON(resdata);
					if (res.success == true) {
						$.LoadingOverlay("hide");
						swal({text: res.msg, buttons: false, icon: "success", timer: 2000});
						setTimeout(function() { window.location.reload(); }, 500);
					}
					else if (res.success == false) {
						swal({text: res.msg, buttons: false, icon: "warning", timer: 2000});
						$.LoadingOverlay("hide");
					}
				}, error: function() {
					swal({text: 'Oops. something went wrong please try again.', buttons: false, icon: "error", timer: 2000});
					$.LoadingOverlay("hide");
				}
			});
		}
	});
});

$('#send_mail_Multiple_Users').click(function() {
	swal({
		title: "Are you sure?",
		text: "Sent New Account Mail",
		icon: "warning",
		buttons: [true, 'Send'],
		dangerMode: true, }).then((willDelete) => { if (willDelete) {
			$.LoadingOverlay("show");
			var formData = new FormData(document.getElementsByName('user_form')[0]);
			formData.append('send_mail_Multiple_Users', 1);
			$.ajax({
				url: 'includes/process.php',
				type: "POST",
				data: formData,
				contentType: false,
				cache: false,
				processData:false,
				success: function(resdata) {
					var res = $.parseJSON(resdata);
					if (res.success == true) {
						$.LoadingOverlay("hide");
						swal({text: res.msg, buttons: false, icon: "success", timer: 2000});
					}
					else if (res.success == false) {
						swal({text: res.msg, buttons: false, icon: "warning", timer: 2000});
						$.LoadingOverlay("hide");
					}
				}, error: function() {
					swal({text: 'Oops. something went wrong please try again.', buttons: false, icon: "error", timer: 2000});
					$.LoadingOverlay("hide");
				}
			});
		}
	});
});
</script>
<?php 
require_once 'includes/footer.php';
