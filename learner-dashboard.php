<?php
@ob_start(); 
@session_start();

if (isset($_SESSION['username'])) {
	$user   	= $_SESSION['username'];
	$role   	= $_SESSION['role'];
	$userid		= $_SESSION['userId'];
	$client_id	= (isset($_SESSION['client_id'])) ? $_SESSION['client_id'] : exit('Oops. something went wrong please try again');

} else {
	header('location: index.php');
}
require_once 'config/db.class.php';
require_once 'includes/learners_menu.php';
$db 			= new DBConnection();
$totalSim		= ( ! empty($userid)) ? $db->getTotalLearnerSIM($userid) : 0;
$totalAttSim	= ( ! empty($userid)) ? $db->getTotalAttemptedSim($userid) : 0;
$totalIncSim	= ( ! empty($userid)) ? $db->getIncompleteLearnerSIM($userid) : 0;
$sim_list		= ( ! empty($userid)) ? $db->getAssignSimData($userid) : 0;
?>
<style type="text/css">
	body {
		background: #e8e8e8;
		overflow-x: hidden
	}
	.flexItem.flexItemsdMinDash {
		margin-bottom: 5px;
	}
	.Learnar-dashboard-E2 {
		margin-top: 90px;
	}
	.Learner-dashboard-E3{
		margin-top: 10px;
	}

	a:focus {
		outline: unset;
		outline: unset;
		outline-offset: unset;
	}

	.flexItem .item {
		margin: 0px;
	}

	.col-container {
		padding-right: 5px;
	}

	.hometab.active,
	.menu1tab.active {
		display: block !important;
	}

	.owl-nav {
		display: none;
	}

	.owl-dots {
		display: block;
		margin-top: 5px;
	}
	.owl-theme .owl-dots .owl-dot.active span{		
		background: #000 !important;
	}

	.owl-theme .owl-dots .owl-dot span {
		width: 13px !important;
		height: 13px !important;
		border: 1px solid #000;
		background: transparent !important;
		border-radius:50% !important;
	}
	.flexItemsdMinDash .item p {
		font-weight: bold;
		padding-bottom: 5px;
		margin-bottom: 0px;
		padding-top: 5px;
		padding-left: 5px;
		padding-right: 5px;
	}
	.L-E-text .svg {
		width: 30px;
		margin-left: 10px;
		margin-top: 5px;
	}
	.L-calender input#reportrange {
		background: #bfbfbf00;
		cursor:pointer;
		border-bottom: 1px solid #000;
	}
</style>
<script type="text/javascript">
	$.LoadingOverlay("show");
</script>
<link rel="stylesheet" href="content/js/daterangepicker/daterangepicker.css" />
<link rel="stylesheet" href="content/css/owlcarousel/owl.carousel.min.css" />
<link rel="stylesheet" href="content/css/owlcarousel/owl.theme.default.css" />
<div class="<?php echo (isset($_SESSION['autologin'])) ? 'Learner-dashboard-E3' : 'Learnar-dashboard-E2'?> LearnarDSHB">
	<div class="container-fluid">
		<div class="LE2-top clearfix">
			<div class="leartop_Box">
				<?php if ( ! empty($getUsers)): ?>
				<h3><?php echo ucwords($getUsers['fname'] . ' ' . $getUsers['lname']); ?>: <?php echo $getUsers['department'] ?></h3>
				<?php endif; ?>
			</div>
			<div class="leartop_Box L-calender">
				<div class="input-group">
					<span class="input-group-addon"><span class="fa fa-calendar"></span></span>
					<input type="text" id="reportrange" class="form-control" readonly="readonly" />
					<input type="hidden" id="date">
					<span class="spcarot"><i class="fa fa-caret-down"></i></span>
				</div>
			</div>
			<div class="leartop_Box ltopselecton clearfix ltopdropdown">
				<select class="form-control multiselect-ui selection sim_list" id="top_filter" data-placeholder="Search & Select Simulations" multiple="multiple">
				</select>
			</div>
			<div class="leartop_Box L-E-text learner_download_btn_Laptop">
				<span class="align-middle">
					<a class="" id="print" title="Download Dashboard Report" onclick="getPDF('#print_data')"><img class="svg" src="img/list/download_report.svg"/></a>
				</span>
			</div>
		</div>
	</div>
	<div class="container-fluid Lear-fluid">		
		<div class="flexItem flexItemsdMinDash">
			<div class="flextlistbaner hometab clearfix owl-carousel owl-theme active">
				<div class="item">
					<ul class="LHBanner">
						<li>Performance</li>
						<li><img class="LHimg" src="img/dash_icon/l_Dash_performance.svg"></li>
					</ul>
					<div class="item learnerITEM">
						<div class="LERneRboxtest LEarbox4-5">
							<ul>
								<li>Total Attempts</li>
								<li class="LEAR24 LEARBlack L-Ralign" id="att_sim">0</li>
							</ul>
							<ul>
								<li>1<sup>st</sup> Attempt</li>
								<li class="LEAR24 LEARGreen L-Ralign" id="pass_1st">--</li>
							</ul>
							<ul>
								<li>Last Attempt</li>
								<li class="LEAR24 LEARGreen L-Ralign" id="pass_2nd">--</li>
							</ul>
							<ul>
								<li>Remediation Required</li>
								<li class="LEAR24 LEARBlack L-Ralign" id="fail">0</li>
							</ul>
						</div>
					</div>
				</div>				
				<div class="item">
					<ul class="LHBanner">
						<li>Decision Promptness</li>
						<li><img class="LHimg" src="img/dash_icon/l_Dash_decision.svg"></li>
					</ul>					
					<div class="item learnerITEM">
						<div class="LERneRboxtest LERneRboxAling LEARavatime">
							<ul>
								<li><img class="img-fluid LDtopsvg" src="img/dash_icon/my_time.svg"></li>
								<li class="LEAR24 LEARGreen L-Ralign" id="my_avg_time">00:00:00</li>
							</ul>
							<ul>
								<li><img class="img-fluid LDtopsvg" src="img/dash_icon/group_time.svg"></li>
								<li class="LEAR24 LEARBlack L-Ralign" id="other_avg_time">00:00:00</li>
							</ul>
						</div>
					</div>
				</div>
				<div class="item">
					<ul class="LHBanner">
						<li>Developed Competencies</li>
						<li><img class="LHimg" src="img/dash_icon/l_Dash_S_com.svg"></li>
					</ul>
					<div class="item learnerITEM learnerITEMscroll">
						<div class="LERneRboxtest LEarbox4-5">
							<ul id="StrongComp1" style="display:none;">
								<li></li>
								<li class="LEAR24 LEARBlack L-Ralign"></li>
							</ul>
							<ul id="StrongComp2" style="display:none;">
								<li></li>
								<li class="LEAR24 LEARBlack L-Ralign"></li>
							</ul>
							<ul id="StrongComp3" style="display:none;">
								<li></li>
								<li class="LEAR24 LEARBlack L-Ralign"></li>
							</ul>
							<ul id="StrongComp4" style="display:none;">
								<li></li>
								<li class="LEAR24 LEARBlack L-Ralign"></li>
							</ul>
							<ul id="StrongComp5" style="display:none;">
								<li></li>
								<li class="LEAR24 LEARBlack L-Ralign"></li>
							</ul>
							<ul id="StrongComp6" style="display:none;">
								<li></li>
								<li class="LEAR24 LEARBlack L-Ralign"></li>
							</ul>
						</div>
					</div>
				</div>
				<div class="item">
					<ul class="LHBanner">
						<li>Developing Competencies</li>
						<li><img class="LHimg" src="img/dash_icon/l_Dash_W_com.svg"></li>
					</ul>
					<div class="item learnerITEM learnerITEMscroll">
						<div class="LERneRboxtest LEarbox4-5">
							<ul id="WeakComp1" style="display:none;">
								<li></li>
								<li class="LEAR24 LEARBlack L-Ralign"></li>
							</ul>
							<ul id="WeakComp2" style="display:none;">
								<li></li>
								<li class="LEAR24 LEARBlack L-Ralign"></li>
							</ul>
							<ul id="WeakComp3" style="display:none;">
								<li></li>
								<li class="LEAR24 LEARBlack L-Ralign"></li>
							</ul>
							<ul id="WeakComp4" style="display:none;">
								<li></li>
								<li class="LEAR24 LEARBlack L-Ralign"></li>
							</ul>
							<ul id="WeakComp5" style="display:none;">
								<li></li>
								<li class="LEAR24 LEARBlack L-Ralign"></li>
							</ul>
							<ul id="WeakComp6" style="display:none;">
								<li></li>
								<li class="LEAR24 LEARBlack L-Ralign"></li>
							</ul>
						</div>
					</div>
				</div>
				<div class="item">
					<ul class="LHBanner">
						<li>Job Readiness</li>
						<li><img class="LHimg" src="img/dash_icon/l_Dash_Job.svg"></li>
					</ul>
					<div class="item learnerITEM">
						<div class="LERneRboxtest LEarbox4-5">
							<ul>
								<li><div class="jobRed"><img class="Job_Redimg" src="img/dash_icon/lob_readiness.svg"><span class="LEAR36 LEARGreen" id="job_readiness">0</span></div></li>
							</ul>
						</div>
					</div>
				</div>
				<div class="item">
					<div class="item learnerITEM">
						<div class="form-group clearfix Lblock1">
							<select class="form-control multiselect-ui selection group_list" id="standing_groups" data-placeholder="Group1" multiple="multiple">
							</select>
						</div>
						<div class="LERneRboxtest LERneRboxR Learbox1">
							<ul>
								<li><img class="img-fluid LDtopsvg" src="img/dash_icon/me.svg"></li>
								<li><span class="LEAR40 LEARGreen" id="my_standing">0</span><span class="LEAR40 LEARBlack">/<span id="tlearners">0</span></span><span class="S_botmtext">My Rank</span></li>
							</ul>
							<ul>
								<li><img class="img-fluid LDtopsvg" src="img/dash_icon/my_score.svg"></li>
								<li><span class="LEAR24 LEARGreen" id="my_score">0</span><span class="S_botmtext">My Score</span></li>
							</ul>
						</div>
					</div>
				</div>
				<div class="item">
					<ul class="LHBanner">
						<li>Group</li>
						<li><img class="LHimg" src="img/dash_icon/l_Dash_group.svg"></li>
					</ul>
					<div class="item learnerITEM">
						<div class="LERneRboxtest LERneRboxAling">
							<ul class="GraphscoreB">
								<li class="Graphscore">
									<div id="score2">
										<img src="img/dash_icon/topper02.svg" />
										<span class="S_two LEARBlack"></span>
										<span class="LEAR24 LEARBlack otherscore1">0%</span>
									</div>
									<div id="score1">
										<img src="img/dash_icon/topper01.svg" />
										<span class="S_one LEARBlack"></span>
										<span class="LEAR24 LEARBlack otherscore2">0%</span>
									</div>
									<div id="score3">
										<img src="img/dash_icon/topper03.svg" />
										<span class="S_three LEARBlack"></span>
										<span class="LEAR24 L-Ralign LEARBlack otherscore3">0%</span>
									</div>
								</li>
							</ul>
						</div>
					</div>
				</div>
				<div class="item">
					<ul class="LHBanner">
						<li>Department</li>
						<li><img class="LHimg" src="img/dash_icon/l_Dash_department.svg"></li>
					</ul>
					<div class="item learnerITEM">
						<div class="LERneRboxtest LERneRboxAling">
							<ul class="GraphscoreB">
								<li class="Graphscore">
									<div id="ul_score2">
										<img src="img/dash_icon/topper02.svg" />
										<span class="S_two LEARBlack"></span>
										<span class="LEAR24 LEARBlack otherscore1">0%</span>
									</div>
									<div id="ul_score1">
										<img src="img/dash_icon/topper01.svg" />
										<span class="S_one LEARBlack"></span>
										<span class="LEAR24 LEARBlack otherscore2">0%</span>
									</div>
									<div id="ul_score3">
										<img src="img/dash_icon/topper03.svg" />
										<span class="S_three LEARBlack"></span>
										<span class="LEAR24 L-Ralign LEARBlack otherscore3">0%</span>
									</div>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="container-fluid" id="print_data">
		<div class="PREFOtext">My Progress</div>
		<div class="col-container">
			<div class="col flexItem70">
				<div class="row ASignedlear">
					<div class="L-E-box col-sm-12">
						<a href="assigned-simulations.php">
							<div class="green col-sm-3">
								<div class="ShoWSiM">
									<div class="LEAR40"><?php echo $totalSim; ?></div>
									<div>Assigned Simulation</div>
								</div>
							</div>
						</a>
						<a href="assigned-simulations.php">
							<div class="blue col-sm-3">
								<div class="ShoWSiM">
									<div class="LEAR40"><?php echo count($totalAttSim); ?></div>
									<div>Attempted Simulation</div>
								</div>
							</div>
						</a>
						<a href="incomplete-simulations.php">
							<div class="yellow col-sm-3">
								<div class="ShoWSiM">
									<div class="LEAR40"><?php echo $totalIncSim; ?></div>
									<div>Incomplete Simulation</div>
								</div>
							</div>
						</a>
					</div>
				</div>
				<div class="row">					
					<div class="col-sm-6">
						<form method="get">
							<div class="DropboxE2 ltopdropdown">
								<div class="form-group">
									<select class="form-control multiselect-ui selection" name="sim_id2" id="sim_id2" data-placeholder="Search & Select Simulations" multiple="multiple">
										<option value="0" selected="selected" disabled="disabled">Search & Select Simulations</option>
									</select>
								</div>
							</div>
						</form>
					</div>
				</div>
				<div class="row">
					<div class="L-E2part-4 clearfix">
						<div class="col-sm-6 LEAR-G LEAR-G1">
							<div class="LGRAph"><div id="pass_fail" style="height: 220px; width: 100%;"></div></div>
						</div>
						<div class="col-sm-6 LEAR-G LEAR-G1">
							<div class="LGRAph"><div id="sim_score" style="height: 220px; width: 100%;"></div></div>
						</div>
						<div class="col-sm-6 LEAR-G LEAR-G1">
							<div class="LGRAph"><div id="complete_incomplete" style="height: 220px; width: 100%;"></div></div>
						</div>
						<div class="col-sm-6 LEAR-G LEAR-G1">
							<div class="LGRAph"><div id="attempt_dataPoints" style="height: 220px; width: 100%;"></div></div>
						</div>
					</div>
				</div>
			</div>
			<div class="col flexItem30 LRaeNer30">
				<div class="Echart B2">
					<div class="LEARHEADING70">MY EDUCATOR</div>
					<div class="Righttextbox1">
						<div class="table-responsive Echartable_s">
							<table class="table">
								<thead class="thaeed">
									<tr>
										<th>Educator</th>
										<th>Unit</th>
										<th>Location</th>
									</tr>
								</thead>
								<tbody class="educator">
								</tbody>
							</table>
						</div>
					</div>
				</div>
				<div class="Echart B2">
					<div class="LEARHEADING70">SIMULATION SCHEDULE</div>
					<div class="table-responsive Echartable_s">
						<table class="table">
							<thead class="thaeed">
								<tr>
									<th>Category</th>
									<th>Simulation</th>
									<th>Start Date</th>
									<?php if ( ! empty($client_id) && $client_id != 9): ?>
									<th>Closing Date</th>
									<?php endif; ?>
								</tr>
							</thead>
							<tbody>
							<?php if ( ! empty($sim_list)):
							$sql = "SELECT s.scenario_id, s.Scenario_title, LEFT(s.Scenario_title, 20) AS title, c.category FROM scenario_master AS s LEFT JOIN category_tbl AS c ON s.scenario_category = c.cat_id WHERE s.scenario_id IN ($sim_list)";
							$q = $db->prepare($sql); $q->execute();
							if ($q->rowCount() > 0):
								foreach ($q->fetchAll(PDO::FETCH_ASSOC) as $row):
									$scenario_id = $row['scenario_id'];
									$asql = "SELECT DATE_FORMAT(a.start_date, '%e-%b-%y %l:%i %p') AS sdate, DATE_FORMAT(a.end_date, '%e-%b-%y %l:%i %p') AS edate, g.group_id FROM assignment_tbl a LEFT JOIN group_tbl g ON a.group_id = g.group_id WHERE FIND_IN_SET($scenario_id, a.scenario_id) AND FIND_IN_SET($userid, g.learner)";
									$aq = $db->prepare($asql); $aq->execute();
									$adata = $aq->fetch(PDO::FETCH_ASSOC);
									$group_id[] = $adata['group_id']; ?>
								<tr>
									<td><?php echo $row['category'] ?></td>
									<td title="<?php echo $row['Scenario_title']; ?>"><?php echo $row['title']; ?>...</td>
									<td><?php echo $adata['sdate']; ?></td>
									<?php if ( ! empty($client_id) && $client_id != 9): ?>
									<td><?php echo $adata['edate']; ?></td>
									<?php endif; ?>
								</tr>
							<?php endforeach; endif; endif; ?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php 
if ( ! empty($group_id)):
	$gid 	= $db->addMultiIds(array_unique($group_id));
	$lid 	= $db->getGroupColumns($gid, 'group_leader')[0]['col'];
	$udata	= $db->getUsersColumns($lid, 'username,department,location');
else:
	$udata	= [];
endif; ?>
<script type="text/javascript" src="content/js/daterangepicker/moment.min.js"></script>
<script type="text/javascript" src="content/js/daterangepicker/daterangepicker.js"></script>
<script type="text/javascript" src="content/js/canvasjs.min.js"></script>
<script type="text/javascript" src="content/js/jspdf.min.js"></script>
<script type="text/javascript" src="content/js/html2canvas.js"></script>
<script type="text/javascript" src="content/js/owlcarousel/owl.carousel.min.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
		/* MY EDUCATOR */
		var leader = '<?php echo ( ! empty($udata)) ? json_encode($udata) : '' ?>';
		if (leader.length > 0) {
			var html = '';
			$.each(JSON.parse(leader), function(i, item) {
				html = '<tr><td>'+ item.username +'</td><td>'+ item.department +'</td><td>'+ item.location +'</td></tr>';
				$('.educator').append(html);
			});
		}
		
		var owl = $('.owl-carousel');
		owl.owlCarousel({
			loop:false,
			margin: 5,
			nav: true,
			responsive:{
				0: { items: 1 },
				600: { items: 3 },
				1200: { items: 5 }
			}
		});

		var start = moment().subtract(29, 'days');
		var end = moment();

		function cb(start, end) {
			$('#reportrange span').html(start.format('YYYY-MM-DD') + ' - ' + end.format('YYYY-MM-DD'));
			$('#date').val(start.format('YYYY-MM-DD') + ' - ' + end.format('YYYY-MM-DD'));
		}

		$('#reportrange').daterangepicker({
			startDate: start,
			endDate: end,
			locale: { format: 'YYYY-MM-DD' },
			ranges: {
				'Today': [moment(), moment()],
				'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
				'Last 7 Days': [moment().subtract(6, 'days'), moment()],
				'Last 30 Days': [moment().subtract(29, 'days'), moment()],
				'This Month': [moment().startOf('month'), moment().endOf('month')],
				'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
			}
		}, cb);
		cb(start, end);

		var date = $('#date').val();
		
		/* SIM LIST */
		var maxCount = 1;
		$('.sim_list').multiselect({
			disableIfEmpty: true,
			filterPlaceholder: 'Search & Select Simulations',
			enableCaseInsensitiveFiltering: true,
			enableFiltering: true,
			maxHeight: 250,
			numberDisplayed: 1,
			includeSelectAllOption: false,
			onChange: function(option, checked, select) {
				var selectID = $(option).parent().attr('id');
				var date = $('#date').val();

				//Get selected count
				var selectedCount = $('#'+ selectID).find("option:selected").length;
				
				//If the selected count is equal to the max allowed count, then disable any unchecked boxes
				if (selectedCount >= maxCount) {
					$.LoadingOverlay("show");
					$('#'+ selectID +" option:not(:selected)").prop('disabled', true);
					$('#'+ selectID).multiselect('refresh');
					var sim_id = $('#'+ selectID).find("option:selected").val();
				}
				else {
					$.LoadingOverlay("show");
					$('#'+ selectID +" option:disabled").prop('disabled', false);
					$('#'+ selectID).multiselect('refresh');
					var sim_id = '';					
				}
				TopFilterData(date, sim_id);
			}
		});

		/* GROUPS LIST */
		var maxGCount = 1;
		$('#standing_groups').multiselect({
			disableIfEmpty: true,
			filterPlaceholder: 'Search & Select Group',
			enableCaseInsensitiveFiltering: true,
			enableFiltering: true,
			maxHeight: 250,
			numberDisplayed: 1,
			includeSelectAllOption: false,
			onChange: function(option, checked, select) {
				var selectID = $(option).parent().attr('id');
				var date = $('#date').val();

				//Get selected count
				var selectedCount = $('#'+ selectID).find("option:selected").length;
				
				//If the selected count is equal to the max allowed count, then disable any unchecked boxes
				if (selectedCount >= maxGCount) {
					$.LoadingOverlay("show");
					$('#'+ selectID +" option:not(:selected)").prop('disabled', true);
					$('#'+ selectID).multiselect('refresh');
					var group_id = $('#'+ selectID).find("option:selected").val();
					var sim_id = $('#top_filter').find("option:selected").val();
				}
				else {
					$.LoadingOverlay("show");
					$('#'+ selectID +" option:disabled").prop('disabled', false);
					$('#'+ selectID).multiselect('refresh');
					var group_id = '';
					var sim_id = '';
				}
				GroupsFilterData(group_id, date, sim_id);
			}
		});

		$('#sim_id2').multiselect({
			disableIfEmpty: true,
			filterPlaceholder: 'Search & Select Simulations',
			enableCaseInsensitiveFiltering: true,
			enableFiltering: true,
			maxHeight: 250,
			includeSelectAllOption: true,
			onChange: function(option, checked, select) {
				$.LoadingOverlay("show");
				if ($('#sim_id2 :selected').length > 0) {
					var selectednumbers = [];
					$('#sim_id2 :selected').each(function(i, selected) {
						selectednumbers[i] = $(selected).val();
					});
					var sim_id = selectednumbers;
				}
				else {
					var sim_id = '';
				}
				defaultGraphData(date, sim_id);
			}
		});
		
		function simList(date) {
			$('.sim_list,#sim_id2').empty(); $('.sim_list,#sim_id2').multiselect('refresh');
			$.getJSON('includes/learner-dash-grid-data.php?getLearnerSim=true&date='+ date , function(res) {
				if (res.success == true) {
					$('.sim_list,#sim_id2').multiselect('dataprovider', res.sim_data);
				}
				else if (res.success == false) {
					$('.sim_list,#sim_id2').multiselect('rebuild');
					$('.sim_list,#sim_id2').multiselect('refresh');
				}
			});
		}
		
		/* GET SIM LIST */
		simList(date);
		
		/* Top Filter */
		function TopFilterData(date, sim_id) {
			if (sim_id != '') {
				var param = 'topFilterData=true&date='+ date +'&sim_id='+ sim_id;
				console.log(param);
				$.ajax({
					type: "POST",
					url: 'includes/learner-dash-grid-data.php',
					data: param,
					cache: false,
					success: function (resdata) {
						$.LoadingOverlay("hide");
					//	console.log("resdata" ,resdata);
						var res = $.parseJSON(resdata);
						if (res.success == true) {
							/* Top Scorers and Me */
							var cur_top_score		= res.data.cur_top_score;
							var other_top_score1	= res.data.other_top_score1;
							var other_top_score2	= res.data.other_top_score2;

							var other_top_ul_score1 = res.data.other_top_ul_score1;
							var other_top_ul_score2 = res.data.other_top_ul_score2;

							/* ME */
							if (cur_top_score > other_top_score1 && cur_top_score > other_top_score2) {
								$('#score1 span:nth-child(2)').html('Me');
								$('#score1 span:nth-child(3)').html(cur_top_score +'%');
							}
							else if (cur_top_score > other_top_score1 && cur_top_score < other_top_score2) {
								$('#score2 span:nth-child(2)').html('Me');
								$('#score2 span:nth-child(3)').html(cur_top_score +'%');
							}
							else if (cur_top_score > other_top_score2 && cur_top_score < other_top_score1) {
								$('#score2 span:nth-child(2)').html('Me');
								$('#score2 span:nth-child(3)').html(cur_top_score +'%');
							}
							else if (cur_top_score < other_top_score1 && cur_top_score < other_top_score2) {
								$('#score3 span:nth-child(2)').html('Me');
								$('#score3 span:nth-child(3)').html(cur_top_score +'%');
							}
							/* OTHER-1 */
							if (other_top_score1 > cur_top_score && other_top_score1 > other_top_score2) {
								$('#score1 span:nth-child(2)').html(1);
								$('#score1 span:nth-child(3)').html(other_top_score1 +'%');
							}
							else if (other_top_score1 > cur_top_score && other_top_score1 < other_top_score2) {
								$('#score2 span:nth-child(2)').html(2);
								$('#score2 span:nth-child(3)').html(other_top_score1 +'%');
							}
							else if (other_top_score1 > other_top_score2 && other_top_score1 < cur_top_score) {
								$('#score2 span:nth-child(2)').html(2);
								$('#score2 span:nth-child(3)').html(other_top_score1 +'%');
							}
							else if (other_top_score1 < cur_top_score && other_top_score1 < other_top_score2) {
								$('#score3 span:nth-child(2)').html(3);
								$('#score3 span:nth-child(3)').html(other_top_score1 +'%');
							}
							/* OTHER-2 */
							if (other_top_score2 > cur_top_score && other_top_score2 > other_top_score1) {
								$('#score1 span:nth-child(2)').html(1);
								$('#score1 span:nth-child(3)').html(other_top_score2 +'%');
							}
							else if (other_top_score2 > cur_top_score && other_top_score2 < other_top_score1) {
								$('#score2 span:nth-child(2)').html(2);
								$('#score2 span:nth-child(3)').html(other_top_score2 +'%');
							}
							else if (other_top_score2 > other_top_score1 && other_top_score2 < cur_top_score) {
								$('#score2 span:nth-child(2)').html(2);
								$('#score2 span:nth-child(3)').html(other_top_score2 +'%');
							}
							else if (other_top_score2 < cur_top_score && other_top_score2 < other_top_score1) {
								$('#score3 span:nth-child(2)').html(3);
								$('#score3 span:nth-child(3)').html(other_top_score2 +'%');
							}

							/* My standing in my Unit/Location */
							/* ME */
							if (cur_top_score > other_top_ul_score1 && cur_top_score > other_top_ul_score2) {
								$('#ul_score1 span:nth-child(2)').html('Me');
								$('#ul_score1 span:nth-child(3)').html(cur_top_score +'%');
							}
							else if (cur_top_score > other_top_ul_score1 && cur_top_score < other_top_ul_score2) {
								$('#ul_score2 span:nth-child(2)').html('Me');
								$('#ul_score2 span:nth-child(3)').html(cur_top_score +'%');
							}
							else if (cur_top_score > other_top_ul_score2 && cur_top_score < other_top_ul_score1) {
								$('#ul_score2 span:nth-child(2)').html('Me');
								$('#ul_score2 span:nth-child(3)').html(cur_top_score +'%');
							}
							else if (cur_top_score < other_top_ul_score1 && cur_top_score < other_top_ul_score2) {
								$('#ul_score3 span:nth-child(2)').html('Me');
								$('#ul_score3 span:nth-child(3)').html(cur_top_score +'%');
							}
							/* OTHER-1 */
							if (other_top_ul_score1 > cur_top_score && other_top_ul_score1 > other_top_ul_score2) {
								$('#ul_score1 span:nth-child(2)').html(1);
								$('#ul_score1 span:nth-child(3)').html(other_top_ul_score1 +'%');
							}
							else if (other_top_ul_score1 > cur_top_score && other_top_ul_score1 < other_top_ul_score2) {
								$('#ul_score2 span:nth-child(2)').html(2);
								$('#ul_score2 span:nth-child(3)').html(other_top_ul_score1 +'%');
							}
							else if (other_top_ul_score1 > other_top_ul_score2 && other_top_ul_score1 < cur_top_score) {
								$('#ul_score2 span:nth-child(2)').html(2);
								$('#ul_score2 span:nth-child(3)').html(other_top_ul_score1 +'%');
							}
							else if (other_top_ul_score1 < cur_top_score && other_top_ul_score1 < other_top_ul_score2) {
								$('#ul_score3 span:nth-child(2)').html(3);
								$('#ul_score3 span:nth-child(3)').html(other_top_ul_score1 +'%');
							}
							/* OTHER-2 */
							if (other_top_ul_score2 > cur_top_score && other_top_ul_score2 > other_top_ul_score1) {
								$('#ul_score1 span:nth-child(2)').html(1);
								$('#ul_score1 span:nth-child(3)').html(other_top_ul_score2 +'%');
							}
							else if (other_top_ul_score2 > cur_top_score && other_top_ul_score2 < other_top_ul_score1) {
								$('#ul_score2 span:nth-child(2)').html(2);
								$('#ul_score2 span:nth-child(3)').html(other_top_ul_score2 +'%');
							}
							else if (other_top_ul_score2 > other_top_ul_score1 && other_top_ul_score2 < cur_top_score) {
								$('#ul_score2 span:nth-child(2)').html(2);
								$('#ul_score2 span:nth-child(3)').html(other_top_ul_score2 +'%');
							}
							else if (other_top_ul_score2 < cur_top_score && other_top_ul_score2 < other_top_ul_score1) {
								$('#ul_score3 span:nth-child(2)').html(3);
								$('#ul_score3 span:nth-child(3)').html(other_top_ul_score2 +'%');
							}
							
							/* My Decision Promptness */
							$('#my_avg_time').html(res.data.cur_average_time);
							$('#other_avg_time').html(res.data.other_average_time);

							/* Strongest Competencies */
							if (res.data.strongCompLable1) {
								$('#StrongComp1').show();
								$('#StrongComp1 li:nth-child(1)').html(res.data.strongCompLable1);
								$('#StrongComp1 li:nth-child(2)').html(res.data.strongCompScore1 +'%');
							}
							if (res.data.strongCompLable2) {
								$('#StrongComp2').show();
								$('#StrongComp2 li:nth-child(1)').html(res.data.strongCompLable2);
								$('#StrongComp2 li:nth-child(2)').html(res.data.strongCompScore2 +'%');
							}
							if (res.data.strongCompLable3) {
								$('#StrongComp3').show();
								$('#StrongComp3 li:nth-child(1)').html(res.data.strongCompLable3);
								$('#StrongComp3 li:nth-child(2)').html(res.data.strongCompScore3 +'%');
							}
							if (res.data.strongCompLable4) {
								$('#StrongComp4').show();
								$('#StrongComp4 li:nth-child(1)').html(res.data.strongCompLable4);
								$('#StrongComp4 li:nth-child(2)').html(res.data.strongCompScore4 +'%');
							}
							if (res.data.strongCompLable5) {
								$('#StrongComp5').show();
								$('#StrongComp5 li:nth-child(1)').html(res.data.strongCompLable5);
								$('#StrongComp5 li:nth-child(2)').html(res.data.strongCompScore5 +'%');
							}
							if (res.data.strongCompLable6) {
								$('#StrongComp6').show();
								$('#StrongComp6 li:nth-child(1)').html(res.data.strongCompLable6);
								$('#StrongComp6 li:nth-child(2)').html(res.data.strongCompScore6 +'%');
							}
							/* Weakest Competencies */
							if (res.data.weakCompLable1) {
								$('#WeakComp1').show();
								$('#WeakComp1 li:nth-child(1)').html(res.data.weakCompLable1);
								$('#WeakComp1 li:nth-child(2)').html(res.data.weakCompScore1 +'%');
							}
							if (res.data.weakCompLable2) {
								$('#WeakComp2').show();
								$('#WeakComp2 li:nth-child(1)').html(res.data.weakCompLable2);
								$('#WeakComp2 li:nth-child(2)').html(res.data.weakCompScore2 +'%');
							}
							if (res.data.weakCompLable3) {
								$('#WeakComp3').show();
								$('#WeakComp3 li:nth-child(1)').html(res.data.weakCompLable3);
								$('#WeakComp3 li:nth-child(2)').html(res.data.weakCompScore3 +'%');
							}
							if (res.data.weakCompLable4) {
								$('#WeakComp4').show();
								$('#WeakComp4 li:nth-child(1)').html(res.data.weakCompLable4);
								$('#WeakComp4 li:nth-child(2)').html(res.data.weakCompScore4 +'%');
							}
							if (res.data.weakCompLable5) {
								$('#WeakComp5').show();
								$('#WeakComp5 li:nth-child(1)').html(res.data.weakCompLable5);
								$('#WeakComp5 li:nth-child(2)').html(res.data.weakCompScore5 +'%');
							}
							if (res.data.weakCompLable6) {
								$('#WeakComp6').show();
								$('#WeakComp6 li:nth-child(1)').html(res.data.weakCompLable6);
								$('#WeakComp6 li:nth-child(2)').html(res.data.weakCompScore6 +'%');
							}

							/* My standing in my groups */
							if (res.data.assignGroup.length > 0) {
								$('#standing_groups').empty(); $('#standing_groups').multiselect('refresh');
								$('#standing_groups').multiselect('dataprovider', res.data.assignGroup);
							}

							/* Total Attempted Simulation */
							$('#att_sim').html(res.data.totalAttempt);

							/* Pass 1st */
							if (res.data.pass_1st == 1) {
								$('#pass_1st').html('Yes');
							}
							
							/* Pass 2nd */
							if (res.data.pass_2nd == 1) {
								$('#pass_2nd').html('Yes');
							}

							/* Fail */
							$('#fail').html(res.data.fail);

							/* Job Readiness */
							$('#job_readiness').html(res.data.job_readiness + '%');

							/* Attempts */
							$('#1attempt_score').html(res.data.pass1score);

							$('#2attempt_score').html(res.data.pass2score);
						}
					},error: function() {
						$.LoadingOverlay("hide");
						swal({text: 'Oops. something went wrong please try again.', buttons: false, icon: "error", timer: 2000});
					}
				});
			}
			else {
					$('#score1 span:nth-child(2)').html('');
					$('#score2 span:nth-child(2)').html('');
					$('#score3 span:nth-child(2)').html('');
					$('#score1 span:nth-child(3)').html('0%');
					$('#score2 span:nth-child(3)').html('0%');
					$('#score3 span:nth-child(3)').html('0%');

					$('#ul_score1 span:nth-child(2)').html('');
					$('#ul_score2 span:nth-child(2)').html('');
					$('#ul_score3 span:nth-child(2)').html('');
					$('#ul_score1 span:nth-child(3)').html('0%');
					$('#ul_score2 span:nth-child(3)').html('0%');
					$('#ul_score3 span:nth-child(3)').html('0%');

					$('#my_avg_time,#other_avg_time').html('00:00:00');
					$('#StrongComp1,#StrongComp2,#StrongComp3,#StrongComp4,#StrongComp5,#StrongComp6,#WeakComp1,#WeakComp2,#WeakComp3,#WeakComp4,#WeakComp5,#WeakComp6').hide();
					$('#standing_groups').multiselect('deselectAll', true);
					$('#standing_groups').multiselect('refresh');
					$('#standing_groups').multiselect('disable');
					$('#my_standing,#my_score,#tlearners,#avg_score,#att_sim,#fail,#job_readiness,#1attempt_score,#2attempt_score').html(0);
					$('#pass_1st,#pass_2nd').html('--');
					$.LoadingOverlay("hide");
				}
		}

		/* Groups Filter */
		function GroupsFilterData(group_id, date, sim_id) {
			if (sim_id != '' && group_id != '') {
				var param = 'groupFilterData=true&date='+ date +'&sim_id='+ sim_id +'&group_id='+ group_id + '&userId='+<?php echo $userid?>;
				$.ajax({
					type: "POST",
					url: 'includes/learner-dash-grid-data.php',
					data: param,
					cache: false,
					success: function (resdata) {
						$.LoadingOverlay("hide");
						var res = $.parseJSON(resdata);
						if (res.success == true) {
							$('#my_standing').html(res.data.position);
							$('#my_score').html(res.data.lscore);
							$('#tlearners').html(res.data.totall);
							$('#avg_score').html(res.data.top_score);
						}
					},error: function() {
						$.LoadingOverlay("hide");
						swal({text: 'Oops. something went wrong please try again.', buttons: false, icon: "error", timer: 2000});
						$('#my_standing,#my_score,#tlearners,#avg_score').html(0);
					}
				});
			} else {
				$('#my_standing,#my_score,#tlearners,#avg_score').html(0);
				$.LoadingOverlay("hide");
			}
		}
		
		/* GET DEFAULT DATA */
		function defaultGraphData(date, sim_id) {
		//	alert("hi");
			var param = 'getLearnerDefaultData=true&date='+ date +'&sim_id='+ sim_id;
		//	console.log(param);
			$.ajax({
				type: "POST",
				url: 'includes/learner-dash-grid-data.php',
				data: param,
				cache: false,
				success: function (resdata) {
					$.LoadingOverlay("hide");
					var res = $.parseJSON(resdata);
					console.log("res",resdata);
					if (res.success == true) {
						CanvasJS.addColorSet("colorcode1", ["#6d3073", "#3b602d", "#c07744", "#678087", "#678087"]);
						CanvasJS.addColorSet("colorcode2", ["#faab21", "#1185bb", "#2E8B57", "#3CB371", "#90EE90"]);
						/* PASS-FAIL */
						if (res.data.passing_dataPoints.length && res.data.achieved_dataPoints.length) {
							var passFailChart = new CanvasJS.Chart("pass_fail", {
								colorSet: "colorcode1",
								title: { text: "Pass Fail"},
								animationEnabled: true,
								theme: "light2",
								backgroundColor: '#fff',
								axisX: { gridThickness: 0, tickLength: 0, lineThickness: 0, labelFontColor: "black", labelFormatter: function() { return " "; } },
								axisY: { gridThickness: 0, tickLength: 0, lineThickness: 0, labelFormatter: function() { return " "; } },
								toolTip: { shared: true, reversed: true },
								data: [
										{
											type: "stackedColumn",
											name: "Total Score",
											showInLegend: true,
											dataPoints: res.data.passing_dataPoints
										},
										{
											type: "stackedColumn",
											name: "Achieved Score",
											showInLegend: true,
											dataPoints: res.data.achieved_dataPoints
										}]
								});
								passFailChart.render();
						}
						/* Complete-Incomplete */
						if (res.data.totalq_dataPoints.length && res.data.attemptq_dataPoints.length) {
							var completeIncompleteChart = new CanvasJS.Chart("complete_incomplete", {
								colorSet: "colorcode2",
								title: { text: "Complete / Incomplete"},
								animationEnabled: true,
								theme: "light2",
								backgroundColor: '#fff',
								axisX: { gridThickness: 0, tickLength: 0, lineThickness: 0, labelFontColor: "black", labelFormatter: function() { return " "; } },
								axisY: { gridThickness: 0, tickLength: 0, lineThickness: 0, labelFormatter: function() { return " "; } },
								toolTip: { shared: true, reversed: true },
								data: [
										{
											type: "stackedBar",
											name: "Total Question",
											showInLegend: true,
											dataPoints: res.data.totalq_dataPoints
										},
										{
											type: "stackedBar",
											name: "Attempted Question",
											showInLegend: true,
											dataPoints: res.data.attemptq_dataPoints
										}]
								});
								completeIncompleteChart.render();
						}
						/* NUMBER OF ATTEMPT */
						if (res.data.attempt_dataPoints.length) {
							var attemptdataPointsChart = new CanvasJS.Chart("attempt_dataPoints", {
								colorSet: "colorcode2",
								title: { text: "NUMBER OF ATTEMPTS"},
								animationEnabled: true,
								theme: "light2",
								backgroundColor: '#fff',
								axisX: { gridThickness: 0, tickLength: 0, lineThickness: 0, labelFontColor: "black", labelFormatter: function() { return " "; } },
								axisY: { gridThickness: 0, tickLength: 0, lineThickness: 0, labelFormatter: function() { return " "; } },
								toolTip: { content: "<div class='wordwrap'>{label}: {y}</div>" },
								data: [{
										type: "doughnut",
										indexLabel: "{y}",
										indexLabelPlacement: "inside",
										indexLabelFontWeight: "bolder",
										indexLabelFontColor: "black",
										showInLegend: true,
										legendText: "{label} : {y}",
										dataPoints: res.data.attempt_dataPoints
										}]
								});
								attemptdataPointsChart.render();
						}
						/* SIM SCORE */
						if (res.data.score_dataPoints.length) {
							var scoredataPointsChart = new CanvasJS.Chart("sim_score", {
								colorSet: "colorcode2",
								title: { text: "SCORE"},
								animationEnabled: true,
								theme: "light2",
								backgroundColor: '#fff',
								axisX: { gridThickness: 0, tickLength: 0, lineThickness: 0, labelFontColor: "black", labelFormatter: function() { return " "; } },
								axisY: { gridThickness: 0, tickLength: 0, lineThickness: 0, labelFormatter: function() { return " "; } },
								toolTip: { content: "<div class='wordwrap'>{label}: {y}</div>" },
								data: [{
										type: "line",
										indexLabel: "{y}",
										indexLabelPlacement: "inside",
										indexLabelFontWeight: "bolder",
										indexLabelFontColor: "black",
										dataPoints: res.data.score_dataPoints
										}]
								});
								scoredataPointsChart.render();
						}
					}
				},error: function() {
					$.LoadingOverlay("hide");
					swal({text: 'Oops. something went wrong please try again.', buttons: false, icon: "error", timer: 2000});
				}
			});
		}

		defaultGraphData(date, '');

		$('#reportrange').on('change', function() {
			$.LoadingOverlay("show");
			var date = $(this).val();
			$('#date').val(date);
			simList(date);
			var sim_id = '';
			if ($('#sim_id2 :selected').length > 0) {
				var selectednumbers = [];
				$('#sim_id2 :selected').each(function(i, selected) {
					selectednumbers[i] = $(selected).val();
				});
				var sim_id = selectednumbers;
			}
			defaultGraphData(date, sim_id);
			$.LoadingOverlay("hide");
		});
	});

	function getPDF(id) {
		var HTML_Width = $(id).width();
		var HTML_Height = $(id).height();
		var top_left_margin = 15;
		var PDF_Width = HTML_Width + (top_left_margin * 2);
		var PDF_Height = (PDF_Width * 1.5) + (top_left_margin * 2);
		var canvas_image_width = HTML_Width;
		var canvas_image_height = HTML_Height;
		var totalPDFPages = Math.ceil(HTML_Height / PDF_Height) - 1;
		html2canvas($(id)[0], { allowTaint: true }).then(function(canvas) {
			var imgData = canvas.toDataURL("image/jpeg", 1.0);
			var pdf = new jsPDF('p', 'pt', [PDF_Width, PDF_Height]);
			pdf.addImage(imgData, 'JPG', top_left_margin, top_left_margin, canvas_image_width, canvas_image_height);
			for (var i = 1; i <= totalPDFPages; i++) {
				pdf.addPage(PDF_Width, PDF_Height);
				pdf.addImage(imgData, 'JPG', top_left_margin, -(PDF_Height * i) + (top_left_margin * 4), canvas_image_width, canvas_image_height);
			}
			pdf.save("dashboard-report.pdf");
		});
	}

	document.onreadystatechange = function() {
		if (document.readyState == "complete") {
			$.LoadingOverlay("hide");
		}
	};
</script>
<?php 
require_once 'includes/footer.php';
