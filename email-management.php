<?php 
session_start();
if (isset($_SESSION['username'])) {
	$user	= $_SESSION['username'];
	$role	= $_SESSION['role'];
	$userid	= $_SESSION['userId'];
}
else {
    header('location: index.php');
}
require_once 'config/db.class.php';
require_once 'includes/learners_menu.php';
require_once 'config/pagination.php';
$db  = new DBConnection();
$msg = '';
if (isset($_SESSION['msg']) && ! empty($_SESSION['msg'])):
	$msg = $_SESSION['msg'];
endif;

if (isset($_GET['delete']) && ! empty($_GET['eid'])):
	$id		= (isset($_GET['eid'])) ?  $_GET['eid'] : '';
	$sql	= "DELETE FROM email_template_tbl WHERE MD5(eid) = ?";
    $q		= $db->prepare($sql);
	if ($q->execute(array($id))) $msg = 'Email template delete successfully.!';
endif;

$sqlr = "SELECT * FROM email_template_tbl ORDER BY eid DESC";
$q = $db->prepare($sqlr); $q->execute();
$rowCount = $q->rowCount();
$count = ($sqlr) ? $rowCount : 0;
$page  = (isset($_REQUEST['page']) && ! empty($_REQUEST['page'])) ? $_REQUEST['page'] : 1;
$show  = 15;
$start = ($page - 1) * $show;
$pagination = getPaginationString($count, $show, $page, 1, basename(__FILE__), '?page=');
$sql = "SELECT * FROM email_template_tbl ORDER BY eid DESC LIMIT $start, $show";
$results = $db->prepare($sql); ?>
<div class="bottomheader">
    <div class="container-fluid">
        <ul class="breadcrumb">
			<?php echo $db->breadcrumbs(); ?>
         </ul>
     </div>
</div>
<div class="Group_managment">
    <div class="container-fluid">
		<div class="row">
			<div class="col-sm-12">
				<?php /* ?>
				<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
					<div class="panel panelCaollapse">
						<div class="panel-heading" role="tab" id="headingOne">
							<h4 class="panel-title">
								<a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
									Create New Template
								</a>
							</h4>
						</div>				
						<div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
							<div class="panel-body">
								<form method="post" action="includes/process.php" enctype="multipart/form-data">
									<div class="usermanage-form"> 
									<div class="row">
										<div class="col-sm-6">
											<div class="form-group">
												<input type="text" class="form-control" name="email_subject" id="email_subject" required="required" placeholder="Subject">
											</div>
										</div>
										<div class="col-sm-6">
											<div class="form-group">
												<input type="text" class="form-control" name="email_type" id="email_type" required="required" placeholder="Type">
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-sm-12">
											<div class="form-group">
												<textarea class="editor1" id="editor1" name="tmp_des" required="required"></textarea>
											</div>
										</div>							                            
									</div>
									<div class="row">
										<div class="col-sm-12 ">
											<div class="form-group">
												<textarea name="comment" id="comment" placeholder="Comment" class="form-control"></textarea>
											</div>
										</div>
									</div>
									</div>
									<hr class="linebox">
									<div class="usermanage-add clearfix">
									<input type="hidden" name="status" value="1" />
									<input type="submit" name="add_email_tmp" class="btn btn-primary pull-right PullRcatagry" value="Add Template">
								</div>
							</form>
							</div>
						</div>
					</div>	
				</div>
				<?php */ ?>
				<?php if (isset($_SESSION['msg'])): ?>
					<div class="alert alert-warning alert-dismissable"><?php echo $_SESSION['msg']; ?></div>
				<?php endif; ?>
				<div class="usermanage_main">
					<div class="table-box">
						<div class="tableheader clearfix">
						   <div class="tableheader_left">List of Email Template</div>
						   <!--<div class="tableheader_right">
							   <div class="Searchbox"><input type="text" class="serchtext"></div>
							   <div class="sorttext">  <span class="sort">Sort by</span><img  src="img/down_arrow_white.png"></div>
						   </div>-->
						</div>                
					</div>
					<div class="usermanage-form1">
					   <div class="table-responsive">        
							<table class="table table-striped">
								<thead class="Theader">
									<tr>
										<th>#</th>
										<th>Subject</th>
										<th>Type</th>
										<th>Comment</th>
										<th>Status</th>
										<th>Created Date</th>
										<th>Edit</th>                                                                    
									</tr>
								 </thead>
								 <tbody>
								 <?php 
								 if ($results->execute() && $rowCount > 0):
									$i = $start + 1;
									foreach ($results->fetchAll(PDO::FETCH_ASSOC) as $row): ?>
								<tr>
									<td><?php echo $i ?></td>
									<td><?php echo $row['subject'] ?></td>
									<td><?php echo $row['type'] ?></td>
									<td><?php echo $row['comment'] ?></td>
									<td><?php echo ( ! empty($row['status']) ) ? 'Active' : 'Inactive'; ?></td>
									<td><?php echo $db->dateFormat($row['edate'], 'd-M-y'); ?></td>
									<td><a class="edit tooltiptop" href="edit-email-template.php?edit=true&eid=<?php echo md5($row['eid']) ?>">
										<span class="tooltiptext">Edit</span>
										<img class="svg icoctc" src="img/list/edit.svg"></a>
									</td>
								</tr>
								<?php $i++; endforeach; else: ?>
								<tr><td colspan="7" style="text-align:center"><strong>No data available in database.</strong></td></tr>
								<?php endif; ?>
							   </tbody>
							</table>
						</div>
						<div class="pull-right">
							<nav aria-label="navigation"><?php echo ( ! empty($pagination) ) ? $pagination : '&nbsp;'; ?></nav>
						</div>
					</div>
				 </div>
			</div>
		</div>
    </div>
</div>
<script type="text/javascript">
	CKEDITOR.replace('editor1', {
		customConfig: 'ckeditor_config.js',
	});
</script>   
<?php 
unset($_SESSION['msg']);
require_once 'includes/footer.php';
