<?php 
/**
* @package   	API
* @version   	1.0
* @lastupdate	07-Aug-18
*/

require_once 'rest-api.php';

class API extends REST_API {
	
	public function __construct()
	{
		parent::__construct();
	}
	
	/*
	* Public method for access api.
	* This method dynmically call the method based on the query string
	*/

	public function processApi()
	{
		$req = (isset($_REQUEST['rquest'])) ? $_REQUEST['rquest'] : $this->response('', 404);
		$func = strtolower(trim(str_replace("/", "", $req)));
		if ((int)method_exists($this, $func) > 0)
			$this->$func();
		else
			$this->response('', 404);
	}
	
	/*
	* Encode array into JSON
	*/
	
	private function json($data)
	{
		if (is_array($data)) {
			return json_encode($data);
		}
	}
	
	private function Condition($keyset)
	{
		$keyarray = array();
		foreach ($keyset as $k => $v):
			$keyarray[] = "`".$k."` = '".$v."'";
		endforeach;
		return implode(' AND ', $keyarray);
	}
	
	public function getData()
	{
		$table_name = $this->_request['table'];
		$conditions = (isset($this->_request['conditions'])) ? $this->_request['conditions'] : NULL;
		if (isset($this->_request['order_by']))
			$order_by = $this->_request['order_by'];
		else
			$order_by = NULL;
			$q = "SELECT * FROM ". $table_name ." WHERE 1=1";
			if ($conditions != NULL):
				foreach ($conditions as $key => $value):
					$q .= " AND {$key} = '${value}'";
				endforeach;
			endif;
			if ($order_by != NULL):
				$q .=" ORDER BY ";
				foreach ($order_by as $key => $value):
					$q .= " {$key} ${value}";
				endforeach;
			endif;
			$sql = $this->prepare($q);
			$sql->execute();
			$rowCount = $sql->rowCount();
			if ($rowCount > 0):
				while ($row = $sql->fetch(PDO::FETCH_ASSOC))
					$result[] = $row;
					$this->response($this->json($result), 200);
			else:
				$error = array('status' => "Failed", "msg" => "Sorry It's Not Working");
				$this->response($this->json($error), 400);
			endif;
	}
	
	public function getRow()
	{
		$table_name = $this->_request['table'];
		$conditions = $this->_request['conditions'];
		if (isset($this->_request['order_by']))
			$order_by = $this->_request['order_by'];
		else 
			$order_by = NULL;
			
			$q = "SELECT * FROM ". $table_name ." WHERE 1=1";
			    if ($conditions != NULL) {
					foreach ($conditions as $key => $value):
						$q .= " AND {$key} = '${value}'";
					endforeach;
			    }
			    if ($order_by != NULL) {
			        $q .=" ORDER BY ";
			        foreach ($order_by as $key => $value):
			            $q .= " {$key} ${value}";
			        endforeach;
			    }
				$q .= " LIMIT 1";
				$sql = $this->prepare($q);
				$sql->execute();
				$rowCount = $sql->rowCount();
				if ($rowCount > 0):
					if ($row = $sql->fetch(PDO::FETCH_ASSOC)):
						$data = array_merge(array('status' => 'Succes'), $row);
						$this->response($this->json($data), 200);
					else:
						$error = array('status' => "Failed", 'msg' => "Sorry It's Not Working");
						$this->response($this->json($error), 400);
					endif;
				else:
					$error = array('status' => "Failed", 'msg' => "Data not found");
					$this->response($this->json($error), 400);
				endif;
		}
		
		public function getColumnData()
		{
			$table	= $this->_request['table'];
			$column	= $this->_request['column'];
			if (isset($this->_request['conditions']))
				$conditions = $this->_request['conditions'];
			else
				$conditions =  NULL;
				
				$q = "SELECT ". $column ." FROM ". $table ." WHERE 1=1";
			    if ($conditions != NULL):
			        foreach ($conditions as $key => $value):
			            if (is_numeric($value))
							$q .= " AND {$key} = ${value}";
			            else
			                $q .= " AND {$key} = '${value}'";
			        endforeach;
			    endif;
			    $sql = $this->prepare($q);
				$sql->execute();
				$rowCount = $sql->rowCount();
			    if ($rowCount > 0):
					while ($row = $sql->fetch(PDO::FETCH_ASSOC))
						$result[] = $row;
						$data = array_merge(array('status' => 'Succes'), array('data' => $result));
						$this->response($this->json($data), 200);
				else:
					$error = array('status' => "Failed", 'msg' => "Data not found");
					$this->response($this->json($error), 400);
				endif;
		}
		
		public function Insert()
		{
			$table	= $this->_request['table'];
			$data	= $this->_request['data'];
			if (isset($data['password'])):
				$data['password'] = md5($data['password']);
			endif;
			$col	= implode("`,`", array_keys($data));
			$val 	= implode("','", array_values($data));
			$q	 	= "INSERT INTO $table (`$col`) VALUES ('$val')";
			$sql	= $this->prepare($q);
			if ($sql->execute() && $this->lastInsertId() > 0):
				$this->response($this->json(array('status' => "Succes", 'inserted_id' => $this->lastInsertId())), 200);
			else:
				$error = array('status' => "Failed", 'msg' => "Failed To Insert ". $q);
				$this->response($this->json($error), 400);
			endif;
		}
		
		public function Update()
		{
			$table = $this->_request['table'];
			$data  = $this->_request['data'];
			if (isset($data['password'])):
				$data['password'] = md5($data['password']);
			endif;
			$primary_key = $this->_request['primary_key'];
			$set = array();
			foreach ($data as $k => $v):
				$set[] = "`".$k."` = '".$v."'";
			endforeach;
			$setstring = implode(', ', $set);
			$cond = $this->Condition($primary_key);
			$q	  = "UPDATE $table SET $setstring WHERE $cond";
			$sql  = $this->prepare($q);
			if ($sql->execute()):
				$this->response($this->json(array('status' => "Succes", 'response' => 'Updated')), 200);
			else:
				$error = array('status' => "Failed", 'msg' => "Failed To Updated");
				$this->response($this->json($error), 400);
			endif;
		}
		
		public function Delete()
		{
			$table_name = $this->_request['table'];
			$conditions = $this->_request['conditions'];
			$con = $this->Condition($conditions);
			$q	 = "DELETE FROM $table_name WHERE $con";
			$sql = $this->prepare($q);
			if ($sql->execute()):
				$this->response($this->json(array('status' => "Succes", 'response' => 'Deleted')), 200);
			endif;
			$this->response('', 204);
			$error = array('status' => "Failed", "msg" => "Failed To Delete");
			$this->response($this->json($error), 400);
		}
}

$api = new API;
$api->processApi();
