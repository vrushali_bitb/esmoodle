<?php
ob_start();
session_start();
if (isset($_SESSION['username'])) {
	$user	= $_SESSION['username'];
	$role	= $_SESSION['role'];
	$userid	= $_SESSION['userId'];
	$domain = (isset($_SESSION['domain_name'])) ? $_SESSION['domain_name'] : $_SERVER['HTTP_HOST'];
} else {
	header('location: index.php');
}
require_once 'config/db.class.php';
require_once 'includes/learners_menu.php';
$db     = new DBConnection();

/* page access only when permission */
(empty($clientData['mhome'])) ? header('location: index.php') : '';

$data	  = $db->getCMSPage(1);
$bg_file  = ( ! empty($data['bg_img'])) ? $data['bg_img'] : '';
$path	  = 'scenario/upload/' . $domain . '/'; ?>
<div class="bottomheader">
	<div class="container-fluid">
		<ul class="breadcrumb">
			<?php echo $db->breadcrumbs(); ?>
		</ul>
	</div>
</div>
<style>
	.tooltip{
		position: absolute;
	}
</style>
<div class="Group_managment">
	<div class="container-fluid">
		<div class="row">
			<div class="col-sm-12">
				<form method="post" name="add_sim_linear_multiple_details_form" id="add_sim_linear_multiple_details_form" enctype="multipart/form-data">
					<div class="usermanage-form homePgMang">
						<div class="row">
						   <span data-toggle="tooltip" data-placement="top" title="Upload Background Image Size: 1920X800"><i class="fa fa-info-circle fa-2x" aria-hidden="true"></i></span>
							<div class="addsce-img">
								<div class="adimgbox">
									<div class="form-group">
										<input type="file" name="file" id="file" class="form-control file">
										<input type="text" class="form-control controls" disabled placeholder="Select background image." value="<?php echo $bg_file; ?>">
										<input type="hidden" name="bg_file" id="bg_file" value="<?php echo $bg_file; ?>" />
										<div class="browsebtn browsebtntmp">
											<span id="splashImg">
												<?php if ( ! empty($bg_file)) : ?>
													<a href="<?php echo $path . $bg_file ?>" target="_blank" title="View background image"><i class="fa fa-eye" aria-hidden="true"></i></a>
												<?php endif; ?>
											</span>
											<span id="ImgDelete" <?php echo ( ! empty($bg_file)) ? '' : 'style="display:none"'; ?>>
												<a href="javascript:void(0);" data-bg-file="<?php echo ( ! empty($bg_file)) ? $bg_file : ''; ?>" class="delete_bg_file" title="Delete background image"><i class="fa fa-times" aria-hidden="true"></i></a>
											</span>
										</div>
									</div>
								</div>
								<div class="adimgbox browsebtn"><button class="browse btn btn-primary" type="button">Browse</button></div>
								<div class="adimgbox browsebtn"><button type="button" name="upload" id="upload" class="btn btn-primary">Uplaod</button></div>
							</div>
						</div>
						<div class="row">
							<div class="form-group">
								<textarea class="form-control controls" id="page_des" name="page_des"><?php echo ( ! empty($data['page_content'])) ? $data['page_content'] : ''; ?></textarea>
							</div>
						</div>
					</div>
					<div class="usermanage-add clearfix">
						<input type="hidden" name="cms_page_id" value="1" />
						<input type="hidden" name="add_page_data" value="1" />
						<input type="submit" name="add_page_btn" class="btn btn-primary pull-right PullRcatagry" value="Update">
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
$(function () {
  $('[data-toggle="tooltip"]').tooltip()
})
$('#upload').click(function() {
	$.LoadingOverlay("show");
	var file_data = $('#file').prop('files')[0];
	var form_data = new FormData();
	form_data.append('file', file_data);
	form_data.append('page_bg_img', true);
	$.ajax({
		url: "includes/upload.php",
		type: "POST",
		data: form_data,
		contentType: false,
		cache: false,
		processData: false,
		success: function(resdata) {
			var res = $.parseJSON(resdata);
			if (res.success == true) {
				$.LoadingOverlay("hide");
				var viewHtml = '<a href="<?php echo $path ?>' + res.file_name + '" target="_blank" title="View background image"><i class="fa fa-eye" aria-hidden="true"></i></a>';
				$('#splashImg').show().html(viewHtml);
				$('#ImgDelete').show();
				$('.delete_bg_file').attr('data-bg-file', res.file_name);
				$('#bg_file').val(res.file_name);
				$('#upload').html('Upload').removeAttr('disabled', 'disabled');
				swal(res.msg, { buttons: false, timer: 2000});
			} else if (res.success == false) {
				$.LoadingOverlay("hide");
				swal({text: res.msg, buttons: false, icon: "warning", timer: 2000});
				$('#bg_file').val('');
				$('#splashImg').hide('slow');
				$('#upload').html('Upload').removeAttr('disabled', 'disabled');
			}
		},
		error: function() {
			$.LoadingOverlay("hide");
			$('#splashImg').hide('slow');
			swal("Error", 'Oops. something went wrong please try again.?', "error");
		}
	});
});

$('.delete_bg_file').click(function() {
	var file_name  = $(this).attr("data-bg-file");
	var dataString = 'delete=' + true + '&file=' + file_name;
	if (file_name) {
		swal({
			title: "Are you sure?",
			text: "Delete this file.",
			icon: "warning",
			buttons: [true, 'Delete'],
			dangerMode: true }).then((willDelete) => {
			if (willDelete) {
				$.LoadingOverlay("show");
				$.ajax({
					url: "includes/delete-file.php",
					type: "POST",
					data: dataString,
					cache: false,
					success: function(resdata) {
						$.LoadingOverlay("hide");
						var res = $.parseJSON(resdata);
						if (res.success == false) {
							swal("Error", res.msg, "error");
							$('#splashImg').hide('slow');
						} else if (res.success == true) {
							$('#splashImg, #ImgDelete').hide('slow');
							$('#bg_file').val('');
							swal(res.msg, { buttons: false, timer: 1000 });
						}
					},
					error: function() {
						$.LoadingOverlay("hide");
						swal("Error", 'Oops, something went wrong. Please try again later.?', "error");
					}
				});
			} else {
				swal("Your file is safe.!", { buttons: false, timer: 1000 });
			}
		});
	}
});

CKEDITOR.replace('page_des', {
	customConfig: 'config-max.js'
});
</script>
<?php
require_once 'includes/footer.php';
