<?php 
ob_start();
session_start();
if(isset($_SESSION['username'])) {
    $user 	= $_SESSION['username'];
	$role 	= $_SESSION['role'];
	$userid	= $_SESSION['userId'];
}
else {
    header('location: index.php');
}
require_once 'config/db.class.php';
require_once 'includes/learners_menu.php';
require_once 'config/pagination.php';
$db  				= new DBConnection();
$msg 				= '';
$type_icon_path	  	= 'img/type_icon/';
$action_icon_path 	= 'img/list/';

if (isset($_GET['delete']) && ! empty($_GET['scenario_id'])):
	$sql  = "DELETE t1.*, t2.*, t3.*, t4.*, t5.*, t6.*, t7.*, t8.*, t9.*, t10.*, t11, t12 ";
	$sql .= "FROM scenario_master t1 
			 LEFT JOIN question_tbl t2 ON t1.scenario_id = t2.scenario_id 
			 LEFT JOIN feedback_tbl t11 ON t2.question_id = t11.question_id 
			 LEFT JOIN answer_tbl t3 ON t3.question_id = t2.question_id 
			 LEFT JOIN scenario_attempt_tbl t4 ON t1.scenario_id = t4.scenario_id 
			 LEFT JOIN score_tbl t5 ON t1.scenario_id = t5.scenario_id 
			 LEFT JOIN competency_tbl t6 ON t1.scenario_id = t6.scenario_id 
			 LEFT JOIN multi_score_tbl t7 ON t1.scenario_id = t7.scenario_id 
			 LEFT JOIN open_res_tbl t8 ON t1.scenario_id = t8.scenario_id 
			 LEFT JOIN sim_pages t9 ON t1.scenario_id = t9.scenario_id 
			 LEFT JOIN sim_branding_tbl t10 ON t1.scenario_id = t10.scenario_id 
			 LEFT JOIN comments_tbl t12 ON t1.scenario_id = t12.scenario_id 
			 WHERE t1.scenario_id = ?";
    $q = $db->prepare($sql);
	if ($q->execute(array(base64_decode($_GET['scenario_id'])))):
		$msg = 'Simulation delete successfully.';
	endif;
endif;

#----------Serch----------------
$search = '';
if (isset($_GET['search']) && ! empty($_GET['search'])):
	extract($_GET);
	$search = " AND (s.Scenario_title LIKE '%$search%' OR c.category LIKE '%$search%' OR st.status_name)";
endif;
#----------Sort by----------------
if (isset($_GET['orderby']) && $_GET['orderby'] == 'category'):
	$orderby = (( ! empty($search)) ? $search : " ")." ORDER BY c.category";
elseif (isset($_GET['orderby']) && $_GET['orderby'] == 'status'):
	$orderby = (( ! empty($search)) ? $search : " ")." ORDER BY s.status";
elseif (isset($_GET['orderby']) && $_GET['orderby'] == 'stage'):
	$orderby = (( ! empty($search)) ? $search : " ")." ORDER BY st.status_name";
else:
	$orderby = (( ! empty($search)) ? $search : " ")." ORDER BY s.scenario_id DESC";
endif;
$sqlr 		= "SELECT s.*, LEFT(s.Scenario_title, 25) AS stitle, c.category, st.status_name, t.type_name, t.type_icon FROM scenario_master s LEFT JOIN category_tbl c ON s.scenario_category = c.cat_id LEFT JOIN scenario_status st ON s.status = st.sid LEFT JOIN scenario_type t ON s.scenario_type = t.st_id WHERE 1=1 $orderby";
$q 			= $db->prepare($sqlr); $q->execute();
$rowCount	= $q->rowCount();
$count 		= ($sqlr) ? $rowCount : 0;
$page  		= (isset($_REQUEST['page']) && ! empty($_REQUEST['page'])) ? $_REQUEST['page'] : 1;
$show 	 	= 15;
$start 		= ($page - 1) * $show;
$pagination	= getPaginationString($count, $show, $page, 1, basename(__FILE__), '?page=');
$sqlr 	   .= " LIMIT $start, $show";
$results 	= $db->prepare($sqlr); ?>
<div class="bottomheader">
    <div class="container-fluid">
        <ul class="breadcrumb">
			<?php echo $db->breadcrumbs(); ?>
         </ul>
     </div>
</div>
<div class="simulation_managment">
    <div class="container-fluid">
		<div class="row">
			<div class="col-sm-12">
				<?php if ( ! empty($msg)): ?>
				<div class="alert alert-success alert-dismissable"><?php echo $msg; ?></div>
				<?php endif; ?>
				<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
					<div class="panel panelCaollapse">
						<div class="panel-heading" role="tab" id="headingOne">
							<h4 class="panel-title">
								<a  class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
									Create Simulation
								</a>
							</h4>
						</div>
						<div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
							<div class="panel-body">
								<div class="usermanage-form">
										<div class="row">
											<div class="newCatg clearfix">
												<div class="col-sm-3">
													<div class="form-group form-category">
														<form method="post" name="NewCategory_form" id="NewCategory_form">
															<input type="text" name="category" id="category" class="form-control pluscontrol" placeholder="New Category" />
															<input type="hidden" name="create_category" value="1" />
															<button type="submit" class="btn btn-default" id="create"><img src="img/icons/plus.png" class="plusimg" /></button>
														</form>
													</div>
												</div>
											</div>
										</div>
										<div class="row">	                   
										<form enctype="multipart/form" method="post" name="create_simulation_form" id="create_simulation_form">
											<input type="hidden" name="create_simulation" value="1" />
											<div class="col-sm-3">
												<div class="form-group  form-select">
													<select class="form-control" name="scenario_category" id="scenario_category" required="required" data-placeholder="Scenario Category">
													<option value="0" selected>Select Category</option>
													<?php foreach ($db->getCategory() as $category): ?>
													<option value="<?php echo $category['cat_id'] ?>"><?php echo $category['category'] ?></option>
													<?php endforeach; ?>
													</select>
												</div>
											</div>
											<div class="col-sm-3">
												<div class="form-group">
												<input type="text" class="form-control" name="scenario_name" id="scenario_name" placeholder="Simulation Title" required="required">
												</div>
											</div> 
											<div class="col-sm-3">
												<div class="form-group form-select">
													<select class="form-control multiselect-ui selection" multiple="multiple" name="scenario_type[]" id="scenario_type" required="required" data-placeholder="Scenario Type">
													<?php foreach ($db->getScenarioTypes() as $types): ?>
													<option value="<?php echo $types['st_id'] ?>"><?php echo $types['type_name'] ?></option>
													<?php endforeach; ?>
													</select>
												</div>
											</div> 
											<div class="col-sm-3">
												<div class="form-group form-select">
													<select class="form-control multiselect-ui selection" name="author[]" id="author" multiple="multiple" data-placeholder="Author Name List">
													<?php foreach ($db->getUserByRole("'author'", NULL) as $author): ?>
													<option value="<?php echo $author['id'] ?>"><?php echo $author['username'] ?></option>
													<?php endforeach; ?>
													</select>
												</div>
											</div>
											<div class="col-sm-3">
												<div class="form-group form-select">
													<select class="form-control multiselect-ui selection" name="reviewer[]" id="reviewer" multiple="multiple" data-placeholder="Reviewer Name List">
													<?php foreach ($db->getUserByRole("'reviewer'", NULL) as $reviewer): ?>
													<option value="<?php echo $reviewer['id'] ?>"><?php echo $reviewer['username'] ?></option>
													<?php endforeach; ?>
													</select>
												</div>
											</div>
											<div class="col-sm-3">
												<div class="form-group">
													<div class="input-group date form_datetime" data-date-format="yyyy-mm-dd" data-link-field="dtp_input1">
														<input class="form-control" name="start_date" type="text" readonly placeholder="Start Date">
														<span class="input-group-addon"><span class="fa fa-calendar"></span></span>
													</div>
												</div>
											</div>
											<div class="col-sm-3">
												<div class="form-group">
													<div class="input-group date form_datetime" data-date-format="yyyy-mm-dd" data-link-field="dtp_input1">
														<input class="form-control" name="end_date" type="text" readonly placeholder="End Date">
														<span class="input-group-addon"><span class="fa fa-calendar"></span></span>
													</div>
												</div>
											</div>
										</div>
									<div class="usermanage-add clearfix">
										<input type="submit" class="btn btn-primary pull-right PullRcatagry" id="Create_Sim" value="Create Simulation">
									</div>
									</form>
								</div> 
							</div>
						</div>
					</div>
				</div>
				<div class="usermanage_main">
					<div class="table-box">
						<div class="tableheader clearfix">
						   <div class="tableheader_left">Simulation List </div>
						   <div class="tableheader_right">
								<div class="Searchbox">
									<form method="get">
										<input type="text" class="serchtext" name="search" placeholder="Search" required>
										<div class="serchBTN">
											<img class="img-responsive" src="img/dash_icon/search.svg">
										</div>
									</form>
								</div>
								<div class="btn-group btn-shortBY">
									<button type="button" class="btn btn-Transprate"><span class="sort">Sort by</span></button>
									<button type="button" class="btn btn-Transprate dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img  src="img/dash_icon/down_arrow.svg"></button>
									<div class="dropdown-menu shortBYMenu">
										<a class="dropdown-item" href="<?php echo $db->getBaseUrl($db->getFile()); ?>">Default Sorting</a>
										<a class="dropdown-item" href="<?php echo $db->getBaseUrl($db->getFile().'?orderby=category'); ?>">Category</a>
										<a class="dropdown-item" href="<?php echo $db->getBaseUrl($db->getFile().'?orderby=status'); ?>">Status</a>
										<a class="dropdown-item" href="<?php echo $db->getBaseUrl($db->getFile().'?orderby=stage'); ?>">Stage</a>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="usermanage-form1">
						<div class="table-responsive">
							<table class="table table-striped">
								<thead class="Theader">
									<tr>
										<th>Category Name</th>
										<th>Simulation Title</th>
										<th>Creation Date</th>
										<th>Author Name</th>
										<th>Reviewer Name</th>
										<th>Duration(min)</th>
										<th>Scenario Type</th>
										<th>Status</th>
										<th>Stage</th>
                                        <th colspan="5">Action</th>
									</tr>
								</thead>
								<tbody>
								<?php if ($results->execute() && $rowCount > 0):
									foreach ($results->fetchAll(PDO::FETCH_ASSOC) as $row):
										$sim_id 	= md5($row['scenario_id']);
										$sim_type	= $row['sim_ques_type'];
										$temp_type	= $row['sim_temp_type'];
									?>
									<tr>
										<td><?php echo $row['category']; ?></td>
										<td title="<?php echo $row['Scenario_title']; ?>"><?php echo $row['stitle']; ?>..</td>
										<td><?php echo $db->dateFormat($row['created_on'], 'd-M-y'); ?></td>
										<td><?php if ( ! empty($row['assign_author'])): ?>
											<a data-toggle="popover" title="View Author" href="<?php echo $db->getBaseUrl('includes/ajax.php?getTooltipData=true&ids='. $row['assign_author']) ?>" onclick="return false"><i class="fa fa-info-circle fa-2x" aria-hidden="true"></i></a>
											<?php endif; ?>
										</td>
										<td><?php if ( ! empty($row['assign_reviewer'])): ?>
											<a data-toggle="popover" title="View Reviewer" href="<?php echo $db->getBaseUrl('includes/ajax.php?getTooltipData=true&ids='. $row['assign_reviewer']) ?>" onclick="return false"><i class="fa fa-info-circle fa-2x" aria-hidden="true"></i></a>
											<?php endif; ?>
										</td>
										<td><?php echo $row['duration'] ?></td>
										<td><?php foreach ($db->getMultiScenarioType($row['scenario_type']) as $scenario_type): ?>
											<img src="<?php echo $type_icon_path . $scenario_type['type_icon'] ?>" class="type_icon" title="<?php echo $scenario_type['type_name'] ?>" />
											<?php endforeach; ?>
										</td>
										<td><?php echo ( ! empty($row['status'])) ? 'Active' : 'Inactive'; ?></td>
										<td><strong><?php echo $row['status_name']; ?></strong></td>
										<?php if ($row['status'] == 8 && ($temp_type == 1 || $temp_type == 2)): ?>
										<?php $publish_url = $db->getPublishPage($sim_type, $temp_type); ?>
										<td><a href="<?php echo $publish_url; ?>?view=true&type=zip&sim_id=<?php echo $sim_id; ?>" class="tooltiptop" target="_blank"><span class="tooltiptext">Publish as Web Object</span><img class="svg web_icon" src="<?php echo $action_icon_path ?>WEB.svg"></a></td>
                                		<td><a href="<?php echo $publish_url; ?>?view=true&type=scrom&sim_id=<?php echo $sim_id; ?>" class="tooltiptop" target="_blank"><span class="tooltiptext">SCORM Publish</span><img class="svg" src="<?php echo $action_icon_path ?>publish.svg"></a></td>
										<?php endif; ?>
										<td><a class="tooltiptop edit" data-scenario-id="<?php echo $sim_id; ?>" href="javascript:void(0);"><span class="tooltiptext">Edit Other Details</span><img class="svg" src="<?php echo $action_icon_path ?>edit.svg"></a></td>
										<?php if ($row['status'] != 5 && $row['status'] != 12): ?>
										<td><a class="tooltiptop" href="<?php echo $db->getEditAuthPage($row['sim_ques_type'], $row['sim_temp_type'], $sim_id); ?>"><span class="tooltiptext">Edit Simulation</span><img class="svg" src="<?php echo $action_icon_path ?>create_sim.svg"></a></td>
										<?php else: ?>
										<td><a class="disabled" href="JavaScript:void(0);" title="Edit Simulation"><img class="svg" src="<?php echo $action_icon_path ?>create_sim.svg"></a></td>
										<?php endif; ?>
										<td><a class="tooltiptop clone_sim" data-scenario-id="<?php echo $sim_id; ?>" href="javascript:void(0);"><span class="tooltiptext">Duplicate</span><img class="svg web_icon" src="<?php echo $action_icon_path ?>duplicate Sim.svg"></a></td>
										<?php /* if ($row['status'] == 8 && ($temp_type == 1 || $temp_type == 2)): ?>
										<td><a href="javascript:void(0);" data-scenario-id="<?php echo $sim_id; ?>" class="tooltiptop publishXML"><span class="tooltiptext">Generate XML</span><img class="svg" src="<?php echo $action_icon_path ?>xml.svg"></a></td>
										<?php endif; */ ?>
										<td><a class="tooltiptop" href="<?php echo $db->getPreviewAuthPage($row['sim_ques_type'], $row['sim_temp_type'], $sim_id); ?>" target="_blank"><span class="tooltiptext">Preview</span><img class="svg" src="<?php echo $action_icon_path ?>preview1.svg"></a></td>
										<td><a class="btncalltoaction tooltipLeft" href="<?php echo basename(__FILE__) ?>?delete=true&scenario_id=<?php echo base64_encode($row['scenario_id']) ?>" onclick="return confirm('Are you sure to delete this scenario');"><span class="tooltiptext">Delete</span><img class="svg" src="<?php echo $action_icon_path ?>delete.svg"></a></td>
									</tr>
									<?php endforeach; else: ?>
									<tr><td colspan="17" style="text-align:center"><strong>No data available in database.</strong></td></tr>
									<?php endif; ?>
								</tbody>
						   </table>
						</div>
						<div class="pull-right">
							<nav aria-label="navigation"><?php echo ( ! empty($pagination)) ? $pagination : '&nbsp;'; ?></nav>
						</div>
					</div>
				</div>
			</div>
		</div>
    </div>
</div>

<div id="load_popup_modal_show" class="modal fade" tabindex="-1"></div>

<script type="text/javascript">
$(function() {
	$('.multiselect-ui').multiselect({
		includeSelectAllOption: true,
		filterPlaceholder: 'Search & select users',
		enableCaseInsensitiveFiltering : true,
		enableFiltering: true,
		buttonWidth: '250px',
		maxHeight: 250
	})
});

function isNumberKey(evt) {
	var charCode = (evt.which) ? evt.which : evt.keyCode
	return !(charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57));
}

$('.edit').click(function () {
	var id = $(this).attr('data-scenario-id');
	if (id != '') {
		$.LoadingOverlay("show");
		var $modal = $('#load_popup_modal_show');
		$modal.load('edit-scenario-modal.php', {'data-id': id }, function(res) {
			if (res != '') {
				$.LoadingOverlay("hide");
				$modal.modal('show');
			}
			else {
				$.LoadingOverlay("hide");
				$("#showmsg").hide();
				swal("Error", 'Oops. something went wrong please try again.?', "error");
			}
		});
	}
});

$('.clone_sim').click(function () {
    var sim_id 		= $(this).attr('data-scenario-id');
	var dataString	= 'sim_id='+ sim_id + '&clone_sim='+ true;
    if (sim_id != '') {
		swal({
			title: "Are you sure?",
			text: "Clone this Simulation.",
			icon: "warning",
			buttons: [true, 'Clone'],
			dangerMode: true, }).then((willDelete) => { if (willDelete) {
				$.LoadingOverlay("show");
				$.ajax({
					url: "includes/process.php",
					type: "POST",
					data: dataString,
					cache: false,
					success: function(resdata) {
						var res = $.parseJSON(resdata);
						$.LoadingOverlay("hide");
						if (res.success == false) {
							swal({text: res.msg, buttons: false, icon: "error", timer: 3000});
						}
						else if (res.success == true) {
							swal({text: res.msg, buttons: false, icon: "success", timer: 3000});
							setTimeout(function() { window.location.reload(); }, 3000);
						}
					},error: function() {
						$.LoadingOverlay("hide");
						swal({text: oopsmsg, buttons: false, icon: "error", timer: 3000});
					}
				});
			}
		});
	}
});
</script>
<script src="content/js/tooltip.js"></script>
<script type="text/javascript" src="content/js/bootstrap-datetimepicker.js" charset="UTF-8"></script>
<script type="text/javascript">
	$('.form_datetime').datetimepicker({
		weekStart: 1,
		todayBtn:  0,
		autoclose: 1,
		todayHighlight: 1,
		startView: 2,
		forceParse: 0,
		minView: 2,
		showMeridian: 1,
		clearBtn: 1
	});
</script>
<?php 
require_once 'includes/footer.php';
