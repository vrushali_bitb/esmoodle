<?php 
session_start();
if (isset($_SESSION['username'])) {
    $user   = $_SESSION['username'];
    $role   = $_SESSION['role'];
    $userid = $_SESSION['userId'];
	$domain = (isset($_SESSION['domain_name'])) ? $_SESSION['domain_name'] : $_SERVER['HTTP_HOST'];
}
else {
    header('location: index.php');
}
require_once 'config/db.class.php';
require_once 'includes/learners_menu.php';
$db 		= new DBConnection();
$sim_id		= (isset($_GET['sim_id']) && ! empty($_GET['sim_id'])) ? $_GET['sim_id'] : '';
$ques_id	= ( ! empty($_GET['ques_id'])) ? base64_decode($_GET['ques_id']) : '';
$user_id	= ( ! empty($_GET['user_id'])) ? base64_decode($_GET['user_id']) : '';
$uid		= ( ! empty($_GET['uid'])) ? $_GET['uid'] : '';
$sim_data 	= $db->getScenario($sim_id);
$sim_com 	= $db->getScenarioCompetency($sim_id);
$simGroup	= $db->getAssignSimGroup($sim_data['scenario_id']);
$path		= 'scenario/upload/'.$domain.'/';
$vtheme		= 'city'; #city, fantasy, forest, sea
$aPoster	= $db->getBaseUrl('scenario/img/audio-poster.jpg');
$ques_val1 = $ques_val2 = $ques_val3 = $ques_val4 = $ques_val5 = $ques_val6 = 0;
if ( ! empty($ques_id)):
	$qsql = "SELECT * FROM question_tbl WHERE question_id = '". $ques_id ."'";
	try {
		$qstmt	 = $db->prepare($qsql); $qstmt->execute();
		$qrow	 = $qstmt->rowCount();
		$qresult = ($qrow > 0) ? $qstmt->fetch() : '';
	}
	catch(PDOException $e) {
		echo "Oops. something went wrong. ".$e->getMessage(); exit;
	}
endif; ?>
<link rel="stylesheet" type="text/css" href="content/css/qustemplate.css" />
<link rel="stylesheet" type="text/css" href="content/css/questionresponsive.css" />
<link rel="stylesheet" type="text/css" href="content/css/template-updated.css" />
<link rel="stylesheet" href="videojs/node_modules/video.js/dist/video-js.min.css" />
<link rel="stylesheet" href="videojs/node_modules/video.js/dist/themes/forest/index.css" />
<script type="text/javascript" src="videojs/node_modules/video.js/dist/video.min.js"></script>
<script type="text/javascript">
$.LoadingOverlay("show");
</script>
<style>
    .header { z-index: 999; }
    .col-sm-12.Questiontmpbanner { margin-top: 20px; }
    .close_panel { width: 54px; }
    [data-title] {
        font-size: 18px;
        position: relative;
        cursor: help;
    }
    [data-title]:hover::before {
        content: attr(data-title);
        position: absolute;
        bottom: -20px;
        padding: 0px;
        color: #2c3545;
        font-size: 14px;
        white-space: nowrap;
    }
    span.truncateTxt {
        width: 30px;
        white-space: nowrap;
        overflow: hidden;
        text-overflow: ellipsis;
        float: left;
    }
</style>
<form method="post" class="form-inline" name="add_sim_linear_multiple_details_form" id="add_sim_linear_multiple_details_form">
    <input type="hidden" name="add_score_open_response_sim" value="1" />
    <input type="hidden" name="sim_id" id="sim_id" value="<?php echo $sim_data['scenario_id'] ?>" />
    <input type="hidden" name="submit_type" id="submit_type" />
    <input type="hidden" name="ques_id" id="ques_id" value="<?php echo $ques_id; ?>" />
    <input type="hidden" name="uid" id="uid" value="<?php echo $uid; ?>" />
    <input type="hidden" name="question_type" id="question_type" value="<?php echo ( ! empty($qresult['question_type'])) ? $qresult['question_type'] : 9 ?>" />
    <input type="hidden" name="location" id="location" value="<?php echo $db->getBaseUrl($db->getFile().'?add_data=true&sim_id='. $sim_id .'&uid='. $uid .'&user_id='. base64_encode($user_id)); ?>" />
    <div class="question_tree_banner tab-content clearfix RORtemp">
    	<!--Add-Sim-Details-->
        <div class="main tab-pane fade in <?php echo (empty($user_id)) ? 'in active' : ''; ?>" id="main_menu">
            <div class="main_sim question_tree_menu">
                <div class="educaterSim educaterSim1 Simulation_det stopb clearfix">
                    <h3>Select Learner</h3>
                </div>
                <div class="Group_managment educater-banner">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group form-select">
                                <select class="form-control" name="group" id="group" data-placeholder="Select Sim Group">
                                    <option value="0" selected="selected" disabled="disabled">Select Group</option>
                                    <?php foreach ($db->getLearnerByGroup($simGroup) as $simGroup): ?>
                                    <option value="<?php echo $simGroup['group_id'] ?>"><?php echo $simGroup['group_name'] ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group form-select">
							<?php if ( ! empty($user_id)): ?>
                            <input type="hidden" name="learner" id="learner" value="<?php echo $user_id; ?>" />
                            <?php else: ?>
                            <select name="learner" id="learner" class="form-control multiselect-ui selection" data-placeholder="Learner List *">
                            </select>
                            <?php endif; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--Question-List-->
        <div class="Questio-template tab-pane fade <?php echo ( ! empty($user_id)) ? 'in active' : ''; ?>" id="questiontamp">
            <div class="main_sim_scroll">
                <div class="educaterSim Simulation_det stopb clearfix unsetp">
                    <div class="skilQue">
                    <?php if ( ! empty($sim_com['comp_col_1']) &&  ! empty($sim_com['comp_val_1'])): ?>
                    <div class="skills">
                        <label data-title="<?php echo $sim_com['comp_col_1'] ?>"><span class="truncateTxt"><?php echo $sim_com['comp_col_1'] ?></span></label>
                        <input type="text" class="form-control" id="comp_val_1" readonly="readonly" value="<?php echo $sim_com['comp_val_1']; ?>" />
                    </div>
                    <?php endif; ?>
                    
                    <?php if ( ! empty($sim_com['comp_col_2']) &&  ! empty($sim_com['comp_val_2'])): ?>
                    <div class="skills">
                        <label data-title="<?php echo $sim_com['comp_col_2'] ?>"><span class="truncateTxt"><?php echo $sim_com['comp_col_2'] ?></span></label>
                        <input type="text" class="form-control" id="comp_val_2" readonly="readonly" value="<?php echo $sim_com['comp_val_2']; ?>" />
                    </div>
                    <?php endif; ?>
                
                    <?php if ( ! empty($sim_com['comp_col_3']) &&  ! empty($sim_com['comp_val_3'])): ?>
                    <div class="skills">
                        <label data-title="<?php echo $sim_com['comp_col_3'] ?>"><span class="truncateTxt"><?php echo $sim_com['comp_col_3'] ?></span></label>
                        <input type="text" class="form-control" id="comp_val_3" readonly="readonly" value="<?php echo $sim_com['comp_val_3']; ?>" />
                    </div>
                    <?php endif; ?>
                
                    <?php if ( ! empty($sim_com['comp_col_4']) &&  ! empty($sim_com['comp_val_4'])): ?>
                    <div class="skills">
                        <label data-title="<?php echo $sim_com['comp_col_4'] ?>"><span class="truncateTxt"><?php echo $sim_com['comp_col_4'] ?></span></label>
                        <input type="text" class="form-control" id="comp_val_4" readonly="readonly" value="<?php echo $sim_com['comp_val_4']; ?>" />
                    </div>
                    <?php endif; ?>
                
                    <?php if ( ! empty($sim_com['comp_col_5']) &&  ! empty($sim_com['comp_val_5'])): ?>
                    <div class="skills">
                        <label data-title="<?php echo $sim_com['comp_col_5'] ?>"><span class="truncateTxt"><?php echo $sim_com['comp_col_5'] ?></span></label>
                        <input type="text" class="form-control" id="comp_val_5" readonly="readonly" value="<?php echo $sim_com['comp_val_5']; ?>" />
                    </div>
                    <?php endif; ?>
                
                    <?php if ( ! empty($sim_com['comp_col_6']) &&  ! empty($sim_com['comp_val_6'])): ?>
                    <div class="skills">
                        <label data-title="<?php echo $sim_com['comp_col_6'] ?>"><span class="truncateTxt"><?php echo $sim_com['comp_col_6'] ?></span></label>
                        <input type="text" class="form-control" id="comp_val_6" readonly="readonly" value="<?php echo $sim_com['comp_val_6']; ?>" />
                    </div>
                    <?php endif; ?>
                    </div>
                </div>
                <div id="wrapper">
                    <div class="width12 educaterwidth12" id="left-wrapper">
                        <div class="topbannericon"><img class="img-fluid closeleftmenuA" src="img/list/left_arrow.svg"></div>
                        <div class="question-list-banner">
                            <!--Start-Load-Question-List-->
                            <ul class="question-list" id="sortable"></ul>
                            <!--End-->
                        </div>
                    </div>
                   <?php $ques_type = ( ! empty($qresult['question_type'])) ? $qresult['question_type'] : ''; ?>
                   <div id="page-content-wrapper" class="OpenResp">
                        <div class="container-fluid">
                            <div class="row">
                                <div class="tab-content">
                                    <div class="Q-left educaterQ-left">
                                        <a href="#list-toggle" id="list-toggle" class="hideQTmp"><img class="close_panel" src="img/list/questiion_list.svg" /></a>
                                    </div>
                                    <?php 
                                    #---Questions
                                    $ques_att  = '';
                                    $qatt_type = '';
                                    if ( ! empty($qresult['qaudio'])):
                                        $qatt_type = 'qa';
                                        $ques_att = $qresult['qaudio'];
                                    endif;
                                    
                                    #--Questions-Assets
                                    $ques_assets = '';
                                    $assets_type = '';
                                    if ( ! empty($qresult['audio'])):
                                        $assets_type = 'a';
                                        $ques_assets = $qresult['audio'];
                                    elseif ( ! empty($qresult['video'])):
                                        $assets_type = 'v';
                                        $ques_assets = $qresult['video'];
                                    elseif ( ! empty($qresult['screen'])):
                                        $assets_type = 's';
                                        $ques_assets = $qresult['screen'];
                                    elseif ( ! empty($qresult['image'])):
                                        $assets_type = 'i';
                                        $ques_assets = $qresult['image'];
                                    elseif ( ! empty($qresult['document'])):
                                        $assets_type = 'd';
                                        $ques_assets = $qresult['document'];
                                    endif; ?>
                                    <!--Question-->
                                    <div id="MCQQuestion" class="tab-pane fade <?php echo ( ! empty($user_id) && ! empty($ques_id)) ? 'in active' : ''; ?>">
                                        <div class="container-fluid">
                                            <div class="widthQ100 classic-template educaterTemp">
                                                <div class="tab-content educater-banner_Qus">
                                                    <div id="match-Q" class="match-Q tab-pane fade in active">
                                                        <div class="quesbox CQtmp">
                                                            <div class="row">
                                                                <div class="col-sm-12 Questiontmpbanner">
                                                                    <div class="critical_div">
                                                                        <h3 class="qusHeading">QUESTION</h3>
                                                                        <div class="CIRtiCaltext">
                                                                            Critical Question: <input type="checkbox" name="mcq_criticalQ" id="mcq_criticalQ" class="form-control criticalQ" value="1" <?php echo ( ! empty($qresult['critical'])) ? 'checked="checked"' : ''; ?> disabled />
                                                                        </div>
                                                                    </div>
                                                                    <div class="quesbox-flex">
                                                                        <div class="form-group">
                                                                            <input type="text" name="mcq" id="mcq" class="form-control QuesForm inputtextWrap mcq" placeholder="Click here to enter question" value="<?php echo ( ! empty($qresult['questions'])) ? stripslashes($qresult['questions']) : ''; ?>" readonly="readonly" />
                                                                        </div>
                                                                    </div>
                                                                    <div class="q-icon-banner clearfix">
                                                                        <div class="Ques-comp">
                                                                            <h5>Competency Score</h5>
                                                                            <?php if ( ! empty($sim_com['comp_col_1']) &&  ! empty($sim_com['comp_val_1'])): ?>
                                                                            <div class="form-group QusScore">
                                                                                <input type="text" class="form-control mcq" name="mcq_ques_val_1" id="mcq_ques_val_1" onkeypress="return isNumberKey(event);" value="<?php echo ( ! empty($qresult['ques_val_1'])) ? $qresult['ques_val_1'] : ''; ?>" readonly="readonly" />
                                                                            </div>
                                                                            <input type="hidden" class="form-control" id="mcq_ques_val_1h" value="<?php echo ( ! empty($qresult['ques_val_1'])) ? $qresult['ques_val_1'] : ''; ?>" />
                                                                            <?php else: ?>
                                                                            <input type="hidden" class="form-control" name="mcq_ques_val_1" value="0" />
                                                                            <?php endif; ?>
                                                                            
                                                                            <?php if ( ! empty($sim_com['comp_col_2']) &&  ! empty($sim_com['comp_val_2'])): ?>
                                                                            <div class="form-group QusScore">
                                                                                <input type="text" class="form-control mcq" name="mcq_ques_val_2" id="mcq_ques_val_2" onkeypress="return isNumberKey(event);" value="<?php echo ( ! empty($qresult['ques_val_2'])) ? $qresult['ques_val_2'] : ''; ?>" readonly="readonly" />
                                                                            </div>
                                                                            <input type="hidden" class="form-control" id="mcq_ques_val_2h" value="<?php echo ( ! empty($qresult['ques_val_2'])) ? $qresult['ques_val_2'] : ''; ?>" />
                                                                            <?php else: ?>
                                                                            <input type="hidden" class="form-control" name="mcq_ques_val_2" value="0" />
                                                                            <?php endif; ?>
                                                                            
                                                                            <?php if ( ! empty($sim_com['comp_col_3']) &&  ! empty($sim_com['comp_val_3'])): ?>
                                                                            <div class="form-group QusScore">
                                                                                <input type="text" class="form-control mcq" name="mcq_ques_val_3" id="mcq_ques_val_3" onkeypress="return isNumberKey(event);" value="<?php echo ( ! empty($qresult['ques_val_3'])) ? $qresult['ques_val_3'] : ''; ?>" readonly="readonly" />
                                                                            </div>
                                                                            <input type="hidden" class="form-control" id="mcq_ques_val_3h" value="<?php echo ( ! empty($qresult['ques_val_3'])) ? $qresult['ques_val_3'] : ''; ?>" />
                                                                            <?php else: ?>
                                                                            <input type="hidden" class="form-control" name="mcq_ques_val_3" value="0" />
                                                                            <?php endif; ?>
                                                                            
                                                                            <?php if ( ! empty($sim_com['comp_col_4']) &&  ! empty($sim_com['comp_val_4'])): ?>
                                                                            <div class="form-group QusScore">
                                                                                <input type="text" class="form-control mcq" name="mcq_ques_val_4" id="mcq_ques_val_4" onkeypress="return isNumberKey(event);" value="<?php echo ( ! empty($qresult['ques_val_4'])) ? $qresult['ques_val_4'] : ''; ?>" readonly="readonly" />
                                                                            </div>
                                                                            <input type="hidden" class="form-control" id="mcq_ques_val_4h" value="<?php echo ( ! empty($qresult['ques_val_4'])) ? $qresult['ques_val_4'] : ''; ?>" />
                                                                            <?php else: ?>
                                                                            <input type="hidden" class="form-control" name="mcq_ques_val_4" value="0" />
                                                                            <?php endif; ?>
                                                                            
                                                                            <?php if ( ! empty($sim_com['comp_col_5']) &&  ! empty($sim_com['comp_val_5'])): ?>
                                                                            <div class="form-group QusScore">
                                                                                <input type="text" class="form-control mcq" name="mcq_ques_val_5" id="mcq_ques_val_5" onkeypress="return isNumberKey(event);" value="<?php echo ( ! empty($qresult['ques_val_5'])) ? $qresult['ques_val_5'] : ''; ?>" readonly="readonly" />
                                                                            </div>
                                                                            <input type="hidden" class="form-control" id="mcq_ques_val_5h" value="<?php echo ( ! empty($qresult['ques_val_5'])) ? $qresult['ques_val_5'] : ''; ?>" />
                                                                            <?php else: ?>
                                                                            <input type="hidden" class="form-control" name="mcq_ques_val_5h" value="0" />
                                                                            <?php endif; ?>
                                                                            
                                                                            <?php if ( ! empty($sim_com['comp_col_6']) &&  ! empty($sim_com['comp_val_6'])): ?>
                                                                            <div class="form-group QusScore">
                                                                                <input type="text" class="form-control mcq" name="mcq_ques_val_6" id="mcq_ques_val_6" onkeypress="return isNumberKey(event);" value="<?php echo ( ! empty($qresult['ques_val_6'])) ? $qresult['ques_val_6'] : ''; ?>" readonly="readonly" />
                                                                            </div>
                                                                            <input type="hidden" class="form-control" id="mcq_ques_val_6h" value="<?php echo ( ! empty($qresult['ques_val_6'])) ? $qresult['ques_val_6'] : ''; ?>" />
                                                                            <?php else: ?>
                                                                            <input type="hidden" class="form-control" name="mcq_ques_val_6" value="0" />
                                                                            <?php endif; ?>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="Ansbox">
                                                            <div class="row">
                                                                <div class="col-sm-12">
                                                                    <div class="col-sm-6">
                                                                    	<h3 class="AnsHeading">Answer</h3>
                                                                    </div>
                                                                    <div class="matchingchice">
                                                                        <div class="ApendOption-box">
                                                                        	<div class="MatchOption-box1 MatchOption-box1Pading clearfix">
                                                                            <?php if ( ! empty($qresult['question_id']) && ! empty($uid)):
																			$ans_sql	= "SELECT * FROM open_res_tbl WHERE uid = '". $uid ."' AND question_id = '". $qresult['question_id'] ."' AND userid = '". $user_id ."'";
                                                                            $qexe 	 	= $db->prepare($ans_sql); $qexe->execute();
																			$arow 		= $qexe->rowCount();
                                                                            $ansData 	= $qexe->fetch(PDO::FETCH_ASSOC);
                                                                            if ( ! empty($ansData['file_name'])):
                                                                                $qext = strtolower(pathinfo($ansData['file_name'], PATHINFO_EXTENSION));
                                                                                if ($qext == 'webm' || $qext == 'mp4' || $qext == 'x-matroska'): ?>
                                                                                <video id="qvideo-1" class="video-js vjs-theme-<?php echo $vtheme ?>" controls preload="auto" width="500px" height="264px" data-setup="{}" data-type="video"><source src="<?php echo $path . $ansData['file_name'] ?>" type="video/<?php echo strtolower(pathinfo($ansData['file_name'], PATHINFO_EXTENSION)) ?>" /></video>
                                                                                <?php elseif ($qext == 'mp3' || $qext == 'MP3'): ?>
                                                                                <audio id="qvideo-1" class="video-js vjs-theme-<?php echo $vtheme ?>" controls preload="auto" width="350px" height="180px" data-setup="{}" data-type="audio" poster="<?php echo $aPoster ?>"><source src="<?php echo $path . $ansData['file_name'] ?>" type="audio/<?php echo strtolower(pathinfo($ansData['file_name'], PATHINFO_EXTENSION)) ?>" /></audio>
                                                                                <?php endif; elseif ( ! empty($ansData['tts_data'])): ?>
                                                                                <textarea class="form-control FeedForm inputtextWrap" readonly="readonly"><?php echo $ansData['tts_data'] ?></textarea>
                                                                                <?php elseif ( ! empty($ansData['code'])): ?>
                                                                                <textarea class="form-control FeedForm inputtextWrap code_data" id="code_data_<?php echo $qresult['question_id'] ?>"><?php echo htmlentities(base64_decode($ansData['code'])); ?></textarea>
                                                                                <?php endif; ?>
                                                                                <input type="hidden" name="open_res_id" value="<?php echo $ansData['open_res_id'] ?>" />
                                                                                <div class="Ques-comp">
                                                                                    <h5>Add Score</h5>
                                                                                    <?php if ( ! empty($sim_com['comp_col_1']) &&  ! empty($sim_com['comp_val_1'])): ?>
                                                                                    <div class="form-group QusScore">
                                                                                        <input type="text" class="form-control score mcq" name="mcq_ans_val1" id="mcq_ans_val1" onkeypress="return isNumberKey(event);" value="<?php echo ( ! empty($ansData['ans_val1'])) ? $ansData['ans_val1'] : ''; ?>" />
                                                                                    </div>
                                                                                    <?php endif; ?>
                                                                                    <?php if ( ! empty($sim_com['comp_col_2']) &&  ! empty($sim_com['comp_val_2'])): ?>
                                                                                    <div class="form-group QusScore">
                                                                                        <input type="text" class="form-control score mcq" name="mcq_ans_val2" id="mcq_ans_val2" onkeypress="return isNumberKey(event);" value="<?php echo ( ! empty($ansData['ans_val2'])) ? $ansData['ans_val2'] : ''; ?>" />
                                                                                    </div>
                                                                                    <?php endif; ?>
                                                                                    <?php if ( ! empty($sim_com['comp_col_3']) &&  ! empty($sim_com['comp_val_3'])): ?>
                                                                                    <div class="form-group QusScore">
                                                                                        <input type="text" class="form-control score mcq" name="mcq_ans_val3" id="mcq_ans_val3" onkeypress="return isNumberKey(event);" value="<?php echo ( ! empty($ansData['ans_val3'])) ? $ansData['ans_val3'] : ''; ?>" />
                                                                                    </div>
                                                                                    <?php endif; ?>
                                                                                    <?php if ( ! empty($sim_com['comp_col_4']) &&  ! empty($sim_com['comp_val_4'])): ?>
                                                                                    <div class="form-group QusScore">
                                                                                        <input type="text" class="form-control score mcq" name="mcq_ans_val4[]" id="mcq_ans_val4" onkeypress="return isNumberKey(event);" value="<?php echo ( ! empty($ansData['ans_val4'])) ? $ansData['ans_val4'] : ''; ?>" />
                                                                                    </div>
                                                                                    <?php endif; ?>
                                                                                    <?php if ( ! empty($sim_com['comp_col_5']) &&  ! empty($sim_com['comp_val_5'])): ?>
                                                                                    <div class="form-group QusScore">
                                                                                        <input type="text" class="form-control score mcq" name="mcq_ans_val5" id="mcq_ans_val5" onkeypress="return isNumberKey(event);" value="<?php echo ( ! empty($ansData['ans_val5'])) ? $ansData['ans_val5']: ''; ?>" />
                                                                                    </div>
                                                                                    <?php endif; ?>
                                                                                    <?php if ( ! empty($sim_com['comp_col_6']) &&  ! empty($sim_com['comp_val_6'])): ?>
                                                                                    <div class="form-group QusScore">
                                                                                        <input type="text" class="form-control score mcq" name="mcq_ans_val6" id="mcq_ans_val6" onkeypress="return isNumberKey(event);" value="<?php echo ( ! empty($ansData['ans_val6'])) ? $ansData['ans_val6'] : ''; ?>" />
                                                                                    </div>
                                                                                    <?php endif; ?>
                                                                                </div>
                                                                           	<?php endif; ?>
                                                                            </div>
                                                                            <?php 
																			if ( ! empty($qresult['question_id']) && ! empty($uid)):
																				if ($arow > 0):
																					$feed_assets = '';
																					$feed_type   = '';
																					if ( ! empty($ansData['file_name_feedback'])):
																						$feed_assets = $ansData['file_name_feedback'];
																						$feed_type   = 'file';
																					elseif ( ! empty($ansData['tts_data_feedback'])):
																						$feed_assets = $ansData['tts_data_feedback'];
																						$feed_type   = 'text';
																					endif;
																				endif; ?>
                                                                            <input type="hidden" name="file_name_1" id="file_name_1" <?php echo ($feed_type == 'file' && ! empty($feed_assets)) ? 'value="'. $feed_assets .'"' : ''; ?> />
																			<input type="hidden" name="tts_data_1" id="tts_data_1" <?php echo ($feed_type == 'text' && ! empty($feed_assets)) ? 'value="'. $feed_assets .'"' : ''; ?> />
                                                                            <div class="OR-mainboxA OR-mainboxAli" id="open_res_answer_1" <?php echo (empty($feed_assets)) ? 'style="display:none;"' : ''; ?>>
                                                                            	<div class="ORyouranswer"> Your Feedback 
                                                                                    <a href="javascript:void(0);" <?php echo ($feed_type == 'file' && ! empty($feed_assets)) ? '' : 'style="display:none;"'; ?> data-assets="<?php echo ($feed_type == 'file' && ! empty($feed_assets)) ? $feed_assets : ''; ?>" data-assets-id="1" data-input-id="#file_name_1" data-path="../<?php echo $path ?>" class="delete_assets" title="Delete"><i class="fa fa-times" aria-hidden="true"></i></a>
                                                                                	<a href="javascript:void(0);" <?php echo ($feed_type == 'text' && ! empty($feed_assets)) ? '' : 'style="display:none;"'; ?> data-assets-id="1" data-input-id="#tts_data_1" class="delete_tts" title="Delete"><i class="fa fa-times" aria-hidden="true"></i></a>
                                                                                </div>
                                                                                <?php if ($feed_type == 'file' && ! empty($feed_assets)):
																						$qext = strtolower(pathinfo($feed_assets, PATHINFO_EXTENSION));
                                                                                		if ($qext == 'webm' || $qext == 'mp4' || $qext == 'x-matroska'): ?>
                                                                                        <video id="qvideo-1" class="video-js vjs-theme-<?php echo $vtheme ?>" controls preload="auto" width="500px" height="264px" data-setup="{}" data-type="video"><source src="<?php echo $path . $feed_assets ?>" type="video/<?php echo strtolower(pathinfo($feed_assets, PATHINFO_EXTENSION)) ?>" /></video>
                                                                                		<?php elseif ($qext == 'mp3' || $qext == 'MP3'): ?>
                                                                                		<audio id="qvideo-1" class="video-js vjs-theme-<?php echo $vtheme ?>" controls preload="auto" width="350px" height="180px" data-setup="{}" data-type="audio" poster="<?php echo $aPoster ?>"><source src="<?php echo $path . $feed_assets ?>" type="audio/<?php echo strtolower(pathinfo($feed_assets, PATHINFO_EXTENSION)) ?>" /></audio>
                                                                                		<?php endif; ?>
                                                                                <?php else: ?>
                                                                              	<div class="ORVideobox" style="display:none;"></div>
                                                                                <div class="ORAudiobox" style="display:none;"></div>
                                                                                <?php endif; ?>
                                                                                <div class="ORTextbox" <?php echo ($feed_type == 'text' && ! empty($feed_assets)) ? '' : 'style="display:none;"'; ?>><?php echo ($feed_type == 'text' && ! empty($feed_assets)) ? $feed_assets : ''; ?></div>
                                                                            </div>
                                                                            <div class="ORsubmitbox zoomIn fade-in" id="action_1" <?php echo ( ! empty($feed_assets)) ? 'style="display:none;"' : ''; ?>>
                                                                            	<div class="instance">Select an option to give your feedback</div>
                                                                                <div class="ORgraaybox">
                                                                                	<div class="ORbox ORbox1" data-toggle="tooltip" data-placement="top" title="Text only"><img class="img-fluid add-tts-educator-feedback" data-box-id="1" src="img/list/text_only.svg" /></div>
                                                                                    <div class="ORbox ORbox2" data-toggle="tooltip" data-placement="top" title="Screen record"><img class="img-fluid rec-screen-educator-feedback" data-box-id="1" src="img/list/screen_record1.svg" /></div>
                                                                                    <div class="ORbox ORbox3" data-toggle="tooltip" data-placement="top" title="Voice record"><img class="img-fluid rec-audio-educator-feedback" data-box-id="1" src="img/list/voice_record.svg" /></div>
                                                                                    <div class="ORbox ORbox4" data-toggle="tooltip" data-placement="top" title="Video record"><img class="img-fluid rec-video-educator-feedback" data-box-id="1" src="img/list/video_record.svg" /></div>
                                                                                 </div>
                                                                            </div>
                                                                            <?php endif; ?>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
	</div>
    <div class="sim_bluebanner">
    	<ul class="Ques_linear_save">
			<?php if ( ! empty($user_id)): ?>
        	<li><button type="submit" class="submitques update_next"><img src="img/list/save-t.svg" />SAVE & CONTINUE</button></li>
            <li><button type="submit" class="submitques update_close"><img src="img/list/close.svg" />SAVE & CLOSE</button></li>
			<?php else: ?>
			<li><button type="submit" class="submitques update_next"><img src="img/list/save-t.svg" />CONTINUE</button></li>
			<?php endif; ?>
        </ul>
    </div>
</form>

<div id="load_popup_modal_show" class="modal educaterpopupmodl fade" tabindex="-1"></div>

<script src="content/js/jquery-ui-1.12.1/jquery-ui.js"></script>
<script type="text/javascript">
$(function(){
    $('.code_data').each(function(e){
        CKEDITOR.replace( this.id, { customConfig: 'config-open-response.js' });
    });
});

$('#learner').multiselect({
	includeSelectAllOption: false,
	filterPlaceholder: 'Search & select learner',
	enableCaseInsensitiveFiltering : true,
	enableFiltering: true,
	buttonWidth: '250px',
	maxHeight: 250
});

function isNumberKey(evt) {
	var charCode = (evt.which) ? evt.which : evt.keyCode
	return !(charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57));
}

$(".closeleftmenuA").click(function(e) {
	e.preventDefault();
	$("#list-toggle").addClass("changebtnleft");
	$("#wrapper").addClass("toggledlist");
	$(".match-Q.tab-pane.fade").addClass("tabpadding150");
	$(".Q-left").addClass("left150");
	$(".widthQ100").addClass("TogglelistwidthQ100 educaterwidthQ100");
	$(".main_sim_scroll").addClass("TogglelistwidthQ100");	
	$(".modal.Ques-pop-style.in ").addClass("TogglelistwidthQ100");
    $(".list-icon").addClass("Labsolute");
    $(".OpenResp").addClass("educttempOpenResp");			
});

$("#list-toggle").click(function(e) {
	e.preventDefault();
	$("#list-toggle").toggleClass("changebtnleft");
	$("#wrapper").toggleClass("toggledlist");
	$(".match-Q.tab-pane.fade").toggleClass("tabpadding150");
	$(".Q-left").toggleClass("left150");
	$(".widthQ100").toggleClass("TogglelistwidthQ100 educaterwidthQ100");	
	$(".main_sim_scroll").toggleClass("TogglelistwidthQ100");		
	$(".modal.Ques-pop-style.in ").toggleClass("TogglelistwidthQ100");
    $(".list-icon").toggleClass("Labsolute");
    $(".OpenResp").toggleClass("educttempOpenResp");		
});

/* Get Questions List */
function getQuesList() {
	$.ajax({
		type: 'GET',
		url: 'includes/process.php',
		data: 'getQuesList=true&sim_id=<?php echo $sim_id ?>',
		success:function(resdata) {
			$.LoadingOverlay("hide");
			var res = $.parseJSON(resdata);
			if (res.success == true) {
				var base 	= $('#location').val();
				var current	= window.location.href;
				var qurl 	= '';
				var qclass	= '';
				$('.question-list').empty();
				$.each(res.data, function (key, val) {
					qurl = base + '&ques_id='+ val.ques_id;
					if (current == qurl){
						qclass = 'active';
					} else {
						qclass = '';
					}
					var qlist = '<label class="ui-state-default '+ qclass +'" id="question-'+ val.qid +'"><a href="'+ qurl +'"><li>'+ val.qname +'</li></a></label>';
					$('.question-list').append(qlist);
				});
			} else {
				swal({text: 'Questions list not load please try again later.', buttons: false, icon: "error", timer: 2000 });
			}
		},error: function() {
			$.LoadingOverlay("hide");
			swal({text: 'Oops. something went wrong please try again.', buttons: false, icon: "error", timer: 2000 });
		}
	});
}

getQuesList();

$('.update_next').on('click', function(){
	$('#submit_type').val(1);
});

$('.update_close').on('click', function(){
	$('#submit_type').val(2);
});

//------Competency-Score-------
$('#mcq_ans_val1').on('change', function(){
	var score = parseInt($(this).val());
	var maxallow = parseInt($('#mcq_ques_val_1').val());
    if (maxallow < score) {
		$(this).val('');
		swal('Maximum '+maxallow+' score', { buttons: false, timer: 1000 });
	}
});

$('#mcq_ans_val2').on('change', function(){
	var score = parseInt($(this).val());
	var maxallow = parseInt($('#mcq_ques_val_2').val());
	if (score > maxallow) {
		$(this).val('');
		swal('Maximum '+maxallow+' score', { buttons: false, timer: 1000 });
	}
});

$('#mcq_ans_val3').on('change', function(){
	var score = parseInt($(this).val());
	var maxallow = parseInt($('#mcq_ques_val_3').val());
	if (score > maxallow) {
		$(this).val('');
		swal('Maximum '+maxallow+' score', { buttons: false, timer: 1000 });
	}
});

$('#mcq_ans_val4').on('change', function(){
	var score = parseInt($(this).val());
	var maxallow = parseInt($('#mcq_ques_val_4').val());
	if (score > maxallow) {
		$(this).val('');
		swal('Maximum '+maxallow+' score', { buttons: false, timer: 1000 });
	}
});

$('#mcq_ans_val5').on('change', function(){
	var score = parseInt($(this).val());
	var maxallow = parseInt($('#mcq_ques_val_5').val());
	if (score > maxallow) {
		$(this).val('');
		swal('Maximum '+maxallow+' score', { buttons: false, timer: 1000 });
	}
});

$('#mcq_ans_val6').on('change', function(){
	var score = parseInt($(this).val());
	var maxallow = parseInt($('#mcq_ques_val_6').val());
	if (score > maxallow) {
		$(this).val('');
		swal('Maximum '+maxallow+' score', { buttons: false, timer: 1000 });
	}
});

$(".inputtextWrap").hover(function() {
	$(this).attr('title', $(this).val());
}, function() {
	$(this).css('cursor', 'auto');
});

$('#group').on('change', function(){
	var gid = $(this).val();
	if (gid != '')
		$.LoadingOverlay("show");
		$('#learner').empty();
		$('#learner').multiselect('refresh');
		$.getJSON('includes/ajax.php?getLearnerByGroup=true&group_id='+ gid, function(res) {
			$.LoadingOverlay("hide");
			if (res.success == true && res.data != null) {
				$('#learner').multiselect('dataprovider', res.data);
			}
			else if (res.success == false) {
				$('#learner').multiselect('rebuild');
				$('#learner').multiselect('refresh');
			}
		});
});

//----------------Add-TTS------------------
$('body').on('click', '.add-tts-educator-feedback', function(){
	var box_id	= $(this).attr('data-box-id');
	if (box_id != '') {
		$.LoadingOverlay("show");
		var $modal = $('#load_popup_modal_show');
		$modal.load('tts-open-response-modal.php', { 'box_id': box_id }, function(res) {
			$.LoadingOverlay("hide");
			if (res != '') {
				$modal.modal('show', {backdrop: 'static', keyboard: false});
			}
			else {
				swal({text: 'Oops, something went wrong. Please try again later', buttons: false, icon: "error", timer: 1000});
			}
		});
	}
});

//-----------Record-Audio-----------------
$('body').on('click', '.rec-audio-educator-feedback', function() {
	var box_id	= $(this).attr('data-box-id');
	var qid		= $(this).attr('data-qid');
	var type	= $(this).attr('data-qtype');
	if (box_id != '') {
		$.LoadingOverlay("show");
		var $modal = $('#load_popup_modal_show');
		$modal.load('rec-audio-open-response-modal.php', { 'box_id': box_id }, function(res) {
			$.LoadingOverlay("hide");
			if (res != '') {
				$modal.modal('show', {backdrop: 'static', keyboard: false});
			}
			else {
				swal({text: 'Oops, something went wrong. Please try again later', buttons: false, icon: "error", timer: 1000});
			}
		});
	}
});

//-----------Record-Video----------------
$('body').on('click', '.rec-video-educator-feedback', function() {
	var box_id	= $(this).attr('data-box-id');
	var qid		= $(this).attr('data-qid');
	var type	= $(this).attr('data-qtype');
	if (box_id != '') {
		$.LoadingOverlay("show");
		var $modal = $('#load_popup_modal_show');
		$modal.load('rec-video-open-response-modal.php', { 'box_id': box_id }, function(res) {
			$.LoadingOverlay("hide");
			if (res != '') {
				$modal.modal('show', {backdrop: 'static', keyboard: false});
			}
			else {
				swal({text: 'Oops, something went wrong. Please try again later', buttons: false, icon: "error", timer: 1000});
			}
		});
	}
});

//-----------Record-Screen---------------
$('body').on('click', '.rec-screen-educator-feedback', function() {
	var box_id	= $(this).attr('data-box-id');
	if (box_id != '') {
		$.LoadingOverlay("show");
		var $modal = $('#load_popup_modal_show');
		$modal.load('rec-screen-open-response-modal.php', { 'box_id': box_id }, function(res) {
			$.LoadingOverlay("hide");
			if (res != '') {
				$modal.modal('show', {backdrop: 'static', keyboard: false});
			}
			else {
				swal({text: 'Oops, something went wrong. Please try again later', buttons: false, icon: "error", timer: 1000});
			}
		});
	}
});

//--------------Delete-Assets-------------
$('body').on('click', '.delete_assets', function() {
	var cur   		= $(this);
	var file_name	= cur.attr('data-assets');
	var assetsId	= cur.attr('data-assets-id');
	var inputId		= cur.attr('data-input-id');
	var path		= cur.attr('data-path');
	var dataString  = 'delete_assets='+ true +'&assets_path='+ path +'&file='+ file_name;
	if (file_name){
		swal({
			title: "Are you sure?",
			text: "Delete this answer.",
			icon: "warning",
			buttons: [true, 'Delete'],
			dangerMode: true, }).then((willDelete) => { if (willDelete) {
				$.LoadingOverlay("show");
				$.ajax({
					url: "includes/delete-file.php",
					type: "POST",
					data: dataString,
					cache: false,
					success: function(resdata){
						var res = $.parseJSON(resdata);
						if (res.success == false) {
							$.LoadingOverlay("hide");
							swal({text: res.msg, buttons: false, icon: "error", timer: 1000});
						}
						else if (res.success == true) {
							$(inputId).val('');
							$('#open_res_answer_'+ assetsId).hide();
							$('#open_answer_btn_'+ assetsId).hide();
							$('#action_'+ assetsId).show();
							$.LoadingOverlay("hide");
							swal({text: res.msg, buttons: false, timer: 1000});
						}
					},error: function() {
						swal({text: 'Oops. something went wrong please try again.', buttons: false, icon: "error", timer: 1000});
					}
				});
			} else { swal({text: 'Your file is safe', buttons: false, timer: 1000}); }
		});
	}
});

//--------------Delete-TTS-------------
$('body').on('click', '.delete_tts', function() {
	var cur   		= $(this);
	var assetsId	= cur.attr('data-assets-id');
	var inputId		= cur.attr('data-input-id');
	swal({
		title: "Are you sure?",
		text: "Delete this Text.",
		icon: "warning",
		buttons: [true, 'Delete'],
		dangerMode: true, }).then((willDelete) => { if (willDelete) {
			$.LoadingOverlay("show");
			$('#tts_data_'+ assetsId).val('');
			$('#open_res_answer_'+ assetsId).hide();
			$('#open_answer_btn_'+ assetsId).hide();
			$('#open_res_answer_'+ assetsId +' .ORTextbox').hide().html('');
			$('#action_'+ assetsId).show();
			$.LoadingOverlay("hide");
		} else { 
			$.LoadingOverlay("hide");
			swal({text: 'Your Text Data is safe', buttons: false, timer: 1000}); 
		}
	});
});

document.onreadystatechange = function() {
	if (document.readyState == "complete"){
		$.LoadingOverlay("hide");
	}
};
</script>
<?php 
require_once 'includes/footer.php';
