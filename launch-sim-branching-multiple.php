<?php 
ob_start();
session_start();
if (isset($_SESSION['username'])):
	$user 	= $_SESSION['username'];
	$role 	= $_SESSION['role'];
	$userid = $_SESSION['userId'];
	$domain = (isset($_SESSION['domain_name'])) ? $_SESSION['domain_name'] : $_SERVER['HTTP_HOST'];
else:
    header('location: index.php');
endif;
require_once 'config/db.class.php';
require_once 'includes/header.php';
$db 		= new DBConnection();
$sim_id		= (isset($_GET['sim_id'])) ? $_GET['sim_id'] : die('Error');
$ques_id	= (isset($_GET['ques_id'])) ? base64_decode($_GET['ques_id']) : '';
$save		= (isset($_GET['save']) && $_GET['save'] == 'false') ? FALSE : TRUE;
$simData	= $db->getScenario($sim_id);
$simID 		= $simData['scenario_id'];
$brand		= $db->getSimBranding($simID);
$uid 		= $db->UID();
if ($save):
	$tAttempt	= $db->getTotalAttempt($simID, $userid);
	$attempt	= $db->getAssignSimOtherData($simID, $userid)[0];
	if ( ! empty($attempt['no_attempts']) && $tAttempt >= $attempt['no_attempts']):
		echo "<script>alert('Number of attempts of this simulation is completed.');</script>";
		echo "<script>window.location.href='learner-dashboard.php';</script>";
		exit;
	endif;
endif;
$back_img	= ( ! empty($simData['sim_back_img'])) ? 'img/char_bg/'. $simData['sim_back_img'] : '';
$path		= $db->getBaseUrl('scenario/upload/'.$domain.'/');
$root_path	= $db->rootPath() . 'scenario/upload/'.$domain.'/';
$aPoster	= $db->getBaseUrl('scenario/img/audio-poster.jpg');
$noImage	= $db->getBaseUrl('scenario/img/palceholder.jpg');
$vtheme		= 'forest'; #city, fantasy, forest, sea
$left_char	= $right_char = '';
if ( ! empty($simData['sim_char_img'])):
	$left_char = 'img/char_bg/'. $simData['sim_char_img'];
elseif ( ! empty($simData['sim_char_left_img'])):
	$left_char = 'img/char_bg/'. $simData['sim_char_left_img'];
elseif ( ! empty($simData['char_own_choice']) && file_exists($root_path . $simData['char_own_choice'])):
	$left_char = $path . $simData['char_own_choice'];
endif;
if ( ! empty($simData['sim_char_right_img'])):
	$right_char = 'img/char_bg/'. $simData['sim_char_right_img'];
endif;
if ( ! empty($brand['font_type'])):
$type = str_replace('+', ' ', $brand['font_type']); ?>
<style type="text/css">
@import url("https://fonts.googleapis.com/css2?family=<?php echo $brand['font_type'] ?>:wght@100;200;300;400;500;600;700;800;900&display=swap");
body { font-family: '<?php echo $type ?>' !important; }
h1,h2,h3,h4,h5,h6 { font-family: '<?php echo $type ?>' !important; }
p { font-family: '<?php echo $type ?>' !important; }
</style>
<?php endif; ?>
<script type="text/javascript">
	window.addEventListener('beforeunload', function (e) {
	  // Cancel the event
	  e.preventDefault(); // If you prevent default behavior in Mozilla Firefox prompt will always be shown
	  // Chrome requires returnValue to be set
	  e.returnValue = '';
	});
	
	function disableF5(e) { if ((e.which || e.keyCode) == 116 || (e.which || e.keyCode) == 82) e.preventDefault(); };
	
	$(document).ready(function(){
		$(document).on("keydown", disableF5);
	});
	
	$(document).bind("contextmenu", function(e){
		return false;
	});
	
	history.pushState(null, null, location.href);
    window.onpopstate = function() {
        history.go(1);
    };
	
	var simBrand = <?php echo json_encode($brand); ?>

	$.LoadingOverlay("show");
</script>
<style type="text/css">
    .Dflex .multiDBDbox { height: auto !important; }
<?php if ( ! empty($back_img)): ?>
	.linear_classic {
		background: url("<?php echo $back_img ?>") center top no-repeat;
		background-size: cover;
		overflow:hidden;
		display: flex;
		height:100vh;
	}
	.bordered { background: #abdd89; }
	.swappable { cursor:pointer; }
<?php elseif ( ! empty($simData['bg_color'])): ?>
	.linear_classic { background: <?php echo $simData['bg_color']; ?>; display:flex;}
<?php endif; if ( ! empty($brand['option_bg'])): ?>
	.questioninput:before { background: <?php echo $brand['option_bg']; ?>; }
	.questioninput:after { background: <?php echo $brand['option_bg']; ?>; }
	.rectangle1:after { background: <?php echo $brand['option_bg']; ?>; }
<?php endif; ?>
</style>
<div class="mobileroteateimg"></div>
<div class="menu-banner-box">
	<div class="time-banner-box">
    	<button class="V_S-tetail" id="view_sim" data-sim-id="<?php echo $sim_id; ?>">
    		<img class="Deskview" src="img/list/view_scenario_b.svg" />
        	<img class="mobview" src="img/list/view_scenario.svg" />
        	View Scenario
    	</button>
  	</div>
    <!---SIM-Timer-->
    <div class="timer-banner">
    	<div class="time-text">
        	<span class="timer-pause" data-minutes-left="<?php echo $simData['duration'] ?>" style="pointer-events: none;"></span>
    	</div>
  	</div>
	  <?php if ($save): ?>
    <div class="close-banner">
    	<img class="svg" src="img/list/close_learner.svg" />
    </div>
	<?php endif; ?>
</div>
<script type="text/javascript">
var timer = $('.timer-pause').startTimer({
  onComplete: function(element){
	  <?php if ($save): ?>
		$('.signbtn1').show();
	  <?php else: ?>
		swal({text: "Simulation time end. please relaunch.!", icon: "info"});
	  <?php endif; ?>
  },
  allowPause: true
});
</script>
<div class="linear_classic">
<input type="hidden" name="uid" id="uid" value="<?php echo $uid; ?>" />
	<input type="hidden" name="simId" id="simId" value="<?php echo $simID; ?>" />
	<input type="hidden" name="userid" id="userid" value="<?php echo $userid; ?>" />
	<input type="hidden" name="save" id="save" value="<?php echo $save; ?>" />
	<input type="hidden" name="apiurl" id="apiurl" value='<?php echo $db->getBaseUrl('api-process.php') ?>' />
	<?php if ( ! empty($left_char)): ?>
	<div class="CharFlex <?php echo ( ! empty($right_char)) ? 'CharFlextwo' : ''; ?>"><img class="img-fulid qusTempimg" src="<?php echo $left_char; ?>" /></div>
	<?php endif; ?>
	<div class="flexQTeMP <?php echo ( ! empty($right_char)) ? 'flexQTeMPtwochar' : ''; ?><?php echo ( ! empty($left_char) && empty($right_char)) ? 'flexQTeMPonechar' : ''; ?><?php echo (empty($left_char) && empty($right_char)) ? 'flexQTeMPnochar' : ''; ?>">
	<?php 
		$qSql = $db->prepare("SELECT * FROM question_tbl WHERE scenario_id = '". $simID ."'"); $qSql->execute();
		$totalQ = $qSql->rowCount();
		if ($totalQ > 0): $q = 1;
			$qdata = $qSql->fetchAll(PDO::FETCH_ASSOC);
			foreach ($qdata as $qrow):
				$shuffle = ( ! empty($qrow['shuffle'])) ? TRUE : FALSE;
				$ansData = $db->getAnswersByQuestionId(md5($qrow['question_id']), $shuffle);
				$qtype	 = $qrow['question_type'];
				if ( ! empty($ques_id) && $ques_id == $qrow['question_id']):
					$show = 'style="display:block"';
				elseif (empty($ques_id) && $q == 1):
					$show = 'style="display:block"';
				else:
					$show = 'style="display:none"';
				endif; ?>
				<div class="MatchingQusBannerlunch multimatchingB branchingQuestions<?php echo $qrow['question_id']; ?>" <?php echo $show; ?>>
					<div class="Matchingcontainer">
						<div class="Questmtf">
							<div class="MatchingQusText fade-in">
								<div class="qus-text">
									<h3>QUESTION <?php echo $q .'/'. $totalQ; ?></h3>
									<p class="more"><?php echo stripslashes($qrow['questions']); ?></p>
								</div>
								<div class="qusassestbox">
									<div class="Qusimg">
									<?php if ( ! empty($qrow['audio']) && file_exists($root_path . $qrow['audio'])): ?>
									<audio id="qvideo-<?php echo $q ?>" class="video-js vjs-theme-<?php echo $vtheme ?>" controls preload="auto" width="300px" height="100px" data-setup="{}" data-type="audio" poster="<?php echo $aPoster ?>">
										<source src="<?php echo $path . $qrow['audio'] ?>" type="audio/<?php echo strtolower(pathinfo($qrow['audio'], PATHINFO_EXTENSION)); ?>" />
									</audio>
									<?php elseif ( ! empty($qrow['video']) && file_exists($root_path . $qrow['video'])): ?>
									<a href="<?php echo $path . $qrow['video']; ?>" data-fancybox data-caption="<?php echo stripslashes($qrow['questions']); ?>" class="qassets_icon">
										<video id="qvideo-<?php echo $q ?>" class="video-js vjs-theme-<?php echo $vtheme ?>" controls preload="auto" width="640" height="264" data-setup="{}" data-type="video">
											<source src="<?php echo $path . $qrow['video']; ?>" type="video/<?php echo strtolower(pathinfo($qrow['video'], PATHINFO_EXTENSION)); ?>" />
										</video>
									</a>
									<?php elseif ( ! empty($qrow['screen']) && file_exists($root_path . $qrow['screen'])): ?>
									<a href="<?php echo $path . $qrow['screen']; ?>" data-fancybox data-caption="<?php echo stripslashes($qrow['questions']); ?>" class="qassets_icon">
										<video id="qvideo-<?php echo $q ?>" class="video-js vjs-theme-<?php echo $vtheme ?>" controls preload="auto" width="200" height="100" data-setup="{}" data-type="video">
											<source src="<?php echo $path . $qrow['screen']; ?>" type="video/<?php echo strtolower(pathinfo($qrow['screen'], PATHINFO_EXTENSION)); ?>" />
										</video>
									</a>
									<?php elseif ( ! empty($qrow['image']) && file_exists($root_path . $qrow['image'])): ?>
									<a href="<?php echo $path . $qrow['image']; ?>" data-fancybox data-caption="<?php echo stripslashes($qrow['questions']); ?>" class="qassets_icon">
										<img class="img-fluid" src="<?php echo $path . $qrow['image']; ?>">
									</a>
									<?php elseif ( ! empty($qrow['document']) && file_exists($root_path . $qrow['document'])): ?>
									<a href="javascript:;" data-src="<?php echo $path . $qrow['document']; ?>" data-fancybox data-type="iframe" data-caption="<?php echo stripslashes($qrow['questions']); ?>" class="document">
										<img class="img-fluid pdfclissicsIm" src="img/icons/pdf_icon.svg">
									</a>
									<?php elseif ( ! empty($qrow['speech_text'])): ?>
									<button onclick="speak('div.intro_<?php echo $q ?>')">
										<i id="playicon" class="fa fa-play playicon" aria-hidden="true"></i>
										<i id="pauseicon" class="fa fa-pause pauseicon" style="display:none" aria-hidden="true"></i></button>
										<div class="intro_<?php echo $q ?>" style="display:none;"><?php echo $qrow['speech_text'] ?></div>
									<?php endif; ?>
									</div>
								</div>
							</div>
						</div>
						<?php #--------MTF--------
						if ($qtype == 1): ?>
						<div class="DestmatchingDrop">
							<div class="Drag_Drop Drag_Dropmultitext" id="option_<?php echo $qrow['question_id']; ?>" data-qtype="<?php echo $qtype ?>">
								<div class="row desktop_multiple_DND">
									<div class="fixingHeight">
										<div class="col-sm-4 fade-in">
										<?php 
										$mtf_choice_sql = "SELECT answer_id, question_id, choice_option, match_option FROM answer_tbl WHERE question_id = '". $qrow['question_id'] ."' ORDER BY (CASE WHEN choice_order_no != 0 THEN choice_order_no WHEN choice_order_no = 0 THEN RAND() END)";
										$mtf_choice = $db->prepare($mtf_choice_sql); $mtf_choice->execute();
										if ($mtf_choice->rowCount() > 0):
											$mtf_choice_data = $mtf_choice->fetchAll(PDO::FETCH_ASSOC);
											foreach ($mtf_choice_data as $mtf_choice_row): ?>
											<div class="flexheight33 questioninput option_brand">
												<div class="textwraping dtwraping option_brand"><?php echo stripslashes($mtf_choice_row['choice_option']); ?></div>
											</div>
										<?php endforeach; endif; ?>
										</div>
										<div class="col-sm-4 fade-in">
											<?php $mtf_choice->execute(); 
											if ($mtf_choice->rowCount() > 0): $mtf1 = 1;
											foreach ($mtf_choice_data as $mtf_choice_row): ?>
											<div class="flexheight33 FHdrop mtf_ordered_list">
												<div href="tgt<?php echo $mtf1.'_'.$q ?>" id="tgt<?php echo $mtf1.'_'.$q ?>" class="dragBox option_brand drop dragBox<?php echo $mtf1.'_'.$q ?>" data-qtype="<?php echo $qtype ?>" data-qid="<?php echo $mtf_choice_row['question_id'] ?>" data-aid="<?php echo $mtf_choice_row['answer_id'] ?>" data-choice-option="<?php echo $mtf_choice_row['answer_id'] ?>"></div>
											</div>
											<?php $mtf1++; endforeach; endif; ?>
										</div>
										<div class="col-sm-4 fade-in">
										<?php $mtf_match_sql = "SELECT answer_id, match_option FROM answer_tbl WHERE question_id = '". $qrow['question_id'] ."' ORDER BY (CASE WHEN match_order_no != 0 THEN match_order_no WHEN match_order_no = 0 THEN RAND() END)";
										$mtf_match = $db->prepare($mtf_match_sql); $mtf_match->execute();
										if ($mtf_match->rowCount() > 0): $mtf2 = 1;
										foreach ($mtf_match->fetchAll(PDO::FETCH_ASSOC) as $mtf_match_row): ?>
										<div class="flexheight33 Optioninput">
											<div class="rectangle1 dragdrop option_brand" id="drg<?php echo $mtf2.'_'.$q ?>" data-match-option="<?php echo $mtf_match_row['answer_id'] ?>">
												<div class="dndopt"><?php echo stripslashes($mtf_match_row['match_option']); ?></div>
											</div>
										</div>
										<?php $mtf2++; endforeach; endif; ?>
										</div>
									</div>
									<div class="submitMTFcenter col-sm-12">
										<button type="button" id="btn_<?php echo $q; ?>" class="btn submitbtn fade-in" data-show-report="<?php echo ($totalQ == $q) ? 'true' : 'false'; ?>" data-target="<?php echo $q + 1 ?>" data-cur="<?php echo $qrow['question_id']; ?>" data-sim-id="<?php echo $sim_id ?>" data-qid="<?php echo $qrow['question_id'] ?>">SUBMIT</button>
									</div>
								</div>
							</div>
						</div>
						<?php #------SEQUENCE--------
						elseif ($qtype == 2): ?>
						<div class="MatchingOption" id="option_<?php echo $qrow['question_id']; ?>" data-qtype="<?php echo $qtype ?>">
							<ul id="MatchingOptiondragble" class="Matchingdstyle advertise_container fade-in active">
							<?php $sq_sql = "SELECT answer_id, question_id, choice_option FROM answer_tbl WHERE question_id = '". $qrow['question_id'] ."' ORDER BY RAND()";
							$sq_choice = $db->prepare($sq_sql); $sq_choice->execute();
							if ($sq_choice->rowCount() > 0):
							$sq_choice_data = $sq_choice->fetchAll(PDO::FETCH_ASSOC);
							foreach ($sq_choice_data as $sq_choice_row): ?>
							<li class="draglistbox option_brand" data-qtype="<?php echo $qtype ?>" data-qid="<?php echo $sq_choice_row['question_id'] ?>" data-aid="<?php echo $sq_choice_row['answer_id'] ?>">
								<div class="dragbanner">
									<div class="dragIconContainer"><img src="img/qus_icon/white_dots.svg" class="dragIcon" /></div>
									<div class="seqoptions"><?php echo stripslashes($sq_choice_row['choice_option']); ?></div>
								</div>
							</li>
							<?php endforeach; endif; ?>
							</ul>
							<div class="submitMTFcenter col-sm-12">
								<button type="button" id="btn_<?php echo $q; ?>" class="btn submitbtn fade-in" data-show-report="<?php echo ($totalQ == $q) ? 'true' : 'false'; ?>" data-target="<?php echo $q + 1 ?>" data-cur="<?php echo $qrow['question_id']; ?>" data-sim-id="<?php echo $sim_id ?>" data-qid="<?php echo $qrow['question_id'] ?>">SUBMIT</button>
							</div>
						</div>
						<?php #-----SORTING--------
						elseif ($qtype == 3): ?>
						<div class="Drag_Drop IMGMATHCHDD" id="option_<?php echo $qrow['question_id']; ?>" data-qtype="<?php echo $qtype ?>">
							<div class="row desktop_SORTING">
								<div class="DestmatchingDrop fade-in">
									<div class="Dflex-banner">
										<?php $sort_choice_sql = "SELECT answer_id, question_id, choice_option, image FROM answer_tbl WHERE question_id = '". $qrow['question_id'] ."'";
										$sort_choice = $db->prepare($sort_choice_sql); $sort_choice->execute();
										if ($sort_choice->rowCount() > 0):
										$sort_choice_data = $sort_choice->fetchAll(PDO::FETCH_ASSOC);
										foreach ($sort_choice_data as $sort_choice_row): ?>
										<div class="Dflex option_brand">
											<div class="multiDBDbanner">
												<div class="multiDBDbox option_brand">
													<?php if ( ! empty($sort_choice_row['image']) && file_exists($root_path . $sort_choice_row['image'])): ?>
													<a href="<?php echo $path . $sort_choice_row['image']; ?>" data-fancybox data-caption="<?php echo $sort_choice_row['choice_option']; ?>" class="qassets_icon"><img class="feedimage" src="<?php echo $path . $sort_choice_row['image'] ?>" /></a>									
													<?php endif; ?>
													<div class="textwraping"><?php echo $sort_choice_row['choice_option']; ?></div>
												</div>
											</div>
										</div>
										<?php endforeach; endif; ?>
									</div>
									<div class="Dflex-banner Dflex-bannerDG">
										<?php $sort_choice->execute(); foreach ($sort_choice_data as $sort_choice_row): ?>
										<div class="multiDropbox drop1 option_brand" data-qid="<?php echo $sort_choice_row['question_id'] ?>" data-aid="<?php echo $sort_choice_row['answer_id'] ?>" data-choice-option="<?php echo $sort_choice_row['answer_id'] ?>"></div>
										<?php endforeach; ?>
									</div>
									<div class="dropareabox col-sm-12">
										<?php $sort_option_sql = "SELECT sub.sub_options_id, sub.sub_option FROM sub_options_tbl AS sub LEFT JOIN answer_tbl AS ans ON sub.answer_id = ans.answer_id WHERE ans.question_id = '". $qrow['question_id'] ."' ".(( ! empty($shuffle)) ? "ORDER BY RAND() ": "");
										$sort_option = $db->prepare($sort_option_sql); $sort_option->execute();
										if ($sort_option->rowCount() > 0): $q4 = 1;
										$sort_option_data = $sort_option->fetchAll(PDO::FETCH_ASSOC);
										foreach ($sort_option_data as $sort_option_row): ?>
										<div id="multidrag<?php echo $q4 ?>" class="box navy dragdrop1 sorting_dragdrop option_brand" data-match-option="<?php echo $sort_option_row['sub_options_id'] ?>">
											<?php echo stripslashes($sort_option_row['sub_option']); ?>
										</div>
										<?php $q4++; endforeach; endif; ?>
									</div>
								</div>
								<div class="submitMTFcenter col-sm-12">
									<button type="button" id="btn_<?php echo $q; ?>" class="btn submitbtn fade-in" data-show-report="<?php echo ($totalQ == $q) ? 'true' : 'false'; ?>" data-target="<?php echo $q + 1 ?>" data-cur="<?php echo $qrow['question_id']; ?>" data-sim-id="<?php echo $sim_id ?>" data-qid="<?php echo $qrow['question_id'] ?>">SUBMIT</button>
								</div>
							</div>
						</div>
						<?php #------MCQ--------
						elseif ($qtype == 4): ?>
						<div class="MCQOption bannermcq" id="option_<?php echo $qrow['question_id']; ?>" data-qtype="<?php echo $qtype ?>">
							<?php foreach ($ansData as $mcq_data): ?>
							<div class="row options option_brand advertise_container fade-in" data-qtype="<?php echo $qtype ?>" data-qid="<?php echo $mcq_data['question_id'] ?>" data-aid="<?php echo $mcq_data['answer_id'] ?>" data-box-id="<?php echo $q; ?>">
								<div class="multiQusChecK"><div class="Check-boxQB Check-boxMCQ"><i class="fa fa-circle" aria-hidden="true"></i></div></div>
								<div class="multiQustext"><?php echo stripslashes($mcq_data['choice_option']); ?></div>
							</div>
							<?php endforeach; ?>
							<div class="submitMTFcenter col-sm-12">
								<button type="button" id="btn_<?php echo $q; ?>" class="btn submitbtn fade-in" data-show-report="<?php echo ($totalQ == $q) ? 'true' : 'false'; ?>" data-target="<?php echo $q + 1 ?>" data-cur="<?php echo $qrow['question_id']; ?>" data-sim-id="<?php echo $sim_id ?>" data-qid="<?php echo $qrow['question_id'] ?>">SUBMIT</button>
							</div>
						</div>
						<?php #------MMCQ--------
						elseif ($qtype == 5): ?>
						<div class="MMCQOption bannermcq" id="option_<?php echo $qrow['question_id']; ?>" data-qtype="<?php echo $qtype ?>">
							<?php foreach ($ansData as $mmcq_data): ?>
							<div class="row mmcq_options option_brand advertise_container fade-in" data-qtype="<?php echo $qtype ?>" data-qid="<?php echo $mmcq_data['question_id'] ?>" data-aid="<?php echo $mmcq_data['answer_id'] ?>" data-box-id="<?php echo $q; ?>">
								<div class="multiQusChecK"><div class="Check-boxQB"><i class="fa fa-check" aria-hidden="true"></i></div></div>
								<div class="multiQustext"><?php echo stripslashes($mmcq_data['choice_option']); ?></div>                     
							</div>
							<?php endforeach; ?>
							<div class="submitMTFcenter col-sm-12">
								<button type="button" id="btn_<?php echo $q; ?>" class="btn submitbtn fade-in" data-show-report="<?php echo ($totalQ == $q) ? 'true' : 'false'; ?>" data-target="<?php echo $q + 1 ?>" data-cur="<?php echo $qrow['question_id']; ?>" data-sim-id="<?php echo $sim_id ?>" data-qid="<?php echo $qrow['question_id'] ?>">SUBMIT</button>
							</div>
						</div>
						<?php #------SWIPING------
						elseif ($qtype == 7): ?>
						<div class="MCQOption" id="option_<?php echo $qrow['question_id']; ?>" data-qtype="<?php echo $qtype ?>">
							<div class="home-demo">
								<div class="row CoursalsTYbullet">
									<div class="large-12 columns">
										<div class="owl-carousel fade-in swipingcoursal">
										<?php foreach ($ansData as $swipe_data): ?>
										<div class="swipeoptions option_brand" data-qtype="<?php echo $qtype ?>" data-qid="<?php echo $swipe_data['question_id'] ?>" data-aid="<?php echo $swipe_data['answer_id'] ?>">
											<div class="multiDBDboxAllIGN">
												<?php $swip_cls = 'dd_twrap'; if ( ! empty($swipe_data['image']) && file_exists($root_path . $swipe_data['image'])): $swip_cls = ''; ?>
												<a href="<?php echo $path . $swipe_data['image']; ?>" data-fancybox data-caption="<?php echo stripslashes($swipe_data['choice_option']); ?>" class="qassets_icon"><img class="feedimage" src="<?php echo $path . $swipe_data['image'] ?>" /></a>
												<?php endif; ?>
												<div class="textwraping <?php echo $swip_cls; ?>"><?php echo stripslashes($swipe_data['choice_option']); ?></div>
											</div>
										</div>
										<?php endforeach; ?>
										</div>
										<div class="submitMTFcenter col-sm-12">
											<button type="button" id="btn_<?php echo $q; ?>" class="btn submitbtn fade-in" data-show-report="<?php echo ($totalQ == $q) ? 'true' : 'false'; ?>" data-target="<?php echo $q + 1 ?>" data-cur="<?php echo $qrow['question_id']; ?>" data-sim-id="<?php echo $sim_id ?>" data-qid="<?php echo $qrow['question_id'] ?>">SUBMIT</button>
										</div>
									</div>
								</div>
							</div>
						</div>
						<?php #------Single-D&D------
						elseif ($qtype == 8 && $qrow['seleted_drag_option'] == 2): ?>
						<div class="Drag_Drop imgDNDS Drag_Dropmultiimgtext" id="option_<?php echo $qrow['question_id']; ?>" data-qtype="<?php echo $qtype ?>" data-drag-option="<?php echo $qrow['seleted_drag_option'] ?>">
							<div class="row desktop_multiple_DND">
								<div class="flex33 DCbanner fade-in">
								<?php $dnd_choice_sql = "SELECT question_id, sub_option, image FROM sub_options_tbl WHERE question_id = '". $qrow['question_id'] ."'";
								$dnd_choice = $db->prepare($dnd_choice_sql); $dnd_choice->execute();
								if ($dnd_choice->rowCount() > 0):
								$dnd_choice_data = $dnd_choice->fetchAll(PDO::FETCH_ASSOC);
								foreach ($dnd_choice_data as $dnd_choice_row): ?>
								<div class="flexheight33 multiDBDbanner DDaling DDaling1">
									<div class="multiDBDbox option_brand">
										<div class="multiDBDboxAllIGN">
											<?php if ( ! empty($dnd_choice_row['image']) && file_exists($root_path . $dnd_choice_row['image'])): ?>
											<a href="<?php echo $path . $dnd_choice_row['image']; ?>" data-fancybox data-caption="<?php echo stripslashes($dnd_choice_row['sub_option']); ?>" class="qassets_icon"><img src="<?php echo $path . $dnd_choice_row['image']; ?>" /></a>                                
											<?php endif; ?>
											<div class="textwraping <?php echo (empty($dnd_choice_row['image'])) ? 'dd_twrap' : ''; ?>"><?php echo stripslashes($dnd_choice_row['sub_option']); ?></div>
										</div>
									</div>
								</div>
								<?php endforeach; endif; ?>
								<div class="flexheight33 multiDBDbanner DROPAREA1">
									<?php $dndi = 1; foreach ($dnd_choice_data as $data): ?>
									<div class="dragBox option_brand drop dragBox<?php echo $dndi.'_'.$q ?> dragdrop2" data-qtype="<?php echo $qtype ?>" data-qid="<?php echo $data['question_id'] ?>"></div>
									<?php $dndi++; endforeach; ?>
								</div>
								</div>
								<div class="flex33 DROPclassicbanner fade-in">
									<?php if (count($ansData) > 0):
									foreach ($ansData as $dnd_match_row): ?>
									<div class="multiDBDbox dragdrop DDrop_multi option_brand" data-aid="<?php echo $dnd_match_row['answer_id'] ?>">
									<div class="multiDBDboxAllIGN">
										<?php if ( ! empty($dnd_match_row['image']) && file_exists($root_path . $dnd_match_row['image'])): ?>
										<a href="<?php echo $path . $dnd_match_row['image']; ?>" data-fancybox data-caption="<?php echo stripslashes($dnd_match_row['choice_option']); ?>" class="qassets_icon">
										<img src="<?php echo $path . $dnd_match_row['image']; ?>" /></a>
										<?php endif; ?>
										<div class="textwraping <?php echo (empty($dnd_match_row['image'])) ? 'dd_twrap' : ''; ?>"><?php echo stripslashes($dnd_match_row['choice_option']); ?></div>
										</div>
									</div>
								<?php endforeach; endif; ?>
								</div>
								<div class="submitMTFcenter col-sm-12">
									<button type="button" id="btn_<?php echo $q; ?>" class="btn submitbtn fade-in" data-show-report="<?php echo ($totalQ == $q) ? 'true' : 'false'; ?>" data-target="<?php echo $q + 1 ?>" data-cur="<?php echo $qrow['question_id']; ?>" data-sim-id="<?php echo $sim_id ?>" data-qid="<?php echo $qrow['question_id'] ?>">SUBMIT</button>
								</div>
							</div>
						</div>
						<?php #-----Multi-D&D------
						elseif ($qtype == 8 && $qrow['seleted_drag_option'] == 1): ?>
						<div class="Drag_Drop imgDNDS Drag_Dropmultiimgtext" id="option_<?php echo $qrow['question_id']; ?>" data-qtype="<?php echo $qtype ?>" data-drag-option="<?php echo $qrow['seleted_drag_option'] ?>">
							<div class="row desktop_multiple_DND">
								<div class="DestmatchingDrop MATCHDNDDesktop">
									<div class="col-sm-3 fade-in">
									<?php 
									$dnd2_choice_sql = "SELECT answer_id, question_id, image, choice_option FROM answer_tbl WHERE question_id = '". $qrow['question_id'] ."' ".(($shuffle == TRUE) ? "ORDER BY RAND()" : "");
									$dnd2_choice_exe = $db->prepare($dnd2_choice_sql); $dnd2_choice_exe->execute();
									if ($dnd2_choice_exe->rowCount() > 0):
										$dnd2_choice_data = $dnd2_choice_exe->fetchAll(PDO::FETCH_ASSOC);
										foreach ($dnd2_choice_data as $dnd2_choice_row): ?>
										<div class="flexheight33 multiDBDbanner QTbAnnEr option_brand">
											<div class="multiDBDbox multiDBDboxAllIGN option_brand">
												<?php if ( ! empty($dnd2_choice_row['image']) && file_exists($root_path . $dnd2_choice_row['image'])): ?>
												<img src="<?php echo $path . $dnd2_choice_row['image']; ?>" />
												<?php endif; ?>
												<div class="textwraping <?php echo (empty($dnd2_choice_row['image'])) ? 'dd_twrap' : ''; ?>"><?php echo stripslashes($dnd2_choice_row['choice_option']); ?></div>
											</div>
										</div>
										<?php endforeach; endif; ?>
									</div>
									<div class="col-sm-3 fade-in droppoint">
										<?php foreach ($dnd2_choice_data as $dnd2_choice_row): ?>
										<div class="flexheight33 multiDBDbanner">
											<div href="tgt1" class="dragBox option_brand dragBox<?php echo $dnd2_choice_row['answer_id'] ?>  drop dragdrop_multi" data-qid="<?php echo $dnd2_choice_row['question_id'] ?>" data-aid="<?php echo $dnd2_choice_row['answer_id'] ?>" data-choice-option="<?php echo $dnd2_choice_row['answer_id'] ?>"></div>
										</div>
										<?php endforeach; ?>
									</div>
									<div class="col-sm-3 fade-in">
										<?php foreach ($dnd2_choice_data as $dnd2_choice_row): ?>
											<div class="flexheight33 multiDBDbanner"></div>
										<?php endforeach; ?>
									</div>
									<div class="col-sm-3 fade-in pickpoint">
										<?php $sql2dnd = "SELECT answer_id, image2, match_option FROM answer_tbl WHERE question_id = '". $qrow['question_id'] ."' ".(($shuffle == TRUE) ? "ORDER BY RAND()" : "");
										$dnd2exe = $db->prepare($sql2dnd); $dnd2exe->execute();
										if ($dnd2exe->rowCount() > 0):
										foreach ($dnd2exe->fetchAll(PDO::FETCH_ASSOC) as $dnd2data): ?>
										<div class="flexheight33 multiDBDbanner BGMUltTicolor">
											<div class="multiDBDbox maTcHingdrop MultiMatchRB swappable notswap option_brand" id="drg<?php echo $dnd2data['answer_id'] ?>" data-match-option="<?php echo $dnd2data['answer_id'] ?>">
												<div class="multiDBDboxAllIGN">
													<?php if ( ! empty($dnd2data['image2']) && file_exists($root_path . $dnd2data['image2'])): ?>
													<img src="<?php echo $path . $dnd2data['image2']; ?>" />
													<?php endif; ?>
													<div class="textwraping <?php echo (empty($dnd2data['image2'])) ? 'dd_twrap' : ''; ?>"><?php echo stripslashes($dnd2data['match_option']); ?></div>
												</div>
											</div>
										</div><?php endforeach; endif; ?>
									</div>
								</div>
								<div class="submitMTFcenter col-sm-12">
									<button type="button" id="btn_<?php echo $q; ?>" class="btn submitbtn fade-in" data-show-report="<?php echo ($totalQ == $q) ? 'true' : 'false'; ?>" data-target="<?php echo $q + 1 ?>" data-cur="<?php echo $qrow['question_id']; ?>" data-sim-id="<?php echo $sim_id ?>" data-qid="<?php echo $qrow['question_id'] ?>">SUBMIT</button>
								</div>
							</div>
						</div>
						<?php endif; ?>
					</div>
				</div>
				<?php if ($totalQ == $q): ?>
				<span class="report-view">
					<div class="signbtn1 signbtnRepot" style="display:none;">
						<div class="repotView"><a href="learner-progress-branching-multiple.php?report=true&sid=<?php echo $sim_id; ?>" target="_blank">
							<img src="img/list/report_simicon.svg" />VIEW REPORT</a>
						</div>
					</div>
				</span>
				<?php endif; $q++; endforeach; endif; ?>
			</div>
			<?php if ( ! empty($right_char)): ?>
			<div class="CharFlextwo right">
				<img class="img-fulid qusTempimg" src="<?php echo $right_char; ?>">
			</div>
			<?php endif; ?>
	</div>

<div id="load_popup_modal_show" class="modal" tabindex="-1" data-keyboard="false" data-backdrop="static"></div>

<script src="content/js/articulate.js"></script>
<script src="content/js/owlcarousel/owl.carousel.min.js"></script>
<script src="content/js/launch-sim.js?v=<?php echo time() ?>"></script>
<script src="content/js/sim-branding.js?v=<?php echo time() ?>"></script>
<script type="text/javascript">
	var selectedDiv = null;
	var swappableIndex = 0;
	$('.pickpoint .swappable').each(function() {
		$(this).attr('data-pos', swappableIndex);
		swappableIndex++;
	})
	$('.pickpoint .swappable').bind('click',function(div){
		$('.pickpoint .swappable').removeClass('bordered');    
		selectedDiv=$(this).attr('id');
		$(this).addClass('bordered');
	});
	$('.droppoint .dragBox ').bind('click',function(div){
		//$(this).before($("#" + selectedDiv));
		$(this).append($("#" + selectedDiv)).find('.bordered').removeClass('bordered');
		selectedDiv = null;
	});
	$('body').on('click', '.droppoint .dragBox .swappable',function(){
		var prePos = $(this).attr('data-pos');
		$('.pickpoint .flexheight33').eq(prePos).append($(this));
	});

	/*var selectedDiv = null;
	$('.swappable').bind('click',function(div){
		$('.swappable').removeClass('bordered');
		if(selectedDiv===null){
			selectedDiv=$(this).attr('id');
			$(this).addClass('bordered');
		}else{ 
			if($(this).hasClass('notswap') == false) {
				var p1 = $(this).parent();
				$("#" + selectedDiv).parent().append(this);
				p1.append($("#" + selectedDiv));
				$('.pickpoint .swappable').addClass('notswap');
				$('.droppoint .swappable').removeClass('notswap');
			}
			selectedDiv = null;
			
		}
	});*/

	$(function() {
		$('ul.Matchingdstyle').sortable({
			update: function(evt) {
				//console.log(JSON.stringify($(this).sortable('serialize')));
			}
		});
		
		//draggable
		$('.dragdrop').draggable({
			revert: true,
			placeholder: true,
			droptarget: '.drop',
			scroll:true,
			drag: function(evt){+
				console.log(evt);
			},
			drop: function(evt, droptarget) {
				//$(this).appendTo(droptarget).draggable();
				if ($(droptarget).find('.dragdrop').length == 0){
					//== empty drop target, simply drop
					$(this).appendTo(droptarget).draggable();
				} else {
					//== swapping drops
					var d1 = this;
					var d2 = $(droptarget).find('.dragdrop').eq(0);
					$(d2).appendTo($(d1).parent()).draggable();
					$(d1).appendTo(droptarget).draggable();
				}
			}
		});
		
		//draggable
		$('.dragdrop1').draggable({
			revert: true,
			placeholder: true,
			droptarget: '.drop1',
			drop: function(evt, droptarget) {
				$(this).appendTo(droptarget).draggable();
			}
		});
	});
	
	$('.options').click(function() {
		var aid = $(this).data('aid');
		var bid = $(this).data('box-id');
		$('#btn_'+ bid).attr('data-aid', aid);
		$('.options').removeClass('press');
		$(this).addClass('press');
		$('.btn').removeAttr('disabled', true);
	});
	
	$('.mmcq_options').click(function() {
		var aid = $(this).data('aid');
		var bid = $(this).data('box-id');
		$('#btn_'+ bid).attr('data-aid', aid);
		$(this).toggleClass("press").checked = true;
		$('.btn').removeAttr('disabled', true);
	});
	
	function onDragStart(event) {
		event.dataTransfer.setData('text/plain', event.target.id);
		event.currentTarget.style.backgroundColor = '#fff';
	}
	
	function onDragOver(event) {
	  event.preventDefault();
	}
	
	function onDrop(event) {
		const id = event.dataTransfer.getData('text');
	 	const draggableElement = document.getElementById(id);
		const dropzone = event.target;
		dropzone.appendChild(draggableElement);
		event.dataTransfer.clearData();
	}
	
	//Function handleDragStart(), Its purpose is to store the id of the draggable element.
	function handleDragStart(e) {
		e.dataTransfer.setData("text", this.id); //note: using "this" is the same as using: e.target.
	}//end function


	//The dragenter event fires when dragging an object over the target. 
	//The css class "drag-enter" is append to the targets object.
	function handleDragEnterLeave(e) {
		if(e.type == "dragenter") {
			this.className = "drag-enter" 
		} else {
			this.className = "" //Note: "this" referces to the target element where the "dragenter" event is firing from.
		}
	}//end function

	//Function handles dragover event eg.. moving your source div over the target div element.
	//If drop event occurs, the function retrieves the draggable element’s id from the DataTransfer object.
	function handleOverDrop(e) {
		e.preventDefault(); 
  		//Depending on the browser in use, not using the preventDefault() could cause any number of strange default behaviours to occur.
		if (e.type != "drop") {
			return; //Means function will exit if no "drop" event is fired.
		}
		//Stores dragged elements ID in var draggedId
		var draggedId = e.dataTransfer.getData("text");
		//Stores referrence to element being dragged in var draggedEl
		var draggedEl = document.getElementById(draggedId);

		//if the event "drop" is fired on the dragged elements original drop target e.i..  it's current parentNode, 
		//then set it's css class to ="" which will remove dotted lines around the drop target and exit the function.
		if (draggedEl.parentNode == this) {
			this.className = "";
			return; //note: when a return is reached a function exits.
		}
		//Otherwise if the event "drop" is fired from a different target element, detach the dragged element node from it's
		//current drop target (i.e current perantNode) and append it to the new target element. Also remove dotted css class. 
		draggedEl.parentNode.removeChild(draggedEl);
		this.appendChild(draggedEl); //Note: "this" references to the current target div that is firing the "drop" event.
		this.className = "";
	}//end Function

	//Retrieve two groups of elements, those that are draggable and those that are drop targets:
	var draggable = document.querySelectorAll('[draggable]')
	var targets = document.querySelectorAll('[data-drop-target]');
	//Note: using the document.querySelectorAll() will aquire every element that is using the attribute defind in the (..)

	//Register event listeners for the"dragstart" event on the draggable elements:
	for(var i = 0; i < draggable.length; i++) {
		draggable[i].addEventListener("dragstart", handleDragStart);
	}

	//Register event listeners for "dragover", "drop", "dragenter" & "dragleave" events on the drop target elements.
	for(var i = 0; i < targets.length; i++) {
		targets[i].addEventListener("dragover", handleOverDrop);
		targets[i].addEventListener("drop", handleOverDrop);
		targets[i].addEventListener("dragenter", handleDragEnterLeave);
		targets[i].addEventListener("dragleave", handleDragEnterLeave);
	}		
	
	//-----DRAG-DROP--------
	function allowDrop(ev) {
		ev.preventDefault();
	}
	
	function drag(ev) {
		ev.dataTransfer.setData("text", ev.target.id);
	}
	
	function drop(ev) {
		ev.preventDefault();
		var data = ev.dataTransfer.getData("text");
		var nodeName = ev.target.getAttribute('href');
		if (nodeName == null) {
			console.log("failed");
		}
		else {
			ev.target.appendChild(document.getElementById(data));
			$("div[href$="+nodeName+"]").addClass('success');
			var match_option = $('#'+ data).data('match-option');
			$('#'+ nodeName).attr('data-match-option', match_option);
		}
	}
	
	//-----------Feedback-----------------
	var uid		= $('#uid').val();
	var simId	= $('#simId').val();
	var userid	= $('#userid').val();
	var apiurl	= $('#apiurl').val();
	
	$('body').on('click', '.submitbtn', function() {
		var curData	= $(this);
		var cur  	= curData.data('cur');
		var option	= $('#option_' + cur);
		var qtype 	= option.data('qtype');
		var report	= $(this).data('show-report');
		var target	= $(this).data('target');
		var current	= $(this).data('cur');
		var sid		= $(this).data('sim-id');		
		var qid		= $(this).data('qid');
		var aid		= $(this).data('aid');
		var drag	= option.data('drag-option');
		var url 	= 'view-branching-multiple-feedback-modal.php';
		var $modal	= $('#load_popup_modal_show');
		
		$.LoadingOverlay("show");
		/* MTF */
		if (qtype == 1) {
			var mtf_data = [];
			$('#option_'+ cur +' .mtf_ordered_list').each(function(){
				mtf_data.push({"choice_option_id": $(this).children('div').data('choice-option'), "match_option_id": $(this).children('div').children('div').data('match-option')});
			});
			var dataString = {"data": mtf_data};
			$modal.load(url, {'report': report, 'target': target, 'current': current, 'sid': sid, "qtype": qtype, "qid": qid, multi_data: JSON.stringify(dataString)}, function(res) {
				if (res != 'error') {
					$.LoadingOverlay("hide");
					$modal.modal('show', {backdrop: 'static', keyboard: false});
				}
				else {
					$.LoadingOverlay("hide");
					swal({text: 'Oops, something went wrong. Please try again later', buttons: false, icon: "error", timer: 1000});
				}
			});
		}
		/* SEQUENCE */
		else if (qtype == 2) {
			var sq_data = [];
			var sqi = 1;
			$('#option_'+ cur +' ul.Matchingdstyle li').each(function(){
				sq_data.push({"choice_option_id": $(this).data('aid')});
				sqi++;
			});
			var dataString = {"data": sq_data};
			$modal.load(url, {'report': report, 'target': target, 'current': current, 'sid': sid, "qtype": qtype, "qid": qid, multi_data: JSON.stringify(dataString)}, function(res) {
				if (res != 'error') {
					$.LoadingOverlay("hide");
					$modal.modal('show', {backdrop: 'static', keyboard: false});
				}
				else {
					$.LoadingOverlay("hide");
					swal({text: 'Oops, something went wrong. Please try again later', buttons: false, icon: "error", timer: 1000});
				}
			});
		}
		/* SORTING */
		else if (qtype == 3) {
			var sort_data = [];
			$('#option_'+ cur +' div.sorting_dragdrop').each(function(){
				var sort_choice = $(this).parent('div');
				sort_data.push({"choice_option_id": sort_choice.data('choice-option'), "match_option_id": $(this).data('match-option')});
			});
			var dataString = {"data": sort_data};
			$modal.load(url, {'report': report, 'target': target, 'current': current, 'sid': sid, "qtype": qtype, "qid": qid, multi_data: JSON.stringify(dataString)}, function(res) {
				if (res != 'error') {
					$.LoadingOverlay("hide");
					$modal.modal('show', {backdrop: 'static', keyboard: false});
				}
				else {
					$.LoadingOverlay("hide");
					swal({text: 'Oops, something went wrong. Please try again later', buttons: false, icon: "error", timer: 1000});
				}
			});
		}
		/* MCQ */
		else if (qtype == 4) {
			var mcqdata = $('#option_'+ cur +' .press');
			if (mcqdata.data('aid')) {
				$modal.load(url, {'report': report, 'target': target, 'current': current, 'sid': sid, "qtype": qtype, 'qid': mcqdata.data('qid'), 'aid': mcqdata.data('aid')}, function(res) {
					if (res != 'error') {
						$.LoadingOverlay("hide");
						$modal.modal('show', {backdrop: 'static', keyboard: false});
					}
					else {
						$.LoadingOverlay("hide");
						swal({text: 'Oops, something went wrong. Please try again later', buttons: false, icon: "error", timer: 1000});
					}
				});
			}
			else {
				$.LoadingOverlay("hide");
				swal({text: 'Oops, something went wrong. Please try again later', buttons: false, icon: "error", timer: 1000});
			}
		}
		/* MMCQ */
		else if (qtype == 5) {
			var mmcq_data = [];
			$('#option_'+ cur +' .press').each(function(){
				mmcq_data.push({"match_option_id": $(this).data('aid')});
			});
			var dataString = {"data": mmcq_data};
			$modal.load(url, {'report': report, 'target': target, 'current': current, 'sid': sid, "qtype": qtype, "qid": qid, multi_data: JSON.stringify(dataString)}, function(res) {
				if (res != 'error') {
					$.LoadingOverlay("hide");
					$modal.modal('show', {backdrop: 'static', keyboard: false});
				}
				else {
					$.LoadingOverlay("hide");
					swal({text: 'Oops, something went wrong. Please try again later', buttons: false, icon: "error", timer: 1000});
				}
			});
		}
		/* SWIPING */
		else if (qtype == 7) {
			var swip = $('#option_'+ cur +' .owl-stage .center .swipeoptions');
			if (swip.data('aid')) {
				$modal.load(url, {'report': report, 'target': target, 'current': current, 'sid': sid, "qtype": qtype, 'qid': swip.data('qid'), 'aid': swip.data('aid')}, function(res) {
					if (res != 'error') {
						$.LoadingOverlay("hide");
						$modal.modal('show', {backdrop: 'static', keyboard: false});
					}
					else {
						$.LoadingOverlay("hide");
						swal({text: 'Oops, something went wrong. Please try again later', buttons: false, icon: "error", timer: 1000});
					}
				});
			}
			else {
				$.LoadingOverlay("hide");
				swal({text: 'Oops, something went wrong. Please try again later', buttons: false, icon: "error", timer: 1000});
			}
		}
		/* D&D Multiple */
		else if (qtype == 8 && drag == 1) {
			var dd1_data = [];
			$('#option_'+ cur +' div.dragdrop_multi').each(function(){
				dd1_data.push({"choice_option_id": $(this).data('choice-option'), "match_option_id": $(this).children('div').data('match-option')});
			});
			var dataString = {"data": dd1_data};
			$modal.load(url, {'report': report, 'target': target, 'current': current, drag_option: drag, 'sid': sid, "qtype": qtype, "qid": qid, multi_data: JSON.stringify(dataString)}, function(res) {
				if (res != 'error') {
					$.LoadingOverlay("hide");
					$modal.modal('show', {backdrop: 'static', keyboard: false});
				}
				else {
					$.LoadingOverlay("hide");
					swal({text: 'Oops, something went wrong. Please try again later', buttons: false, icon: "error", timer: 1000});
				}
			});
		}
		/* D&D Single */
		else if (qtype == 8 && drag == 2) {
			var dd_data = $('#option_'+ cur +' .dragdrop2');
			$modal.load(url, {'report': report, 'target': target, 'current': current, drag_option: drag, 'sid': sid, "qtype": qtype, 'qid': dd_data.data('qid'), 'aid': dd_data.children('div').data('aid'), "choice_option_id": dd_data.children('div').data('aid'), "match_option_id": dd_data.children('div').data('aid')}, function(res) {
				if (res != 'error') {
					$.LoadingOverlay("hide");
					$modal.modal('show', {backdrop: 'static', keyboard: false});
				}
				else {
					$.LoadingOverlay("hide");
					swal({text: 'Oops, something went wrong. Please try again later', buttons: false, icon: "error", timer: 1000});
				}
			});
		}
	});
</script>
<?php 
require_once 'includes/footer.php';
