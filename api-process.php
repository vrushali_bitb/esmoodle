<?php 
header("Access-Control-Allow-Origin: *");
header('Access-Control-Allow-Methods: GET, POST');
header('Access-Control-Allow-Headers: X-Requested-With, Content-Type');
if ($_SERVER['REQUEST_METHOD'] == 'POST'):
	$json	 = file_get_contents('php://input');
	$http	 = isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on' ? 'https://' : 'http://';
	$baseUrl = $http . $_SERVER['HTTP_HOST'] . rtrim(dirname($_SERVER['PHP_SELF']), '/\\') . '/';
	if (isset($_POST['multi_data'])):
		$post = json_decode($_POST['multi_data'], TRUE);
		$req  = (isset($post['req'])) ? $post['req'] : '';
		$url  = $baseUrl . 'rest/'. $req;
		if ( ! empty($post)):
			$d = 0;
			foreach ($post['data'] as $res):
				$post_data = array('req' => $post['req'], 'table' => $post['table'], 'data' => $post['data'][$d]);
				$ch = curl_init();
				curl_setopt($ch, CURLOPT_URL, $url);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
				curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($post_data));
				$response = curl_exec($ch);
				curl_close($ch);
			$d++;
			endforeach;
			echo $response;
		endif;
	elseif (isset($_POST['single_data'])):
		$post	= json_decode($_POST['single_data'], TRUE);
		$req	= (isset($post['req'])) ? $post['req'] : '';
		$url	= $baseUrl . 'rest/'. $req;
		$ch 	= curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($post));
		$response = curl_exec($ch);
		echo $response;
		curl_close($ch);
	endif;
else:
	$error = array('status' => 'Failed', 'msg' => "Not Working");
	echo json_encode($error);
endif;
