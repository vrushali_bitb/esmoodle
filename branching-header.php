<?php 
session_start();
if (isset($_SESSION['username'])) {
    $user   = $_SESSION['username'];
    $role   = $_SESSION['role'];
    $userid = $_SESSION['userId'];
	$domain = (isset($_SESSION['domain_name'])) ? $_SESSION['domain_name'] : $_SERVER['HTTP_HOST'];
}
else {
    header('location: index.php');
}
require_once 'config/db.class.php';
require_once 'includes/learners_menu.php';
$db 		= new DBConnection();
$sim_id		= (isset($_GET['sim_id']) && ! empty($_GET['sim_id'])) ? $_GET['sim_id'] : '';
$ques_id	= (isset($_GET['ques_id'])) ? base64_decode($_GET['ques_id']) : '';
$sim_data 	= $db->getScenario($sim_id);
$sim_com 	= $db->getScenarioCompetency($sim_id);
$sim_page 	= $db->getSimPage($sim_data['scenario_id']);
$sim_brand 	= $db->getSimBranding($sim_data['scenario_id']);
$sim_type	= $db->getMultiScenarioType($sim_data['author_scenario_type']);
$path		= $db->getBaseUrl('scenario/upload/'.$domain.'/');
$uploadpath	= '../scenario/upload/'.$domain.'/';
$root_path	= $db->rootPath() . 'scenario/upload/'.$domain.'/';
$prelink 	= 'launch-sim-branching-multiple.php?preview=true&save=false&sim_id='. $sim_id;
$preview    = ( ! empty($ques_id)) ? $prelink . '&ques_id='. base64_encode($ques_id) : $prelink;
$type_name	= $sim_type[0]['type_name'];
$type 		= strtolower($type_name);
$gettype 	= '';
if ($type == 'image and text'):
	$gettype = 'image';
else:
	$gettype = $type;
endif;
$upload_file_name = '';
if ( ! empty($sim_data['image_url'])):
	$upload_file_name = $sim_data['image_url'];
elseif ( ! empty($sim_data['video_url'])):
	$upload_file_name = $sim_data['video_url'];
elseif ( ! empty($sim_data['audio_url'])):
	$upload_file_name = $sim_data['audio_url'];
elseif ( ! empty($sim_data['web_object_file'])):
	$upload_file_name = $sim_data['web_object_file'];
endif;
$web_object = '';
if ( ! empty($sim_data['web_object'])):
	$web_object = $sim_data['web_object'];
endif;
$ques_val1 = $ques_val2 = $ques_val3 = $ques_val4 = $ques_val5 = $ques_val6 = 0;
if ( ! empty($ques_id)):
	$qsql = "SELECT * FROM question_tbl WHERE question_id = '". $ques_id ."'";
	try {
		$qstmt	 = $db->prepare($qsql); $qstmt->execute();
		$qrow	 = $qstmt->rowCount();
		$qresult = ($qrow > 0) ? $qstmt->fetch() : [];
		if ( ! empty($qresult)):
			try {
				# Get FeedBack
				$feedsql  = 'SELECT * FROM feedback_tbl WHERE question_id = :question_id';
				$feedstmt = $db->prepare($feedsql);
				$feedstmt->bindParam(':question_id', $qresult['question_id'], PDO::PARAM_INT);
				$feedstmt->execute();
				$feedResult = ($feedstmt->rowCount() > 0) ? $feedstmt->fetchAll(PDO::FETCH_ASSOC) : [];

			} catch(PDOException $e) {
				echo "Oops. something went wrong. ".$e->getMessage(); exit;
			}
		endif;
	}
	catch(PDOException $e) {
		echo "Oops. something went wrong. ".$e->getMessage(); exit;
	}
endif;
$linkDataArray = $quesJsonArray	= [];
$tqsql  	= "SELECT *, LEFT(questions, 8) AS qtitle FROM question_tbl WHERE MD5(scenario_id) = '". $sim_id ."'";
$tqstmt 	= $db->prepare($tqsql); $tqstmt->execute();
$quesRow	= $tqstmt->rowCount();
$tqresult	= $tqstmt->fetchAll();
$qsn 		= 1;
foreach ($tqresult as $tqueSocre):
	if ( ! empty($tqueSocre['ques_val_1'])) $ques_val1 += $tqueSocre['ques_val_1'];
	if ( ! empty($tqueSocre['ques_val_2'])) $ques_val2 += $tqueSocre['ques_val_2'];
	if ( ! empty($tqueSocre['ques_val_3'])) $ques_val3 += $tqueSocre['ques_val_3'];
	if ( ! empty($tqueSocre['ques_val_4'])) $ques_val4 += $tqueSocre['ques_val_4'];
	if ( ! empty($tqueSocre['ques_val_5'])) $ques_val5 += $tqueSocre['ques_val_5'];
	if ( ! empty($tqueSocre['ques_val_6'])) $ques_val6 += $tqueSocre['ques_val_6'];
	
	//create JSON for grapgh - saumya
	$linkQues	= $tqueSocre["question_id"];
	$subString	= $qsn .'. '. $tqueSocre["qtitle"] . '...';
	if ($tqueSocre['question_type'] == '1'){
		$questionType = 'MTF';
	}
	elseif ($tqueSocre['question_type'] == '2'){
		$questionType = 'SEQUENCE';
	}
	elseif ($tqueSocre['question_type'] == '3'){
		$questionType = 'SORTING';
	}
	elseif ($tqueSocre['question_type'] == '4'){
		$questionType = 'MCQ';
	}
	elseif ($tqueSocre['question_type'] == '5'){
		$questionType = 'MMCQ';
	}
	elseif ($tqueSocre['question_type'] == '8'){
		$questionType = 'DND';
	}
	elseif ($tqueSocre['question_type'] == '7'){
		$questionType = 'SWIPE';
	}
	else {
		$questionType = 'SET TEMPLATE';
	}
	$quesJsonArray[] = array("key" 		=> $tqueSocre["question_id"],
							 "text"		=> $subString,
							 "fields"	=> [['name' => $questionType]],
							 "fullText"	=> $tqueSocre["questions"],
							 "sim_id"	=> $sim_id,
							 "url"		=> $db->getBaseUrl('branching-multiple-template.php'));
	$feedSql  		= "SELECT question_id, nextQid FROM feedback_tbl WHERE question_id = '". $tqueSocre["question_id"] ."'";
	$feedSqlPrepare	= $db->prepare($feedSql); $feedSqlPrepare->execute();
	$feedSqlResult 	= $feedSqlPrepare->fetchAll();
	foreach ($feedSqlResult as $feedSqlResultt):
		$linkDataArray[] = array("from" => $feedSqlResultt["question_id"], "to" => $feedSqlResultt["nextQid"]);
	endforeach;
	$qsn++;
endforeach;

$obj 						= (object)null; // create empty object for JSON
$obj->class 				= 'go.GraphLinksModel';
$obj->copiesArrays 			= true;
$obj->copiesArrayObjects	= true;
$obj->nodeDataArray 		= $quesJsonArray;
$obj->linkDataArray 		= $linkDataArray;
?>
<link rel="stylesheet" type="text/css" href="content/css/owlcarousel/owl.carousel.min.css" />
<link rel="stylesheet" type="text/css" href="content/css/qustemplate.css" />
<link rel="stylesheet" type="text/css" href="content/css/questionresponsive.css" />
<link rel="stylesheet" type="text/css" href="content/css/template-updated.css" />
<link rel="stylesheet" type="text/css" href="content/css/jquery.fontselect.css">
<script type="text/javascript" src="content/js/inputmask/jquery.inputmask.js"></script>
<script type="text/javascript" src="content/js/spectrum/spectrum.min.js"></script>
<link rel="stylesheet" type="text/css" href="content/js/spectrum/spectrum.min.css" />
<link rel="stylesheet" type="text/css" href="content/fancybox/jquery.fancybox.min.css" />
<script type="text/javascript" src="content/fancybox/jquery.fancybox.min.js"></script>
<link rel="stylesheet" type="text/css" href="content/css/ZoomSlider.css">
<script type="text/javascript" src="content/js/go.js"></script>
<script src="content/js/HyperlinkText.js"></script>
<script src="content/js/ZoomSlider.js"></script>
<script type="text/javascript">
$.LoadingOverlay("show");
</script>
<style type="text/css">
	body {
		overflow-x:hidden;
		background:#efefef;
	}
	[data-title] {
		font-size: 16px;
		position: relative;
		cursor: help;
	}  
	[data-title]:hover::before {
		content: attr(data-title);
		position: absolute;
		bottom: -20px;
		padding: 0px;
		color: #2c3545;
		font-size: 14px;
		z-index: 99;
		white-space: nowrap;
	}
	span.truncateTxt {
		width: 30px;
		white-space: nowrap;
		overflow: hidden;
		text-overflow: ellipsis;
		float: left;
	}
	:required  {
		background: url('img/list/star.png') no-repeat !important;
		background-position:right top !important;
		background-color: #fff !important;
	}
	.required  {
		background: url('img/list/star.png') no-repeat !important;
		background-position:right top !important;
		background-color: #fff !important;
	}
</style>