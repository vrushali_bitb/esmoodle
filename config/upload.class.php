<?php 

/**
* @package   	Upload Class
* @author    	suraj vishwakarma <surajvishwakarma319@gmail.com>
* @version   	1.0
* @lastupdate	20-Jun-18
*/

class Uploads {
	
	public function uploadFile($file_name, $file_tmp_name, $upload_path) {
		if ( ! empty($file_name) && ! empty($file_tmp_name) && ! empty($upload_path))
			$filename = basename($file_name);
			$file_ext = strtolower(substr(strrchr($filename, "."), 1));
			$new_file_name = str_replace('.'.$file_ext, '', $filename).rand(01,99).time().'.'.$file_ext;
			@move_uploaded_file($file_tmp_name, $upload_path . $new_file_name);
			return ( ! empty($new_file_name)) ? $new_file_name : '';
	}
	public function uploadZipFile($file_name, $file_tmp_name, $upload_path) {
		if ( ! empty($file_name) && ! empty($file_tmp_name) && ! empty($upload_path))
			$filename = basename($file_name);
			$file_ext = strtolower(substr(strrchr($filename, "."), 1));
			$new_file_name = str_replace('.'.$file_ext, '', $filename).rand(01,99).time().'.'.$file_ext;
			$zipname = str_replace('.'.$file_ext, '', $filename);
			@move_uploaded_file($file_tmp_name, $upload_path . $new_file_name);
			$res = array('zipname' => $zipname, 'filename' => $new_file_name);
			return ( ! empty($res)) ? $res : '';
	}

}