<?php
/**
 * This is the db connectionuniversal file
 *
 * PHP version 7
 *
 * LICENSE: This source file is subject to version 3.01 of the PHP license
 * that is available through the world-wide-web at the following URI:
 * http://www.php.net/license/3_01.txt.  If you did not receive a copy of
 * the PHP License AND are unable to obtain it through the web, please
 * send a note to license@php.net so we can mail you a copy immediately.
 *
 * @category   Db_Connection
 * @package    PackageName
 * @author     Himanshu Kumar <kumar@gmail.com>
 * @copyright  2018-2019 The KS Orga
 * @license    http://www.php.net/license/3_01.txt  PHP License 3.01
 * @version    SVN: $Id$
 * @link       http://pear.php.net/package/PackageName
 * @see        NetOther, Net_Sample::Net_Sample()
 * @since      File available since Release 1.2.0
 * @deprecated File deprecated in Release 2.0.0
 */

/**
 * This is a "Docblock Comment," also known as a "docblock."  The class'
 * docblock, below, contains a complete description of how to write these.
 */
class DBConnection extends PDO
{
    /**
     * This is a DB Connection
     * 
     * @return object void
     */
	 
	 public $siteLogo	= '';
	 
	 public $role		= [];

	 public $main_conn	= NULL;
	 
	 public $sub_conn	= NULL;

	 const googleFont	= "'Roboto', 'Roboto Condesded', 'Roboto Slab', 'Open Sans', 'Source Sans Pro', 'Lato', 'Montserrat', 'Oswawald', 'Raleway', 'Source Serif Pro'";
	 
	 /**
	 * This is a login function
	 * 
	 * @return void
	 */
	public function __construct()
	{
		
		//echo $_SERVER['HTTP_HOST'];die;
		/* Connect main DB */
		if ($_SERVER['HTTP_HOST'] == 'localhost:8081' || $_SERVER['HTTP_HOST'] == '127.0.0.1' || $_SERVER['HTTP_HOST'] == 'localhost' || $_SERVER['HTTP_HOST'] == '192.168.1.4'):
			/* On Local */
			$this->main_servername	= "localhost";
			$this->main_username	= "root";
			$this->main_password	= "";
			$this->main_db_name		= "easysim_main_db";
		else:
			/* On Server */
			$this->main_servername	= "localhost";
			$this->main_username	= "root";
			$this->main_password	= "Mysqluat01";
			$this->main_db_name		= "easysim_main_db";
		endif;
		
		/* GET-TIMEZONE */
		$timezone  = (isset($_SESSION['timezone'])) ? $_SESSION['timezone'] : $this->getTimezone();
		
		/* SET-TIMEZONE */
		date_default_timezone_set($timezone);
		date_default_timezone_get();
		
		$options = [
			PDO::MYSQL_ATTR_INIT_COMMAND		=> 'SET NAMES utf8, time_zone = "'. date('P') .'"',
			PDO::ATTR_ERRMODE					=> PDO::ERRMODE_EXCEPTION,
			PDO::ATTR_DEFAULT_FETCH_MODE		=> PDO::FETCH_ASSOC,
			PDO::ATTR_EMULATE_PREPARES			=> FALSE,
			PDO::MYSQL_ATTR_LOCAL_INFILE		=> TRUE
		];
		
		try {
			$this->main_conn = new PDO("mysql:host=$this->main_servername;dbname=$this->main_db_name", $this->main_username, $this->main_password, $options);
		}
		catch(PDOException $e) {
			echo "Main Connection failed:". $e->getMessage();
			exit;
		}
		
		/* Get DB Name & check Domain Exist */
		$host = $_SERVER['HTTP_HOST'];
		if ( ! empty($host)):
			$sql = "SELECT client_id, domain_name, db_name, type, role, logo FROM client_tbl WHERE domain_name = '". $host ."' AND status = '0'";
			$q	 = $this->main_conn->prepare($sql); $q->execute();
			if ($q->rowCount() > 0):
				$main_data = $q->fetch(PDO::FETCH_ASSOC);
				$_SESSION['client_id']		= $main_data['client_id'];
				$_SESSION['account_type']	= $main_data['type'];
				$_SESSION['domain_name']	= $main_data['domain_name'];
				$this->siteLogo 			= $main_data['logo'];
				$getRole = explode(',', $main_data['role']);
				foreach ($getRole as $role):
					$this->role[trim($role)] = ucwords($role);
				endforeach;
			else:
				header("location: suspendedpage.html");
				exit;
			endif;
		endif;
		
		if ($_SERVER['HTTP_HOST'] == 'localhost:8081' || $_SERVER['HTTP_HOST'] == '127.0.0.1' || $_SERVER['HTTP_HOST'] == 'localhost' || $_SERVER['HTTP_HOST'] == '192.168.1.4'):
			/* On Local */
			$host			= 'localhost';
			$dbname			= $main_data['db_name'];
			$dbuser			= 'root';
			$dbpassword		= '';
			$dbcharset 		= 'utf8';
		else:
			/* On Server */
			$host			= 'localhost';
			$dbname			= $main_data['db_name'];
			$dbuser			= 'root';
			$dbpassword		= 'Mysqluat01';
			$dbcharset 		= 'utf8';
		endif;
		
		$dsn = "mysql:host=$host;dbname=$dbname;charset=$dbcharset";
		
		/* Connect with subdomain DB */
		try {
            $this->sub_conn = parent::__construct($dsn, $dbuser, $dbpassword, $options);
            $this->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		}
        catch(PDOException $e) {
            echo "There is some problem in subdomain connection: ". $e->getMessage();
			exit;
        }
    }
	
	/**
     * This function will close connection
     * 
     * @return null
    */
    public function closeConnection()
    {
        $this->main_conn = NULL;
        $this->sub_conn	 = NULL;
    }

	/**
     * Destructors for close connection automatically called at the end of execution. 
     * 
    */	
	public function __destruct() {
		$this->closeConnection();
	}

	public function getTimezone($ip = null)
    {
        $apiurl = 'http://ip-api.com/json/' . $ip;
        $ipInfo = file_get_contents($apiurl);
        $ipInfo = json_decode($ipInfo);
        return $ipInfo->timezone;
    }
	
	/**
     * This is a userRoleRedirect to redirect user as per their role
     * 
     * @param string $role takes user role
     * 
     * @return void
     */
    public function userRoleRedirect($role)
    {
        if (isset($_SESSION)) {
			switch ($role) {
				case 'superAdmin':
                	header('location: super-admin-dashboard.php');
                	break;
				case 'clientAdmin':
									header('location: client-admin-dashboard.php');
									break;
				case 'admin':
                	header('location: admin-dashboard.php');
                	break;
				case 'author':
									header('location: author-dashboard.php');
                	break;
				case 'reviewer':
                	header('location: reviewer-dashboard.php');
                	break;
        case 'learner':
                	header('location: learner-dashboard.php');
                	break;
        case 'educator':
                	header('location: educator-dashboard.php');
                	break;
        default:
                	return FALSE;
            }
        }
    }
	
	public function checkAccountType() {
		if (isset($_SESSION)) {
			$account_type = $_SESSION['role'];
			switch ($account_type) {
				case 'superAdmin':
					return 1;
                	break;
				case 'clientAdmin':
					return 2;
                	break;
				case 'admin':
					return 3;
                	break;
				case 'author':
					return 4;
					break;
				case 'reviewer':
					return 5;
					break;
				case 'learner':
					return 6;
					break;
				case 'educator':
					return 6;
					break;
            	default:
				return FALSE;
            }
        }
	}
	
	public function checkUser($login_name, $pwd, $role)
    {
        $sql = "SELECT id FROM users WHERE username = '". $login_name ."' AND password = '". md5($pwd) ."' AND role = '". $role ."' AND status = '1'";
        $exe = $this->prepare($sql); $exe->execute();
        return ($exe->rowCount() > 0) ? TRUE : FALSE;
    }

    public function checkEmail($email) {
        return ( ! preg_match("/^[_\.0-9a-zA-Z-]+@([0-9a-zA-Z][0-9a-zA-Z-]+\.)+[a-zA-Z]{2,6}$/i", $email)) ? FALSE : TRUE;
    }

    public function assigneeExist($sid)
    {
        $sql = "SELECT scenario_id, assignee,";
        $sql = $sql . " created_by FROM scenario_master"; 
        $sql = $sql . " WHERE scenario_id = '". $sid ."' AND assignee != '0'";
        $exe = $this->prepare($sql); $exe->execute();
        return ($exe->rowCount() > 0) ? TRUE : FALSE;
    }
	
	public function checkUserBlock($login_name, $pwd, $role)
    {
		$sql = "SELECT id FROM users WHERE username = '". $login_name ."' AND password = '". md5($pwd) ."' AND role = '". $role ."' AND status = '0'";
		$exe = $this->prepare($sql); $exe->execute();
		return ($exe->rowCount() > 0) ? TRUE : FALSE;
	}
	
	public function getAnswers($qid)
	{
		$sql = "SELECT answer_id, choice_option, choice_order_no, match_order_no, match_option, match_sorting_item, image, video, nextQid, ans_val1, ans_val2, ans_val3, ans_val4, ans_val5, ans_val6 FROM answer_tbl WHERE question_id = '". $qid ."'";
		$q	 = $this->prepare($sql); $q->execute();
		if ($q->rowCount() > 0):
			$i = 1;
			$answer = [];
			foreach ($q->fetchAll(PDO::FETCH_ASSOC) as $row):
				$ansdata = array("ansid"				=> $row['answer_id'],
								 "#choice_option"		=> $row['choice_option'],
								 "choice_order_no"		=> $row['choice_order_no'],
								 "match_order_no"		=> $row['match_order_no'],
								 "#match_option"		=> $row['match_option'],
								 "match_sorting_item"	=> $row['match_sorting_item'],
								 "image" 				=> $row['image'],
								 "video" 				=> $row['video'],
								 "nextQid" 				=> $row['nextQid'],
								 "ans_val1" 			=> $row['ans_val1'],
								 "ans_val2" 			=> $row['ans_val2'],
								 "ans_val3" 			=> $row['ans_val3'],
								 "ans_val4" 			=> $row['ans_val4'],
								 "ans_val5" 			=> $row['ans_val5'],
								 "ans_val6" 			=> $row['ans_val6']);
				$answer['answer'. $i] = $ansdata;
			$i++;
			endforeach;
			return ( ! empty($answer)) ? $answer : '';
		endif;
	}
	
	public function scenarioTypeExist($name)
	{
		$sql = "SELECT type_name FROM scenario_type WHERE type_name = '". $name ."'";
		$q	 = $this->prepare($sql); $q->execute();
		return ($q->rowCount() > 0) ? TRUE : FALSE;
	}
	
	public function simExistInQuestion($sid)
	{
		$sql = "SELECT scenario_id FROM question_tbl WHERE scenario_id = '". $sid ."'";
		$q	 = $this->prepare($sql); $q->execute();
		return ($q->rowCount() > 0) ? TRUE : FALSE;
	}
	
	public function categoryExist($category)
	{
		$sql = "SELECT category FROM category_tbl WHERE category = '". $category ."'";
		$q	 = $this->prepare($sql); $q->execute();
		return ($q->rowCount() > 0) ? TRUE : FALSE;
	}
	
	public function getTotalCategory()
	{
		$sql = "SELECT cat_id FROM category_tbl";
		$q	 = $this->prepare($sql); $q->execute();
		$count = $q->rowCount();
		return ($count > 0) ? $count : 0;
	}
	
	public function groupExist($name)
	{
		$sql = "SELECT group_name FROM group_tbl WHERE group_name = '". $name ."'";
		$q	 = $this->prepare($sql); $q->execute();
		return ($q->rowCount() > 0) ? TRUE : FALSE;
	}
	
	public function scenarioStatusExist($name)
	{
		$sql = "SELECT status_name FROM scenario_status WHERE status_name = '". $name ."'";
		$q	 = $this->prepare($sql); $q->execute();
		return ($q->rowCount() > 0) ? TRUE : FALSE;
	}
	
	public function getAssigneeName($id)
	{
		$sql = "SELECT id, CONCAT(fname, ' ', lname) AS fullname, username, email, role, company, department, location, designation FROM users WHERE id = '". $id ."'";
		$q	 = $this->prepare($sql); $q->execute();
		if ($q->rowCount() > 0):
			$data = $q->fetch(PDO::FETCH_ASSOC);
			return $data;
		else:
			return [];
		endif;
	}
	
	public function getScenario($id = NULL)
	{
		$sql = "SELECT * FROM scenario_master";
		if ($id != NULL):
			$sql .= " WHERE MD5(scenario_id) = '". $id ."'";
		else:
			$sql .= " ORDER BY scenario_id DESC";
		endif;
		$q = $this->prepare($sql); $q->execute();
		if ($q->rowCount() > 0):
			foreach ($q->fetchAll(PDO::FETCH_ASSOC) as $row):
				$data[] = $row;
			endforeach;
			return ($id != NULL ) ? $data[0] : $data;
		endif;
	}
	
	public function getTotalScenario()
	{
		$sql = "SELECT scenario_id FROM scenario_master";
		$q	 = $this->prepare($sql); $q->execute();
		$count = $q->rowCount();
		return ($count > 0) ? $count : 0;
	}
	
	public function getTimeSpent()
	{
		$sql = "SELECT SUM(duration) AS time FROM scenario_master WHERE duration IS NOT NULL";
		$q	 = $this->prepare($sql); $q->execute();
		$tq	 = $q->fetch(PDO::FETCH_ASSOC);
		return gmdate("H:i:s", $tq['time']);
	}
	
	public function getDraftScenario($status)
	{
		$sql 	= "SELECT scenario_id FROM scenario_master WHERE status = '". $status ."'";
		$q	 	= $this->prepare($sql); $q->execute();
		$count	= $q->rowCount();
		return ($count > 0) ? $count : 0;
	}
	
	public function getAssignScenario()
	{
		$sql 	= "SELECT scenario_id FROM scenario_master WHERE status IN (5, 8, 13)";
		$q	 	= $this->prepare($sql); $q->execute();
		$count	= $q->rowCount();
		return ($count > 0) ? $count : 0;
	}
	
	public function getTotalTemplate()
	{
		$sql 	= "SELECT eid FROM email_template_tbl";
		$q	 	= $this->prepare($sql); $q->execute();
		$count	= $q->rowCount();
		return ($count > 0) ? $count : 0;
	}
	
	public function getIncompleteLearnerSIM($uid)
	{	$count 	= 0;
		$sim_id	= $this->getAssignSimData($uid);
		if ( ! empty($sim_id)):
			$sql 	= "SELECT DISTINCT scenario_id FROM scenario_master WHERE scenario_id IN ($sim_id) AND scenario_id NOT IN (SELECT scenario_id FROM scenario_attempt_tbl WHERE userid = '". $uid ."')";
			$exe	= $this->prepare($sql); $exe->execute();
			$count	= $exe->rowCount();
		endif;
		return $count;
	}
	
	public function getTotalAttempt($sim_id, $user_id)
	{
		$sql 	= "SELECT aid FROM scenario_attempt_tbl WHERE scenario_id = '". $sim_id ."' AND userid = '". $user_id ."'";
		$exe 	= $this->prepare($sql); $exe->execute();
		$count	= $exe->rowCount();
		return ($count > 0) ? $count : 0;
	}
	
	public function getAttemptData($id)
	{
		$sql = "SELECT * FROM scenario_attempt_tbl WHERE aid = '". $id ."'";
		$q	 = $this->prepare($sql); $q->execute();
		if ($q->rowCount() > 0):
			$data = $q->fetch(PDO::FETCH_ASSOC);
			return $data;
		else:
			return [];
		endif;
	}
	
	public function getLastSIM($uid, $limit = NULL)
	{
		$sql = "SELECT max(aid) AS maid, scenario_id, userid FROM `scenario_attempt_tbl` WHERE userid = '". $uid ."' AND uid IN (SELECT uid FROM score_tbl) GROUP BY scenario_id ORDER BY maid DESC";
		if ($limit != NULL) $sql .= " LIMIT $limit";
		$q = $this->prepare($sql); $q->execute();
		if ($q->rowCount() > 0):
			foreach ($q->fetchAll(PDO::FETCH_ASSOC) as $row):
				$data[] = $row;
			endforeach;
			return $data;
		else:
			return [];
		endif;
	}

	public function getLastMultiSIM($uid, $limit = NULL)
	{
		$sql = "SELECT max(aid) AS maid, scenario_id, userid FROM `scenario_attempt_tbl` WHERE userid = '". $uid ."' AND uid IN (SELECT uid FROM score_tbl) OR uid IN (SELECT uid FROM multi_score_tbl) GROUP BY scenario_id ORDER BY maid DESC";
		if ($limit != NULL) $sql .= " LIMIT $limit";
		$q = $this->prepare($sql); $q->execute();
		if ($q->rowCount() > 0):
			foreach ($q->fetchAll(PDO::FETCH_ASSOC) as $row):
				$data[] = $row;
			endforeach;
			return $data;
		else:
			return [];
		endif;
	}
	
	public function getTotalLearnerSIM($uid)
	{
		$count	= 0;
		$sim_id	= $this->getAssignSimData($uid);
		if ( ! empty($sim_id)):
			$sql 	= "SELECT DISTINCT scenario_id FROM scenario_master WHERE scenario_id IN ($sim_id)";
			$q	 	= $this->prepare($sql); $q->execute();
			$count	= $q->rowCount();
		endif;
		return $count;
	}
	
	public function scenarioExist($title)
	{
		$sql 	= "SELECT Scenario_title FROM scenario_master WHERE Scenario_title = '". $title ."'";
		$exe	= $this->prepare($sql); $exe->execute();
		return ($exe->rowCount() > 0) ? TRUE : FALSE;
	}
	
	public function getQuestionsByScenario($sid)
	{
		$sql = "SELECT *, (ques_val_1 + ques_val_2 + ques_val_3 + ques_val_4 + ques_val_5 + ques_val_6) AS totalQScore FROM question_tbl WHERE MD5(scenario_id) = '". $sid ."'";
		$q	 = $this->prepare($sql); $q->execute();
		if ($q->rowCount() > 0):
			foreach ($q->fetchAll(PDO::FETCH_ASSOC) as $row):
				$data[] = $row;
			endforeach;
			return $data;
		else:
			return [];
		endif;
	}
	
	public function getQuestionsScoreBySim($sid, $uid)
	{
		$sql = "SELECT q.questions, (q.ques_val_1 + q.ques_val_2 + q.ques_val_3 + q.ques_val_4 + q.ques_val_5 + q.ques_val_6) AS totalQScore, (ans.ans_val1 + ans.ans_val2 + ans.ans_val3 + ans.ans_val4 + ans.ans_val5 + ans.ans_val6) as totalAnsval, ans.choice_option FROM 
				question_tbl q LEFT JOIN 
				score_tbl s ON q.question_id = s.question_id LEFT JOIN 
				answer_tbl ans ON s.choice_option_id = ans.answer_id WHERE MD5(s.scenario_id) = '". $sid ."' AND s.uid = '". $uid ."'";
		$q	 = $this->prepare($sql); $q->execute();
		if ($q->rowCount() > 0):
			foreach ($q->fetchAll(PDO::FETCH_ASSOC) as $row):
				$data[] = $row;
			endforeach;
			return $data;
		else:
			return [];
		endif;
	}
	
	public function getQuestionsScoreByMultiSim($sid, $uid)
	{
		$sql = "SELECT q.questions, (q.ques_val_1 + q.ques_val_2 + q.ques_val_3 + q.ques_val_4 + q.ques_val_5 + q.ques_val_6) AS totalQScore, s.status FROM 
				question_tbl q LEFT JOIN 
				multi_score_tbl s ON q.question_id = s.question_id WHERE 
				MD5(s.scenario_id) = '". $sid ."' AND s.uid = '". $uid ."'";
		$q	 = $this->prepare($sql); $q->execute();
		if ($q->rowCount() > 0):
			foreach ($q->fetchAll(PDO::FETCH_ASSOC) as $row):
				$data[] = $row;
			endforeach;
			return $data;
		else:
			return [];
		endif;
	}

	public function getOpenRespQuestionsScore($sid, $uid)
	{
		$sql = "SELECT q.questions, (q.ques_val_1 + q.ques_val_2 + q.ques_val_3 + q.ques_val_4 + q.ques_val_5 + q.ques_val_6) AS totalQScore, ( ans.ans_val1 + ans.ans_val2 + ans.ans_val3 + ans.ans_val4 + ans.ans_val5 + ans.ans_val6) AS totalAnsval FROM 
				question_tbl q LEFT JOIN 
				open_res_tbl ans ON q.question_id = ans.question_id WHERE 
				MD5(ans.scenario_id) = '". $sid ."' AND ans.uid = '". $uid ."'";
		$q	 = $this->prepare($sql); $q->execute();
		if ($q->rowCount() > 0):
			foreach ($q->fetchAll(PDO::FETCH_ASSOC) as $row):
				$data[] = $row;
			endforeach;
			return $data;
		else:
			return [];
		endif;
	}
	
	public function getMissedDecisionReview($sid, $uid)
	{
		$sql = "SELECT * FROM (SELECT q.questions, (q.ques_val_1 + q.ques_val_2 + q.ques_val_3 + q.ques_val_4 + q.ques_val_5 + q.ques_val_6) AS totalQScore, ( ans.ans_val1 + ans.ans_val2 + ans.ans_val3 + ans.ans_val4 + ans.ans_val5 + ans.ans_val6) AS totalAnsval, ans.choice_option FROM 
				question_tbl q LEFT JOIN 
				score_tbl s ON q.question_id = s.question_id LEFT JOIN 
				answer_tbl ans ON s.choice_option_id = ans.answer_id WHERE 
				MD5(s.scenario_id) = '". $sid ."' AND s.uid = '". $uid ."') AS tbl WHERE 
				tbl.totalQScore != tbl.totalAnsval";
		$q	 = $this->prepare($sql); $q->execute();
		if ($q->rowCount() > 0):
			foreach ($q->fetchAll(PDO::FETCH_ASSOC) as $row):
				$data[] = $row;
			endforeach;
			return $data;
		else:
			return [];
		endif;
	}
	
	public function getOpenRespMissedDecisionReview($sid, $uid)
	{
		$sql = "SELECT * FROM (SELECT q.questions, (q.ques_val_1 + q.ques_val_2 + q.ques_val_3 + q.ques_val_4 + q.ques_val_5 + q.ques_val_6) AS totalQScore, ( ans.ans_val1 + ans.ans_val2 + ans.ans_val3 + ans.ans_val4 + ans.ans_val5 + ans.ans_val6) AS totalAnsval FROM 
				question_tbl q LEFT JOIN 
				open_res_tbl ans ON q.question_id = ans.question_id WHERE 
				MD5(ans.scenario_id) = '". $sid ."' AND ans.uid = '". $uid ."') AS tbl WHERE 
				tbl.totalQScore != tbl.totalAnsval";
		$q	 = $this->prepare($sql); $q->execute();
		if ($q->rowCount() > 0):
			foreach ($q->fetchAll(PDO::FETCH_ASSOC) as $row):
				$data[] = $row;
			endforeach;
			return $data;
		else:
			return [];
		endif;
	}
	
	public function getMissedDecisionReviewMulti($sid, $uid)
	{
		$sql = "SELECT q.questions, (q.ques_val_1 + q.ques_val_2 + q.ques_val_3 + q.ques_val_4 + q.ques_val_5 + q.ques_val_6) AS totalQScore, s.status FROM 
				question_tbl q LEFT JOIN 
				multi_score_tbl s ON q.question_id = s.question_id WHERE 
				s.status = 0 AND MD5(s.scenario_id) = '". $sid ."' AND s.uid = '". $uid ."'";
		$q	 = $this->prepare($sql); $q->execute();
		if ($q->rowCount() > 0):
			foreach ($q->fetchAll(PDO::FETCH_ASSOC) as $row):
				$data[] = $row;
			endforeach;
			return $data;
		else:
			return [];
		endif;
	}
	
	public function getCorrectDecisionReview($sid, $uid)
	{
		$sql = "SELECT * FROM (SELECT q.questions, (q.ques_val_1 + q.ques_val_2 + q.ques_val_3 + q.ques_val_4 + q.ques_val_5 + q.ques_val_6) AS totalQScore, ( ans.ans_val1 + ans.ans_val2 + ans.ans_val3 + ans.ans_val4 + ans.ans_val5 + ans.ans_val6) AS totalAnsval, ans.choice_option FROM 
				question_tbl q LEFT JOIN 
				score_tbl s ON q.question_id = s.question_id LEFT JOIN 
				answer_tbl ans ON s.choice_option_id = ans.answer_id WHERE 
				MD5(s.scenario_id) = '". $sid ."' AND s.uid = '". $uid ."') AS tbl WHERE 
				tbl.totalQScore = tbl.totalAnsval";
		$q	 = $this->prepare($sql); $q->execute();
		if ($q->rowCount() > 0):
			foreach ($q->fetchAll(PDO::FETCH_ASSOC) as $row):
				$data[] = $row;
			endforeach;
			return $data;
		else:
			return [];
		endif;
	}
	
	public function getOpenRespCorrectDecisionReview($sid, $uid)
	{
		$sql = "SELECT * FROM (SELECT q.questions, (q.ques_val_1 + q.ques_val_2 + q.ques_val_3 + q.ques_val_4 + q.ques_val_5 + q.ques_val_6) AS totalQScore, ( ans.ans_val1 + ans.ans_val2 + ans.ans_val3 + ans.ans_val4 + ans.ans_val5 + ans.ans_val6) AS totalAnsval FROM 
				question_tbl q LEFT JOIN 
				open_res_tbl ans ON q.question_id = ans.question_id WHERE 
				MD5(ans.scenario_id) = '". $sid ."' AND ans.uid = '". $uid ."') AS tbl WHERE 
				tbl.totalQScore = tbl.totalAnsval";
		$q	 = $this->prepare($sql); $q->execute();
		if ($q->rowCount() > 0):
			foreach ($q->fetchAll(PDO::FETCH_ASSOC) as $row):
				$data[] = $row;
			endforeach;
			return $data;
		else:
			return [];
		endif;
	}

	public function getCorrectDecisionReviewMulti($sid, $uid)
	{
		$sql = "SELECT q.questions, (q.ques_val_1 + q.ques_val_2 + q.ques_val_3 + q.ques_val_4 + q.ques_val_5 + q.ques_val_6) AS totalQScore, s.status FROM 
				question_tbl q LEFT JOIN 
				multi_score_tbl s on q.question_id = s.question_id WHERE s.status = 1 AND 
				MD5(s.scenario_id) = '". $sid ."' AND s.uid = '". $uid ."'";
		$q	 = $this->prepare($sql); $q->execute();
		if ($q->rowCount() > 0):
			foreach ($q->fetchAll(PDO::FETCH_ASSOC) as $row):
				$data[] = $row;
			endforeach;
			return $data;
		else:
			return [];
		endif;
	}
	
	public function getOpenRespFeedBack($sid, $uid)
	{
		$sql = "SELECT * FROM (SELECT q.questions, (q.ques_val_1 + q.ques_val_2 + q.ques_val_3 + q.ques_val_4 + q.ques_val_5 + q.ques_val_6) AS totalQScore, (ans.ans_val1 + ans.ans_val2 + ans.ans_val3 + ans.ans_val4 + ans.ans_val5 + ans.ans_val6) AS totalAnsval, ans.tts_data_feedback, ans.file_name_feedback FROM 
				question_tbl q LEFT JOIN 
				open_res_tbl ans ON q.question_id = ans.question_id WHERE 
				MD5(ans.scenario_id) = '". $sid ."' AND ans.uid = '". $uid ."') AS tbl";
		$q	 = $this->prepare($sql); $q->execute();
		if ($q->rowCount() > 0):
			foreach ($q->fetchAll(PDO::FETCH_ASSOC) as $row):
				$data[] = $row;
			endforeach;
			return $data;
		else:
			return [];
		endif;
	}
	
	public function getTotalQuestionsByScenario($sid)
	{
		$sql = "SELECT COUNT(question_id) AS tq FROM question_tbl WHERE MD5(scenario_id) = '". $sid ."'";
		$q	 = $this->prepare($sql); $q->execute();
		$tq	 = $q->fetch(PDO::FETCH_ASSOC);
		return $tq['tq'];
	}
	
	public function getQuestionsByQuesId($qid)
	{
		$sql = "SELECT * FROM question_tbl WHERE MD5(question_id) = '". $qid ."'";
		$q	 = $this->prepare($sql); $q->execute();
		if ($q->rowCount() > 0):
			foreach ($q->fetchAll(PDO::FETCH_ASSOC) as $row):
				$data[] = $row;
			endforeach;
			return $data;
		else:
			return [];
		endif;
	}
	
	public function getAnswerMaster($id)
	{
		$sql = "SELECT * FROM answer_tbl WHERE MD5(answer_id) = '". $id ."'";
		$q	 = $this->prepare($sql); $q->execute();
		if ($q->rowCount() > 0):
			$data = $q->fetch(PDO::FETCH_ASSOC);
			return $data;
		else:
			return [];
		endif;
	}
	
	public function getAnswersByQuestionId($qid, $shuffle = FALSE)
	{
		$sql = "SELECT * FROM answer_tbl WHERE MD5(question_id) = '". $qid ."' ".(($shuffle == TRUE) ? "ORDER BY RAND()" : "");
		$q	 = $this->prepare($sql); $q->execute();
		if ($q->rowCount() > 0):
			foreach ($q->fetchAll(PDO::FETCH_ASSOC) as $row):
				$data[] = $row;
			endforeach;
			return $data;
		else:
			return [];
		endif;
	}
	
	public function getScenarioTypes($id = NULL)
	{
		$sql = "SELECT * FROM scenario_type ";
		if ($id != NULL):
			$sql .= "WHERE MD5(st_id) = '". $id ."'";
		else:
			$sql .= "ORDER BY type_name";
		endif;
		$q = $this->prepare($sql); $q->execute();
		if ($q->rowCount() > 0):
			foreach ($q->fetchAll(PDO::FETCH_ASSOC) as $row):
				$data[] = $row;
			endforeach;
			return ($id != NULL) ? $data[0] : $data;
		endif;
	}
	
	public function getMultiScenarioType($id)
	{
		if ( ! empty($id)):
			$sql = "SELECT * FROM scenario_type WHERE st_id IN ($id)";
			$q	 = $this->prepare($sql); $q->execute();
			if ($q->rowCount() > 0):
				foreach ($q->fetchAll(PDO::FETCH_ASSOC) as $row):
					$data[] = $row;
				endforeach;
				return $data;
			else:
				return [];
			endif;
		else:
			return [];
		endif;
	}
	
	public function getMultiScenarioStage($id = NULL)
	{
		$sql = "SELECT * FROM scenario_status WHERE ";
		if ($id != NULL):
			$sql .= "sid IN ($id) ORDER BY status_name";
		else:
			$sql .= "status_name != '' ORDER BY status_name";
		endif;
		$q = $this->prepare($sql); $q->execute();
		if ($q->rowCount() > 0):
			foreach ($q->fetchAll(PDO::FETCH_ASSOC) as $row):
				$data[] = $row;
			endforeach;
			return $data;
		else:
			return [];
		endif;
	}
	
	public function getTotalGroup()
	{
		$sql 	= "SELECT group_id FROM group_tbl";
		$exe	= $this->prepare($sql); $exe->execute();
		$count	= $exe->rowCount();
		return ($count > 0) ? $count : 0;
	}
	
	public function getGroup($id = NULL, $multi = FALSE)
	{
		$sql = "SELECT * FROM group_tbl ";
		if ($id != NULL && $multi == FALSE):
			$sql .= "WHERE MD5(group_id) = '". $id ."'";
		elseif ($id != NULL && $multi != FALSE):
			$sql .= "WHERE group_id IN ($id)";
		endif;
		$q = $this->prepare($sql); $response = $q->execute();
		if ($q->rowCount() > 0):
			foreach ($q->fetchAll(PDO::FETCH_ASSOC) as $row):
				$data[] = $row;
			endforeach;
			if ($id != NULL && $multi == FALSE)
				return $data[0];
			else
				return $data;
		endif;
	}
	
	public function getLearnerByGroup($id, $group_leader = NULL)
	{
		$sql  = "SELECT * FROM group_tbl WHERE 1=1 AND ";
		$sql .= "learner != '' AND ";
		if ($group_leader != NULL):
			$sql .= "group_leader = '". $group_leader ."' AND ";
		endif;
		$sql .= " group_id IN ($id)";
		$q	 = $this->prepare($sql); $q->execute();
		if ($q->rowCount() > 0):
			foreach ($q->fetchAll(PDO::FETCH_ASSOC) as $row):
				$data[] = $row;
			endforeach;
			return $data;
		else:
			return [];
		endif;
	}
	
	public function getGroupByEducator($columns, $educator_id)
	{
		$sql = "SELECT $columns FROM group_tbl WHERE group_leader IN ($educator_id)";
		$q	 = $this->prepare($sql); $q->execute();
		if ($q->rowCount() > 0):
			foreach ($q->fetchAll(PDO::FETCH_ASSOC) as $row):
				$data[] = $row;
			endforeach;
			return $data;
		else:
			return [];
		endif;
	}
	
	public function getGroupColumns($id = NULL, $columns = NULL)
	{
		$sql  = "SELECT ";
		$sql .= ($columns == NULL) ? " * " : "GROUP_CONCAT(DISTINCT $columns SEPARATOR ',') AS col";
		$sql .= " FROM group_tbl";
		$sql .= ($id != NULL) ? " WHERE group_id IN ($id)" : "";
		$sql .= ($columns != NULL) ? " AND $columns != '' " : "";
		$q	  = $this->prepare($sql); $q->execute();
		if ($q->rowCount() > 0):
			foreach ($q->fetchAll(PDO::FETCH_ASSOC) as $row):
				$data[] = $row;
			endforeach;
			return $data;
		else:
			return [];
		endif;
	}
	
	public function getUsers($id = NULL)
	{
		$sql = "SELECT * FROM users";
		if ($id != NULL):
			$sql .= " WHERE MD5(id) = '". $id ."'";
		endif;
		$q = $this->prepare($sql); $q->execute();
		if ($q->rowCount() > 0):
			foreach ($q->fetchAll(PDO::FETCH_ASSOC) as $row):
				$data[] = $row;
			endforeach;
			return ($id != NULL) ? $data[0] : $data;
		endif;
	}
	
	public function getTotalUsers($role = NULL)
	{
		$cond = ($role != NULL) ? "WHERE role = '". $role ."'" : '';
		$sql  = "SELECT id FROM users ".(( ! empty($cond)) ? $cond : "");
		$q	  = $this->prepare($sql); $q->execute();
		$count = $q->rowCount();
		return ($count > 0) ? $count : 0;
	}
	
	public function getTotalNewUsers()
	{
		$sql = "SELECT id FROM users WHERE last_login IS NULL";
		$q	 = $this->prepare($sql); $q->execute();
		$count = $q->rowCount();
		return ($count > 0) ? $count : 0;
	}
	
	public function getUserByRole($role = NULL, $id = NULL)
	{
		$sql = "SELECT * FROM users WHERE ";
		if ( $id != NULL && $role != NULL):
			$sql .= "MD5(id) = '". $id ."' AND role = '". $role ."'";
		elseif ( $role != NULL && $id == NULL):
			$sql .= "role IN ($role) ORDER BY username";
		endif;
		$q = $this->prepare($sql); $q->execute();
		if ($q->rowCount() > 0):
			foreach ($q->fetchAll(PDO::FETCH_ASSOC) as $row):
				$data[] = $row;
			endforeach;
			return ($id != NULL && $role != NULL) ? $data[0] : $data;
		endif;
	}
	public function getGroupLearner($columns = NULL)
	{
		$sql  = "SELECT ";
		$sql .= ($columns == NULL) ? " * " : "GROUP_CONCAT(DISTINCT $columns SEPARATOR ',') AS col";
		$sql .= " FROM group_tbl WHERE 1=1";
		$sql .= ($columns != NULL) ? " AND $columns != '' " : "";
		$q	  = $this->prepare($sql); $q->execute();
		if ($q->rowCount() > 0):
			foreach ($q->fetchAll(PDO::FETCH_ASSOC) as $row):
				$data[] = $row;
			endforeach;
			return $data;
		else:
			return [];
		endif;
	}
	
	public function getIndividualLearner($columns = NULL)
	{	$lid  = $this->getGroupLearner('learner')[0]['col'];
		$lid  = rtrim($lid, ',');
		$sql  = "SELECT ";
		$sql .= ($columns == NULL) ? "*" : "$columns";
		$sql .= " FROM users WHERE role = 'learner'";
		if ( ! empty($lid)):
			$sql .= " AND id NOT IN ($lid)";
		endif;
		$q = $this->prepare($sql); $q->execute();
		if ($q->rowCount() > 0):
			foreach ($q->fetchAll(PDO::FETCH_ASSOC) as $row):
				$data[] = $row;
			endforeach;
			return $data;
		else:
			return [];
		endif;
	}
	
	public function getLearnerMultiId($id = NULL)
	{
		$sql = "SELECT id, username, role FROM users ";
		if ($id != NULL):
			$sql .= "WHERE id IN ($id) ";
		endif;
		$sql .= "ORDER BY username";
		$q	 = $this->prepare($sql); $q->execute();
		if ($q->rowCount() > 0):
			foreach ($q->fetchAll(PDO::FETCH_ASSOC) as $row):
				$data[] = $row;
			endforeach;
			return $data;
		else:
			return [];
		endif;
	}
	
	public function getMultiUsers($id = NULL)
	{
		$sql = "SELECT * FROM users WHERE id IN ($id) ORDER BY fname";
		$q	 = $this->prepare($sql); $q->execute();
		if ($q->rowCount() > 0):
			foreach ($q->fetchAll(PDO::FETCH_ASSOC) as $row):
				$data[] = $row;
			endforeach;
			return $data;
		else:
			return [];
		endif;
	}
	
	public function getUsersColumns($id = NULL, $columns = NULL)
	{
		$sql  = "SELECT ";
		$sql .= ($columns == NULL) ? " * " : "$columns";
		$sql .= " FROM users";
		$sql .= ($id != NULL) ? " WHERE id IN ($id)" : "";
		$sql .= " ORDER BY username";
		$q	  = $this->prepare($sql); $q->execute();
		if ($q->rowCount() > 0):
			foreach ($q->fetchAll(PDO::FETCH_ASSOC) as $row):
				$data[] = $row;
			endforeach;
			return $data;
		else:
			return [];
		endif;
	}
	
	public function getCategory($id = NULL)
	{
		$sql = "SELECT * FROM category_tbl WHERE ";
		if ($id != NULL):
			$sql .= "cat_id = '". $id ."'";
		else:
			$sql .= "status = '1' ORDER BY category";
		endif;
		$data = [];
		$q = $this->prepare($sql); $q->execute();
		if ($q->rowCount() > 0):
			foreach ($q->fetchAll(PDO::FETCH_ASSOC) as $row):
				$data[] = $row;
			endforeach;
		endif;
		return ($id != NULL) ? $data[0] : $data;
	}
	
	public function getScenarioStatus($id = NULL)
	{
		$sql = "SELECT * FROM scenario_status";
		if ($id != NULL):
			$sql .= " WHERE MD5(sid) = '". $id ."'";
		endif;
		$q = $this->prepare($sql); $q->execute();
		if ($q->rowCount() > 0):
			foreach ($q->fetchAll(PDO::FETCH_ASSOC) as $row):
				$data[] = $row;
			endforeach;
			return ($id != NULL) ? $data[0] : $data;
		endif;
	}
	
	public function truncateText($text, $chars) {
		$text = strip_tags($text);
		if (strlen($text) < $chars):
			return $text;
		else:
			$text = substr($text, 0, $chars);
			$text = substr($text, 0, strrpos($text, ' '));
			$text = $text.'....';
			return $text;
		endif;
	}

	public function scoreExist($sid, $qid)
	{
		$sql = "SELECT sid FROM score_tbl WHERE scenario_id = '". $sid ."' AND question_id = '". $qid ."' ";
		$q	 = $this->prepare($sql); $q->execute();
		return ($q->rowCount() > 0) ? TRUE : FALSE;
	}
	
	public function getTotalAttemptQues($sid, $uid)
	{
		$lq  = $this->getLastAttempt($sid, $uid);
		$sql = "SELECT sid FROM score_tbl WHERE scenario_id = '". $sid ."' AND uid = '". $lq['uid'] ."' GROUP BY question_id";
		$q	 = $this->prepare($sql); $q->execute();
		$count = $q->rowCount();
		return ($count > 0) ? $count : FALSE;
	}
	
	public function getTotalAttemptQuesOpenResp($sid, $uid)
	{
		$lq  = $this->getLastAttemptOpenResp($sid, $uid);
		$sql = "SELECT open_res_id FROM open_res_tbl WHERE scenario_id = '". $sid ."' AND uid = '". $lq['uid'] ."'";
		$q	 = $this->prepare($sql); $q->execute();
		$count = $q->rowCount();
		return ($count > 0) ? $count : FALSE;
	}

	public function getTotalAttemptQuesMulti($sid, $uid)
	{
		$lq  = $this->getLastAttemptMulti($sid, $uid);
		$sql = "SELECT mid FROM multi_score_tbl WHERE scenario_id = '". $sid ."' AND uid = '". $lq['uid'] ."'";
		$q	 = $this->prepare($sql); $q->execute();
		$count = $q->rowCount();
		return ($count > 0) ? $count : FALSE;
	}
	
	public function isAttempt($sid, $uid = NULL)
	{
		$sql = "SELECT sid FROM score_tbl WHERE scenario_id = '". $sid ."' ".(($uid != NULL) ? " AND userid = '". $uid ."'" : "");
		$q	 = $this->prepare($sql); $q->execute();
		return ($q->rowCount() > 0) ? TRUE : FALSE;
	}
	
	public function isAttemptOpenResTemp($sid, $uid = NULL)
	{
		$sql = "SELECT open_res_id FROM open_res_tbl WHERE scenario_id = '". $sid ."' ".(($uid != NULL) ? " AND userid = '". $uid ."'" : " ");
		$q	 = $this->prepare($sql); $q->execute();
		return ($q->rowCount() > 0) ? TRUE : FALSE;
	}

	public function isAttemptMulti($sid, $uid = NULL)
	{
		$sql = "SELECT mid FROM multi_score_tbl WHERE scenario_id = '". $sid ."' ".(($uid != NULL) ? " AND userid = '". $uid ."'" : " ");
		$q	 = $this->prepare($sql); $q->execute();
		return ($q->rowCount() > 0) ? TRUE : FALSE;
	}
	
	public function dateFormat($date, $format) {
		if (strtotime($date)):
			return date_format(date_create($date), $format);
		endif;
	}
	
	public function checkUserExist($uname, $role) {
		$sql = "SELECT id FROM users WHERE username = '". $uname ."' AND role = '". $role ."'";
		$q	 = $this->prepare($sql); $q->execute();
		return ($q->rowCount() > 0) ? TRUE : FALSE;
	}
	
	public function checkUserNameExist($uname, $getData = NULL) {
		$sql = "SELECT * FROM users WHERE username = '". $uname ."'";
		$q	 = $this->prepare($sql); $q->execute();
		$count = $q->rowCount();
		if ($getData == NULL):
			return ($count > 0) ? TRUE : FALSE;
		elseif ($getData != NULL && $count > 0):
			$data = $q->fetch(PDO::FETCH_ASSOC);
			return $data;
		else:
			return [];
		endif;
	}
	
	public function addMultiIds($muliIds) {
		return implode(',', $muliIds);
	}

	public function updateStatus($status, $scenario_id) {
		$sql = "UPDATE scenario_master SET status = '". $status ."' WHERE MD5(scenario_id) = '". $scenario_id ."'";
		$q	 = $this->prepare($sql);
		return ($q->execute()) ? TRUE : FALSE;
	}
	
	public function updateSimStatus($status, $scenario_id) {
		$sql = "UPDATE scenario_master SET update_sim = '". $status ."' WHERE MD5(scenario_id) = '". $scenario_id ."'";
		$q	 = $this->prepare($sql);
		return ($q->execute()) ? TRUE : FALSE;
	}
	
	public function updateTrueOption($true_options_val, $question_id) {
		$sql = "UPDATE question_tbl SET true_options = '". $true_options_val ."' WHERE question_id = '". $question_id ."'";
		$q	 = $this->prepare($sql);
		return ($q->execute()) ? TRUE : FALSE;
	}
	
	public function getBaseUrl($page = NULL) {
		if ($_SERVER['HTTP_HOST'] == '127.0.0.1' || $_SERVER['HTTP_HOST'] == 'localhost'):
			$baseUrl = $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['HTTP_HOST'] . rtrim(dirname($_SERVER['PHP_SELF']), '/\\') . '/';
		else:
			$http	 = isset($_SERVER['HTTPS']) && strtolower($_SERVER['HTTPS']) == 'on' ? 'https://' : 'http://';
			$baseUrl = $http . $_SERVER['HTTP_HOST'] . rtrim(dirname($_SERVER['PHP_SELF']), '/\\') . '/';
		endif;
		return ($page == NULL) ? $baseUrl : $baseUrl . $page;
	}

	public function getRootUrl($page = NULL) {
		if ($_SERVER['HTTP_HOST'] == '127.0.0.1' || $_SERVER['HTTP_HOST'] == 'localhost'):
			$baseUrl = $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['HTTP_HOST'] . '/';
		else:
			$http	 = isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on' ? 'https://' : 'http://';
			$baseUrl = $http . $_SERVER['HTTP_HOST'] . '/';
		endif;
		return ($page == NULL) ? $baseUrl : $baseUrl . $page;
	}
	
	public function getAttemptQues($sid, $uid = NULL) {
		$sql = "SELECT sid FROM score_tbl WHERE scenario_id = '". $sid ."' ".(($uid != NULL) ? " AND uid = '". $uid ."'" : " ")." GROUP BY question_id ORDER BY sid DESC";
		$q = $this->prepare($sql); $q->execute();
		$count = $q->rowCount();
		return ($count > 0) ? $count : 0;
	}
	
	public function getLastAttempt($sid, $uid) {
		$sql = "SELECT * FROM scenario_attempt_tbl WHERE scenario_id = '". $sid ."' AND userid = '". $uid ."' AND uid IN (SELECT uid FROM score_tbl) ORDER BY aid DESC LIMIT 1";
		$q = $this->prepare($sql); $q->execute();
		if ($q->rowCount() > 0):
			$data = $q->fetch(PDO::FETCH_ASSOC);
			return $data;
		else:
			return [];
		endif;
	}
	
	public function getLastAttemptOpenResp($sid, $uid) {
		$sql = "SELECT * FROM scenario_attempt_tbl WHERE scenario_id = '". $sid ."' AND userid = '". $uid ."' AND uid IN (SELECT uid FROM open_res_tbl) ORDER BY aid DESC LIMIT 1";
		$q = $this->prepare($sql); $q->execute();
		if ($q->rowCount() > 0):
			$data = $q->fetch(PDO::FETCH_ASSOC);
			return $data;
		else:
			return [];
		endif;
	}

	public function getLastAttemptMulti($sid, $uid) {
		$sql = "SELECT * FROM scenario_attempt_tbl WHERE scenario_id = '". $sid ."' AND userid = '". $uid ."' AND uid IN (SELECT uid FROM multi_score_tbl) ORDER BY aid DESC LIMIT 1";
		$q = $this->prepare($sql); $q->execute();
		if ($q->rowCount() > 0):
			$data = $q->fetch(PDO::FETCH_ASSOC);
			return $data;
		else:
			return [];
		endif;
	}
	
	public function UID() {
		$sql = $this->prepare("SELECT UUID() as uid"); $sql->execute();
		if ($sql->rowCount() > 0):
			$row = $sql->fetch(PDO::FETCH_ASSOC);
			$uid = $row['uid'];
		endif;
		return (isset($uid)) ? $uid : md5(time());
	}
	
	public function getCustomerSatisfaction($uid, $sid, $column, $tble_name) {
		$sql   = $this->prepare("SELECT $column FROM $tble_name WHERE uid = '". $uid ."' AND scenario_id = '". $sid ."' GROUP BY question_id"); $sql->execute();
		$count = $sql->rowCount();
		return ($count > 0) ? $count : 0;
	}

	public function getBranchingSatisfaction($uid, $sid, $column, $tble_name, $multi = FALSE) {
		$totalQuestions = $this->getTotalQuestionsByScenario(md5($sid));
		if ($multi == FALSE):
			$sql = $this->prepare("SELECT choice_option_id FROM $tble_name WHERE uid = '". $uid ."' AND scenario_id = '". $sid ."' ORDER BY $column DESC LIMIT 1"); $sql->execute();
			if ($sql->rowCount() > 0):
				$row = $sql->fetch(PDO::FETCH_ASSOC);
				if ( ! empty($row['choice_option_id'])):
					$endsql	= $this->prepare("SELECT End_Sim FROM answer_tbl WHERE answer_id = '". $row['choice_option_id'] ."'"); $endsql->execute();
					return ($endsql->rowCount() > 0) ? $totalQuestions : $this->getCustomerSatisfaction($uid, $sid, $column, $tble_name);
				endif;
			endif;
		elseif ($multi == TRUE):
			$sql = $this->prepare("SELECT question_id, status FROM $tble_name WHERE uid = '". $uid ."' AND scenario_id = '". $sid ."' ORDER BY $column DESC LIMIT 1"); $sql->execute();
			$tq  = $sql->rowCount();
			if ($tq > 0):
				$row = $sql->fetch(PDO::FETCH_ASSOC);
				if ( ! empty($row['question_id'])):
					$endsql	= $this->prepare("SELECT EndSim, feedback_type FROM feedback_tbl WHERE question_id = '". $row['question_id'] ."'"); $endsql->execute();
					$tq  = $endsql->rowCount();
					$endrow = $endsql->fetchAll(PDO::FETCH_ASSOC);
					if ( ! empty($row['status']) && $endrow[0]['feedback_type'] == 1 && ! empty($endrow[0]['EndSim'])):
						return $totalQuestions;
					elseif ( ! empty($row['status']) && $endrow[0]['feedback_type'] == 1 && empty($endrow[0]['EndSim'])):
						return $tq;
					elseif (empty($row['status']) && $endrow[1]['feedback_type'] == 2 && ! empty($endrow[1]['EndSim'])):
						return $totalQuestions;
					elseif (empty($row['status']) && $endrow[1]['feedback_type'] == 2 && empty($endrow[1]['EndSim'])):
						return $tq;
					endif;
				endif;
			endif;
		endif;
	}
	
	public function checkQesExist($uid, $scenario_id, $question_id) {
		$sql = "SELECT sid FROM score_tbl WHERE uid = '". $uid ."' AND MD5(scenario_id) = '". $scenario_id ."' AND question_id = '". $question_id ."' ORDER BY sid DESC";
		$q   = $this->prepare($sql); $q->execute();
		return ($q->rowCount() > 0) ? TRUE : FALSE;
	}
	
	public function checkQesAnsExist($uid, $scenario_id, $question_id, $answer_id) {
		$sql = "SELECT sid FROM score_tbl WHERE uid = '". $uid ."' AND MD5(scenario_id) = '". $scenario_id ."' AND question_id = '". $question_id ."' AND answer_id = '". $answer_id ."' ORDER BY sid DESC";
		$q	 = $this->prepare($sql); $q->execute();
		return ($q->rowCount() > 0) ? TRUE : FALSE;
	}

	public function readyToReviewr($sid){
		$sql = "UPDATE scenario_master SET `status` = '7', `assign_reviewer` = '0' WHERE MD5(scenario_id) = '". $sid ."'";
		$q	 = $this->prepare($sql);
		return ($q->execute()) ? TRUE : FALSE;
	}
	
	public function getEmailTemplate($id = NULL, $type = NULL)
	{
		$sql = "SELECT * FROM email_template_tbl WHERE ";
		if ($id != NULL && $type == NULL):
			$sql .= "MD5(eid) = '". $id ."'";
		elseif ($id == NULL && $type != NULL):
			$sql .= "type = '". $type ."'";
		endif;
		$q = $this->prepare($sql); $q->execute();
		if ($q->rowCount() > 0):
			$data = $q->fetch(PDO::FETCH_ASSOC);
			return $data;
		else:
			return [];
		endif;
	}
	
	public function userEmailExists($email, $getData = NULL) {
		$sql = "SELECT * FROM users WHERE email = '". $email ."'";
		$q	 = $this->prepare($sql); $q->execute();
		$count = $q->rowCount();
		if ($getData == NULL):
			return ($count > 0) ? TRUE : FALSE;
		elseif ($getData != NULL):
			if ($count > 0):
				$data = $q->fetch(PDO::FETCH_ASSOC);
				return $data;
			endif;
		endif;
	}
	
	public function get_random_string($length = 20) {
		$key  = '';
		$keys = array_merge(range(0, 9), range('a', 'z'));
		for ($i = 0; $i < $length; $i++):
			$key .= $keys[array_rand($keys)];
		endfor;
		return $key;
	}
	
	public function updatePassword($pwd, $id, $role){
		$sql = "UPDATE users SET `password` = '". md5($pwd) ."', `last_change_pwd` = '". date('Y-m-d') ."' WHERE id = '". $id ."' AND role = '". $role ."'";
		$q	 = $this->prepare($sql);
		return ($q->execute()) ? TRUE : FALSE;
	}
	
	public function updateNextQues($qid, $aid, $sid){
		$sql = "UPDATE answer_tbl SET `nextQid` = '". $qid ."' WHERE answer_id = '". $aid ."'";
		$q	 = $this->prepare($sql);
		return ($q->execute()) ? TRUE : FALSE;
	}
	
	public function getScenarioCompetency($sim_id)
	{
		if ($sim_id != NULL):
			$sql = "SELECT * FROM competency_tbl WHERE MD5(scenario_id) = '". $sim_id ."'";
			$q	 = $this->prepare($sql); $q->execute();
			if ($q->rowCount() > 0):
				$data = $q->fetch(PDO::FETCH_ASSOC);
				return $data;
			else:
				return [];
			endif;
		endif;
	}
	
	public function getScenarioQuestion($sim_id, $all = FALSE)
	{
		if ($sim_id != NULL && $all == FALSE):
			$sql = "SELECT *, SUM(ques_val_1) AS ques_val_1, SUM(ques_val_2) AS ques_val_2, SUM(ques_val_3) AS ques_val_3, SUM(ques_val_4) AS ques_val_4, SUM(ques_val_5) AS ques_val_5, SUM(ques_val_6) AS ques_val_6 FROM question_tbl WHERE MD5(scenario_id) = '". $sim_id ."'";
		else:
			$sql = "SELECT questions AS name FROM question_tbl WHERE MD5(scenario_id) = '". $sim_id ."'";
		endif;
		$q = $this->prepare($sql); $q->execute();
		if ($q->rowCount() > 0):
			foreach ($q->fetchAll(PDO::FETCH_ASSOC) as $row):
				$data[] = $row;
			endforeach;
			return ($sim_id != NULL && $all == FALSE) ? $data[0] : $data;
		else:
			return [];
		endif;
	}
	
	public function getTotalTime($scenario_id, $userid) {
		$sql = "SELECT SEC_TO_TIME( SUM( TIME_TO_SEC( tmp.timediff ) ) ) AS timeSum FROM (SELECT subq.uid, subq.startdate, subq.enddate, TIMEDIFF(subq.enddate, subq.startdate) AS timediff FROM (SELECT a.*, TIME(a.attempt_date) AS startdate, MAX(TIME(s.cur_date_time)) AS enddate FROM `scenario_attempt_tbl` a LEFT JOIN score_tbl s ON a.uid = s.uid WHERE a.userid = '". $userid ."' AND s.scenario_id = '". $scenario_id ."' AND s.uid IN (SELECT uid FROM score_tbl) GROUP BY a.aid ORDER BY s.cur_date_time DESC) subq) tmp";
		$q	 = $this->prepare($sql); $q->execute();
		if ($q->rowCount() > 0):
			$data = $q->fetch(PDO::FETCH_ASSOC);
			return $data;
		else:
			return [];
		endif;
	}
	
	public function getLastTotalTime($scenario_id, $userid, $uid) {
		$sql = "SELECT SEC_TO_TIME( SUM( TIME_TO_SEC( tmp.timediff ) ) ) AS timeSum FROM (SELECT subq.uid, subq.startdate, subq.enddate, TIMEDIFF(subq.enddate, subq.startdate) AS timediff FROM (SELECT a.*, TIME(a.attempt_date) AS startdate, MAX(TIME(s.cur_date_time)) AS enddate FROM `scenario_attempt_tbl` a LEFT JOIN score_tbl s ON a.uid = s.uid WHERE a.userid = '". $userid ."' AND s.scenario_id = '". $scenario_id ."' AND s.uid = '". $uid ."' AND s.uid IN (SELECT uid FROM score_tbl) GROUP BY a.aid ORDER BY s.cur_date_time DESC) subq) tmp";
		$q	 = $this->prepare($sql); $q->execute();
		if ($q->rowCount() > 0):
			$data = $q->fetch(PDO::FETCH_ASSOC);
			return $data;
		else:
			return [];
		endif;
	}
	
	public function getLearnerTotalTime($uid) {
		$sql = "SELECT SEC_TO_TIME( SUM( TIME_TO_SEC( tmp.timediff ) ) ) AS timeSum FROM (SELECT subq.uid, subq.startdate, subq.enddate, TIMEDIFF(subq.enddate, subq.startdate) AS timediff FROM (SELECT a.*, TIME(a.attempt_date) AS startdate, MAX(TIME(s.cur_date_time)) AS enddate FROM `scenario_attempt_tbl` a LEFT JOIN score_tbl s ON a.uid = s.uid WHERE a.userid = '". $uid ."' AND s.uid IN (SELECT uid FROM score_tbl) GROUP BY a.aid ORDER BY s.cur_date_time DESC) subq) tmp";
		$q	 = $this->prepare($sql); $q->execute();
		if ($q->rowCount() > 0):
			$data = $q->fetch(PDO::FETCH_ASSOC);
			return $data;
		else:
			return [];
		endif;
	}
	
	public function getOpenResTotalTime($scenario_id, $userid) {
		$sql = "SELECT SEC_TO_TIME( SUM( TIME_TO_SEC( tmp.timediff ) ) ) AS timeSum FROM (SELECT subq.uid, subq.startdate, subq.enddate, TIMEDIFF(subq.enddate, subq.startdate) AS timediff FROM (SELECT a.*, TIME(a.attempt_date) AS startdate, MAX(TIME(s.cur_date_time)) AS enddate FROM `scenario_attempt_tbl` a LEFT JOIN open_res_tbl s ON a.uid = s.uid WHERE a.userid = '". $userid ."' AND s.scenario_id = '". $scenario_id ."' AND s.uid IN (SELECT uid FROM open_res_tbl) GROUP BY a.aid ORDER BY s.cur_date_time DESC) subq) tmp";
		$q	 = $this->prepare($sql); $q->execute();
		if ($q->rowCount() > 0):
			$data = $q->fetch(PDO::FETCH_ASSOC);
			return $data;
		else:
			return [];
		endif;
	}
	
	public function getOpenResLastTotalTime($scenario_id, $userid, $uid) {
		$sql = "SELECT SEC_TO_TIME( SUM( TIME_TO_SEC( tmp.timediff ) ) ) AS timeSum FROM (SELECT subq.uid, subq.startdate, subq.enddate, TIMEDIFF(subq.enddate, subq.startdate) AS timediff FROM (SELECT a.*, TIME(a.attempt_date) AS startdate, MAX(TIME(s.cur_date_time)) AS enddate FROM `scenario_attempt_tbl` a LEFT JOIN open_res_tbl s ON a.uid = s.uid WHERE a.userid = '". $userid ."' AND s.scenario_id = '". $scenario_id ."' AND s.uid = '". $uid ."' AND s.uid IN (SELECT uid FROM open_res_tbl) GROUP BY a.aid ORDER BY s.cur_date_time DESC) subq) tmp";
		$q	 = $this->prepare($sql); $q->execute();
		if ($q->rowCount() > 0):
			$data = $q->fetch(PDO::FETCH_ASSOC);
			return $data;
		else:
			return [];
		endif;
	}

	public function getMultiTotalTime($scenario_id, $userid) {
		$sql = "SELECT SEC_TO_TIME( SUM( TIME_TO_SEC( tmp.timediff ) ) ) AS timeSum FROM (SELECT subq.uid, subq.startdate, subq.enddate, TIMEDIFF(subq.enddate, subq.startdate) AS timediff FROM (SELECT a.*, TIME(a.attempt_date) AS startdate, MAX(TIME(s.cur_date_time)) AS enddate FROM `scenario_attempt_tbl` a LEFT JOIN multi_score_tbl s ON a.uid = s.uid WHERE a.userid = '". $userid ."' AND s.scenario_id = '". $scenario_id ."' AND s.uid IN (SELECT uid FROM multi_score_tbl) GROUP BY a.aid ORDER BY s.cur_date_time DESC) subq) tmp";
		$q	 = $this->prepare($sql); $q->execute();
		if ($q->rowCount() > 0):
			$data = $q->fetch(PDO::FETCH_ASSOC);
			return $data;
		else:
			return [];
		endif;
	}

	public function getLastTotalTimeMulti($scenario_id, $userid, $uid) {
		$sql = "SELECT SEC_TO_TIME( SUM( TIME_TO_SEC( tmp.timediff ) ) ) AS timeSum FROM (SELECT subq.uid, subq.startdate, subq.enddate, TIMEDIFF(subq.enddate, subq.startdate) AS timediff FROM (SELECT a.*, TIME(a.attempt_date) AS startdate, MAX(TIME(s.cur_date_time)) AS enddate FROM `scenario_attempt_tbl` a LEFT JOIN multi_score_tbl s ON a.uid = s.uid WHERE a.userid = '". $userid ."' AND s.scenario_id = '". $scenario_id ."' AND s.uid = '". $uid ."' AND s.uid IN (SELECT uid FROM multi_score_tbl) GROUP BY a.aid ORDER BY s.cur_date_time DESC) subq) tmp";
		$q	 = $this->prepare($sql); $q->execute();
		if ($q->rowCount() > 0):
			$data = $q->fetch(PDO::FETCH_ASSOC);
			return $data;
		else:
			return [];
		endif;
	}
	
	public function getUpload($type){
		$sql = "SELECT upload_id, img_name FROM upload_tbl WHERE type IN ($type)";
		$q	 = $this->prepare($sql); $q->execute();
		if ($q->rowCount() > 0):
			foreach ($q->fetchAll(PDO::FETCH_ASSOC) as $row):
				$data[] = $row;
			endforeach;
			return $data;
		else:
			return [];
		endif;
	}
	
	public function getLastAttemptSIM($uid) {
		$sql = "SELECT DISTINCT st.aid, st.uid, st.userid, s.scenario_id, s.Scenario_title AS sim_title, s.duration, s.passing_marks, s.scenario_category, s.status FROM 
				scenario_attempt_tbl AS st LEFT JOIN 
				scenario_master AS s ON s.scenario_id = st.scenario_id WHERE 
				st.userid = '". $uid ."' AND (st.uid IN (SELECT uid FROM score_tbl) OR st.uid IN (SELECT uid FROM multi_score_tbl) OR st.uid IN (SELECT uid FROM open_res_tbl)) ORDER BY st.aid DESC LIMIT 1";
		$q	 = $this->prepare($sql); $q->execute();
		if ($q->rowCount() > 0):
			$row = $q->fetch(PDO::FETCH_ASSOC);
		else:
			$row = [];
		endif;
		return $row;
	}
	
	public function getAuthorNewSim($uid) {
		$sql = "SELECT scenario_id FROM scenario_master WHERE assign_author != '' AND FIND_IN_SET('". $uid ."', assign_author) AND author_scenario_type = '0' AND sim_ques_type = '0'";
		$q	 = $this->prepare($sql); $q->execute();
		$count = $q->rowCount();
		return ($count > 0) ? $count : 0;
	}

	public function getAuthorTotalSim($userid) {
		$sql = "SELECT DISTINCT s.scenario_id FROM scenario_master s INNER JOIN group_tbl g LEFT JOIN category_tbl c ON c.cat_id = s.scenario_category WHERE 
				(CASE WHEN s.assign_author != '' THEN FIND_IN_SET('". $userid ."', s.assign_author) WHEN s.assign_group != '' THEN FIND_IN_SET('". $userid ."', g.author) OR FIND_IN_SET('". $userid ."', s.assign_author) END)";
		$q	 = $this->prepare($sql); $q->execute();
		$count = $q->rowCount();
		return ($count > 0) ? $count : 0;
	}

	public function getAuthorPendingSim($userid) {
		$sql = "SELECT DISTINCT s.scenario_id FROM scenario_master s INNER JOIN group_tbl g LEFT JOIN category_tbl c ON c.cat_id = s.scenario_category WHERE 
				s.scenario_id NOT IN (SELECT scenario_id FROM question_tbl) AND (CASE WHEN s.assign_author != '' THEN FIND_IN_SET('". $userid ."', s.assign_author) WHEN s.assign_group != '' THEN FIND_IN_SET('". $userid ."', g.author) OR FIND_IN_SET('". $userid ."', s.assign_author) END)";
		$q	 = $this->prepare($sql); $q->execute();
		$count = $q->rowCount();
		return ($count > 0) ? $count : 0;
	}

	public function getReviewerNewSim($userid) {
		$sql = "SELECT DISTINCT s.scenario_id, s.Scenario_title FROM scenario_master s INNER JOIN group_tbl g LEFT JOIN category_tbl c ON c.cat_id = s.scenario_category WHERE 
				s.author_scenario_type = '0' AND s.sim_ques_type = '0' AND (CASE WHEN s.assign_reviewer != '' THEN FIND_IN_SET('". $userid ."', s.assign_reviewer) WHEN s.assign_group != '' THEN FIND_IN_SET('". $userid ."', g.reviewer) OR FIND_IN_SET('". $userid ."', s.assign_reviewer) END)";
		$q	 = $this->prepare($sql); $q->execute();
		$count = $q->rowCount();
		return ($count > 0) ? $count : 0;
	}

	public function getReviewerTotalSim($userid) {
		$sql = "SELECT DISTINCT s.scenario_id FROM scenario_master s INNER JOIN group_tbl g LEFT JOIN category_tbl c ON c.cat_id = s.scenario_category WHERE s.author_scenario_type != '0' AND 
				(CASE WHEN s.assign_reviewer != '' THEN FIND_IN_SET('". $userid ."', s.assign_reviewer) WHEN s.assign_group != '' THEN FIND_IN_SET('". $userid ."', g.reviewer) OR FIND_IN_SET('". $userid ."', s.assign_reviewer) END)";
		$q	 = $this->prepare($sql); $q->execute();
		$count = $q->rowCount();
		return ($count > 0) ? $count : 0;
	}

	public function getReviewerPendingSim($userid) {
		$sql = "SELECT DISTINCT s.scenario_id FROM scenario_master s INNER JOIN group_tbl g LEFT JOIN category_tbl c ON c.cat_id = s.scenario_category WHERE 
				s.scenario_id NOT IN (SELECT scenario_id FROM question_tbl) AND (CASE WHEN s.assign_reviewer != '' THEN FIND_IN_SET('". $userid ."', s.assign_reviewer) WHEN s.assign_group != '' THEN FIND_IN_SET('". $userid ."', g.reviewer) OR FIND_IN_SET('". $userid ."', s.assign_reviewer) END)";
		$q	 = $this->prepare($sql); $q->execute();
		$count = $q->rowCount();
		return ($count > 0) ? $count : 0;
	}
	
	public function checkLastChangePassword($uid){
		$sql = "SELECT last_change_pwd FROM users WHERE id = '". $uid ."' AND last_change_pwd <= DATE_SUB(CURDATE(), INTERVAL 3 MONTH)";
		$q	 = $this->prepare($sql); $q->execute();
		return ($q->rowCount() > 0) ? TRUE : FALSE;
	}
	
	public function getLearner($id){
		if ( ! empty($id)):
			$data = $this->getLearnerMultiId($id);
			if ( ! empty($data)):
				foreach ($data as $l):
					$name[] = ucwords($l['username']);
				endforeach;
				return $this->addMultiIds($name);
			endif;
		else:
			return [];
		endif;
	}
	
	public function SupportStatus($sid = NULL) {
		$status = array('1' => 'Processing',
						'2' => 'Complete',
						'3' => 'Closed',
						'4' => 'On Hold',
						'5' => 'Working');
		return (isset($status[(int)$sid]) && $sid != NULL) ? $status[(int)$sid] : $status;
	}
	
	public function checkShuffleQues($qid, $type = 2){
		if ($type == 2):
			$sql = "SELECT question_id FROM question_tbl WHERE question_type = 1 AND question_id = '". $qid ."' AND question_id IN (SELECT question_id FROM answer_tbl WHERE choice_order_no = 0 AND match_order_no = 0)";
		elseif ($type == 1):
			$sql = "SELECT question_id FROM question_tbl WHERE question_type = 1 AND question_id = '". $qid ."' AND question_id IN (SELECT question_id FROM answer_tbl WHERE choice_order_no != 0 AND match_order_no != 0)";
		endif;
		$q	 = $this->prepare($sql); $q->execute();
		return ($q->rowCount() > 0) ? TRUE : FALSE;
	}
	
	public function hmsToSeconds($time){
		$time = explode(':', $time);
		return (+$time[0]) * 60 + (+$time[1]);
	}
	
	public function getAssignSim()
	{
		$sql = "SELECT GROUP_CONCAT(DISTINCT scenario_id) AS sim_id FROM assignment_tbl";
		$q	 = $this->prepare($sql); $q->execute();
		if ($q->rowCount() > 0):
			foreach ($q->fetchAll(PDO::FETCH_ASSOC) as $row):
				$data[] = $row['sim_id'];
			endforeach;
			return $data[0];
		else:
			return [];
		endif;
	}
	
	public function getAssignSimByGroupId($group_id)
	{
		$sql = "SELECT GROUP_CONCAT(DISTINCT scenario_id) AS sim_id FROM assignment_tbl WHERE group_id IN ($group_id)";
		$q	 = $this->prepare($sql); $q->execute();
		if ($q->rowCount() > 0):
			foreach ($q->fetchAll(PDO::FETCH_ASSOC) as $row):
				$data[] = $row['sim_id'];
			endforeach;
			return $data[0];
		else:
			return [];
		endif;
	}
	
	public function getAssignSimGroup($sim_id)
	{
		$sql = "SELECT GROUP_CONCAT(DISTINCT group_id) AS gid FROM assignment_tbl WHERE FIND_IN_SET('". $sim_id ."', scenario_id)";
		$q	 = $this->prepare($sql); $q->execute();
		if ($q->rowCount() > 0):
			foreach ($q->fetchAll(PDO::FETCH_ASSOC) as $row):
				$data[] = $row['gid'];
			endforeach;
			return $this->addMultiIds($data);
		else:
			return [];
		endif;
	}
	
	public function getAssignSimLearner($sim_id)
	{
		$sql = "SELECT GROUP_CONCAT(DISTINCT g.learner) AS lid FROM assignment_tbl a LEFT JOIN group_tbl g ON a.group_id = g.group_id WHERE FIND_IN_SET('". $sim_id ."', a.scenario_id) AND a.group_id != ''";
		$q	 = $this->prepare($sql); $q->execute();
		if ($q->rowCount() > 0):
			$data = $q->fetchAll(PDO::FETCH_ASSOC);
			$lid  = $data[0]['lid'];
			if (strpos($lid, ',') == true):
				return implode(',', array_unique(explode(',', $lid)));
			else:
				return $lid;
			endif;
		else:
			return [];
		endif;
	}
	
	public function getAssignSimData($uid)
	{
		#--------Group-Learner-------
		$sql = "SELECT GROUP_CONCAT(DISTINCT scenario_id) AS sim_id FROM assignment_tbl a LEFT JOIN group_tbl g ON a.group_id = g.group_id WHERE a.scenario_id != '' AND a.group_id != '' AND FIND_IN_SET('". $uid ."', g.learner)";
		$q	 = $this->prepare($sql); $q->execute();
		if ($q->rowCount() > 0):
			foreach ($q->fetchAll(PDO::FETCH_ASSOC) as $row):
				$gdata[] = $row['sim_id'];
			endforeach;
		endif;
		#----------Learner----------
		$sql = "SELECT GROUP_CONCAT(DISTINCT scenario_id) AS sim_id FROM assignment_tbl WHERE learner_id != '' AND FIND_IN_SET('". $uid ."', learner_id)";
		$q	 = $this->prepare($sql); $q->execute();
		if ($q->rowCount() > 0):
			foreach ($q->fetchAll(PDO::FETCH_ASSOC) as $row):
				$ldata[] = $row['sim_id'];
			endforeach;
		endif;
		$res = $this->addMultiIds(array_filter(array_unique(array_merge($gdata, $ldata))));
		return ( ! empty($res)) ? $res : 0;
	}
	
	public function getAssignSimOtherData($sim_id, $user_id)
	{
		#--------Group-Learner-------
		$sql = "SELECT * FROM assignment_tbl a LEFT JOIN group_tbl g ON a.group_id = g.group_id WHERE FIND_IN_SET('". $sim_id ."', a.scenario_id) AND a.group_id != '' AND FIND_IN_SET('". $user_id ."', g.learner) ORDER BY a.assignment_id DESC LIMIT 1";
		$q	 = $this->prepare($sql); $q->execute();
		if ($q->rowCount() > 0):
			foreach ($q->fetchAll(PDO::FETCH_ASSOC) as $row):
				$data[] = $row;
			endforeach;
		else:
			#--------Learner--------
			$sql = "SELECT * FROM assignment_tbl WHERE FIND_IN_SET('". $sim_id ."', scenario_id) AND learner_id != '' AND FIND_IN_SET('". $user_id ."', learner_id) ORDER BY assignment_id DESC LIMIT 1";
			$q	 = $this->prepare($sql); $q->execute();
			if ($q->rowCount() > 0):
				foreach ($q->fetchAll(PDO::FETCH_ASSOC) as $row):
					$data[] = $row;
				endforeach;
			endif;
		endif;
		return ( ! empty($data)) ? $data : [];
	}
	
	public function GetSupport($support_id)
	{
		if ($support_id != NULL):
			$sql = "SELECT * FROM support_tbl WHERE support_id = '". $support_id ."'";
			$q	 = $this->prepare($sql); $q->execute();
			if ($q->rowCount() > 0):
				$data = $q->fetch(PDO::FETCH_ASSOC);
				return $data;
			else:
				return [];
			endif;
		endif;
	}
	
	public function GetEducator($gid)
	{
		$sql = "SELECT DISTINCT u.id, u.username, g.group_name, g.learner FROM users AS u LEFT JOIN group_tbl AS g ON u.id = g.group_leader WHERE g.group_leader != '0' AND learner != '' AND g.group_id IN ($gid)";
		$q	 = $this->prepare($sql); $q->execute();
		if ($q->rowCount() > 0):
			foreach ($q->fetchAll(PDO::FETCH_ASSOC) as $row):
				$data[] = $row;
			endforeach;
			return $data;
		else:
			return [];
		endif;
	}
	
	public function GetResetLearner($gid)
	{
		$sql = "SELECT u.id, u.username, CONCAT(u.fname, ' ', u.lname) AS full_name, u.email FROM users AS u LEFT JOIN group_tbl AS g ON FIND_IN_SET(u.id, g.learner) WHERE g.group_id IN ($gid)";
		$q	 = $this->prepare($sql); $q->execute();
		if ($q->rowCount() > 0):
			foreach ($q->fetchAll(PDO::FETCH_ASSOC) as $row):
				$data[] = $row;
			endforeach;
			return $data;
		else:
			return [];
		endif;
	}
	
	public function getSimPage($sim_id){
		if ($sim_id != NULL):
			$sql = "SELECT * FROM sim_pages WHERE scenario_id = '". $sim_id ."'";
			$q	 = $this->prepare($sql); $q->execute();
			if ($q->rowCount() > 0):
				foreach ($q->fetchAll(PDO::FETCH_ASSOC) as $row):
					$data[] = $row;
				endforeach;
				return $data;
			else:
				return [];
			endif;
		endif;
	}
	
	public function getSimBranding($sim_id){
		if ($sim_id != NULL):
			$sql = "SELECT * FROM sim_branding_tbl WHERE scenario_id = '". $sim_id ."'";
			$q = $this->prepare($sql); $q->execute();
			if ($q->rowCount() > 0):
				$data = $q->fetch(PDO::FETCH_ASSOC);
				return $data;
			else:
				return [];
			endif;
		endif;
	}
	
	public function getQuestionList($sim_id)
	{
		$sql = "SELECT question_id, videoq_cue_point, LEFT(questions, 8) AS ques FROM question_tbl WHERE MD5(scenario_id) = '". $sim_id ."' ORDER BY qorder = 0, qorder";
		$q = $this->prepare($sql); $q->execute();
		if ($q->rowCount() > 0):
			foreach ($q->fetchAll(PDO::FETCH_ASSOC) as $row):
				$data[] = array('ques_id' => base64_encode($row['question_id']), 'qid' => $row['question_id'], 'qname' => $row['ques'] . '...', 'cue' => $row['videoq_cue_point']);
			endforeach;
			return $data;
		else:
			return [];
		endif;
	}
	
	public function getCuePointList($sim_id)
	{
		$sql = "SELECT question_id, videoq_cue_point FROM question_tbl WHERE MD5(scenario_id) = '". $sim_id ."' ORDER BY qorder = 0, qorder";
		$q = $this->prepare($sql); $q->execute();
		if ($q->rowCount() > 0):
			foreach ($q->fetchAll(PDO::FETCH_ASSOC) as $row):
				$data[] = array('ques_id' => base64_encode($row['question_id']), 'qid' => $row['question_id'], 'qname' => $row['videoq_cue_point']);
			endforeach;
			return $data;
		else:
			return [];
		endif;
	}
	
	public function getFeedback($question_id = NULL, $answer_id = NULL, $sub_options_id = NULL, $feedback_type = NULL, $all = NULL){
		$sql = "SELECT * FROM feedback_tbl WHERE 1=1 ";
		if ($question_id != NULL):
			$sql .= "AND question_id = '". $question_id ."'";
		endif;
		if ($answer_id != NULL):
			$sql .= " AND answer_id = '". $answer_id ."'";
		endif;
		if ($sub_options_id != NULL):
			$sql .= " AND sub_options_id = '". $sub_options_id ."'";
		endif;
		if ($feedback_type != NULL):
			$sql .= " AND feedback_type = '". $feedback_type ."'";
		endif;
		$q = $this->prepare($sql); $q->execute();
		if ($q->rowCount() > 0):
			foreach ($q->fetchAll(PDO::FETCH_ASSOC) as $row):
				$data[] = $row;
			endforeach;
			return ($all != NULL) ? $data : $data[0];
		else:
			return [];
		endif;
	}
	
	public function feedback_remove($qid){
		$sql = "DELETE FROM feedback_tbl WHERE question_id = '". $qid ."'";
		$exe = $this->prepare($sql); $exe->execute();
	}
	
	public function getSubOptions($answer_id, $column = NULL)
	{
		$sql  = ($column != NULL) ? "SELECT $column" : "SELECT *";
		$sql .= " FROM sub_options_tbl WHERE answer_id IN ($answer_id)";
		$q = $this->prepare($sql); $q->execute();
		if ($q->rowCount() > 0):
			foreach ($q->fetchAll(PDO::FETCH_ASSOC) as $row):
				$data[] = ($column != NULL) ? $row[$column] : $row;
			endforeach;
			return $data;
		else:
			return [];
		endif;
	}
	
	public function getEditAuthPage($sim_type, $temp_type, $sim_id){
		$url = '';
		if ($sim_type == 1):
			if ($temp_type == 1):
				$url = 'linear-classic-template.php?add_data=true&sim_id='. $sim_id;
			elseif ($temp_type == 2):
				$url = 'linear-multiple-template.php?add_data=true&sim_id='. $sim_id;			
			endif;
		elseif ($sim_type == 2):
			if ($temp_type == 4):
				$url = 'branching-classic-template.php?add_data=true&sim_id='. $sim_id;
			elseif ($temp_type == 5):
				$url = 'branching-multiple-template.php?add_data=true&sim_id='. $sim_id;
			endif;
		elseif ($sim_type == 3):
			if ($temp_type == 7):
				$url = 'open-response-template.php?add_data=true&sim_id='. $sim_id;
			endif;
		elseif ($sim_type == 4):
			if ($temp_type == 8):
				$url = 'update-single-video-template.php?add_data=true&sim_id='. $sim_id;
			endif;
		elseif ($sim_type == 5):
			if ($temp_type == 6):
				$url = 'linear-classic-video-template.php?add_data=true&sim_id='. $sim_id;
			elseif ($temp_type == 9):
				$url = 'linear-video-multiple-template.php?add_data=true&sim_id='. $sim_id;
			endif;
		elseif ($sim_type == 6):
			if ($temp_type == 10):
				$url = 'branching-video-classic-template.php?add_data=true&sim_id='. $sim_id;
			elseif ($temp_type == 11):
				$url = 'branching-video-multiple-template.php?add_data=true&sim_id='. $sim_id;
			endif;
		endif;
		return $url;
	}

	public function getPreviewAuthPage($sim_type, $temp_type, $sim_id){
		$url = '';
		if ($sim_type == 1):
			if ($temp_type == 1):
				$url = 'launch-sim-linear-classic.php?preview=true&save=false&sim_id='. $sim_id;
			elseif ($temp_type == 2):
				$url = 'launch-sim-linear-multiple.php?preview=true&save=false&sim_id='. $sim_id;
			endif;
		elseif ($sim_type == 2):
			if ($temp_type == 4):
				$url = 'launch-sim-branching-classic.php?preview=true&save=false&sim_id='. $sim_id;
			elseif ($temp_type == 5):
				$url = 'launch-sim-branching-multiple.php?preview=true&save=false&sim_id='. $sim_id;
			endif;
		elseif ($sim_type == 3):
			if ($temp_type == 7):
				$url = 'launch-sim-open-response-preview.php?preview=true&save=false&sim_id='. $sim_id;
			endif;
		elseif ($sim_type == 4):
			if ($temp_type == 8):
				$url = 'launch-single-video.php?preview=true&save=false&sim_id='. $sim_id;
			endif;
		elseif ($sim_type == 5):
			if ($temp_type == 6):
				$url = '';
			elseif ($temp_type == 9):
				$url = '';
			endif;
		elseif($sim_type == 6):
			if ($temp_type == 10):
				$url = 'launch-branching-video-classic.php?preview=true&save=false&sim_id='. $sim_id;
			elseif ($temp_type == 11):
				$url = 'launch-branching-video-multiple.php?preview=true&save=false&sim_id='. $sim_id;
			endif;
		endif;
		return $url;
	}
	
	public function getScoreAchievedOpenResp($sim_id, $uid){
		#------------------------TOTAL PASS/FAIL-------------------------------
		$sql = "SELECT * FROM competency_tbl WHERE scenario_id = '". $sim_id ."'";
		$q	 = $this->prepare($sql); $q->execute();
		$row = $q->fetch(PDO::FETCH_ASSOC);
		
		$sqlSc = "SELECT SUM(ans_val1) AS com_score_1, SUM(ans_val2) AS com_score_2, SUM(ans_val3) AS com_score_3, SUM(ans_val4) AS com_score_4, SUM(ans_val5) AS com_score_5, SUM(ans_val6) AS com_score_6 FROM open_res_tbl ans WHERE scenario_id = '". $sim_id ."' AND uid = '". $uid ."'";
		$sc = $this->prepare($sqlSc); $sc->execute();
		foreach ($sc->fetchAll(PDO::FETCH_ASSOC) as $scRow):
			$wedata[] = $scRow;
		endforeach;
		
		$sqlQSc = "SELECT SUM(q.ques_val_1) AS com_qes_score_1, SUM(q.ques_val_2) AS com_qes_score_2, SUM(q.ques_val_3) AS com_qes_score_3, SUM(q.ques_val_4) AS com_qes_score_4, SUM(q.ques_val_5) AS com_qes_score_5, SUM(q.ques_val_6) AS com_qes_score_6 FROM open_res_tbl sc LEFT JOIN question_tbl q ON sc.question_id = q.question_id WHERE sc.scenario_id = '". $sim_id ."' AND sc.uid = '". $uid ."'";
		$qsc = $this->prepare($sqlQSc); $qsc->execute();
		foreach ($qsc->fetchAll(PDO::FETCH_ASSOC) as $QscRow):
			$qdata[] = $QscRow;
		endforeach;
		
		@$getweightage_1 = ( ! empty($qdata[0]['com_qes_score_1'])) ? $qdata[0]['com_qes_score_1'] / $wedata[0]['com_score_1'] : '';
		@$getweightage_2 = ( ! empty($qdata[0]['com_qes_score_2'])) ? $qdata[0]['com_qes_score_2'] / $wedata[0]['com_score_2'] : '';
		@$getweightage_3 = ( ! empty($qdata[0]['com_qes_score_3'])) ? $qdata[0]['com_qes_score_3'] / $wedata[0]['com_score_3'] : '';
		@$getweightage_4 = ( ! empty($qdata[0]['com_qes_score_4'])) ? $qdata[0]['com_qes_score_4'] / $wedata[0]['com_score_4'] : '';
		@$getweightage_5 = ( ! empty($qdata[0]['com_qes_score_5'])) ? $qdata[0]['com_qes_score_5'] / $wedata[0]['com_score_5'] : '';
		@$getweightage_6 = ( ! empty($qdata[0]['com_qes_score_6'])) ? $qdata[0]['com_qes_score_6'] / $wedata[0]['com_score_6'] : '';
		
		$weightage = 0;
		$weightage += ( ! empty($getweightage_1)) ? round($row['weightage_1'] / $getweightage_1) : 0;
		$weightage += ( ! empty($getweightage_2)) ? round($row['weightage_2'] / $getweightage_2) : 0;
		$weightage += ( ! empty($getweightage_3)) ? round($row['weightage_3'] / $getweightage_3) : 0;
		$weightage += ( ! empty($getweightage_4)) ? round($row['weightage_4'] / $getweightage_4) : 0;
		$weightage += ( ! empty($getweightage_5)) ? round($row['weightage_5'] / $getweightage_5) : 0;
		$weightage += ( ! empty($getweightage_6)) ? round($row['weightage_6'] / $getweightage_6) : 0;
		
		#-----------------JOB READINESS REPORT----------------------
		$dataPointweightage1 = array(array("y" => ( ! empty($getweightage_1) && ! empty($row['comp_col_1'])) ? round($row['weightage_1'] / $getweightage_1) : 0, "label" => ( ! empty($getweightage_1) && ! empty($row['comp_col_1'])) ? stripslashes($row['comp_col_1']) : ''),
								 	 array("y" => ( ! empty($getweightage_2) && ! empty($row['comp_col_2'])) ? round($row['weightage_2'] / $getweightage_2) : 0, "label" => ( ! empty($getweightage_2) && ! empty($row['comp_col_2'])) ? stripslashes($row['comp_col_2']) : ''),
								 	 array("y" => ( ! empty($getweightage_3) && ! empty($row['comp_col_3'])) ? round($row['weightage_3'] / $getweightage_3) : 0, "label" => ( ! empty($getweightage_3) && ! empty($row['comp_col_3'])) ? stripslashes($row['comp_col_3']) : ''),
									 array("y" => ( ! empty($getweightage_4) && ! empty($row['comp_col_4'])) ? round($row['weightage_4'] / $getweightage_4) : 0, "label" => ( ! empty($getweightage_4) && ! empty($row['comp_col_4'])) ? stripslashes($row['comp_col_4']) : ''),
								 	 array("y" => ( ! empty($getweightage_5) && ! empty($row['comp_col_5'])) ? round($row['weightage_5'] / $getweightage_5) : 0, "label" => ( ! empty($getweightage_5) && ! empty($row['comp_col_5'])) ? stripslashes($row['comp_col_5']) : ''),
									 array("y" => ( ! empty($getweightage_6) && ! empty($row['comp_col_6'])) ? round($row['weightage_6'] / $getweightage_6) : 0, "label" => ( ! empty($getweightage_6) && ! empty($row['comp_col_6'])) ? stripslashes($row['comp_col_6']) : ''));
	
		$dataPoints3 = array_values(array_filter(array_map('array_filter', $dataPointweightage1)));
		
		$dataPointweightage2 = array(array("y" => $row['weightage_1'], "label" => ( ! empty($row['weightage_1']) && ! empty($row['comp_col_1'])) ? stripslashes($row['comp_col_1']) : ''), array("y" => $row['weightage_2'], "label" => ( ! empty($row['weightage_2']) && ! empty($row['comp_col_2'])) ? stripslashes($row['comp_col_2']) : ''), array("y" => $row['weightage_3'], "label" => ( ! empty($row['weightage_3']) && ! empty($row['comp_col_3'])) ? stripslashes($row['comp_col_3']) : ''), array("y" => $row['weightage_4'], "label" => ( ! empty($row['weightage_4']) && ! empty($row['comp_col_4'])) ? stripslashes($row['comp_col_4']) : ''), array("y" => $row['weightage_5'], "label" => ( ! empty($row['weightage_5']) && ! empty($row['comp_col_5'])) ? stripslashes($row['comp_col_5']) : ''), array("y" => $row['weightage_6'], "label" => ( ! empty($row['weightage_6']) && ! empty($row['comp_col_6'])) ? stripslashes($row['comp_col_6']) : ''));
		
		$dataPoints4 = array_values(array_filter(array_map('array_filter', $dataPointweightage2)));
		
		return array('weightage'			=> $weightage,
					 'job_read_achieved'	=> array_sum(array_column($dataPoints3, 'y')),
					 'job_read_score' 		=> array_sum(array_column($dataPoints4, 'y')),
					 'total_ques_score'		=> array_sum($qdata[0]));
	}
	
	public function breadcrumbs($separator = ' &raquo; ', $home = 'Home') {
		$path = array_filter(explode('/', parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH)));
		$base = $this->getBaseUrl();
		$breadcrumbs = array("<a href=\"$base\">$home</a>");
		$last = @end(array_keys($path));
		foreach ($path as $x => $crumb):
			$title = ucwords(str_replace(array('.php', '_', '-'), array('', ' ', ' '), $crumb));
			if ($x != $last)
				$breadcrumbs[] = "<li><a href=\"$base$crumb\">$title</a></li>";
			else
				$breadcrumbs[] = "<li>$title</li>";
		endforeach;
		echo implode($separator, $breadcrumbs);
	}
	
	public function sortMultiArray($result) {
		if ( ! empty($result)):
			foreach ($result as $key => $value):
				$result[$key] = $value;
				asort($result[$key]);
			endforeach;
		endif;
		return ( ! empty($result)) ? $result : [];
	}
	
	public function matchArrayValues($a1, $a2){
		$match = TRUE;
		foreach ($a1 as $k => $v):
			if (array_values($v) != array_values($a2[$k])):
				$match = FALSE;
				break;
			endif;
		endforeach;
		return $match;
	}
	
	public function getFile() {
		return basename($_SERVER['SCRIPT_NAME']);
	}
	
	public function build_url($array){
		return http_build_query($array);
	}
	
	public function get_percentage($total, $number) {
		if ($total > 0) {
			return round($number / ($total / 100));
		} else {
			return 0;
		}
	}

	public function sizeFilter($bytes) {
		$label = array('B', 'KB', 'MB', 'GB', 'TB', 'PB');
		for ($i = 0; $bytes >= 1024 && $i < (count($label) - 1); $bytes /= 1024, $i++);
		return (round($bytes, 2) . " " . $label[$i]);
    }

	public function folderSize ($dir, $label = FALSE) {
		$size = 0;
		foreach (glob(rtrim($dir, '/').'/*', GLOB_NOSORT) as $each) {
        	$size += is_file($each) ? filesize($each) : $this->folderSize($each);
    	}
		return ($label == TRUE) ? $size : $this->sizeFilter($size);
	}

	public function getCountry($cid = NULL) {
		$sql = "SELECT * FROM countries". (($cid != NULL) ? " WHERE id = '". $cid ."'" : " ");
		$q = $this->main_conn->prepare($sql); $q->execute();
		if ($q->rowCount() > 0):
			while ($row = $q->fetch(PDO::FETCH_ASSOC)):
				$data[] = $row;
			endwhile;
		endif;
		return ( ! empty($data)) ? $data : array(array('id' => 0, 'Country' => 'Country not found'));
	}
	
	public function getState($cid = NULL, $sid = NULL) {
		$sql = "SELECT * FROM states". (($cid != NULL) ? " WHERE country_id = '". $cid ."'" : " ") . (($sid != NULL) ? " WHERE id = '". $sid ."'" : " ");
		$q = $this->main_conn->prepare($sql); $q->execute();
		if ($q->rowCount() > 0):
			while ($row = $q->fetch(PDO::FETCH_ASSOC)):
				$data[] = $row;
			endwhile;
		endif;
		return ( ! empty($data)) ? $data : array(array('id' => 0, 'name' => 'State not found'));
	}
	
	public function getCity($state_id = NULL, $cid = NULL) {
		$sql = "SELECT * FROM cities WHERE 1=1 ".(($state_id != NULL) ? " AND state_id = '". $state_id ."'" : "") . (($cid != NULL) ? " AND id = '". $cid ."'" : "");
		$q = $this->main_conn->prepare($sql); $q->execute();
		if ($q->rowCount() > 0):
			while ($row = $q->fetch(PDO::FETCH_ASSOC)):
				$data[] = $row;
			endwhile;
		endif;
		return ( ! empty($data)) ? $data : array(array('id' => 0, 'name' => 'City not found'));
	}

	public function getClient($cid = NULL) {
		$sql = "SELECT c.*, p.* FROM 
				client_tbl AS c LEFT JOIN 
				client_permissions AS p ON c.client_id = p.client_id 
				".(($cid != NULL) ? " WHERE c.client_id = '". $cid ."'" : "");
		$q = $this->main_conn->prepare($sql); $q->execute();
		if ($q->rowCount() > 0):
			while ($row = $q->fetch(PDO::FETCH_ASSOC)):
				$data[] = $row;
			endwhile;
		endif;
		return ($cid != NULL) ? $data[0] : $data;
	}

	/**
     * Get Total Clients.
     *
     * @return default Total no of Clients and SubClients
     * @return Total no of Clients when Pass int 1
     * @return Total no of SubClients when Pass int 2
     */
	public function getTotalClient($type = NULL) {
		$cond = '';
		if ($type != NULL && $type == 1):
			$cond = "AND type = 'client'";
		elseif ($type != NULL && $type == 2):
			$cond = "AND type = 'subclient'";
		endif;
		$sql 	= "SELECT client_id FROM client_tbl WHERE type != 'super_admin' $cond";
		$q 		= $this->main_conn->prepare($sql); $q->execute();
		$count	= $q->rowCount();
		return ($count > 0) ? $count : 0;
	}

	public function getTotalSubClient($client_id) {
		$sql 	= "SELECT c.client_id FROM client_tbl AS c LEFT JOIN mapping_tbl AS m ON c.client_id = m.client_id WHERE m.client_id = '". $client_id ."'";
		$q 	 	= $this->main_conn->prepare($sql); $q->execute();
		$count	= $q->rowCount();
		return ($count > 0) ? $count : 0;
	}

	public function getClientData($columns, $tbl_name, $condi = NULL) {
		$sql = "SELECT $columns FROM $tbl_name";
		if ($condi != NULL):
			$sql .= $condi;
		endif;
		$q = $this->main_conn->prepare($sql); $q->execute();
		if ($q->rowCount() > 0):
			while ($row = $q->fetch(PDO::FETCH_ASSOC)):
				$data[] = $row;
			endwhile;
		endif;
		return ( ! empty($data)) ? $data : [];
	}

	public function getClientPermission($client_id)
	{
		$sql = "SELECT *, (admin + auth + reviewer + learner + educator) AS totalUsers FROM client_permissions WHERE client_id = '". $client_id ."'";
		$q = $this->main_conn->prepare($sql); $q->execute();
		if ($q->rowCount() > 0):
			$data = $q->fetch(PDO::FETCH_ASSOC);
		else:
			$data = [];
		endif;
		return $data;
	}

	public function domainExist($dname)
	{
		$sql = "SELECT domain_name FROM client_tbl WHERE domain_name = '". $dname ."'";
		$q	 = $this->main_conn->prepare($sql); $q->execute();
		return ($q->rowCount() > 0) ? TRUE : FALSE;
	}

	public function webBranding($cid = NULL) {
		$condi = $res = '';
		if ($cid != NULL):
			$condi = " WHERE client_id = '". $cid ."'";
		else:
			$host  = $_SERVER['HTTP_HOST'];
			$cond  = " WHERE domain_name = '". $host ."'";
			$cdata = $this->getClientData('client_id', 'client_tbl', $cond);
			$condi = " WHERE client_id = '". $cdata[0]['client_id'] ."'";
		endif;
		$sql = "SELECT * FROM client_branding" . $condi;
		$q = $this->main_conn->prepare($sql); $q->execute();
		if ($q->rowCount() > 0):
			$data = $q->fetch(PDO::FETCH_ASSOC);
		else:
			$data = [];
		endif;
		if ( ! empty($data) && $cid == NULL):
			$res .= '<style type="text/css">';
			if ( ! empty($data['font_type'])):
				$res .= "@import url('https://fonts.googleapis.com/css2?family=". $data['font_type'] .":wght@100;200;300;400;500;600;700;800;900&display=swap');";
				$type = str_replace('+', ' ', $data['font_type']);
				$res .= ' body { font-family: '. $type .' !important; } ';
				$res .= ' h1,h2,h3,h4,h5,h6,p { font-family: '. $type .' !important; } ';
			endif;
			if ( ! empty($data['font_color'])):
				$res .= ' body { color: '. $data['font_color'] .' !important; } ';
			endif;
			if ( ! empty($data['font_size'])):
				$res .= ' body { font-size: '. $data['font_size'] .' !important; } ';
			endif;
			if ( ! empty($data['btn_color'])):
				$res .= ' .btn.btn-primary { background-color:' . $data['btn_color'] .'; border-color: '. $data['btn_color'] .'; !important; } ';
				$res .= ' .header { border-bottom: 3px solid ' . $data['btn_color'] .' !important; } ';
				$res .= ' .match-Tab .nav-tabs>li.active>a:focus:after, .match-Tab .nav-tabs>li.active>a:hover:after, .match-Tab .nav-tabs>li.active> { background: '. $data['btn_color'] .' !important; } ';				
				$res .= ' .match-Tab li.active>a:after { background: '. $data['btn_color'] .' !important; } ';
				$res .= ' .form-group.QusScore .form-control { background: '. $data['btn_color'] .' !important; } ';
				$res .= ' .toggleIcon:hover span { background: '. $data['btn_color'] .' !important; } ';
				$res .= ' .regsterbtn { background: '. $data['btn_color'] .' !important; } ';
				$res .= ' .svg path { fill:' . $data['btn_color'] .' !important; }';
				$res .= ' .svg polygon { fill:' . $data['btn_color'] .' !important; }';
				$res .= ' .svg rect { fill:' . $data['btn_color'] .' !important; }';
				$res .= ' .svg line { fill:' . $data['btn_color'] .' !important; }';
				$res .= ' .browsebtn.browsebtntmp i { color:' . $data['btn_color'] .' !important; }';
				$res .= ' .admin-dashboard.Ev2.launchSce li.active>a:after, .modal-dialog.modal-lg.ViewSEnArIo li.active>a:after { background:' . $data['btn_color'] .' !important; }';
				$res .= ' .admin-dashboard.Ev2 ul.nav.nav-tabs li.active>a:after { background:' . $data['btn_color'] .' !important; }';				
			endif;
			if ( ! empty($data['btn_hcolor'])):
				$res .= ' .btn.btn-primary:hover, .btn.btn-primary:focus { background-color:' . $data['btn_hcolor'] .'; border-color: '. $data['btn_hcolor'] .'; !important; }';
				$res .= ' .dashboard-box:hover .width70 rect { fill:' . $data['btn_hcolor'] .' !important; }';
				$res .= ' .dashboard-box:hover .width70 path { fill:' . $data['btn_hcolor'] .' !important; }';
				$res .= ' li.matchingQ.active svg path, li.matchingQ svg path:hover, li.SequanceQ.active svg path, li.SequanceQ svg path:hover, li.DregDropQ.active svg path, li.DregDropQ svg path:hover, li.VideoQ.active svg path, li.VideoQ svg path:hover, li.MCQQ.active svg path, li.MCQQ svg path:hover, li.MMCQQ.active svg path, li.MMCQQ svg path:hover, li.SwapingQ.active svg path, li.SwapingQ svg path:hover { fill: ' . $data['btn_hcolor'] .' !important; }';
				$res .= ' .info-circle  i:focus, i:hover { color: ' . $data['btn_hcolor'] .' !important; }';
				$res .= ' .svg:hover path { fill:' . $data['btn_hcolor'] .' !important; }';
				$res .= ' .svg:hover polygon { fill:' . $data['btn_hcolor'] .' !important; }';
				$res .= ' .svg:hover rect { fill:' . $data['btn_hcolor'] .' !important; }';
				$res .= ' .svg:hover line { fill:' . $data['btn_hcolor'] .' !important; }';
				$res .= ' .close:focus, .close:hover { color:' . $data['btn_hcolor'] .' !important; }';
			endif;
			$res .= '</style>';
		else:
			$res = $data;
		endif;
		return ( ! empty($res)) ? $res : [];
	}
	
	public function getUsedHostingData($client_id = NULL){
		if ($client_id != NULL):
			$cdata = $this->getClientData('c.domain_name, c.hosting_space', 'mapping_tbl AS m LEFT JOIN client_tbl AS c ON m.sub_client_id = c.client_id', " WHERE m.client_id = '". $client_id ."'");
		else:
		 	$cdata = $this->getClientData('domain_name, hosting_space', 'client_tbl', " WHERE type != 'super_admin'");
		endif;
		if ( ! empty($cdata)):
			$total_size = 0;
			foreach ($cdata as $data):
				$domain_name	 = $data['domain_name'];
				$folderSize 	 = $this->folderSize('scenario/upload/'. $domain_name, TRUE);
				$folderSizeLable = $this->folderSize('scenario/upload/'. $domain_name);
				$total_size		 += $folderSize;
				$hspace_data[]	 = array('y' => $folderSize, 'legendText' => $domain_name . ': ' . $folderSizeLable .'/'. $data['hosting_space'] .' MB');
			endforeach;
			return array('total_size' => $this->sizeFilter($total_size), 'dataPoints' => $hspace_data);
		 else:
			return array('total_size' => 0 .'B', 'dataPoints' => array('y' => 0, 'legendText' => ''));
		 endif;
	 }

	 public function getClientMapData($sub_client_id){
		$sql = "SELECT c.domain_name, c.organization FROM mapping_tbl AS m LEFT JOIN client_tbl AS c ON m.client_id = c.client_id WHERE m.sub_client_id = '". $sub_client_id ."'";
		$q	 = $this->main_conn->prepare($sql); $q->execute();
		if ($q->rowCount() > 0):
			$data = $q->fetch(PDO::FETCH_ASSOC);
		else:
			$data = [];
		endif;
		return $data;
	 }

	 public function checkProctorPermission($client_id)
	{
		if ( ! empty($client_id)):
			$sql = "SELECT proctor FROM client_permissions WHERE client_id = '". $client_id ."'";
			$q   = $this->main_conn->prepare($sql); $q->execute();
			$row = $q->fetch(PDO::FETCH_ASSOC);
			$res = ( ! empty($row['proctor'])) ? TRUE : FALSE;
		else:
			$res = FALSE;
		endif;
		return $res;
	}

	 public function getCMSPage($pid){
		$sql = "SELECT * FROM cms_page_tbl WHERE cms_page_id = '". $pid ."'";
		$q 	 = $this->prepare($sql); $q->execute();
		if ($q->rowCount() > 0):
			$data = $q->fetch(PDO::FETCH_ASSOC);
		else:
			$data = [];
		endif;
		return $data;
	 }
	 
	 public function checkCriticalClassicTemp($sim_id, $uid){
		$sql = "SELECT (q.ques_val_1 + q.ques_val_2 + q.ques_val_3 + q.ques_val_4 + q.ques_val_5 + q.ques_val_6) as totalQuesval, 
				(ans.ans_val1 + ans.ans_val2 + ans.ans_val3 + ans.ans_val4 + ans.ans_val5 + ans.ans_val6) as totalAnsval FROM 
				score_tbl sc LEFT JOIN 
				question_tbl q ON sc.question_id = q.question_id LEFT JOIN 
				answer_tbl ans ON sc.choice_option_id = ans.answer_id WHERE 
				sc.scenario_id = '". $sim_id ."' AND sc.uid = '". $uid ."' AND q.critical = 1";
		$q = $this->prepare($sql); $q->execute();
		if ($q->rowCount() > 0):
			foreach ($q->fetchAll(PDO::FETCH_ASSOC) as $row):
				if ($row['totalQuesval'] == $row['totalAnsval']):
					$res = TRUE;
				else: 
					$res = FALSE;
					break;
				endif;
			endforeach;
			return $res;
		else:
			return 2;
		endif;
	}

	public function checkCriticalMultiTemp($sim_id, $uid){
		$sql = "SELECT sc.status FROM 
				multi_score_tbl sc LEFT JOIN 
				question_tbl q ON sc.question_id = q.question_id WHERE 
				sc.scenario_id = '". $sim_id ."' AND sc.uid = '". $uid ."' AND q.critical = 1";
		$q = $this->prepare($sql); $q->execute();
		if ($q->rowCount() > 0):
			foreach ($q->fetchAll(PDO::FETCH_ASSOC) as $row):
				if ($row['status'] == 1):
					$res = TRUE;
				else:
					$res = FALSE;
					break;
				endif;
			endforeach;
			return $res;
		else:
			return 2;
		endif;
	}

	public function checkCriticalOpenRes($sim_id, $uid){
		$sql = "SELECT (q.ques_val_1 + q.ques_val_2 + q.ques_val_3 + q.ques_val_4 + q.ques_val_5 + q.ques_val_6) as totalQuesval, 
				(sc.ans_val1 + sc.ans_val2 + sc.ans_val3 + sc.ans_val4 + sc.ans_val5 + sc.ans_val6) as totalAnsval FROM 
				open_res_tbl sc LEFT JOIN 
				question_tbl q ON sc.question_id = q.question_id WHERE 
				sc.scenario_id = '". $sim_id ."' AND sc.uid = '". $uid ."' AND q.critical = 1";
		$q = $this->prepare($sql); $q->execute();
		if ($q->rowCount() > 0):
			foreach ($q->fetchAll(PDO::FETCH_ASSOC) as $row):
				if ($row['totalQuesval'] == $row['totalAnsval']):
					$res = TRUE;
				else:
					$res = FALSE;
					break;
				endif;
			endforeach;
			return $res;
		else:
			return 2;
		endif;
	}

	public function GetFilterUsers($id, $column, $orderby)
	{
		$sql = "SELECT $column FROM users WHERE $orderby != '' AND id IN ($id) GROUP BY $orderby ORDER BY $orderby";
		$q = $this->prepare($sql); $q->execute();
		if ($q->rowCount() > 0):
			foreach ($q->fetchAll(PDO::FETCH_ASSOC) as $row):
				$data[] = array('label' => $row[$column], 'title' => $row[$column], 'value' => $row[$column]);
			endforeach;
			return $data;
		endif;
	}
	
	public function GetUsersByFilter($ids, $column, $cond)
	{
		$sql = "SELECT $column FROM users WHERE id IN ($ids) AND $cond";
		$q = $this->prepare($sql); $q->execute();
		if ($q->rowCount() > 0):
			foreach ($q->fetchAll(PDO::FETCH_ASSOC) as $row):
				$data[] = $row[$column];
			endforeach;
			return $data;
		endif;
	}

	public function rootPath() {
		if ($_SERVER['HTTP_HOST'] == '127.0.0.1' || $_SERVER['HTTP_HOST'] == 'localhost'):
			$path = $_SERVER['DOCUMENT_ROOT'] . rtrim(dirname($_SERVER['PHP_SELF']), '/\\') . '/';
		else:
			$path = $_SERVER['DOCUMENT_ROOT'] . '/';
		endif;
		return $path;
	}

	public function getLastAttemptMultiSIM($sim_id, $uid, $limit = 1, $fdate = NULL, $tdate = NULL) {
		$sql = "SELECT DISTINCT st.aid, st.uid, st.userid, DATE_FORMAT(st.attempt_date, '%e-%b-%y %l:%i %p') AS atdate, s.scenario_id, s.Scenario_title AS sim_title, s.duration, s.passing_marks, s.scenario_category, s.sim_temp_type, s.status FROM 
				scenario_attempt_tbl AS st LEFT JOIN 
				scenario_master AS s ON s.scenario_id = st.scenario_id WHERE st.scenario_id = '". $sim_id ."' AND st.userid = '". $uid ."' AND (st.uid IN (SELECT uid FROM score_tbl) OR st.uid IN (SELECT uid FROM multi_score_tbl) OR st.uid IN (SELECT uid FROM open_res_tbl))";
		if ( ! empty($tdate) && ! empty($fdate)):
			$sql .= " AND DATE(attempt_date) BETWEEN '". $fdate ."' AND '". $tdate ."'";
		elseif ( ! empty($tdate) && empty($fdate)):
			$sql .= " AND DATE(attempt_date) = '". $tdate ."'";
		elseif ( ! empty($fdate) && empty($tdate)):
			$sql .= " AND DATE(attempt_date) = '". $fdate ."'";
		endif;
		$sql .= " ORDER BY st.aid ASC LIMIT $limit";
		$q	  = $this->prepare($sql); $q->execute();
		if ($q->rowCount() > 0):
			foreach ($q->fetchAll(PDO::FETCH_ASSOC) as $row):
				$data[] = $row;
			endforeach;
		else:
			$data = [];
		endif;
		return $data;
	}

	public function analyticsReport($sim_id, $uid, $limit, $fdate = NULL, $tdate = NULL)
	{
		$from_date = $fdate; $to_date = $tdate; $res = '';
		$data = $this->getLastAttemptMultiSIM($sim_id, $uid, $limit, $from_date, $to_date);
		$i	  =	1;
		foreach ($data as $value):
			$sim_temp_type = $value['sim_temp_type'];
			$scenario_id   = $value['scenario_id'];
			$attempt_uid   = $value['uid'];
			$learner_id	   = $value['userid'];
			
			#---------INDIVIUAL COMPETENCY----------------
			$sqlcom  = "SELECT (SUM(comp_val_1) + SUM(comp_val_2) + SUM(comp_val_3) + SUM(comp_val_4) + SUM(comp_val_5) + SUM(comp_val_6)) AS totalCompval FROM competency_tbl WHERE scenario_id = '". $scenario_id ."'";
			$comexc  = $this->prepare($sqlcom); $comexc->execute();
			$comp 	 = $comexc->fetch(PDO::FETCH_ASSOC);
			$comval	 = $comp['totalCompval'];
			if ($sim_temp_type == 2):
				$sqlans  = "SELECT (SUM(q.ques_val_1) + SUM(q.ques_val_2) + SUM(q.ques_val_3) + SUM(q.ques_val_4) + SUM(q.ques_val_5) + SUM(q.ques_val_6)) AS totalAnsval FROM 
							multi_score_tbl sc LEFT JOIN question_tbl q ON sc.question_id = q.question_id WHERE 
							sc.status = 1 AND sc.scenario_id = '". $scenario_id ."' AND sc.uid = '". $attempt_uid ."'";
			elseif ($sim_temp_type == 7):
				$sqlans  = "SELECT (SUM(ans_val1) + SUM(ans_val2) + SUM(ans_val3) + SUM(ans_val4) + SUM(ans_val5) + SUM(ans_val6)) AS totalAnsval FROM 
							open_res_tbl WHERE scenario_id = '". $scenario_id ."' AND uid = '". $attempt_uid ."'";
			elseif ($sim_temp_type == 1 || $sim_temp_type == 8):
				$sqlans  = "SELECT (SUM(a.ans_val1) + SUM(a.ans_val2) + SUM(a.ans_val3) + SUM(a.ans_val4) + SUM(a.ans_val5) + SUM(a.ans_val6)) AS totalAnsval FROM 
							score_tbl sc LEFT JOIN answer_tbl a ON sc.choice_option_id = a.answer_id WHERE 
							sc.scenario_id = '". $scenario_id ."' AND sc.uid = '". $attempt_uid ."'";
			endif;
			$ansexc  = $this->prepare($sqlans); $ansexc->execute();
			$ans 	 = $ansexc->fetch(PDO::FETCH_ASSOC);
			$ansval	 = $ans['totalAnsval'];
			$score	 = $this->get_percentage($comval, $ansval);
			$pmarks	 = ($value['passing_marks'] > 0) ? $value['passing_marks'] : 0;
			if ($sim_temp_type == 1 || $sim_temp_type == 8):
				if ($this->checkCriticalClassicTemp($scenario_id, $attempt_uid) == TRUE || $this->checkCriticalClassicTemp($scenario_id, $attempt_uid) == 2):
					$label = ($score > 0 && $score >= $pmarks) ? 'P' : 'RR';
					$class = ($score > 0 && $score >= $pmarks) ? 'LEARGreen' : 'LEARRed';
				else:
					$label = 'RR';
					$class = 'LEARRed';
				endif;
				$totalTime = $this->getLastTotalTime($scenario_id, $learner_id, $attempt_uid);
			elseif ($sim_temp_type == 7):
				if ($this->checkCriticalOpenRes($scenario_id, $attempt_uid) == TRUE || $this->checkCriticalOpenRes($scenario_id, $attempt_uid) == 2):
					$label = ($score > 0 && $score >= $pmarks) ? 'P' : 'RR';
					$class = ($score > 0 && $score >= $pmarks) ? 'LEARGreen' : 'LEARRed';
				else:
					$label = 'RR';
					$class = 'LEARRed';
				endif;
				$totalTime = $this->getOpenResLastTotalTime($scenario_id, $learner_id, $attempt_uid);
			elseif ($sim_temp_type == 2):
				if ($this->checkCriticalMultiTemp($scenario_id, $attempt_uid) == TRUE || $this->checkCriticalMultiTemp($scenario_id, $attempt_uid) == 2):
					$label = ($score > 0 && $score >= $pmarks) ? 'P' : 'RR';
					$class = ($score > 0 && $score >= $pmarks) ? 'LEARGreen' : 'LEARRed';
				else:
					$label = 'RR';
					$class = 'LEARRed';
				endif;
				$totalTime = $this->getLastTotalTimeMulti($scenario_id, $learner_id, $attempt_uid);
			endif;
			$res 	.= "<ul>
							<li class='LEAR36 LEARblue'>A $i</li>
							<li class='LEAR36'> $score% </li>
							<li class='LEAR36 $class'> $label </li>
						</ul>
						<div class='AReptDate'>". $value['atdate'] ." | ". $totalTime['timeSum'] ."</div>";
		$i++; endforeach;
		return $res;
	}

	public function educatorReportPage($type)
	{
		$url = '';
		switch ($type) {
			case 1:
				$url = 'learner-progress-educator.php';
				break;
			case 2:
				$url = 'learner-progress-linear-multiple-educator.php';
				break;
			case 7:
				$url = 'open-response-learner-progress-educator.php';
				break;
			case 8:
				$url = 'learner-progress-single-video-educator.php';
				break;
			default:
				return '#';
		}
		return $url;
	}

	public function GetPercentageMissedQues($temp_type, $getLastAttempt, $question_id, $ques_column_name, $ans_column_name, $get_total_learner)
	{
		$fail = $nf = 0;
		$user_data  = [];
		foreach ($getLastAttempt as $lastData):
			if ($temp_type == 1 || $temp_type == 8):
				$sql = "SELECT q.$ques_column_name AS totalQuesval, a.$ans_column_name AS totalAnsval FROM score_tbl sc LEFT JOIN question_tbl q ON sc.question_id = q.question_id LEFT JOIN answer_tbl a ON sc.choice_option_id = a.answer_id WHERE sc.scenario_id = '". $lastData['scenario_id'] ."' AND sc.question_id = '". $question_id ."' AND sc.uid = '". $lastData['uid'] ."'";
			elseif ($temp_type == 2):
				$sql = "SELECT q.$ques_column_name AS totalQuesval, q.$ques_column_name AS totalAnsval FROM multi_score_tbl sc LEFT JOIN question_tbl q ON sc.question_id = q.question_id WHERE sc.status = 1 AND sc.scenario_id = '". $lastData['scenario_id'] ."' AND sc.question_id = '". $question_id ."' AND sc.uid = '". $lastData['uid'] ."'";
			elseif ($temp_type == 7):
				$sql = "SELECT q.$ques_column_name AS totalQuesval, a.$ans_column_name AS totalAnsval FROM question_tbl q LEFT JOIN open_res_tbl a ON q.question_id = a.question_id WHERE a.scenario_id = '". $lastData['scenario_id'] ."' AND a.question_id = '". $question_id ."' AND a.uid = '". $lastData['uid'] ."'";
			endif;
			$q   = $this->prepare($sql); $q->execute();
			$row = $q->fetch(PDO::FETCH_ASSOC);
			$totalQuesval = $row['totalQuesval'];
			$totalAnsval  = $row['totalAnsval'];
			if (empty($totalQuesval) || $totalQuesval != $totalAnsval):
				$fail++;
				$user = $this->getAssigneeName($lastData['userid']);
				$user_data[] = array('username' => $user['username'], 'full_name' => $user['fullname'], 'location' => $user['location'], 'company' => $user['company'], 'department' => $user['department'], 'designation' => $user['designation']);
			endif;
		endforeach;
		$nf += $fail;
		$percentage = ( ! empty($get_total_learner) && ! empty($fail)) ? $this->get_percentage($get_total_learner, $nf) : 0;
		return array('percentage' => $percentage, 'user_data' => $user_data);
	}

	public function getTotalAttemptedSim($user_id) {
		$sim_id = $this->getAssignSimData($user_id);
		if ( ! empty($sim_id)):
			$sql  = "SELECT DISTINCT st.* FROM scenario_attempt_tbl AS st LEFT JOIN scenario_master AS s ON s.scenario_id = st.scenario_id WHERE ";
			$sql .= "st.scenario_id IN ($sim_id) AND ";
			$sql .= "st.userid = '". $user_id ."' AND (st.uid IN (SELECT uid FROM score_tbl) OR st.uid IN (SELECT uid FROM multi_score_tbl) OR st.uid IN (SELECT uid FROM open_res_tbl)) GROUP BY st.scenario_id";
			$q	 = $this->prepare($sql); $q->execute();
			if ($q->rowCount() > 0):
				foreach ($q->fetchAll(PDO::FETCH_ASSOC) as $row):
					$data[] = $row['aid'];
				endforeach;
				return $data;
			else:
				$data = [];
			endif;
		else:
			$data = [];
		endif;
		return $data;
	}

	public function getPublishPage($sim_type, $temp_type){
		$url = '';
		if ($sim_type == 1):
			if ($temp_type == 1):
				$url = 'launch-sim-linear-classic-publish.php';
			elseif ($temp_type == 2):
				$url = 'launch-sim-linear-multiple-publish.php';
			endif;
		endif;
		return $url;
	}

	public function isAttempted($sid, $uid){
		$sql = "SELECT aid FROM scenario_attempt_tbl WHERE scenario_id = '". $sid ."' AND userid = '". $uid ."' AND (uid IN (SELECT uid FROM score_tbl) OR uid IN (SELECT uid FROM multi_score_tbl) OR uid IN (SELECT uid FROM open_res_tbl))";
		$q	 = $this->prepare($sql); $q->execute();
		return ($q->rowCount() > 0) ? TRUE : FALSE;
	}

	public function getSim($columns, $sid)
	{
		$sql = "SELECT $columns FROM scenario_master WHERE scenario_id IN ($sid)";
		$q	 = $this->prepare($sql); $q->execute();
		if ($q->rowCount() > 0):
			foreach ($q->fetchAll(PDO::FETCH_ASSOC) as $row):
				$data[] = $row;
			endforeach;
			return $data;
		else:
			return [];
		endif;
	}

	public function learnerReport($sim_id, $uid, $limit, $fdate = NULL, $tdate = NULL)
	{
		$from_date	= $fdate; $to_date = $tdate;
		$data 		= $this->getLastAttemptMultiSIM($sim_id, $uid, $limit, $from_date, $to_date);
		$rdata		= [];
		foreach ($data as $value):
			$sim_temp_type = $value['sim_temp_type'];
			$scenario_id   = $value['scenario_id'];
			$attempt_uid   = $value['uid'];
			$learner_id	   = $value['userid'];
			
			#---------INDIVIUAL COMPETENCY----------------
			$sqlcom  = "SELECT (SUM(comp_val_1) + SUM(comp_val_2) + SUM(comp_val_3) + SUM(comp_val_4) + SUM(comp_val_5) + SUM(comp_val_6)) AS totalCompval FROM competency_tbl WHERE scenario_id = '". $scenario_id ."'";
			$comexc  = $this->prepare($sqlcom); $comexc->execute();
			$comp 	 = $comexc->fetch(PDO::FETCH_ASSOC);
			$comval	 = $comp['totalCompval'];
			if ($sim_temp_type == 2):
				$sqlans  = "SELECT (SUM(q.ques_val_1) + SUM(q.ques_val_2) + SUM(q.ques_val_3) + SUM(q.ques_val_4) + SUM(q.ques_val_5) + SUM(q.ques_val_6)) AS totalAnsval FROM 
							multi_score_tbl sc LEFT JOIN question_tbl q ON sc.question_id = q.question_id WHERE 
							sc.status = 1 AND sc.scenario_id = '". $scenario_id ."' AND sc.uid = '". $attempt_uid ."'";
			elseif ($sim_temp_type == 7):
				$sqlans  = "SELECT (SUM(ans_val1) + SUM(ans_val2) + SUM(ans_val3) + SUM(ans_val4) + SUM(ans_val5) + SUM(ans_val6)) AS totalAnsval FROM 
							open_res_tbl WHERE scenario_id = '". $scenario_id ."' AND uid = '". $attempt_uid ."'";
			elseif ($sim_temp_type == 1 || $sim_temp_type == 8):
				$sqlans  = "SELECT (SUM(a.ans_val1) + SUM(a.ans_val2) + SUM(a.ans_val3) + SUM(a.ans_val4) + SUM(a.ans_val5) + SUM(a.ans_val6)) AS totalAnsval FROM 
							score_tbl sc LEFT JOIN answer_tbl a ON sc.choice_option_id = a.answer_id WHERE 
							sc.scenario_id = '". $scenario_id ."' AND sc.uid = '". $attempt_uid ."'";
			endif;
			$ansexc  = $this->prepare($sqlans); $ansexc->execute();
			$ans 	 = $ansexc->fetch(PDO::FETCH_ASSOC);
			$ansval	 = $ans['totalAnsval'];
			$score	 = $this->get_percentage($comval, $ansval);
			$pmarks	 = ($value['passing_marks'] > 0) ? $value['passing_marks'] : 0;
			if ($sim_temp_type == 1 || $sim_temp_type == 8):
				if ($this->checkCriticalClassicTemp($scenario_id, $attempt_uid) == TRUE || $this->checkCriticalClassicTemp($scenario_id, $attempt_uid) == 2):
					$label = ($score > 0 && $score >= $pmarks) ? 'P' : 'RR';
				else:
					$label = 'RR';
				endif;
				$totalTime = $this->getLastTotalTime($scenario_id, $learner_id, $attempt_uid);
			elseif ($sim_temp_type == 7):
				if ($this->checkCriticalOpenRes($scenario_id, $attempt_uid) == TRUE || $this->checkCriticalOpenRes($scenario_id, $attempt_uid) == 2):
					$label = ($score > 0 && $score >= $pmarks) ? 'P' : 'RR';
				else:
					$label = 'RR';
				endif;
				$totalTime = $this->getOpenResLastTotalTime($scenario_id, $learner_id, $attempt_uid);
			elseif ($sim_temp_type == 2):
				if ($this->checkCriticalMultiTemp($scenario_id, $attempt_uid) == TRUE || $this->checkCriticalMultiTemp($scenario_id, $attempt_uid) == 2):
					$label = ($score > 0 && $score >= $pmarks) ? 'P' : 'RR';
				else:
					$label = 'RR';
				endif;
				$totalTime = $this->getLastTotalTimeMulti($scenario_id, $learner_id, $attempt_uid);
			endif;
			$rdata[] = [
						'lable'	=> $label,
						'score'	=> $score.'%',
						'date'	=> $value['atdate'],
						'time'	=> $totalTime['timeSum']
					];
		endforeach;
		return $rdata;
	}
}
