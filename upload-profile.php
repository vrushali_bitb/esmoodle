<?php 
require_once 'config/src/class.upload.php';
ob_start();
$uploadpath = 'img/profile/';
if ( ! is_dir($uploadpath)) mkdir($uploadpath);
$valid_exts = ['jpg'];
if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_FILES)):
	$ad_img = (isset($_FILES['file'])) ? $_FILES['file'] : '';
	$error  = FALSE;
	if ( ! empty($ad_img) && ! in_array(strtolower(pathinfo($ad_img['name'], PATHINFO_EXTENSION)), $valid_exts)):
		$resdata = array('success' => FALSE, 'msg' => 'Please select valid file. accept only jpg');
		$error	 = TRUE;
	elseif ($error == FALSE && ! empty($ad_img) && $ad_img['size'] > 5242880):
		$resdata = array('success' => FALSE, 'msg' => 'File size must be less than 5 MB.!');
		$error   = TRUE;
	elseif ($error == FALSE):
		$handle = new Upload($ad_img);
		if ($handle->uploaded) {
			$handle->image_resize            = TRUE;
			$handle->image_x                 = 670;
			$handle->image_y                 = 837;
			$handle->image_ratio			 = TRUE;
			$handle->file_safe_name 		 = TRUE;
			$handle->file_overwrite 		 = FALSE;
			$handle->file_auto_rename		 = TRUE;
			$handle->Process($uploadpath);
			if ($handle->processed) {
				$ad_img_name = $handle->file_dst_name;
				#----------Profile-Thumb-1---------
				$handle->image_resize            = TRUE;
				$handle->file_name_body_pre 	 = 'thumb_';
				$handle->image_x                 = 50;
				$handle->image_y                 = 50;
				$handle->image_ratio			 = TRUE;
				$handle->image_ratio_crop     	 = TRUE;
				$handle->image_ratio_fill 		 = FALSE;
				$handle->file_safe_name 		 = TRUE;
				$handle->file_overwrite 		 = FALSE;
				$handle->file_auto_rename		 = TRUE;
				$handle->Process($uploadpath);
				$handle->processed;
				$ad_img_name1 = $handle->file_dst_name;
				#----------Profile-Thumb-2---------
				$handle->image_resize            = TRUE;
				$handle->file_name_body_pre 	 = 'thumb_125_';
				$handle->image_x                 = 125;
				$handle->image_y                 = 125;
				$handle->image_ratio			 = TRUE;
				$handle->image_ratio_crop     	 = TRUE;
				$handle->image_ratio_fill 		 = FALSE;
				$handle->file_safe_name 		 = TRUE;
				$handle->file_overwrite 		 = FALSE;
				$handle->file_auto_rename		 = TRUE;
				$handle->Process($uploadpath);
				$handle->processed;
				$ad_img_name2 = $handle->file_dst_name;
			}
			$handle->Clean();
			$resdata = ( ! empty($ad_img_name)) ? array('success' => TRUE, 'img_name' => $ad_img_name, 'img_name1' => $ad_img_name1, 'img_name2' => $ad_img_name2) : array('success' => FALSE, 'msg' => 'Error image not upload. try again later.!');
		}
	endif;
	echo json_encode($resdata);
else:
	$resdata = array('success' => FALSE, 'msg' => 'Error image not upload. try again later.!');
	echo json_encode($resdata);
endif;

ob_flush();
