<?php 
ob_start();
session_start();
if (isset($_SESSION['username'])) {
  $user     = $_SESSION['username'];
  $role     = $_SESSION['role'];
  $userid   = $_SESSION['userId'];
  $cid		= (isset($_SESSION['client_id'])) ? $_SESSION['client_id'] : exit('Oops. something went wrong please try again');
  $domain   = (isset($_SESSION['domain_name'])) ? $_SESSION['domain_name'] : $_SERVER['HTTP_HOST'];
} else {
  header('location: index.php');
}
require_once 'config/db.class.php';
require_once 'includes/header.php';
$db         = new DBConnection();
$sim_id		= (isset($_GET['review']) && ! empty($_GET['sim_id'])) ? $_GET['sim_id'] : '';
$sim_data   = $db->getScenario($sim_id);
$user_data 	= $db->getUsers(md5($userid));
$path		= 'scenario/upload/'.$domain.'/';
$noimg		= 'scenario/upload/noimage.png';
if ( ! empty($sim_data['sim_cover_img']) && file_exists($path . $sim_data['sim_cover_img'])):
    $assetsUrl = $path . $sim_data['sim_cover_img'];
else:
    $assetsUrl = $noimg;
endif; ?>
<input type="hidden" id="url" value="<?php echo base64_encode($db->getBaseUrl($db->getFile() .'?review=true&sim_id='. $sim_id)); ?>" />
<input type="hidden" id="scenario_id" value="<?php echo $sim_data['scenario_id'] ?>" />
<input type="hidden" id="user_id" value="<?php echo $userid ?>" />
<style>
	body{
		overflow:hidden;
	}
</style>
<div class="rev-top-banner">
	<div class="rev-topLbanner">
        <a data-fancybox data-caption="<?php echo $sim_data['Scenario_title']; ?>" href="<?php echo $assetsUrl; ?>"><img class="img-fluid" src="<?php echo $assetsUrl; ?>"></a>
		<div class="revTopText">
			<h3 class="title" title="<?php echo $sim_data['Scenario_title']; ?>"><?php echo $db->truncateText($sim_data['Scenario_title'], 50); ?></h3>
			<div class="rev-auth">
				<p>Author:</p>
				<?php if ( ! empty($sim_data['assign_author'])): ?>
				<a data-toggle="popover" title="View Author" href="<?php echo $db->getBaseUrl('includes/ajax.php?getTooltipData=true&ids='. $sim_data['assign_author']); ?>" onclick="return false"><i class="fa fa-info-circle fa-2x" aria-hidden="true"></i></a>
				<?php endif; ?>
			</div>
			<div class="rev-review">
				<p>Reviewer:</p>
				<?php if ( ! empty($sim_data['assign_reviewer'])): ?>
				<a data-toggle="popover" title="View Reviewer" href="<?php echo $db->getBaseUrl('includes/ajax.php?getTooltipData=true&ids='. $sim_data['assign_reviewer']); ?>" onclick="return false"><i class="fa fa-info-circle fa-2x" aria-hidden="true"></i></a>
				<?php endif; ?>
			</div>
		</div>
	</div>
	<div class="rev-topRbanner">
		<div class="btn rev" id="share-button">GET SHAREABLE LINK</div>
		<?php if ($role == 'reviewer'): ?>
		<div class="btn rev" id="Notify" data-login-type="author">NOTIFY AUTHOR</div>
		<?php elseif ($role == 'author'): ?>
		<div  class="btn rev" id="Notify" data-login-type="reviewer">NOTIFY REVIEWER</div>
		<?php endif; ?>
		<div class="btn rev" onclick="getPDF('.show_comment')">EXPORT PDF</div>
		<!--<div class="btn rev">REVIEW</div>-->
		<!--<div class="btn rev">FEEDBACK</div-->
	</div>
</div>
<div class="RevBanner100">
	<div class="RevBanner70">
    <?php 
        $preview_url = $db->getPreviewAuthPage($sim_data['sim_ques_type'], $sim_data['sim_temp_type'], $sim_id);
        if ($preview_url != 'JavaScript:void(0);'): ?>
        <iframe src="<?php echo $preview_url; ?>" width="100%" height="500px"></iframe>
        <?php else: ?>
        <h4>Oops. Preview page not available.!</h4>
        <?php endif; ?>
	</div>
	<div class="RevBanner30">
		<div class="Comment1">
			<h4>Comment</h4>
			<button type="button" title="Loading Chat" data-loading-text="<i class='fa fa-refresh fa-spin'></i> Loading..." class="btn btn-xs btn-default pull-right load_chat"><i class="fa fa-refresh"></i></button>
			<div class="input-group">
				<form class="comment-form" name="comment-form" method="post">
					<div class="CMTPostB">
						<div class="commentbox">
							<div class="Replyname" style="background:#00d5f0"><?php echo ( ! empty($user_data['fname'])) ? $user_data['fname'] : $user_data['username']; ?></div>
							<input type="hidden" name="scenario_id" value="<?php echo $sim_data['scenario_id'] ?>">
							<input type="hidden" name="comment_add" value="1">
							<input type="hidden" name="chat_type" value="1">
							<input type="hidden" name="uid" value="<?php echo $userid; ?>">
							<input type="text" class="form-control comment" name="comment" required="required" placeholder="Add comment..." aria-describedby="basic-addon2"/>
						</div>
						<ul class="msgpostbtn">
							<li class="msgpstgray"><input type="submit" value="Enter"></li>
						</ul>
					</div>
				</form>					  
			</div>
		</div>
		<div class="addcommentbanner">
			<div class="Rev-addcomment show_comment" id="show_comment"></div>
			<div class="add_commentbox"></div>
		</div>
		<div class="revinst">Type your feedback and press <b>Enter</b></div>
	</div>
</div>
<script type="text/javascript" src="content/js/comment.js"></script>
<script type="text/javascript" src="content/js/tooltip.js"></script>
<script type="text/javascript" src="content/js/polyfill.min.js"></script>
<script type="text/javascript" src="content/js/jspdf.min.js"></script>
<script type="text/javascript" src="content/js/html2canvas.js"></script>
<script type="text/javascript">
	/* GET SHAREABLE LINK */
	var $temp = $("<input>");
	var $url  = $(location).attr('href');
	$('#share-button').on('click', function(){
		$("body").append($temp);
		$temp.val($url).select();
		document.execCommand("copy");
		$temp.remove();
		$(this).text("LINK COPIED.");
	});
	/* GET COMMENT PDF */
	function getPDF(id){
		var HTML_Width = $(id).width();
		var HTML_Height = $(id).height();
		var top_left_margin = 15;
		var PDF_Width = HTML_Width+(top_left_margin*2);
		var PDF_Height = (PDF_Width*1.5)+(top_left_margin*2);
		var canvas_image_width = HTML_Width;
		var canvas_image_height = HTML_Height;		
		var totalPDFPages = Math.ceil(HTML_Height/PDF_Height)-1;
		html2canvas($(id)[0],{allowTaint:true}).then(function(canvas) {
			canvas.getContext('2d');
			var imgData = canvas.toDataURL("image/jpeg", 1.0);
			var pdf = new jsPDF('p', 'pt',  [PDF_Width, PDF_Height]);
		    pdf.addImage(imgData, 'JPG', top_left_margin, top_left_margin,canvas_image_width,canvas_image_height);
			for (var i = 1; i <= totalPDFPages; i++) {
				pdf.addPage(PDF_Width, PDF_Height);
				pdf.addImage(imgData, 'JPG', top_left_margin, -(PDF_Height*i)+(top_left_margin*4),canvas_image_width,canvas_image_height);
			}
			pdf.save("<?php echo $sim_data['Scenario_title'] ?>-comment.pdf");
        });
	};
</script>
<?php 
require_once 'includes/footer.php';
