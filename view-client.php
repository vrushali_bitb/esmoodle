<?php 
ob_start();
session_start();
if (isset($_SESSION['username'])) {
	$client_id	= $_SESSION['client_id'];
	$userid		= $_SESSION['userId'];
    $user		= $_SESSION['username'];
	$role		= $_SESSION['role'];
}
else {
	header('location: index');
}
require_once 'config/db.class.php';
require_once 'includes/learners_menu.php';
$db   		= new DBConnection();

/* page access only super administrator or client admin */
$ac_type	= $db->checkAccountType();
($ac_type != 1 && $ac_type != 2) ? header('location: index') : '';
$cid  		= ( ! empty($_GET['cid'])) ? base64_decode($_GET['cid']) : exit('Error');
$data 		= $db->getClient($cid);
$logo 		= $data['logo'];
$logo_path	= $db->getBaseUrl('img/logo/');
$columns 	= 'name, email, phone_code, phone';
$tbl_name	= 'client_contact_detail';
$condi 		= " WHERE client_id = '". $cid ."'";
$execond 	= $condi . " AND  type = 1";
$deccond 	= $condi . " AND  type = 2";
$billcond 	= $condi . " AND  type = 3";
$executive 	= @$db->getClientData($columns, $tbl_name, $execond)[0];
$decision  	= @$db->getClientData($columns, $tbl_name, $deccond)[0];
$billing  	= @$db->getClientData($columns, $tbl_name, $billcond)[0]; ?>
<div class="User-edit_profile">
    <div class="container-fluid">
        <div class="edit_profile-banner ViEWpage-banner">
            <div class="row">
                <div class="col-sm-12">
					<div class="edit-profilemainbanner">
						<div class="edit-profile-img">
							<div class="previewimgshow">
							<a href="https://<?php echo $data['domain_name'] ?>" target="_blank"><img class="img-fluid" id="uploadPreview" src="<?php echo $logo_path . $logo; ?>"></a>
							</div>
							<div class="previewtextshow">
								<div><span class="previewtextbold">Organization Name</span>: <span><?php echo $data['organization'] ?></span></div>
								<div><span class="previewtextbold">Country</span>: <span><?php echo ( ! empty($data['country'])) ? $db->getCountry($data['country'])[0]['name'] : '--'; ?></span></div>
								<div><span class="previewtextbold">State</span>: <span><?php echo ( ! empty($data['state'])) ? $db->getState(NULL, $data['state'])[0]['name'] : '--'; ?></span></div>
								<div><span class="previewtextbold">City</span>: <span><?php echo ( ! empty($data['city'])) ? $db->getCity(NULL, $data['city'])[0]['name'] : '--';?></span></div>
								<div><span class="previewtextbold">Tax ID</span>: <span><?php echo $data['tax_id'] ?></span></div>
								<div><span class="previewtextbold">Address</span>: <span><?php echo $data['address'] ?></span></div>
							</div>
						</div>
						<div class="edit-profilefield">
							<button type="button" class="btn btn-primary pull-right PullRcatagry" onclick="window.history.back();">Back</button>
							<h3>Contact Detail</h3>
							<div class="view-etable">
								<div class="table-responsive">
									<table class="table">
										<tr>
											<th colspan="4">Executive Contact</th>
										</tr>
										<tr>
											<td>Contact Name</td>
											<td>Email</td>
											<td>Phone Code</td>
											<td>Contact PH Number</td>
										</tr>
										<tr class="vieweditfontbig">
											<td><?php echo $executive['name'] ?></td>
											<td><?php echo $executive['email'] ?></td>
											<td><?php echo $executive['phone_code'] ?></td>
											<td><?php echo $executive['phone'] ?></td>
										</tr>
									</table>
								</div>
								<div class="table-responsive">
									<table class="table">
										<tr>
											<th colspan="4">Decision Maker</th>
										</tr>
										<tr>
											<td>Contact Name</td>
											<td>Email</td>
											<td>Phone Code</td>
											<td>Contact PH Number</td>
										</tr>
										<tr class="vieweditfontbig">
											<td><?php echo $decision['name'] ?></td>
											<td><?php echo $decision['email'] ?></td>
											<td><?php echo $decision['phone_code'] ?></td>
											<td><?php echo $decision['phone'] ?></td>
										</tr>
									</table>
								</div>
								<div class="table-responsive">
									<table class="table">
										<tr>
											<th colspan="4">Billing Contact</th>
										</tr>
										<tr>
											<td>Contact Name</td>
											<td>Email</td>
											<td>Phone Code</td>
											<td>Contact PH Number</td>
										</tr>
										<tr class="vieweditfontbig">
											<td><?php echo $billing['name'] ?></td>
											<td><?php echo $billing['email'] ?></td>
											<td><?php echo $billing['phone_code'] ?></td>
											<td><?php echo $billing['phone'] ?></td>
										</tr>
									</table>
								</div>
								<h3>Login Detail</h3>
								<div class="table-responsive">
									<table class="table">
										<tr>
											<th>Subdomain Name</th>
											<th>Database Name</th>
											<th>Account Type</th>
											<th>Role</th>
											<th>Login Type</th>
										</tr>
										<tr class="vieweditfontbig">
											<td><?php echo $data['domain_name'] ?></td>
											<td><?php echo $data['db_name'] ?></td>
											<td><?php echo $data['account_type'] ?></td>
											<td><?php echo $data['role'] ?></td>
											<td><?php echo $data['type'] ?></td>
										</tr>
									</table>
								</div>
								<h3>SMTP Mail Configuration Detail</h3>
								<div class="table-responsive">
									<table class="table">
										<tr>
											<th>SMTP Host Name</th>
											<th>SMTP User Name</th>
											<th>SMTP Password</th>
											<th>SMTP PORT</th>
										</tr>
										<tr class="vieweditfontbig">
											<td><?php echo $data['smtp_host_name'] ?></td>
											<td><?php echo $data['smtp_user_name'] ?></td>
											<td><?php echo $data['smtp_pwd'] ?></td>
											<td><?php echo $data['smtp_port'] ?></td>
										</tr>
									</table>
								</div>
								<h3>License Information</h3>
								<div class="table-responsive">
									<table class="table">
										<tr>
											<th>Start Date</th>
											<th>End Date</th>
											<th>No. of Categories</th>
											<th>Simulation in each Category</th>
											<th>No. of Admin</th>
											<th>No. of Author</th>
											<th>No. of Reviewer</th>
											<th>No. of Learner</th>
											<th>No. of Educator</th>
										</tr>
										<tr class="vieweditfontbig">
											<td><?php echo $data['start_date'] ?></td>
											<td><?php echo $data['end_date'] ?></td>
											<td class="rolviwtext"><?php echo $data['category'] ?></td>
											<td class="rolviwtext"><?php echo $data['sim'] ?></td>
											<td class="rolviwtext"><?php echo $data['admin'] ?></td>
											<td class="rolviwtext"><?php echo $data['auth'] ?></td>
											<td class="rolviwtext"><?php echo $data['reviewer'] ?></td>
											<td class="rolviwtext"><?php echo $data['learner'] ?></td>
											<td class="rolviwtext"><?php echo $data['educator'] ?></td>
										</tr>
									</table>
								</div>
								<h3>Hosting Detail</h3>
								<div class="table-responsive">
									<table class="table">
										<tr>
											<th>Hosting Space (MB)</th>
											<th>Used Space (MB)</th>
										</tr>
										<tr class="vieweditfontbig">
											<td><?php echo $data['hosting_space'] ?></td>
											<td class="rolviwtext"><?php echo $db->folderSize('scenario/upload/'. $data['domain_name']); ?></td>
										</tr>
									</table>
								</div>
								<h3>Manage Permissions</h3>
								<div class="table-responsive">
									<table class="table">
										<tr>
											<th>Manage Home Page</th>
											<th>Manage Branding</th>
											<th>Manage Proctoring</th>
										</tr>
										<tr class="vieweditfontbig">
											<td><?php echo ( ! empty($data['mhome'])) ? 'Yes' : 'No' ?></td>
											<td><?php echo ( ! empty($data['mbrand'])) ? 'Yes' : 'No' ?></td>
											<td><?php echo ( ! empty($data['proctor'])) ? 'Yes' : 'No' ?></td>
										</tr>
									</table>
								</div>
								<h3>Manage Template</h3>
								<?php $sim_temp = ( ! empty($data['templates'])) ? explode(',', $data['templates']) : []; ?>
								<div class="table-responsive SuperManageTemp">
									<div class="edit-sim-box">
										<div class="user_text editSim">Classic Simulation - Linear Simulation</div>
										<div class="linersim">
											<div class="edit-sim-radio Linerm">
												<div class="Check-box">
													<label class="checkstyle">
														<input type="checkbox" name="sim_tmp[]" value="1" class="linear_tmp tmp_type" <?php echo (in_array(1, $sim_temp)) ? 'checked' : ''; ?> disabled />
														<span class="checkmark"></span>
														<span class="text-R">Classic Template</span>
													</label>
													<span class="smallT">
														<p class="question-type">(MCQ, Drag and Drop, Swiping)</p>
														<p class="question-type">(Multiple Feedback)</p>
													</span>
												</div>
												<div class="Check-box">
													<label class="checkstyle">
														<input type="checkbox" name="sim_tmp[]" value="2" class="linear_tmp tmp_type" <?php echo (in_array(2, $sim_temp)) ? 'checked' : ''; ?> disabled />
														<span class="checkmark"></span>
														<span class="text-R">Multiple Template</span>
													</label>
													<span class="smallT">
														<p class="question-type">(MTF, Sequence, Sorting, MMCQ, MCQ, Drag and Drop, Swiping)</p>
														<p class="question-type">(Single Feedback)</p>
													</span>
												</div>  
											</div>
										</div> 
										<div class="linersim linersimBotom">
											<div class="user_text editSim">Classic Simulation - Branching Simulation</div>						
											<div class="edit-sim-radio Linerm">
												<div class="Check-box">
													<label class="checkstyle">
														<input type="checkbox" name="sim_tmp[]" value="4" class="branch_tmp tmp_type" <?php echo (in_array(4, $sim_temp)) ? 'checked' : ''; ?> disabled />
														<span class="checkmark"></span>
														<span class="text-R">Classic Template</span>
													</label>
													<span class="smallT">
														<p class="question-type">(MCQ, Drag and Drop,Swiping)</p>
														<p class="question-type">(Multiple Feedback)</p>
													</span>
												</div>
												<div class="Check-box">
													<label class="checkstyle">
														<input type="checkbox" name="sim_tmp[]" value="5" class="branch_tmp tmp_type" <?php echo (in_array(5, $sim_temp)) ? 'checked' : ''; ?> disabled />
														<span class="checkmark"></span>
														<span class="text-R">Multiple Template</span>
													</label>
													<span class="smallT">
														<p class="question-type">(MTF, Sorting, MMCQ, MCQ, Drag and Drop, Swiping)</p>
														<p class="question-type">(Single Feedback)</p>
													</span>
												</div>
											</div>
										</div>
										<div class="user_text editSim">Open Response Simulation</div>
										<div class="linersim linersimlast">
											<div class="Check-box">
												<label class="checkstyle">
													<input type="checkbox" name="sim_tmp[]" value="7" class="tmp_type" <?php echo (in_array(7, $sim_temp)) ? 'checked' : ''; ?> disabled />
													<span class="checkmark"></span>
													<span class="text-R">Open Response</span>
												</label>
											</div>
										</div>
										<div class="user_text editSim">Video Simulation</div>
										<div class="linersim">
											<div class="Check-box">
												<label class="checkstyle">
													<input type="checkbox" name="sim_tmp[]" value="8" class="tmp_type" <?php echo (in_array(8, $sim_temp)) ? 'checked' : ''; ?> disabled />
													<span class="checkmark"></span>
													<span class="text-R">Single Video Simulation</span>
												</label>
											</div>
										</div>
										<div class="linersim">
											<div class="user_text editSim">Linear Video Simulation</div>				
											<div class="edit-sim-radio Linerm">
												<div class="Check-box">
													<label class="checkstyle">
														<input type="checkbox" name="sim_tmp[]" value="6" class="linear_tmp tmp_type" <?php echo (in_array(6, $sim_temp)) ? 'checked' : ''; ?> disabled />
														<span class="checkmark"></span>
														<span class="text-R">Classic Template</span>
													</label>
													<span class="smallT">
														<p class="question-type">(MCQ, Drag and Drop, Swiping)</p>
														<p class="question-type">(Multiple Feedback)</p>
													</span>
												</div>
												<div class="Check-box">
													<label class="checkstyle">
														<input type="checkbox" name="sim_tmp[]" value="9" class="linear_tmp tmp_type" <?php echo (in_array(9, $sim_temp)) ? 'checked' : ''; ?> disabled />
														<span class="checkmark"></span>
														<span class="text-R">Multiple Template</span>
													</label>
													<span class="smallT">
														<p class="question-type">(MTF, Sequence, Sorting, MMCQ, MCQ, Drag and Drop, Swiping)</p>
														<p class="question-type">(Single Feedback)</p>
													</span>
												</div>
											</div>
										</div>
										<div class="linersim linersimBotom">
											<div class="user_text editSim">Branching Video Simulation</div>					
											<div class="edit-sim-radio Linerm">
												<div class="Check-box">
													<label class="checkstyle">
														<input type="checkbox" name="sim_tmp[]" value="10" class="branch_tmp tmp_type" <?php echo (in_array(10, $sim_temp)) ? 'checked' : ''; ?> disabled />
														<span class="checkmark"></span>
														<span class="text-R">Classic Template</span>
													</label>
													<span class="smallT">
														<p class="question-type">(MCQ, Drag and Drop,Swiping)</p>
														<p class="question-type">(Multiple Feedback)</p>
													</span>
												</div>
												<div class="Check-box">
													<label class="checkstyle">
														<input type="checkbox" name="sim_tmp[]" value="11" class="branch_tmp tmp_type" <?php echo (in_array(11, $sim_temp)) ? 'checked' : ''; ?> disabled />
														<span class="checkmark"></span>
														<span class="text-R">Multiple Template</span>
													</label>
													<span class="smallT">
														<p class="question-type">(MTF, Sorting, MMCQ, MCQ, Drag and Drop, Swiping)</p>
														<p class="question-type">(Single Feedback)</p>
													</span>
												</div>  
											</div>
										</div>
									</div>
								</div>
								<h3>Payment Detail</h3>
								<div class="table-responsive">
									<table class="table">
										<tr>
											<th>Amount Per Year</th>
											<th>Currency Type</th>
											<th>Payment Received</th>
										</tr>
										<tr class="vieweditfontbig">
											<td><?php echo ( ! empty($data['amount'])) ? $data['amount'] .' '. $data['amount_type'] : '--' ?></td>
											<td><?php echo ( ! empty($data['amount_cc'])) ? $data['amount_cc'] : '--' ?></td>
											<td><?php echo ( ! empty($data['pay_received'])) ? 'YES' : 'No' ?></td>
										</tr>	
									</table>
								</div>
							</div>
						</div>
                	</div>
            	</div>
        	</div>
    	</div>
	</div>
</div>
<?php 
require_once 'includes/footer.php';
