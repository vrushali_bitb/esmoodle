<div id="load_popup_modal_contant" class="" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h3 class="modal-title">Add Background</h3>
            </div>
            <div class="modal-body">
                <form enctype="multipart/form" name="char_background_form" id="char_background_form" method="post">
                	<input type="hidden" name="add_char_background" value="1" />
                    <div class="usermanage-form usermanage-banner">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <input type="file" name="file_image" class="file form-control" id="file_image" />
                                    <input type="text" class="form-control controls" disabled placeholder="Upload Size(1920/1080)" />
                                    <input type="hidden" name="type" value="1" />
                                </div>
                            </div>
                            <div class="col-sm-3 browsebtn">
                                <button class="browse btn btn-primary" type="button">Browse</button>
                            </div>
                            <div class="col-sm-3 browsebtn">
                                <span id="splashImg"></span> &nbsp;&nbsp; <span id="ImgDelete" style="display:none">
                                <a href="javascript:void(0);" data-img-name="" class="delete_group_icon" title="Delete Icon"><i class="fa fa-times" aria-hidden="true"></i></a></span>
                            </div>
                            <div class="col-sm-12">
                            	<div class="form-group">
                                	<textarea name="description" id="description" placeholder="Description" class="form-control"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="usermanage-add add1 clearfix"><input type="submit" class="btn btn-primary pull-right upload_img" value="Add"></div>
                </form>
            </div>
        </div>
     </div>
</div>
<script type="text/javascript">
$("#char_background_form").on('submit', (function(e) {
	e.preventDefault();
	$.LoadingOverlay("show");
	$.ajax({
		url: 'includes/process.php',
		type: "POST",
		data: new FormData(this),
		contentType: false,
		cache: false,
		processData:false,
		success: function(resdata) {
			var res = $.parseJSON(resdata);
			if (res.success == true) {
				$.LoadingOverlay("hide");
				swal({text: res.msg, buttons: false, icon: "success", timer: 1000});
				setTimeout(function() { window.location.reload(); }, 500);
			}
			else if (res.success == false) {
				$.LoadingOverlay("hide");
				swal({text: res.msg, buttons: false, icon: "error", timer: 1000});
			}
		}, error: function() {
			$.LoadingOverlay("hide");
			swal({text: 'Oops, something went wrong. Please try again later', buttons: false, icon: "error", timer: 1000});
		}
	});
}));
</script>
