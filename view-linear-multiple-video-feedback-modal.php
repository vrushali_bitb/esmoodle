<?php 
session_start();
if (isset($_SESSION['username'])):
	$user 	= $_SESSION['username'];
	$role 	= $_SESSION['role'];
	$userid	= $_SESSION['userId'];
	$domain = (isset($_SESSION['domain_name'])) ? $_SESSION['domain_name'] : $_SERVER['HTTP_HOST'];
else:
    header('location: index.php');
endif;
require_once 'config/db.class.php';
$db			= new DBConnection();
$path		= $db->getBaseUrl('scenario/upload/'.$domain.'/');
$aPoster	= $db->getBaseUrl('scenario/img/audio-poster.jpg');
$root_path	= $db->rootPath() . 'scenario/upload/'.$domain.'/';
$vtheme		= 'forest'; #city, fantasy, forest, sea

if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['qtype'])):
	$qtype 		= ( ! empty($_POST['qtype'])) ? $_POST['qtype'] : exit('error');
	$sid		= ( ! empty($_POST['sid'])) ? $_POST['sid'] : exit('error');
	$qid		= ( ! empty($_POST['qid'])) ? $_POST['qid'] : exit('error');
	$aid		= ( ! empty($_POST['aid'])) ? $_POST['aid'] : '';
	$subid		= ( ! empty($_POST['subid'])) ? $_POST['subid'] : '';
	$report		= ( ! empty($_POST['report'])) ? $_POST['report'] : '';
	$target		= ( ! empty($_POST['target'])) ? $_POST['target'] : '';
	$current	= ( ! empty($_POST['current'])) ? $_POST['current'] : '';
	$drag		= ( ! empty($_POST['drag_option'])) ? $_POST['drag_option'] : '';
	$post 		= ( ! empty($_POST['multi_data'])) ? json_decode($_POST['multi_data'], TRUE) : [];
	if ($qtype == 1):
		/****MTF****/
		foreach ($post['data'] as $mtf_data):
			if ( ! empty($mtf_data['choice_option_id']) && ! empty($mtf_data['match_option_id']) && $mtf_data['choice_option_id'] == $mtf_data['match_option_id']):
				$res = 1;
			else:
				$res = 2;
				break;
			endif;
		endforeach;
		$feedData = $db->getFeedback($qid, NULL, NULL, $res);
	elseif ($qtype == 2):
		/****Sequence****/
		$sequence = $db->getAnswersByQuestionId(md5($qid));
		foreach ($sequence as $sqData):
			$getCorrectSQ[] = $sqData['answer_id'];
		endforeach;
		foreach ($post['data'] as $sq_data):
			$getPostSQ[] = $sq_data['choice_option_id'];
		endforeach;
		if ( ! empty($getCorrectSQ) && ! empty($getPostSQ) && $getCorrectSQ === $getPostSQ):
			$res = 1;
		else:
			$res = 2;
		endif;
		$feedData = $db->getFeedback($qid, NULL, NULL, $res);
	elseif ($qtype == 3):
		/****Sorting****/
		$sorting = $db->getAnswersByQuestionId(md5($qid));
		foreach ($sorting as $sortData):
			$getCorrectSort[$sortData['answer_id']] = $db->getSubOptions($sortData['answer_id'], 'sub_options_id');
		endforeach;
		foreach ($post['data'] as $sort_data):
			if ( ! empty($sort_data['choice_option_id']))
			$getPostSort[$sort_data['choice_option_id']][] = ( ! empty($sort_data['match_option_id'])) ? $sort_data['match_option_id'] : [];
		endforeach;
		$c 		  = $db->sortMultiArray($getCorrectSort);
		$p 		  = ( ! empty($getPostSort)) ? $db->sortMultiArray($getPostSort) : [];
		$res	  = ( ! empty($c) && ! empty($p) && $db->matchArrayValues($c, $p) == TRUE) ? 1 : 2;
		$feedData = $db->getFeedback($qid, NULL, NULL, $res);
	elseif ($qtype == 4 || $qtype == 7):
		/****MCQ**** OR ****SWIPING****/
		$true 		= $db->getAnswerMaster(md5($aid))['true_option'];
		$res 		= ($true) ? 1 : 2;
		$feedData	= $db->getFeedback($qid, NULL, NULL, $res);
	elseif ($qtype == 5):
		/****MMCQ****/
		$mmcqTrue = $db->getQuestionsByQuesId(md5($qid))[0]['true_options'];
		if ($mmcqTrue):
			$data = explode(',', $mmcqTrue);
			foreach ($data as $data):
				$mmcqOption 	  = ($data > 1) ? $data - 1 : 0;
				$mmcqData 		  = $db->getAnswersByQuestionId(md5($qid));
				$mmcqTrueOption[] = $mmcqData[$mmcqOption]['answer_id'];
			endforeach;
			foreach ($post['data'] as $mmcq_data):
				$getPostMmcq[] = $mmcq_data['match_option_id'];
			endforeach;
			if ( ! empty($getPostMmcq)):
				$res = (empty(array_diff($getPostMmcq, $mmcqTrueOption))) ? 1 : 2;
			else:
				$res = 2;
			endif;
			$feedData = $db->getFeedback($qid, NULL, NULL, $res);
		endif;
	elseif ($qtype == 8 && $drag == 1):
		/****D&D-Multiple****/
		foreach ($post['data'] as $dd_data):
			if ( ! empty($dd_data['choice_option_id']) && ! empty($dd_data['match_option_id']) && $dd_data['choice_option_id'] == $dd_data['match_option_id']):
				$res = 1;
			else:
				$res = 2;
				break;
			endif;
		endforeach;
		$feedData = $db->getFeedback($qid, NULL, NULL, $res);
	elseif ($qtype == 8 && $drag == 2):
		/****D&D-Single****/
		$true 		= $db->getAnswerMaster(md5($aid))['true_option'];
		$res 		= ($true) ? 1 : 2;
		$feedData	= $db->getFeedback($qid, NULL, NULL, $res);
	else:
		exit('error');
	endif;
	$feedAsset  = '';
	if ( ! empty($feedData['feedback'])):
		$feedAsset = TRUE;
	elseif ( ! empty($feedData['feed_speech_text'])):
		$feedAsset = TRUE;
	elseif ( ! empty($feedData['feed_audio']) && file_exists($root_path . $feedData['feed_audio'])):
		$feedAsset = TRUE;
	elseif ( ! empty($feedData['feed_video']) && file_exists($root_path . $feedData['feed_video'])):
		$feedAsset = TRUE;
	elseif ( ! empty($feedData['feed_screen']) && file_exists($root_path . $feedData['feed_screen'])):
		$feedAsset = TRUE;
	elseif ( ! empty($feedData['feed_image']) && file_exists($root_path . $feedData['feed_image'])):
		$feedAsset = TRUE;
	elseif ( ! empty($feedData['feed_document']) && file_exists($root_path . $feedData['feed_document'])):
		$feedAsset = TRUE;
	endif;
	$status = ($res == 1) ? TRUE : FALSE;
	if ( ! empty($feedAsset)): ?>
<link rel="stylesheet" href="videojs/node_modules/video.js/dist/video-js.min.css" />
<link rel="stylesheet" href="videojs/node_modules/video.js/dist/themes/forest/index.css" />
<script type="text/javascript" src="videojs/node_modules/video.js/dist/video.min.js"></script>
<div id="load_popup_modal_contant" class="feedback_box" role="dialog">
	<div class="modal-dialog modal-dialog-centered">
    	<div class="modal-content">
        	<div class="modal-header feedback-header">
            	<button type="button" class="feedclose" data-show-report="<?php echo $report; ?>" data-target="<?php echo $target; ?>" data-cur="<?php echo $current; ?>" data-qtype="<?php echo $qtype; ?>" data-status="<?php echo $status; ?>" data-qid="<?php echo $qid; ?>" data-dismiss="modal">&times;</button>
     			<h3 class="modal-title" id="exampleModalLabel">FEEDBACK</h3>
      		</div>
      		<div class="modal-body">
                <div class="row feedtextarea">
                    <div class="row feed">
                        <div class="flexfeed">
                           <div class="flexfeed img-videobox">
								<?php if ( ! empty($feedData['feed_audio']) && file_exists($root_path . $feedData['feed_audio'])): ?>
								<audio id="fvideo-1" class="video-js vjs-theme-<?php echo $vtheme ?>" controls preload="auto" width="300px" height="100px" data-setup="{}" data-type="audio" poster="<?php echo $aPoster ?>">
									<source src="<?php echo $path . $feedData['feed_audio'] ?>" type="audio/<?php echo strtolower(pathinfo($feedData['feed_audio'], PATHINFO_EXTENSION)); ?>" />
								</audio>
								<?php elseif ( ! empty($feedData['feed_video']) && file_exists($root_path . $feedData['feed_video'])): ?>
								<video id="fvideo-1" class="video-js vjs-theme-<?php echo $vtheme ?>" controls preload="auto" width="640" height="264" data-setup="{}" data-type="video">
									<source src="<?php echo $path . $feedData['feed_video'] ?>" type="video/<?php echo strtolower(pathinfo($feedData['feed_video'], PATHINFO_EXTENSION)); ?>" />
								</video>
								<?php elseif ( ! empty($feedData['feed_screen']) && file_exists($root_path . $feedData['feed_screen'])): ?>
								<video id="fvideo-1" class="video-js vjs-theme-<?php echo $vtheme ?>" controls preload="auto" width="640" height="264" data-setup="{}" data-type="video">
									<source src="<?php echo $path . $feedData['feed_screen'] ?>" type="video/<?php echo strtolower(pathinfo($feedData['feed_screen'], PATHINFO_EXTENSION)); ?>" />
								</video>
								<?php elseif ( ! empty($feedData['feed_image']) && file_exists($root_path . $feedData['feed_image'])): ?>
								<a href="<?php echo $path . $feedData['feed_image']; ?>" data-fancybox data-caption="<?php echo stripslashes($feedData['feedback']); ?>">
									<img class="img-fluid" src="<?php echo $path . $feedData['feed_image']; ?>" />
								</a>
								<?php elseif ( ! empty($feedData['feed_document']) && file_exists($root_path . $feedData['feed_document'])): ?>
								<a href="javascript:;" data-src="<?php echo $path . $feedData['feed_document']; ?>" data-fancybox data-type="iframe" data-caption="<?php echo stripslashes($feedData['feedback']); ?>" class="document">
									<img class="img-fluid pdfclissicsIm" src="img/icons/pdf_icon.svg" />
								</a>
								<?php elseif ( ! empty($feedData['feed_speech_text'])): ?>
									<button onclick="speak_feedback('div.feedback_intro_1')"><i id="playicon_feedback" class="fa fa-play playicon" aria-hidden="true"></i><i id="pauseicon_feedback" class="fa fa-pause pauseicon" style="display:none" aria-hidden="true"></i></button>
									<div class="feedback_intro_1" style="display:none;"><?php echo $feedData['feed_speech_text'] ?></div>
								<?php endif; ?>
							</div>
						   <div class="feedbacktext"><?php echo ( ! empty($feedData['feedback'])) ? stripslashes($feedData['feedback']) : ''; ?></div>
					   </div>                        
                    </div>
                </div>
      		</div>
  		</div>
  	</div>
</div>
<script src="content/js/launch-sim-linear-multiple-video-feedback.js?v=<?php echo time() ?>"></script>
<?php else: ?>
<style type="text/css">
#load_popup_modal_show { display:none !important;}
</style>
<div class="feedclose" data-show-report="<?php echo $report; ?>" data-target="<?php echo $target; ?>" data-cur="<?php echo $current; ?>" data-qtype="<?php echo $qtype; ?>" data-status="<?php echo $status; ?>" data-qid="<?php echo $qid; ?>"></div>
<script src="content/js/launch-sim-linear-multiple-video.js?v=<?php echo time() ?>"></script>
<?php endif; else: exit('error'); endif; ?>
