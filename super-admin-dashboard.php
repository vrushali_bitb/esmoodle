<?php 
ob_start();
session_start();
if (isset($_SESSION['username'])) {
  $user   = $_SESSION['username'];
  $role   = $_SESSION['role'];
  $userid = $_SESSION['userId'];
} else {
  header('location: index.php');
}

require_once 'config/db.class.php';
require_once 'includes/learners_menu.php';
$db = new DBConnection();

/* page access only super administrator */
($db->checkAccountType() != 1) ? header('location: index') : '';

/* Get clients hosting data */
$hdata = $db->getUsedHostingData(); ?>
<style>
  body{
    overflow:hidden;
  }
  .flexRI.client.clients {
      display: flex;
  }
  .flexRI.client {
      display: flex;
  }
  .flexRI {
      display: flex;
  }
  flexItem .item {
    margin: 5px;
}
  .hometab.active,
  .menu1tab.active{
      display: flex !important;
  }
  @media (min-width:320px)   and (max-width: 767px) {
  body { overflow: auto; }
  .menu1tab.active {
    display: block !important;
}
  }
</style>
<div class="admin-dashboard Ev2 super-adminE2">
  <div class="container-fluid">
  <div class="flexItem flexItemsdMinDash">
    <div class="col-container flextlistbaner flextlistbanertab2 menu1tab clearfix owl-theme active">
      <div class="item">
        <div class="supDASHTOP supDASHTOPtab2 supadminDASHTOP">
          <div class="clienttextbox">
            <div class="CTUser">CLIENT</div>
            <div class="CTUserNo" id="superAdmin_client">0</div>
            <div class="CTGroupBanneR">
              <div class="CTGroup">SUB-CLIENT</div>
              <div class="CTGroupNo" id="superAdmin_sub_client">0</div>
            </div>
          </div>
        </div>
      </div>
      <div class="item">
        <div class="supDASHTOP supDASHTOPtab2">          
          <div class="superadhtable-baaner">
            <ul class="superadhtable manAGEYurself ">
              <li>Group <span id="superAdmin_group">0</span></li>
              <li>Admin <span id="superAdmin_admin">0</span></li>
              <li>Author <span id="superAdmin_author">0</span></li>
              <li>Reviewer <span id="superAdmin_reviewer">0</span></li>
              <li>Educator <span id="superAdmin_educator">0</span></li>
              <li>Learner <span id="superAdmin_learner">0</span></li>
            </ul>
          </div>
        </div>
      </div>
      <div class="item item900" style="width:900px">
        <div class="supDASHTOP supDASHTOPtab2 supDASHTOPta2_1">
          <div class="flextlistitem">
            <div class="textFlex">
              <div class="usermanage-form">
                <select class="form-control multiselect-ui selection" name="client_id" id="client_id" data-placeholder="Select Sub-Client" multiple="multiple">
                  <option value="0" selected="selected" disabled="disabled">Select Sub-Client</option>
                </select>
              </div>
              <div class="ShowhostingBanner">
                <div class="Showhostingitem">
                  <div class="hosting_space">
                    <div class="A24F">HOSTING SPACE</div>
                    <div class="hosting_no" id="hosting_space">0</div>
                  </div>
                </div>
                <div class="Showhostingitem">
                  <p class="A24F">START DATE <span id="sdate"></span</p>
                  <p class="A24F">END DATE <span id="edate"></span<</p>
                </div>
                <div class="Showhostingitem">
                  <div class="superadhtable-baaner">
                    <ul class="superadhtable HOSTstartban">
                      <li>Group <span id="group">0</span></li>
                      <li>Admin <span id="admin">0</span></li>
                      <li>Author <span id="author">0</span></li>
                      <li>Reviewer <span id="reviewer">0</span></li>
                      <li>Educator <span id="educator">0</span></li>
                      <li>Learner <span id="learner">0</span></li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="item">
        <div class="supDASHTOP supDASHTOPtab2">
          <div id="UsedHostingData"  style="width: 100%; height: 170px;"></div>
        </div>
      </div>
    </div>
  </div>

  </div>
  <div class="tab-content super-admin">
    <div id="home" class="tab-pane fade in active">
      <div class="adminT">
        <div class="col-sm-2"><a href="<?php echo $db->getBaseUrl('client-management.php') ?>">
            <div class="dashboard-box colorBoxW"> <img class="img-responsive width70 svg" src="img/dash_icon/manage_client.svg">
              <div class="dash-sub-title">Manage Client</div>
            </div>
          </a> </div>
        <div class="col-sm-2"> <a href="<?php echo $db->getBaseUrl('email-management.php') ?>">
            <div class="dashboard-box colorBoxW"> <img class="img-responsive width70 svg" src="img/dash_icon/manage_template.svg">
              <div class="dash-sub-title">Manage Template</div>
            </div>
          </a> </div>
        <div class="col-sm-2"><a href="<?php echo $db->getBaseUrl('manage-branding.php') ?>">
            <div class="dashboard-box colorBoxW"> <img class="img-responsive width70 svg" src="img/dash_icon/branding.svg">
              <div class="dash-sub-title">Manage Branding</div>
            </div>
          </a> </div>
        <div class="col-sm-2"><a href="<?php echo $db->getBaseUrl('home-page-management.php') ?>">
            <div class="dashboard-box colorBoxW"> <img class="img-responsive width70 svg" src="img/dash_icon/homepage.svg">
              <div class="dash-sub-title">Manage Home Page</div>
            </div>
          </a> </div>
        <div class="col-sm-2"><a href="#">
            <div class="dashboard-box colorBoxW"> <img class="img-responsive width70 svg" src="img/dash_icon/library.svg">
              <div class="dash-sub-title">Manage Library</div>
            </div>
          </a> </div>
        <div class="col-sm-2"><a href="<?php echo $db->getBaseUrl('') ?>">
            <div class="dashboard-box colorBoxW"> <img class="img-responsive width70 svg" src="img/dash_icon/view_report1.svg">
              <div class="dash-sub-title">View Report</div>
            </div>
          </a> </div>
        <div class="col-sm-2"><a href="#">
            <div class="dashboard-box colorBoxW"> <img class="img-responsive width70 svg" src="img/dash_icon/Rules_Management.svg">
              <div class="dash-sub-title">Rule Management</div>
            </div>
          </a> </div>
        <div class="col-sm-2"><a href="<?php echo $db->getBaseUrl('support.php') ?>">
            <div class="dashboard-box colorBoxW"> <img class="img-responsive width70 svg" src="img/dash_icon/support_1.svg">
              <div class="dash-sub-title">Suport</div>
            </div>
          </a> </div>
      </div>
    </div>
  </div>
</div>
<script src="content/js/canvasjs.min.js"></script>
<script type="text/javascript">
  $(function() {
    function defaultData(){
      $.LoadingOverlay("show");
      $.ajax({
        url: 'includes/main-process.php',
        type: "POST",
        data: 'getDefaultData='+ true,
        cache: false,
        success: function(resdata) {
          $.LoadingOverlay("hide");
          var res = $.parseJSON(resdata);
          if (res.success == true) {
            $('#superAdmin_organization').html(res.organization);
            $('#superAdmin_client').html(res.client);
            $('#superAdmin_sub_client').html(res.subclient);
            $('#superAdmin_group').html(res.data.group);
            $('#superAdmin_admin').html(res.data.admin);
            $('#superAdmin_author').html(res.data.auth);
            $('#superAdmin_reviewer').html(res.data.reviewer);
            $('#superAdmin_learner').html(res.data.learner);
            $('#superAdmin_educator').html(res.data.educator);
          }
          else {
            swal({text: 'Oops. something went wrong please try again.', buttons: false, icon: "error", timer: 2000});
          }
        },error: function() {
          $.LoadingOverlay("hide");
          swal({text: 'Oops. something went wrong please try again.', buttons: false, icon: "error", timer: 2000});
        }
      });
    }

    defaultData();

    var maxCount = 1;
    $('.multiselect-ui').multiselect({
      disableIfEmpty: true,
      filterPlaceholder: 'Search & Select Client',
      enableCaseInsensitiveFiltering: true,
      enableFiltering: true,
      maxHeight: 250,
      numberDisplayed: 1,
      includeSelectAllOption: false,
      onChange: function(option, checked, select) {
        //Get selected count
        var selectedCount = $("#client_id").find("option:selected").length;
        
        //If the selected count is equal to the max allowed count, then disable any unchecked boxes
        if (selectedCount >= maxCount) {
          $("#client_id option:not(:selected)").prop('disabled', true);
          $("#client_id").multiselect('refresh');
          var cid = $("#client_id").find("option:selected").val();
          $.LoadingOverlay("show");
          $.ajax({
            url: 'includes/main-process.php',
            type: "POST",
            data: 'getClientData='+ true + '&client_id='+ cid,
            cache: false,
            success: function(resdata) {
              $.LoadingOverlay("hide");
              var res = $.parseJSON(resdata);
              if (res.success == true) {
                if (res.data.client != false) {
                  $('#clients_data').html(res.data.client);
                }
                else {
                  $('#clients_data').html(0);
                }
                $('#organization').html(res.organization);
                $('#sdate').html(res.start_date);
                $('#edate').html(res.end_date);
                $('#hosting_space').html(res.used_space + ' / ' + res.space);
                $('#group').html(res.data.group);
                $('#admin').html(res.data.admin);
                $('#author').html(res.data.auth);
                $('#reviewer').html(res.data.reviewer);
                $('#learner').html(res.data.learner);
                $('#educator').html(res.data.educator);
              }
              else if (res.success == false) {
                swal({text: res.msg, buttons: false, icon: "error", timer: 2000});
              }
            },error: function() {
              $.LoadingOverlay("hide");
              swal({text: 'Oops. something went wrong please try again.', buttons: false, icon: "error", timer: 2000});
            }
          });
        }
        //If the selected count is less than the max allowed count, then set all boxes to enabled
        else {
          $("#client_id option:disabled").prop('disabled', false);
          $("#client_id").multiselect('refresh');
          $('#group,#admin,#author,#reviewer,#learner,#educator,#clients_data,#hosting_space').html(0);
          $('#organization,#sdate,#edate').html('&nbsp;');
        }
      }
    });

		$('#client_id').empty();
		$('#client_id').multiselect('refresh');
		$.getJSON('includes/main-process.php?getClients=true', function(res) {
      if (res.success == true) {
				$('#client_id').multiselect('dataprovider', res.data);
			}
			else if (res.success == false) {
				$('#client_id').multiselect('rebuild');
				$('#client_id').multiselect('refresh');
			}
		});
  });
  
  CanvasJS.addColorSet("colorcode", ["#6d3073", "#3b602d", "#c07744", "#678087", "#678087"]);
  var UsedHostingData = new CanvasJS.Chart("UsedHostingData", {
    colorSet: "colorcode",
    height:200,
    backgroundColor: "",
    animationEnabled: true,
    theme: "light2", // "light1", "light2", "dark1", "dark2"      
    data: [{
      type: "doughnut",
      showInLegend: false,
      toolTipContent: "{legendText}",
      indexLabelFontSize: 10,
      dataPoints: <?php echo json_encode($hdata['dataPoints'], JSON_NUMERIC_CHECK); ?>}]
  });
  UsedHostingData.render();
  var total = '<?php echo $hdata['total_size'] ?>';
  UsedHostingData.set("title", {text: total, verticalAlign: "center"});
</script>
<?php 
require_once 'includes/footer.php';
