<?php
session_start();
if (isset($_SESSION['username'])) {
	$user   = $_SESSION['username'];
	$role   = $_SESSION['role'];
	$userid = $_SESSION['userId'];
} else {
	header('location: index.php');
}
require_once 'config/db.class.php';
require_once 'includes/learners_menu.php';
$db = new DBConnection();

#--------------------ASSIGNED LEARNERS------------------
$sql = "SELECT DISTINCT s.Scenario_title, GROUP_CONCAT(DISTINCT a.group_id SEPARATOR ',') as groupid FROM 
		scenario_master s LEFT JOIN 
		assignment_tbl a ON s.scenario_id = a.scenario_id INNER JOIN 
		group_tbl g LEFT JOIN 
		category_tbl c ON s.scenario_category = c.cat_id WHERE 
		s.assign_author != '' AND s.author_scenario_type != '0' AND a.group_id IS NOT NULL GROUP BY a.scenario_id";
$q = $db->prepare($sql);
if ($q->execute() && $q->rowCount() > 0):
	foreach ($q->fetchAll(PDO::FETCH_ASSOC) as $row):
		$l = 0;
		$data[] = $db->getGroupColumns($row['groupid'], 'learner')[0]['col'];
		$getdata[] = implode(',', array_unique(explode(',', $db->addMultiIds($data))));
		$total_assign_learner = count(explode(',', $getdata[$l]));
		$get_learner = $getdata[$l];
		$l++;
		$dataPoints[] = array('y' => $total_assign_learner, 'name' => $db->getLearner($get_learner), 'label' => $row['Scenario_title']);
	endforeach;
else:
	$dataPoints[] = [];
endif;
#--------------END-ASSIGNED LEARNERS--------------------

#---------------TOTAL SIMULATION---------------
$total_assign_sim = $db->getTotalScenario();
#---------------END TOTAL SIMULATION----------

#---------------TOTAL PENDING SIMULATION---------------
$sqlp = "SELECT DISTINCT scenario_id FROM scenario_master WHERE scenario_id NOT IN (SELECT scenario_id FROM question_tbl)";
$ps = $db->prepare($sqlp); $ps->execute();
$total_pending_sim = $ps->rowCount();
$pendingDataPoints = array(array("y" => $total_pending_sim, "color" => "#ed7d31"), array("y" => (!empty($total_pending_sim)) ? $total_assign_sim - $total_pending_sim : $total_assign_sim, "color" => "#4372c4"));
#---------------END PENDING SIMULATION--------------

#------------------NEW SIMULATION-------------------
$sqlnew = "SELECT DISTINCT Scenario_title FROM scenario_master WHERE sim_ques_type = '0'";
$new = $db->prepare($sqlnew); $new->execute();
$total_new_sim = $new->rowCount();
if ($total_new_sim > 0) :
	foreach ($new->fetchAll(PDO::FETCH_ASSOC) as $newrow) :
		$newDataPoints[] = array('y' => 1, 'name' => $newrow['Scenario_title']);
	endforeach;
else :
	$newDataPoints[] = [];
endif;
#---------------END NEW SIMULATION---------------

#---------------REVIEWED SIMULATION--------------
$sqlreviewed = "SELECT DISTINCT Scenario_title FROM scenario_master WHERE status = '14'";
$reviewed = $db->prepare($sqlreviewed);
if ($reviewed->execute() && $reviewed->rowCount() > 0) :
	foreach ($reviewed->fetchAll(PDO::FETCH_ASSOC) as $revrow) :
		$revieweddataPoints[] = array('y' => 1, 'label' => $revrow['Scenario_title']);
	endforeach;
else :
	$revieweddataPoints[] = [];
endif;
#---------------END REVIEWED SIMULATION-----------

#---------------REVIEW IN PROCESS-----------------
$sqlpro = "SELECT DISTINCT Scenario_title FROM scenario_master WHERE status = '14'";
$reviewpro = $db->prepare($sqlpro);
if ($reviewpro->execute() && $reviewpro->rowCount() > 0) :
	foreach ($reviewpro->fetchAll(PDO::FETCH_ASSOC) as $reviewprorow) :
		$reviewprodataPoints[] = array('y' => 1, 'label' => $reviewprorow['Scenario_title']);
	endforeach;
else :
	$reviewprodataPoints[] = [];
endif;
#---------------END REVIEW IN PREOCESS-----------

#---------------READY REVIEW FIXED---------------
$sqlreview = "SELECT DISTINCT Scenario_title FROM scenario_master WHERE status = '15'";
$readyreview = $db->prepare($sqlreview);
if ($readyreview->execute() && $readyreview->rowCount() > 0) :
	foreach ($readyreview->fetchAll(PDO::FETCH_ASSOC) as $readyreviewrow) :
		$readyreviewdataPoints[] = array('y' => 1, 'label' => $readyreviewrow['Scenario_title']);
	endforeach;
else :
	$readyreviewdataPoints[] = [];
endif;
#---------------END REVIEW FIXED---------------

#--------READY FOR PUBLISHED SIMULATION-----------
$sqlreadypub = "SELECT DISTINCT Scenario_title FROM scenario_master WHERE status = '5'";
$readypub = $db->prepare($sqlreadypub);
if ($readypub->execute() && $readypub->rowCount() > 0) :
	foreach ($readypub->fetchAll(PDO::FETCH_ASSOC) as $readypubrow) :
		$readypubdataPoints[] = array('y' => 1, 'label' => $readypubrow['Scenario_title']);
	endforeach;
else :
	$readypubdataPoints[] = [];
endif;
#--------END READY FOR PUBLISHED SIMULATION--------

#---------------PUBLISH SIMULATION---------------
$sqlpub = "SELECT DISTINCT scenario_id FROM scenario_master WHERE status = '8'";
$pub = $db->prepare($sqlpub); $pub->execute();
$total_publish_sim = $pub->rowCount();
#---------------END PUBLISH SIMULATION----------
?>
<style>
.canvasjs-chart-canvas {
	padding-top: 0px !important;
}

.wordwrap {
	white-space: pre-wrap;
	white-space: -moz-pre-wrap;
	white-space: -pre-wrap;
	white-space: -o-pre-wrap;
	word-wrap: break-word;
}
</style>
<script type="application/javascript">
	function getNum(val) {
		if (isNaN(val)) {
			return 0;
		}
		return val;
	}
	window.onload = function() {
		CanvasJS.addColorSet("perfLastSim", ["#4472c4", "#ed7d31", "#a5a5a5", "#ffc000", "#f7cb2b"]);
		CanvasJS.addColorSet("perfLastSim", ["#3ec1d5", "#ff9a00", "#ef4631", "#ffc000", "#f7cb2b"]);
		//---------------ASSIGNED LEARNERS---------------
		var chart = new CanvasJS.Chart("chartContainer", {
			backgroundColor: "",
			animationEnabled: true,
			theme: "light2",
			axisX: {
				gridThickness: 0,
				tickLength: 0,
				lineThickness: 0,
				labelFontColor: "white",
				labelFormatter: function() {
					return " ";
				}
			},
			axisY: {
				gridThickness: 0,
				tickLength: 0,
				lineThickness: 0,
				labelFormatter: function() {
					return " ";
				}
			},
			toolTip: {
				content: "<div style='max-width:200px;' class='wordwrap'><strong>{label}</strong>: {name}</div>"
			},
			data: [{
				type: "column",
				indexLabel: "{y}",
				indexLabelPlacement: "inside",
				indexLabelFontWeight: "bolder",
				indexLabelFontColor: "white",
				dataPoints: <?php echo json_encode($dataPoints); ?>
			}]
		});
		chart.render();
		var assignedsimsum = 0;
		for (var i = 0; i < chart.options.data[0].dataPoints.length; i++) {
			assignedsimsum += chart.options.data[0].dataPoints[i].y;
		}
		$("#assignedsim").html("Total Assigned Learners: " + getNum(assignedsimsum));
		//--------------END-ASSIGNED LEARNERS-------------

		//---------------PENDING SIMULATION---------------
		var pendingchart = new CanvasJS.Chart("pendingchartContainer", {
			animationEnabled: true,
			data: [{
				type: "pie",
				indexLabel: "{y}",
				indexLabelPlacement: "inside",
				indexLabelFontWeight: "bolder",
				indexLabelFontColor: "white",
				dataPoints: <?php echo json_encode($pendingDataPoints); ?>
			}]
		});
		pendingchart.render();
		$("#pending").html("Total Pending Simulation: " + <?php echo $total_pending_sim ?>);
		//----------END-PENDING-SIMULATION------------

		//---------------NEW-SIMULATION---------------
		var newchart = new CanvasJS.Chart("newchartContainer", {
			theme: "light2",
			colorSet: "perfLastSim",
			animationEnabled: true,
			title: {
				text: "<?php echo $total_new_sim ?>",
				verticalAlign: "center",
				dockInsidePlotArea: true
			},
			data: [{
				type: "doughnut",
				innerRadius: "50%",
				toolTipContent: "{name}",
				dataPoints: <?php echo json_encode($newDataPoints); ?>
			}]
		});
		newchart.render();
		var newsimsum = 0;
		for (var i = 0; i < newchart.options.data[0].dataPoints.length; i++) {
			newsimsum += newchart.options.data[0].dataPoints[i].y;
		}
		$("#newsimsum").html("Total New Simulations: " + getNum(newsimsum));
		//--------------END-NEW-SIMULATION------------

		//--------------REVIEWED SIMULATION------------			
		var reviewedchart = new CanvasJS.Chart("reviewedchartContainer", {
			backgroundColor: "",
			animationEnabled: true,
			theme: "light2",
			data: [{
				type: "funnel",
				indexLabel: "{label}",
				indexLabelPlacement: "inside",
				indexLabelFontWeight: "bolders",
				indexLabelFontColor: "white",
				toolTipContent: "{label}",
				dataPoints: <?php echo json_encode($revieweddataPoints); ?>
			}]
		});
		reviewedchart.render();
		var reviewedsum = 0;
		for (var i = 0; i < reviewedchart.options.data[0].dataPoints.length; i++) {
			reviewedsum += reviewedchart.options.data[0].dataPoints[i].y;
		}
		$("#reviewedchart").html("Total Reviewed Simulations: " + getNum(reviewedsum));
		//-------------END-REVIEWED-SIMULATION----------

		//--------------REVIEW IN PROCESS------------
		var reviewprochart = new CanvasJS.Chart("reviewprodataPoints", {
			backgroundColor: "",
			animationEnabled: true,
			colorSet: "reviewpro",
			theme: "light2",
			data: [{
				type: "pie",
				indexLabel: "{label}",
				toolTipContent: "{label}",
				dataPoints: <?php echo json_encode($reviewprodataPoints); ?>
			}]
		});
		reviewprochart.render();
		var reviewprosum = 0;
		for (var i = 0; i < reviewprochart.options.data[0].dataPoints.length; i++) {
			reviewprosum += reviewprochart.options.data[0].dataPoints[i].y;
		}
		$("#reviewprochart").html("Total Review in Process: " + getNum(reviewprosum));
		//-------------END-REVIEW IN PROCESS----------

		//-------------REVIEW FIXED----------
		var reviewfixedchart = new CanvasJS.Chart("reviewfixedchartContainer", {
			backgroundColor: "",
			animationEnabled: true,
			colorSet: "reviewpro",
			theme: "light2",
			data: [{
				type: "pyramid",
				indexLabel: "{label}",
				indexLabelPlacement: "inside",
				indexLabelFontWeight: "bolders",
				indexLabelFontColor: "white",
				toolTipContent: "{label}",
				dataPoints: <?php echo json_encode($readyreviewdataPoints); ?>
			}]
		});
		reviewfixedchart.render();
		var reviewfixedsum = 0;
		for (var i = 0; i < reviewfixedchart.options.data[0].dataPoints.length; i++) {
			reviewfixedsum += reviewfixedchart.options.data[0].dataPoints[i].y;
		}
		$("#reviewfixedchart").html("Total Review Fixed: " + getNum(reviewfixedsum));
		//-------------END-REVIEW FIXED----------

		//--------------READY FOR PUBLISHED------------			
		var readypubchart = new CanvasJS.Chart("readypubdataPoints", {
			backgroundColor: "",
			animationEnabled: true,
			axisX: {
				gridThickness: 0,
				tickLength: 0,
				lineThickness: 0,
				labelFontColor: "#fff",
				labelAngle: 0
			},
			axisY: {
				gridThickness: 0,
				tickLength: 0,
				lineThickness: 0,
				labelFontColor: "#fff",
				labelFormatter: function() {
					return " ";
				}
			},
			theme: "light2",
			data: [{
				type: "bar",
				indexLabel: "{label}",
				indexLabelPlacement: "inside",
				indexLabelFontWeight: "bolders",
				indexLabelFontColor: "white",
				toolTipContent: "{label}",
				dataPoints: <?php echo json_encode($readypubdataPoints); ?>
			}]
		});
		readypubchart.render();
		var readypubsum = 0;
		for (var i = 0; i < readypubchart.options.data[0].dataPoints.length; i++) {
			readypubsum += readypubchart.options.data[0].dataPoints[i].y;
		}
		$("#readypubchart").html("Total Ready to Publish: " + getNum(readypubsum));
		//-------------END-READY FOR PUBLISHED----------
	}
</script>
<div class="bottomheader">
	<div class="container-fluid">
		<ul class="breadcrumb">
			<?php echo $db->breadcrumbs(); ?>
		</ul>
	</div>
</div>
<div class="author-report">
	<div class="bottomheader1">
		<div class="container-fluid" id="print_data">
			<div class="row">
				<div class="col-sm-8">
					<h3>Overview</h3>
				</div>
				
				<div class="col-sm-1">
					<div class="L-E-text learner_download_btn_Laptop">
						<span class="align-middle">
							<a class="" id="print" title="Download Report" onclick="getPDF('#print_data')"><img class="svg" src="img/list/download_report.svg" /></a>
						</span>
					</div>
				</div>
				<div class="col-sm-3 L-calender GPeducater usermanage-form">
					<select class="form-control" id="report">
						<option selected="selected" hidden>Overview</option>
						<option value="1">Group's Performance</option>
						<option value="2">Systemwide Analytics</option>
					</select>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-4">
					<div class="Authoreport-box">
						<div class="Authoreport-head">
							<h3>TOTAL SIMULATIONS</h3>
						</div>
						<div class="circleChart" style="height: 200px; width: 100%;" data-value="<?php echo $total_assign_sim; ?>" data-text="<?php echo $total_assign_sim; ?>"></div>
						<div>Total Simulations Authored: <?php echo $total_assign_sim; ?></div>
						<p>For detail click <a href="<?php echo $db->getBaseUrl('admin-report-details.php?view=true&type=2') ?>">View report</a></p>
					</div>
				</div>
				<div class="col-sm-4">
					<div class="Authoreport-box">
						<div class="Authoreport-head">
							<h3>PENDING SIMULATIONS</h3>
						</div>
						<div id="pendingchartContainer" style="height: 200px; width: 100%;"></div>
						<div id="pending"></div>
						<p>For detail click <a href="<?php echo $db->getBaseUrl('admin-report-details.php?view=true&type=3') ?>">View report</a></p>
					</div>
				</div>
				<div class="col-sm-4">
					<div class="Authoreport-box">
						<div class="Authoreport-head">
							<h3>NEW SIMULATIONS</h3>
						</div>
						<div id="newchartContainer" style="height: 200px; width: 100%;"></div>
						<div id="newsimsum"></div>
						<p>For detail click <a href="<?php echo $db->getBaseUrl('admin-report-details.php?view=true&type=4') ?>">View report</a></p>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-4">
					<div class="Authoreport-box">
						<div class="Authoreport-head">
							<h3>ASSIGNED LEARNERS</h3>
						</div>
						<div id="chartContainer" style="height: 200px; width: 100%;"></div>
						<div id="assignedsim"></div>
						<p>For detail click <a href="<?php echo $db->getBaseUrl('admin-report-details.php?view=true&type=1') ?>">View report</a></p>
					</div>
				</div>
				<div class="col-sm-4">
					<div class="Authoreport-box">
						<div class="Authoreport-head">
							<h3>READY TO PUBLISH</h3>
						</div>
						<div id="readypubdataPoints" style="height: 200px; width: 100%;"></div>
						<div id="readypubchart"></div>
						<p>For detail click <a href="<?php echo $db->getBaseUrl('admin-report-details.php?view=true&type=8') ?>">View report</a></p>
					</div>
				</div>
				<div class="col-sm-4">
					<div class="Authoreport-box">
						<div class="Authoreport-head">
							<h3>PUBLISHED</h3>
						</div>
						<div class="circleChart" style="height: 200px; width: 100%;" data-value="<?php echo (!empty($total_publish_sim)) ? $total_publish_sim : 0; ?>" data-text="<?php echo (!empty($total_publish_sim)) ? $total_publish_sim : 0; ?>"></div>
						<div>Total Published Simulations: <?php echo (!empty($total_publish_sim)) ? $total_publish_sim : 0; ?></div>
						<p>For detail click <a href="<?php echo $db->getBaseUrl('admin-report-details.php?view=true&type=9') ?>">View report</a></p>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-4">
					<div class="Authoreport-box">
						<div class="Authoreport-head">
							<h3>SIMULATIONS UNDER REVIEW</h3>
						</div>
						<div id="reviewprodataPoints" style="height: 200px; width: 100%;"></div>
						<div id="reviewprochart"></div>
						<p>For detail click <a href="<?php echo $db->getBaseUrl('admin-report-details.php?view=true&type=6') ?>">View report</a></p>
					</div>
				</div>
				<div class="col-sm-4">
					<div class="Authoreport-box">
						<div class="Authoreport-head">
							<h3>SIMULATIONS REVIEWED</h3>
						</div>
						<div id="reviewedchartContainer" style="height: 200px; width: 100%;"></div>
						<div id="reviewedchart"></div>
						<p>For detail click <a href="<?php echo $db->getBaseUrl('admin-report-details.php?view=true&type=5') ?>">View report</a></p>
					</div>
				</div>
				<div class="col-sm-4">
					<div class="Authoreport-box">
						<div class="Authoreport-head">
							<h3>REVIEW FIXED</h3>
						</div>
						<div id="reviewfixedchartContainer" style="height: 200px; width: 100%;"></div>
						<div id="reviewfixedchart"></div>
						<p>For detail click <a href="<?php echo $db->getBaseUrl('admin-report-details.php?view=true&type=7') ?>">View report</a></p>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<style>
	p.circleChart_text {
		font-size: 30px !important;
	}
</style>
<script type="text/javascript">
	$('#report').on('change', function() {
		var id = $(this).val();
		if (id == 1) window.location.href = 'group-performance-report.php';
		if (id == 2) window.location.href = 'learner-system-report.php';
	})
</script>
<script src="content/js/canvasjs.min.js"></script>
<script src="content/js/polyfill.min.js"></script>
<script src="content/js/circleChart/circleChart.es5.min.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
		$(".circleChart").circleChart({
			color: "#05afec",
			backgroundColor: "#4d4d4d",
			background: true,
			size: 200,
			widthRatio: 0.4,
			animate: true,
			textCenter: true,
			textSize: '50px',
			textWeight: "bold",
			relativeTextSize: 10 / 7,
		});
	})
</script>
<script type="text/javascript" src="content/js/jspdf.min.js"></script>
<script type="text/javascript" src="content/js/html2canvas.js"></script>
<script type="text/javascript">
	function getPDF(id) {
		var HTML_Width = $(id).width();
		var HTML_Height = $(id).height();
		var top_left_margin = 15;
		var PDF_Width = HTML_Width + (top_left_margin * 2);
		var PDF_Height = (PDF_Width * 1.5) + (top_left_margin * 2);
		var canvas_image_width = HTML_Width;
		var canvas_image_height = HTML_Height;
		var totalPDFPages = Math.ceil(HTML_Height / PDF_Height) - 1;
		html2canvas($(id)[0], {
			allowTaint: true
		}).then(function(canvas) {
			var imgData = canvas.toDataURL("image/jpeg", 1.0);
			var pdf = new jsPDF('p', 'pt', [PDF_Width, PDF_Height]);
			pdf.addImage(imgData, 'JPG', top_left_margin, top_left_margin, canvas_image_width, canvas_image_height);
			for (var i = 1; i <= totalPDFPages; i++) {
				pdf.addPage(PDF_Width, PDF_Height);
				pdf.addImage(imgData, 'JPG', top_left_margin, -(PDF_Height * i) + (top_left_margin * 4), canvas_image_width, canvas_image_height);
			}
			pdf.save("admin-overview-report.pdf");
		});
	};
</script>
<?php
require_once 'includes/footer.php';
