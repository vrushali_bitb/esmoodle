<?php 
session_start();
if(isset($_SESSION['username'])) {
	$userid	= $_SESSION['userId'];
    $user = $_SESSION['username'];
	$role = $_SESSION['role'];
}
else {
    header('location: index.php');
}
require_once 'config/db.class.php';
require_once 'config/pagination.php';
require_once 'includes/learners_menu.php';
$db 	 	= new DBConnection();
$learner	= "'learner'";
$icon_path	= 'img/type_icon/';
$sqlr 		= "SELECT s.scenario_id, s.Scenario_title, s.created_on, s.start_date, s.end_date, s.assign_author, s.assign_reviewer, s.duration, c.category, st.status_name, t.type_name, t.type_icon FROM scenario_master s LEFT JOIN category_tbl c ON c.cat_id = s.scenario_category LEFT JOIN scenario_status st ON st.sid = s.status LEFT JOIN scenario_type t ON t.st_id = s.scenario_type WHERE s.status = '8' ORDER BY s.scenario_id DESC";
$q 			= $db->prepare($sqlr); $q->execute();
$rowCount 	= $q->rowCount();
$count 		= ($sqlr) ? $rowCount : 0;
$page  		= (isset($_REQUEST['page']) && ! empty($_REQUEST['page'])) ? $_REQUEST['page'] : 1;
$show  		= 15;
$start 		= ($page - 1) * $show;
$pagination	= getPaginationString($count, $show, $page, 1, basename(__FILE__), '?page=');
$sql = $sqlr .= " LIMIT $start, $show";
$results = $db->prepare($sql); ?>
<style>
.row.multi_assign_option, .row.update_multi_assign_option {
    margin-left: 0px;
	margin-right: 0px;
}
.input-group .form-control {
    z-index: 0;
}
.add_more_multi_assign, .edit_remove {
    display: inline-block;
    margin-top: 10px;
    cursor: pointer;
}
</style>
<div class="bottomheader">
    <div class="container-fluid">
        <ul class="breadcrumb">
			<?php echo $db->breadcrumbs(); ?>
        </ul>
     </div>
</div>
<div class="simulation_assignment">
    <div class="container-fluid">
		<div class="row">
			<div class="col-sm-12">
				<!--------------Assign-Simulation--------------->
				<form enctype="multipart/form" name="simulation_assignment_form" id="simulation_assignment_form" method="post">
					<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
						<div class="panel panelCaollapse">
							<div class="panel-heading" role="tab" id="headingOne">
								<h4 class="panel-title">
									<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne" class="collapsed">
										Assign Simulation 
									</a>
								</h4>
							</div>
							<div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
								<div class="panel-body">
									<div class="usermanage-form" id="load_1">
										<div class="row">
											<div class="col-sm-3">
												<div class="form-group form-select">
												<select name="category" id="category" class="form-control" data-placeholder="Category List *">
													<option value="0" selected="selected" disabled="disabled">Select Category *</option>
													<?php foreach ($db->getCategory() as $category): ?>
													<option value="<?php echo $category['cat_id'] ?>"><?php echo ucwords($category['category']) ?></option>
													<?php endforeach; ?>
												</select>
												</div>
											</div>
											<div class="col-sm-3">
												<div class="form-group form-select">
												<select name="scenario[]" id="scenario" class="form-control multiselect-ui selection learner" multiple="multiple" data-placeholder="Simulation List *">
												</select>
												</div>
											</div>
										</div>
										<div class="row multi_assign">
											<div class="row multi_assign_option">
												<div class="col-sm-3">
													<div class="form-group form-select">
													<select name="group[]" id="groups_1" class="form-control groups" data-input-id="1" data-placeholder="Group">
													<option value="0" selected="selected">Select Group</option>
													<?php foreach ($db->getGroup() as $group): ?>
													<option value="<?php echo $group['group_id'] ?>"><?php echo ucwords($group['group_name']) ?></option>
													<?php endforeach; ?>
													</select>
													</div>
												</div>
												<div class="col-sm-3" id="groupLearner_1" style="display:none">
													<div class="form-group form-select" id="groupLearnerList_1"></div>
												</div>
												<div class="col-sm-3">
													<div class="form-group form-select">
														<select name="learner[0][]" id="learner_1" class="form-control multiselect-ui selection learner" multiple="multiple" data-placeholder="Learner List">
														<?php foreach ($db->getIndividualLearner('id, username') as $learner): ?>
														<option value="<?php echo $learner['id'] ?>"><?php echo $learner['username'] ?></option>
														<?php endforeach; ?>
														</select>
													</div>
												</div>
												<div class="col-sm-3">
													<div class="form-group">
														<div class="input-group date form_datetime" data-date-format="yyyy-mm-dd hh:ii:ss p" data-link-field="dtp_input1">
														<input class="form-control" name="start_date[]" id="start_date" type="text" readonly placeholder="Start Date">
														<span class="input-group-addon"><span class="fa fa-calendar"></span></span>
														</div>
													</div>
												</div>
												<div class="col-sm-3">
													<div class="form-group">
														<div class="input-group date form_datetime" data-date-format="yyyy-mm-dd hh:ii:ss p" data-link-field="dtp_input1">
															<input class="form-control" name="end_date[]" id="end_date" type="text" readonly placeholder="End Date">
															<span class="input-group-addon"><span class="fa fa-calendar"></span></span>
														</div>
													</div>
												</div>
												<div class="col-sm-3">
													<div class="form-group">
														<input class="form-control" type="tel" name="attempt_no[]" id="attempt_no" placeholder="Number of attempts" onkeypress="return isNumberKey(event);" min="1">
													</div>
												</div>
												<div class="add_multi_assign"><img src="img/icons/plus.png"></div>
											</div>
										</div>
									</div>
									<div class="usermanage-add clearfix">
										<input type="hidden" name="simulation_assignment" value="1" />
										<button type="submit" name="simulation_assignment_btn" id="simulation_assignment_btn" class="btn btn-primary">Assign Simulation</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</form>
				
				<!--------------Update-Simulation--------------->
				<form enctype="multipart/form" name="simulation_update_form" id="simulation_update_form" method="post">
					<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
						<div class="panel panelCaollapse">
							<div class="panel-heading" role="tab" id="headingOne">
								<h4 class="panel-title">
									<a role="button" class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseOne">
										Edit Assigned Simulation
									</a>
								</h4>
							</div>                    
							<div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
								<div class="panel-body">
									<div class="usermanage-form" id="load_2">
										<div class="row">
											<div class="col-sm-3">
												<div class="form-group form-select">
												<select name="update_category" id="update_category" class="form-control" data-placeholder="Category List *">
													<option value="0" selected="selected" disabled="disabled">Select Category *</option>
													<?php foreach ($db->getCategory() as $category): ?>
													<option value="<?php echo $category['cat_id'] ?>"><?php echo ucwords($category['category']) ?></option>
													<?php endforeach; ?>
												</select>
												</div>
											</div>
											<div class="col-sm-3">
												<div class="form-group form-select">
												<select name="update_scenario" id="update_scenario" class="form-control selection" data-placeholder="Simulation List *">
												<option value="0" selected="selected" disabled="disabled">Select Simulation</option>
												</select>
												</div>
											</div>
										</div>
										<div class="row update_multi_assign">
											<div class="row update_multi_assign_option"></div>
										</div>
									</div>
									<div class="usermanage-add clearfix">
										<input type="hidden" name="update_simulation" value="1" />
										<input type="hidden" id="get_total_assignment" value="0" />
										<button type="submit" name="simulation_update_btn" id="simulation_update_btn" class="btn btn-primary">Edit Assigned Simulation</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</form>
					
				<!--------------Reset-Simulation-Data----------->
				<form enctype="multipart/form" name="reset_data_form" id="reset_data_form" method="post">
					<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
						<div class="panel panelCaollapse">
							<div class="panel-heading" role="tab" id="headingOne">
								<h4 class="panel-title">
									<a role="button" class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseOne">
										Reset Simulation Data
									</a>
								</h4>
							</div>                    
							<div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
								<div class="panel-body">
									<div class="usermanage-form">
										<div class="row">
											<div class="col-sm-4">
												<div class="form-group form-select">
												<select name="reset_category" id="reset_category" class="form-control" data-placeholder="Category List *">
													<option value="0" selected="selected" disabled="disabled">Select Category *</option>
													<?php foreach ($db->getCategory() as $category): ?>
													<option value="<?php echo $category['cat_id'] ?>"><?php echo ucwords($category['category']) ?></option>
													<?php endforeach; ?>
												</select>
												</div>
											</div>
											<div class="col-sm-4">
												<div class="form-group form-select">
												<select name="reset_sim_id" id="reset_sim_id" class="form-control selection" data-placeholder="Simulation List *">
													<option value="0" selected="selected" disabled="disabled">Select Simulation *</option>
												</select>
												</div>
											</div>
											<div class="col-sm-4">
												<div class="form-group form-select">
												<select name="reset_user_id[]" id="reset_user_id" class="form-control multiselect-ui selection" multiple="multiple" data-placeholder="Learner List *">
												</select>
												</div>
											</div>
										</div>
									</div>
									<div class="usermanage-add clearfix">
										<input type="hidden" name="reset_sim_data" value="1" />
										<button type="submit" name="simulation_update_btn" id="simulation_update_btn" class="btn btn-primary">Reset Data</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</form>
				
				<div class="usermanage_main">
					<div class="table-box">
						<div class="tableheader clearfix">
						   <div class="tableheader_left">Simulation List</div>
						</div>
					</div>            
					<div class="usermanage-form1">
						<div class="table-responsive">
							<table class="table table-striped">
								<thead class="Theader">
									<tr>
										<th>Category Name</th>
										<th>Simulation Title</th>
										<th>Creation Date</th>
										<th>Start Date</th>
										<th>End Date</th>
										<th>Author Name</th>
										<th>Reviewer Name</th>
										<th>Total Questions</th>
										<th>Duration in min</th>
										<th>Scenario Type</th>
										<th>Stage</th>
									</tr>
								</thead>
								<tbody>
								<?php if ($results->execute() && $rowCount > 0) {
									foreach ($results->fetchAll(PDO::FETCH_ASSOC) as $row) {
										$type_icon = ( ! empty ($row['type_icon'])) ? $icon_path . $row['type_icon'] : ''; ?>
									<tr>
										<td><?php echo $row['category']; ?></td>
										<td title="<?php echo $row['Scenario_title']; ?>"><?php echo $db->truncateText($row['Scenario_title'], 30); ?></td>
										<td><?php echo $db->dateFormat($row['created_on'], 'd-M-y'); ?></td>
										<td><?php echo $db->dateFormat($row['start_date'], 'd-M-y'); ?></td>
										<td><?php echo $db->dateFormat($row['end_date'], 'd-M-y'); ?></td>
										<td><?php if ( ! empty($row['assign_author'])): ?><a data-toggle="popover" title="View Author" href="<?php echo $db->getBaseUrl('includes/ajax.php?getTooltipData=true&ids='. $row['assign_author']) ?>" onclick="return false"><i class="fa fa-info-circle fa-2x" aria-hidden="true"></i></a><?php endif; ?></td>
										<td><?php if ( ! empty($row['assign_reviewer'])): ?><a data-toggle="popover" title="View Reviewer" href="<?php echo $db->getBaseUrl('includes/ajax.php?getTooltipData=true&ids='. $row['assign_reviewer']) ?>" onclick="return false"><i class="fa fa-info-circle fa-2x" aria-hidden="true"></i></a><?php endif; ?></td>
										<td><?php echo $db->getTotalQuestionsByScenario(md5($row['scenario_id'])); ?></td>
										<td><?php echo $row['duration']; ?></td>
										<td><?php echo ( ! empty($type_icon)) ? '<img src="'.$type_icon.'" class="type_icon" title="'.$row['type_name'].'">' : ''; ?></td>
										<td><strong><?php echo $row['status_name']; ?></strong></td>
									</tr>
									<?php } } else { ?>
									<tr>
										<td colspan="9" style="text-align:center"><strong>No data available in database.</strong></td>
									</tr>
									<?php }  ?>
								</tbody>
						   </table>
						</div>
						<div class="pull-right">
							<nav aria-label="navigation"><?php echo ( ! empty($pagination)) ? $pagination : '&nbsp;'; ?></nav>
						</div>
					 </div>
				  </div>
			</div>
		</div>
	</div>
</div>
<script src="content/js/tooltip.js"></script>
<script type="text/javascript">
function isNumberKey(evt) {
	var charCode = (evt.which) ? evt.which : evt.keyCode
	return !(charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57));
}

$('.learner').multiselect({
	disableIfEmpty: true,
	includeSelectAllOption: true,
	filterPlaceholder: 'Search & Select learner',
	enableCaseInsensitiveFiltering : true,
	enableFiltering: true,
	buttonWidth: '250px',
	maxHeight: 250
});

$('.scenario').multiselect({
	disableIfEmpty: true,
	includeSelectAllOption: true,
	filterPlaceholder: 'Search & Select simulation',
	enableCaseInsensitiveFiltering : true,
	enableFiltering: true,
	buttonWidth: '250px',
	maxHeight: 250
});

$('#reset_user_id').multiselect({
	disableIfEmpty: true,
	includeSelectAllOption: true,
	filterPlaceholder: 'Search & Select',
	enableCaseInsensitiveFiltering : true,
	enableFiltering: true,
	buttonWidth: '250px',
	maxHeight: 250
});

$('#category').on('change', function() {
	var cid = $(this).val();
	if (cid != '')
		$("#simulation_assignment_form").LoadingOverlay("show");
		$('#scenario').empty();
		$('#scenario').multiselect('refresh');
		$.getJSON('includes/ajax.php?getScenario=true&catid='+cid, function(res) {
			$("#simulation_assignment_form").LoadingOverlay("hide", true);
			if (res.success == true) {
				$('#scenario').multiselect('dataprovider', res.data);
			}
			else if (res.success == false) {
				$('#scenario').multiselect('rebuild');
				$('#scenario').multiselect('refresh');
			}
		});
});

$('#update_category').on('change', function() {
	var cid = $(this).val();
	if (cid != '')
		$("#simulation_update_form").LoadingOverlay("show");
		$('#update_scenario,.update_multi_assign').empty();
		$('#get_total_assignment').val('');
		$('#update_scenario').find("option:eq(0)").html("Please wait..");
		$.getJSON('includes/ajax.php?getScenarioUpdate=true&catid='+ cid, function(res) {
			$("#simulation_update_form").LoadingOverlay("hide", true);
			if (res.success == true) {
				$('#update_scenario').append('<option value="" selected="selected" disabled="disabled">Select Simulation</option>');
				$.each(res.data, function (key, val) {
					var option = '<option value="'+val.value+'">'+val.label+'</option>';
					$('#update_scenario').append(option);
				});
			}
			else if (res.success == false) {
				swal({text: 'Assign Simulation data not found.', buttons: false, icon: "warning", timer: 3000});
				$('#update_scenario,.update_multi_assign').empty();
				$('#update_scenario').append('<option value="" selected="selected" disabled="disabled">Select Simulation</option>');
			}
		});
	});

$('#reset_category').on('change', function() {
	var cid = $(this).val();
	if (cid != '')
		$("#reset_data_form").LoadingOverlay("show");
		$('#reset_sim_id,#reset_user_id').empty();
		$('#reset_user_id').multiselect('refresh');
		$('#reset_user_id').multiselect('rebuild');
		$.getJSON('includes/ajax.php?getScenarioUpdate=true&catid='+cid, function(res) {
			$("#reset_data_form").LoadingOverlay("hide", true);
			if (res.success == true) {
				$('#reset_sim_id').append('<option value="0" selected="selected" disabled="disabled">Select Simulation *</option>');
				$.each(res.data, function (key, val) {
					var option = '<option value="'+val.value+'">'+val.label+'</option>';
					$('#reset_sim_id').append(option);
				});
			}
			else if (res.success == false) {
				$('#reset_sim_id').empty().append('<option value="0" selected="selected" disabled="disabled">Select Simulation *</option>');
			}
		});
});

$('#reset_sim_id').on('change', function(){
	var sid = $(this).val();
	if (sid != '')
		$("#reset_data_form").LoadingOverlay("show");
		$('#reset_user_id').empty();
		$('#reset_user_id').multiselect('refresh');
		$.getJSON('includes/ajax.php?getResetSimLearner=true&sim_id='+sid, function(res) {
			$("#reset_data_form").LoadingOverlay("hide", true);
			if (res.success == true) {
				$('#reset_user_id').multiselect('dataprovider', res.user_data);
			}
			else if (res.success == false) {
				$('#reset_user_id').multiselect('rebuild');
				$('#reset_user_id').multiselect('refresh');
			}
		});
});

$('#update_scenario').on('change', function() {
	var edit_sim = $(this).val();
	if (edit_sim) {
		$("#simulation_update_form").LoadingOverlay("show");
		$('.update_multi_assign').empty();
		$.getJSON('includes/ajax.php?getAssignScenarioData=true&sim_id='+ edit_sim, function(res) {
			$("#simulation_update_form").LoadingOverlay("hide", true);
			if (res.success == true) {
				var odata = res.other_data;
				if (odata) {
					if (odata.length > 0) {
						$('#get_total_assignment').val(odata.length);
						var ua = 0;
						$.each(odata, function(key, val) {
							var reshtml = '<input type="hidden" name="update_assignment_id[]" value="'+val.assignment_id+'" />\
							<div class="row update_multi_assign_option">\
							<div class="col-sm-3">\
								<div class="form-group form-select">\
								<select name="update_group[]" id="update_groups" class="form-control groups" data-input-id="'+ua+'" data-placeholder="Group">\
								<option value="0" selected="selected">Select Group</option>\
								<?php foreach ($db->getGroup() as $group): ?>'
								var gid = '<?php echo $group['group_id'] ?>'; 
								if (gid == val.group_id) {
									var gselected = 'selected="selected"';
								}
								else {
									var gselected = '';
								}
								reshtml += '<option value="<?php echo $group['group_id'] ?>" '+gselected+'><?php echo ucwords($group['group_name']) ?></option>\
								<?php endforeach; ?>
								</select>\
								</div>\
							</div>\
							<div class="col-sm-3" id="update_groupLearner_'+ua+'" style="display:none">\
							<div class="form-group form-select" id="update_groupLearnerList_'+ua+'"></div>\
							</div>\
							<div class="col-sm-3">\
								<div class="form-group form-select">\
								<select name="update_learner['+ua+'][]" id="update_learner_'+ua+'" class="form-control multiselect-ui selection learner" multiple="multiple" data-placeholder="Learner List">\
								<?php /*foreach ($db->getIndividualLearner('id, username') as $learner): ?>'
									var search = '<?php echo $learner['id'] ?>';
									var str = val.learner_id;
									var arr = str.split(",");
									if (arr.indexOf(search) !== -1) {
										var lselected = 'selected="selected"';
									}
									else {
										var lselected = '';
									}
									reshtml += '<option value="<?php echo $learner['id'] ?>" '+lselected+'><?php echo $learner['username'] ?></option>\
									<?php endforeach;*/ ?>
									</select>\
								</div>\
							</div>\
							<div class="col-sm-3">\
								<div class="form-group">\
									<div class="input-group date form_datetime" data-date-format="yyyy-mm-dd hh:ii:ss p" data-link-field="dtp_input1">'
									if (val.start_date != '0000-00-00 00:00:00') {
										var sdate = val.start_date;
									}
									else {
										var sdate = '';
									}
									reshtml += '<input class="form-control" name="update_start_date[]" id="update_start_date" type="text" readonly placeholder="Start Date" value="'+sdate+'">\
									<span class="input-group-addon"><span class="fa fa-calendar"></span></span>\
								</div>\
							</div>\
							</div>\
							<div class="col-sm-3">\
								<div class="form-group">\
									<div class="input-group date form_datetime" data-date-format="yyyy-mm-dd hh:ii:ss p" data-link-field="dtp_input1">'
									if (val.end_date != '0000-00-00 00:00:00') {
										var edate = val.end_date;
									}
									else {
										var edate = '';
									}
									reshtml += '<input class="form-control" name="update_end_date[]" id="update_end_date" type="text" readonly placeholder="End Date" value="'+edate+'">\
										<span class="input-group-addon"><span class="fa fa-calendar"></span></span>\
									</div>\
								</div>\
							</div>\
							<div class="col-sm-3">\
								<div class="form-group">\
									<input class="form-control" type="tel" name="update_attempt_no[]" id="update_attempt_no" placeholder="Number of attempts" onkeypress="return isNumberKey(event);" min="1" value="'+val.no_attempts+'">\
								</div></div>'
								if (ua == 0) {
									reshtml += '<img class="add_more_multi_assign" src="img/icons/plus.png">';
								}
								else {
									reshtml += '<img class="edit_remove" data-aid="'+val.assignment_id+'" src="img/icons/minus.png">';
								}
							reshtml += '</div>';
							
							$('.update_multi_assign').append(reshtml);
							
							$('#update_learner_'+ ua).multiselect({
								includeSelectAllOption: true,
								filterPlaceholder: 'Search & select learner',
								enableCaseInsensitiveFiltering : true,
								enableFiltering: true,
								buttonWidth: '250px',
								maxHeight: 250
							});
							ua++;
						});

						$('.update_multi_assign').on('click', '.edit_remove', function(e){
							e.preventDefault();
							var rid = $(this).data('aid');
							$(this).parent().remove();
							ua--;
						});
						
						$("#simulation_update_form").LoadingOverlay("hide", true);
					}
				}
			}
			else if (res.success == false) {
				$("#simulation_update_form").LoadingOverlay("hide", true);
				swal({text: 'Assign Simulation data not found.', buttons: false, icon: "warning", timer: 3000});
				$('#update_scenario,.update_multi_assign').empty();
				$('#update_scenario').append('<option value="" selected="selected" disabled="disabled">Select Simulation</option>');
			}
		});
	}
});

var a = 2;
var l = 1;
$('.add_multi_assign').on('click', function(){
	$('.multi_assign').append('<div class="row multi_assign_option">\
	<div class="col-sm-3">\
		<div class="form-group form-select">\
		<select name="group[]" id="groups" class="form-control groups" data-input-id="'+a+'" data-placeholder="Group">\
		<option value="0" selected="selected">Select Group</option>\
		<?php foreach ($db->getGroup() as $group): ?>
		<option value="<?php echo $group['group_id'] ?>"><?php echo ucwords($group['group_name']) ?></option>\
		<?php endforeach; ?>
		</select>\
		</div>\
	</div>\
	<div class="col-sm-3" id="groupLearner_'+a+'" style="display:none">\
	<div class="form-group form-select" id="groupLearnerList_'+a+'"></div>\
	</div>\
		<div class="col-sm-3">\
		<div class="form-group form-select">\
			<select name="learner['+l+'][]" id="learner_'+a+'" class="form-control multiselect-ui selection learner" multiple="multiple" data-placeholder="Learner List">\
			<?php foreach ($db->getIndividualLearner('id, username') as $learner): ?>
			<option value="<?php echo $learner['id'] ?>"><?php echo $learner['username'] ?></option>\
			<?php endforeach; ?>
			</select>\
		</div>\
	</div>\
	<div class="col-sm-3">\
		<div class="form-group">\
			<div class="input-group date form_datetime" data-date-format="yyyy-mm-dd hh:ii:ss p" data-link-field="dtp_input1">\
			<input class="form-control" name="start_date[]" id="start_date" type="text" readonly placeholder="Start Date">\
			<span class="input-group-addon"><span class="fa fa-calendar"></span></span>\
		</div>\
	</div>\
	</div>\
	<div class="col-sm-3">\
		<div class="form-group">\
			<div class="input-group date form_datetime" data-date-format="yyyy-mm-dd hh:ii:ss p" data-link-field="dtp_input1">\
				<input class="form-control" name="end_date[]" id="end_date" type="text" readonly placeholder="End Date">\
				<span class="input-group-addon"><span class="fa fa-calendar"></span></span>\
			</div>\
		</div>\
	</div>\
	<div class="col-sm-3">\
		<div class="form-group">\
			<input class="form-control" type="tel" name="attempt_no[]" id="attempt_no" placeholder="Number of attempts" onkeypress="return isNumberKey(event);" min="1">\
		</div>\
	</div>\
	<div class="remove SAremove"><img src="img/icons/minus.png"></div>\
	</div>');
	$('#learner_'+ a).multiselect({
		includeSelectAllOption: true,
		filterPlaceholder: 'Search & select learner',
		enableCaseInsensitiveFiltering : true,
		enableFiltering: true,
		buttonWidth: '250px',
		maxHeight: 250
	});
	a++; l++;
});

$('.multi_assign').on('click', '.remove', function(e){
	e.preventDefault();
	$(this).parent().remove();
	a--; l--;
});

jQuery('body').on('click', '.groups', function() {
	var input_id = $(this).data('input-id');
	var id = $(this).val();
	if (id > 0) {
		$.ajax({
			url: 'ajax.php',
			data: 'get_learner='+ true + '&gid='+ id,
			type: 'POST',
			success: function(data) {
				$('#groupLearner_'+ input_id).show();
				$('#groupLearnerList_'+ input_id).html(data);
			}
		});
	}
	else { 
		$('#groupLearner_'+ input_id).css('display', 'none');
		$('#groupLearnerList_'+ input_id).html('');
	}
});

jQuery('body').on('click', '.add_more_multi_assign', function() {
	var edit_total = $('#get_total_assignment').val();
	$('.update_multi_assign').append('<div class="row update_multi_assign_option">\
	<div class="col-sm-3">\
		<div class="form-group form-select">\
		<select name="update_group[]" id="update_group" class="form-control groups" data-input-id="'+edit_total+'" data-placeholder="Group">\
		<option value="0" selected="selected">Select Group</option>\
		<?php foreach ($db->getGroup() as $group): ?>
		<option value="<?php echo $group['group_id'] ?>"><?php echo ucwords($group['group_name']) ?></option>\
		<?php endforeach; ?>
		</select>\
		</div>\
	</div>\
	<div class="col-sm-3" id="update_groupLearner_'+edit_total+'" style="display:none">\
	<div class="form-group form-select" id="update_groupLearnerList_'+edit_total+'"></div>\
	</div>\
		<div class="col-sm-3">\
		<div class="form-group form-select">\
			<select name="update_learner['+edit_total+'][]" id="update_learner_'+edit_total+'" class="form-control multiselect-ui selection learner" multiple="multiple" data-placeholder="Learner List">\
			<?php foreach ($db->getIndividualLearner('id, username') as $learner): ?>
			<option value="<?php echo $learner['id'] ?>"><?php echo $learner['username'] ?></option>\
			<?php endforeach; ?>
			</select>\
		</div>\
	</div>\
	<div class="col-sm-3">\
		<div class="form-group">\
			<div class="input-group date form_datetime" data-date-format="yyyy-mm-dd hh:ii:ss p" data-link-field="dtp_input1">\
			<input class="form-control" name="update_start_date[]" id="update_start_date" type="text" readonly placeholder="Start Date">\
			<span class="input-group-addon"><span class="fa fa-calendar"></span></span>\
		</div>\
	</div>\
	</div>\
	<div class="col-sm-3">\
		<div class="form-group">\
			<div class="input-group date form_datetime" data-date-format="yyyy-mm-dd hh:ii:ss p" data-link-field="dtp_input1">\
				<input class="form-control" name="update_end_date[]" id="update_end_date" type="text" readonly placeholder="End Date">\
				<span class="input-group-addon"><span class="fa fa-calendar"></span></span>\
			</div>\
		</div>\
	</div>\
	<div class="col-sm-3">\
		<div class="form-group">\
			<input class="form-control" type="tel" name="update_attempt_no[]" id="update_attempt_no" placeholder="Number of attempts" onkeypress="return isNumberKey(event);" min="1">\
		</div>\
	</div>\
	<img class="edit_remove" src="img/icons/minus.png"></div>');
	$('#update_learner_'+ edit_total).multiselect({
		includeSelectAllOption: true,
		filterPlaceholder: 'Search & select learner',
		enableCaseInsensitiveFiltering : true,
		enableFiltering: true,
		buttonWidth: '250px',
		maxHeight: 250
	});
	edit_total++;
	$('#get_total_assignment').val(edit_total);
});
</script>
<script type="text/javascript" src="content/js/bootstrap-datetimepicker.js" charset="UTF-8"></script>
<script type="text/javascript">
$('.form_datetime').datetimepicker({
	weekStart: 1,
	todayBtn:  0,
	autoclose: 1,
	todayHighlight: 1,
	startView: 2,
	forceParse: 0,
	showMeridian: 1,
	clearBtn: 1
});	
$('body').on('focus', ".form_datetime", function() {
	$(this).datetimepicker({
		weekStart: 1,
		todayBtn:  0,
		autoclose: 1,
		todayHighlight: 1,
		startView: 2,
		forceParse: 0,
		showMeridian: 1,
		clearBtn: 1
	});
});
</script>
<?php 
require_once 'includes/footer.php';
