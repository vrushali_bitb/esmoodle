<?php 
session_start();
if (isset($_SESSION['username'])) {
	$user	= $_SESSION['username'];
	$role	= $_SESSION['role'];
	$userid	= $_SESSION['userId'];
	$domain = (isset($_SESSION['domain_name'])) ? $_SESSION['domain_name'] : $_SERVER['HTTP_HOST'];
}
else {
    header('location: index.php');
}
require_once 'config/db.class.php';
require_once 'includes/learners_menu.php';
require_once 'config/pagination.php';
$db = new DBConnection();
$sqlr = "SELECT s.*, u.username, u.role FROM support_tbl AS s LEFT JOIN users AS u ON s.user_id = u.id ".(($role != 'admin') ? " WHERE s.user_id = '". $userid ."'" : " ")." ORDER BY s.support_id DESC";
$q = $db->prepare($sqlr); $q->execute();
$rowCount = $q->rowCount();
$count = ($sqlr) ? $rowCount : 0;
$page  = (isset($_REQUEST['page']) && ! empty($_REQUEST['page'])) ? $_REQUEST['page'] : 1;
$show  = 15;
$start = ($page - 1) * $show;
$pagination = getPaginationString($count, $show, $page, 1, basename(__FILE__), '?page=');
$sqlr .= " LIMIT $start, $show";
$results = $db->prepare($sqlr); ?>
<style>
	.tooltip{
		position: absolute;
	}
</style>
<div class="bottomheader">
  <div class="container-fluid">
    <ul class="breadcrumb">
      <?php echo $db->breadcrumbs(); ?>
    </ul>
  </div>
</div>
<div class="Group_managment">
  <div class="container-fluid">
	  <div class="row">
			<div class="col-sm-12">
				<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
				  <div class="panel panelCaollapse">
					<div class="panel-heading" role="tab" id="headingOne">
					  <h4 class="panel-title">
					  <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne" class="collapsed"> Support</a>
					  </h4>
					</div>
					<div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
					  <div class="panel-body">
						<form method="post" name="support_form" id="support_form" enctype="multipart/form-data">
						  <div class="usermanage-form">
							<div class="row">
							  <div class="col-sm-6">
								<div class="form-group">
								  <input type="text" class="form-control" name="support_subject" id="support_subject" required="required" placeholder="Subject">
								</div>
							  </div>
							</div>
							<div class="row">
							  <div class="col-sm-12">
								<div class="form-group">
								  <textarea class="form-control" name="support_email_des" required="required" placeholder="Description"></textarea>
								</div>
							  </div>
							</div>
							<div class="row">
								<div class="col-sm-12">
									<div class="addsce-imgadm">
										<div class="adimgbox">
											<div class="form-group">
												<input type="file" name="file" id="file" class="form-control file">
												<input type="text" class="form-control controls" disabled placeholder="Attachment">
												<input type="hidden" name="support_attachment" id="support_attachment" />
												<div class="browsebtn browsebtntmp">
													<span id="splashImg"></span> &nbsp;&nbsp; <span id="ImgDelete" style="display:none">
													<a href="javascript:void(0);" data-media-file="" class="delete_media_file" title="Delete Attachment">
													<i class="fa fa-times" aria-hidden="true"></i></a></span>
												</div>
											</div>
										</div>
										<div class="adimgbox browsebtn"><button class="browse btn btn-primary" type="button">Browse</button></div>
										<div class="adimgbox browsebtn"><button type="button" name="upload" id="upload" class="btn btn-primary">Uplaod</button></div>
									</div>
								</div>
							</div>
						  </div>
						  <hr class="linebox">
						  <div class="usermanage-add clearfix">
							<input type="hidden" name="support" value="1" />
							<input type="submit" name="support_btn" class="btn btn-primary pull-right PullRcatagry" value="Send">
						  </div>
						</form>
					  </div>
					</div>
				  </div>
				</div>
				<div class="usermanage_main">
					<div class="table-box">
						<div class="tableheader clearfix">
						   <div class="tableheader_left">List of Support Tickets</div>
						   <!--<div class="tableheader_right">
							   <div class="Searchbox"><input type="text" class="serchtext"></div>
							   <div class="sorttext">  <span class="sort">Sort by</span><img  src="img/down_arrow_white.png"></div>
						   </div>-->
						</div>                
					</div>
					<div class="usermanage-form1">
					   <div class="table-responsive">        
							<table class="table table-striped">
								<thead class="Theader">
									<tr>
										<th>#</th>
										<td>Date</td>
										<th>Subject</th>
										<th>Description</th>
										<th>Attachment</th>
										<th>Status</th>
										<?php if ($role == 'admin'): ?>
										<th>User</th>
										<th>Action</th>
										<?php endif; ?>
									</tr>
								 </thead>
								 <tbody>
								 <?php 
								 if ($results->execute() && $rowCount > 0):
									$i = $start + 1;
									foreach ($results->fetchAll(PDO::FETCH_ASSOC) as $row): ?>
								<tr>
									<td><?php echo $i ?></td>
									<td><?php echo $db->dateFormat($row['datetime'], 'd-M-y'); ?></td>
									<td><?php echo $row['subject'] ?></td>
									<td><span data-toggle="tooltip" data-placement="top" title="<?php echo $row['des']; ?>"><?php echo $db->truncateText($row['des'], 40); ?></span></td>
									<td><?php if ( ! empty($row['attachment'])): ?>
										<a href="<?php echo $db->getBaseUrl('scenario/attachment/'. $domain .'/'. $row['attachment']) ?>" target="_blank" title="View Attachment"><i class="fa fa-eye" aria-hidden="true"></i></a>
										<?php endif; ?>
									</td>
									<td><?php echo $db->SupportStatus($row['status']); ?></td>
									<?php if ($role == 'admin'): ?>
									<td><?php echo $row['username'].' ('. $row['role'] .')'; ?></td>
									<td><a class="edit" data-support-id="<?php echo $row['support_id'] ?>" href="javascript:void(0);" title="Edit Status">
									<img class="svg" src="img/list/edit.svg"></a></td>
									<?php endif; ?>
								</tr>
								<?php $i++; endforeach; else: ?>
								<tr><td colspan="7" style="text-align:center"><strong>No data available in database.</strong></td></tr>
								<?php endif; ?>
							   </tbody>
							</table>
						</div>
						<div class="pull-right">
							<nav aria-label="navigation"><?php echo ( ! empty($pagination) ) ? $pagination : '&nbsp;'; ?></nav>
						</div>
					</div>
				 </div>
			</div>
	  </div>
  </div>
</div>
<div id="load_popup_modal_show" class="modal fade" tabindex="-1"></div>
<script type="text/javascript">
$(document).ready(function(){
  $('[data-toggle="tooltip"]').tooltip();   
});
$('#upload').click(function() {
	$.LoadingOverlay("show");
	var file_data = $('#file').prop('files')[0];
	var form_data = new FormData();
	form_data.append('file', file_data);
	form_data.append('support_attachment', true);
	$.ajax({
		url: "includes/support-process.php",
		type: "POST",
		data: form_data,
		contentType: false,
		cache: false,
		processData:false,
		success: function(resdata) {
			var res = $.parseJSON(resdata);
			if (res.success == true) {
				var viewHtml = '<a href="'+ res.path + res.file_name+'" target="_blank" title="View Attachment"><i class="fa fa-eye" aria-hidden="true"></i></a>';
				$('#splashImg').show().html(viewHtml);
				$('#ImgDelete').show();
				$('.delete_media_file').attr('data-media-file', res.file_name);
				$('#support_attachment').val(res.file_name);
				$.LoadingOverlay("hide");
				swal(res.msg, { buttons: false, timer: 1000, });
			}
			else if (res.success == false) {
				$('#support_attachment').val('');
				$('#splashImg').hide('slow');
				$.LoadingOverlay("hide");
				swal("Error", res.msg, "error");
			}
		},error: function() {
			$.LoadingOverlay("hide");
			$('#splashImg').hide('slow');
			swal("Error", 'Oops. something went wrong please try again.', "error");
		}
	});
});
	
$('.delete_media_file').click(function() {
	var file_name   = $(this).attr("data-media-file");
	var dataString	= 'delete='+ true + '&attachment_file='+file_name;
	if (file_name) {
		swal({
			title: "Are you sure?",
			text: "Delete this file.",
			icon: "warning",
			buttons: [true, 'Delete'],
			dangerMode: true, }).then((willDelete) => { if (willDelete) {
				$.LoadingOverlay("show");
				$.ajax({
					url: "includes/delete-file.php",
					type: "POST",
					data: dataString,
					cache: false,
					success: function(resdata) {
						var res = $.parseJSON(resdata);
						if (res.success == false) {
							$.LoadingOverlay("hide");
							swal("Error", res.msg, "error");
							$('#splashImg').hide('slow');
						}
						else if (res.success == true) {
							$.LoadingOverlay("hide");
							$('#splashImg, #ImgDelete').hide('slow');
							$('#support_attachment').val('');
							swal(res.msg, { buttons: false, timer: 1000, });
						}
					},error: function() {
						$.LoadingOverlay("hide");
						swal("Error", 'Oops, something went wrong. Please try again later.?', "error");
					}
				});
			} else { $.LoadingOverlay("hide"); swal("Attachment is safe.!", { buttons: false, timer: 1000, }); }
		});
		}
	});
	
	$('.edit').click(function () {
		var id = $(this).attr('data-support-id');
		if (id != '') {
			$.LoadingOverlay("show");
			var $modal = $('#load_popup_modal_show');
			$modal.load('edit-support-modal.php', {'data-id': id }, function(res) {
				if (res != '') {
					$.LoadingOverlay("hide");
					$modal.modal('show');
				}
				else {
					$.LoadingOverlay("hide");
					$("#showmsg").hide();
					swal("Error", 'Oops. something went wrong please try again.?', "error");
				}
			});
		}
	});
	
	$("#support_form").on('submit', (function(e) {
		e.preventDefault();
		$.LoadingOverlay("show");
		$.ajax({
			url: 'includes/support-process.php',
			type: "POST",
			data: new FormData(this),
			contentType: false,
			cache: false,
			processData:false,
			success: function(resdata) {
				var res = $.parseJSON(resdata);
				if (res.success == true) {
					$.LoadingOverlay("hide");
					swal({text: res.msg, buttons: false, icon: "success", timer: 2000});
					setTimeout(function() { window.location.reload(); }, 3000);
				}
				else if (res.success == false) {
					swal({text: res.msg, buttons: false, icon: "error", timer: 2000});
					$.LoadingOverlay("hide");
				}
			}, error: function() {
				swal({text: 'Oops. something went wrong please try again.', buttons: false, icon: "error", timer: 2000});
				$.LoadingOverlay("hide");
			}
		});
	}));
</script>
<?php 
unset($_SESSION['msg']);
require_once 'includes/footer.php';

