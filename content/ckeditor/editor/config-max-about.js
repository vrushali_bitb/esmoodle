/**
 * @license Copyright (c) 2003-2017, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here. For example:
	config.language = 'en';
	config.uiColor  = '#ffffff';
	config.height   = 80;
	config.toolbarCanCollapse = true;
	
	config.toolbarGroups = [
		{ name: 'document', groups: [ 'mode', 'document', 'doctools' ] },
		{ name: 'clipboard', groups: [ 'clipboard', 'undo' ] },
		{ name: 'styles', groups: [ 'styles' ] },
		{ name: 'colors', groups: [ 'colors' ] },
		{ name: 'tools', groups: [ 'tools' ] },
		{ name: 'others', groups: [ 'others' ] },
		{ name: 'editing', groups: [ 'find', 'selection', 'spellchecker', 'editing' ] },
		{ name: 'forms', groups: [ 'forms' ] },		
		{ name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
		{ name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi', 'paragraph' ] },
		{ name: 'links', groups: [ 'links' ] },
		{ name: 'insert', groups: [ 'insert' ] },		
		{ name: 'about', groups: [ 'about' ] }
	];

	config.removeButtons = 'Source,Save,NewPage,Preview,Print,Templates,Cut,Copy,Paste,PasteText,PasteFromWord,Undo,Redo,Find,Replace,Scayt,SelectAll,About,Maximize,ShowBlocks,BGColor,Styles,Format,Iframe,PageBreak,SpecialChar,Smiley,HorizontalRule,Flash,Strike,Subscript,Superscript,CopyFormatting,RemoveFormat,NumberedList,BulletedList,Outdent,Indent,CreateDiv,Blockquote,BidiLtr,BidiRtl,Language,Anchor,Form,Checkbox,Radio,TextField,Textarea,Select,Button,HiddenField,ImageButton,Table,Image,EasyImageUpload,';
	config.removePlugins = 'flash,youtube,easyimage,cloudservices';
	config.extraPlugins = 'wordcount';
	config.removeDialogTabs = 'link:upload;image:Upload';
	config.wordcount = {
    // Whether or not you want to show the Paragraphs Count
    showParagraphs: true,
    // Whether or not you want to show the Word Count
    showWordCount: true,
    // Whether or not you want to show the Char Count
    showCharCount: true,
    // Whether or not you want to count Spaces as Chars
    countSpacesAsChars: false,
    // Whether or not to include Html chars in the Char Count
    countHTML: false,
    // Maximum allowed Word Count, -1 is default for unlimited
    maxWordCount: -1,
    // Maximum allowed Char Count, -1 is default for unlimited
    maxCharCount: 550,
    // Add filter to add or remove element before counting (see CKEDITOR.htmlParser.filter), Default value : null (no filter)
    filter: new CKEDITOR.htmlParser.filter({
        elements: {
            div: function( element ) {
                if(element.attributes.class == 'mediaembed') {
                    return false;
                }
            }
        }
    })
  };
};
