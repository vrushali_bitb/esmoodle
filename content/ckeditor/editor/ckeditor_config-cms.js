/**
 * @license Copyright (c) 2003-2018, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see https://ckeditor.com/legal/ckeditor-oss-license
 */

/*----------This config for email template-----------*/

CKEDITOR.editorConfig = function( config ) {
	config.language = 'en';
	config.uiColor = '#ffffff';
	config.height = 400;
	config.toolbarCanCollapse = true;
	
	config.toolbarGroups = [
		{ name: 'document', groups: [ 'mode', 'document', 'doctools' ] },
		{ name: 'clipboard', groups: [ 'clipboard', 'undo' ] },
		{ name: 'editing', groups: [ 'find', 'selection', 'spellchecker', 'editing' ] },
		{ name: 'forms', groups: [ 'forms' ] },
		'/',
		{ name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
		{ name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi', 'paragraph' ] },
		{ name: 'links', groups: [ 'links' ] },
		{ name: 'insert', groups: [ 'insert' ] },
		'/',
		{ name: 'styles', groups: [ 'styles' ] },
		{ name: 'colors', groups: [ 'colors' ] },
		{ name: 'tools', groups: [ 'tools' ] },
		{ name: 'others', groups: [ 'others' ] },
		{ name: 'about', groups: [ 'about' ] }
	];
	config.removeButtons = 'Save,NewPage,Find,Replace,SelectAll,Form,Checkbox,Radio,TextField,ImageButton,Textarea,Select,Button,HiddenField,CopyFormatting,RemoveFormat,Language,Flash,PageBreak,Iframe,ShowBlocks,About,Image,EasyImageUpload,';
	config.removePlugins = 'flash';
	config.extraPlugins = 'image';
	var baseUrl = window.location;
	config.filebrowserBrowseUrl			= baseUrl + 'content/ckeditor/ckfinder/ckfinder.html';
    config.filebrowserImageBrowseUrl	= baseUrl + 'content/ckeditor/ckfinder/ckfinder.html?Type=Images';
    config.filebrowserUploadUrl			= baseUrl + 'content/ckeditor/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files';
    config.filebrowserImageUploadUrl	= baseUrl + 'content/ckeditor/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images';
    config.height = '250';
};
