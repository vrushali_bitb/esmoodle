/**
 * @license Copyright (c) 2003-2018, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see https://ckeditor.com/legal/ckeditor-oss-license
 */

/*CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here. For example:
	// config.language = 'fr';
	// config.uiColor = '#AADC6E';
};
*/

CKEDITOR.editorConfig = function( config ) {
	config.language = 'en';
	config.uiColor = '#ffffff';
	config.height = 500;
	config.toolbarCanCollapse = true;
	var baseUrl = window.location;
	config.toolbarGroups = [
		{ name: 'document', groups: [ 'mode', 'document', 'doctools' ] },
		{ name: 'clipboard', groups: [ 'clipboard', 'undo' ] },
		{ name: 'editing', groups: [ 'find', 'selection', 'spellchecker', 'editing' ] },
		{ name: 'forms', groups: [ 'forms' ] },
		{ name: 'styles', groups: [ 'styles' ] },
		{ name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
		{ name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi', 'paragraph' ] },
		{ name: 'links', groups: [ 'links' ] },
		{ name: 'insert', groups: [ 'insert' ] },
		{ name: 'colors', groups: [ 'colors' ] },
		{ name: 'tools', groups: [ 'tools' ] },
		{ name: 'others', groups: [ 'others' ] },
		{ name: 'about', groups: [ 'about' ] }
	];
	config.allowedContent = true;
	config.removeButtons = 'Source,Save,Templates,Find,SelectAll,Replace,NewPage,Preview,Print,Form,Checkbox,Radio,TextField,Textarea,Select,Button,ImageButton,HiddenField,Strike,Superscript,Subscript,CopyFormatting,RemoveFormat,NumberedList,BulletedList,Outdent,Indent,Blockquote,CreateDiv,BidiLtr,BidiRtl,Language,Anchor,Table,HorizontalRule,Smiley,SpecialChar,PageBreak,Iframe,FontSize,Font,Maximize,ShowBlocks,About,Image,EasyImageUpload,';
	config.removePlugins = 'flash';
	config.extraPlugins = 'image,youtube';
	config.filebrowserBrowseUrl			= baseUrl + 'content/ckeditor/ckfinder.html';
    config.filebrowserImageBrowseUrl	= baseUrl + 'content/ckeditor/ckfinder.html?Type=Images';    
    config.filebrowserUploadUrl			= baseUrl + 'content/ckeditor/core/connector/php/connector.php?command=QuickUpload&type=Files';
    config.filebrowserImageUploadUrl	= baseUrl + 'content/ckeditor/core/connector/php/connector.php?command=QuickUpload&type=Images';
	config.cloudServices_tokenUrl 		= baseUrl + '/scenario/upload/'+ baseUrl + '/';
    config.cloudServices_uploadUrl 		= baseUrl + '/scenario/upload/'+ baseUrl + '/';
    config.height = '250';
};
