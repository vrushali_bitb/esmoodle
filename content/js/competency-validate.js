// JavaScript Document

/* Competency Score Validated */

var time 	 = 3000;
var msgtimer = 2000;
var oopsmsg	 = 'Oops. something went wrong please try again.';
var sim_id   = $('#sim_id').val();
if (sim_id != '') {
    $.ajax({
        type: 'POST',
        url: 'includes/process.php',
        data: { checkScore: true, sim_id: sim_id },
        cache: false,
        success: function (resdata) {
            var res = $.parseJSON(resdata);
			if (res.success == true) {
				swal({text: res.msg, buttons: false, icon: "warning"});
			}
        }, error: function () {
            swal({ text: oopsmsg, buttons: false, icon: "error", timer: msgtimer });
        }
    });
}