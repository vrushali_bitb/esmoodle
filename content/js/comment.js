$(document).ready(function(){
	var scenarioID	= $('#scenario_id').val();
	var userID		= $('#user_id').val();
	var idOne 		= '#show_comment';

	function listComment(id, type) {
		$(".addcommentbanner").LoadingOverlay("show");
		$.post("includes/process.php", { simID: scenarioID, getComments: true, getType: type }, function (res) {
			$(".addcommentbanner").LoadingOverlay("hide", true);
			var getdata 	= JSON.parse(res);
			var data    	= getdata['resData'];
			var curUser 	= userID;
			var cdate  		= getdata['cdate'];
			var comments	= "";
			var replies	 	= "";
			var item 		= "";
			var parent 		= -1;
			var results 	= new Array();
			var list 		= $("<ul class='outer-comment'>");
			var item 		= $("<li>").html(comments);
			for (var i = 0; (i < data.length); i++) {
				var commentId = data[i]['comment_id'];
				parent = data[i]['parent_comment_id'];
				if (parent == "0") {
					if (data[i]['fname']) {
						var name = data[i]['fname'];
					}
					else { var name = data[i]['username']; }
					
					if (data[i]['img']) {
						var profile = 'img/profile/'+ data[i]['img'];
					}
					else { var profile = 'img/profile.png'; }

					if (data[i]['fname'] && data[i]['lname']) {
						var full_name = data[i]['fname'] + ' ' + data[i]['lname'];
					}
					else { var full_name = data[i]['username']; }
					
					var commentContainer = $('<div class="comment-row"></div>');
					
					var containerData = '<div class="bulletComment">'+
					'<div class="NameDate"><span>'+ full_name +'</span>'+
					'<span class="Datetext">'+ data[i]['datetime'] +'</span></div>'+
					'<div class="commentbox"><div class="Replyname" style="background:#00bcd4">'+ name +'</div>'+
					'<div class="comment-text">'+ data[i]['comment'] +'</div>';
					if (userID == data[i]['user_id']) {
						containerData += '<span class="remove_chat" data-comment-id="'+data[i]['comment_id']+'" data-parent-comment-id="'+data[i]['parent_comment_id']+'"><i class="fa fa-times" aria-hidden="true"></i></span>';
					}
					containerData += '</div></div>';
					
					commentContainer.append(containerData);
					
					var reply_list = $('<ul class="reply">');
					
					commentContainer.append(reply_list);
					
					listReplies(commentId, data, curUser, cdate, reply_list);
					
					commentContainer.append('<form class="comment-form">'+
						'<div class="Rev-replycomment">'+
						'<input type="hidden" name="scenario_id" id="scenario_id" value="'+ data[i]['scenario_id'] +'">'+
						'<input type="hidden" name="comment_add" value="1">'+
						'<input type="hidden" name="chat_type" value="'+ type +'" />'+
						'<input type="hidden" name="uid" value="'+ data[i]['user_id'] +'">'+
						'<input type="hidden" name="comment_id" id="commentId" value="'+ commentId +'">'+
						'<input type="text" class="form-control comment" name="comment" required="required" placeholder="Add Reply..." aria-describedby="basic-addon2">'+
						'<ul class="msgpostbtn">'+
						'<li class="msgpstgray"><input type="submit" value="Reply"/></li>'+
						'</ul></div></form>');
					var item = $("<li>").append(commentContainer);
					list.append(item);
				}
			}
			$(""+id+"").html(list);
		});
	}
	
	function listReplies(commentId, data, curUser, cdate, list) {
		for (var i = 0; (i < data.length); i++)
		{
			if (commentId == data[i].parent_comment_id)
			{
				if (data[i]['fname']) {
					var name = data[i]['fname'];
				}
				else { var name = data[i]['username']; }
				
				if (data[i]['img']) {
					var profile = 'img/profile/'+data[i]['img'];
				}
				else { var profile = 'img/profile.png'; }

				if (data[i]['fname'] && data[i]['lname']) {
					var full_name = data[i]['fname'] + ' ' + data[i]['lname'];
				}
				else { var full_name = data[i]['username']; }
				
				var comments = '<div class="bulletComment">'+
								'<div class="NameDate"><span>'+ full_name +'</span>'+
								'<span class="Datetext">'+ data[i]['datetime'] +'</span></div>'+
								'<div class="commentbox"><div class="Replyname" style="background:#00bcd4">'+ name +'</div>'+
								'<div class="comment-text">'+ data[i]['comment'] +'</div>';
								if (curUser == data[i]['user_id']) {
									comments += '<span class="remove_chat" data-comment-id="'+data[i]['comment_id']+'" data-parent-comment-id="'+data[i]['parent_comment_id']+'"><i class="fa fa-times" aria-hidden="true"></i></span>';
								}
								comments += '</div></div>';
				var item = $("<li>").html(comments);
				list.append(item);
			}
		}
	}
	/* Submit Comment */
	$('body').on('submit', '.comment-form', function(e) {
		e.preventDefault();
		$.ajax({
			url: 'includes/process.php',
			type: "POST",
			data: new FormData(this),
			contentType: false,
			cache: false,
			processData: false,
			success: function(resdata) {
				$('.comment-form').trigger("reset");
				listComment(idOne, 1);
			},error: function(){
				swal({text: 'Comment not added. Please try again later.', buttons: false, icon: "error", timer: 3000});
			}
		});
	});
	
	/* Remove Comment */
	$('body').on('click', '.remove_chat', function(e) {
		e.preventDefault();
		var commentID		= $(this).attr('data-comment-id');
		var parentCommentID	= $(this).attr('data-parent-comment-id');
		var dataString		= 'remove_chat='+ true + '&commentID='+ commentID+ '&parentCommentID='+ parentCommentID;
		$.ajax({
			url: 'includes/process.php',
			type: 'POST',
			data: dataString,
			cache: false,
			success: function(resdata){
				listComment(idOne, 1);
			},error: function(){
				swal({ text: 'Comment not remove. Please try again later.', buttons: false, icon: "error", timer: 3000});
			}
		});
	});
	
	listComment(idOne, 1);

	$('.load_chat').on('click', function(){
		$(".addcommentbanner").LoadingOverlay("show");
		var cur			= $(this);
		var btn_text 	= cur.html();
		var load_text	= cur.attr('data-loading-text');
		cur.html(load_text);
		setTimeout(function() { 
			listComment(idOne, 1); 
			cur.html(btn_text);
			$(".addcommentbanner").LoadingOverlay("hide", true);
		}, 1000);
	});
});
