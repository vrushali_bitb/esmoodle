function isItIE() {
	user_agent = navigator.userAgent;
	var is_it_ie = user_agent.indexOf("MSIE") > -1 || user_agent.indexOf("Trident/") > -1;
	return is_it_ie;
}

if (isItIE()) {
	alert("Oh snap! We don't support this version of your browser, and neither should you!");
} else {
	//console.log('It is not Internet Explorer');
}

$(document).bind("contextmenu", function (e) {
	return false;
});

$(document).keyup(function (evtobj) {
	if (!(evtobj.altKey || evtobj.ctrlKey || evtobj.shiftKey)) {
		if (evtobj.keyCode == 16) { return false; }
		if (evtobj.keyCode == 17) { return false; }
		//$("body").append(evtobj.keyCode + " ");
	}
});

document.onkeydown = function (e) {
	if (e.ctrlKey &&
		(e.keyCode === 85)) {
		return false;
	}
};

jQuery(document).ready(function () {
	$('body').on('click', '.Bran-panel-videoT', function () {
		if ($(this).hasClass('act')) {
			$(this).removeClass('act');
			$('.panel-collapseV').slideDown(500);
			$('.Linr_mulVid_baner').removeClass('Linr_mulVid_baneradd');
		}
		else {
			$(this).addClass('act');
			$('.panel-collapseV').slideUp(500);
			$('.Linr_mulVid_baner').addClass('Linr_mulVid_baneradd');
		}
	});

	$('body').on('click', '.panel-Mult_videoT', function () {
		if ($(this).hasClass('act')) {
			$(this).removeClass('act');
			$('.panel-collapseV').slideDown(500);
			$('.Linr_mulVid_baner').removeClass('Linr_mulVid_baneradd');
		}
		else {
			$(this).addClass('act');
			$('.panel-collapseV').slideUp(500);
			$('.Linr_mulVid_baner').addClass('Linr_mulVid_baneradd');
		}
	});


	$(".dataTables_paginate.paging_simple_numbers").insertAfter(".table-responsive.AdminReportSys");
	$(".dataTables_info").insertBefore(".dataTables_paginate.paging_simple_numbers");
	$('.dt-buttons').insertBefore('.table-responsive.AdminReportSys');


	$("#change_password").click(function () {
		$(".menu").css('display', 'none');
		$(".toggleIcon.menuopen").removeClass('on')
	});

	jQuery(".OneCharTab").click(function () {
		jQuery(".OneChar").show();
		jQuery(".TwoChar").hide();
		jQuery(".row.back-margin.onec").show();
	})
	jQuery(".TwoCharTab").click(function () {
		jQuery(".TwoChar").show();
		jQuery(".OneChar").hide();
		jQuery(".row.back-margin.onec").show();
	})
	jQuery(".NoCharTab").click(function () {
		jQuery(".TwoChar").hide();
		jQuery(".OneChar").hide();
		jQuery(".row.back-margin.onec").hide();
	})
	jQuery(".menuopen").click(function () {
		jQuery(this).toggleClass("on");
		jQuery(".menu").toggle();
	});
	jQuery(".shorttogle").click(function () {
		jQuery(".menushorting").toggle();
	});
	jQuery(".shorttogle").click(function () {
		jQuery(".shorttogle").toggleClass("downafter");
	});

	jQuery(".sendpop").click(function () {
		jQuery(".send").toggle();
	});


	jQuery(".tial").click(function () {
		jQuery(".maincardbox .col-md-3").addClass("cardbox100");
		jQuery(".imgbox").addClass("imgbox100");
		jQuery(".video_img").addClass("video_img100");
		jQuery(".gridcard").addClass("gridcard100");
		jQuery(".cardbox ").addClass("clearfix");
		jQuery(".scenarioimg ").addClass("scenarioimg100");
	});
	jQuery(".tial").click(function () {
		jQuery(".tial100").show();
		jQuery(".tial").hide();
	});
	jQuery(".tial100").click(function () {
		jQuery(".tial").show();
		jQuery(".tial100").hide();
	});

	jQuery(".tial100").click(function () {
		jQuery(".maincardbox .col-md-3").removeClass("cardbox100");
		jQuery(".imgbox").removeClass("imgbox100");
		jQuery(".video_img").removeClass("video_img100");
		jQuery(".gridcard").removeClass("gridcard100");
		jQuery(".cardbox ").removeClass("clearfix");
		jQuery(".scenarioimg ").removeClass("scenarioimg100");

	});

	jQuery(".pie").click(function () {
		jQuery("#chartContainer").hide();
		jQuery("#chartContainerbar").hide();
		jQuery("#chartContainerspline").hide();
		jQuery("#chartContainerpie").show();
	});

	jQuery(".line").click(function () {
		jQuery("#chartContainer").hide();
		jQuery("#chartContainerbar").hide();
		jQuery("#chartContainerspline").show();
		jQuery("#chartContainerpie").hide();
	});

	jQuery(".bar").click(function () {
		jQuery("#chartContainer").hide();
		jQuery("#chartContainerbar").show();
		jQuery("#chartContainerspline").hide();
		jQuery("#chartContainerpie").hide();
	});

	jQuery(".dotsmenu").click(function () {
		jQuery(".dotsmenu").toggleClass("dotsmenu1");
	})

	jQuery(function ($) {
		jQuery('.edit-profile-icon').on('click', function (e) {
			e.preventDefault();
			jQuery('#proFileImg')[0].click();
		});
	});

});

$(function () {
	$(".shorttogle1").on("click", function (evt) {
		if ($("select").attr("size") == 5) {
			$("select").attr("size", 1);
		} else {
			$("select").attr("size", 5);
			$("select").focus();
		}
	});

	$("select option").on({
		click: function () {
			$("select").attr('size', 1);
		},
		mouseenter: function () {
			$(this).css({ background: '#498BFC', color: '#fff' });
		},
		mouseleave: function () {
			$(this).css({ background: '#fff', color: '#000' });
		}
	});

});

$(document).on('click', '.browse', function () {
	var file = $(this).parent().parent().parent().find('.file');
	file.trigger('click');
});

$(document).on('change', '.file', function () {
	$(this).parent().find('.controls').val($(this).val().replace(/C:\\fakepath\\/i, ''));
});

jQuery(".inner li").click(function (e) {
	e.stopPropagation();
	jQuery(this).children('.inner').toggle();
});

function openNav() {
	document.getElementById("question_tree_menu").style.width = "calc(50% - 56px)";
	document.getElementById("question_tree_body").style.marginLeft = "calc(51% - 40px)";
}

function openNav2() {
	document.getElementById("question_tree_menu2").style.width = "calc(100% - 50px)";
}
function openNav1() {
	document.getElementById("question_tree_menu1").style.width = "calc(100% - 50px)";
}

function closeNav() {
	var x = document.getElementById("question_tree_menu");
	var y = document.getElementById("question_tree_body");
	if (x.style.width === "calc(50% - 56px)") {
		x.style.width = "0px";
		y.style.marginLeft = "-3px";
	} else {
		x.style.width = "calc(50% - 56px)";
		y.style.marginLeft = "calc(51% - 40px)";
	}
}

jQuery('img.svg').each(function () {

	var $img = jQuery(this);

	var imgID = $img.attr('id');

	var imgClass = $img.attr('class');

	var imgURL = $img.attr('src');

	jQuery.get(imgURL, function (data) {

		// Get the SVG tag, ignore the rest

		var $svg = jQuery(data).find('svg');

		// Add replaced image's ID to the new SVG

		if (typeof imgID !== 'undefined') {

			$svg = $svg.attr('id', imgID);

		}

		// Add replaced image's classes to the new SVG

		if (typeof imgClass !== 'undefined') {

			$svg = $svg.attr('class', imgClass + ' replaced-svg');

		}

		// Remove any invalid XML tags as per http://validator.w3.org

		$svg = $svg.removeAttr('xmlns:a');

		// Replace image with new SVG

		$img.replaceWith($svg);


	}, 'xml');


});/*]]>*/

