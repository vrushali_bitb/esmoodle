// JavaScript Document
var time 		= 3000;
var msgtimer	= 2000;
var oopsmsg		= 'Oops. something went wrong please try again.';

//------------------Two-Characters------------------
$('body').on('click', '.2char', function() {
	var check 	 = $(".2char:input:checked").length
	var value 	 = $(this).val();
	var RightIMG = $('.RightIMG').length;
	if ($(this).prop("checked") == true){
		$('.1char').prop('checked', false);
		if (check == 1) {
			if (value.indexOf('|') != -1) {
				var lData = value.split("|");
				$(this).val(lData[0]+'|L');
			}
			else {
				$(this).val(value+'|L');
			}
			$(this).after('<span class="leftIMG" title="Left side character">L</span>');
		}
		else if (check == 2) {
			if (RightIMG == 1) {
				if (value.indexOf('|') != -1) {
					var lData = value.split("|");
					$(this).val(lData[0]+'|L');
				}
				else {
					$(this).val(value+'|L');
				}					
				$(this).after('<span class="leftIMG" title="Right side character">L</span>');
			}
			else {
				if (value.indexOf('|') != -1) {
					var rData = value.split("|");
					$(this).val(rData[0]+'|R');
				}
				else {
					$(this).val(value+'|R');
				}
				$(this).after('<span class="RightIMG" title="Right side character">R</span>');
			}
		}
		else if (check > 2) {
			$(this).prop("checked", false);
			$(this).parent().find('.leftIMG, .RightIMG').remove();
			if (value.indexOf('|') != -1) {
				var data = value.split("|");
				$(this).val(data[0]);
			}
			swal({text: 'Select only two characters.', buttons: false, icon: "warning", timer: time});
		}
	}
	else if ($(this).prop("checked") == false) {
		if (value.indexOf('|') != -1) {
			var data = value.split("|");
			$(this).val(data[0]);
		}
		$(this).parent().find('.leftIMG, .RightIMG').remove();
	}		
});

$('body').on('click', '.1char', function() {
	if ($(this).prop("checked") == true) {
		$('.2char').prop('checked', false);
		$('.bluecheckbox').find('.leftIMG, .RightIMG').remove();
	}
});

$('body').on('click', '.NoCharTab', function() {
	if ($(this).prop("checked") == true) {
		$('#character_type').val(1);
		$('.1char,.2char').prop('checked', false);
		$('.bluecheckbox').find('.leftIMG, .RightIMG').remove();
	}
});
//-----------------End-Two-Characters------------------

$("#show_hide_password a").on('click', function(event) {
	event.preventDefault();
	if($('#show_hide_password input').attr("type") == "text"){
		$('#show_hide_password input').attr('type', 'password');
		$('#show_hide_password i').addClass( "fa-eye-slash");
		$('#show_hide_password i').removeClass( "fa-eye");
	}else if($('#show_hide_password input').attr("type") == "password"){
		$('#show_hide_password input').attr('type', 'text');
		$('#show_hide_password i').removeClass( "fa-eye-slash");
		$('#show_hide_password i').addClass( "fa-eye");
	}
});

$("#show_hide_password1 a").on('click', function(event) {
	event.preventDefault();
	if($('#show_hide_password1 input').attr("type") == "text"){
		$('#show_hide_password1 input').attr('type', 'password');
		$('#show_hide_password1 i').addClass( "fa-eye-slash");
		$('#show_hide_password1 i').removeClass( "fa-eye");
	} else if($('#show_hide_password1 input').attr("type") == "password"){
		$('#show_hide_password1 input').attr('type', 'text');
		$('#show_hide_password1 i').removeClass( "fa-eye-slash");
		$('#show_hide_password1 i').addClass( "fa-eye");
	}
});

$('.publishXML').click(function() {
	var id = $(this).attr('data-scenario-id');
	if (id != '') {
		$.LoadingOverlay("show");
		var dataXml = 'xml='+ true + '&scenario_id='+ id;
		$.ajax({
			type: 'POST',
			url: 'scenario-xml.php',
			data: dataXml,
			cache: false,
			success: function(resdata) {
				var res = $.parseJSON(resdata);
				$.LoadingOverlay("hide");
				if (res.success == true) {
					swal({text: res.msg, buttons: false, icon: "success", timer: msgtimer});
				}
				else if (res.success == false) {
					swal({text: res.msg, buttons: false, icon: "error", timer: msgtimer});
				}
				setTimeout(function() { window.location.reload(); }, time);
			}, error: function() {
				$.LoadingOverlay("hide");
				swal({text: oopsmsg, buttons: false, icon: "error", timer: msgtimer });
			}
		});
	}
});

$('#file_image').change(function() {
	$('#splashImg').show().html(' <img src="scenario/img/loader.gif"> Please wait....');
	var file_data = $('#file_image').prop('files')[0];
	var form_data = new FormData();
	form_data.append('file', file_data);
	form_data.append('splash_img', true);
	$.ajax({
		url: "includes/upload.php",
		type: "POST",
		data: form_data,
		contentType: false,
		cache: false,
		processData:false,
		success: function(resdata) {
			var res = $.parseJSON(resdata);
			if (res.success == true) {
				var viewHtml = '<a href="scenario/upload/'+res.img_name+'" target="_blank" title="View Image"><i class="fa fa-eye" aria-hidden="true"></i></a>';
				$('#splashImg').html(viewHtml);
				$('#ImgDelete').show();
				$('.delete_image').attr('data-img-name', res.img_name);
				swal({text: res.msg, buttons: false, icon: "success", timer: msgtimer});
				$('#splash_image_name').val(res.img_name);
			}
			else if (res.success == false) {
				$('#splash_image_name').val('');
				swal({text: res.msg, buttons: false, icon: "error", timer: msgtimer});
				$('#splashImg').hide('slow');
			}
		}, error: function() {
			$('#splashImg').hide('slow');
			swal({text: oopsmsg, buttons: false, icon: "error", timer: msgtimer });
		}
	});
});

$('.delete_image').on('click', function () {
	var img_name	= $(this).attr("data-img-name");
	var dataString	= 'delete='+ true + '&file='+img_name;
	if (img_name) {
		swal({
			title: "Are you sure?",
			text: "Delete this image.",
			icon: "warning",
			buttons: [true, 'Delete'],
			dangerMode: true, }).then((willDelete) => { if (willDelete) {
				$('#splashImg').show().html(' <img src="scenario/img/loader.gif"> Please wait....');
				$.ajax({
					url: "includes/delete-file.php",
					type: "POST",
					data: dataString,
					cache: false,
					success: function(resdata) {
						var res = $.parseJSON(resdata);
						if (res.success == false) {
							$('#splashImg').hide('slow');
							swal({text: res.msg, buttons: false, icon: "error", timer: msgtimer});
						}
						else if (res.success == true) {
							$('#splashImg, #ImgDelete').hide('slow');
							$('#splash_image_name').val('');
							swal({text: res.msg, buttons: false, icon: "success", timer: msgtimer});
						}
					},error: function() {
						swal({text: oopsmsg, buttons: false, icon: "error", timer: msgtimer });
					}
				});
				} else { swal({text: 'Your image is safe', buttons: false, timer: msgtimer }); }
			});
	}
});

$('#type_icon').change(function() {
	$('#splashImg').show().html('<img src="scenario/img/loader.gif"> Please wait....');
	var file_data = $('#type_icon').prop('files')[0];
	var form_data = new FormData();
	form_data.append('file', file_data);
	form_data.append('type_icon_img', true);
	$.ajax({
		url: "includes/upload.php",
		type: "POST",
		data: form_data,
		contentType: false,
		cache: false,
		processData:false,
		success: function(resdata) {
			var res = $.parseJSON(resdata);
			if (res.success == true) {
				var viewHtml = '<a href="img/type_icon/'+res.img_name+'" target="_blank" title="View Icon"><i class="fa fa-eye" aria-hidden="true"></i></a>';
				$('#splashImg').html(viewHtml);
				$('#ImgDelete').show();
				$('.delete_type_icon').attr('data-img-name', res.img_name);
				swal({text: res.msg, buttons: false, icon: "success", timer: msgtimer});
				$('#type_icon_name').val(res.img_name);
			}
			else if (res.success == false) {
				$('#type_icon').val('');
				swal({text: res.msg, buttons: false, icon: "error", timer: msgtimer});
				$('#splashImg').hide('slow');
			}
		}, error: function() {
			$('#splashImg').hide('slow');
			swal({text: oopsmsg, buttons: false, icon: "error", timer: msgtimer });
		}
	});
});

$('.delete_type_icon').on('click', function () {
	var img_name	= $(this).attr("data-img-name");
	var dataString	= 'delete='+ true +'&type_icon='+ img_name;
	if (img_name) {
		swal({
			title: "Are you sure?",
			text: "Delete this Icon.",
			icon: "warning",
			buttons: [true, 'Delete'],
			dangerMode: true, }).then((willDelete) => { if (willDelete) {
				$('#splashImg').show().html(' <img src="scenario/img/loader.gif"> Please wait....');
				$.ajax({
					url: "includes/delete-file.php",
					type: "POST",
					data: dataString,
					cache: false,
					success: function(resdata) {
						var res = $.parseJSON(resdata);
						if (res.success == false) {
							swal({text: res.msg, buttons: false, icon: "error", timer: msgtimer});
							$('#splashImg').hide('slow');
						}
						else if (res.success == true) {
							$('#splashImg, #ImgDelete').hide('slow');
							$('#type_icon_name').val('');
							swal({text: res.msg, buttons: false, icon: "success", timer: msgtimer});
						}
					},error: function() {
						swal({text: oopsmsg, buttons: false, icon: "error", timer: msgtimer });
					}
				});
			} else { swal({text: "Your Icon is safe.!", buttons: false, timer: msgtimer }); }
		});
	}
});

$('#group_icon').change(function() {
	$.LoadingOverlay("hide");
	var file_data = $('#group_icon').prop('files')[0];
	var form_data = new FormData();
	form_data.append('file', file_data);
	form_data.append('group_icon_img', true);
	$.ajax({
		url: "includes/upload.php",
		type: "POST",
		data: form_data,
		contentType: false,
		cache: false,
		processData:false,
		success: function(resdata) {
			$.LoadingOverlay("hide");
			var res = $.parseJSON(resdata);
			if (res.success == true) {
				var viewHtml = '<a href="img/group_icon/'+res.img_name+'" target="_blank" title="View Icon"><i class="fa fa-eye" aria-hidden="true"></i></a>';
				$('#splashImg').show().html(viewHtml);
				$('#ImgDelete').show();
				$('.delete_group_icon').attr('data-img-name', res.img_name);
				swal({text: res.msg, buttons: false, icon: "success", timer: msgtimer});
				$('#group_icon_name').val(res.img_name);
			}
			else if (res.success == false) {
				$('#group_icon').val('');
				swal({text: res.msg, buttons: false, icon: "error", timer: msgtimer});
			}
		}, error: function() {
			$.LoadingOverlay("hide");
			swal({text: oopsmsg, buttons: false, icon: "error", timer: msgtimer});
		}
	});
});

$('.delete_group_icon').on('click', function () {
	var img_name	= $(this).attr("data-img-name");
	var dataString	= 'delete='+true+'&group_icon='+img_name;
	if (img_name) {
		swal({
			title: "Are you sure?",
			text: "Delete this Icon.",
			icon: "warning",
			buttons: [true, 'Delete'],
			dangerMode: true, }).then((willDelete) => { if (willDelete) {
				$.LoadingOverlay("show");
				$.ajax({
					url: "includes/delete-file.php",
					type: "POST",
					data: dataString,
					cache: false,
					success: function(resdata) {
						$.LoadingOverlay("hide");
						var res = $.parseJSON(resdata);
						if (res.success == false) {
							swal({text: res.msg, buttons: false, icon: "error", timer: msgtimer});
						}
						else if (res.success == true) {
							$('#ImgDelete').hide('slow');
							$('#group_icon_name').val('');
							swal({text: res.msg, buttons: false, icon: "success", timer: msgtimer});
						}
					},error: function() {
						$.LoadingOverlay("hide");
						swal({text: oopsmsg, buttons: false, icon: "error", timer: msgtimer});
					}
				});
			} else { 
				$.LoadingOverlay("hide");
				swal({text: 'Your Icon is safe', buttons: false, timer: msgtimer});
			}
		});
	}
});

$("#NewCategory_form").on('submit', (function(e) {
	e.preventDefault();
	$('#create').attr('disabled', 'disabled').html('<i class="fa fa-refresh fa-spin fa-fw"></i><span class="sr-only">Loading...</span> Please wait....');
	$.ajax({
		url: 'includes/process.php',
		type: "POST",
		data: new FormData(this),
		contentType: false,
		cache: false,
		processData:false,
		success: function(resdata) {
			var res = $.parseJSON(resdata);
			if (res.success == true) {
				swal({text: res.msg, buttons: false, icon: "success", timer: msgtimer});
				setTimeout(function() { window.location.reload(); }, time);
			}
			else if (res.success == false) {
				swal({text: res.msg, buttons: false, icon: "error", timer: msgtimer});
				$('#create').html('<img src="img/icons/plus.png" class="plusimg">').removeAttr('disabled', true);
			}
		}, error: function() {
			swal({text: oopsmsg, buttons: false, icon: "error", timer: msgtimer });
			$('#create').html('<img src="img/icons/plus.png" class="plusimg">').removeAttr('disabled', true);
		}
	});
}));

$("#create_simulation_form").on('submit', (function(e) {
	e.preventDefault();
	$('#Create_Sim').attr('disabled', 'disabled').html('<i class="fa fa-refresh fa-spin fa-fw"></i><span class="sr-only">Loading...</span> Please wait....');
	$.ajax({
		url: 'includes/process.php',
		type: "POST",
		data: new FormData(this),
		contentType: false,
		cache: false,
		processData:false,
		success: function(resdata) {
			var res = $.parseJSON(resdata);
			if (res.success == true) {
				swal({text: res.msg, buttons: false, icon: "success", timer: msgtimer});
				setTimeout(function() { window.location.reload(); }, time);
			}
			else if (res.success == false) {
				swal({text: res.msg, buttons: false, icon: "error", timer: msgtimer});
				$('#Create_Sim').html('Create Simulation').removeAttr('disabled', true);
			}
		}, error: function() {
			swal({text: oopsmsg, buttons: false, icon: "error", timer: msgtimer });
			$('#Create_Sim').html('Create Simulation').removeAttr('disabled', true);
		}
	});
}));

$("#simulation_assignment_form").on('submit', (function(e) {
	e.preventDefault();
	$('#simulation_assignment_btn').attr('disabled', 'disabled').html('<i class="fa fa-refresh fa-spin fa-fw"></i><span class="sr-only">Loading...</span> Please wait....');
	$.ajax({
		url: 'includes/process.php',
		type: "POST",
		data: new FormData(this),
		contentType: false,
		cache: false,
		processData:false,
		success: function(resdata) {
			var res = $.parseJSON(resdata);
			if (res.success == true) {
				swal({text: res.msg, buttons: false, icon: "success", timer: msgtimer});
				setTimeout(function() { window.location.reload(); }, time);
			}
			else if (res.success == false) {
				swal({text: res.msg, buttons: false, icon: "error", timer: msgtimer});
				$('#simulation_assignment_btn').html('Assign Simulation').removeAttr('disabled', true);
			}
		}, error: function() {
			swal({text: oopsmsg, buttons: false, icon: "error", timer: msgtimer });
		}
	});
}));

$("#simulation_update_form").on('submit', (function(e) {
	e.preventDefault();
	$('#simulation_update_btn').attr('disabled', 'disabled').html('<i class="fa fa-refresh fa-spin fa-fw"></i><span class="sr-only">Loading...</span> Please wait....');
	$.ajax({
		url: 'includes/process.php',
		type: "POST",
		data: new FormData(this),
		contentType: false,
		cache: false,
		processData:false,
		success: function(resdata) {
			var res = $.parseJSON(resdata);
			if (res.success == true) {
				swal({text: res.msg, buttons: false, icon: "success", timer: msgtimer});
				setTimeout(function() { window.location.reload(); }, time);
			}
			else if (res.success == false) {
				swal({text: res.msg, buttons: false, icon: "error", timer: msgtimer});
				$('#simulation_update_btn').html('Unassigned Simulation').removeAttr('disabled', true);
			}
		}, error: function() {
			swal({text: oopsmsg, buttons: false, icon: "error", timer: msgtimer });
			$('#simulation_update_btn').html('Unassigned Simulation').removeAttr('disabled', true);
		}
	});
}));

$("#template_form").on('submit', (function(e) {
	e.preventDefault();
	$('#create_sim_template').attr('disabled', 'disabled').html('<i class="fa fa-refresh fa-spin fa-fw"></i><span class="sr-only">Loading...</span> Please wait....');
	$.ajax({
		url: 'includes/process.php',
		type: "POST",
		data: new FormData(this),
		contentType: false,
		cache: false,
		processData:false,
		success: function(resdata) {
			var res = $.parseJSON(resdata);
			if (res.success == true) {
				swal({text: res.msg, buttons: false, icon: "success", timer: msgtimer});
				setTimeout(function() { window.location.href = res.return_url; }, time);
			}
			else if (res.success == false) {
				swal({text: res.msg, buttons: false, icon: "error", timer: msgtimer});
				$('#create_sim_template').html('Create Simulation').removeAttr('disabled', true);
			}
		}, error: function() {
			swal({text: oopsmsg, buttons: false, icon: "error", timer: msgtimer });
		}
	});
}));

$("#add_simulation_from").on('submit', (function(e) {
	for (instance in CKEDITOR.instances) { CKEDITOR.instances[instance].updateElement(); }
	e.preventDefault();
	$('#upload').attr('disabled', 'disabled').html('<i class="fa fa-refresh fa-spin fa-fw"></i><span class="sr-only">Loading...</span> Please wait....');
	$.ajax({
		url: 'includes/process.php',
		type: "POST",
		data: new FormData(this),
		contentType: false,
		cache: false,
		processData:false,
		success: function(resdata) {
			var res = $.parseJSON(resdata);
			if (res.success == true) {
				swal({text: res.msg, buttons: false, icon: "success", timer: msgtimer});
				setTimeout(function() { window.location.href = res.return_url; }, time);
			}
			else if (res.success == false) {
				swal({text: res.msg, buttons: false, icon: "error", timer: msgtimer});
				$('#upload').html('Create Simulation').removeAttr('disabled', true);
			}
		}, error: function() {
			swal({text: oopsmsg, buttons: false, icon: "error", timer: msgtimer});
			$('#upload').html('Create Simulation').removeAttr('disabled', true);
		}
	});
}));

$("#add_sim_linear_details_form").on('submit', (function(e) {
	e.preventDefault();
	$.LoadingOverlay("show");
	$.ajax({
		url: 'includes/process.php',
		type: "POST",
		data: new FormData(this),
		contentType: false,
		cache: false,
		processData:false,
		success: function(resdata) {
			var res = $.parseJSON(resdata);
			if (res.success == true) {
				if (res.all_update == true) {
					swal('', res.msg, "success", { closeOnClickOutside: false,
						buttons: {
							catch: { text: "Preview", value: "preview", closeModal: false },
							ok: true, } } ).then((value) => {
								switch (value) {
									case "ok":
										setTimeout(function() { window.location.href = res.return_url }, 100);
									break;
									case "preview":
										var id = $('#scenario_id').val();
										if (id != '') {
											var dataXml = 'xml='+true+'&scenario_id='+id;
											$.ajax({
												type: 'POST',
												url: 'scenario-xml.php',
												data: dataXml,
												cache: false,
												success: function(resxmldata) {
													var respreview = $.parseJSON(resxmldata);
													if (respreview.succes == true) {
														setTimeout(function() { window.location.href = res.return_url }, 100);
														window.open(respreview.auth_preview_url, '_blank');
													}
													else if (respreview.succes == false) {
														$.LoadingOverlay("hide");
														swal({text: respreview.msg, buttons: false, icon: "error", timer: msgtimer});
													}
												},error: function() {
													$.LoadingOverlay("hide");
													swal({text: oopsmsg, buttons: false, icon: "error", timer: msgtimer });
												}
											});
										}
									break;
									default:
										setTimeout(function() { window.location.href = res.return_url }, 100);
								}
							});
						}
				else {
					setTimeout(function() { window.location.href = res.return_url }, 800);
				}
				$.LoadingOverlay("hide");
			}
			else if (res.success == false) {
				$.LoadingOverlay("hide");
				swal({text: res.msg, buttons: false, icon: "error", timer: msgtimer});
			}
		}
	});
}));

$("#add_sim_branching_details_form").on('submit', (function(e) {
	e.preventDefault();
	$.LoadingOverlay("show");
	$.ajax({
		url: 'includes/process.php',
		type: "POST",
		data: new FormData(this),
		contentType: false,
		cache: false,
		processData:false,
		success: function(resdata) {
			var res = $.parseJSON(resdata);
			if (res.success == true) {
				$.LoadingOverlay("hide");
				if (res.all_update == true) {
					swal({text: res.msg, buttons: false, icon: "success", timer: msgtimer});
					setTimeout(function() { window.location.href = res.return_url }, 800);
				}
				else {
					setTimeout(function() { window.location.href = res.return_url }, 800);
				}
			}
			else if (res.success == false) {
				$.LoadingOverlay("hide");
				swal({text: res.msg, buttons: false, icon: "error", timer: msgtimer});
			}
		}, error: function() {
			$.LoadingOverlay("hide");
			swal({text: oopsmsg, buttons: false, icon: "error", timer: msgtimer });
		}
	});
}));

$('.add_char').click(function(){
	var id   = 'add_char';
	var type = $(this).attr('data-type');
	$.LoadingOverlay("show");
	var $modal = $('#load_popup_modal_show');
	$modal.load('add-char.php', {'data-id': id, 'type': type}, function(res) {
		if (res != '') {
			$.LoadingOverlay("hide");
			$modal.modal('show');
		}
		else {
			$.LoadingOverlay("hide");
			swal({text: oopsmsg, buttons: false, icon: "error", timer: msgtimer });
		}
	});
});

$('.add_bg').click(function(){
	var id = 'add_bg';
	$.LoadingOverlay("show");
	var $modal = $('#load_popup_modal_show');
	$modal.load('add-bg.php', {'data-id': id }, function(res) {
		if (res != '') {
			$.LoadingOverlay("hide");
			$modal.modal('show');
		}
		else {
			$.LoadingOverlay("hide");
			swal({text: oopsmsg, buttons: false, icon: "error", timer: msgtimer });
		}
	});
});

$('body').on('click', '.delete_char_bg_img', function() {
	var upload_id	= $(this).attr("data-upload-id");
	var img_name	= $(this).attr("data-img-name");
	var dataString	= 'delete='+ true + '&file='+img_name+ '&upload_id='+upload_id;
	if (upload_id) {
		swal({
			title: "Are you sure?",
			text: "Delete this image.",
			icon: "warning",
			buttons: [true, 'Delete'],
			dangerMode: true, }).then((willDelete) => { if (willDelete) {
				$.LoadingOverlay("show");
				$.ajax({
					url: "includes/process.php",
					type: "POST",
					data: dataString,
					cache: false,
					success: function(resdata) {
						var res = $.parseJSON(resdata);
						if (res.success == false) {
							$.LoadingOverlay("hide");
							swal({text: res.msg, buttons: false, icon: "error", timer: msgtimer});
						}
						else if (res.success == true) {
							$.LoadingOverlay("hide");
							swal({text: res.msg, buttons: false, icon: "success", timer: msgtimer});
							setTimeout(function() { window.location.reload(); }, time);
						}
					},error: function() {
						$.LoadingOverlay("hide");
						swal({text: oopsmsg, buttons: false, icon: "error", timer: msgtimer });
					}
				});
			}
		});
	}
});

$("#profile_form").on('submit', (function(e) {
	e.preventDefault();
	$.LoadingOverlay("show");
	$.ajax({
		url: 'includes/process.php',
		type: "POST",
		data: new FormData(this),
		contentType: false,
		cache: false,
		processData:false,
		success: function(resdata) {
			var res = $.parseJSON(resdata);
			if (res.success == true) {
				swal({text: res.msg, buttons: false, icon: "success", timer: msgtimer});
				$.LoadingOverlay("hide");
				if (res.return_url != null) {
					setTimeout(function() { window.location.href = res.return_url; }, time);
				}
				else { setTimeout(function() { window.history.back(); }, time); }
			}
			else if (res.success == false) {
				swal({text: res.msg, buttons: false, icon: "error", timer: msgtimer});
				$.LoadingOverlay("hide");
			}
		}, error: function() {
			swal({text: oopsmsg, buttons: false, icon: "error", timer: msgtimer });
			$.LoadingOverlay("hide");
		}
	});
}));

$("#change_pwd_form").on('submit', (function(e) {
	e.preventDefault();
	$.LoadingOverlay("show");
	$.ajax({
		url: 'includes/process.php',
		type: "POST",
		data: new FormData(this),
		contentType: false,
		cache: false,
		processData:false,
		success: function(resdata) {
			var res = $.parseJSON(resdata);
			if (res.success == true) {
				$.LoadingOverlay("hide");
				swal({text: res.msg, buttons: false, icon: "success", timer: msgtimer});
				setTimeout(function() { window.location.href = res.return_url; }, time);
			}
			else if (res.success == false) {
				$.LoadingOverlay("hide");
				swal({text: res.msg, buttons: false, icon: "error", timer: msgtimer});
			}
		}, error: function() {
			$.LoadingOverlay("hide");
			swal({text: oopsmsg, buttons: false, icon: "error", timer: msgtimer });
		}
	});
}));

$("#ForgetFrom").on('submit', (function(e) {
	e.preventDefault();
	$.LoadingOverlay("show");
	$.ajax({
		url: 'includes/process.php',
		type: "POST",
		data: new FormData(this),
		contentType: false,
		cache: false,
		processData:false,
		success: function(resdata) {
			var res = $.parseJSON(resdata);
			if (res.success == true) {
				$.LoadingOverlay("hide");
				swal({text: res.msg, buttons: false, icon: "success", timer: msgtimer});
				setTimeout(function() { window.location.href = res.return_url; }, time);
			}
			else if (res.success == false) {
				swal({text: res.msg, buttons: false, icon: "error", timer: msgtimer});
				$.LoadingOverlay("hide");
			}
		}, error: function() {
			swal({text: oopsmsg, buttons: false, icon: "error", timer: msgtimer});
			$.LoadingOverlay("hide");
		}
	});
}));

$('#Notify').click(function() {
	var simID 	= $('#scenario_id').val();
	var type 	= $(this).attr('data-login-type');
	var url		= $('#url').val();
	if (simID != '') {
		$.LoadingOverlay("show");
		var dataString = 'notify='+ true +'&type='+ type +'&notify_sim_id='+ simID +'&url='+ url;
		$.ajax({
			type: 'POST',
			url: 'includes/process.php',
			data: dataString,
			cache: false,
			success: function(resdata) {
				var res = $.parseJSON(resdata);
				if (res.success == true) {
					$.LoadingOverlay("hide");
					swal({text: res.msg, buttons: false, icon: "success", timer: msgtimer});
				}
				else if (res.success == false) {
					$.LoadingOverlay("hide");
					swal({text: res.msg, buttons: false, icon: "error", timer: msgtimer});
				}
			}, error: function() {
				swal({text: oopsmsg, buttons: false, icon: "error", timer: msgtimer });
			}
		});
	}
});

$("#create_db_form").on('submit', (function(e) {
	e.preventDefault();
	$.LoadingOverlay("show");
	$.ajax({
		url: 'includes/main-process.php',
		type: "POST",
		data: new FormData(this),
		contentType: false,
		cache: false,
		processData:false,
		success: function(resdata) {
			var res = $.parseJSON(resdata);
			if (res.success == true) {
				$.LoadingOverlay("hide");
				swal({text: res.msg, buttons: false, icon: "success", timer: msgtimer});
				setTimeout(function() { window.location.reload(); }, time);
			}
			else if (res.success == false) {
				swal({text: res.msg, buttons: false, icon: "error", timer: msgtimer});
				$.LoadingOverlay("hide");
			}
		},error: function() {
			swal({text: oopsmsg, buttons: false, icon: "error", timer: msgtimer});
			$.LoadingOverlay("hide");
		}
	});
}));

$("#superAdmin_form").on('submit', (function(e) {
	e.preventDefault();
	$.LoadingOverlay("show");
	$.ajax({
		url: 'includes/main-process.php',
		type: "POST",
		data: new FormData(this),
		contentType: false,
		cache: false,
		processData:false,
		success: function(resdata) {
			var res = $.parseJSON(resdata);
			if (res.success == true) {
				$.LoadingOverlay("hide");
				swal({text: res.msg, buttons: false, icon: "success", timer: msgtimer});
				if (res.return_url && res.return_url != null) {
					setTimeout(function() { window.location.href = res.return_url; }, time);
				}
				else {
					setTimeout(function() { window.location.reload(); }, time);
				}
			}
			else if (res.success == false) {
				swal({text: res.msg, buttons: false, icon: "error", timer: msgtimer});
				$.LoadingOverlay("hide");
			}
		},error: function() {
			swal({text: oopsmsg, buttons: false, icon: "error", timer: msgtimer});
			$.LoadingOverlay("hide");
		}
	});
}));

$("#add_sim_linear_multiple_details_form").on('submit', (function(e) {
	for (instance in CKEDITOR.instances) { CKEDITOR.instances[instance].updateElement(); }
	e.preventDefault();
	$.LoadingOverlay("show");
	$.ajax({
		url: 'includes/process.php',
		type: "POST",
		data: new FormData(this),
		contentType: false,
		cache: false,
		processData: false,
		success: function(resdata) {
			var res = $.parseJSON(resdata);
			if (res.success == true) {
				$.LoadingOverlay("hide");
				swal({text: res.msg, buttons: false, icon: "success", timer: msgtimer});
				setTimeout(function() { window.location.href = res.return_url; }, msgtimer);
			}
			else if (res.success == false) {
				$.LoadingOverlay("hide");
				swal({text: res.msg, buttons: 'OK', icon: "warning"});
			}
		}, error: function() {
			$.LoadingOverlay("hide");
			swal({text: oopsmsg, buttons: false, icon: "error", timer: msgtimer});
		}
	});
}));

$("#add_sim_linear_video_multiple_details_form").on('submit', (function(e) {
	for (instance in CKEDITOR.instances) { CKEDITOR.instances[instance].updateElement(); }
	e.preventDefault();
	$.LoadingOverlay("show");
	$.ajax({
		url: 'includes/process.php',
		type: "POST",
		data: new FormData(this),
		contentType: false,
		cache: false,
		processData: false,
		success: function(resdata) {
			var res = $.parseJSON(resdata);
			if (res.success == true) {
				$.LoadingOverlay("hide");
				swal({text: res.msg, buttons: false, icon: "success", timer: msgtimer});
				setTimeout(function() { window.location.href = res.return_url; }, msgtimer);
			}
			else if (res.success == false) {
				$.LoadingOverlay("hide");
				swal({text: res.msg, buttons: 'OK', icon: "warning"});
			}
		}, error: function() {
			$.LoadingOverlay("hide");
			swal({text: oopsmsg, buttons: false, icon: "error", timer: msgtimer});
		}
	});
}));

$("#reset_data_form").on('submit', (function(e) {
	e.preventDefault();
	$.LoadingOverlay("show");
	$.ajax({
		url: 'includes/process.php',
		type: "POST",
		data: new FormData(this),
		contentType: false,
		cache: false,
		processData:false,
		success: function(resdata) {
			var res = $.parseJSON(resdata);
			if (res.success == true) {
				$.LoadingOverlay("hide");
				swal({text: res.msg, buttons: false, icon: "success", timer: msgtimer});
			}
			else if (res.success == false) {
				swal({text: res.msg, buttons: false, icon: "error", timer: msgtimer});
				$.LoadingOverlay("hide");
			}
		}, error: function() {
			swal({text: oopsmsg, buttons: false, icon: "error", timer: msgtimer});
			$.LoadingOverlay("hide");
		}
	});
}));

/* Linear Form Teplate 
Author : Saumya 
Date : 05-11-2020 */
$("#Form_linear_Video_Template").on('submit', (function(e) {
	for (instance in CKEDITOR.instances) { CKEDITOR.instances[instance].updateElement(); }
	e.preventDefault();
	$.LoadingOverlay("show");
	$.ajax({
		url: 'includes/process.php',
		type: "POST",
		data: new FormData(this),
		contentType: false,
		cache: false,
		processData:false,
		success: function(resdata) {				
			var res = $.parseJSON(resdata);
			if (res.success == true) {
				$.LoadingOverlay("hide");
				swal({text: res.msg, buttons: false, icon: "success", timer: msgtimer});
				setTimeout(function() { window.location.href = res.return_url; }, msgtimer);
			}
			else if (res.success == false) {
				swal({text: res.msg, buttons: false, icon: "error", timer: msgtimer});
				$.LoadingOverlay("hide");
			}
		}, error: function() {
			swal({text: oopsmsg, buttons: false, icon: "error", timer: msgtimer});
			$.LoadingOverlay("hide");
		}
	});
}));

/* Branching Classic Sim 
	Author : Saumya 
	Date : 05-12-2020 
*/
$("#add_update_branching_classic_sim").on('submit', (function(e) {
	for (instance in CKEDITOR.instances) { CKEDITOR.instances[instance].updateElement(); }
	e.preventDefault();
	$.LoadingOverlay("show");
	$.ajax({
		url: 'includes/process.php',
		type: "POST",
		data: new FormData(this),
		contentType: false,
		cache: false,
		processData:false,
		success: function(resdata) {
			var res = $.parseJSON(resdata);
			if (res.success == true) {
				$.LoadingOverlay("hide");
				swal({text: res.msg, buttons: false, icon: "success", timer: msgtimer});
				setTimeout(function() { window.location.href = res.return_url; }, time);
			}
			else if (res.success == false) {
				$.LoadingOverlay("hide");
				swal({text: res.msg, buttons: false, icon: "error", timer: msgtimer});
			}
		}, error: function() {
			$.LoadingOverlay("hide");
			swal({text: oopsmsg, buttons: false, icon: "error", timer: msgtimer});
		}
	});
}));

/* Branching Multiple Sim 
Author 	: Saumya 
Date 	: 23-12-2020 */
$("#add_sim_branching_multiple_details_form").on('submit', (function(e) {
	for (instance in CKEDITOR.instances) { CKEDITOR.instances[instance].updateElement(); }
	e.preventDefault();
	$.LoadingOverlay("show");
	$.ajax({
		url: 'includes/process.php',
		type: "POST",
		data: new FormData(this),
		contentType: false,
		cache: false,
		processData:false,
		success: function(resdata) {
			var res = $.parseJSON(resdata);
			if (res.success == true) {
				$.LoadingOverlay("hide");
				swal({text: res.msg, buttons: false, icon: "success", timer: msgtimer});
				setTimeout(function() { window.location.href = res.return_url; }, time);
			}
			else if (res.success == false) {
				$.LoadingOverlay("hide");
				swal({text: res.msg, buttons: false, icon: "error", timer: msgtimer});
			}
		}, error: function() {
			$.LoadingOverlay("hide");
			swal({text: oopsmsg, buttons: false, icon: "error", timer: msgtimer});
		}
	});
}));

/* Branching Video Classic Sim 
Author 	: Saumya 
Date 	: 15-02-2021 */
$("#branching_video_classic_template_form").on('submit', (function(e) {
	for (instance in CKEDITOR.instances) { CKEDITOR.instances[instance].updateElement(); }
	e.preventDefault();
	$.LoadingOverlay("show");
	$.ajax({
		url: 'includes/process.php',
		type: "POST",
		data: new FormData(this),
		contentType: false,
		cache: false,
		processData: false,
		success: function(resdata) { 
			var res = $.parseJSON(resdata);
			if (res.success == true) {
				$.LoadingOverlay("hide");
				swal({text: res.msg, buttons: false, icon: "success", timer: msgtimer});
				setTimeout(function() { window.location.href = res.return_url; }, time);
			}
			else if (res.success == false) {
				$.LoadingOverlay("hide");
				swal({text: res.msg, buttons: false, icon: "error", timer: msgtimer});
			}
		}, error: function() {
			$.LoadingOverlay("hide");
			swal({text: oopsmsg, buttons: false, icon: "error", timer: msgtimer});
		}
	});
}));

/* Branching Video Multiple Sim 
Author 	: Saumya 
Date 	: 25-02-2021 */
$("#branching_video_multiple_template_form").on('submit', (function(e) {
	for (instance in CKEDITOR.instances) { CKEDITOR.instances[instance].updateElement(); }
	e.preventDefault();
	$.LoadingOverlay("show");
	$.ajax({
		url: 'includes/process.php',
		type: "POST",
		data: new FormData(this),
		contentType: false,
		cache: false,
		processData: false,
		success: function(resdata) { 
			var res = $.parseJSON(resdata);
			if (res.success == true) {
				$.LoadingOverlay("hide");
				swal({text: res.msg, buttons: false, icon: "success", timer: msgtimer});
				setTimeout(function() { window.location.href = res.return_url; }, time);
			}
			else if (res.success == false) {
				$.LoadingOverlay("hide");
				swal({text: res.msg, buttons: false, icon: "error", timer: msgtimer});
			}
		}, error: function() {
			$.LoadingOverlay("hide");
			swal({text: oopsmsg, buttons: false, icon: "error", timer: msgtimer});
		}
	});
}));
