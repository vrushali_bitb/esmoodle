// JavaScript Document

/* Verification API */

/* API URL */
var base_url            = 'https://proctor.easysim.app/api/';
var snapshot_endpoint   = base_url + 'snapshot_validate';
var analysis_endpoint   = base_url + 'attention_analysis';
var report_endpoint     = base_url + 'attention_analysis_report';

/* Check Snapshot Verification */
/* String:: profile_url & snapshot_url */
function snapshot_valid(profile_url, snapshot_url) {
    if (profile_url != '' && snapshot_url != '') {
        var jsonObjects = {profile_url:profile_url, snapshot_url:snapshot_url + '?time='+ Date.now()};
        $.ajax({
            url: snapshot_endpoint,
            type: "POST",
            data: JSON.stringify(jsonObjects),
            dataType: 'json',
            contentType: 'application/json',
            crossDomain:true,
            cache: false,
            success: function(res) {
                var mid = res.message_id;
                var msg = '';
                if (mid == 'nurl') {
                    msg = 'URL issue.';
                }
                else if (mid == 'nfp') {
                    msg = 'Face detection issue in your profile pic. Please upload clear face pic.';
                }
                else if (mid == 'nfc') {
                    msg = 'Face detection issue in webcam.';
                }

                if (res.success == true && res.match == true){
                    matchProgress(res.score);
                    swal({text: 'Face verification successfully.', icon: "success"});
                    $('#launch_btn').show();
                    $('#Verify').hide();
                }
                else if (res.success == true && res.match == false){
                    matchProgress(res.score);
                    swal({text: msg, icon: "warning"});
                }
                else if (res.success == false){
                    swal({text: 'Oops. something went wrong please try again.', icon: "error"});
                }
            }, error: function() {
                swal({text: 'Oops. something went wrong please try again.', buttons: false, icon: "error", timer: 5000});
            }
        });
    }
}

function matchProgress(param) {
    if (param){
        window.percent = 0;
        window.progressInterval = window.setInterval(function(){
        if(window.percent < param) {
            window.percent++;
            $('.progress').addClass('progress-striped').addClass('active');
            $('.progress .progress-bar:first').removeClass().addClass('progress-bar')
            .addClass((percent < 10) ? 'progress-bar-danger' : ((percent < param) ? 'progress-bar-warning' : 'progress-bar-success'));
            $('.progress .progress-bar:first').width(window.percent + '%');
            $('.progress .progress-bar:first').text(window.percent + '%');
        } /* else {
            //window.clearInterval(window.progressInterval);
            jQuery('.progress').removeClass('progress-striped').removeClass('active');
            jQuery('.progress .progress-bar:first').text('Done!');
        } */
        }, 100);
    }
}

/* Attention Analysis */
/* String:: file name & basse url */
function attention_analysis(filename, video_url, profile_url) {
    if (filename != '' && url != '') {
        var jsonObjects = {vid:filename, video_url:video_url, profile_url:profile_url + '?time='+ Date.now()};
        $.ajax({
            url: analysis_endpoint,
            type: "POST",
            data: JSON.stringify(jsonObjects),
            dataType: 'json',
            contentType: 'application/json',
            crossDomain:true,
            cache: false,
            success: function(res) {
                console.log(res);
            }, error: function() {
                swal({text: 'Oops. something went wrong please try again.', buttons: false, icon: "error", timer: 5000});
            }
        });
    }
}

/* Attention Analysis Report */
/* ['file name', 'file name'] */
function attention_analysis_report(filename) {
    if (filename != '') {
        $('#frames,#outoffocus').html(0);
        $('#video_url').attr('data-src', 'javascript:void(0);');
        var jsonObjects = {vid:[filename]};
        $.ajax({
            url: report_endpoint,
            type: "POST",
            data: JSON.stringify(jsonObjects),
            dataType: 'json',
            contentType: 'application/json',
            crossDomain:true,
            cache: false,
            success: function(res) {
                if (res.success == true && res.data.length > 0)
                {
                    $('#attention_report,#attention_chart').show();
                    var frames      = res.data[0]['frames'];
                    var outoffocus  = res.data[0]['outoffocus'];
                    var report      = res.data[0]['report'];
                    var sleep       = res.data[0]['sleep'];
                    var status      = res.data[0]['status'];
                    var std         = res.data[0]['std'];
                    var video_url   = res.data[0]['video'];
                    var distance    = res.data[0]['distance'];
                    var timeline    = res.data[0]['timeline'];
                    var dataPoints  = [];
                    for (var i = 0; i < distance.length; i++){
                        dataPoints.push({'x':timeline[i], 'y':distance[i]});
                    }
                    var attention_chart = new CanvasJS.Chart("attention_chart", {
                        backgroundColor: "",
                        animationEnabled: true,
                        theme: "light2", //"light1", "light2", "dark1", "dark2"
                        title: { text: "" },
                        //axisX: { gridThickness: 0, tickLength: 0, lineThickness: 0, labelFontColor: "white", labelFormatter: function() { return " "; } },
                        //axisY: { gridThickness: 0, tickLength: 0, lineThickness: 0, labelFormatter: function(){ return " "; } },
                        data: [{
                                type: "line",
                                showInLegend: false,
                                color: "#4472c4",
                                dataPoints: dataPoints
                            }],
                        });
                    attention_chart.render();
                    $('#frames').html(frames);
                    $('#outoffocus').html(outoffocus);
                    $('#report').html(report);
                    $('#sleep').html(sleep);
                    $('#status').html(status);
                    $('#std').html(std);
                    if (video_url != '') {
                        var vhtml = '<a href="javascript:void(0);" id="video_url" data-fancybox data-src="'+ video_url +'"><img class="img-responsive" src="img/list/video_sce.svg" style="width: 100px !important; height: 50px !important;" /></a>';
                        $('#video').empty().html(vhtml);
                    }
                    $.LoadingOverlay("hide");
                }
                else {
                    $.LoadingOverlay("hide");
                    swal({text: 'Data not found.!', buttons: false, icon: "warning", timer: 5000});
                }
            }, error: function() {
                $.LoadingOverlay("hide");
                swal({text: 'Oops. something went wrong please try again.', buttons: false, icon: "error", timer: 5000});
                return false;
            }
        });
    }
}
