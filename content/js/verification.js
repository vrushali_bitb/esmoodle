// JavaScript Document

/* For Face Verification */

/* eslint-disable */
var options = {
    controls: true,
    fluid: false,
    controlBar: {
        volumePanel: false,
        fullscreenToggle: false
    },
    // dimensions of the video.js player
    width: 640,
    height: 480,
    plugins: {
        record: {
            debug: true,
            imageOutputType: 'dataURL',
            imageOutputFormat: 'image/png',
            imageOutputQuality: 0.92,
            image: {
              // image media constraints: set resolution of camera
              width: { min: 640, ideal: 640, max: 1280 },
              height: { min: 480, ideal: 480, max: 920 }
            },
            // dimensions of captured video frames
            frameWidth: 640,
            frameHeight: 480
        }
    }
};

// apply some workarounds for opera browser
applyVideoWorkaround();

var player = videojs('myImage', options, function() {
    // print version information at startup
    var msg = 'Using video.js ' + videojs.VERSION +
        ' with videojs-record ' + videojs.getPluginVersion('record') +
        ' and recordrtc ' + RecordRTC.version;
    videojs.log(msg);
});

// error handling
player.on('deviceError', function() {
    console.log('device error:', player.deviceErrorCode);
    swal({text: player.deviceErrorCode, buttons: false, icon: "warning", timer: 3000});
});

player.on('error', function(element, error) {
    console.error(error);
    swal({text: error, buttons: false, icon: "warning", timer: 3000});
});

// user clicked the record button and started recording
player.on('startRecord', function() {
    console.log('started recording!');
	$('.upload').removeAttr('disabled', true);
});

// stop device stream only
$('.stopRecord').on('click', function(){
	player.record().stopStream();
});

// user completed recording and stream is available
player.on('finishRecord', function() {
    // the blob object contains the recorded data that
    // can be downloaded by the user, stored on server etc.
    // console.log('finished recording: ', player.recordedData);
	// Create an instance of FormData and append the video parameter that
    // will be interpreted in the server as a file	
    if (player.recordedData) {
        //console.log('finished recording: ', player.recordedData);
        var data        = player.recordedData;
        var serverUrl	= 'includes/upload.php';
        var formData	= new FormData();
        formData.append('file', data);
        formData.append('face_verify', 1);
        $('.verify').html('<i class="fa fa-spinner fa-spin fa-fw"></i><span class="sr-only">Loading...</span> Please wait....');
        $.ajax({
            url: serverUrl,
            method: 'POST',
            data: formData,
            cache: false,
            processData: false,
            contentType: false,
            success: function (resdata) {
                var res = $.parseJSON(resdata);
                if (res.success) {
                    $('.verify').removeAttr('disabled', true).removeClass('disabled').html('Verify');
                }
            }
            ,error: function() {
                swal({text: 'Oops. something went wrong please try again.', buttons: false, icon: "error", timer: 5000});
            }
        });
    }
});

player.on('retry', function() {
    $('.verify').attr('disabled', true).addClass('disabled').html('Verify');
    console.log('retry');
});

var file_name   = $('#vfile_name').val();
var profile     = $('#profile').val();

$('.verify').on('click', function(e){
    e.preventDefault();
    player.record().stopStream();
    $('.verify').attr('disabled', true).addClass('disabled');
    snapshot_valid(profile, file_name);
});
