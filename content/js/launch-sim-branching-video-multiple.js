// JavaScript Document

// Use Saving Selected Options in DB

var time 		= 3000;
var msgtimer	= 2000;
var oopsmsg		= 'Oops. something went wrong please try again.';

var uid			= $('#uid').val();
var simId		= $('#simId').val();
var userid		= $('#userid').val();
var apiurl		= $('#apiurl').val();
var save		= $('#save').val();

var cur  		= curData.data('cur');
var next 		= curData.data('target');
var report		= curData.data('show-report');
var qtype 		= curData.data('qtype');	
var status 		= curData.data('status');
var qid 		= curData.data('qid');
var nextvid		= '#qvideo-'+ next;

if (save) {
	var dataAttemptString = {"req":"Insert", "table":"multi_score_tbl", "data":{"uid":uid, "scenario_id":simId, "question_type": qtype, "question_id": quesId, "status": status, "userid": userid}};
	$.ajax({
		type: 'POST',
		url: apiurl,
		data: {single_data: JSON.stringify(dataAttemptString)}, /* Use single_data for send a single post data */
		cache: false,
		dataType: "json",
		crossDomain: true,
		success: function(resdata){
			var res = resdata;
			if (res.status == 'Succes') {
				console.log('score added.!');
			}
		},error: function() {
			swal({text: oopsmsg, buttons: false, icon: "error", timer: msgtimer});
		}
	});
}
if (report){
	timer.trigger('click');
	$('.signbtnRepot').show();
}
else {
	$('.MatchingQusBanner_'+ cur).fadeOut();
	$('.MatchingQusBanner_'+ next).fadeIn();
	var ndata	= $(nextvid);
	var stime	= ndata.data('start-time');
	var etime	= ndata.data('end-time');
	var type  	= ndata.data('type');
	var qtype 	= ndata.data('qtype');
	var box_id	= ndata.data('video-id');
	if (etime != '') {
		var qplayer = videojs(nextvid);
		if (stime == 0) {
			qplayer.on('timeupdate', function(e){
				if (qplayer.currentTime() >= etime){
					qplayer.pause();
					$('.MatchingQusBanner_'+ next +' #option_'+ box_id).fadeIn(1000);
				}
			});
		}
		else if (stime != '' || stime != 0) {
			qplayer.currentTime(stime);
			qplayer.play();
			qplayer.on('timeupdate', function(e){
				if (qplayer.currentTime() >= etime) {
					qplayer.pause();
					$('.MatchingQusBanner_'+ next +' #option_'+ box_id).fadeIn(1000);
				}
			});
		}
	}
	else {
		var qplayer = videojs(nextvid);
		qplayer.play();
		qplayer.on('ended', function(e) {
			$('.MatchingQusBanner_'+ next +' #option_'+ box_id).fadeIn(1000);
		});
	}
}
