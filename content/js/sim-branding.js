// JavaScript Document

// Use for SIM BRANDING

// Question Background
if (simBrand.ques_bg) {
    $('.MatchingQusText').css("background-color", simBrand.ques_bg);
}

// Option Background
if (simBrand.option_bg) {
    $('.option_brand').css("background-color", simBrand.option_bg);
}

// Option Hover
if (simBrand.option_hover) {
    $(".option_brand").hover(function(){
        $(this).css("background-color", simBrand.option_hover);
        }, function(){
        $(this).css("background-color", simBrand.option_bg);
      });
}

// Option Selected
if (simBrand.option_selected) {
    $('.active').css("background-color", simBrand.option_selected);
}

// Submit button Background
if (simBrand.btn_bg) {
    $('.btn.submitbtn').css("background-color", simBrand.btn_bg);
}

// Submit button Hover
if (simBrand.btn_hover) {
    $(".btn.submitbtn").hover(function(){
        $(this).css("background-color", simBrand.btn_hover);
        }, function(){
        $(this).css("background-color", simBrand.btn_bg);
      });
}

// Font Size
if (simBrand.font_size) {
    $('body').attr("style", "font-size: "+ simBrand.font_size +" !important");
}

// Font Color
if (simBrand.font_color) {
    $('body').css("color", simBrand.font_color);
}
