// JavaScript Document

// Use Saving Selected Options in DB

var time 		= 3000;
var msgtimer	= 2000;
var oopsmsg		= 'Oops. something went wrong please try again.';

var uid			= $('#uid').val();
var simId		= $('#simId').val();
var userid		= $('#userid').val();
var apiurl		= $('#apiurl').val();
var save		= $('#save').val();

var curData		= $('.feedclose');
var cur  		= curData.data('cur');
var next 		= curData.data('target');
var report		= curData.data('show-report');
var option		= $('#option_' + cur);
var qtype 		= option.data('qtype');
var qid 		= '#qvideo-'+ next;

if ($(qid).hasClass('video-js')) {
	var qplayer = videojs(qid);
	qplayer.play();
	var start_time	= $(qid).data('start-time'); //stop time in seconds
	var end_time 	= $(qid).data('end-time'); //stop time in seconds
	var type  		= $(qid).data('type');
	var qtype 		= $(qid).data('qtype');
	if (end_time != '' && type == 'video') {
		if (start_time == 0) {
			qplayer.on('timeupdate', function(e){
				if (qplayer.currentTime() >= end_time){
					qplayer.pause();
					$('.MatchingQusBanner_'+ next +' .Matchingcontainer .Videoquestion').css({'display':'block'});
					$('.MatchingQusBanner_'+ next +' .launchvideopatch').css({'display':'block'});
					$('.MatchingQusBanner_'+ next +' .Videoquestion_btn').css({'display':'block'});
					$('.MatchingQusBanner_'+ next +' .MCQOption .VideoOptions').css({'display':'block'}).addClass('advertise_container fade-in');
				}
			});
		}
		else if (start_time != 0) {
			qplayer.currentTime(start_time);
			qplayer.play();
			qplayer.on('timeupdate', function(e){
				if (qplayer.currentTime() >= end_time) {
					qplayer.pause();
					$('.MatchingQusBanner_'+ next +' .Matchingcontainer .Videoquestion').css({'display':'block'});
					$('.MatchingQusBanner_'+ next +' .launchvideopatch').css({'display':'block'});
					$('.MatchingQusBanner_'+ next +' .Videoquestion_btn').css({'display':'block'});
					$('.MatchingQusBanner_'+ next +' .MCQOption .VideoOptions').css({'display':'block'}).addClass('advertise_container fade-in');
				}
			});
		}
	}
}

if (save) {
	/* MCQ */
	if (qtype == 4) {
		var mcqdata = $('#option_'+ cur +' .press');
		if (mcqdata.data('aid')) {
			var mcqDataAttemptString = {"req":"Insert", "table":"score_tbl", "data":{"uid":uid, "scenario_id":simId, "question_type": qtype, "question_id": mcqdata.data('qid'), "choice_option_id": mcqdata.data('aid'), "match_option_id": mcqdata.data('aid'), "userid": userid}};
			$.ajax({
				type: 'POST',
				url: apiurl,
				data: {single_data: JSON.stringify(mcqDataAttemptString)}, /* Use single_data for send a single post data */
				cache: false,
				dataType: "json",
				crossDomain: true,
				success: function(resdata){
					var res = resdata;
					if (res.status == 'Succes') {
						console.log('score added.!');
					}
				},error: function() {
					swal({text: oopsmsg, buttons: false, icon: "error", timer: msgtimer});
				}
			});
		}
	}
}
if (report){
	if (save) {
		timer.trigger('click');
		$('.signbtnRepot').show();
	}
	else {
		swal({text: "You've attempted all questions!", buttons: false, icon: "success", closeOnClickOutside: false, closeOnEsc: false});
	}
}
else {
	$('.MatchingQusBanner_'+ cur).fadeOut();
	$('.MatchingQusBanner_'+ next).fadeIn(1000);
}
