// JavaScript Document

var time 		= 3000;
var msgtimer	= 2000;
var oopsmsg		= 'Oops. something went wrong please try again.';

var uid			= $('#uid').val();
var simId		= $('#simId').val();
var userid		= $('#userid').val();
var apiurl		= $('#apiurl').val();
var save		= $('#save').val();

$('body').on('click', '.submitbtn', function() {
	var curData	= $(this);
	var cur  	= curData.data('cur');
	var next 	= curData.data('target');
	var report	= curData.data('show-report');
	if (report){
		swal({text: "You've attempted all questions!", buttons: false, icon: "success", closeOnClickOutside: false, closeOnEsc: false});
	}
	else {
		$('.MatchingQusBanner_'+ cur).fadeOut();
		$('.MatchingQusBanner_'+ next).fadeIn(1000);
	}
});
