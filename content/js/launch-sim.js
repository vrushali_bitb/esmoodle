// JavaScript Document

// Use Saving Selected Options in DB

var time 	 = 3000;
var msgtimer = 2000;
var oopsmsg	 = 'Oops. something went wrong please try again.';

var uid		= $('#uid').val();
var simId	= $('#simId').val();
var userid	= $('#userid').val();
var apiurl	= $('#apiurl').val();
var save 	= $('#save').val();

if (uid != '' && save == true) {
	var dataAttemptString = {"req":"Insert", "table":"scenario_attempt_tbl", "data":{"uid":uid, "scenario_id":simId, "userid":userid}};
	$.ajax({
		type: 'POST',
		url: apiurl,
		data: { single_data: JSON.stringify(dataAttemptString) },
		cache: false,
		dataType: "json",
		crossDomain: true,
		success: function (resdata) {
			var res = resdata;
			if (res.status == 'Succes') {
				console.log('Attempt data added.!');
			}
		}, error: function () {
			swal({ text: oopsmsg, buttons: false, icon: "error", timer: msgtimer });
		}
	});
}

$('.CharFlex.CharFlextwo').insertBefore('.Questmtf');

$('.CharFlextwo.right').insertAfter('.Questmtf');

//-----------View-SIM-----------------
$('body').on('click', '#view_sim', function () {
	var sim_id = $(this).attr('data-sim-id');
	if (sim_id != '') {
		$.LoadingOverlay("show");
		var $modal = $('#load_popup_modal_show');
		$modal.load('view-sim-modal.php', { 'data-id': sim_id }, function (res) {
			$.LoadingOverlay("hide");
			if (res != '') {
				$modal.modal('show', { backdrop: 'static', keyboard: false });
			}
			else {
				swal({ text: oopsmsg, buttons: false, icon: "error", timer: msgtimer });
			}
		});
	}
});

//-----------Close-Page----------------
$('.close-banner').click(function () {
	swal({
		title: "Are you sure?",
		text: "Close this Simulation.",
		icon: "warning",
		buttons: [true, 'Close'],
		closeOnClickOutside: false,
		closeOnEsc: false,
		dangerMode: true,
	}).then((willDelete) => {
		if (willDelete) {
			$.LoadingOverlay("show");
			setTimeout(function(){
				window.location.href = 'assigned-simulations.php';
				$.LoadingOverlay("hide");
			}, 10000);
		}
	});
});

$('a.qassets_icon').fancybox({
	'titleShow': true,
	'transitionIn': 'elastic',
	'transitionOut': 'elastic',
	'easingIn': 'easeOutBack',
	'easingOut': 'easeInBack'
});

$('.document').fancybox({
	openEffect: 'elastic',
	closeEffect: 'elastic',
	autoSize: true,
	type: 'iframe',
	iframe: { preload: false }
});

var owl = $('.owl-carousel');
owl.owlCarousel({
	margin: 10,
	loop: false,
	center: true,
	responsive: {
		0: { items: 1 },
		600: { items: 2 },
		1000: { items: 4 }
	}
});

function speak(obj) {
	var speaking = $().articulate('isSpeaking');
	var paused = $().articulate('isPaused');

	// This is how you can use one button for a speak/pause toggle
	// Is browser speaking? (This returns 'true' even when paused)
	// If it is, is speaking paused? If paused, then resume; otherwise pause
	// If it isn't, then initiate speaking

	if (speaking) {
		if (paused) {
			$().articulate('resume');
		} else {
			$().articulate('pause');
		}
	} else {
		$(obj).articulate('speak');
	};

	var playicon = document.getElementById("playicon");
	var pauseicon = document.getElementById("pauseicon");

	if (playicon.style.display === 'none') {
		playicon.style.display = "block";
		pauseicon.style.display = "none";
	} else {
		playicon.style.display = "none";
		pauseicon.style.display = "block";
	}
};

function speak_feedback(obj) {
	var speaking = $().articulate('isSpeaking');
	var paused = $().articulate('isPaused');

	// This is how you can use one button for a speak/pause toggle
	// Is browser speaking? (This returns 'true' even when paused)
	// If it is, is speaking paused? If paused, then resume; otherwise pause
	// If it isn't, then initiate speaking

	if (speaking) {
		if (paused) {
			$().articulate('resume');
		} else {
			$().articulate('pause');
		}
	} else {
		$(obj).articulate('speak');
	};

	var playicon_feedback = document.getElementById("playicon_feedback");
	var pauseicon_feedback = document.getElementById("pauseicon_feedback");

	if (playicon_feedback.style.display === 'none') {
		playicon_feedback.style.display = "block";
		pauseicon_feedback.style.display = "none";
	} else {
		playicon_feedback.style.display = "none";
		pauseicon_feedback.style.display = "block";
	}
};

document.onreadystatechange = function () {
	if (document.readyState == "complete") {
		$.LoadingOverlay("hide");
	}
};
