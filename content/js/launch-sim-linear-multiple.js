// JavaScript Document

// Use Saving Selected Options in DB

var time 		= 3000;
var msgtimer	= 2000;
var oopsmsg		= 'Oops. something went wrong please try again.';

var uid			= $('#uid').val();
var simId		= $('#simId').val();
var userid		= $('#userid').val();
var apiurl		= $('#apiurl').val();
var save		= $('#save').val();

var curData		= $('.feedclose');
var cur  		= curData.data('cur');
var next 		= curData.data('target');
var report		= curData.data('show-report');
var qtype 		= curData.data('qtype');	
var status 		= curData.data('status');
var qid 		= curData.data('qid');

if (save) {
	var dataAttemptString = {"req":"Insert", "table":"multi_score_tbl", "data":{"uid":uid, "scenario_id":simId, "question_type": qtype, "question_id": qid, "status": status, "userid": userid}};
	$.ajax({
		type: 'POST',
		url: apiurl,
		data: {single_data: JSON.stringify(dataAttemptString)}, /* Use single_data for send a single post data */
		cache: false,
		dataType: "json",
		crossDomain: true,
		success: function(resdata){
			var res = resdata;
			if (res.status == 'Succes') {
				console.log('score added.!');
			}
		},error: function() {
			swal({text: oopsmsg, buttons: false, icon: "error", timer: msgtimer});
		}
	});
}
if (report){
	if (save) {
		timer.trigger('click');
		$('.signbtnRepot').show();
	}
	else {
		swal({text: "You've attempted all questions!", buttons: false, icon: "success", closeOnClickOutside: false, closeOnEsc: false});
	}
}
else {
	$('.MatchingQusBanner_'+ cur).fadeOut();
	$('.MatchingQusBanner_'+ next).fadeIn(1000);
}
