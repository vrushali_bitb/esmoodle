// JavaScript Document

/* For Proctor Video Recording */

var file_name = $('#vfile_name').val() + '.webm';

/* eslint-disable */
var options = {
    controls: true,
	loop: false,
   	width: 320,
    height: 240,
    fluid: false,
    plugins: {
        record: {
            audio: true,
            video: true,
            maxLength: 3000,
            debug: true,
            convertEngine: 'ts-ebml', // use the ts-ebml convert plugin to inject metadata in webm files
			audioEngine: 'recordrtc',
			videoMimeType: 'video/webm;codecs=vp8,opus',
            timeSlice: 2000, // fire the timestamp event every 2 seconds
        }
    }
};

// apply some workarounds for opera browser
applyVideoWorkaround();

var player = videojs('myVideo', options, function() {
    // print version information at startup
    var msg = 'Using video.js ' + videojs.VERSION +
        ' with videojs-record ' + videojs.getPluginVersion('record') +
        ' and recordrtc ' + RecordRTC.version;
    videojs.log(msg);
});

// error handling
player.on('deviceError', function() {
    //console.log('device error:', player.deviceErrorCode);
    swal({text: player.deviceErrorCode, buttons: false, icon: "error", timer: 8000});
});

player.on('error', function(element, error) {
    //console.error(error);
    swal({text: error, buttons: false, icon: "error", timer: 8000});
});

player.on('ready', function() {
	player.record().getDevice();
});

player.on('deviceReady', function() {    
    player.record().start();
});

// user clicked the record button and started recording
player.on('startRecord', function() {
    console.log('started recording!');
});

function stopRecStream(){
    player.record().stopStream();
    player.record().stopDevice();
    player.record().stop();
}

$('body').on('click', '.swal-button--confirm', function(e){
    e.preventDefault();
    e.stopImmediatePropagation();
    stopRecStream();
});

// monitor stream data during recording
player.on('timestamp', function() {
    // timestamps
    if($('.signbtnRepot').is(":visible")){
        stopRecStream();
    }
});

player.on('finishRecord', function() {
    if (player.recordedData) {
        var data        = player.recordedData;
        var serverUrl	= 'includes/upload.php';
        var formData	= new FormData();
        formData.append('file', data, file_name);
        formData.append('proctor_video', 1);
        console.log('uploading recording:', file_name);
        $.ajax({
            url: serverUrl,
            method: 'POST',
            data: formData,
            cache: false,
            processData: false,
            contentType: false,
            success: function (resdata) {
                var res = $.parseJSON(resdata);
                if (res.success == true && res.file_name != '') {
                    var fname       = res.file_name;
                    var video_url   = $('#base_path').val() + 'proctor/' + fname;
                    var profile_url = $('#profile').val();
                    attention_analysis(fname, video_url, profile_url);
                }
            }
            ,error: function() {
                swal({text: 'Oops. something went wrong please try again.', buttons: false, icon: "error", timer: 5000 });
            }
        });
    }
});
