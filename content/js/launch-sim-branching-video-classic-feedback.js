// JavaScript Document

// Use Saving Selected Options in DB

var time 		= 3000;
var msgtimer	= 2000;
var oopsmsg		= 'Oops. something went wrong please try again.';
var uid			= $('#uid').val();
var simId		= $('#simId').val();
var userid		= $('#userid').val();
var apiurl		= $('#apiurl').val();
var save		= $('#save').val();

$('.feedclose').on('click', function(){ 
	var curData	= $(this);
	var cur  	= curData.data('cur');
	var next 	= curData.data('target');
	var report	= curData.data('show-report');
	var option	= $('#option_' + cur);
	var qtype 	= option.data('qtype');
	var nextvid	= '#qvideo-'+ next;
	if (save) {
		/* MCQ */
		if (qtype == 4) {
			var mcqdata = $('#option_'+ cur +' .press');
			if (mcqdata.data('aid')) {
				var mcqDataAttemptString = {"req":"Insert", "table":"score_tbl", "data":{"uid":uid, "scenario_id":simId, "question_type": qtype, "question_id": mcqdata.data('qid'), "choice_option_id": mcqdata.data('aid'), "match_option_id": mcqdata.data('aid'), "userid": userid}};
				$.ajax({
					type: 'POST',
					url: apiurl,
					data: {single_data: JSON.stringify(mcqDataAttemptString)}, /* Use single_data for send a single post data */
					cache: false,
					dataType: "json",
					crossDomain: true,
					success: function(resdata){
						var res = resdata;
						if (res.status == 'Succes') {
							console.log('score added.!');
						}
					},error: function() {
						swal({text: oopsmsg, buttons: false, icon: "error", timer: msgtimer});
					}
				});
			}
		}
		/* SWIPING */
		else if (qtype == 7) {
			var swip = $('#option_'+ cur +' .owl-stage .center .swipeoptions');
			if (swip.data('aid')) {
				var swipDataAttemptString = {"req":"Insert", "table":"score_tbl", "data":{"uid":uid, "scenario_id":simId, "question_type": qtype, "question_id": swip.data('qid'), "choice_option_id": swip.data('aid'), "match_option_id": swip.data('aid'), "userid": userid}};
				$.ajax({
					type: 'POST',
					url: apiurl,
					data: {single_data: JSON.stringify(swipDataAttemptString)}, /* Use single_data for send a single post data */
					cache: false,
					dataType: "json",
					crossDomain: true,
					success: function(resdata){
						var res = resdata;
						if (res.status == 'Succes') {
							console.log('score added.!');
						}
					},error: function() {
						swal({text: oopsmsg, buttons: false, icon: "error", timer: msgtimer});
					}
				});
			}
		}
		/* DRAG AND DROP */
		else if (qtype == 8) {
			var dd_data = $('#option_'+ cur +' .dragBox');
			if (dd_data.children('div').data('aid')) {
				var ddDataAttemptString = {"req":"Insert", "table":"score_tbl", "data":{"uid":uid, "scenario_id":simId, "question_type": qtype, "question_id": dd_data.data('qid'), "choice_option_id": dd_data.children('div').data('aid'), "match_option_id": dd_data.children('div').data('aid'), "userid": userid}};
				$.ajax({
					type: 'POST',
					url: apiurl,
					data: {single_data: JSON.stringify(ddDataAttemptString)}, /* Use single_data for send a single post data */
					cache: false,
					dataType: "json",
					crossDomain: true,
					success: function(resdata){
						var res = resdata;
						if (res.status == 'Succes') {
							console.log('score added.!');
						}
					},error: function() {
						swal({text: oopsmsg, buttons: false, icon: "error", timer: msgtimer});
					}
				});
			}
		}
	}
	if (report){
		if (save) {
			timer.trigger('click');
			$('.signbtnRepot').show();
		}
		else {
			swal({text: "You've attempted all questions!", buttons: false, icon: "success", closeOnClickOutside: false, closeOnEsc: false});
		}
	}
	else {
		$('.MatchingQusBanner_'+ cur).fadeOut();
		$('.MatchingQusBanner_'+ next).fadeIn();
		var ndata	= $(nextvid);
		var stime	= ndata.data('start-time');
		var etime	= ndata.data('end-time');
		var type  	= ndata.data('type');
		var qtype 	= ndata.data('qtype');
		var box_id	= ndata.data('video-id');
		if (etime != '') {
			var qplayer = videojs(nextvid);
			if (stime == 0) {
				qplayer.on('timeupdate', function(e){
					if (qplayer.currentTime() >= etime){
						qplayer.pause();
						$('.MatchingQusBanner_'+ next +' #option_'+ box_id).fadeIn(1000);
					}
				});
			}
			else if (stime != '' || stime != 0) {
				qplayer.currentTime(stime);
				qplayer.play();
				qplayer.on('timeupdate', function(e){
					if (qplayer.currentTime() >= etime) {
						qplayer.pause();
						$('.MatchingQusBanner_'+ next +' #option_'+ box_id).fadeIn(1000);
					}
				});
			}
		}
		else if (stime == '' || etime == '') {
			var qplayer = videojs(nextvid);
			qplayer.play();
			qplayer.on('ended', function(e) {
				$('.MatchingQusBanner_'+ next +' #option_'+ box_id).fadeIn(1000);
			});
		}
	}
});
