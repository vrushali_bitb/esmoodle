// JavaScript Document
$(document).ready(function() {
  function loadTooltipData($el, contentUrl) {
    $.ajax(contentUrl, {
      method: 'GET',
      dataType: 'html',
      success: function(data) {
        $el.data('bs.popover').options.content = data;
        // setup handlers for hiding the tooltip (but leaving it shown when they mouse over from the
        // tooltip to the originating element, or vice versa)
        $el.data('bs.popover').tip()
          .add($el)
          .mouseenter(function() {
            $el.data('mouse-in-tooltip', true);
          })
          .mouseleave(function() {
            $el.data('mouse-in-tooltip', false);
            setTimeout(function() {
              hideTooltip($el);
            }, 250);
          });
        $el.popover('show');
      }
    });
  }

  function showTooltip($el, contentUrl) {
    if ($el.data('bs.popover')) {
      if (!$el.data('bs.popover').tip().hasClass('in'))
        $el.popover('show');
    } else {
      $el.popover({
        container: 'body',
        content: 'Loading...',
        placement: 'bottom',
        html: true
      }).popover('show');
      loadTooltipData($el, contentUrl);
    }
  }

  function hideTooltip($el) {
    if ( ! $el.data('mouse-in-tooltip')) {
      $el.popover('hide');
    }
  }

  $('[data-toggle="popover"]').mouseenter(function() {
    var $el = $(this);
    showTooltip($el, this.href);
  })
});
