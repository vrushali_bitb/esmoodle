// JavaScript Document

// Use Saving Selected Options in DB

var time 		= 3000;
var msgtimer	= 2000;
var oopsmsg		= 'Oops. something went wrong please try again.';

var uid			= $('#uid').val();
var simId		= $('#simId').val();
var userid		= $('#userid').val();
var apiurl		= $('#apiurl').val();

//----------------Add-TTS------------------
$('body').on('click', '.add-tts', function() {
	var box_id	= $(this).attr('data-box-id');
	var qid		= $(this).attr('data-qid');
	var type	= $(this).attr('data-qtype');
	if (box_id != '') {
		$.LoadingOverlay("show");
		var $modal = $('#load_action_popup_modal_show');
		$modal.load('tts-open-response-modal.php', {'box_id': box_id, 'qid': qid, 'type': type}, function(res) {
			$.LoadingOverlay("hide");
			if (res != '') {
				$modal.modal('show', {backdrop: 'static', keyboard: false});
			}
			else {
				swal({text: oopsmsg, buttons: false, icon: "error", timer: msgtimer});
			}
		});
	}
});

//----------------Add-Code------------------
$('body').on('click', '.add-code', function() {
	var box_id	= $(this).attr('data-box-id');
	var qid		= $(this).attr('data-qid');
	var type	= $(this).attr('data-qtype');
	if (box_id != '') {
		$.LoadingOverlay("show");
		var $modal = $('#load_action_popup_modal_show');
		$modal.load('code-open-response-modal.php', {'box_id': box_id, 'qid': qid, 'type': type}, function(res) {
			$.LoadingOverlay("hide");
			if (res != '') {
				$modal.modal('show', {backdrop: 'static', keyboard: false});
			}
			else {
				swal({text: oopsmsg, buttons: false, icon: "error", timer: msgtimer});
			}
		});
	}
});

//-----------Record-Audio-----------------
$('body').on('click', '.rec-audio', function() {
	var box_id	= $(this).attr('data-box-id');
	var qid		= $(this).attr('data-qid');
	var type	= $(this).attr('data-qtype');
	if (box_id != '') {
		$.LoadingOverlay("show");
		var $modal = $('#load_action_popup_modal_show');
		$modal.load('rec-audio-open-response-modal.php', {'box_id': box_id, 'qid': qid, 'type': type}, function(res) {
			$.LoadingOverlay("hide");
			if (res != '') {
				$modal.modal('show', {backdrop: 'static', keyboard: false});
			}
			else {
				swal({text: oopsmsg, buttons: false, icon: "error", timer: msgtimer});
			}
		});
	}
});

//-----------Record-Video----------------
$('body').on('click', '.rec-video', function() {
	var box_id	= $(this).attr('data-box-id');
	var qid		= $(this).attr('data-qid');
	var type	= $(this).attr('data-qtype');
	if (box_id != '') {
		$.LoadingOverlay("show");
		var $modal = $('#load_action_popup_modal_show');
		$modal.load('rec-video-open-response-modal.php', {'box_id': box_id, 'qid': qid, 'type': type}, function(res) {
			$.LoadingOverlay("hide");
			if (res != '') {
				$modal.modal('show', {backdrop: 'static', keyboard: false});
			}
			else {
				swal({text: oopsmsg, buttons: false, icon: "error", timer: msgtimer});
			}
		});
	}
});

//-----------Record-Screen---------------
$('body').on('click', '.rec-screen', function() {
	var box_id	= $(this).attr('data-box-id');
	var qid		= $(this).attr('data-qid');
	var type	= $(this).attr('data-qtype');
	if (box_id != '') {
		$.LoadingOverlay("show");
		var $modal = $('#load_action_popup_modal_show');
		$modal.load('rec-screen-open-response-modal.php', {'box_id': box_id, 'qid': qid, 'type': type}, function(res) {
			$.LoadingOverlay("hide");
			if (res != '') {
				$modal.modal('show', {backdrop: 'static', keyboard: false});
			}
			else {
				swal({text: oopsmsg, buttons: false, icon: "error", timer: msgtimer});
			}
		});
	}
});

//--------------Delete-Assets-------------
$('body').on('click', '.delete_assets', function() {
	var cur   		= $(this);
	var file_name	= cur.attr('data-assets');
	var assetsId	= cur.attr('data-assets-id');
	var inputId		= cur.attr('data-input-id');
	var path		= cur.attr('data-path');
	var dataString  = 'delete_assets='+ true +'&assets_path='+ path +'&file='+ file_name;
	if (file_name){
		swal({
			title: "Are you sure?",
			text: "Delete this answer.",
			icon: "warning",
			buttons: [true, 'Delete'],
			dangerMode: true, }).then((willDelete) => { if (willDelete) {
				$.LoadingOverlay("show");
				$.ajax({
					url: "includes/delete-file.php",
					type: "POST",
					data: dataString,
					cache: false,
					success: function(resdata){
						var res = $.parseJSON(resdata);
						if (res.success == false) {
							$.LoadingOverlay("hide");
							swal({text: res.msg, buttons: false, icon: "error", timer: 1000});
						}
						else if (res.success == true) {
							$(inputId).val('');
							$('#open_res_answer_'+ assetsId).hide();
							$('#open_answer_btn_'+ assetsId).hide();
							$('#action_'+ assetsId).show();
							$.LoadingOverlay("hide");
							swal({text: res.msg, buttons: false, timer: 1000});
						}
					},error: function() {
						swal({text: oopsmsg, buttons: false, icon: "error", timer: msgtimer});
					}
				});
			} else { swal({text: 'Your file is safe', buttons: false, timer: 1000}); }
		});
	}
});

//--------------Delete-TTS-------------
$('body').on('click', '.delete_tts', function() {
	var cur   		= $(this);
	var assetsId	= cur.attr('data-assets-id');
	var inputId		= cur.attr('data-input-id');
	swal({
		title: "Are you sure?",
		text: "Delete this Text.",
		icon: "warning",
		buttons: [true, 'Delete'],
		dangerMode: true, }).then((willDelete) => { if (willDelete) {
			$.LoadingOverlay("show");
			$('#tts_data_'+ assetsId).val('');
			$('#open_res_answer_'+ assetsId).hide();
			$('#open_answer_btn_'+ assetsId).hide();
			$('#open_res_answer_'+ assetsId +' .ORTextbox').hide().html('');
			$('#action_'+ assetsId).show();
			$.LoadingOverlay("hide");
		} else { 
			$.LoadingOverlay("hide");
			swal({text: 'Your Text Data is safe', buttons: false, timer: 1000}); 
		}
	});
});

/* Send-Open-Response-Data */
$('body').on('click', '.submitbtn', function() {
	var curData	= $(this);
	var qid 	= curData.data('qid');
	var cur  	= curData.data('cur');
	var next 	= curData.data('target');
	var report	= curData.data('show-report');
	var option	= $('#option_' + cur);
	var qtype 	= option.data('qtype');
	if (qtype) {
		file_name	= $('#file_name_'+ cur).val();
		tts_data  	= $('#tts_data_'+ cur).val();
		tts_data  	= tts_data.replace(/'/g, "\\'");
		code_data  	= btoa($('#code_data_'+ cur).val());
		if (file_name) {
			var fileDataAttemptString = {"req":"Insert", "table":"open_res_tbl", "data":{"uid":uid, "scenario_id":simId, "question_type": qtype, "question_id": qid, "file_name": file_name, "userid": userid}};
			$.ajax({
				type: 'POST',
				url: apiurl,
				data: { single_data : JSON.stringify(fileDataAttemptString)}, /* Use single_data for send a single post data */
				cache: false,
				dataType: "json",
				crossDomain: true,
				success: function(resdata){
					var res = resdata;
					if (res.status == 'Succes') {
						console.log('score added.!');
					}
				},error: function() {
					swal({text: oopsmsg, buttons: false, icon: "error", timer: msgtimer});
				}
			});
		}
		else if (tts_data) {
			var ttsDataAttemptString = {"req":"Insert", "table":"open_res_tbl", "data":{"uid":uid, "scenario_id":simId, "question_type": qtype, "question_id": qid, "tts_data": tts_data, "userid": userid}};
			$.ajax({
				type: 'POST',
				url: apiurl,
				data: { single_data : JSON.stringify(ttsDataAttemptString)}, /* Use single_data for send a single post data */
				cache: false,
				dataType: "json",
				crossDomain: true,
				success: function(resdata){
					var res = resdata;
					if (res.status == 'Succes') {
						console.log('score added.!');
					}
				},error: function() {
					swal({text: oopsmsg, buttons: false, icon: "error", timer: msgtimer});
				}
			});
		}
		else if (code_data) {
			var codeDataAttemptString = {"req":"Insert", "table":"open_res_tbl", "data":{"uid":uid, "scenario_id":simId, "question_type": qtype, "question_id": qid, "code": code_data, "userid": userid}};
			$.ajax({
				type: 'POST',
				url: apiurl,
				data: { single_data : JSON.stringify(codeDataAttemptString)}, /* Use single_data for send a single post data */
				cache: false,
				dataType: "json",
				crossDomain: true,
				success: function(resdata){
					var res = resdata;
					if (res.status == 'Succes') {
						console.log('score added.!');
					}
				},error: function() {
					swal({text: oopsmsg, buttons: false, icon: "error", timer: msgtimer});
				}
			});
		}
	}
	if (report){
		timer.trigger('click');
		swal({title: "",
			  text: "Your answers have been submitted to your educator. Performance report will be generated once educator evaluates your responses.",
			  icon: "success",
			  buttons: [false, 'Close'],
			  closeOnClickOutside: false,
			  closeOnEsc: false,
			  dangerMode: true }).then((willDelete) => { if (willDelete) { window.location.href = 'assigned-simulations.php'; }
		});
	}
	else {
		$('.MatchingQusBanner_'+ cur).fadeOut();
		$('.MatchingQusBanner_'+ next).fadeIn(1000);
	}
});
