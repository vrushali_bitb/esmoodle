// JavaScript Document
	var msgtimer = 2000;
	var oopsmsg  = 'Oops. something went wrong please try again.';
		
	$("#show_hide_password a").on('click', function(event) {
        event.preventDefault();
        if($('#show_hide_password input').attr("type") == "text"){
            $('#show_hide_password input').attr('type', 'password');
            $('#show_hide_password i').addClass( "fa-eye-slash");
            $('#show_hide_password i').removeClass( "fa-eye");
        }else if($('#show_hide_password input').attr("type") == "password"){
            $('#show_hide_password input').attr('type', 'text');
            $('#show_hide_password i').removeClass( "fa-eye-slash");
            $('#show_hide_password i').addClass( "fa-eye");
        }
    });
	
	$("#show_hide_password1 a").on('click', function(event) {
		event.preventDefault();
		if($('#show_hide_password1 input').attr("type") == "text"){
			$('#show_hide_password1 input').attr('type', 'password');
			$('#show_hide_password1 i').addClass( "fa-eye-slash");
			$('#show_hide_password1 i').removeClass( "fa-eye");
		}else if($('#show_hide_password1 input').attr("type") == "password"){
			$('#show_hide_password1 input').attr('type', 'text');
			$('#show_hide_password1 i').removeClass( "fa-eye-slash");
			$('#show_hide_password1 i').addClass( "fa-eye");
		}
	});
	
	$("#loginregerstion").on('submit', (function(e) {
		e.preventDefault();
		$.LoadingOverlay("show");
		$.ajax({
			url: 'login-regis-process.php',
			type: "POST",
			data: new FormData(this),
			contentType: false,
			cache: false,
			processData:false,
			success: function(resdata) {
				var res = $.parseJSON(resdata);
				if (res.success == true) {
					swal({text: res.msg, buttons: false, icon: "success", timer: msgtimer});
					setTimeout(function() { window.location.href = res.url; }, msgtimer);
				}
				else if (res.success == false) {
					swal({text: res.msg, buttons: false, icon: "warning", timer: msgtimer});
					$("#refresh_characters").click();
					$('#captcha_code').val('');
				}
				$.LoadingOverlay("hide");
			},error: function() {
				swal({text: oopsmsg, buttons: false, icon: "error", timer: msgtimer});
				$.LoadingOverlay("hide");
			}
		});
	}));
	
	$("#regisFrom").on('submit', (function(e) {
		e.preventDefault();
		$.LoadingOverlay("show");
		$.ajax({
			url: 'includes/process.php',
			type: "POST",
			data: new FormData(this),
			contentType: false,
			cache: false,
			processData:false,
			success: function(resdata) {
				var res = $.parseJSON(resdata);
				if (res.success == true) {
					swal({text: res.msg, buttons: false, icon: "success", timer: msgtimer});
					setTimeout(function() { window.location.href = res.return_url; }, msgtimer);
				}
				else if (res.success == false) {
					swal({text: res.msg, buttons: false, icon: "warning", timer: msgtimer });
					$("#refresh_characters").click();
					$('#captcha_code').val('');
				}
				$('#btnSubmit').html('Register').removeAttr('disabled', true);
				$.LoadingOverlay("hide");
			},error: function() {
				$('#btnSubmit').html('Register').removeAttr('disabled', true);
				swal({text: oopsmsg, buttons: false, icon: "error", timer: msgtimer });
				$.LoadingOverlay("hide");
			}
		});
	}));
