<?php 
ob_start();
session_start();
if (isset($_SESSION['username'])):
	$user 	= $_SESSION['username'];
	$role 	= $_SESSION['role'];
	$userid = $_SESSION['userId'];
	$domain = (isset($_SESSION['domain_name'])) ? $_SESSION['domain_name'] : $_SERVER['HTTP_HOST'];
else:
    header('location: index.php');
endif;
require_once 'config/db.class.php';
require_once 'includes/header.php';
$db 		= new DBConnection();
$sim_id		= (isset($_GET['sim_id'])) ? $_GET['sim_id'] : die('Error');
$ques_id	= (isset($_GET['ques_id'])) ? base64_decode($_GET['ques_id']) : '';
$save		= (isset($_GET['save']) && $_GET['save'] == 'false') ? FALSE : TRUE;
$simData	= $db->getScenario($sim_id);
$simID 		= $simData['scenario_id'];
$brand		= $db->getSimBranding($simID);
$uid 		= $db->UID();
if ($save):
	$tAttempt	= $db->getTotalAttempt($simID, $userid);
	$attempt	= $db->getAssignSimOtherData($simID, $userid)[0];
	if ( ! empty($attempt['no_attempts']) && $tAttempt >= $attempt['no_attempts']):
		echo "<script>alert('Number of attempts of this simulation is completed.');</script>";
		echo "<script>window.location.href='learner-dashboard.php';</script>";
		exit;
	endif;
endif;
$path		= $db->getBaseUrl('scenario/upload/'.$domain.'/');
$root_path	= $db->rootPath() . 'scenario/upload/'.$domain.'/';
$aPoster	= $db->getBaseUrl('scenario/img/audio-poster.jpg');
$noImage	= $db->getBaseUrl('scenario/img/palceholder.jpg');
$vtheme		= 'forest'; #city, fantasy, forest, sea
if ( ! empty($brand['font_type'])):
	$type = str_replace('+', ' ', $brand['font_type']); ?>
	<style type="text/css">
	@import url("https://fonts.googleapis.com/css2?family=<?php echo $brand['font_type'] ?>:wght@100;200;300;400;500;600;700;800;900&display=swap");
	body { font-family: '<?php echo $type ?>' !important; }
	h1,h2,h3,h4,h5,h6 { font-family: '<?php echo $type ?>' !important; }
	p { font-family: '<?php echo $type ?>' !important; }
	</style>
<?php endif; ?>
<script type="text/javascript">
	window.addEventListener('beforeunload', function (e) {
	  // Cancel the event
	  e.preventDefault(); // If you prevent default behavior in Mozilla Firefox prompt will always be shown
	  // Chrome requires returnValue to be set
	  e.returnValue = '';
	});
	
	function disableF5(e) { if ((e.which || e.keyCode) == 116 || (e.which || e.keyCode) == 82) e.preventDefault(); };
	
	$(document).ready(function(){
		$(document).on("keydown", disableF5);
	});
	
	$(document).bind("contextmenu", function(e){
		return false;
	});
	
	history.pushState(null, null, location.href);
    window.onpopstate = function() {
        history.go(1);
    };
	
	var simBrand = <?php echo json_encode($brand); ?>

	$.LoadingOverlay("show");
</script>
<style type="text/css">
	.linear_classic {
		background-size: cover;
		overflow:hidden;
		height:100vh;
		background: #000 !important;
	}
	<?php if ( ! empty($brand['option_bg'])): ?>
	.questioninput:before { background: <?php echo $brand['option_bg']; ?>; }
	.questioninput:after { background: <?php echo $brand['option_bg']; ?>; }
	.rectangle1:after { background: <?php echo $brand['option_bg']; ?>; }
	<?php endif; ?>
	.video-js .vjs-time-control { display:block; }
	.video-js .vjs-remaining-time { display:none; }
</style>
<div class="menu-banner-box videmenu-box">
	<div class="time-banner-box">
    	<button class="V_S-tetail" id="view_sim" data-sim-id="<?php echo $sim_id; ?>">
    		<img class="Deskview" src="img/list/view_scenario_b.svg" />
        	<img class="mobview" src="img/list/view_scenario.svg" />
        	View Scenario
    	</button>
  	</div>
    <!---SIM-Timer-->
    <div class="timer-banner">
    	<div class="time-text">
        	<span class="timer-pause" data-minutes-left="<?php echo $simData['duration'] ?>" style="pointer-events: none;"></span>
    	</div>
  	</div>
	<?php if ($save): ?>
    <div class="close-banner">
    	<img class="svg" src="img/list/close_learner.svg" />
    </div>
	<?php endif; ?>
</div>
<script type="text/javascript">
	var timer = $('.timer-pause').startTimer({
	onComplete: function(element){
		<?php if ($save): ?>
			$('.signbtn1').show();
		<?php else: ?>
			swal({text: "Simulation time end. please relaunch.!", icon: "info"});
		<?php endif; ?>
	},
	allowPause: true
	});
</script>
<div class="linear_classic videotempQus">
	<input type="hidden" name="uid" id="uid" value="<?php echo $uid; ?>" />
	<input type="hidden" name="simId" id="simId" value="<?php echo $simID; ?>" />
	<input type="hidden" name="userid" id="userid" value="<?php echo $userid; ?>" />
	<input type="hidden" name="save" id="save" value="<?php echo $save; ?>" />
	<input type="hidden" name="apiurl" id="apiurl" value="<?php echo $db->getBaseUrl('api-process.php') ?>" />
	<div class="flexQTeMP flexQTeMPnochar">
	<?php 
	$qSql = $db->prepare("SELECT * FROM question_tbl WHERE scenario_id = '". $simID ."' ORDER BY qorder = 0, qorder"); $qSql->execute();
	$totalQ = $qSql->rowCount();
	if ($totalQ > 0):
		$q = 1; $vqtime = 0;
		$qdata = $qSql->fetchAll(PDO::FETCH_ASSOC);
		foreach ($qdata as $qrow):
			$shuffle = ( ! empty($qrow['shuffle'])) ? TRUE : FALSE;
			$ansData = $db->getAnswersByQuestionId(md5($qrow['question_id']), $shuffle);
			$qtype   = $qrow['question_type'];
			if ( ! empty($ques_id) && $ques_id == $qrow['question_id']):
				$show = 'style="display:block"';
			elseif (empty($ques_id) && $q == 1):
				$show = 'style="display:block"';
			else:
				$show = 'style="display:none"';
			endif;
			if ($vqtime == 0):
				$videoq_start_time	= '00:00';
				$videoq_end_time   	= (isset($qdata[$vqtime])) ? $qdata[$vqtime]['videoq_cue_point'] : '00:00';
			else:
				$new_vtime 			= $vqtime - 1;
				$videoq_start_time 	= (isset($qdata[$new_vtime])) ? $qdata[$new_vtime]['videoq_cue_point'] : '00:00';
				$videoq_end_time   	= (isset($qdata[$vqtime])) ? $qdata[$vqtime]['videoq_cue_point'] : '00:00';
			endif; ?>
			<div class="MatchingQusBannerlunch MatchingQusBanner_<?php echo $qrow['question_id']; ?>" <?php echo $show; ?>>
				<div class="Matchingcontainer VideoMatchingcontainer">
					<div class="launchvideopatch" style="display:none;"></div>
					<div class="videopatch">
						<?php if ( ! empty($qrow['videoq_cue_point'])){ ?>
						<video id="qvideo-<?php echo $qrow['question_id']; ?>" class="video-js vjs-theme-<?php echo $vtheme ?> videoque quesvideo-<?php echo $qrow['question_id'];?>" webkit-playsinline="" playsinline="" preload="auto" width="640" height="264" data-vtype='single' data-setup="{}" <?php echo ($q == 1) ? 'autoplay="autoplay"' : ''; ?> data-start-time="<?php echo ($videoq_start_time == '00:00' || empty($videoq_start_time)) ? '0' : $db->hmsToSeconds($videoq_start_time); ?>" data-end-time="<?php echo $db->hmsToSeconds($videoq_end_time); ?>" data-type="video" data-qtype="<?php echo $qtype ?>" data-video-id="<?php echo $qrow['question_id']; ?>">
							<source src="<?php echo $path . $qrow['videoq_media_file'] ?>" type="video/<?php echo strtolower(pathinfo($qrow['videoq_media_file'], PATHINFO_EXTENSION)); ?>" />
						</video>
						<?php } else { ?>
						<video id="qvideo-<?php echo $qrow['question_id']; ?>" class="video-js vjs-theme-<?php echo $vtheme ?> videoque quesvideo-<?php echo $qrow['question_id'];?>" webkit-playsinline="" playsinline="" preload="auto" width="640" height="264" data-vtype='multiple' data-setup="{}" <?php echo ($q == 1) ? 'autoplay="autoplay"' : ''; ?> data-start-time="" data-end-time="" data-type="video" data-qtype="<?php echo $qtype ?>" data-video-id="<?php echo $qrow['question_id']; ?>">
							<source src="<?php echo $path . $qrow['videoq_media_file'] ?>" type="video/<?php echo strtolower(pathinfo($qrow['videoq_media_file'], PATHINFO_EXTENSION)); ?>" />
						</video>
						<?php } ?>
					</div>
					<div class="overlay">
						<div class="Questmtf Videoquestion" style="display:none;">
							<div class="MatchingQusText fade-in">
								<div class="qus-text">
									<h3>QUESTION <?php echo $q .'/'. $totalQ; ?></h3>
									<p><?php echo stripslashes($qrow['questions']); ?></p>
								</div>
								<div class="qusassestbox">
									<div class="Qusimg">
									<?php if ( ! empty($qrow['audio']) && file_exists($root_path . $qrow['audio'])): ?>
									<audio id="qvideo-<?php echo $q ?>" class="video-js vjs-theme-<?php echo $vtheme ?>" controls preload="auto" width="300px" height="100px" data-setup="{}" data-type="audio" poster="<?php echo $aPoster ?>">
										<source src="<?php echo $path . $qrow['audio'] ?>" type="audio/<?php echo strtolower(pathinfo($qrow['audio'], PATHINFO_EXTENSION)); ?>" />
									</audio>
									<?php elseif ( ! empty($qrow['video']) && file_exists($root_path . $qrow['video'])): ?>
									<a href="<?php echo $path . $qrow['video']; ?>" data-fancybox data-caption="<?php echo stripslashes($qrow['questions']); ?>" class="qassets_icon">
										<video id="qvideo-<?php echo $q ?>" class="video-js vjs-theme-<?php echo $vtheme ?>" controls preload="auto" width="640" height="264" data-setup="{}" data-type="video">
											<source src="<?php echo $path . $qrow['video']; ?>" type="video/<?php echo strtolower(pathinfo($qrow['video'], PATHINFO_EXTENSION)); ?>" />
										</video>
									</a>
									<?php elseif ( ! empty($qrow['screen']) && file_exists($root_path . $qrow['screen'])): ?>
									<a href="<?php echo $path . $qrow['screen']; ?>" data-fancybox data-caption="<?php echo stripslashes($qrow['questions']); ?>" class="qassets_icon">
										<video id="qvideo-<?php echo $q ?>" class="video-js vjs-theme-<?php echo $vtheme ?>" controls preload="auto" width="200" height="100" data-setup="{}" data-type="video">
											<source src="<?php echo $path . $qrow['screen']; ?>" type="video/<?php echo strtolower(pathinfo($qrow['screen'], PATHINFO_EXTENSION)); ?>" />
										</video>
									</a>
									<?php elseif ( ! empty($qrow['image']) && file_exists($root_path . $qrow['image'])): ?>
									<a href="<?php echo $path . $qrow['image']; ?>" data-fancybox data-caption="<?php echo stripslashes($qrow['questions']); ?>" class="qassets_icon">
										<img class="img-fluid" src="<?php echo $path . $qrow['image']; ?>">
									</a>
									<?php elseif ( ! empty($qrow['document']) && file_exists($root_path . $qrow['document'])): ?>
									<a href="javascript:;" data-src="<?php echo $path . $qrow['document']; ?>" data-fancybox data-type="iframe" data-caption="<?php echo stripslashes($qrow['questions']); ?>" class="document">
										<img class="img-fluid pdfclissicsIm" src="img/icons/pdf_icon.svg" />
									</a>
									<?php elseif ( ! empty($qrow['speech_text'])): ?>
									<button onclick="speak('div.intro_<?php echo $q ?>')">
										<i id="playicon" class="fa fa-play playicon" aria-hidden="true"></i>
										<i id="pauseicon" class="fa fa-pause pauseicon" style="display:none" aria-hidden="true"></i></button>
										<div class="intro_<?php echo $q ?>" style="display:none;"><?php echo $qrow['speech_text'] ?></div>
									<?php endif; ?>
									</div>
								</div>
							</div>
						</div>
						<?php #------MCQ------
						if ($qtype == 4): ?>
						<div class="MCQOption bannermcq" id="option_<?php echo $qrow['question_id']; ?>" data-qtype="<?php echo $qtype ?>" style="display:none;">
							<?php foreach ($ansData as $mcq_data): ?>
							<div class="row options option_brand advertise_container fade-in" data-qtype="<?php echo $qtype ?>" data-qid="<?php echo $mcq_data['question_id'] ?>" data-aid="<?php echo $mcq_data['answer_id'] ?>" data-box-id="<?php echo $q; ?>">
								<div class="multiQusChecK"><div class="Check-boxQB Check-boxMCQ"><i class="fa fa-circle" aria-hidden="true"></i></div></div>
								<div class="multiQustext"><?php echo stripslashes($mcq_data['choice_option']); ?></div>
							</div>
							<?php endforeach; ?>
							<div class="submitMTFcenter col-sm-12">
								<button type="button" id="btn_<?php echo $q; ?>" class="btn submitbtn fade-in" data-show-report="<?php echo ($totalQ == $q) ? 'true' : 'false'; ?>" data-target="<?php echo $q + 1 ?>" data-cur="<?php echo $qrow['question_id']; ?>" data-sim-id="<?php echo $sim_id ?>" data-qid="<?php echo $qrow['question_id'] ?>">SUBMIT</button>
							</div>
						</div>
						<?php #------SWIPING------
						elseif ($qtype == 7): ?>
						<div class="SwipeOption" id="option_<?php echo $qrow['question_id']; ?>" data-qtype="<?php echo $qtype ?>" style="display:none;">
							<div class="home-demo">
								<div class="row">
									<div class="large-12 columns">
										<div class="owl-carousel fadeIn swipingcoursal">
										<?php foreach ($ansData as $swipe_data): ?>
										<div class="swipeoptions option_brand" data-qtype="<?php echo $qtype ?>" data-qid="<?php echo $swipe_data['question_id'] ?>" data-aid="<?php echo $swipe_data['answer_id'] ?>">
											<div class="multiDBDboxAllIGN">
												<?php $swip_cls = 'dd_twrap'; if ( ! empty($swipe_data['image']) && file_exists($root_path . $swipe_data['image'])): $swip_cls = ''; ?>
												<a href="<?php echo $path . $swipe_data['image']; ?>" data-fancybox data-caption="<?php echo stripslashes($swipe_data['choice_option']); ?>" class="qassets_icon"><img class="feedimage" src="<?php echo $path . $swipe_data['image'] ?>" /></a>
												<?php endif; ?>
												<div class="textwraping <?php echo $swip_cls; ?>"><?php echo stripslashes($swipe_data['choice_option']); ?></div>
											</div>
										</div>
										<?php endforeach; ?>
										</div>
										<div class="submitMTFcenter col-sm-12">
											<button type="button" id="btn_<?php echo $q; ?>" class="btn submitbtn fadeIn" data-show-report="<?php echo ($totalQ == $q) ? 'true' : 'false'; ?>" data-target="<?php echo $q + 1 ?>" data-cur="<?php echo $qrow['question_id']; ?>" data-sim-id="<?php echo $sim_id ?>" data-qid="<?php echo $qrow['question_id'] ?>">SUBMIT</button>
										</div>
									</div>
								</div>
							</div>
						</div>
						<?php #------D&D------
						elseif ($qtype == 8): ?>
						<div class="Drag_Drop imgDNDS" id="option_<?php echo $qrow['question_id']; ?>" data-qtype="<?php echo $qtype ?>" style="display:none;">
							<div class="row">
								<div class="flex33 DCbanner DCbannerClassic fade-in">
								<?php $dnd_choice_sql = "SELECT question_id, sub_option, image FROM sub_options_tbl WHERE question_id = '". $qrow['question_id'] ."'";
									$dnd_choice = $db->prepare($dnd_choice_sql); $dnd_choice->execute();
									if ($dnd_choice->rowCount() > 0):
										$dnd_choice_data = $dnd_choice->fetchAll(PDO::FETCH_ASSOC);
										foreach ($dnd_choice_data as $dnd_choice_row): ?>
										<div class="flexheight33 multiDBDbanner option_brand">
											<div class="multiDBDbox">
												<?php if ( ! empty($dnd_choice_row['image']) && file_exists($root_path . $dnd_choice_row['image'])): ?>
												<a href="<?php echo $path . $dnd_choice_row['image']; ?>" data-fancybox data-caption="<?php echo stripslashes($dnd_choice_row['sub_option']); ?>" class="qassets_icon"><img src="<?php echo $path . $dnd_choice_row['image']; ?>" /></a>
												<?php endif; ?>
												<div class="textwraping <?php echo (empty($dnd_choice_row['image'])) ? 'dd_twrap' : ''; ?>"><?php echo stripslashes($dnd_choice_row['sub_option']); ?></div>
											</div>
										</div>
										<?php endforeach; endif; ?>
										<div class="flexheight33 multiDBDbanner BGMUltTrans">
											<?php $dndi = 1; foreach ($dnd_choice_data as $data): ?>
											<div class="dragBox drop option_brand dragBox<?php echo $dndi.'_'.$q ?>" data-qtype="<?php echo $qtype ?>" data-qid="<?php echo $data['question_id'] ?>"></div>
											<?php $dndi++; endforeach; ?>
										</div>
								</div>
								<div class="flex33 DROPclassicbanner DROPclassicA fade-in">
								<?php if (count($ansData) > 0): foreach ($ansData as $dnd_match_row): ?>
									<div class="multiDBDbox dragdrop option_brand" data-aid="<?php echo $dnd_match_row['answer_id'] ?>">
										<?php if ( ! empty($dnd_match_row['image']) && file_exists($root_path . $dnd_match_row['image'])): ?>
										<a href="<?php echo $path . $dnd_match_row['image']; ?>" data-fancybox data-caption="<?php echo stripslashes($dnd_match_row['choice_option']); ?>" class="qassets_icon"><img src="<?php echo $path . $dnd_match_row['image']; ?>" /></a>
										<?php endif; ?>
										<div class="textwraping"><?php echo stripslashes($dnd_match_row['choice_option']); ?></div>
									</div>
								<?php endforeach; endif; ?>
								</div>
								<div class="submitMTFcenter col-sm-12">
									<button type="button" id="btn_<?php echo $q; ?>" class="btn submitbtn fade-in" data-show-report="<?php echo ($totalQ == $q) ? 'true' : 'false'; ?>" data-target="<?php echo $q + 1 ?>" data-cur="<?php echo $qrow['question_id']; ?>" data-sim-id="<?php echo $sim_id ?>" data-qid="<?php echo $qrow['question_id'] ?>">SUBMIT</button>
								</div>	
							</div>
						</div>
						<?php endif; ?>
					</div>
				</div>
			</div>
			<?php if ($totalQ == $q): ?>
			<span class="report-view">
				<div class="signbtn1 signbtnRepot" style="display:none;">
					<div class="repotView"><a href="learner-progress-branching-classic-video.php?report=true&sid=<?php echo $sim_id; ?>" target="_blank">
						<img src="img/list/report_simicon.svg" />VIEW REPORT</a>
					</div>
				</div>
			</span>
			<?php endif;
			if ($q == 1): ?>
			<script type="text/javascript">
				var vid			= '<?php echo $qrow['question_id']; ?>';
				var qdata 		= document.getElementById('qvideo-'+ vid);
				var start_time	= qdata.getAttribute('data-start-time');
				var end_time	= qdata.getAttribute('data-end-time');
				var type 		= qdata.getAttribute('data-type');
				var qtype		= qdata.getAttribute('data-qtype');
				var vtype		= qdata.getAttribute('data-vtype');
				var box_id		= qdata.getAttribute('data-video-id');
				if (end_time != '') {
					var qplayer = videojs('qvideo-'+ vid);
					if (start_time == 0) {
						qplayer.on('timeupdate', function(e){
							if (qplayer.currentTime() >= end_time){
								qplayer.pause();
								$('.MatchingQusBanner_'+ vid +' .Matchingcontainer .Videoquestion').fadeIn(1000);
								$('.MatchingQusBanner_'+ vid +' .launchvideopatch').fadeIn(1000);
								$('.MatchingQusBanner_'+ vid +' #option_'+ box_id).fadeIn(1000);
							}
						});
					}
					else if (start_time != '' || start_time != 0) {
						qplayer.currentTime(start_time);
						qplayer.play();
						qplayer.on('timeupdate', function(e){
							if (qplayer.currentTime() >= end_time) {
								qplayer.pause();
								$('.MatchingQusBanner_'+ vid +' .Matchingcontainer .Videoquestion').fadeIn(1000);
								$('.MatchingQusBanner_'+ vid +' .launchvideopatch').fadeIn(1000);
								$('.MatchingQusBanner_'+ vid +' #option_'+ box_id).fadeIn(1000);
							}
						});
					}
				}
				else if (start_time == '' || end_time == '') {
					var qplayer = videojs('qvideo-'+ vid);
					qplayer.on('ended', function(e) {
						$('.MatchingQusBanner_'+ vid +' .Matchingcontainer .Videoquestion').fadeIn(1000);
						$('.MatchingQusBanner_'+ vid +' .launchvideopatch').fadeIn(1000);
						$('.MatchingQusBanner_'+ vid +' #option_'+ box_id).fadeIn(1000);
					});
				}
			</script>
		<?php endif; $q++; $vqtime++; endforeach; endif; ?>
	</div>
</div>
<div id="load_popup_modal_show" class="modal fade" tabindex="-1" data-keyboard="false" data-backdrop="static"></div>
<script src="content/js/articulate.js"></script>
<script src="content/js/owlcarousel/owl.carousel.min.js"></script>
<script src="content/js/launch-sim.js?v=<?php echo time() ?>"></script>
<script src="content/js/sim-branding.js?v=<?php echo time() ?>"></script>
<script type="text/javascript">
	$('.options').click(function() {
		var aid = $(this).data('aid');
		var bid = $(this).data('box-id');
		$('#btn_'+ bid).attr('data-aid', aid);
		$('.options').removeClass('press');
		$(this).addClass('press');
		$('.btn').removeAttr('disabled', true);
	});
	//-----DRAG AND DROP---
	$(function() {
		//draggable
        $('.dragdrop').draggable({
          revert: true,
          placeholder: true,
          droptarget: '.drop',
          drop: function(evt, droptarget) {
			if ($(droptarget).find('.dragdrop').length == 0){
				//== empty drop target, simply drop
				$(this).appendTo(droptarget).draggable();
				$(".flexheight33.multiDBDbanner.BGMUltTrans").addClass("BGMUltTrans1");
			}
			else {
				//== swapping drops
				var d1 = this;
				var d2 = $(droptarget).find('.dragdrop').eq(0);
				$(d2).appendTo($(d1).parent()).draggable();
				$(d1).appendTo(droptarget).draggable();
			}
          }
        });
	});
	//-----------Feedback-----------------
	$('body').on('click', '.submitbtn', function(){
		var cur		= $(this);
		var id  	= cur.data('cur');
		var option	= $('#option_' + id);
		var qtype 	= option.data('qtype');
		var report	= cur.data('show-report');
		var target	= cur.data('target');
		var current	= cur.data('cur');
		var sid		= cur.data('sim-id');
		var url 	= 'view-branching-video-classic-feedback-modal.php';
		var $modal	= $('#load_popup_modal_show');
		$.LoadingOverlay("show");
		/* MCQ */
		if (qtype == 4) {
			var mcqdata = $('#option_'+ id +' .press');
			if (mcqdata.data('aid')) {
				$modal.load(url, {'report': report, 'target': target, 'qType':qtype,'current': current, 'sid': sid, 'qid': mcqdata.data('qid'), 'aid': mcqdata.data('aid')}, function(res) {
					$.LoadingOverlay("hide");
					if (res != 'error') {
						$modal.modal('show', {backdrop: 'static', keyboard: false});
					}
					else { swal({text: 'Oops, something went wrong. Please try again later', buttons: false, icon: "error", timer: 1000}); }
				});
			}
			else { swal({text: 'Oops, something went wrong. Please try again later', buttons: false, icon: "error", timer: 1000}); }
		}
		/* SWIPING */
		else if (qtype == 7) {
			var swip = $('#option_'+ id +' .owl-stage .center .swipeoptions');
			if (swip.data('aid')) {
				$modal.load(url, {'report': report, 'target': target, 'current': current, 'sid': sid, 'qid': swip.data('qid'), 'aid': swip.data('aid')}, function(res) {
					$.LoadingOverlay("hide");
					if (res != 'error') {
						$modal.modal('show', {backdrop: 'static', keyboard: false});
					}
					else { swal({text: 'Oops, something went wrong. Please try again later', buttons: false, icon: "error", timer: 1000}); }
				});
			}
			else { swal({text: 'Oops, something went wrong. Please try again later', buttons: false, icon: "error", timer: 1000}); }
		}
		/* D&D */
		else if (qtype == 8) {
			var dd_data = $('#option_'+ id +' .dragBox');
			if (dd_data.children('div').data('aid')){
				$modal.load(url, {'report': report, 'target': target, 'current': current, 'sid': sid, 'qid': dd_data.data('qid'), 'aid': dd_data.children('div').data('aid')}, function(res) {
					$.LoadingOverlay("hide");
					if (res != 'error') {
						$modal.modal('show', {backdrop: 'static', keyboard: false});
					}
					else { swal({text: 'Oops, something went wrong. Please try again later', buttons: false, icon: "error", timer: 1000}); }
				});
			}
			else { swal({text: 'Oops, something went wrong. Please try again later', buttons: false, icon: "error", timer: 1000}); }
		}
	});
</script>
<?php 
require_once 'includes/footer.php';
