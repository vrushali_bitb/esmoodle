<?php 
session_start();
if (isset($_SESSION['username'])) {
    $user   = $_SESSION['username'];
    $role   = $_SESSION['role'];
    $userid = $_SESSION['userId'];
	$domain = (isset($_SESSION['domain_name'])) ? $_SESSION['domain_name'] : $_SERVER['HTTP_HOST'];
}
else {
    header('location: index.php');
}
require_once 'config/db.class.php';
require_once 'includes/learners_menu.php';
$db 		= new DBConnection();
$sim_id		= (isset($_GET['sim_id']) && ! empty($_GET['sim_id'])) ? $_GET['sim_id'] : '';
$ques_id	= (isset($_GET['ques_id'])) ? base64_decode($_GET['ques_id']) : '';
$sim_data 	= $db->getScenario($sim_id);
$sim_com 	= $db->getScenarioCompetency($sim_id);
$sim_page 	= $db->getSimPage($sim_data['scenario_id']);
$sim_brand 	= $db->getSimBranding($sim_data['scenario_id']);
$sim_type	= $db->getMultiScenarioType($sim_data['author_scenario_type']);
$totalq		= $db->getTotalQuestionsByScenario($sim_id);
$path		= $db->getBaseUrl('scenario/upload/'.$domain.'/');
$uploadpath	= '../scenario/upload/'.$domain.'/';
$root_path	= $db->rootPath() . 'scenario/upload/'.$domain.'/';
$preview 	= 'launch-single-video.php?preview=true&save=false&sim_id='. $sim_id;
$type_name	= $sim_type[0]['type_name'];
$type 		= strtolower($type_name);
$gettype 	= '';
if ($type == 'image and text'):
	$gettype = 'image';
else:
	$gettype = $type;
endif;
$upload_file_name = '';
if ( ! empty($sim_data['image_url']) && file_exists($root_path . $sim_data['image_url'])):
	$upload_file_name = $sim_data['image_url'];
elseif ( ! empty($sim_data['video_url']) && file_exists($root_path . $sim_data['video_url'])):
	$upload_file_name = $sim_data['video_url'];
elseif ( ! empty($sim_data['audio_url']) && file_exists($root_path . $sim_data['audio_url'])):
	$upload_file_name = $sim_data['audio_url'];
elseif ( ! empty($sim_data['web_object_file'])):
	$upload_file_name = $sim_data['web_object_file'];
endif;
$web_object = '';
if ( ! empty($sim_data['web_object'])):
	$web_object = $sim_data['web_object'];
endif;
$ques_val1 = $ques_val2 = $ques_val3 = $ques_val4 = $ques_val5 = $ques_val6 = 0;
if ( ! empty($ques_id)):
	$qsql = "SELECT * FROM question_tbl WHERE question_id = '". $ques_id ."'";
	try {
		$qstmt	 = $db->prepare($qsql); $qstmt->execute();
		$qrow	 = $qstmt->rowCount();
		$qresult = ($qrow > 0) ? $qstmt->fetch() : '';
	}
	catch(PDOException $e) {
		echo "Oops. something went wrong. ".$e->getMessage(); exit;
	}
	$tqsql  = "SELECT * FROM question_tbl WHERE MD5(scenario_id) = '". $sim_id ."'";
	$tqstmt = $db->prepare($tqsql); $tqstmt->execute();
	$tqresult = $tqstmt->fetchAll();
	foreach ($tqresult as $tqueSocre):
		if ( ! empty($tqueSocre['ques_val_1'])) $ques_val1 += $tqueSocre['ques_val_1'];
		if ( ! empty($tqueSocre['ques_val_2'])) $ques_val2 += $tqueSocre['ques_val_2'];
		if ( ! empty($tqueSocre['ques_val_3'])) $ques_val3 += $tqueSocre['ques_val_3'];
		if ( ! empty($tqueSocre['ques_val_4'])) $ques_val4 += $tqueSocre['ques_val_4'];
		if ( ! empty($tqueSocre['ques_val_5'])) $ques_val5 += $tqueSocre['ques_val_5'];
		if ( ! empty($tqueSocre['ques_val_6'])) $ques_val6 += $tqueSocre['ques_val_6'];
	endforeach;
endif; ?>
<link rel="stylesheet" type="text/css" href="content/css/owlcarousel/owl.carousel.min.css" />
<link rel="stylesheet" type="text/css" href="content/css/qustemplate.css" />
<link rel="stylesheet" type="text/css" href="content/css/questionresponsive.css" />
<link rel="stylesheet" type="text/css" href="content/css/template-updated.css" />
<link rel="stylesheet" type="text/css" href="content/css/jquery.fontselect.css" />
<script type="text/javascript" src="content/js/inputmask/jquery.inputmask.js"></script>
<script type="text/javascript" src="content/js/spectrum/spectrum.min.js"></script>
<link rel="stylesheet" type="text/css" href="content/js/spectrum/spectrum.min.css" />
<link rel="stylesheet" type="text/css" href="content/fancybox/jquery.fancybox.min.css" />
<script type="text/javascript" src="content/fancybox/jquery.fancybox.min.js"></script>
<script type="text/javascript">
$.LoadingOverlay("show");
</script>
<style type="text/css">
	body {
		overflow-x:hidden;
		background:#efefef;
	}
	[data-title] {
		font-size: 16px;
		position: relative;
		cursor: help;
	}  
	[data-title]:hover::before {
		content: attr(data-title);
		position: absolute;
		bottom: -20px;
		padding: 0px;
		color: #2c3545;
		font-size: 14px;
		white-space: nowrap;
	}
	span.truncateTxt {
		width: 30px;
		white-space: nowrap;
		overflow: hidden;
		text-overflow: ellipsis;
		float: left;
	}
	:required  {
		background: url('img/list/star.png') no-repeat !important;
		background-position:right top !important;
		background-color: #fff !important;
	}
	.required  {
		background: url('img/list/star.png') no-repeat !important;
		background-position:right top !important;
		background-color: #fff !important;
	}
</style>
<form method="post" class="form-inline" name="add_sim_linear_multiple_details_form" id="add_sim_linear_multiple_details_form">
	<input type="hidden" name="update_single_video_sim" value="1" />
    <input type="hidden" name="sim_id" id="sim_id" value="<?php echo $sim_data['scenario_id'] ?>" />
    <input type="hidden" name="gettype" value="<?php echo $gettype ?>" />
    <input type="hidden" name="character_type" id="character_type" value="" />
    <input type="hidden" name="submit_type" id="submit_type" />
    <input type="hidden" name="get_question_type" id="get_question_type" value="<?php echo ( ! empty($qresult['question_type'])) ? $qresult['question_type'] : '' ?>" />
    <input type="hidden" name="question_type" id="question_type" value="<?php echo ( ! empty($qresult['question_type'])) ? $qresult['question_type'] : 6; ?>" />
    <input type="hidden" name="location" id="location" value="<?php echo $db->getBaseUrl($db->getFile().'?add_data=true&sim_id='. $sim_id); ?>" />
    <div class="sim-side-menu">
        <ul class="side-menu-list nav nav-tabs">
			<li class="Add_Web <?php echo (empty($ques_id)) ? 'active' : ''; ?>"><a data-toggle="tab" href="#Add_Web"><div class="tooltipmenu"><img class="svg addScenario" src="img/list/Add_Learning.svg"/><span class="tooltiptext">Add Learning</span></div></a></li>
			<li class="Add_Scenario"><a data-toggle="tab" href="#Add_Scenario"><div class="tooltipmenu"><img class="svg addScenario" src="img/list/add_Scenario.svg"/><span class="tooltiptext">Add Scenario</span></div></a></li>
            <li class="Add_Branding"><a data-toggle="tab" href="#Add_Branding"><div class="tooltipmenu"><img class="detail_w svg" src="img/list/add_branding.svg"/><span class="tooltiptext">Add Branding</span></div></a></li>
			<li class="main_menu"><a data-toggle="tab" href="#main_menu"><div class="tooltipmenu"><img class="svg add_qa" src="img/list/add_detail.svg"/><span class="tooltiptext">Simulation Details</span></div></a></li>
			<li class="QUeS_menu main_menu <?php echo ( ! empty($ques_id)) ? 'active' : ''; ?>"><a data-toggle="tab" href="#questiontamp"><div class="tooltipmenu"><img class="svg add_qa" src="img/list/add_question1.svg"/><span class="tooltiptext">Question Template</span></div></a></li>
        </ul>
    </div>
    <div class="question_tree_banner tab-content clearfix">
		<!--Add-Web-Object-->
		<div class="bg_selection clearfix tab-pane fade <?php echo (empty($ques_id)) ? 'in active' : ''; ?>" id="Add_Web">
			<div class="question_tree_menu NoBorder" id="question_tree_menu1">
				<div class="Simulation_det stopb clearfix">
					<h3>Add Learning</h3>
					<div class="previewQus Sbtn">
						<a href="<?php echo $preview ?>" target="_blank" title="Preview"><img class="img-fluid" src="img/qus_icon/preview.svg"></a>
					</div>
			    </div>
            	<div class="Group_managment">
					<div class="container">
						<div class="row">
							<div class="col-sm-12">
								<div class="usermanage_main">
									<div class="usermanage-form">
										<div class="row">
											<div class="tille">
												<div class="add_page_option appendSCE">
													<div class="form-group">
														<input type="text" name="web_object_title" class="form-control" placeholder="Title name" autocomplete="off" value="<?php echo ( ! empty($sim_data['web_object_title'])) ? $sim_data['web_object_title'] : ''; ?>" />
													</div>
												</div>
											</div>
											<div class="tilleobj">
												<div class="addsce-img">
													<div class="adimgbox">
														<div class="form-group">
															<input type="file" name="web_file" id="web_file" class="form-control file" />
															<input type="text" class="form-control controls" disabled placeholder="Select Web Object file" value="<?php echo $web_object; ?>" />
															<input type="hidden" name="web_object" id="web_object" value="<?php echo $web_object; ?>" />
															<input type="hidden" name="web_object_type" id="web_object_type" value="application" />
															<div class="browsebtn browsebtntmp">
																<span id="WbDelete" <?php echo ( ! empty($web_object)) ? '' : 'style="display:none"'; ?>>
																	<a href="javascript:void(0);" data-wb="<?php echo ( ! empty($web_object)) ? $web_object : ''; ?>" class="delete_wb" title="Delete File"><i class="fa fa-times" aria-hidden="true"></i></a>
																</span>
															</div>
														</div>
													</div>
													<div class="adimgbox browsebtn"><button class="browse btn btn-primary" type="button">Browse</button></div>
													<div class="adimgbox browsebtn"><button type="button" name="upload_web" id="upload_web" class="btn btn-primary">Upload</button></div>
												</div>
											</div>											
                                         </div>
									</div>
								</div>
							</div>
						</div>
					</div>
                </div>
            </div>
        </div>
		<!--Add-Simulation-->
        <div class="bg_selection clearfix tab-pane fade" id="Add_Scenario">
            <div class="question_tree_menu NoBorder" id="question_tree_menu1">
				<div class="Simulation_det stopb clearfix">
					<h3>Add Scenario</h3>
					<div class="previewQus Sbtn">
						<a href="<?php echo $preview ?>" target="_blank" title="Preview"><img class="img-fluid" src="img/qus_icon/preview.svg"></a>
					</div>
			    </div>
            	<div class="Group_managment">
					<div class="container">
						<div class="row">
							<div class="col-sm-12">
								<div class="usermanage_main">
									<div class="usermanage-form">
										<div class="row">
                                        	<?php if ($gettype != 'text only' && $gettype != 'web object'): ?>
                                            <div class="addsce-img">
                                            	<div class="adimgbox">
                                                    <div class="form-group">
                                                        <input type="file" name="file" id="file" class="form-control file">
                                                        <input type="text" id="scenario_media" class="form-control controls" disabled placeholder="Select <?php echo $gettype; ?> file" value="<?php echo $upload_file_name; ?>">
                                                        <input type="hidden" name="scenario_media_file" id="scenario_media_file" value="<?php echo $upload_file_name; ?>" />
                                                        <input type="hidden" name="scenario_media_file_type" id="scenario_media_file_type" value="<?php echo $gettype; ?>" /> 
														<div class="browsebtn browsebtntmp">
															<span id="splashImg"><?php if ( ! empty($upload_file_name)) : ?><a href="<?php echo $uploadpath . $upload_file_name ?>" target="_blank" title="View Scenario Media File"><i class="fa fa-eye" aria-hidden="true"></i></a><?php endif; ?></span>
															<span id="ImgDelete" <?php echo ( ! empty($upload_file_name)) ? '' : 'style="display:none"'; ?>><a href="javascript:void(0);" data-scenario-media-file="<?php echo ( ! empty($upload_file_name)) ? $upload_file_name : ''; ?>" class="delete_scenario_media_file" title="Delete File"><i class="fa fa-times" aria-hidden="true"></i></a></span>
														</div>
													</div>	
                                                </div>
                                                <div class="adimgbox browsebtn"><button class="browse btn btn-primary" type="button">Browse</button></div>
                                                <div class="adimgbox browsebtn"><button type="button" name="upload" id="upload" class="btn btn-primary">Upload</button></div>
                                            </div>
											<?php elseif ($gettype == 'web object'): ?>
											<div class="addsce-img">
												<div class="adimgbox">
													<div class="form-group">
                                                        <input type="file" name="file" id="file" class="form-control file" />
                                                        <input type="text" class="form-control controls" disabled placeholder="Select <?php echo $gettype; ?> file" value="<?php echo $upload_file_name; ?>" />
                                                        <input type="hidden" name="scenario_media_file" id="scenario_media_file" value="<?php echo $upload_file_name; ?>" />
                                                        <input type="hidden" name="scenario_media_file_type" id="scenario_media_file_type" value="application" />
														<div class="browsebtn browsebtntmp"><span id="ImgDelete" <?php echo ( ! empty($upload_file_name)) ? '' : 'style="display:none"'; ?>><a href="javascript:void(0);" data-wb-file="<?php echo ( ! empty($upload_file_name)) ? $upload_file_name : ''; ?>" class="delete_wb_file" title="Delete File"><i class="fa fa-times" aria-hidden="true"></i></a></span></div>
                                                    </div>
                                                </div>
												<div class="adimgbox browsebtn"><button class="browse btn btn-primary" type="button">Browse</button></div>
                                                <div class="adimgbox browsebtn"><button type="button" name="upload" id="upload" class="btn btn-primary">Upload</button></div>
                                            </div>
                                            <?php endif; ?>
											<div class="user_text addtext">Add Tabs</div>
											<div class="col-sm-12">
												<?php if ( ! empty($sim_page)):
												$pagei = 1; foreach ($sim_page as $pdata): ?>
                                                <div class="plus_page_option">
                                                    <div class="add_page_option appendSCE">
                                                        <div class="form-group">
                                                            <input type="text" name="sim_page_name[]" class="form-control" placeholder="Page name" required="required" value="<?php echo $pdata['sim_page_name'] ?>" />
                                                        </div>
                                                        <div class="form-group">
                                                            <textarea name="sim_page_desc[]" id="sim_page_desc_<?php echo $pagei ?>" class="form-control sim_page_desc"><?php echo $pdata['sim_page_desc'] ?></textarea>
                                                        </div>
                                                        <?php if ($pagei == 1): ?>
                                                         <div class="plus_page"><img src="img/icons/plus.png" title="Add More Page" /></div>
                                                         <?php else: ?>
                                                         <div class="remove minusComp"><img src="img/icons/minus.png" title="Remove Page"></div>
                                                         <?php endif; ?>
                                                    </div>                                               
                                                </div>
                                                <?php $pagei++; endforeach; else: ?>
                                            	<div class="plus_page_option">
                                                    <div class="add_page_option appendSCE">
                                                        <div class="form-group">
                                                            <input type="text" name="sim_page_name[]" class="form-control" placeholder="Page name" required="required" />
                                                        </div>
                                                        <div class="form-group">
                                                            <textarea name="sim_page_desc[]" id="sim_page_desc_0" class="form-control"></textarea>
                                                        </div>
                                                         <div class="plus_page"><img src="img/icons/plus.png" title="Add More Page" /></div>
                                                    </div>                                               
                                                </div>
                                                <?php endif; ?>
											</div>
                                         </div>
									</div>
								</div>
							</div>
						</div>
					</div>
                </div>
            </div>
        </div>
        <!--Add-Branding-->
		<div class="main tab-pane fade" id="Add_Branding">
            <div class="main_sim question_tree_menu">
                <div class="main_sim_scroll">
					<div class="Simulation_det stopb clearfix">
						<h3>Adding Branding</h3>
						<div class="previewQus Sbtn">
							<a href="<?php echo $preview ?>" target="_blank" title="Preview"><img class="img-fluid" src="img/qus_icon/preview.svg"></a>
						</div>
					</div>
					<div class="Group_managment">
						<div class="container">
                        	<div class="branding-color">
								<div class="branding-flex brandingbox">
									<div class="coloredittool-box">
										<div class="coloredittool">
											<div class="user_text colorp">Question Background</div>
											<div class="branding-flex"><span class="colorb">Color</span><input class="type-color-on-page" name="ques_bg_color" id="ques_bg_color" /></div>
											<!--div class="branding-flex"><span class="colorb">Transparency</span><input type="number" class="form-control tcounter" id="ques_bg_transp" name="ques_bg_transp" min="0" value="<?php echo ( ! empty($sim_brand['ques_bg_transparency'])) ? $sim_brand['ques_bg_transparency'] : '' ?>"></div-->
										</div>
										<div class="coloredittool">
											<div class="user_text colorp">Option Background</div>
											<div class="branding-flex"><span class="colorb">Color</span><input class="type-color-on-page" name="ques_option_bg_color" id="ques_option_bg_color" /></div>
										</div>
										<div class="coloredittool">
											<div class="user_text colorp">Option Hover</div>
											<div class="branding-flex"><span class="colorb">Color</span><input class="type-color-on-page" name="ques_option_hover_color" id="ques_option_hover_color" /></div>
										</div>
										<div class="coloredittool">
											<div class="user_text colorp">Option Selected</div>
											<div class="branding-flex"><span class="colorb">Color</span><input class="type-color-on-page" name="ques_option_select_color" id="ques_option_select_color" /></div>
										</div>
									</div>
									<div class="coloredittool-box">
										<div class="coloredittool">
											<div class="user_text colorp">Submit button Background</div>
											<div class="branding-flex"><span class="colorb">Color</span><input class="type-color-on-page" name="btn_bg_color" id="btn_bg_color" /></div>
										</div>
										<div class="coloredittool">
											<div class="user_text colorp">Submit button Hover</div>
											<div class="branding-flex"><span class="colorb">Color</span><input class="type-color-on-page" name="btn_hover_color" id="btn_hover_color" /></div>
										</div>
										<div class="coloredittool">
											<div class="user_text colorp">Submit button Selected</div>
											<div class="branding-flex"><span class="colorb">Color</span><input class="type-color-on-page" name="btn_select_color" id="btn_select_color" /></div>
										</div>
									</div>
								</div>
							</div>
							<div class="user_text fonts">Font</div>
							<div class="branding-font">
								<div class="branding-flex">
                                	<span class="colorb">Font Type</span>
									<input id="font_type" name="font_type" type="text" />
									<?php if  ( ! empty($sim_brand['font_type'])) : ?>
									<div class="col-sm-2"><button class="cl_ftype btn btn-primary" type="button">Set Default Font Type</button></div>
									<?php endif; ?>
								</div>
								<div class="branding-flex">
                                	<span class="colorb">Font Color</span>
                                    <input class="type-color-on-page" name="font_color" id="font_color" />
                                </div>
                                <div class="branding-flex"><span class="colorb">Font Size</span>
                                  <select name="font_size" id="font_size" class="form-control">
								    <option selected="selected" value="0">Select font size</option>
                                  	<option value="10px" <?php echo ( ! empty($sim_brand['font_size']) && $sim_brand['font_size'] == '10px') ? 'selected="selected"' : '' ?>>10px</option>
                                    <option value="12px" <?php echo ( ! empty($sim_brand['font_size']) && $sim_brand['font_size'] == '12px') ? 'selected="selected"' : '' ?>>12px</option>
                                    <option value="16px" <?php echo ( ! empty($sim_brand['font_size']) && $sim_brand['font_size'] == '16px') ? 'selected="selected"' : '' ?>>16px</option>
                                    <option value="18px" <?php echo ( ! empty($sim_brand['font_size']) && $sim_brand['font_size'] == '18px') ? 'selected="selected"' : '' ?>>18px</option>
                                    <option value="20px" <?php echo ( ! empty($sim_brand['font_size']) && $sim_brand['font_size'] == '20px') ? 'selected="selected"' : '' ?>>20px</option>
                                    <option value="24px" <?php echo ( ! empty($sim_brand['font_size']) && $sim_brand['font_size'] == '24px') ? 'selected="selected"' : '' ?>>24px</option>
                                    <option value="26px" <?php echo ( ! empty($sim_brand['font_size']) && $sim_brand['font_size'] == '26px') ? 'selected="selected"' : '' ?>>26px</option>
                                  </select>
                              </div>
							</div>
						</div>
					</div>
                </div>
            </div>
        </div>
        <!--Add-Sim-Details-->
        <div class="main tab-pane fade" id="main_menu">
            <div class="main_sim question_tree_menu">              
                <div class="Simulation_det stopb clearfix">
                    <h3>Simulation Details</h3>
                    <div class="previewQus Sbtn">
                        <a href="<?php echo $preview ?>" target="_blank" title="Preview"><img class="img-fluid" src="img/qus_icon/preview.svg"></a>
                    </div>
                </div>
                <div class="Group_managment">
					<div class="row">
						<div class="col-sm-12">
							<div class="form-group branching ">
								<label class="branching-text">Simulation Name</label>
								<input type="text" name="sim_title" id="sim_title" placeholder="Simulation Name" value="<?php echo $sim_data['Scenario_title'] ?>" class="form-control required" />
							</div>
							<div class="form-group branching">
								<label class="branching-text">Duration(min)</label>
								<input type="text" class="form-control question-box" name="sim_duration" id="sim_duration" onkeypress="return isNumberKey(event);" required="required" autocomplete="off" value="<?php echo $sim_data['duration'] ?>" />
							</div>
							<div class="form-group branching">
								<label class="branching-text">Passing Score(%)</label>
								<input type="text" class="form-control question-box" name="passing_marks" id="passing_marks" onkeypress="return isNumberKey(event);" required="required" autocomplete="off" value="<?php echo $sim_data['passing_marks'] ?>" />
							</div>
						</div>
					</div>
					<div class="Simulation_det" style="margin-top: 50px;">
						<h3>Create Competency</h3>
					</div>
					<div class="row">
					<?php if ( ! empty($sim_com)): ?>
						<div class="plus_comp_option linaer">
						<?php $i = 1; if ( ! empty($sim_com['comp_col_1'])): ?>
						<div class="add_comp_option">
							<div class="form-group">
								<input type="text" placeholder="Competency Name" class="form-control" name="competency_name_1" value="<?php echo $sim_com['comp_col_1'] ?>" required="required" autocomplete="off" />
							</div>
							<div class="form-group labelbox">
								<label>Competency Score</label>
								<input type="text" class="form-control question-box" name="competency_score_1" onkeypress="return isNumberKey(event);" value="<?php echo $sim_com['comp_val_1'] ?>" required="required" />
							</div>
							<div class="form-group labelbox">
								<label>Weightage</label>
								<input type="text" class="form-control question-box" name="weightage_1" onkeypress="return isNumberKey(event);" value="<?php echo $sim_com['weightage_1'] ?>" required="required" />
							</div>
							<div class="plus_comp"><img src="img/icons/plus.png" title="Add More"></div>
						</div>
						<?php endif;
						if ( ! empty($sim_com['comp_col_2'])): $i += 1; ?>
						<div class="add_comp_option">
							<div class="form-group">
								<input type="text" placeholder="Competency Name" class="form-control" name="competency_name_2" value="<?php echo $sim_com['comp_col_2'] ?>" required="required" autocomplete="off" />
							</div>
							<div class="form-group labelbox">
								<label>Competency Score</label>
								<input type="text" class="form-control question-box" name="competency_score_2" onkeypress="return isNumberKey(event);" value="<?php echo $sim_com['comp_val_2'] ?>" required="required" />
							</div>
							<div class="form-group labelbox">
								<label>Weightage</label>
								<input type="text" class="form-control question-box" name="weightage_2" onkeypress="return isNumberKey(event);" value="<?php echo $sim_com['weightage_2'] ?>" required="required" />
							</div>
							<div class="remove minusComp"><img src="img/icons/minus.png" title="Remove"></div>
						</div>
						<?php endif;
						if ( ! empty($sim_com['comp_col_3'])): $i += 1; ?>
						<div class="add_comp_option">
							<div class="form-group">
								<input type="text" placeholder="Competency Name" class="form-control" name="competency_name_3" value="<?php echo $sim_com['comp_col_3'] ?>" required="required" autocomplete="off" />
							</div>
							<div class="form-group labelbox">
								<label>Competency Score</label>
								<input type="text" class="form-control question-box" name="competency_score_3" onkeypress="return isNumberKey(event);" value="<?php echo $sim_com['comp_val_3'] ?>" required="required" />
							</div>
							<div class="form-group labelbox">
								<label>Weightage</label>
								<input type="text" class="form-control question-box" name="weightage_3" onkeypress="return isNumberKey(event);" value="<?php echo $sim_com['weightage_3'] ?>" required="required" />
							</div>
							<div class="remove minusComp"><img src="img/icons/minus.png" title="Remove"></div>
						</div>
						<?php endif;
						if ( ! empty($sim_com['comp_col_4'])): $i += 1; ?>
						<div class="add_comp_option">
							<div class="form-group">
								<input type="text" placeholder="Competency Name" class="form-control" name="competency_name_4" value="<?php echo $sim_com['comp_col_4'] ?>" required="required" autocomplete="off" />
							</div>
							<div class="form-group labelbox">
								<label>Competency Score</label>
								<input type="text" class="form-control question-box" name="competency_score_4" onkeypress="return isNumberKey(event);" value="<?php echo $sim_com['comp_val_4'] ?>" required="required" />
							</div>
							<div class="form-group labelbox">
								<label>Weightage</label>
								<input type="text" class="form-control question-box" name="weightage_4" onkeypress="return isNumberKey(event);" value="<?php echo $sim_com['weightage_4'] ?>" required="required" />
							</div>
							<div class="remove minusComp"><img src="img/icons/minus.png" title="Remove"></div>
						</div>
						<?php endif;
						if ( ! empty($sim_com['comp_col_5'])): $i += 1; ?>
						<div class="add_comp_option">
							<div class="form-group">
								<input type="text" placeholder="Competency Name" class="form-control" name="competency_name_5" value="<?php echo $sim_com['comp_col_5'] ?>" required="required" autocomplete="off" />
							</div>
							<div class="form-group labelbox">
								<label>Competency Score</label>
								<input type="text" class="form-control question-box" name="competency_score_5" onkeypress="return isNumberKey(event);" value="<?php echo $sim_com['comp_val_5'] ?>" required="required" />
							</div>
							<div class="form-group labelbox">
								<label>Weightage</label>
								<input type="text" class="form-control question-box" name="weightage_5" onkeypress="return isNumberKey(event);" value="<?php echo $sim_com['weightage_5'] ?>" required="required" />
							</div>
							<div class="remove minusComp"><img src="img/icons/minus.png" title="Remove"></div>
						</div>
						<?php endif;
						if ( ! empty($sim_com['comp_col_6'])): $i += 1; ?>
						<div class="add_comp_option">
							<div class="form-group">
								<input type="text" placeholder="Competency Name" class="form-control" name="competency_name_6" value="<?php echo $sim_com['comp_col_6'] ?>" required="required" autocomplete="off" />
							</div>
							<div class="form-group labelbox">
								<label>Competency Score</label>
								<input type="text" class="form-control question-box" name="competency_score_6" onkeypress="return isNumberKey(event);" value="<?php echo $sim_com['comp_val_6'] ?>" required="required" />
							</div>
							<div class="form-group labelbox">
								<label>Weightage</label>
								<input type="text" class="form-control question-box" name="weightage_6" onkeypress="return isNumberKey(event);" value="<?php echo $sim_com['weightage_6'] ?>" required="required" />
							</div>
							<div class="remove minusComp"><img src="img/icons/minus.png" title="Remove"></div>
						</div>
						<?php endif; ?>
						</div>
						<?php else: ?>
						<div class="plus_comp_option linaer">
							<div class="add_comp_option">
								<div class="form-group">
									<input type="text" placeholder="Competency Name" class="form-control" name="competency_name_1" required="required" autocomplete="off" />
								</div>
								<div class="form-group labelbox">
									<label>Competency Score</label>
									<input type="text" class="form-control question-box" name="competency_score_1" onkeypress="return isNumberKey(event);" required="required" />
								</div>
								<div class="form-group labelbox">
									<label>Weightage</label>
									<input type="text" class="form-control question-box" name="weightage_1" onkeypress="return isNumberKey(event);" required="required" />
								</div>
								<div class="plus_comp"><img src="img/icons/plus.png" title="Add More"></div>
							</div>
						</div>
						<?php endif; ?>
					</div>
                </div>
           </div>
        </div>
        <!--Question-Temp-->
        <div class="Questio-template ClassicMULtitemp SingleVidEotmp tab-pane fade <?php echo ( ! empty($ques_id)) ? 'in active' : ''; ?>" id="questiontamp">
            <div class="videoBasedtmp">
                <div class="main_sim_scroll">
                    <div class="Simulation_det stopb clearfix unsetp">
                        <div class="skilQue">
							<?php if ( ! empty($sim_com['comp_col_1']) &&  ! empty($sim_com['comp_val_1'])): ?>
                            <div class="skills">
                                <label data-title="<?php echo $sim_com['comp_col_1'] ?>"><span class="truncateTxt"><?php echo $sim_com['comp_col_1'] ?></span></label>
                                <input type="text" class="form-control" id="comp_val_1" readonly="readonly" value="<?php echo abs($sim_com['comp_val_1'] - $ques_val1); ?>" />
                            </div>
                            <?php endif; ?>
							
							<?php if ( ! empty($sim_com['comp_col_2']) &&  ! empty($sim_com['comp_val_2'])): ?>
                            <div class="skills">
                                <label data-title="<?php echo $sim_com['comp_col_2'] ?>"><span class="truncateTxt"><?php echo $sim_com['comp_col_2'] ?></span></label>
                                <input type="text" class="form-control" id="comp_val_2" readonly="readonly" value="<?php echo abs($sim_com['comp_val_2'] - $ques_val2); ?>" />
                            </div>
                            <?php endif; ?>
                        
                            <?php if ( ! empty($sim_com['comp_col_3']) &&  ! empty($sim_com['comp_val_3'])): ?>
                            <div class="skills">
                                <label data-title="<?php echo $sim_com['comp_col_3'] ?>"><span class="truncateTxt"><?php echo $sim_com['comp_col_3'] ?></span></label>
                                <input type="text" class="form-control" id="comp_val_3" readonly="readonly" value="<?php echo abs($sim_com['comp_val_3'] - $ques_val3); ?>" />
                            </div>
                            <?php endif; ?>
                        
                            <?php if ( ! empty($sim_com['comp_col_4']) &&  ! empty($sim_com['comp_val_4'])): ?>
                            <div class="skills">
                                <label data-title="<?php echo $sim_com['comp_col_4'] ?>"><span class="truncateTxt"><?php echo $sim_com['comp_col_4'] ?></span></label>
                                <input type="text" class="form-control" id="comp_val_4" readonly="readonly" value="<?php echo abs($sim_com['comp_val_4'] - $ques_val4); ?>" />
                            </div>
                            <?php endif; ?>
							
							<?php if ( ! empty($sim_com['comp_col_5']) &&  ! empty($sim_com['comp_val_5'])): ?>
                            <div class="skills">
                                <label data-title="<?php echo $sim_com['comp_col_5'] ?>"><span class="truncateTxt"><?php echo $sim_com['comp_col_5'] ?></span></label>
                                <input type="text" class="form-control" id="comp_val_5" readonly="readonly" value="<?php echo abs($sim_com['comp_val_5'] - $ques_val5); ?>" />
                            </div>
                            <?php endif; ?>
                        
                            <?php if ( ! empty($sim_com['comp_col_6']) &&  ! empty($sim_com['comp_val_6'])): ?>
                            <div class="skills">
                                <label data-title="<?php echo $sim_com['comp_col_6'] ?>"><span class="truncateTxt"><?php echo $sim_com['comp_col_6'] ?></span></label>
                                <input type="text" class="form-control" id="comp_val_6" readonly="readonly" value="<?php echo abs($sim_com['comp_val_6'] - $ques_val6); ?>" />
                            </div>
                            <?php endif; ?>
                        </div>
                        <div class="previewQus Sbtn">
                            <a href="<?php echo $preview ?>" target="_blank" title="Preview"><img class="img-fluid" src="img/qus_icon/preview.svg"></a>
                        </div>
                    </div>
                    <div id="wrapper">
                        <!---------------Que Point List------------------>
                        <div class="width12 Quepointlist" id="left-wrapper">
							<div class="topbannericon"><img class="img-fluid closeleftmenuA" src="img/list/left_arrow.svg"></div>
                            <div class="question-list-banner">
                                <!--Start-Load-Question-List-->
                                <ul class="question-list" id="sortable">
                                	<div class="cue_pt_show"></div>
                                </ul>
                                <!--End-->
                                <div class="list-icon">
                                    <ul>
                                        <li class="DelIcon"><img class="img-fluid" src="img/list/delete_question.svg"/></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <?php $ques_type = ( ! empty($qresult['question_type'])) ? $qresult['question_type'] : ''; ?>
                        <div id="page-content-wrapper">
                            <div class="container-fluid videotmp">
                                <div class="row">
                                    <div class="tab-content">
                                        <div class="Q-left">
											<a href="#list-toggle" id="list-toggle" class="hideQTmp"><img class="close_panel" src="img/list/questiion_list.svg" /></a>
										</div>
                                        <!---MCQ-Video-Question-->
                                        <?php if ( ! empty($qresult['videoq_media_file'])): ?>
                                        <div id="MCQQuestion" class="tab-pane fade in active">
                                            <div class="container-fluid">
                                                <div class="widthQ100 classic-template">
                                                    <div class="tab-content">
                                                        <div id="match-Q" class="match-Q tab-pane fade in active">
                                                            <div class="panel panel-video">
                                                                <div class="panel-heading" role="tab" id="questionTwo">
                                                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse1">
                                                                    	<div class="video-text">
                                                                        	<span>VIDEO</span><i class="fa fa-chevron-down pinvideo"></i><i class="fa fa-chevron-up unpinvideo"></i>
                                                                        </div>
                                                                    </a>
                                                                </div>
                                                                <div id="collapse1" class="panel-collapse collapse in">
                                                                    <div class="panel-body">
                                                                        <div class="videoboxBanner">
                                                                            <div class="videouplaodingbox clearfix">
                                                                                <div class="cuepointbtn uploadvideo">
                                                                                    <div class="quesbox-flex videoflexbox">
                                                                                        <input type="file" name="videoqfile" id="videoqfile" class="form-control file" data-id="#videoq_media_file" data-assets="video_assets" />
                                                                                        <input type="hidden" name="videoq_media_file" id="videoq_media_file" value="<?php echo $qresult['videoq_media_file']; ?>" />
                                                                                        <div class="videoflex browsebtn">
                                                                                            <button class="browse btn btn-primary video_assets <?php echo ( ! empty($qresult['videoq_media_file'])) ? 'disabled' : ''; ?>" type="button" <?php echo ( ! empty($qresult['videoq_media_file'])) ? 'disabled="disabled"' : ''; ?>>Browse</button>
                                                                                        </div>
                                                                                        <div class="videoflex browsebtn">
                                                                                            <button type="button" name="upload_videoq_file" id="upload_videoq_file" class="btn btn-primary video_assets <?php echo ( ! empty($qresult['videoq_media_file'])) ? 'disabled' : ''; ?>" <?php echo ( ! empty($qresult['videoq_media_file'])) ? 'disabled="disabled"' : ''; ?>>Upload</button>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="VideoTimebox">
                                                                                    <div class="videoframe video_container" <?php echo ( ! empty($qresult['videoq_media_file'])) ? '' : 'style="display:none;"'; ?>>
                                                                                        <div class="videoflexcrossbtn browsebtn">
                                                                                            <?php /*?><div class="video_assets_container" <?php echo ( ! empty($qresult['videoq_media_file'])) ? '' : 'style="display:none;"'; ?>>
                                                                                                <a href="javascript:void(0);" data-assets="<?php echo $qresult['videoq_media_file']; ?>" data-assets-id="video_assets" data-input-id="#videoq_media_file" data-path="<?php echo $uploadpath ?>" class="delete_video_assets" title="Delete"><i class="fa fa-times" aria-hidden="true"></i></a>
                                                                                            </div><?php */?>
                                                                                        </div>
                                                                                        <video id="videoclip" controls="controls" width="320">
                                                                                        	<source id="qvideo" src="<?php echo $path . $qresult['videoq_media_file']; ?>" />
                                                                                        </video>
                                                                                        <?php /*?><div class="cue_pt_show">
                                                                                        	<div class="cue_points cue_points_1">
																								<?php echo $qresult['videoq_cue_point'] ?> 
																								<a href="javascript:void(0);" data-delete-id="1" title="Delete Cue Point" class="close_cue"><i class="fa fa-times" aria-hidden="true"></i></a>
																							</div>
                                                                                        </div><?php */?>
                                                                                        <div class="cue_pt">
                                                                                            <input type="text" class="timing inputtextWrap" id="cue_point" placeholder="mm:ss" />
                                                                                            <button type="button" class="add_cue videocueAdd">Add Cue Point</button>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <?php 
																#---Questions
																$ques_att = '';
																$qatt_type = '';
																if ( ! empty($qresult['qaudio']) && file_exists($root_path . $qresult['qaudio'])):
																	$qatt_type = 'qa';
																	$ques_att = $qresult['qaudio'];
																endif;
																
																#--Questions-Assets
																$ques_assets = '';
																$assets_type = '';
																if ( ! empty($qresult['audio']) && file_exists($root_path . $qresult['audio'])):
																	$assets_type = 'a';
																	$ques_assets = $qresult['audio'];
																elseif ( ! empty($qresult['video']) && file_exists($root_path . $qresult['video'])):
																	$assets_type = 'v';
																	$ques_assets = $qresult['video'];
																elseif ( ! empty($qresult['screen']) && file_exists($root_path . $qresult['screen'])):
																	$assets_type = 's';
																	$ques_assets = $qresult['screen'];
																elseif ( ! empty($qresult['image']) && file_exists($root_path . $qresult['image'])):
																	$assets_type = 'i';
																	$ques_assets = $qresult['image'];
																elseif ( ! empty($qresult['document']) && file_exists($root_path . $qresult['document'])):
																	$assets_type = 'd';
																	$ques_assets = $qresult['document'];
																endif; ?>
                                                                <div class="videoq_box">
                                                                	<div class="Ans_box_1 videoqboxs">
                                                                    	<div class="quesbox CQtmp">
                                                                        	<input type="hidden" name="updateVQuestionId[]" value="<?php echo $qresult['question_id'] ?>" />
    																		<input type="hidden" name="cue_point[]" class="cue_point" id="cue_point_1" value="<?php echo $qresult['videoq_cue_point'] ?>" />
                                                                            <div class="row">
                                                                                <div class="col-sm-12">
																					<div class="critical_div">
																						<h3 class="qusHeading">QUESTION</h3>
																						<div class="CIRtiCaltext">
																							Critical Question: <input type="checkbox" name="video_criticalQ[]" id="video_criticalQ" class="form-control criticalQ" value="1" <?php echo ( ! empty($qresult['critical'])) ? 'checked="checked"' : ''; ?> />
																						</div>
																					</div>
                                                                                    <div class="quesbox-flex">
                                                                                        <div class="form-group">
                                                                                            <input type="text" name="videoq[]" id="videoq" class="form-control QuesForm inputtextWrap videoq" placeholder="Click here to enter question" required="required" value="<?php echo $qresult['questions']; ?>">
                                                                                            <div class="videoqq_assets_container_1" <?php echo ( ! empty($ques_att)) ? '' : 'style="display:none;"'; ?>>
                                                                                                <div class="videoqq_assets_data_1"><?php echo $ques_att; ?></div>
                                                                                                <a href="javascript:void(0);" data-assets="<?php echo $ques_att; ?>" data-assets-id="videoqq_assets" data-input-id="#videoqq_audio_1,#videoqq_rec_audio_1,#videoqq_text_to_speech_1" data-path="<?php echo $uploadpath ?>" class="delete_assets" title="Delete"><i class="fa fa-times" aria-hidden="true"></i></a>
                                                                                            </div>
                                                                                             <ul class="QueBoxIcon">
                                                                                                <li class="dropdown">
                                                                                                    <button class="btn1 btn-secondary dropdown-toggle videoqq_assets <?php echo ( ! empty($ques_att)) ? 'disabled' : ''; ?>" <?php echo ( ! empty($ques_att)) ? 'disabled="disabled"' : ''; ?> type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img class="img-fluid" src="img/list/audio.svg"></button>
                                                                                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                                                                                        <a class="dropdown-item">
                                                                                                            <div class="tooltipLeft uploadicon">
                                                                                                                <label for="file-input-videoqq-addAudio_1">
                                                                                                                    <img class="img-fluid" src="img/list/text_speech.svg">
                                                                                                                    <span class="tooltiptext">Add Audio</span>
                                                                                                                </label>
                                                                                                                <input id="file-input-videoqq-addAudio_1" data-id="#videoqq_audio_1" data-assets="videoqq_assets_1" class="uploadAudioFileVideoTemp" type="file" name="videoqq_Audio_1" />
                                                                                                                <input type="hidden" name="videoqq_audio[]" id="videoqq_audio_1" value="<?php echo ($qatt_type == 'qa') ? $ques_att : ''; ?>" />
                                                                                                            </div>
                                                                                                        </a>
                                                                                                        <a class="dropdown-item">
                                                                                                            <div class="tooltipLeft">
                                                                                                                <img class="img-fluid rec-audio-videoq" data-input-id="videoqq_rec_audio_1" data-assets="videoqq_assets_1" src="img/list/add_audio.svg">
                                                                                                                <span class="tooltiptext">Record Audio</span>
                                                                                                            </div>
                                                                                                            <input type="hidden" name="videoqq_rec_audio[]" id="videoqq_rec_audio_1" />
                                                                                                         </a>
                                                                                                        <a data-toggle="modal" data-target="#videoqqtts" class="dropdown-item">
                                                                                                            <div class="tooltipLeft">
                                                                                                                <img class="img-fluid" src="img/list/record_audio.svg" />
                                                                                                                <span class="tooltiptext">Text to Speech</span>
                                                                                                            </div>
                                                                                                         </a>
                                                                                                     </div>
                                                                                                </li>
                                                                                            </ul>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="form-popup draggable Ques-pop-style modal" id="videoqqtts">
                                                                                        <div class="popheading">Text to Speech</div>
                                                                                        <div class="textarea">
                                                                                            <textarea type="text" class="form-control1 inputtextWrap" name="videoqq_text_to_speech[]" id="videoqq_text_to_speech_1"><?php echo ( ! empty($qresult['qspeech_text'])) ? stripslashes($qresult['qspeech_text']) : ''; ?></textarea>
                                                                                        </div>
                                                                                        <div class="submitRight1">
                                                                                            <button type="button" class="btn1 submitbtn1" data-dismiss="modal">Insert</button>
                                                                                            <button type="button" class="btn1 submitbtn1" onclick="clearData('videoqq_text_to_speech_');">Clear</button>
                                                                                            <button type="button" class="btn1 submitbtn1" data-dismiss="modal">Cancel</button>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="q-icon-banner clearfix">
                                                                                        <ul class="QueBoxIcon QueBoxIcon1">
                                                                                            <h5>Add Assets</h5>
                                                                                            <li class="dropdown">
                                                                                                <button class="btn1 btn-secondary dropdown-toggle videoq_assets_1 <?php echo ( ! empty($ques_assets)) ? 'disabled' : ''; ?>" <?php echo ( ! empty($ques_assets)) ? 'disabled="disabled"' : ''; ?> type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                                                <img class="img-fluid" src="img/list/audio.svg"></button>
                                                                                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                                                                                    <a class="dropdown-item">
                                                                                                        <div class="tooltip1">
                                                                                                            <img class="img-fluid video_modal" data-modal-id="videoqModal1" data-assets="videoq_assets_1" src="img/list/text_speech.svg" /><span class="tooltiptext">Text to Speech</span>
                                                                                                        </div>
                                                                                                    </a>
                                                                                                    <a class="dropdown-item">
                                                                                                        <div class="tooltip1 uploadicon">
                                                                                                            <label for="file-input-videoq-addAudio_1">
                                                                                                                <img class="img-fluid" src="img/list/add_audio.svg">
                                                                                                                <span class="tooltiptext">Add Audio</span>
                                                                                                            </label>
                                                                                                            <input id="file-input-videoq-addAudio_1" data-id="#videoq_audio_1" data-assets="videoq_assets_1" class="uploadAudioFileVideoTemp" type="file" name="videoq_Audio_1" />
                                                                                                            <input type="hidden" name="videoq_audio[]" id="videoq_audio_1" value="<?php echo ($assets_type == 'a') ? $ques_assets : ''; ?>" />
                                                                                                        </div>
                                                                                                    </a>
                                                                                                    <a class="dropdown-item">
                                                                                                        <div class="tooltip1">
                                                                                                            <img class="img-fluid rec-audio-videoq" data-input-id="videoq_rec_audio_1" data-assets="videoq_assets_1" src="img/list/record_audio.svg">
                                                                                                            <span class="tooltiptext">Record Audio</span>
                                                                                                        </div>
                                                                                                        <input type="hidden" name="videoq_rec_audio[]" id="videoq_rec_audio_1" />
                                                                                                     </a>
                                                                                                </div>
                                                                                            </li>
                                                                                            <li class="dropdown">
                                                                                                <button class="btn1 btn-secondary dropdown-toggle videoq_assets_1 <?php echo ( ! empty($ques_assets)) ? 'disabled' : ''; ?>" <?php echo ( ! empty($ques_assets)) ? 'disabled="disabled"' : ''; ?> type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                                                <img class="img-fluid" src="img/list/video.svg"></button>
                                                                                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                                                                                    <a class="dropdown-item">
                                                                                                        <div class="tooltip1 uploadicon">
                                                                                                            <label for="file-input-videoq-addVideo_1">
                                                                                                                <img class="img-fluid" src="img/list/add_video.svg">
                                                                                                                <span class="tooltiptext">Add Video</span>
                                                                                                            </label>
                                                                                                            <input id="file-input-videoq-addVideo_1" data-id="#videoq_video_1" data-assets="videoq_assets_1" class="uploadFileVideoTemp" type="file" name="videoq_Video_1" />
                                                                                                            <input type="hidden" name="videoq_video[]" id="videoq_video_1" value="<?php echo ($assets_type == 'v') ? $ques_assets : ''; ?>" />
                                                                                                        </div>
                                                                                                    </a>
                                                                                                    <a class="dropdown-item">
                                                                                                        <div class="tooltip1">
                                                                                                            <img class="img-fluid rec-video-videoq" data-input-id="videoq_rec_video_1" data-assets="videoq_assets_1" src="img/qus_icon/rec_video.svg">
                                                                                                            <span class="tooltiptext">Record Video</span>
                                                                                                         </div>
                                                                                                         <input type="hidden" name="videoq_rec_video[]" id="videoq_rec_video_1" />
                                                                                                    </a>
                                                                                                    <a class="dropdown-item">
                                                                                                        <div class="tooltip1">
                                                                                                            <img class="img-fluid rec-screen-videoq" data-input-id="videoq_rec_screen_1" data-assets="videoq_assets_1" src="img/list/screen_record.svg">
                                                                                                            <span class="tooltiptext">Record Screen</span>
                                                                                                         </div>
                                                                                                         <input type="hidden" name="videoq_rec_screen[]" id="videoq_rec_screen_1" value="<?php echo ($assets_type == 's') ? $ques_assets : ''; ?>"  />
                                                                                                    </a>
                                                                                                </div>
                                                                                            </li>
                                                                                            <li>
                                                                                                <div class="tooltip uploadicon videoq_assets_1 <?php echo ( ! empty($ques_assets)) ? 'disabled' : ''; ?>" <?php echo ( ! empty($ques_assets)) ? 'disabled="disabled"' : ''; ?>>
                                                                                                    <label for="file-input-videoq-addImg_1">
                                                                                                        <img class="img-fluid" src="img/list/image.svg">
                                                                                                        <span class="tooltiptext">Add Image</span>
                                                                                                    </label>
                                                                                                    <input id="file-input-videoq-addImg_1" data-id="#videoq_img_1" data-assets="videoq_assets_1" class="uploadImgFileVideo videoq_assets_1" type="file" name="videoq_Img_1" <?php echo ( ! empty($ques_assets)) ? 'disabled="disabled"' : ''; ?> />
                                                                                                    <input type="hidden" name="videoq_img[]" id="videoq_img_1" value="<?php echo ($assets_type == 'i') ? $ques_assets : ''; ?>" />
                                                                                                </div>
                                                                                            </li>
                                                                                            <li>
                                                                                                <div class="tooltip uploadicon videoq_assets_1 <?php echo ( ! empty($ques_assets)) ? 'disabled' : ''; ?>" <?php echo ( ! empty($ques_assets)) ? 'disabled="disabled"' : ''; ?>>
                                                                                                    <label for="file-input-videoq-addDoc-1">
                                                                                                        <img class="img-fluid" src="img/list/add_document.svg" />
                                                                                                        <span class="tooltiptext">Add Document</span>
                                                                                                    </label>
                                                                                                    <input id="file-input-videoq-addDoc-1" data-id="#videoq_doc_1" data-assets="videoq_assets_1" class="uploadDocFileVideo videoq_assets_1" type="file" name="videoq_Doc_1" />
                                                                                                    <input type="hidden" name="videoq_doc[]" id="videoq_doc_1" value="<?php echo ($assets_type == 'd') ? $ques_assets : ''; ?>" />
                                                                                                </div>
                                                                                            </li>
                                                                                        </ul>
                                                                                        <div class="Ques-comp">
                                                                                            <h5>Add Competency score</h5>
                                                                                            <?php if ( ! empty($sim_com['comp_col_1']) &&  ! empty($sim_com['comp_val_1'])): ?>
                                                                                            <div class="form-group QusScore">
                                                                                                <input type="text" class="form-control videoq videoq_val_1" name="videoq_ques_val_1[]" id="videoq_ques_val_1_1" onkeypress="return isNumberKey(event);" value="<?php echo $qresult['ques_val_1'] ?>" />
                                                                                            </div>
                                                                                            <?php endif; ?>
                                                                                            <?php if ( ! empty($sim_com['comp_col_2']) &&  ! empty($sim_com['comp_val_2'])): ?>
                                                                                            <div class="form-group QusScore">
                                                                                                <input type="text" class="form-control videoq videoq_val_2" name="videoq_ques_val_2[]" id="videoq_ques_val_2_1" onkeypress="return isNumberKey(event);" value="<?php echo $qresult['ques_val_2'] ?>" />
                                                                                            </div>
                                                                                            <?php endif; ?>
                                                                                            <?php if ( ! empty($sim_com['comp_col_3']) &&  ! empty($sim_com['comp_val_3'])): ?>
                                                                                            <div class="form-group QusScore">
                                                                                                <input type="text" class="form-control videoq videoq_val_3" name="videoq_ques_val_3[]" id="videoq_ques_val_3_1" onkeypress="return isNumberKey(event);" value="<?php echo $qresult['ques_val_3'] ?>" />
                                                                                            </div>
                                                                                            <?php endif; ?>
                                                                                            <?php if ( ! empty($sim_com['comp_col_4']) &&  ! empty($sim_com['comp_val_4'])): ?>
                                                                                            <div class="form-group QusScore">
                                                                                                <input type="text" class="form-control videoq videoq_val_4" name="videoq_ques_val_4[]" id="videoq_ques_val_4_1" onkeypress="return isNumberKey(event);" value="<?php echo $qresult['ques_val_4'] ?>" />
                                                                                            </div>
                                                                                            <?php endif; ?>
                                                                                            <?php if ( ! empty($sim_com['comp_col_5']) &&  ! empty($sim_com['comp_val_5'])): ?>
                                                                                            <div class="form-group QusScore">
                                                                                                <input type="text" class="form-control videoq videoq_val_5" name="videoq_ques_val_5[]" id="videoq_ques_val_5_1" onkeypress="return isNumberKey(event);" value="<?php echo $qresult['ques_val_5'] ?>" />
                                                                                            </div>
                                                                                            <?php endif; ?>
                                                                                            <?php if ( ! empty($sim_com['comp_col_6']) &&  ! empty($sim_com['comp_val_6'])): ?>
                                                                                            <div class="form-group QusScore">
                                                                                                <input type="text" class="form-control videoq videoq_val_6" name="videoq_ques_val_6[]" id="videoq_ques_val_6_1" onkeypress="return isNumberKey(event);" value="<?php echo $qresult['ques_val_6'] ?>" />
                                                                                            </div>
                                                                                            <?php endif; ?>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="videoq_assets_container_1" <?php echo ( ! empty($ques_assets)) ? '' : 'style="display:none;"'; ?>>
                                                                                        <div class="videoq_assets_data_1"><a class="view_assets" data-src="<?php echo $path . $ques_assets; ?>" href="javascript:;"><?php echo $ques_assets; ?></a></div>
                                                                                        <a href="javascript:void(0);" data-assets="<?php echo $ques_assets ?>" data-assets-id="videoq_assets_1" data-input-id="" data-path="<?php echo $uploadpath ?>" class="delete_assets" title="Delete"><i class="fa fa-times" aria-hidden="true"></i></a>
                                                                                    </div>
                                                                                    <div class="videoq_tts_container_1" style="display:none;">
                                                                                        <div class="videoq_tts_data_1">
                                                                                            <div class="tooltip1">
                                                                                                <img class="img-fluid" src="img/list/text_speech.svg"><span class="tooltiptext">Text to Speech</span>
                                                                                            </div>
                                                                                        </div>
                                                                                        <a href="javascript:void(0);" class="delete_tts" data-modal-id="videoqModal1" data-assets="videoq_assets_1" data-input-id="videoq_text_to_speech_1" title="Delete"><i class="fa fa-times" aria-hidden="true"></i></a>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-popup draggable" id="videoqModal1">
                                                                            <div class="popheading">Text to Speech</div>
                                                                            <div class="textarea">
                                                                                <textarea type="text" class="form-control1 inputtextWrap" name="videoq_text_to_speech[]" id="videoq_text_to_speech_1"></textarea>
                                                                            </div>
                                                                            <div class="submitRight1">
                                                                                <button type="button" class="btn1 submitbtn1 vts_modal_save" data-modal-id="videoqModal1" data-assets="videoq_assets_1" data-input-id="videoq_text_to_speech_1" data-assets-view="videoq_tts_container_1">Insert</button>
                                                                                <button type="button" class="btn1 submitbtn1 vts_modal_cls" data-modal-id="videoqModal1" data-assets="videoq_assets_1" data-input-id="videoq_text_to_speech_1" data-assets-view="videoq_tts_container_1">Clear</button>
                                                                                <button type="button" class="btn1 submitbtn1 vts_modal_close" data-modal-id="videoqModal1" data-assets="videoq_assets_1" data-input-id="videoq_text_to_speech_1" data-assets-view="videoq_tts_container_1">Cancel</button>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-popup draggable" id="videofModal1">
                                                                            <div class="popheading">Text to Speech</div>
                                                                            <div class="textarea">
                                                                                <textarea type="text" class="form-control1 inputtextWrap" name="videoqf_feedback_text_to_speech[]" id="videoqf_feedback_text_to_speech_1"></textarea>
                                                                            </div>
                                                                            <div class="submitRight1">
                                                                                <button type="button" class="btn1 submitbtn1 vts_modal_save" data-modal-id="videofModal1" data-assets="videoqf_assets_1" data-input-id="videoqf_feedback_text_to_speech_1" data-assets-view="videoqf_tts_container_1">Insert</button>
                                                                                <button type="button" class="btn1 submitbtn1 vts_modal_cls" data-modal-id="videofModal1" data-assets="videoqf_assets_1" data-input-id="videoqf_feedback_text_to_speech_1" data-assets-view="videoqf_tts_container_1">Clear</button>
                                                                                <button type="button" class="btn1 submitbtn1 vts_modal_close" data-modal-id="videofModal1" data-assets="videoqf_assets_1" data-input-id="videoqf_feedback_text_to_speech_1" data-assets-view="videoqf_tts_container_1">Cancel</button>
                                                                            </div>
                                                                        </div>
                                                                        <div class="Ansbox">
                                                                            <div class="row">
                                                                                <div class="col-sm-12">
                                                                                    <div class="col-sm-6">
                                                                                        <h3 class="AnsHeading">CHOICE</h3>
                                                                                    </div>
                                                                                    <div class="VideoApendOption VideoApendOption_1">
                                                                                        <div class="MCQApendOption-box Video-box_1">
                                                                                            <div class="MCQRadio">
																							<?php 
																							$videoa_sql = "SELECT * FROM answer_tbl WHERE question_id = '". $qresult['question_id'] ."'";
                                                                                            $videoa_res = $db->prepare($videoa_sql); $videoa_res->execute();
                                                                                            if ($videoa_res->rowCount() > 0): $videoai = 1; $vaoption = 0;
                                                                                            foreach ($videoa_res->fetchAll(PDO::FETCH_ASSOC) as $videoa_row): ?>
                                                                                            <div class="classicQUeSTionbanner">
                                                                                                <input type="hidden" name="updateAnswerid[<?php echo $vaoption ?>][]" value="<?php echo $videoa_row['answer_id'] ?>" />
                                                                                                <div class="Radio-box classtmpR">
                                                                                                    <label class="radiostyle">
                                                                                                        <input type="radio" name="videoq_true_option[<?php echo $vaoption ?>][]" value="<?php echo $videoai ?>" <?php echo ( ! empty($videoa_row['true_option'])) ? 'checked="checked"' : ''; ?> /><span class="radiomark"></span>
                                                                                                    </label>
                                                                                                    <div class="col-sm-12 mcwidth">
                                                                                                        <div class="choicebox">
                                                                                                            <div class="form-group">
                                                                                                                <input type="text" name="videoq_option[<?php echo $vaoption ?>][option][]" class="form-control inputtextWrap videoq" placeholder="Option text goes here" required="required" value="<?php echo stripslashes($videoa_row['choice_option']); ?>" />
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <?php if ($videoai == 1): ?>
                                                                                                    <div class="AddAns-Option">
                                                                                                        <img class="img-fluid videoAddoptions" data-video-option="<?php echo $videoai; ?>" data-video-suboption="<?php echo $vaoption; ?>" data-video-radio="<?php echo $videoai; ?>" src="img/list/add_field.svg">
                                                                                                    </div>
                                                                                                    <?php else: ?>
                                                                                                    <div class="AddAns-Option removeOption" data-remove-answer-id="<?php echo $videoa_row['answer_id'] ?>"><img class="img-fluid" src="img/list/delete_field.svg" /></div>
                                                                                                    <?php endif; ?>
                                                                                                </div>
                                                                                                <div class="q-icon-banner clearfix classicQBaN">
                                                                                                    <div class="Ques-comp">
                                                                                                        <h5>Add Competency score</h5>
                                                                                                        <?php if ( ! empty($sim_com['comp_col_1']) &&  ! empty($sim_com['comp_val_1'])): ?>
                                                                                                        <div class="form-group QusScore">
                                                                                                            <input type="text" class="form-control score videoq" name="videoq_ans_val1[<?php echo $vaoption ?>][score][]" id="videoq_ans_val1_1" onkeypress="return isNumberKey(event);" value="<?php echo $videoa_row['ans_val1'] ?>" />
                                                                                                        </div>
                                                                                                        <?php endif; ?>
                                                                                                        <?php if ( ! empty($sim_com['comp_col_2']) &&  ! empty($sim_com['comp_val_2'])): ?>
                                                                                                        <div class="form-group QusScore">
                                                                                                            <input type="text" class="form-control score videoq" name="videoq_ans_val2[<?php echo $vaoption ?>][score][]" id="videoq_ans_val2_1" onkeypress="return isNumberKey(event);" value="<?php echo $videoa_row['ans_val2'] ?>" />
                                                                                                        </div>
                                                                                                        <?php endif; ?>
                                                                                                        <?php if ( ! empty($sim_com['comp_col_3']) &&  ! empty($sim_com['comp_val_3'])): ?>
                                                                                                        <div class="form-group QusScore">
                                                                                                            <input type="text" class="form-control score videoq" name="videoq_ans_val3[<?php echo $vaoption ?>][score][]" id="videoq_ans_val3_1" onkeypress="return isNumberKey(event);" value="<?php echo $videoa_row['ans_val3'] ?>" />
                                                                                                        </div>
                                                                                                        <?php endif; ?>
                                                                                                        <?php if ( ! empty($sim_com['comp_col_4']) &&  ! empty($sim_com['comp_val_4'])): ?>
                                                                                                        <div class="form-group QusScore">
                                                                                                            <input type="text" class="form-control score videoq" name="videoq_ans_val4[<?php echo $vaoption ?>][score][]" id="videoq_ans_val4_1" onkeypress="return isNumberKey(event);" value="<?php echo $videoa_row['ans_val4'] ?>" />
                                                                                                        </div>
                                                                                                        <?php endif; ?>
                                                                                                        <?php if ( ! empty($sim_com['comp_col_5']) &&  ! empty($sim_com['comp_val_5'])): ?>
                                                                                                        <div class="form-group QusScore">
                                                                                                            <input type="text" class="form-control score videoq" name="videoq_ans_val5[<?php echo $vaoption ?>][score][]" id="videoq_ans_val5_1" onkeypress="return isNumberKey(event);" value="<?php echo $videoa_row['ans_val5'] ?>" />
                                                                                                        </div>
                                                                                                        <?php endif; ?>
                                                                                                        <?php if ( ! empty($sim_com['comp_col_6']) &&  ! empty($sim_com['comp_val_6'])): ?>
                                                                                                        <div class="form-group QusScore">
                                                                                                            <input type="text" class="form-control score videoq" name="videoq_ans_val6[<?php echo $vaoption ?>][score][]" id="videoq_ans_val6_1" onkeypress="return isNumberKey(event);" value="<?php echo $videoa_row['ans_val6'] ?>" />
                                                                                                        </div>
                                                                                                        <?php endif; ?>
                                                                                                    </div>
                                                                                                </div>
                                                                                                <div class="classicQ SingleCmcq">
																									<?php 
                                                                                                    # Get FeedBack
                                                                                                    $feedsql  = 'SELECT * FROM feedback_tbl WHERE answer_id = :answer_id';
                                                                                                    $feedstmt = $db->prepare($feedsql);
                                                                                                    $feedstmt->bindParam(':answer_id', $videoa_row['answer_id'], PDO::PARAM_INT);
                                                                                                    $feedstmt->execute();
                                                                                                    $feedResult = ($feedstmt->rowCount() > 0) ? $feedstmt->fetch(PDO::FETCH_ASSOC) : [];
                                                                                                    #----Feedback
                                                                                                    $qfeed_assets = '';
                                                                                                    $feed_assets_type = '';
                                                                                                    if ( ! empty($feedResult['feed_audio']) && file_exists($root_path . $feedResult['feed_audio']) && $feedResult['feedback_type'] == 1):
                                                                                                        $feed_assets_type = 'fa';
                                                                                                        $qfeed_assets = $feedResult['feed_audio'];
                                                                                                    elseif ( ! empty($feedResult['feed_video']) && file_exists($root_path . $feedResult['feed_video']) && $feedResult['feedback_type'] == 1):
                                                                                                        $feed_assets_type = 'fv';
                                                                                                        $qfeed_assets = $feedResult['feed_video'];
                                                                                                    elseif ( ! empty($feedResult['feed_screen']) && file_exists($root_path . $feedResult['feed_screen']) && $feedResult['feedback_type'] == 1):
                                                                                                        $feed_assets_type = 'fs';
                                                                                                        $qfeed_assets = $feedResult['feed_screen'];
                                                                                                    elseif ( ! empty($feedResult['feed_image']) && file_exists($root_path . $feedResult['feed_image']) && $feedResult['feedback_type'] == 1):
                                                                                                        $feed_assets_type = 'fi';
                                                                                                        $qfeed_assets = $feedResult['feed_image'];
                                                                                                    elseif ( ! empty($feedResult['feed_document']) && file_exists($root_path . $feedResult['feed_document']) && $feedResult['feedback_type'] == 1):
                                                                                                        $feed_assets_type = 'fd';
                                                                                                        $qfeed_assets = $feedResult['feed_document'];
                                                                                                    endif; ?>
                                                                                                    <div class="form-group feed">
                                                                                                        <h5>Feedback</h5>
                                                                                                        <textarea class="form-control FeedForm inputtextWrap videoq" name="videoq_feedback[<?php echo $vaoption ?>][]" id="videoq_feedback<?php echo $videoai ?>" rows="3" placeholder="Feedback text goes here"><?php echo ( ! empty($feedResult['feedback']) && $feedResult['feedback_type'] == 1) ? stripslashes($feedResult['feedback']) : ''; ?></textarea>
                                                                                                        <div class="videoqf_assets_container_<?php echo $vaoption . $videoai ?>" <?php echo ( ! empty($qfeed_assets)) ? '' : 'style="display:none;"'; ?>>
                                                                                                            <div class="videoqf_assets_data_<?php echo $vaoption . $videoai ?>"><a class="view_assets" data-src="<?php echo $path . $qfeed_assets; ?>" href="javascript:;"><?php echo $qfeed_assets; ?></a></div>
                                                                                                            <a href="javascript:void(0);" data-assets="<?php echo $qfeed_assets ?>" data-assets-id="videoqf_assets_<?php echo $vaoption . $videoai ?>" data-input-id="" data-path="<?php echo $uploadpath ?>" class="delete_assets" title="Delete"><i class="fa fa-times" aria-hidden="true"></i></a>
                                                                                                        </div>
                                                                                                        <div class="videoqf_tts_container_<?php echo $vaoption . $videoai ?>" style="display:none;">
                                                                                                            <div class="videoqf_tts_data_<?php echo $vaoption . $videoai ?>">
                                                                                                                <div class="tooltip1">
                                                                                                                    <img class="img-fluid" src="img/list/text_speech.svg"><span class="tooltiptext">Text to Speech</span>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                            <a href="javascript:void(0);" class="delete_tts" data-modal-id="videofModal1" data-assets="videoqf_assets_1" data-input-id="videoqf_feedback_text_to_speech_1" title="Delete"><i class="fa fa-times" aria-hidden="true"></i></a>
                                                                                                        </div>
                                                                                                        <ul class="QueBoxIcon feedQ">
                                                                                                            <h5>Add assets</h5>
                                                                                                            <li class="dropdown">
                                                                                                                <button class="btn1 btn-secondary dropdown-toggle videoqf_assets_<?php echo $vaoption . $videoai ?> <?php echo ( ! empty($qfeed_assets)) ? 'disabled' : ''; ?>" <?php echo ( ! empty($qfeed_assets)) ? 'disabled="disabled"' : ''; ?> type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img class="img-fluid" src="img/list/audio.svg" /></button>
                                                                                                                <div class="dropdown-menu buttom" aria-labelledby="dropdownMenuButton">
                                                                                                                    <a class="dropdown-item">
                                                                                                                        <div class="tooltip1">
                                                                                                                        <img class="img-fluid video_modal" data-modal-id="videofModal<?php echo $vaoption . $videoai ?>" data-assets="videoqf_assets_<?php echo $vaoption . $videoai ?>" src="img/list/text_speech.svg" /><span class="tooltiptext">Text to Speech</span>
                                                                                                                        </div>
                                                                                                                    </a>
                                                                                                                    <a class="dropdown-item">
                                                                                                                        <div class="tooltip1 uploadicon">
                                                                                                                            <label for="file-input-videoqf-feedback-addAudio_<?php echo $vaoption . $videoai ?>">
                                                                                                                                <img class="img-fluid" src="img/list/add_audio.svg">
                                                                                                                                <span class="tooltiptext">Add Audio</span>
                                                                                                                            </label>
                                                                                                                            <input id="file-input-videoqf-feedback-addAudio_<?php echo $vaoption . $videoai ?>" data-id="#videoqf_feedback_audio_<?php echo $vaoption . $videoai ?>" data-assets="videoqf_assets_<?php echo $vaoption . $videoai ?>" class="uploadAudioFileVideoTemp" type="file" name="videoqf_feedback_Audio_<?php echo $vaoption . $videoai ?>" />
                                                                                                                            <input type="hidden" name="videoqf_feedback_audio[<?php echo $vaoption ?>][]" id="videoqf_feedback_audio_<?php echo $vaoption . $videoai ?>" value="<?php echo ($feed_assets_type == 'fa') ? $qfeed_assets : ''; ?>" />
                                                                                                                        </div>
                                                                                                                    </a>
                                                                                                                    <a class="dropdown-item">
                                                                                                                        <div class="tooltip1">
                                                                                                                            <img class="img-fluid rec-audio-videoq" data-input-id="videoqf_feedback_rec_audio_<?php echo $vaoption . $videoai ?>" data-assets="videoqf_assets_<?php echo $vaoption . $videoai ?>" src="img/list/record_audio.svg" />
                                                                                                                            <span class="tooltiptext">Record Audio</span>
                                                                                                                        </div>
                                                                                                                        <input type="hidden" name="videoqf_feedback_rec_audio[<?php echo $vaoption ?>][]" id="videoqf_feedback_rec_audio_<?php echo $vaoption . $videoai ?>" />
                                                                                                                     </a>
                                                                                                                </div>
                                                                                                            </li>
                                                                                                            <li class="dropdown">
                                                                                                                <button class="btn1 btn-secondary dropdown-toggle videoqf_assets_<?php echo $vaoption . $videoai ?> <?php echo ( ! empty($qfeed_assets)) ? 'disabled' : ''; ?>" <?php echo ( ! empty($qfeed_assets)) ? 'disabled="disabled"' : ''; ?> type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img class="img-fluid" src="img/list/video.svg"></button>
                                                                                                                <div class="dropdown-menu buttom" aria-labelledby="dropdownMenuButton">
                                                                                                                    <a class="dropdown-item">
                                                                                                                        <div class="tooltip1 uploadicon">
                                                                                                                            <label for="file-input-videoqf-feedback-addVideo_<?php echo $vaoption . $videoai ?>">
                                                                                                                                <img class="img-fluid" src="img/list/add_video.svg">
                                                                                                                                <span class="tooltiptext">Add Video</span>
                                                                                                                            </label>
                                                                                                                            <input id="file-input-videoqf-feedback-addVideo_<?php echo $vaoption . $videoai ?>" data-id="#videoqf_feedback_video_<?php echo $vaoption . $videoai ?>" data-assets="videoqf_assets_<?php echo $vaoption . $videoai ?>" class="uploadFileVideoTemp" type="file" name="videoqf_feedback_Video_<?php echo $vaoption . $videoai ?>" />
                                                                                                                            <input type="hidden" name="videoqf_feedback_video[<?php echo $vaoption ?>][]" id="videoqf_feedback_video_1" value="<?php echo ($feed_assets_type == 'fv') ? $qfeed_assets : ''; ?>" />
                                                                                                                        </div>
                                                                                                                    </a>
                                                                                                                    <a class="dropdown-item">
                                                                                                                        <div class="tooltip1">
                                                                                                                            <img class="img-fluid rec-video-videoq" data-input-id="videoqf_feedback_rec_video_<?php echo $vaoption . $videoai ?>" data-assets="videoqf_assets_<?php echo $vaoption . $videoai ?>" src="img/qus_icon/rec_video.svg" />
                                                                                                                            <span class="tooltiptext">Record Video</span>
                                                                                                                         </div>
                                                                                                                         <input type="hidden" name="videoqf_feedback_rec_video[<?php echo $vaoption ?>][]" id="videoqf_feedback_rec_video_<?php echo $vaoption . $videoai ?>" />
                                                                                                                    </a>
                                                                                                                    <a class="dropdown-item">
                                                                                                                        <div class="tooltip1">
                                                                                                                            <img class="img-fluid rec-screen-videoq" data-input-id="videoqf_feedback_rec_screen_<?php echo $vaoption . $videoai ?>" data-assets="videoqf_assets_<?php echo $vaoption . $videoai ?>" src="img/list/screen_record.svg" />
                                                                                                                            <span class="tooltiptext">Record Screen</span>
                                                                                                                         </div>
                                                                                                                         <input type="hidden" name="videoqf_feedback_rec_screen[<?php echo $vaoption ?>][]" id="videoqf_feedback_rec_screen_<?php echo $vaoption . $videoai ?>" value="<?php echo ($feed_assets_type == 'fs') ? $qfeed_assets : ''; ?>" />
                                                                                                                    </a>
                                                                                                                </div>
                                                                                                            </li>
                                                                                                            <li>
                                                                                                                <div class="tooltip uploadicon videoqf_assets_<?php echo $vaoption . $videoai ?> <?php echo ( ! empty($qfeed_assets)) ? 'disabled' : ''; ?>" <?php echo ( ! empty($qfeed_assets)) ? 'disabled="disabled"' : ''; ?>>
                                                                                                                    <label for="file-input-videoqf-feedback-addImg_<?php echo $vaoption . $videoai ?>">
                                                                                                                        <img class="img-fluid" src="img/list/image.svg">
                                                                                                                        <span class="tooltiptext">Add Image</span>
                                                                                                                    </label>
                                                                                                                    <input id="file-input-videoqf-feedback-addImg_<?php echo $vaoption . $videoai ?>" data-id="#videoqf_feedback_img_<?php echo $vaoption . $videoai ?>" data-assets="videoqf_assets_<?php echo $vaoption . $videoai ?>" class="uploadImgFileVideo videoqf_assets_<?php echo $vaoption . $videoai ?>" type="file" name="videoqf_feedback_Img_<?php echo $vaoption . $videoai ?>" <?php echo ( ! empty($qfeed_assets)) ? 'disabled="disabled"' : ''; ?> />
                                                                                                                    <input type="hidden" name="videoqf_feedback_img[<?php echo $vaoption ?>][]" id="videoqf_feedback_img_<?php echo $vaoption . $videoai ?>" value="<?php echo ($feed_assets_type == 'fi') ? $qfeed_assets : ''; ?>" />
                                                                                                                </div>
                                                                                                            </li>
                                                                                                            <li>
                                                                                                                <div class="tooltip uploadicon videoqf_assets_<?php echo $vaoption . $videoai ?> <?php echo ( ! empty($qfeed_assets)) ? 'disabled' : ''; ?>" <?php echo ( ! empty($qfeed_assets)) ? 'disabled="disabled"' : ''; ?>>
                                                                                                                    <label for="file-input-videoqf-feedback-addDoc-<?php echo $vaoption . $videoai ?>">
                                                                                                                        <img class="img-fluid" src="img/list/add_document.svg" />
                                                                                                                        <span class="tooltiptext">Add Document</span>
                                                                                                                    </label>
                                                                                                                    <input id="file-input-videoqf-feedback-addDoc-<?php echo $vaoption . $videoai ?>" data-id="#videoqf_feedback_doc_<?php echo $vaoption . $videoai ?>" data-assets="videoqf_assets_<?php echo $vaoption . $videoai ?>" class="uploadDocFileVideo videoqf_assets_<?php echo $vaoption . $videoai ?>" type="file" name="videoqf_feedback_Doc_<?php echo $vaoption . $videoai ?>" <?php echo ( ! empty($qfeed_assets)) ? 'disabled="disabled"' : ''; ?> />
                                                                                                                    <input type="hidden" name="videoqf_feedback_doc[<?php echo $vaoption ?>][]" id="videoqf_feedback_doc_<?php echo $vaoption . $videoai ?>" value="<?php echo ($feed_assets_type == 'fd') ? $qfeed_assets : ''; ?>" />
                                                                                                                </div>
                                                                                                            </li>
                                                                                                        </ul>
                                                                                                        <div class="form-popup draggable Ques-pop-style modal" id="videoqfctts<?php echo $vaoption . $videoai ?>">
                                                                                                            <div class="popheading">Insert Text to Speech</div>
                                                                                                            <div class="textarea">
                                                                                                                <textarea type="text" class="form-control1 inputtextWrap" name="videoqf_feedback_text_to_speech[<?php echo $vaoption ?>][]" id="videoqf_feedback_text_to_speech_<?php echo $vaoption . $videoai ?>"><?php echo ( ! empty($feedResult['feedback']) && $feedResult['feedback_type'] == 1) ? stripslashes($feedResult['feed_speech_text']) : ''; ?></textarea>
                                                                                                            </div>
                                                                                                            <div class="modal-footer">
                                                                                                                <button type="button" class="btn1 submitbtn1" data-dismiss="modal">Insert</button>
                                                                                                                <button type="button" class="btn1 submitbtn1" onclick="clearData('videoqf_feedback_text_to_speech_<?php echo $vaoption . $videoai ?>');">Clear</button>
                                                                                                                <button type="button" class="btn1 submitbtn1" data-dismiss="modal">Cancel</button>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                           </div>
																						   <?php $videoai++; endforeach; endif; ?>
                                                                                        </div>
                                                                                       </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
									<?php endif; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                 </div>
               </div>
        	</div>
    	</div>
    </div>
    <div class="sim_bluebanner">
        <ul class="Ques_linear_save">
           <li><button type="submit" class="submitques update_next"><img src="img/list/save-t.svg" />SAVE & CONTINUE</button></li>
           <li><button type="submit" class="submitques update_close"><img src="img/list/close.svg" />SAVE & CLOSE</button></li>
        </ul>
    </div>
</form>

<div id="load_popup_modal_show" class="modal fade" tabindex="-1"></div>

<script src="content/js/jquery-ui-1.12.1/jquery-ui.js"></script>
<script src="content/js/jquery.fontselect.js"></script>
<script type="text/javascript">
$(function(){
	$(".draggable").draggable();
	$("#cue_point").inputmask("99:99", {"clearIncomplete": true});
});

var assets_path = '<?php echo $path; ?>';

$('body').on('click', '.view_assets', function(e){
    e.preventDefault();
    var to = $(this).attr('data-src');
    $.fancybox.open({
        type: 'iframe',
        src: to,
        toolbar  : false,
        smallBtn : true,
		closeExisting: false,
		scrolling: 'no',
        iframe : {
            preload : true,
            scrolling: 'no',
			css:{
					width:'100%',
					height:'100%'
				}
        }
    });
});

$(".inputtextWrap").hover(function() {
	$(this).attr('title', $(this).val());
}, function() {
	$(this).css('cursor','auto');
});

/* Set Default Font Type */
$('.cl_ftype').click(function () {
	$('#font_type').trigger('setFont', '');
});

function isNumberKey(evt) {
	var charCode = (evt.which) ? evt.which : evt.keyCode
	return !(charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57));
}

function clearData(id){
	$('#'+ id).val('');
}

$(".video-text").click(function(){
  $(".unpinvideo").show();
  $(".pinvideo").hide();
  $(".video-text").toggleClass('unpin-heading');
});

/* Get Questions List */
function getQuesList() {
	$.ajax({
		type: 'GET',
		url: 'includes/process.php',
		data: 'getCuePointList=true&sim_id=<?php echo $sim_id ?>',
		success:function(resdata) {
			$.LoadingOverlay("hide");
			var res = $.parseJSON(resdata);
			if (res.success == true) {
				var base 	= $('#location').val();
				var current	= window.location.href;
				var qurl 	= '';
				var qclass	= '';
				var qno		= 1;
				$('.cue_pt_show').empty();
				$.each(res.data, function (key, val) {
					qurl = base + '&ques_id='+ val.ques_id;
					if (current == qurl){
						qclass = 'active';
					} else {
						qclass = '';
					}
					var qlist = '<label class="ui-state-default '+ qclass +'" id="question-'+ val.qid +'"><span class="QNum">'+ qno +'</span><input type="checkbox" name="ques_no[]" class="form-control ques_no ui-state-CheCK" value="'+ val.qid +'" /><a class="ui-state-text" href="'+ qurl +'"><img class="updownQues" src="img/list/up-down.svg"><li>'+ val.qname +'</li></a></label>';
					$('.cue_pt_show').append(qlist);
					qno++;
				});
			} else {
				swal({text: 'Questions list not load please try again later.', buttons: false, icon: "error", timer: 2000 });
			}
		},error: function() {
			$.LoadingOverlay("hide");
			swal({text: 'Oops. something went wrong please try again.', buttons: false, icon: "error", timer: 2000 });
		}
	});
}

getQuesList();

$('.update_next').on('click', function(){
	$('#submit_type').val(1);
});

$('.update_close').on('click', function(){
	$('#submit_type').val(2);
});

$('.type-color-on-page').spectrum({
	type: "component",
	togglePaletteOnly: "true",
	hideAfterPaletteSelect: "true",
	showInput: "true",
	showInitial: "true",
});

$('#font_type').fontselect({
	placeholder: 'Pick a font from the list',
	searchable: false,
	systemFonts: [],
	googleFonts: [<?php echo $db::googleFont; ?>]
});

/* Delete Question */
$('.DelIcon').on('click', function(e){
	e.preventDefault();
	var ques = $('.ques_no:checked').map(function(){ return this.value; }).get();
	if (ques.length > 0) {
		$.LoadingOverlay("show");
		var dataString = 'del_Question='+ true +'&ques_id='+ ques;
		$.ajax({
			type: 'POST',
			url: 'includes/process.php',
			data: dataString,
			cache: false,
			success: function(resdata) {
				var res = $.parseJSON(resdata);
				if (res.success == true) {
					$.LoadingOverlay("hide");
					getQuesList();
				}
				else if (res.success == false) {
					$.LoadingOverlay("hide");
					swal({text: res.msg, buttons: false, icon: "warning", timer: 2000});
				}
			}, error: function() {
				swal({text: 'Oops. something went wrong please try again.', buttons: false, icon: "error", timer: 2000 });
			}
		});
	} else {
		swal({text: 'Please select at-least one Question', buttons: false, icon: "warning", timer: 2000 });
	}
});

//----------Audio-Upload-All-Template-----------------------
$('body').on('change', '.uploadAudioFile', function(){
	$.LoadingOverlay("show");
	var input_id	= $(this).data('id');
	var assets_id	= $(this).data('assets');
	var file_data	= $(this).prop('files')[0];
	var form_data	= new FormData();
	form_data.append('file', file_data);
	form_data.append('qAudiofiles', '1');
	$.ajax({
		url: "includes/upload.php",
		type: "POST",
		data: form_data,
		contentType: false,
		cache: false,
		processData: false,
		success: function(resdata){
			var res = $.parseJSON(resdata);
			if (res.success == true){
				$(input_id).val(res.file_name);						
				$('.'+ assets_id).addClass("disabled").attr('disabled', true);
				$('.'+ assets_id + '_container').show();
				$('.'+ assets_id + '_data a').text(res.file_name);
				$('.'+ assets_id + '_data a').attr('data-src', assets_path + res.file_name);
				$('.'+ assets_id + '_container a.delete_assets').attr('data-assets', res.file_name);
				$('.'+ assets_id + '_container a.delete_assets').attr('data-assets-id', assets_id);
				$('.'+ assets_id + '_container a.delete_assets').attr('data-input-id', input_id);
				$('.'+ assets_id + '_container a.delete_assets').attr('data-path', res.path);
				swal({text: res.msg, buttons: false, timer: 1000});
			}
			else if (res.success == false){
				$(input_id).val('');
				swal({text: res.msg, buttons: false, icon: "warning", timer: 1000});
			}
			$.LoadingOverlay("hide");
		},error: function(){
			$.LoadingOverlay("hide");
			swal("Error", 'Oops, something went wrong. Please try again later.?', "error");
		}
	});
});

//-----------Video-Upload-All-Template-----------------------
$('body').on('change', '.uploadVideoFile', function() {
	$.LoadingOverlay("show");
	var input_id	= $(this).data('id');
	var assets_id	= $(this).data('assets');
	var file_data	= $(this).prop('files')[0];
	var form_data	= new FormData();
	form_data.append('file', file_data);
	form_data.append('qVideofiles', '1');
	$.ajax({
		url: "includes/upload.php",
		type: "POST",
		data: form_data,
		contentType: false,
		cache: false,
		processData: false,
		success: function(resdata) {
			var res = $.parseJSON(resdata);
			if (res.success == true) {
				$(input_id).val(res.file_name);
				swal({text: res.msg, buttons: false, timer: 1000});
				$('.'+ assets_id).addClass("disabled").attr('disabled', true);
				$('.'+ assets_id + '_container').show();
				$('.'+ assets_id + '_data a').text(res.file_name);
				$('.'+ assets_id + '_data a').attr('data-src', assets_path + res.file_name);
				$('.'+ assets_id + '_container a.delete_assets').attr('data-assets', res.file_name);
				$('.'+ assets_id + '_container a.delete_assets').attr('data-assets-id', assets_id);
				$('.'+ assets_id + '_container a.delete_assets').attr('data-input-id', input_id);
				$('.'+ assets_id + '_container a.delete_assets').attr('data-path', res.path);
			}
			else if (res.success == false) {
				$(input_id).val('');
				swal({text: res.msg, buttons: false, icon: "warning", timer: 1000});
			}
			$.LoadingOverlay("hide");
		},error: function() {
			$.LoadingOverlay("hide");
			swal("Error", 'Oops, something went wrong. Please try again later.?', "error");
		}
	});
});

//-----------Image-Upload-All-Template-----------------------
$('body').on('change', '.uploadImgFile', function() {
	$.LoadingOverlay("show");
	var cur 		= $(this);
	var input_id	= $(this).data('id');
	var assets_id	= $(this).data('assets');
	var file_data	= $(this).prop('files')[0];
	var form_data	= new FormData();
	form_data.append('file', file_data);
	form_data.append('qImgfiles', '1');
	$.ajax({
		url: "includes/upload.php",
		type: "POST",
		data: form_data,
		contentType: false,
		cache: false,
		processData: false,
		success: function(resdata) {
			var res = $.parseJSON(resdata);
			if (res.success == true) {
				$(input_id).val(res.file_name);
				swal({text: res.msg, buttons: false, timer: 1000});
				cur.closest('div').addClass("disabled");
				$('.'+ assets_id).addClass("disabled").attr('disabled', true);
				$('.'+ assets_id + '_container').show();
				$('.'+ assets_id + '_data a').text(res.file_name);
				$('.'+ assets_id + '_data a').attr('data-src', assets_path + res.file_name);
				$('.'+ assets_id + '_container a.delete_assets').attr('data-assets', res.file_name);
				$('.'+ assets_id + '_container a.delete_assets').attr('data-assets-id', assets_id);
				$('.'+ assets_id + '_container a.delete_assets').attr('data-input-id', input_id);
				$('.'+ assets_id + '_container a.delete_assets').attr('data-path', res.path);
			}
			else if (res.success == false) {
				$(input_id).val('');
				swal({text: res.msg, buttons: false, icon: "warning", timer: 1000});
			}
			$.LoadingOverlay("hide");
		},error: function() {
			$.LoadingOverlay("hide");
			swal("Error", 'Oops, something went wrong. Please try again later.?', "error");
		}
	});
});

//-----------Image-Upload-Video-Template--------------------
$('body').on('change', '.uploadImgFileVideo', function() {
	$.LoadingOverlay("show");
	var cur 		= $(this);
	var input_id	= $(this).data('id');
	var assets_id	= $(this).data('assets');
	var file_data	= $(this).prop('files')[0];
	var form_data	= new FormData();
	form_data.append('file', file_data);
	form_data.append('qImgfiles', '1');
	$.ajax({
		url: "includes/upload.php",
		type: "POST",
		data: form_data,
		contentType: false,
		cache: false,
		processData: false,
		success: function(resdata) {
			var res = $.parseJSON(resdata);
			if (res.success == true) {
				$(input_id).val(res.file_name);
				$('.'+ assets_id).addClass("disabled").attr('disabled', true);
				var video	= assets_id.split('_');
				var videoQ	= video[0];
				var videoID	= video[2];
				cur.closest('div').addClass("disabled");
				$('.'+ videoQ +'_assets_container_'+ videoID).show();
				$('.'+ videoQ +'_assets_data_'+ videoID + ' a.view_assets').text(res.file_name);
				$('.'+ videoQ +'_assets_data_'+ videoID + ' a.view_assets').attr('data-src', assets_path + res.file_name);
				$('.'+ videoQ +'_assets_container_'+ videoID +' a.delete_assets').attr('data-assets', res.file_name);
				$('.'+ videoQ +'_assets_container_'+ videoID +' a.delete_assets').attr('data-assets-id', assets_id);
				$('.'+ videoQ +'_assets_container_'+ videoID +' a.delete_assets').attr('data-input-id', input_id);
				$('.'+ videoQ +'_assets_container_'+ videoID +' a.delete_assets').attr('data-path', res.path);
				swal({text: res.msg, buttons: false, timer: 1000});
			}
			else if (res.success == false){
				$(input_id).val('');
				swal({text: res.msg, buttons: false, icon: "warning", timer: 1000});
			}
			$.LoadingOverlay("hide");
		},error: function() {
			$.LoadingOverlay("hide");
			swal("Error", 'Oops, something went wrong. Please try again later.?', "error");
		}
	});
});

//-----------Video-Upload-Video-Template------------------------
$('body').on('change', '.uploadFileVideoTemp', function() {
	$.LoadingOverlay("show");
	var input_id	= $(this).data('id');
	var assets_id	= $(this).data('assets');
	var file_data	= $(this).prop('files')[0];
	var form_data	= new FormData();
	form_data.append('file', file_data);
	form_data.append('qVideofiles', '1');
	$.ajax({
		url: "includes/upload.php",
		type: "POST",
		data: form_data,
		contentType: false,
		cache: false,
		processData: false,
		success: function(resdata) {
			var res = $.parseJSON(resdata);
			if (res.success == true) {
				$(input_id).val(res.file_name);
				$('.'+ assets_id).addClass("disabled").attr('disabled', true);
				var video	= assets_id.split('_');
				var videoQ	= video[0];
				var videoID	= video[2];
				$('.'+ videoQ +'_assets_container_'+ videoID).show();
				$('.'+ videoQ +'_assets_data_'+ videoID + ' a.view_assets').text(res.file_name);
				$('.'+ videoQ +'_assets_data_'+ videoID + ' a.view_assets').attr('data-src', assets_path + res.file_name);
				$('.'+ videoQ +'_assets_container_'+ videoID +' a.delete_assets').attr('data-assets', res.file_name);
				$('.'+ videoQ +'_assets_container_'+ videoID +' a.delete_assets').attr('data-assets-id', assets_id);
				$('.'+ videoQ +'_assets_container_'+ videoID +' a.delete_assets').attr('data-input-id', input_id);
				$('.'+ videoQ +'_assets_container_'+ videoID +' a.delete_assets').attr('data-path', res.path);
				swal({text: res.msg, buttons: false, timer: 1000});
			}
			else if (res.success == false){
				$(input_id).val('');
				swal({text: res.msg, buttons: false, icon: "warning", timer: 1000});
			}
			$.LoadingOverlay("hide");
		},error: function() {
			$.LoadingOverlay("hide");
			swal("Error", 'Oops, something went wrong. Please try again later.?', "error");
		}
	});
});

//----------Audio-Upload-Video-Template-----------------------
$('body').on('change', '.uploadAudioFileVideoTemp', function(){
	$.LoadingOverlay("show");
	var input_id	= $(this).data('id');
	var assets_id	= $(this).data('assets');
	var file_data	= $(this).prop('files')[0];
	var form_data	= new FormData();
	form_data.append('file', file_data);
	form_data.append('qAudiofiles', '1');
	$.ajax({
		url: "includes/upload.php",
		type: "POST",
		data: form_data,
		contentType: false,
		cache: false,
		processData: false,
		success: function(resdata) {
			var res = $.parseJSON(resdata);
			if (res.success == true) {
				$(input_id).val(res.file_name);
				$('.'+ assets_id).addClass("disabled").attr('disabled', true);
				var video	= assets_id.split('_');
				var videoQ	= video[0];
				var videoID	= video[2];
				$('.'+ videoQ +'_assets_container_'+ videoID).show();
				$('.'+ videoQ +'_assets_data_'+ videoID + ' a.view_assets').text(res.file_name);
				$('.'+ videoQ +'_assets_data_'+ videoID + ' a.view_assets').attr('data-src', assets_path + res.file_name);
				$('.'+ videoQ +'_assets_container_'+ videoID +' a.delete_assets').attr('data-assets', res.file_name);
				$('.'+ videoQ +'_assets_container_'+ videoID +' a.delete_assets').attr('data-assets-id', assets_id);
				$('.'+ videoQ +'_assets_container_'+ videoID +' a.delete_assets').attr('data-input-id', input_id);
				$('.'+ videoQ +'_assets_container_'+ videoID +' a.delete_assets').attr('data-path', res.path);
				swal({text: res.msg, buttons: false, timer: 1000});
			}
			else if (res.success == false){
				$(input_id).val('');
				swal({text: res.msg, buttons: false, icon: "warning", timer: 1000});
			}
			$.LoadingOverlay("hide");
		},error: function() {
			$.LoadingOverlay("hide");
			swal("Error", 'Oops, something went wrong. Please try again later.?', "error");
		}
	});
});

//-----------Record Audio-All-Template-----------------------
$('body').on('click', '.rec-audio', function() {
	var input_id  = $(this).attr('data-input-id');
	var assets_id = $(this).attr('data-assets');
	if (input_id != '') {
		$.LoadingOverlay("show");
		var $modal = $('#load_popup_modal_show');
		$modal.load('rec-audio-modal.php', {'data-id': input_id, 'assets-id': assets_id}, function(res) {
			if (res != '') {
				$.LoadingOverlay("hide");
				$modal.modal('show', {backdrop: 'static', keyboard: false});
			}
			else {
				$.LoadingOverlay("hide");
				swal({text: 'Oops, something went wrong. Please try again later', buttons: false, icon: "error", timer: 1000});
			}
		});
	}
});
		
//-----------Record Video-All-Template-----------------------
$('body').on('click', '.rec-video', function() {
	var input_id  = $(this).attr('data-input-id');
	var assets_id = $(this).attr('data-assets');
	if (input_id != '') {
		$.LoadingOverlay("show");
		var $modal = $('#load_popup_modal_show');
		$modal.load('rec-video-modal.php', {'data-id': input_id, 'assets-id': assets_id}, function(res) {
			if (res != '') {
				$.LoadingOverlay("hide");
				$modal.modal('show', {backdrop: 'static', keyboard: false});
			}
			else {
				$.LoadingOverlay("hide");
				swal({text: 'Oops, something went wrong. Please try again later', buttons: false, icon: "error", timer: 1000});
			}
		});
	}
});

//-----------Record Screen-All-Template----------------------
$('body').on('click', '.rec-screen', function() {
	var input_id  = $(this).attr('data-input-id');
	var assets_id = $(this).attr('data-assets');
	if (input_id != '') {
		$.LoadingOverlay("show");
		var $modal = $('#load_popup_modal_show');
		$modal.load('rec-screen-modal.php', {'data-id': input_id, 'assets-id': assets_id}, function(res) {
			if (res != '') {
				$.LoadingOverlay("hide");
				$modal.modal('show', {backdrop: 'static', keyboard: false});
			}
			else {
				$.LoadingOverlay("hide");
				swal({text: 'Oops, something went wrong. Please try again later', buttons: false, icon: "error", timer: 1000});
			}
		});
	}
});

//-----------Record-Audio-Video-Question-Template------------------
$('body').on('click', '.rec-audio-videoq', function() {
	var input_id  = $(this).attr('data-input-id');
	var assets_id = $(this).attr('data-assets');
	if (input_id != '') {
		$.LoadingOverlay("show");
		var $modal = $('#load_popup_modal_show');
		$modal.load('rec-audio-modal.php', {'data-id': input_id, 'assets-id': assets_id, 'video': true}, function(res) {
			if (res != '') {
				$.LoadingOverlay("hide");
				$modal.modal('show', {backdrop: 'static', keyboard: false});
			}
			else {
				$.LoadingOverlay("hide");
				swal({text: 'Oops, something went wrong. Please try again later', buttons: false, icon: "error", timer: 1000});
			}
		});
	}
});

//-----------Record-Video-Video-Question-Template------------------
$('body').on('click', '.rec-video-videoq', function() {
	var input_id  = $(this).attr('data-input-id');
	var assets_id = $(this).attr('data-assets');
	if (input_id != '') {
		$.LoadingOverlay("show");
		var $modal = $('#load_popup_modal_show');
		$modal.load('rec-video-modal.php', {'data-id': input_id, 'assets-id': assets_id, 'video': true}, function(res) {
			if (res != '') {
				$.LoadingOverlay("hide");
				$modal.modal('show', {backdrop: 'static', keyboard: false});
			}
			else {
				$.LoadingOverlay("hide");
				swal({text: 'Oops, something went wrong. Please try again later', buttons: false, icon: "error", timer: 1000});
			}
		});
	}
});

//-----------Record-Screen-Video-Question-Template-----------------
$('body').on('click', '.rec-screen-videoq', function() {
	var input_id  = $(this).attr('data-input-id');
	var assets_id = $(this).attr('data-assets');
	if (input_id != '') {
		$.LoadingOverlay("show");
		var $modal = $('#load_popup_modal_show');
		$modal.load('rec-screen-modal.php', {'data-id': input_id, 'assets-id': assets_id, 'video': true}, function(res) {
			if (res != '') {
				$.LoadingOverlay("hide");
				$modal.modal('show', {backdrop: 'static', keyboard: false});
			}
			else {
				$.LoadingOverlay("hide");
				swal({text: 'Oops, something went wrong. Please try again later', buttons: false, icon: "error", timer: 1000});
			}
		});
	}
});

//-----------For-Video-Template-------------------
$('#upload_videoq_file').click(function() {
	$.LoadingOverlay("show");
	var input_id	= $('#videoqfile').data('id');
	var assets_id	= $('#videoqfile').data('assets');
	var file_data	= $('#videoqfile').prop('files')[0];
	var form_data	= new FormData();
	form_data.append('file', file_data);
	form_data.append('videoq_file', true);
	$.ajax({
		url: "includes/upload.php",
		type: "POST",
		data: form_data,
		contentType: false,
		cache: false,
		processData:false,
		success: function(resdata) {
			var res = $.parseJSON(resdata);
			if (res.success == true) {
				$(input_id).val(res.file_name);
				$('.'+ assets_id).addClass("disabled").attr('disabled', true);
				$('.video_assets_container,.video_container').show();
				$('.video_assets_container a').attr('data-assets', res.file_name);
				$('.video_assets_container a').attr('data-assets-id', assets_id);
				$('.video_assets_container a').attr('data-input-id', input_id);
				$('.video_assets_container a').attr('data-path', res.path);
				
				//-----Load-New-Upload-Video-Question-File------
				$('#videoclip').get(0).pause();
				$('#qvideo').attr('src', res.loadPath + res.file_name);
				$('#videoclip').get(0).load();
				swal({text: res.msg, buttons: false, timer: 1000});
			}
			else if (res.success == false){
				$(input_id).val('');
				swal({text: res.msg, buttons: false, icon: "warning", timer: 1000});
			}
			$.LoadingOverlay("hide");
		},error: function() {
			$.LoadingOverlay("hide");
			swal("Error", 'Oops, something went wrong. Please try again later.?', "error");
		}
	});
});

//-----------Doc-Upload-Video-Template---------------------
$('body').on('change', '.uploadDocFileVideo', function() {
	$.LoadingOverlay("show");
	var cur 		= $(this);
	var input_id	= $(this).data('id');
	var assets_id	= $(this).data('assets');
	var file_data	= $(this).prop('files')[0];
	var form_data	= new FormData();
	form_data.append('file', file_data);
	form_data.append('qDocfiles', '1');
	$.ajax({
		url: "includes/upload.php",
		type: "POST",
		data: form_data,
		contentType: false,
		cache: false,
		processData: false,
		success: function(resdata) {
			var res = $.parseJSON(resdata);
			if (res.success == true) {
				$(input_id).val(res.file_name);
				$('.'+ assets_id).addClass("disabled").attr('disabled', true);
				var video	= assets_id.split('_');
				var videoQ	= video[0];
				var videoID	= video[2];
				cur.closest('div').addClass("disabled");
				$('.'+ videoQ +'_assets_container_'+ videoID).show();
				$('.'+ videoQ +'_assets_data_'+ videoID + ' a.view_assets').text(res.file_name);
				$('.'+ videoQ +'_assets_data_'+ videoID + ' a.view_assets').attr('data-src', assets_path + res.file_name);
				$('.'+ videoQ +'_assets_container_'+ videoID +' a.delete_assets').attr('data-assets', res.file_name);
				$('.'+ videoQ +'_assets_container_'+ videoID +' a.delete_assets').attr('data-assets-id', assets_id);
				$('.'+ videoQ +'_assets_container_'+ videoID +' a.delete_assets').attr('data-input-id', input_id);
				$('.'+ videoQ +'_assets_container_'+ videoID +' a.delete_assets').attr('data-path', res.path);
				swal({text: res.msg, buttons: false, timer: 1000});
			}
			else if (res.success == false){
				$(input_id).val('');
				swal({text: res.msg, buttons: false, icon: "warning", timer: 1000});
			}
			$.LoadingOverlay("hide");
		},error: function() {
			$.LoadingOverlay("hide");
			swal("Error", 'Oops, something went wrong. Please try again later.?', "error");
		}
	});
});

//--------------Delete-Assets-All-Templates------------
$('body').on('click', '.delete_assets', function() {
	var cur   		= $(this);
	var file_name	= cur.attr('data-assets');
	var assetsId	= cur.attr('data-assets-id');
	var inputId		= cur.attr('data-input-id');
	var path		= cur.attr('data-path');
	var divId		= cur.closest('div').attr('class');
	var dataString  = 'delete_assets='+ true +'&assets_path='+ path +'&file='+ file_name;
	if (file_name){
		swal({
			title: "Are you sure?",
			text: "Delete this Assets.",
			icon: "warning",
			buttons: [true, 'Delete'],
			dangerMode: true, }).then((willDelete) => { if (willDelete) {
				$.LoadingOverlay("show");
				$.ajax({
					url: "includes/delete-file.php",
					type: "POST",
					data: dataString,
					cache: false,
					success: function(resdata){
						var res = $.parseJSON(resdata);
						if (res.success == false) {
							$.LoadingOverlay("hide");
							swal({text: res.msg, buttons: false, icon: "error", timer: 2000});
						}
						else if (res.success == true) {
							$(inputId).val('');
							$('.'+ assetsId).removeClass("disabled").removeAttr('disabled', true);
							$('.'+ divId).hide();
							$.LoadingOverlay("hide");
							swal({text: res.msg, buttons: [false, 'OK'], closeOnClickOutside: false, closeOnEsc: false});
						}
					},error: function() {
						swal({text: 'Oops. something went wrong please try again.', buttons: false, icon: "error", timer: 1000});
					}
				});
			} else { swal({text: 'Your file is safe', buttons: false, timer: 1000}); }
		});
	}
});

//--------------Delete-Video-Assets--------------
$('body').on('click', '.delete_video_assets', function() {
	var cur   		= $(this);
	var file_name	= cur.attr('data-assets');
	var assetsId	= cur.attr('data-assets-id');
	var inputId		= cur.attr('data-input-id');
	var path		= cur.attr('data-path');
	var divId		= cur.closest('div').attr('class');
	var dataString = 'delete_assets='+true+'&assets_path='+path+'&file='+file_name;
	if (file_name) {
		swal({
			title: "Are you sure?",
			text: "Delete this file.",
			icon: "warning",
			buttons: [true, 'Delete'],
			dangerMode: true, }).then((willDelete) => { if (willDelete) {
				$.LoadingOverlay("show");
				$.ajax({
					url: "includes/delete-file.php",
					type: "POST",
					data: dataString,
					cache: false,
					success: function(resdata){
						var res = $.parseJSON(resdata);
						if (res.success == false) {
							$.LoadingOverlay("hide");
							swal({text: res.msg, buttons: false, icon: "error", timer: 1000});
						}
						else if (res.success == true) {
							$(inputId).val('');
							$('.'+ assetsId).removeClass("disabled").removeAttr('disabled', true);
							$('.'+ divId).hide();
							$('.video_container').hide();
							$('.videoq_box,.cue_pt_show').empty();
							videoI = 1;
							$.LoadingOverlay("hide");
							swal({text: res.msg, buttons: false, timer: 1000});
						}
					},error: function() {
						swal({text: 'Oops. something went wrong please try again.', buttons: false, icon: "error", timer: 2000});
					}
				});
			} else { swal({text: 'Your file is safe', buttons: false, timer: 1000}); }
		});
	}
});

$('body').on('click', '.video_modal', function(){
	var cur_model	= $(this);
	var modal_id	= cur_model.data('modal-id');
	var assets_id	= cur_model.data('assets');
	$('#'+ modal_id).show();
});

$('body').on('click', '.vts_modal_save', function(){
	var cur_model	= $(this);
	var modal_id	= cur_model.data('modal-id');
	var assets_id	= cur_model.data('assets');
	var view_id		= cur_model.data('assets-view');
	var input_id	= cur_model.data('input-id');
	if ($('#'+ input_id).val().trim().length > 0){
		$('#'+ modal_id).hide();
		$('.'+ view_id).show();
		$('.'+ assets_id).addClass("disabled").attr('disabled', true);
	}
});

$('body').on('click', '.vts_modal_cls', function(){
	var cur_model	= $(this);
	var modal_id	= cur_model.data('modal-id');
	var assets_id	= cur_model.data('assets');
	var input_id	= cur_model.data('input-id');
	$('#'+ input_id).val('');
});

$('body').on('click', '.vts_modal_close', function(){
	var cur_model	= $(this);
	var modal_id	= cur_model.data('modal-id');
	var assets_id	= cur_model.data('assets');
	var input_id	= cur_model.data('input-id');
	var view_id		= cur_model.data('assets-view');
	$('#'+ modal_id).hide();
	$('.'+ view_id).hide();
	$('#'+ input_id).val('');
	$('.'+ assets_id).removeClass("disabled").removeAttr('disabled', true);
});

//--------------Delete-Video-Assets--------------
$('body').on('click', '.delete_tts', function() {
	var cur   	 	= $(this);
	var assetsId	= cur.attr('data-assets');
	var inputId		= cur.attr('data-input-id');
	var divId		= cur.closest('div').attr('class');
	swal({
		title: "Are you sure?",
		text: "Delete Text to Speech Data",
		icon: "warning",
		buttons: [true, 'Delete'],
		dangerMode: true, }).then((willDelete) => { if (willDelete) {
			$.LoadingOverlay("show");
			$('#'+ inputId).val('');
			$('.'+ assetsId).removeClass("disabled").removeAttr('disabled', true);
			$('.'+ divId).hide();
			$.LoadingOverlay("hide");
		} else { swal({text: 'Your data is safe', buttons: false, timer: 2000}); }
	});
});

//-------------Video-Cue-----------------//
var videoI   = $('.videoqboxs').length + 1;
var voption  = $('.videoqboxs').length;
var vradio 	 = $('.videoqboxs').length;
var totalq	 = '<?php echo $totalq ?>';
$('body').on('click', '.videocueAdd', function(){
	if (videoI) {
		totalq++;
		var cue_point = $('#cue_point').val();
		if (cue_point != '' && cue_point != 0) {
			$(".videoq_box").append('<div class="Ans_box_'+videoI+' videoqboxs"><div class="quesbox CQtmp">\
			<input type="hidden" name="cue_point[]" class="cue_point" id="cue_point_'+ videoI +'" value="'+ cue_point +'" />\
			<div class="row">\
				<div class="col-sm-12">\
					<div class="critical_div">\
						<h3 class="qusHeading">QUESTION #'+totalq+'</h3>\
						<div class="CIRtiCaltext">Critical Question: <input type="checkbox" name="video_criticalQ[]" id="video_criticalQ" class="form-control criticalQ" value="1" />\
						</div>\
					</div>\
					<div class="quesbox-flex">\
						<div class="form-group">\
							<input type="text" name="videoq[]" id="videoq" class="form-control QuesForm inputtextWrap videoq" placeholder="Click here to enter question" required="required">\
							<div class="videoqq_assets_container_'+videoI+'" style="display:none;">\
								<div class="videoqq_assets_data_'+videoI+'"></div>\
								<a href="javascript:void(0);" data-assets="" data-assets-id="" data-input-id="" data-path="" class="delete_assets" title="Delete"><i class="fa fa-times" aria-hidden="true"></i></a>\
							</div>\
							 <ul class="QueBoxIcon">\
								<li class="dropdown">\
									<button class="btn1 btn-secondary dropdown-toggle videoqq_assets" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img class="img-fluid" src="img/list/audio.svg"></button>\
									<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">\
										<a class="dropdown-item">\
											<div class="tooltipLeft uploadicon">\
												<label for="file-input-videoqq-addAudio_'+videoI+'">\
													<img class="img-fluid" src="img/list/add_audio.svg">\
													<span class="tooltiptext">Add Audio</span>\
												</label>\
												<input id="file-input-videoqq-addAudio_'+videoI+'" data-id="#videoqq_audio_'+videoI+'" data-assets="videoqq_assets_'+videoI+'" class="uploadAudioFileVideoTemp" type="file" name="videoqq_Audio_'+videoI+'" />\
												<input type="hidden" name="videoqq_audio[]" id="videoqq_audio_'+videoI+'" />\
											</div>\
										</a>\
										<a class="dropdown-item">\
											<div class="tooltipLeft">\
												<img class="img-fluid rec-audio-videoq" data-input-id="videoqq_rec_audio_'+videoI+'" data-assets="videoqq_assets_'+videoI+'" src="img/list/record_audio.svg">\
												<span class="tooltiptext">Record Audio</span>\
											</div>\
											<input type="hidden" name="videoqq_rec_audio[]" id="videoqq_rec_audio_'+videoI+'" />\
										 </a>\
										<a data-toggle="modal" data-target="#videoqqtts" class="dropdown-item">\
											<div class="tooltipLeft">\
												<img class="img-fluid" src="img/list/text_speech.svg" />\
												<span class="tooltiptext">Text to Speech</span>\
											</div>\
										 </a>\
									 </div>\
								</li>\
							</ul>\
						</div>\
					</div>\
					<div class="form-popup draggable Ques-pop-style modal" id="videoqqtts">\
						<div class="popheading">Text to Speech</div>\
						<div class="textarea">\
							<textarea type="text" class="form-control1 inputtextWrap" name="videoqq_text_to_speech[]" id="videoqq_text_to_speech_'+videoI+'"></textarea>\
						</div>\
						<div class="submitRight1">\
							<button type="button" class="btn1 submitbtn1" data-dismiss="modal">Insert</button>\
							<button type="button" class="btn1 submitbtn1" onclick="clearData("videoqq_text_to_speech_'+videoI+'");">Clear</button>\
							<button type="button" class="btn1 submitbtn1" data-dismiss="modal">Cancel</button>\
						</div>\
					</div>\
					<div class="q-icon-banner clearfix">\
						<ul class="QueBoxIcon QueBoxIcon1">\
							<h5>Add Assets</h5>\
							<li class="dropdown">\
								<button class="btn1 btn-secondary dropdown-toggle videoq_assets_'+videoI+'" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">\
								<img class="img-fluid" src="img/list/audio.svg"></button>\
								<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">\
									<a class="dropdown-item">\
										<div class="tooltip1">\
											<img class="img-fluid video_modal" data-modal-id="videoqModal'+videoI+'" data-assets="videoq_assets_'+videoI+'" src="img/list/text_speech.svg" /><span class="tooltiptext">Text to Speech</span>\
										</div>\
									</a>\
									<a class="dropdown-item">\
										<div class="tooltip1 uploadicon">\
											<label for="file-input-videoq-addAudio_'+videoI+'">\
												<img class="img-fluid" src="img/list/add_audio.svg">\
												<span class="tooltiptext">Add Audio</span>\
											</label>\
											<input id="file-input-videoq-addAudio_'+videoI+'" data-id="#videoq_audio_'+videoI+'" data-assets="videoq_assets_'+videoI+'" class="uploadAudioFileVideoTemp" type="file" name="videoq_Audio_'+videoI+'" />\
											<input type="hidden" name="videoq_audio[]" id="videoq_audio_'+videoI+'" />\
										</div>\
									</a>\
									<a class="dropdown-item">\
										<div class="tooltip1">\
											<img class="img-fluid rec-audio-videoq" data-input-id="videoq_rec_audio_'+videoI+'" data-assets="videoq_assets_'+videoI+'" src="img/list/record_audio.svg">\
											<span class="tooltiptext">Record Audio</span>\
										</div>\
										<input type="hidden" name="videoq_rec_audio[]" id="videoq_rec_audio_'+videoI+'" />\
									 </a>\
								</div>\
							</li>\
							<li class="dropdown">\
								<button class="btn1 btn-secondary dropdown-toggle videoq_assets_'+videoI+'" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">\
								<img class="img-fluid" src="img/list/video.svg"></button>\
								<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">\
									<a class="dropdown-item">\
										<div class="tooltip1 uploadicon">\
											<label for="file-input-videoq-addVideo_'+videoI+'">\
												<img class="img-fluid" src="img/list/add_video.svg">\
												<span class="tooltiptext">Add Video</span>\
											</label>\
											<input id="file-input-videoq-addVideo_'+videoI+'" data-id="#videoq_video_'+videoI+'" data-assets="videoq_assets_'+videoI+'" class="uploadFileVideoTemp" type="file" name="videoq_Video_'+videoI+'" />\
											<input type="hidden" name="videoq_video[]" id="videoq_video_'+videoI+'" />\
										</div>\
									</a>\
									<a class="dropdown-item">\
										<div class="tooltip1">\
											<img class="img-fluid rec-video-videoq" data-input-id="videoq_rec_video_'+videoI+'" data-assets="videoq_assets_'+videoI+'" src="img/qus_icon/rec_video.svg">\
											<span class="tooltiptext">Record Video</span>\
										 </div>\
										 <input type="hidden" name="videoq_rec_video[]" id="videoq_rec_video_'+videoI+'" />\
									</a>\
									<a class="dropdown-item">\
										<div class="tooltip1">\
											<img class="img-fluid rec-screen-videoq" data-input-id="videoq_rec_screen_'+videoI+'" data-assets="videoq_assets_'+videoI+'" src="img/list/screen_record.svg">\
											<span class="tooltiptext">Record Screen</span>\
										 </div>\
										 <input type="hidden" name="videoq_rec_screen[]" id="videoq_rec_screen_'+videoI+'" />\
									</a>\
								</div>\
							</li>\
							<li>\
								<div class="tooltip uploadicon videoq_assets_'+videoI+'">\
									<label for="file-input-videoq-addImg_'+videoI+'">\
										<img class="img-fluid" src="img/list/image.svg">\
										<span class="tooltiptext">Add Image</span>\
									</label>\
									<input id="file-input-videoq-addImg_'+videoI+'" data-id="#videoq_img_'+videoI+'" data-assets="videoq_assets_'+videoI+'" class="uploadImgFileVideo videoq_assets_'+videoI+'" type="file" name="videoq_Img_'+videoI+'" />\
									<input type="hidden" name="videoq_img[]" id="videoq_img_'+videoI+'" />\
								</div>\
							</li>\
							<li>\
								<div class="tooltip uploadicon videoq_assets_'+videoI+'">\
									<label for="file-input-videoq-addDoc-'+videoI+'">\
										<img class="img-fluid" src="img/list/add_document.svg" />\
										<span class="tooltiptext">Add Document</span>\
									</label>\
									<input id="file-input-videoq-addDoc-'+videoI+'" data-id="#videoq_doc_'+videoI+'" data-assets="videoq_assets_'+videoI+'" class="uploadDocFileVideo videoq_assets_'+videoI+'" type="file" name="videoq_Doc_'+videoI+'" />\
									<input type="hidden" name="videoq_doc[]" id="videoq_doc_'+videoI+'" />\
								</div>\
							</li>\
						</ul>\
						<div class="Ques-comp">\
							<h5>Add Competency score</h5>\
							<?php if ( ! empty($sim_com['comp_col_1']) && ! empty($sim_com['comp_val_1'])): ?>
							<div class="form-group QusScore">\
								<input type="text" class="form-control videoq videoq_val_1" name="videoq_ques_val_1[]" id="videoq_ques_val_1_'+videoI+'" onkeypress="return isNumberKey(event);" />\
							</div>\
							<?php endif; if ( ! empty($sim_com['comp_col_2']) && ! empty($sim_com['comp_val_2'])): ?>
							<div class="form-group QusScore">\
								<input type="text" class="form-control videoq videoq_val_2" name="videoq_ques_val_2[]" id="videoq_ques_val_2_'+videoI+'" onkeypress="return isNumberKey(event);" />\
							</div>\
							<?php endif; if ( ! empty($sim_com['comp_col_3']) && ! empty($sim_com['comp_val_3'])): ?>
							<div class="form-group QusScore">\
								<input type="text" class="form-control videoq videoq_val_3" name="videoq_ques_val_3[]" id="videoq_ques_val_3_'+videoI+'" onkeypress="return isNumberKey(event);" />\
							</div>\
							<?php endif; if ( ! empty($sim_com['comp_col_4']) && ! empty($sim_com['comp_val_4'])): ?>
							<div class="form-group QusScore">\
								<input type="text" class="form-control videoq videoq_val_4" name="videoq_ques_val_4[]" id="videoq_ques_val_4_'+videoI+'" onkeypress="return isNumberKey(event);" />\
							</div>\
							<?php endif; if ( ! empty($sim_com['comp_col_5']) && ! empty($sim_com['comp_val_5'])): ?>
							<div class="form-group QusScore">\
								<input type="text" class="form-control videoq videoq_val_5" name="videoq_ques_val_5[]" id="videoq_ques_val_5_'+videoI+'" onkeypress="return isNumberKey(event);" />\
							</div>\
							<?php endif; if ( ! empty($sim_com['comp_col_6']) && ! empty($sim_com['comp_val_6'])): ?>
							<div class="form-group QusScore">\
								<input type="text" class="form-control videoq videoq_val_6" name="videoq_ques_val_6[]" id="videoq_ques_val_6_'+videoI+'" onkeypress="return isNumberKey(event);" />\
							</div>\
							<?php endif; ?>
						</div>\
					</div>\
					<div class="videoq_assets_container_'+videoI+'" style="display:none;">\
						<div class="videoq_assets_data_'+videoI+'"><a class="view_assets" data-src="" href="javascript:;"></a></div>\
						<a href="javascript:void(0);" data-assets="" data-assets-id="" data-input-id="" data-path="" class="delete_assets" title="Delete"><i class="fa fa-times" aria-hidden="true"></i></a>\
					</div>\
					<div class="videoq_tts_container_'+videoI+'" style="display:none;">\
						<div class="videoq_tts_data_'+videoI+'">\
							<div class="tooltip1">\
								<img class="img-fluid" src="img/list/text_speech.svg"><span class="tooltiptext">Text to Speech</span>\
							</div>\
						</div>\
						<a href="javascript:void(0);" class="delete_tts" data-modal-id="videoqModal'+videoI+'" data-assets="videoq_assets_'+videoI+'" data-input-id="videoq_text_to_speech_'+videoI+'" title="Delete"><i class="fa fa-times" aria-hidden="true"></i></a>\
					</div>\
				</div>\
			</div>\
		</div>\
		<div class="form-popup draggable" id="videoqModal'+videoI+'">\
			<div class="popheading">Text to Speech</div>\
			<div class="textarea">\
				<textarea type="text" class="form-control1 inputtextWrap" name="videoq_text_to_speech[]" id="videoq_text_to_speech_'+videoI+'"></textarea>\
			</div>\
			<div class="submitRight1">\
				<button type="button" class="btn1 submitbtn1 vts_modal_save" data-modal-id="videoqModal'+videoI+'" data-assets="videoq_assets_'+videoI+'" data-input-id="videoq_text_to_speech_'+videoI+'" data-assets-view="videoq_tts_container_'+videoI+'">Insert</button>\
				<button type="button" class="btn1 submitbtn1 vts_modal_cls" data-modal-id="videoqModal'+videoI+'" data-assets="videoq_assets_'+videoI+'" data-input-id="videoq_text_to_speech_'+videoI+'" data-assets-view="videoq_tts_container_'+videoI+'">Clear</button>\
				<button type="button" class="btn1 submitbtn1 vts_modal_close" data-modal-id="videoqModal'+videoI+'" data-assets="videoq_assets_'+videoI+'" data-input-id="videoq_text_to_speech_'+videoI+'" data-assets-view="videoq_tts_container_'+videoI+'">Cancel</button>\
			</div>\
		</div>\
		<div class="form-popup draggable" id="videofModal'+videoI+'">\
			<div class="popheading">Text to Speech</div>\
			<div class="textarea">\
				<textarea type="text" class="form-control1 inputtextWrap" name="videoqf_feedback_text_to_speech[]" id="videoqf_feedback_text_to_speech_'+videoI+'"></textarea>\
			</div>\
			<div class="submitRight1">\
				<button type="button" class="btn1 submitbtn1 vts_modal_save" data-modal-id="videofModal'+videoI+'" data-assets="videoqf_assets_'+videoI+'" data-input-id="videoqf_feedback_text_to_speech_'+videoI+'" data-assets-view="videoqf_tts_container_'+videoI+'">Insert</button>\
				<button type="button" class="btn1 submitbtn1 vts_modal_cls" data-modal-id="videofModal'+videoI+'" data-assets="videoqf_assets_'+videoI+'" data-input-id="videoqf_feedback_text_to_speech_'+videoI+'" data-assets-view="videoqf_tts_container_'+videoI+'">Clear</button>\
				<button type="button" class="btn1 submitbtn1 vts_modal_close" data-modal-id="videofModal'+videoI+'" data-assets="videoqf_assets_'+videoI+'" data-input-id="videoqf_feedback_text_to_speech_'+videoI+'" data-assets-view="videoqf_tts_container_'+videoI+'">Cancel</button>\
			</div>\
		</div>\
		<div class="Ansbox">\
			<div class="row">\
				<div class="col-sm-12">\
					<div class="col-sm-6"><h3 class="AnsHeading">CHOICE</h3></div>\
					<div class="VideoApendOption VideoApendOption_'+videoI+'">\
						<div class="MCQApendOption-box Video-box_'+videoI+'">\
							<div class="MCQRadio">\
								<div class="Radio-box classtmpR">\
									<label class="radiostyle">\
										<input type="radio" name="videoq_true_option['+voption+'][]" value="'+vradio+'" checked="checked" /><span class="radiomark"></span>\
									</label>\
									<div class="col-sm-12 mcwidth">\
										<div class="choicebox">\
											<div class="form-group">\
												<input type="text" name="videoq_option['+voption+'][option][]" id="videoq_option" class="form-control inputtextWrap videoq" placeholder="Option text goes here" required="required" />\
											</div>\
										</div>\
									</div>\
									<div class="AddAns-Option">\
										<img class="img-fluid videoAddoptions" data-video-option="'+videoI+'" data-video-suboption="'+voption+'" data-video-radio="'+vradio+'" src="img/list/add_field.svg">\
									</div>\
								</div>\
								<div class="q-icon-banner clearfix singlemcqline">\
									<div class="Ques-comp">\
										<h5>Add Competency score</h5>\
										<?php if ( ! empty($sim_com['comp_col_1']) && ! empty($sim_com['comp_val_1'])): ?>
										<div class="form-group QusScore">\
											<input type="text" class="form-control score videoq" name="videoq_ans_val1['+voption+'][score][]" id="videoq_ans_val1_'+videoI+'" onkeypress="return isNumberKey(event);" />\
										</div>\
										<?php endif; if ( ! empty($sim_com['comp_col_2']) && ! empty($sim_com['comp_val_2'])): ?>
										<div class="form-group QusScore">\
											<input type="text" class="form-control score videoq" name="videoq_ans_val2['+voption+'][score][]" id="videoq_ans_val2_'+videoI+'" onkeypress="return isNumberKey(event);" />\
										</div>\
										<?php endif; if ( ! empty($sim_com['comp_col_3']) && ! empty($sim_com['comp_val_3'])): ?>
										<div class="form-group QusScore">\
											<input type="text" class="form-control score videoq" name="videoq_ans_val3['+voption+'][score][]" id="videoq_ans_val3_'+videoI+'" onkeypress="return isNumberKey(event);" />\
										</div>\
										<?php endif; if ( ! empty($sim_com['comp_col_4']) && ! empty($sim_com['comp_val_4'])): ?>
										<div class="form-group QusScore">\
											<input type="text" class="form-control score videoq" name="videoq_ans_val4['+voption+'][score][]" id="videoq_ans_val4_'+videoI+'" onkeypress="return isNumberKey(event);" />\
										</div>\
										<?php endif; if ( ! empty($sim_com['comp_col_5']) && ! empty($sim_com['comp_val_5'])): ?>
										<div class="form-group QusScore">\
											<input type="text" class="form-control score videoq" name="videoq_ans_val5['+voption+'][score][]" id="videoq_ans_val5_'+videoI+'" onkeypress="return isNumberKey(event);" />\
										</div>\
										<?php endif; if ( ! empty($sim_com['comp_col_6']) && ! empty($sim_com['comp_val_6'])): ?>
										<div class="form-group QusScore">\
											<input type="text" class="form-control score videoq" name="videoq_ans_val6['+voption+'][score][]" id="videoq_ans_val6_'+videoI+'" onkeypress="return isNumberKey(event);" />\
										</div>\
										<?php endif; ?>
									</div>\
								</div>\
								<div class="classicQ SingleCmcq">\
									<div class="form-group feed">\
										<h5>Feedback</h5>\
										<textarea class="form-control FeedForm inputtextWrap videoq" name="videoq_feedback['+voption+'][]" id="videoq_feedback'+videoI+'" rows="3" placeholder="Feedback text goes here"></textarea>\
										<div class="videoqf_assets_container_'+ voption + videoI +'" style="display:none;">\
											<div class="videoqf_assets_data_'+ voption + videoI +'"><a class="view_assets" data-src="" href="javascript:;"></a></div>\
											<a href="javascript:void(0);" data-assets="" data-assets-id="" data-input-id="" data-path="" class="delete_assets" title="Delete"><i class="fa fa-times" aria-hidden="true"></i></a>\
										</div>\
										<div class="videoqf_tts_container_'+ voption + videoI +'" style="display:none;">\
											<div class="videoqf_tts_data_'+ voption + videoI +'">\
												<div class="tooltip1">\
													<img class="img-fluid" src="img/list/text_speech.svg"><span class="tooltiptext">Text to Speech</span>\
												</div>\
											</div>\
											<a href="javascript:void(0);" class="delete_tts" data-modal-id="videofModal'+ voption + videoI +'" data-assets="videoqf_assets_'+ voption + videoI +'" data-input-id="videoqf_feedback_text_to_speech_'+ voption + videoI +'" title="Delete"><i class="fa fa-times" aria-hidden="true"></i></a>\
										</div>\
										<ul class="QueBoxIcon feedQ">\
											<h5>Add assets</h5>\
											<li class="dropdown">\
												<button class="btn1 btn-secondary dropdown-toggle videoqf_assets_'+ voption + videoI +'" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img class="img-fluid" src="img/list/audio.svg"></button>\
												<div class="dropdown-menu buttom" aria-labelledby="dropdownMenuButton">\
													<a class="dropdown-item">\
														<div class="tooltip1">\
														<img class="img-fluid video_modal" data-modal-id="videofModal'+ voption + videoI +'" data-assets="videoqf_assets_'+ voption + videoI +'" src="img/list/text_speech.svg"><span class="tooltiptext">Text to Speech</span>\
														</div>\
													</a>\
													<a class="dropdown-item">\
														<div class="tooltip1 uploadicon">\
															<label for="file-input-videoqf-feedback-addAudio_'+ voption + videoI +'">\
																<img class="img-fluid" src="img/list/add_audio.svg">\
																<span class="tooltiptext">Add Audio</span>\
															</label>\
															<input id="file-input-videoqf-feedback-addAudio_'+ voption + videoI +'" data-id="#videoqf_feedback_audio_'+ voption + videoI +'" data-assets="videoqf_assets_'+ voption + videoI +'" class="uploadAudioFileVideoTemp" type="file" name="videoqf_feedback_Audio_'+ voption + videoI +'" />\
															<input type="hidden" name="videoqf_feedback_audio['+voption+'][]" id="videoqf_feedback_audio_'+ voption + videoI +'" />\
														</div>\
													</a>\
													<a class="dropdown-item">\
														<div class="tooltip1">\
															<img class="img-fluid rec-audio-videoq" data-input-id="videoqf_feedback_rec_audio_'+ voption + videoI +'" data-assets="videoqf_assets_'+ voption + videoI +'" src="img/list/record_audio.svg">\
															<span class="tooltiptext">Record Audio</span>\
														</div>\
														<input type="hidden" name="videoqf_feedback_rec_audio['+voption+'][]" id="videoqf_feedback_rec_audio_'+ voption + videoI +'" />\
													 </a>\
												</div>\
											</li>\
											<li class="dropdown">\
												<button class="btn1 btn-secondary dropdown-toggle videoqf_assets_'+ voption + videoI +'" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img class="img-fluid" src="img/list/video.svg"></button>\
												<div class="dropdown-menu buttom" aria-labelledby="dropdownMenuButton">\
													<a class="dropdown-item">\
														<div class="tooltip1 uploadicon">\
															<label for="file-input-videoqf-feedback-addVideo_'+ voption + videoI +'">\
																<img class="img-fluid" src="img/list/add_video.svg">\
																<span class="tooltiptext">Add Video</span>\
															</label>\
															<input id="file-input-videoqf-feedback-addVideo_'+ voption + videoI +'" data-id="#videoqf_feedback_video_'+ voption + videoI +'" data-assets="videoqf_assets_'+ voption + videoI +'" class="uploadFileVideoTemp" type="file" name="videoqf_feedback_Video_'+ voption + videoI +'" />\
															<input type="hidden" name="videoqf_feedback_video['+voption+'][]" id="videoqf_feedback_video_'+ voption + videoI +'" />\
														</div>\
													</a>\
													<a class="dropdown-item">\
														<div class="tooltip1">\
															<img class="img-fluid rec-video-videoq" data-input-id="videoqf_feedback_rec_video_'+ voption + videoI +'" data-assets="videoqf_assets_'+ voption + videoI +'" src="img/qus_icon/rec_video.svg">\
															<span class="tooltiptext">Record Video</span>\
														 </div>\
														 <input type="hidden" name="videoqf_feedback_rec_video['+voption+'][]" id="videoqf_feedback_rec_video_'+ voption + videoI +'" />\
													</a>\
													<a class="dropdown-item">\
														<div class="tooltip1">\
															<img class="img-fluid rec-screen-videoq" data-input-id="videoqf_feedback_rec_screen_'+ voption + videoI +'" data-assets="videoqf_assets_'+ voption + videoI +'" src="img/list/screen_record.svg">\
															<span class="tooltiptext">Record Screen</span>\
														 </div>\
														 <input type="hidden" name="videoqf_feedback_rec_screen['+voption+'][]" id="videoqf_feedback_rec_screen_'+ voption + videoI +'" />\
													</a>\
												</div>\
											</li>\
											<li>\
												<div class="tooltip uploadicon videoqf_assets_'+ voption + videoI +'">\
													<label for="file-input-videoqf-feedback-addImg_'+ voption + videoI +'">\
														<img class="img-fluid" src="img/list/image.svg">\
														<span class="tooltiptext">Add Image</span>\
													</label>\
													<input id="file-input-videoqf-feedback-addImg_'+ voption + videoI +'" data-id="#videoqf_feedback_img_'+ voption + videoI +'" data-assets="videoqf_assets_'+ voption + videoI +'" class="uploadImgFileVideo videoqf_assets_'+ voption + videoI +'" type="file" name="videoqf_feedback_Img_'+ voption + videoI +'" />\
													<input type="hidden" name="videoqf_feedback_img['+voption+'][]" id="videoqf_feedback_img_'+ voption + videoI +'" />\
												</div>\
											</li>\
											<li>\
												<div class="tooltip uploadicon videoqf_assets_'+ voption + videoI +'">\
													<label for="file-input-videoqf-feedback-addDoc-'+ voption + videoI +'">\
														<img class="img-fluid" src="img/list/add_document.svg" />\
														<span class="tooltiptext">Add Document</span>\
													</label>\
													<input id="file-input-videoqf-feedback-addDoc-'+ voption + videoI +'" data-id="#videoqf_feedback_doc_'+ voption + videoI +'" data-assets="videoqf_assets_'+ voption + videoI +'" class="uploadDocFileVideo videoqf_assets_'+ voption + videoI +'" type="file" name="videoqf_feedback_Doc_'+ voption + videoI +'" />\
													<input type="hidden" name="videoqf_feedback_doc['+voption+'][]" id="videoqf_feedback_doc_'+ voption + videoI +'" />\
												</div>\
											</li>\
										</ul>\
										<div class="form-popup draggable Ques-pop-style modal" id="videoqfctts1">\
											<div class="popheading">Insert Text to Speech</div>\
											<div class="textarea">\
												<textarea type="text" class="form-control1 inputtextWrap" name="videoqf_feedback_text_to_speech['+voption+'][]" id="videoqf_feedback_text_to_speech_'+ voption + videoI +'"></textarea>\
											</div>\
											<div class="modal-footer">\
												<button type="button" class="btn1 submitbtn1" data-dismiss="modal">Insert</button>\
												<button type="button" class="btn1 submitbtn1" onclick="clearData("videoqf_feedback_text_to_speech_'+ voption + videoI +'");">Clear</button>\
												<button type="button" class="btn1 submitbtn1" data-dismiss="modal">Cancel</button>\
											</div>\
										</div>\
									</div>\
								</div>\
							</div>\
						</div>\
					</div>\
				</div>\
			</div>\
		</div>\
	</div>');
		$(".cue_pt_show").append('<div class="cue_points cue_points_'+videoI+'"> '+cue_point+' <a href="javascript:void(0);" data-delete-id="'+videoI+'" title="Delete Cue Point" class="close_cue"><i class="fa fa-times" aria-hidden="true"></i></a></div>');
		videoI++; voption++;
		$('#cue_point').val('');
		$(".draggable").draggable();
	}
	else {
		swal({text: 'Please enter cue point.', buttons: false, icon: "warning", timer: 3000}); }
	}
	else { swal({text: 'You will be able to add only questions.', buttons: false, icon: "warning", timer: 3000}); }
});

//----Delete-Video-Cue-Points-------------//
$('body').on('click', '.close_cue', function() {
	var cur = $(this);
	var id	= cur.data('delete-id');
	swal({
		title: "Are you sure?",
		text: "Delete this cue point.",
		icon: "warning",
		buttons: [true, 'Delete'],
		dangerMode: true, }).then((willDelete) => { if (willDelete) {
			$('.cue_points_' +id).remove();
			$('.Ans_box_' +id).remove();
			videoI--; voption--; totalq--;
		}
	});
});

//---------Video Option Append Div Open--------------//
var videoOptionI = $('.classicQUeSTionbanner').length;
$('body').on('click', '.videoAddoptions', function(){
	var optionId    = $(this).attr('data-video-option');
	var subOptionId = $(this).attr('data-video-suboption');
	var subRadioId  = $(this).attr('data-video-radio');
	var radio 		= parseInt(subRadioId) + 1;
	$(this).attr('data-video-radio', radio);
	videoOptionI++;
	$(".Video-box_"+ optionId +":last").append('<div class="MCQRadio">\
	<div class="Radio-box classtmpR">\
		<label class="radiostyle"><input type="radio" name="videoq_true_option['+subOptionId+'][]" value="'+radio+'"><span class="radiomark"></span></label>\
		<div class="col-sm-12 mcwidth">\
			<div class="choicebox">\
				<div class="form-group">\
					<input type="text" name="videoq_option['+subOptionId+'][option][]" class="form-control inputtextWrap videoq" placeholder="Option text goes here" required="required">\
				</div>\
			</div>\
		</div>\
		<div class="AddAns-Option videoRemove"><img class="img-fluid" src="img/list/delete_field.svg"></div>\
	</div>\
	<div class="q-icon-banner clearfix singlemcqline">\
		<div class="Ques-comp">\
		<h5>Add Competency score</h5>\
		<?php if ( ! empty($sim_com['comp_col_1']) && ! empty($sim_com['comp_val_1'])): ?>
		<div class="form-group QusScore">\
			<input type="text" class="form-control score videoq" name="videoq_ans_val1['+subOptionId+'][score][]" id="videoq_ans_val1_'+subOptionId+'" onkeypress="return isNumberKey(event);" />\
		</div>\
		<?php endif; if ( ! empty($sim_com['comp_col_2']) && ! empty($sim_com['comp_val_2'])): ?>
		<div class="form-group QusScore">\
			<input type="text" class="form-control score videoq" name="videoq_ans_val2['+subOptionId+'][score][]" id="videoq_ans_val2_'+subOptionId+'" onkeypress="return isNumberKey(event);" />\
		</div>\
		<?php endif; if ( ! empty($sim_com['comp_col_3']) && ! empty($sim_com['comp_val_3'])): ?>
		<div class="form-group QusScore">\
			<input type="text" class="form-control score videoq" name="videoq_ans_val3['+subOptionId+'][score][]" id="videoq_ans_val3_'+subOptionId+'" onkeypress="return isNumberKey(event);" />\
		</div>\
		<?php endif; if ( ! empty($sim_com['comp_col_4']) && ! empty($sim_com['comp_val_4'])): ?>
		<div class="form-group QusScore">\
			<input type="text" class="form-control score videoq" name="videoq_ans_val4['+subOptionId+'][score][]" id="videoq_ans_val4_'+subOptionId+'" onkeypress="return isNumberKey(event);" />\
		</div>\
		<?php endif; if ( ! empty($sim_com['comp_col_5']) && ! empty($sim_com['comp_val_5'])): ?>
		<div class="form-group QusScore">\
			<input type="text" class="form-control score videoq" name="videoq_ans_val5['+subOptionId+'][score][]" id="videoq_ans_val5_'+subOptionId+'" onkeypress="return isNumberKey(event);" />\
		</div>\
		<?php endif; if ( ! empty($sim_com['comp_col_6']) && ! empty($sim_com['comp_val_6'])): ?>
		<div class="form-group QusScore">\
			<input type="text" class="form-control score videoq" name="videoq_ans_val6['+subOptionId+'][score][]" id="videoq_ans_val6_'+subOptionId+'" onkeypress="return isNumberKey(event);" />\
		</div>\
		<?php endif; ?>
		</div>\
	</div>\
	<div class="classicQ SingleCmcq">\
		<div class="form-group feed">\
			<h5>Feedback</h5>\
			<textarea class="form-control FeedForm inputtextWrap videoq" name="videoq_feedback['+subOptionId+'][]" rows="3" placeholder="Feedback text goes here"></textarea>\
			<div class="videoqf_assets_container_'+ subOptionId + videoOptionI +'" style="display:none;">\
				<div class="videoqf_assets_data_'+ subOptionId + videoOptionI +'"><a class="view_assets" data-src="" href="javascript:;"></a></div>\
				<a href="javascript:void(0);" data-assets="" data-assets-id="" data-input-id="" data-path="" class="delete_assets" title="Delete"><i class="fa fa-times" aria-hidden="true"></i></a>\
			</div>\
			<div class="videoqf_tts_container_'+ subOptionId + videoOptionI +'" style="display:none;">\
				<div class="videoqf_tts_data_'+ subOptionId + videoOptionI +'">\
					<div class="tooltip1">\
						<img class="img-fluid" src="img/list/text_speech.svg"><span class="tooltiptext">Text to Speech</span>\
					</div>\
				</div>\
				<a href="javascript:void(0);" class="delete_tts" data-modal-id="videofModal'+ subOptionId + videoOptionI +'" data-assets="videoqf_assets_'+ subOptionId + videoOptionI +'" data-input-id="videoqf_feedback_text_to_speech_'+ subOptionId + videoOptionI +'" title="Delete"><i class="fa fa-times" aria-hidden="true"></i></a>\
			</div>\
			<ul class="QueBoxIcon feedQ">\
				<h5>Add assets</h5>\
				<li class="dropdown">\
					<button class="btn1 btn-secondary dropdown-toggle videoqf_assets_'+ subOptionId + videoOptionI +'" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img class="img-fluid" src="img/list/audio.svg"></button>\
					<div class="dropdown-menu buttom" aria-labelledby="dropdownMenuButton">\
						<a class="dropdown-item">\
							<div class="tooltip1">\
							<img class="img-fluid video_modal" data-modal-id="videofModal'+ subOptionId + videoOptionI +'" data-assets="videoqf_assets_'+ subOptionId + videoOptionI +'" src="img/list/text_speech.svg"><span class="tooltiptext">Text to Speech</span>\
							</div>\
						</a>\
						<a class="dropdown-item">\
							<div class="tooltip1 uploadicon">\
								<label for="file-input-videoqf-feedback-addAudio_'+ subOptionId + videoOptionI +'">\
									<img class="img-fluid" src="img/list/add_audio.svg">\
									<span class="tooltiptext">Add Audio</span>\
								</label>\
								<input id="file-input-videoqf-feedback-addAudio_'+ subOptionId + videoOptionI +'" data-id="#videoqf_feedback_audio_'+ subOptionId + videoOptionI +'" data-assets="videoqf_assets_'+ subOptionId + videoOptionI +'" class="uploadAudioFileVideoTemp" type="file" name="videoqf_feedback_Audio_'+ subOptionId + videoOptionI +'" />\
								<input type="hidden" name="videoqf_feedback_audio['+subOptionId+'][]" id="videoqf_feedback_audio_'+ subOptionId + videoOptionI +'" />\
							</div>\
						</a>\
						<a class="dropdown-item">\
							<div class="tooltip1">\
								<img class="img-fluid rec-audio-videoq" data-input-id="videoqf_feedback_rec_audio_'+ subOptionId + videoOptionI +'" data-assets="videoqf_assets_'+ subOptionId + videoOptionI +'" src="img/list/record_audio.svg">\
								<span class="tooltiptext">Record Audio</span>\
							</div>\
							<input type="hidden" name="videoqf_feedback_rec_audio['+subOptionId+'][]" id="videoqf_feedback_rec_audio_'+ subOptionId + videoOptionI +'" />\
						 </a>\
					</div>\
				</li>\
				<li class="dropdown">\
					<button class="btn1 btn-secondary dropdown-toggle videoqf_assets_'+ subOptionId + videoOptionI +'" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img class="img-fluid" src="img/list/video.svg"></button>\
					<div class="dropdown-menu buttom" aria-labelledby="dropdownMenuButton">\
						<a class="dropdown-item">\
							<div class="tooltip1 uploadicon">\
								<label for="file-input-videoqf-feedback-addVideo_'+ subOptionId + videoOptionI +'">\
									<img class="img-fluid" src="img/list/add_video.svg">\
									<span class="tooltiptext">Add Video</span>\
								</label>\
								<input id="file-input-videoqf-feedback-addVideo_'+ subOptionId + videoOptionI +'" data-id="#videoqf_feedback_video_'+ subOptionId + videoOptionI +'" data-assets="videoqf_assets_'+ subOptionId + videoOptionI +'" class="uploadFileVideoTemp" type="file" name="videoqf_feedback_Video_'+ subOptionId + videoOptionI +'" />\
								<input type="hidden" name="videoqf_feedback_video['+subOptionId+'][]" id="videoqf_feedback_video_'+ subOptionId + videoOptionI +'" />\
							</div>\
						</a>\
						<a class="dropdown-item">\
							<div class="tooltip1">\
								<img class="img-fluid rec-video-videoq" data-input-id="videoqf_feedback_rec_video_'+ subOptionId + videoOptionI +'" data-assets="videoqf_assets_'+ subOptionId + videoOptionI +'" src="img/qus_icon/rec_video.svg">\
								<span class="tooltiptext">Record Video</span>\
							 </div>\
							 <input type="hidden" name="videoqf_feedback_rec_video['+subOptionId+'][]" id="videoqf_feedback_rec_video_'+ subOptionId + videoOptionI +'" />\
						</a>\
						<a class="dropdown-item">\
							<div class="tooltip1">\
								<img class="img-fluid rec-screen-videoq" data-input-id="videoqf_feedback_rec_screen_'+ subOptionId + videoOptionI +'" data-assets="videoqf_assets_'+ subOptionId + videoOptionI +'" src="img/list/screen_record.svg">\
								<span class="tooltiptext">Record Screen</span>\
							 </div>\
							 <input type="hidden" name="videoqf_feedback_rec_screen['+subOptionId+'][]" id="videoqf_feedback_rec_screen_'+ subOptionId + videoOptionI +'" />\
						</a>\
					</div>\
				</li>\
				<li>\
					<div class="tooltip uploadicon videoqf_assets_'+ subOptionId + videoOptionI +'">\
						<label for="file-input-videoqf-feedback-addImg_'+ subOptionId + videoOptionI +'">\
							<img class="img-fluid" src="img/list/image.svg">\
							<span class="tooltiptext">Add Image</span>\
						</label>\
						<input id="file-input-videoqf-feedback-addImg_'+ subOptionId + videoOptionI +'" data-id="#videoqf_feedback_img_'+ subOptionId + videoOptionI +'" data-assets="videoqf_assets_'+ subOptionId + videoOptionI +'" class="uploadImgFileVideo videoqf_assets_'+ subOptionId + videoOptionI +'" type="file" name="videoqf_feedback_Img_'+ subOptionId + videoOptionI +'" />\
						<input type="hidden" name="videoqf_feedback_img['+subOptionId+'][]" id="videoqf_feedback_img_'+ subOptionId + videoOptionI +'" />\
					</div>\
				</li>\
				<li>\
					<div class="tooltip uploadicon videoqf_assets_'+ subOptionId + videoOptionI +'">\
						<label for="file-input-videoqf-feedback-addDoc-'+ subOptionId + videoOptionI +'">\
							<img class="img-fluid" src="img/list/add_document.svg" />\
							<span class="tooltiptext">Add Document</span>\
						</label>\
						<input id="file-input-videoqf-feedback-addDoc-'+ subOptionId + videoOptionI +'" data-id="#videoqf_feedback_doc_'+ subOptionId + videoOptionI +'" data-assets="videoqf_assets_'+ subOptionId + videoOptionI +'" class="uploadDocFileVideo videoqf_assets_'+ subOptionId + videoOptionI +'" type="file" name="videoqf_feedback_Doc_'+ subOptionId + videoOptionI +'" />\
						<input type="hidden" name="videoqf_feedback_doc['+subOptionId+'][]" id="videoqf_feedback_doc_'+ subOptionId + videoOptionI +'" />\
					</div>\
				</li>\
			</ul>\
			<div class="form-popup draggable Ques-pop-style modal" id="videoqfctts1">\
				<div class="popheading">Insert Text to Speech</div>\
				<div class="textarea">\
					<textarea type="text" class="form-control1 inputtextWrap" name="videoqf_feedback_text_to_speech['+subOptionId+'][]" id="videoqf_feedback_text_to_speech_'+ subOptionId + videoOptionI +'"></textarea>\
				</div>\
				<div class="modal-footer">\
					<button type="button" class="btn1 submitbtn1" data-dismiss="modal">Insert</button>\
					<button type="button" class="btn1 submitbtn1" onclick="clearData("videoqf_feedback_text_to_speech_'+ subOptionId + videoOptionI +'");">Clear</button>\
					<button type="button" class="btn1 submitbtn1" data-dismiss="modal">Cancel</button>\
				</div>\
			</div>\
		</div>\
	</div></div>');
});

$('body').on('click', '.AddAns-Option.videoRemove', function(e){
	e.preventDefault();
	$(this).parent().parent().remove();
	videoOptionI--;
	$('.videoAddoptions').removeClass('disabled');
});

//--------------Delete-Options------------
$('body').on('click', '.removeOption', function(e) {
	e.preventDefault();
	var ans_id	= $(this).attr('data-remove-answer-id');
	var curr	= $(this);
	if (ans_id) {
		var dataString = 'delete_classic_answer='+ true +'&ans_id='+ ans_id;
		swal({
			title: "Are you sure?",
			text:  "Delete this Option",
			icon: "warning",
			buttons: [true, 'Delete'],
			dangerMode: true, }).then((willDelete) => { if (willDelete) {
				$.LoadingOverlay("show");
				$.ajax({
					url: "includes/process.php",
					type: "POST",
					data: dataString,
					cache: false,
					success: function(resdata) {
						var res = $.parseJSON(resdata);
						$.LoadingOverlay("hide");
						if (res.success == true) {
							swal(res.msg, { buttons: false, timer: 2000 });
							curr.parent().parent().remove();
						}
						else if (res.success == false) {
							swal(res.msg, { buttons: false, timer: 2000 });
						}
					},error: function() {
						$.LoadingOverlay("hide");
						swal("Error", 'Oops, something went wrong. Please try again later.?', "error");
					}
				});
			}
		});
	}
});

var x = <?php echo ( ! empty($i)) ? $i : 1; ?>;
var maxField = 6;
$(".plus_comp").click(function() {
	if (x < maxField) {
		x++;
		$(".plus_comp_option").append('<div class="add_comp_option linaer">\
		<div class="form-group"><input type="text" class="form-control control1" placeholder="Competency Name" name="competency_name_'+x+'" autocomplete="off" required="required"></div>\
		<div class="form-group labelbox"><label>Competency Score</label><input type="text" class="form-control question-box" name="competency_score_'+x+'" onkeypress="return isNumberKey(event);" autocomplete="off" required="required"></div>\
		<div class="form-group labelbox"><label>Weightage</label><input type="text" class="form-control question-box" name="weightage_'+x+'" onkeypress="return isNumberKey(event);" autocomplete="off" required="required"></div><div class="remove minusComp"><img src="img/icons/minus.png" title="Remove"></div></div>');
		var getMax = $('.add_comp_option').length;
		if (getMax >= maxField) {
			$('.plus_comp').addClass('disabled');
		}
		else { $('.plus_comp').removeClass('disabled'); }
	}
	else {
		$('.plus_comp').addClass('disabled');
	}
});

$('.plus_comp_option').on('click', '.remove', function(e) {
	e.preventDefault();
	$(this).parent().remove();
	x--;
	$('.plus_comp').removeClass('disabled');
});
	
$("#menu-toggle").click(function(e) {
	e.preventDefault();
	$("#menu-toggle").toggleClass("changebtn");
	$("#wrapper").toggleClass("toggled");
	$(".match-Q.tab-pane.fade").toggleClass("tabpadding150");
	$(".widthQ100").toggleClass("TogglewidthQ100");		
	$(".modal.Ques-pop-style.in ").toggleClass("TogglewidthQ100");		
});

$(".closeleftmenuA").click(function(e) {
	e.preventDefault();
	$("#list-toggle").addClass("changebtnleft");
	$("#wrapper").addClass("toggledlist");
	$(".match-Q.tab-pane.fade").addClass("tabpadding150");
	$(".Q-left").addClass("left150");
	$(".widthQ100").addClass("TogglelistwidthQ100");
	$(".main_sim_scroll").addClass("TogglelistwidthQ100");	
	$(".modal.Ques-pop-style.in ").addClass("TogglelistwidthQ100");
    $(".list-icon").addClass("Labsolute");			
});

$("#list-toggle").click(function(e) {
	e.preventDefault();
	$("#list-toggle").removeClass("changebtnleft");
	$("#wrapper").removeClass("toggledlist");
	$(".match-Q.tab-pane.fade").removeClass("tabpadding150");
	$(".Q-left").removeClass("left150");
	$(".widthQ100").removeClass("TogglelistwidthQ100");	
	$(".main_sim_scroll").removeClass("TogglelistwidthQ100");		
	$(".modal.Ques-pop-style.in ").removeClass("TogglelistwidthQ100");
    $(".list-icon").removeClass("Labsolute");			
});

$('#upload').click(function() {
	$(this).attr('disabled', 'disabled').html('<i class="fa fa-refresh fa-spin fa-fw"></i><span class="sr-only">Loading...</span> Please wait....');
	var file_data = $('#file').prop('files')[0];
	var form_data = new FormData();
	form_data.append('file', file_data);
	form_data.append('scenario_media_file', true);
	var ftype   = file_data.type;
	var gettype = $('#scenario_media_file_type').val();
	var ext 	= ftype.split('/')[0];
	if (ext == gettype) {
		$.ajax({
			url: "includes/upload.php",
			type: "POST",
			data: form_data,
			contentType: false,
			cache: false,
			processData:false,
			success: function(resdata) {
				var res = $.parseJSON(resdata);
				if (res.success == true) {
					if (res.zip == true) {
						$('#ImgDelete').show();
						$('.delete_wb_file').attr('data-wb-file', res.file_name);
						$('#scenario_media_file').val(res.file_name);
						$('#upload').html('Upload').removeAttr('disabled', 'disabled');
					}
					else {
						var viewHtml = '<a href="<?php echo $uploadpath ?>'+res.file_name+'" target="_blank" title="View Scenario Media File"><i class="fa fa-eye" aria-hidden="true"></i></a>';
						$('#splashImg').show().html(viewHtml);
						$('#ImgDelete').show();
						$('.delete_scenario_media_file').attr('data-scenario-media-file', res.file_name);
						$('#scenario_media_file').val(res.file_name);
						$('#upload').html('Upload').removeAttr('disabled', 'disabled');
					}
					swal(res.msg, { buttons: false, timer: 1000 });
				}
				else if (res.success == false) {
					$('#scenario_media_file').val('');
					swal("Error", res.msg, "error");
					$('#splashImg').hide('slow');
					$('#upload').html('Upload').removeAttr('disabled', 'disabled');
				}
			},error: function() {
				$('#splashImg').hide('slow');
				swal("Error", 'Oops. something went wrong please try again.?', "error");
			}
		});
	} else {
		$('#scenario_media_file,#scenario_media').val('');
		swal("Warning", 'Please select valid file. accept only '+gettype+' file', "warning");
		$('#upload').html('Upload').removeAttr('disabled', 'disabled');
	}
});

$('.delete_scenario_media_file').click(function() {
	var file_name   = $(this).attr("data-scenario-media-file");
	var dataString	= 'delete='+ true + '&scenario_media_file='+file_name;
	if (file_name) {
		swal({
			title: "Are you sure?",
			text: "Delete this file.",
			icon: "warning",
			buttons: [true, 'Delete'],
			dangerMode: true, }).then((willDelete) => { if (willDelete) {
				$.LoadingOverlay("show");
				$.ajax({
					url: "includes/delete-file.php",
					type: "POST",
					data: dataString,
					cache: false,
					success: function(resdata) {
						$.LoadingOverlay("hide");
						var res = $.parseJSON(resdata);
						if (res.success == false) {
							swal("Error", res.msg, "error");
							$('#splashImg').hide('slow');
						}
						else if (res.success == true) {
							$('#splashImg, #ImgDelete').hide('slow');
							$('#scenario_media_file,#scenario_media').val('');
							swal(res.msg, { buttons: false, timer: 1000 });
						}
					},error: function() {
						swal("Error", 'Oops, something went wrong. Please try again later.?', "error");
					}
				});
			} else { swal("Your file is safe.!", { buttons: false, timer: 1000, }); }
		});
	}
});

/* Upload Web Object */
$('#upload_web').click(function() {
	$(this).attr('disabled', 'disabled').html('<i class="fa fa-refresh fa-spin fa-fw"></i><span class="sr-only">Loading...</span> Please wait....');
	var file_data = $('#web_file').prop('files')[0];
	var form_data = new FormData();
	form_data.append('file', file_data);
	form_data.append('web_object_file', true);
	var ftype = file_data.type;
	var type  = $('#web_object_type').val();
	var ext   = ftype.split('/')[0];
	if (ext == type) {
		$.ajax({
			url: "includes/upload.php",
			type: "POST",
			data: form_data,
			contentType: false,
			cache: false,
			processData: false,
			success: function(resdata) {
				var res = $.parseJSON(resdata);
				if (res.success == true) {
					$('#WbDelete').show();
					$('.delete_wb').attr('data-wb', res.file_name);
					$('#web_object').val(res.file_name);
					$('#upload_web').html('Upload').removeAttr('disabled', 'disabled');
					swal(res.msg, { buttons: false, timer: 1000, });
				}
				else if (res.success == false) {
					swal("Error", res.msg, "error");
					$('#web_object').val('');
					$('#WbDelete').hide('slow');
					$('#upload_web').html('Upload').removeAttr('disabled', 'disabled');
				}
			},error: function() {
				$('#WbDelete').hide('slow');
				swal("Error", 'Oops. something went wrong please try again.?', "error");
			}
		});
	} else { 
		swal("Warning", 'Please select valid file. accept only zip file', "warning");
		$('#upload_web').html('Upload').removeAttr('disabled', 'disabled');
	}
});

/* Delete Web Object */
$('.delete_wb').click(function() {
	var file_name   = $(this).attr("data-wb");
	var dataString	= 'delete='+ true + '&wb_file='+ file_name;
	if (file_name) {
		swal({
			title: "Are you sure?",
			text: "Delete Web Object file.",
			icon: "warning",
			buttons: [true, 'Delete'],
			dangerMode: true, }).then((willDelete) => { if (willDelete) {
				$.LoadingOverlay("show");
				$.ajax({
					url: "includes/delete-file.php",
					type: "POST",
					data: dataString,
					cache: false,
					success: function(resdata) {
						$.LoadingOverlay("hide");
						var res = $.parseJSON(resdata);
						if (res.success == false) {
							swal("Error", res.msg, "error");
							$('#WbDelete').hide('slow');
						}
						else if (res.success == true) {
							$('#WbDelete').hide('slow');
							$('#web_object').val('');
							swal(res.msg, { buttons: false, timer: 1000, });
						}
					},error: function() {
						swal("Error", 'Oops, something went wrong. Please try again later.?', "error");
					}
				});
			} else { swal("Your file is safe.!", { buttons: false, timer: 1000, }); }
		});
	}
});

$('#uploadBG').click(function() {
	$(this).attr('disabled', 'disabled').html('<i class="fa fa-refresh fa-spin fa-fw"></i><span class="sr-only">Loading...</span> Please wait....');
	var file_data = $('#bg_file').prop('files')[0];
	var form_data = new FormData();
	form_data.append('file', file_data);
	form_data.append('add_own_background', true);
	var ftype = file_data.type;
	var ext = ftype.split('/')[0];
	if (ext == 'image') {
		$.ajax({
			url: "includes/upload.php",
			type: "POST",
			data: form_data,
			contentType: false,
			cache: false,
			processData:false,
			success: function(resdata) {
				var res = $.parseJSON(resdata);
				if (res.success == true) {
					$('.delete_scenario_bg_file').attr('data-scenario-bg-file', res.file_name).show();
					$('#scenario_bg_file').val(res.file_name);
					$('.own-bg-prev').attr('src', '<?php echo $uploadpath ?>' + res.file_name);
					$('#uploadBG').html('Upload').removeAttr('disabled', 'disabled');
					swal(res.msg, { buttons: false, timer: 1000, });
				}
				else if (res.success == false) {
					$('#scenario_bg_file').val('');
					swal("Error", res.msg, "error");
					$('.own-bg-prev').attr('src', 'img/scenario-img.png');
					$('#uploadBG').html('Upload').removeAttr('disabled', 'disabled');
				}
			},error: function() {
				swal("Error", 'Oops. something went wrong please try again.?', "error");
			}
		});
	} else { 
		swal("Warning", 'Please select valid file. accept only jpeg, jpg, png file', "warning");
		$('#uploadBG').html('Upload').removeAttr('disabled', 'disabled');
	}
});

$('.delete_scenario_bg_file').click(function() {
	var file_name   = $(this).attr("data-scenario-bg-file");
	var dataString	= 'delete='+ true + '&scenario_media_file='+file_name;
	if (file_name) {
		swal({
			title: "Are you sure?",
			text: "Delete this file.",
			icon: "warning",
			buttons: [true, 'Delete'],
			dangerMode: true, }).then((willDelete) => { if (willDelete) {
				$.LoadingOverlay("show");
				$.ajax({
					url: "includes/delete-file.php",
					type: "POST",
					data: dataString,
					cache: false,
					success: function(resdata) {
						$.LoadingOverlay("hide");
						var res = $.parseJSON(resdata);
						if (res.success == false) {
							swal("Error", res.msg, "error");
						}
						else if (res.success == true) {
							$('#scenario_bg_file').val('');
							$('.delete_scenario_bg_file').hide();
							$('.own-bg-prev').attr('src', 'img/scenario-img.png');
							swal(res.msg, { buttons: false, timer: 1000, });
						}
					},error: function() {
						$.LoadingOverlay("hide");
						swal("Error", 'Oops, something went wrong. Please try again later.?', "error");
					}
				});
			} else { swal("Your file is safe.!", { buttons: false, timer: 1000, }); }
		});
	}
});

$('.sim_page_desc').each(function(e){
	CKEDITOR.replace(this.id, { customConfig: "config-max-description.js" });
});

if ($('#sim_page_desc_0').length) {
	CKEDITOR.replace('sim_page_desc_0', { customConfig : "config-max-description.js" });
}

var page = <?php echo ( ! empty($pagei)) ? $pagei++ : 1; ?>;
$(".plus_page").click(function() {
	$(".plus_page_option:last").append('<div class="add_page_option appendSCE">\
	<div class="form-group">\
		<input type="text" name="sim_page_name[]" class="form-control" placeholder="Page name" required="required" />\
	</div>\
	<div class="form-group">\
		<textarea name="sim_page_desc[]" id="sim_page_desc_'+ page +'" class="form-control"></textarea>\
	</div>\
	 <div class="remove minusComp"><img src="img/icons/minus.png" title="Remove Page"></div></div>');
	 var page_des_id = 'sim_page_desc_'+ page;
	 CKEDITOR.replace(page_des_id, {
		customConfig : "config-max-description.js",
	});
	page++;
});

$('.plus_page_option').on('click', '.remove', function(e) {
	e.preventDefault();
	$(this).parent().remove();
	page--;
});

$('input[name="bgoption"]').on('change', function (){
	var bg = $(this).val();
	if (bg == 1) {
		$('#scenario_bg_file').val('');
		$('input[name="sim_background_img"]').attr('checked', false);
	}
	else if (bg == 2) {
		$("#bg_color").spectrum("set", '');
		$('input[name="sim_background_img"]').attr('checked', false);
	}
	else if (bg == 3) {
		$("#bg_color").spectrum("set", '');
		$('#scenario_bg_file').val('');
	}
});

<?php if  ( ! empty($sim_data['bg_color'])) : ?>
$("#bg_color").spectrum("set", '<?php echo $sim_data['bg_color'] ?>');
<?php endif; 
if  ( ! empty($sim_brand['ques_bg'])) : ?>
$("#ques_bg_color").spectrum("set", '<?php echo $sim_brand['ques_bg'] ?>');
<?php endif; 
if  ( ! empty($sim_brand['option_bg'])) : ?>
$("#ques_option_bg_color").spectrum("set", '<?php echo $sim_brand['option_bg'] ?>');
<?php endif; 
if  ( ! empty($sim_brand['option_hover'])) : ?>
$("#ques_option_hover_color").spectrum("set", '<?php echo $sim_brand['option_hover'] ?>');
<?php endif; 
if  ( ! empty($sim_brand['option_selected'])) : ?>
$("#ques_option_select_color").spectrum("set", '<?php echo $sim_brand['option_selected'] ?>');
<?php endif; 
if  ( ! empty($sim_brand['btn_bg'])) : ?>
$("#btn_bg_color").spectrum("set", '<?php echo $sim_brand['btn_bg'] ?>');
<?php endif; 
if  ( ! empty($sim_brand['btn_hover'])) : ?>
$("#btn_hover_color").spectrum("set", '<?php echo $sim_brand['btn_hover'] ?>');
<?php endif; 
if  ( ! empty($sim_brand['btn_selected'])) : ?>
$("#btn_select_color").spectrum("set", '<?php echo $sim_brand['btn_selected'] ?>');
<?php endif; 
if  ( ! empty($sim_brand['font_color'])) : ?>
$("#font_color").spectrum("set", '<?php echo $sim_brand['font_color'] ?>');
<?php endif; 
if  ( ! empty($sim_brand['font_type'])) : ?>
$("#font_type").trigger("setFont", '<?php echo $sim_brand['font_type'] ?>');
<?php endif; ?>
document.onreadystatechange = function() {
	if (document.readyState == "complete"){
		$.LoadingOverlay("hide");
	}
};
</script>
<?php 
require_once 'includes/footer.php';
