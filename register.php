<?php
session_start();
require_once 'config/db.class.php';
require_once 'config/securimage/securimage.php';
$obj    = new DBConnection;
$image  = new Securimage();
$logo   = ( ! empty($obj->siteLogo)) ? $obj->siteLogo : 'EasySIM_logo.svg';
$brand  = $obj->webBranding();
# Check if already login then redirect
if (isset($_SESSION['username']) && isset($_SESSION['role'])) :
  $user   = $_SESSION['username'];
  $role   = $_SESSION['role'];
  $userid = $_SESSION['userId'];
  if (isset($role)) $obj->userRoleRedirect($role);
endif; ?>
<!doctype html>
<!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]><html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--><html class="no-js" lang="en"> <!--<![endif]-->
<head>
  <title>EasySIM: User Registration</title>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="shortcut icon" type="image/x-icon" href="favicon.ico" />
  <!-- Chrome, Firefox OS and Opera -->
  <meta name="theme-color" content="#2c3545">
  <!-- Windows Phone -->
  <meta name="msapplication-navbutton-color" content="#2c3545">
  <!-- iOS Safari -->
  <meta name="apple-mobile-web-app-status-bar-style" content="#2c3545">
  <link rel="stylesheet" type="text/css" href="content/css/bootstrap.css" />
  <link rel="stylesheet" type="text/css" href="content/css/style.css?v=<?php echo time() ?>" />
  <link rel="stylesheet" type="text/css" href="content/css/responsive.css?v=<?php echo time() ?>" />
  <script type="text/javascript" src="content/js/jquery-3.3.1.min.js"></script>
  <script type="text/javascript" src="content/js/bootstrap.3.3.7.min.js"></script>
  <script type="text/javascript" src="content/js/sweetalert.min.js"></script>
  <script type="text/javascript" src="content/js/loadingoverlay.min.js"></script>
</head>
<body>
  <?php echo ( ! empty($brand)) ? $brand : ''; ?>
  <style type="text/css">
    .js-hidden { display: none; }
  </style>
  <header>
    <div class="home_header">
      <ul class="logowhite">
        <a href="<?php echo $obj->getBaseUrl(); ?>">
          <li><img class="white-svg-logo" src="<?php echo $obj->getBaseUrl('img/logo/'. $logo); ?>"></li>
        </a>
        <li><img class="white-svg-logo1" src="<?php echo $obj->getBaseUrl('img/homepage/logo.svg'); ?>"></li>
      </ul>
    </div>
  </header>
  <div class="register-form">
    <div class="col-md-4"></div>
    <div class="registerbanner">
      <div class="col-md-4">
        <div class="logintext">Welcome</div>
        <div class="login_box">
          <form name="regisFrom" class="password-strength" id="regisFrom" method="post">
            <input type="hidden" name="user_regis" value="1" />
            <div class="form-group">
              <div class="input-group"> <span class="loginicions"><img src="img/left_menu/user.svg"></span>
                <input type="text" class="form-control" id="username" name="username" placeholder="Username" autofocus required>
              </div>
            </div>
            <div class="form-group">
              <div class="register_password1">
                <div class="input-group"> <span class="loginicions"><img src="img/left_menu/password.svg"></span>
                  <input class="password-strength__input form-control" type="password" id="password_1" name="password_1" placeholder="Password" required>
                </div>
                <div class="input-group-append">
                  <button class="password-strength__visibility btn btn-outline-secondary" type="button"> <span class="password-strength__visibility-icon" data-visible="hidden"><i class="fa fa-eye-slash"></i></span> <span class="password-strength__visibility-icon js-hidden" data-visible="visible"><i class="fa fa-eye"></i></span></button>
                </div>
              </div>
              <small class="password-strength__error text-danger js-hidden">This symbol is not allowed!</small>
            </div>
            <div class="password-strength__bar-block progress mb-4">
              <div class="password-strength__bar progress-bar bg-danger" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
            </div>
            <div class="form-group">
              <div class="input-group" id="show_hide_password1"> <span class="loginicions"><img src="img/left_menu/password.svg"></span>
                <input type="password" class="password-strength__input form-control" id="password_2" name="password_2" placeholder="Confirm Password" required>
                <div class="input-group-addon loginaddon"><a href="javascript:void(0);"><i class="fa fa-eye-slash" aria-hidden="true"></i></a></div>
              </div>
            </div>
            <div class="form-group">
              <div class="input-group"> <span class="loginicions"><img src="img/left_menu/email.svg"></span>
                <input type="email" class="form-control" id="email" name="email" placeholder="Email" required>
              </div>
            </div>
            <div class="form-group">
            <?php 
              $data = array(
                'input_text'          => '',
                'audio_icon_url'      => 'img/left_menu/capcha_audio.svg',
                'audio_icon_url2'     => 'img/left_menu/capcha_audio_hover.svg',
                'refresh_icon_url'    => 'img/left_menu/capcha_replay.svg',
                'refresh_icon_url2'   => 'img/left_menu/capcha_replay_hover.svg',
                'input_icon_url'      => 'img/left_menu/Capcha.svg',
                'icon_size'           => 32,
                'refresh_alt_text'    => 'Refresh Characters',
                'refresh_title_text'  => 'Refresh Characters',
                'input_attributes'    => array('class' => 'form-control', 'placeholder' => 'Enter Characters Displayed')
              );
              echo Securimage::getCaptchaHtml($data); ?>
            </div>
            <center>
              <div class="form-group">
                <div class="input-group">
                  <button type="submit" id="btnSubmit" class="regsterbtn password-strength__submit btn" disabled="disabled" name="reg_user">REGISTER</button>
                </div>
              </div>
            </center>
            <div class="form-group"> <a href="<?php echo $obj->getBaseUrl('login'); ?>">
                <div class="createacount">Already a member?</div>
              </a> </div>
          </form>
        </div>
      </div>
    </div>
  </div>
  <script src="content/js/pwd-script.js?v=<?php echo time() ?>"></script>
  <script src="content/js/app.js?v=<?php echo time() ?>"></script>
  <script src="content/js/script.js?v=<?php echo time() ?>"></script>
  <script type="text/javascript">
    $(function() {
      $('[data-toggle="tooltip"]').tooltip()
    })
  </script>
