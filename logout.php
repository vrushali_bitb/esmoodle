<?php 
session_start();
unset($_SESSION['username']);
unset($_SESSION['role']);
unset($_SESSION['userId']);

unset($_SESSION['autologin']);
unset($_SESSION['key']);
unset($_SESSION['redirecturl']);
header('location: index');
