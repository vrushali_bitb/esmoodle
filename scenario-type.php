<?php 
session_start();
if(isset($_SESSION['username'])){
    $user	= $_SESSION['username'];
	$role	= $_SESSION['role'];
	$userid	= $_SESSION['userId'];
}
else {
    header('location: index.php');
}
require_once 'includes/db.php';
require_once 'config/db.class.php';
require_once 'includes/formkey.class.php';
require_once 'includes/learners_menu.php';
require_once 'config/pagination.php';
$formKey = new formKey();
$db = new DBConnection();
$type_icon_path	= 'img/type_icon/';

if(isset($_GET['delete']) && ! empty($_GET['type_id'])):
	$q = $db->prepare("DELETE FROM scenario_type WHERE st_id = ?");
	if ($q->execute(array(base64_decode($_GET['type_id'])))) $delete = 'Scenario type delete successfully.';
endif;

$sqlr = "SELECT * FROM scenario_type ORDER BY st_id DESC";
$q = $db->prepare($sqlr); $q->execute();
$rowCount = $q->rowCount();
$count = ($sqlr) ? $rowCount : 0;
$page  = (isset($_REQUEST['page']) && ! empty($_REQUEST['page'])) ? $_REQUEST['page'] : 1;
$show  = 15;
$start = ($page - 1) * $show;
$pagination = getPaginationString($count, $show, $page, 1, basename(__FILE__), '?page=');
$sql = "SELECT * FROM scenario_type ORDER BY st_id DESC LIMIT $start, $show";
$results = $db->prepare($sql);
$msg = (isset($_GET['msg'])) ? $_GET['msg'] : ''; ?>
<div class="bottomheader">
    <div class="container">
        <ul class="breadcrumb">
            <?php echo $db->breadcrumbs(); ?>
         </ul>
     </div>
</div>
<div class="Group_managment">
    <div class="container">
		<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
            <div class="panel panelCaollapse">
            	<?php if ( ! empty($msg) && isset($_GET['add']) == 'true'): ?>
                	<div class="alert alert-success alert-dismissable">Scenario type added successfully.</div>
                <?php elseif ( ! empty($msg) && isset($_GET['exist']) == 'true'): ?>
                	<div class="alert alert-danger alert-dismissable">Scenario type already exists. please try another one.</div>
                <?php endif; ?>
                <?php if ( ! empty($delete)): ?>
                	<div class="alert alert-success alert-dismissable"><?php echo $delete; ?></div>
                <?php endif; ?>
                <div class="panel-heading" role="tab" id="headingOne">
                    <h4 class="panel-title">
                        <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                            Create New Scenario Type
                        </a>
                    </h4>
                </div>				
                <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                    <div class="panel-body">
                    	<form method="post" enctype="multipart/form" action="operations/manage_scenario.php" name="frmscenarioModal">
                        	<?php echo $formKey->outputKey(); ?>
                            <input type="hidden" name="frmscenario" id="frmscenario" value="<?php echo encryptString('modalScenarioType', 'e'); ?>">
                            <div class="usermanage-form">
                            	<div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <input type="text" class="form-control" name="scenario_name" placeholder="Scenario Type Name">
                                        </div>
                                    </div>                                    
                                </div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <input type="file" name="type_icon" id="type_icon" class="form-control file">
                                            <input type="text" class="form-control controls" disabled placeholder="Upload Type Icon">
                                            <input type="hidden" name="type_icon_name" id="type_icon_name" />
                                        </div>
                                    </div>
                                    <div class="col-sm-3 browsebtn">
                                        <button class="browse btn btn-primary" type="button">Browse</button>
                                    </div>
                                    <div class="col-sm-3 browsebtn">
                                        <span id="splashImg"></span> &nbsp;&nbsp; <span id="ImgDelete" style="display:none">
                                        <a href="javascript:void(0);" data-img-name="" class="delete_type_icon" title="Delete Icon"><i class="fa fa-times" aria-hidden="true"></i></a></span>
                                    </div>
                                </div>  
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <textarea name="scenario_description" id="scenario_description" placeholder="Type Description" class="form-control"></textarea>
                                        </div>
                                    </div>
                                </div>                          
                            </div>
                        	<hr class="linebox">
                        	<div class="usermanage-add clearfix">
                            	<input type="submit" name="create" id="create" class="btn btn-primary pull-right" value="Add">
                        	</div>
                    	</form>
                    </div>
                </div>
            </div>
		</div>
        <div class="usermanage_main">
            <div class="table-box">
                <div class="tableheader clearfix">
                   <div class="tableheader_left">List of Scenario Type</div>
                   <div class="tableheader_right">
                       <div class="Searchbox"><input type="text" class="serchtext"></div>
                       <div class="sorttext"><span class="sort">Sort by</span><img src="img/down_arrow_white.png"></div>
                   </div>
                </div>
            </div>
            <div class="usermanage-form1">
               <div class="table-responsive">
               		<table class="table table-striped">
                    	<thead class="Theader">
                            <tr>
                                <th>#</th>
                                <th>Type Name</th>
                                <th>Icon</th>
                                <th>Description</th>
                                <th>Created Date</th>
                                <th>Edit</th>
                                <th>Delete</th>
                            </tr>
                         </thead>
                         <tbody>
						 <?php if ($results->execute() && $rowCount > 0):
								$i = $start + 1;
								foreach ($results->fetchAll(PDO::FETCH_ASSOC) as $row):
								$type_icon = ( ! empty ($row['type_icon'])) ? $type_icon_path . $row['type_icon'] : ''; ?>
                            <tr>
                                <td><?php echo $i ?></td>
                                <td><?php echo $row['type_name'] ?></td>
                                <td><?php echo ( ! empty($type_icon)) ? '<img src="'.$type_icon.'" class="type_icon">' : '' ?></td>
                                <td><?php echo $row['type_des'] ?></td>
                                <td><?php echo $db->dateFormat($row['type_add_date'], 'd-M-y'); ?></td>
                                <td><a class="edit" title="Edit" data-scenario-type-id="<?php echo md5($row['st_id']) ?>" href="javascript:void(0);"><img src="img/list/edit.svg"></a></td>
                                <td><a href="<?php echo basename(__FILE__) ?>?delete=true&type_id=<?php echo base64_encode($row['st_id']) ?>" title="Delete" onclick="return confirm('Are you sure delete this scenario type.?');"><img src="img/list/delete.svg"></a></td>
                            </tr>
						<?php $i++; endforeach; else: ?>
                        <tr><td><strong>No data available in database.</strong></td></tr>
                        <?php endif; ?>
                       </tbody>
                    </table>
                </div>
                <div class="pull-right">
                    <nav aria-label="navigation"><?php echo ( ! empty($pagination) ) ? $pagination : '&nbsp;'; ?></nav>
                </div>
            </div>
         </div>
    </div>
</div>

<div id="load_popup_modal_show" class="modal fade" tabindex="-1"></div>

<script type="text/javascript">
	$('.edit').click(function () {
		var id = $(this).attr('data-scenario-type-id');
		if (id != '') {
			$.LoadingOverlay("show");
			var $modal = $('#load_popup_modal_show');
			$modal.load('edit-scenario-type-modal.php', {'data-id': id }, function(res) {
				if (res != '') {
					$.LoadingOverlay("hide");
					$modal.modal('show');
				}
				else {
					$.LoadingOverlay("hide");
					swal("Error", 'Oops. something went wrong please try again.?', "error");
				}
			});
		}
	});
</script>
<?php 
require_once 'includes/footer.php';
