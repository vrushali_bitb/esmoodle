<?php 
ob_start();
session_start();
if (isset($_SESSION['username'])) {
  $user   = $_SESSION['username'];
  $role   = $_SESSION['role'];
  $userid = $_SESSION['userId'];
  $cid		= (isset($_SESSION['client_id'])) ? $_SESSION['client_id'] : exit('Oops. something went wrong please try again');
} else {
  header('location: index.php');
}

require_once 'config/db.class.php';
require_once 'includes/learners_menu.php';
$db = new DBConnection();

/* page access only administrator */
($db->checkAccountType() != 3) ? header('location: index') : '';
?>
<style>
  body{
    overflow: hidden;
  }
  admin-dashboard.Ev2 .supDASHTOP {
      margin: 0px;
  }
  .cls-1 {
      stroke: transparent !important;
  }
  .flexRI.client.clients {
      display: flex;
  }
  .wordwrap {
    white-space: normal;
}



.owl-nav {
    display: none;
}

.owl-dots {
    display: block;
    margin-top: 15px;
}
.owl-theme .owl-dots .owl-dot.active span, .owl-theme .owl-dots .owl-dot:hover span {
    background: #6aa84f !important;
}

.owl-theme .owl-dots .owl-dot span {
    width: 20px !important;
    height: 20px !important;
    border: 1px solid #000;
    background: #ffffff !important;
}
@media (min-width: 1024px) and (max-width: 1450px){
  .adminT {
    height: calc(100vh - 375px);
    overflow: auto;
}
  .dashboard-box {
      padding: 5px;
  }
  .flexItem.flexItemsdMinDash {
      margin-bottom: 0px;
  }
}
@media only screen and (max-width: 767px){
body {
    overflow:auto;
}
}
</style>
<link rel="stylesheet" href="content/js/daterangepicker/daterangepicker.css" />
<link rel="stylesheet" href="content/css/owlcarousel/owl.carousel.min.css" />
<link rel="stylesheet" href="content/css/owlcarousel/owl.theme.default.css" />
<div class="admin-dashboard Ev2">
  <div class="container-fluid">
    <div class="L-calender">
      <div class="input-group calenderclient">
        <input type="text" id="reportrange" class="form-control" readonly="readonly" />
        <span class="input-group-addon"><span class="fa fa-calendar"></span></span></div>
    </div>
    <div class="flexItem flexItemsdMinDash admiNDASH">
      <div class="flextlistbaner hometab clearfix owl-carousel owl-theme active">
        <!--div class="item">
          <div class="flextlistitem">
            <div class="textFlex CAtextflex">
              <div class="clienttextbox">
                <div class="CTUser">USER</div>
                <div class="CTUserNo"><?php echo $db->getTotalUsers(); ?></div>
                <div class="CTGroupBanneR">
                  <div class="CTGroup">Group</div>
                  <div class="CTGroupNo"><?php echo $db->getTotalGroup(); ?></div>
                </div>
              </div>
              <ul class="flexLI clientflexLI">
                <li>Admin <span><?php echo $db->getTotalUsers('admin'); ?></span></li>
                <li>Author <span><?php echo $db->getTotalUsers('author'); ?></span></li>
                <li>Reviewer <span><?php echo $db->getTotalUsers('reviewer'); ?></span></li>
                <li>Educator <span><?php echo $db->getTotalUsers('educator'); ?></span></li>
                <li>Learner <span><?php echo $db->getTotalUsers('learner'); ?></span></li>
              </ul>
            </div>
          </div>
        </div-->
        <!--div class="item">
          <div class="flextlistitem SIMulationT1">
            <div class="textFlex CAtextflex2">
              <div class="col-sm-6">
                <div class="Tright"><?php echo $db->getTotalCategory(); ?></div>
                <div class="A16F">SIMULATION<br>CATEGORIES</div>
              </div>
              <div class="col-sm-6">
                <div class="Tright"><?php echo $db->getTotalScenario(); ?></div>
                <div class="A16F">SIMULATIONS</div>
              </div>
            </div>
          </div>
        </div-->
       
        <div class="item">
          <div class="flextlistitem newFLBOX">
            <div class="usermanage-form ASBAner">
              <div class="form-group form-select">
                <select class="form-control" name="status">
                  <option selected="selected" hidden="">Select</option>
                  <option value="1">Registered User</option>
                  <option value="0">Active User</option>
                  <option value="0">Archived User</option>
                  <option value="0">Delete User</option>
                </select>
              </div>
            </div>
            <div class="LICE-TotalUser">
                <div class="LEAR48 LEARGreen">40</div>
                <div class="LEAR24">Total Users</div>
            </div>
          </div>
        </div>
        <div class="item">
          <div class="flextlistitem newFLBOX">
            <div class="usermanage-form ASBAner">
              <div class="form-group form-select">
                <select class="form-control" name="status">
                  <option selected="selected" hidden="">Select Role</option>
                  <option value="1">Author</option>
                  <option value="0">Reviewer</option>
                  <option value="0">Educator</option>
                  <option value="0">Learner</option>
                </select>
              </div>
            </div>
            <div class="LICE-TotalUser">
                <div class="LEAR48 LEARGreen">8/10</div>
                <div class="LEAR24">Assigned Authors</div>
            </div>
          </div>
        </div>
        <div class="item">
          <div class="flextlistitem newFLBOX">
            <div class="usermanage-form ASBAner">
              <div class="form-group form-select">
                <select class="form-control" name="status">
                  <option selected="selected" hidden="">Select</option>
                  <option value="1">Simulation</option>
                  <option value="0">Educator</option>
                  <option value="0">Group</option>
                  <option value="0">Unit</option>
                  <option value="0">Location</option>
                </select>
              </div>
            </div>
            <div class="LICE-TotalUser adminpass">
                <div><span class="LEAR36 LEARRed">Pass%</span><span class="LEAR36 LEARGreen">RR%</span></div>
                <div class="LEAR24 LEARblue">Simulation name</div>
            </div>
          </div>
        </div>
        <div class="item">
          <div class="flextlistitem">
            <ul class="SimuDET">
              <li>
                <div>Number of Categories</div>                
                <div>Simulations Published</div>                
                <div>Simulations Assigned</div>
                <div>Unassigned Simulations</div>
                <div>Average Time taken/sim</div>                
              </li>
              <li>
                <div>3</div>
                <div>10</div>
                <div>41</div>
                <div>41</div>
                <div>45min</div>
              </li>
            </ul>
          </div>
        </div>
        <div class="item">
          <div class="flextlistitem">
          <div class="LICE-DETAdmin">
                <div class="LEAR24 BolD24">License Renewal in</div>
                <div class="LEAR48 LEARpurple">20</div>
                <div class="LEAR24">Days</div>
            </div>
          </div>
        </div>
        <div class="item">
          <div class="flextlistitem T1">
            <div class="LICE-DETAdmin">
                <div class="LEAR24 BolD24">Space used</div>
                <div class="LEAR48 LEARpurple">15GB/1TB</div>
            </div>
          </div>
        </div>
        <div class="item">
          <div class="flextlistitem">
            <div id="top_learner" style="height: 170px; width: 100%;"></div>
          </div>
        </div>
        <div class="item">
          <div class="flextlistitem T1">
            <div id="lead_group" style="height: 170px; width: 100%;"></div>
          </div>
        </div>
        <div class="item">
          <div class="flextlistitem">
            <div id="trand_sim" style="height: 170px; width: 100%;"></div>
          </div>
        </div>
        <div class="item">
          <div class="flextlistitem T1">
            <div id="top_comp" style="height: 170px; width: 100%;"></div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="tab-content">
    <div id="home" class="tab-pane fade in active">
      <div class="adminT">
        <div class="col-sm-2"> <a href="<?php echo $db->getBaseUrl('user-management.php') ?>">
          <div class="dashboard-box colorBoxW"> <img class="img-responsive width70 svg" src="img/dash_icon/manage_user.svg">
            <div class="dash-sub-title">Manage Users</div>
            <div class="dash-msg clearfix">
              <div class="notino"><img class="img-responsive" src="img/massage.png"> <span class="notifi-icon"><?php echo $db->getTotalNewUsers(); ?></span></div>
            </div>
          </div>
          </a> </div>
        <div class="col-sm-2"> <a href="<?php echo $db->getBaseUrl('group-management.php') ?>">
          <div class="dashboard-box colorBoxW"> <img class="img-responsive width70 svg" src="img/dash_icon/manage_group.svg">
            <div class="dash-sub-title">Manage Groups</div>
            <div class="dash-msg clearfix">
              <div class="notino"><img class="img-responsive" src="img/massage.png"> <span class="notifi-icon"><?php echo $db->getTotalGroup(); ?></span></div>
            </div>
          </div>
          </a> </div>
        <div class="col-sm-2"> <a href="<?php echo $db->getBaseUrl('manage-simulations.php') ?>">
          <div class="dashboard-box colorBoxW"> <img class="img-responsive width70 svg" src="img/dash_icon/manage_sim.svg">
            <div class="dash-sub-title">Manage Simulations</div>
            <div class="dash-msg clearfix">
              <div class="notino"> <img class="img-responsive" src="img/massage.png"> <span class="notifi-icon"><?php echo $db->getDraftScenario(2); ?></span></div>
            </div>
          </div>
          </a> </div>
        <div class="col-sm-2"> <a href="<?php echo $db->getBaseUrl('simulation-assignment.php') ?>">
          <div class="dashboard-box colorBoxW"> <img class="img-responsive width70 svg" src="img/dash_icon/assign_sim.svg">
            <div class="dash-sub-title">Assign Simulations</div>
            <div class="dash-msg clearfix">
              <div class="notino"><img class="img-responsive" src="img/massage.png"> <span class="notifi-icon"><?php echo $db->getAssignScenario(); ?></span></div>
            </div>
          </div>
          </a> </div>
        <div class="col-sm-2"> <a href="<?php echo $db->getBaseUrl('email-management.php') ?>">
          <div class="dashboard-box colorBoxW"> <img class="img-responsive width70 svg" src="img/dash_icon/manage_template.svg">
            <div class="dash-sub-title">Manage Templates</div>
            <div class="dash-msg clearfix">
              <div class="notino"><img class="img-responsive" src="img/massage.png"> <span class="notifi-icon"><?php echo $db->getTotalTemplate(); ?></span></div>
            </div>
          </div>
          </a></div>
        <div class="col-sm-2">
          <a href="<?php echo $db->getBaseUrl('category-management.php') ?>">
          <div class="dashboard-box colorBoxW"> <img class="img-responsive width70 svg" src="img/left_menu/category.svg">
            <div class="dash-sub-title">Manage Category</div>
            <div class="dash-msg clearfix"><div class="notino"><img class="img-responsive" src="img/massage.png"> <span class="notifi-icon"><?php echo $db->getTotalCategory(); ?></span></div></div>
          </div>
          </a>
        </div>
        <?php if ( ! empty($mhome) && ! empty($clientData['mhome'])): ?>
        <div class="col-sm-2">
          <a href="<?php echo $db->getBaseUrl('home-page-management.php') ?>">
          <div class="dashboard-box colorBoxW"> <img class="img-responsive width70 svg" src="img/dash_icon/homepage.svg">
            <div class="dash-sub-title">Manage Home Page</div>
            <div class="dash-msg clearfix"><div class="notino"><img class="img-responsive" src="img/massage.png"> <span class="notifi-icon">1</span> </div></div>
          </div>
          </a>
        </div>
        <?php endif; ?>
        <?php if ( ! empty($mbrand) && ! empty($clientData['mbrand'])): ?>
        <div class="col-sm-2">
          <a href="<?php echo $db->getBaseUrl('manage-branding.php') ?>">
            <div class="dashboard-box colorBoxW"> <img class="img-responsive width70 svg" src="img/dash_icon/branding.svg">
              <div class="dash-sub-title">Manage Branding</div>
              <div class="dash-msg clearfix"><div class="notino"><img class="img-responsive" src="img/massage.png"><span class="notifi-icon">1</span></div></div>
            </div>
          </a>
        </div>
        <?php endif; ?>
        <div class="col-sm-2">
          <a href="<?php echo $db->getBaseUrl('admin-report.php') ?>">
          <div class="dashboard-box colorBoxW"> <img class="img-responsive width70 svg" src="img/dash_icon/view_report1.svg">
            <div class="dash-sub-title">View Report</div>
            <div class="dash-msg clearfix">
              <div class="notino"><img class="img-responsive" src="img/massage.png"><span class="notifi-icon">1</span></div>
            </div>
          </div>
          </a>
        </div>
      </div>
    </div>
  </div>
</div>
<script src="content/js/daterangepicker/moment.min.js"></script>
<script src="content/js/daterangepicker/daterangepicker.js"></script>
<script src="content/js/canvasjs.min.js"></script>
<script src="content/js/owlcarousel/owl.carousel.min.js"></script>
<script>
$(document).ready(function() {
  $('.owl-carousel').owlCarousel({
		loop:false,
		margin:5,
		nav:true,
		mouseDrag:true,
		responsive:{
			0:{
				items:1
			},
			700:{
				items:3
			},
			1200:{
				items:6
			}
		}
	})
  $.LoadingOverlay("show");
  
  var start = moment().subtract(29, 'days');
  var end   = moment();
  
  function cb(start, end) {
    $('#reportrange span').html(start.format('YYYY-MM-DD') + ' - ' + end.format('YYYY-MM-DD'));
  }
  
  $('#reportrange').daterangepicker({
    startDate: start,
    endDate: end,
    locale: { format: 'YYYY-MM-DD' },
    ranges: {
      'Today': [moment(), moment()],
      'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
      'Last 7 Days': [moment().subtract(6, 'days'), moment()],
      'Last 30 Days': [moment().subtract(29, 'days'), moment()],
      'This Month': [moment().startOf('month'), moment().endOf('month')],
      'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
    } }, cb);
  cb(start, end);
  
  function defaultGraphData(date){
    $.ajax({
      type: "POST",
      url: 'includes/client-admin-dash-grid-data.php',
      data: 'getClientAdminDashData='+ true + '&date='+ date,
      cache: false,
      success: function (resdata) {
        $.LoadingOverlay("hide");
        var res = $.parseJSON(resdata);
        if (res.success == true) {
          CanvasJS.addColorSet("colorcode", ["#6d3073", "#3b602d", "#c07744", "#678087", "#678087"]);
          /* TRENDING SIM */
          if (res.data.sim.length) {
            var trendingSimChart = new CanvasJS.Chart("trand_sim", {
                colorSet: "colorcode",
                title: { text: "Trending Simulations"},
                animationEnabled: true,
                theme: "light2",
                backgroundColor: '#fff',
                axisX: { gridThickness: 0, tickLength: 0, lineThickness: 0, labelFontColor: "black", labelFormatter: function() { return " "; } },
                axisY: { gridThickness: 0, tickLength: 0, lineThickness: 0, labelFormatter: function() { return " "; } },
                toolTip: { content: "<div class='wordwrap'>{label}: {y}</div>" },
                data: [{
                  type: "area",
                  indexLabel: "{y}",
                  indexLabelPlacement: "inside",
                  indexLabelFontWeight: "bolder",
                  indexLabelFontColor: "black",
                  dataPoints: res.data.sim
                }]
              });
              trendingSimChart.render();
          }
          /* TOP LEARNER */
          if (res.data.learner.length) {
            var learnerChart = new CanvasJS.Chart("top_learner", {
                colorSet: "colorcode",
                title: { text: "Top 5 Learners"},
                animationEnabled: true,
                theme: "light2",
                backgroundColor: '#fff',
                axisX: { gridThickness: 0, tickLength: 0, lineThickness: 0, labelFontColor: "black", labelFormatter: function() { return " "; } },
                axisY: { gridThickness: 0, tickLength: 0, lineThickness: 0, labelFormatter: function() { return " "; } },
                toolTip: { content: "<div class='wordwrap'>{label}: {s}: {y} / {p}</div>" },
                data: [{
                  type: "column",
                  indexLabel: "{y}",
                  indexLabelPlacement: "inside",
                  indexLabelFontWeight: "bolder",
                  indexLabelFontColor: "white",
                  dataPoints: res.data.learner
                }]
              });
              learnerChart.render();
          }
          /* LEADING GROUPS */
          if (res.data.group.length) {
            var groupChart = new CanvasJS.Chart("lead_group", {
                colorSet: "colorcode",
                title: { text: "Leading Groups"},
                animationEnabled: true,
                theme: "light2",
                backgroundColor: '#fff',
                axisX: { gridThickness: 0, tickLength: 0, lineThickness: 0, labelFontColor: "black", labelFormatter: function() { return " "; } },
                axisY: { gridThickness: 0, tickLength: 0, lineThickness: 0, labelFormatter: function() { return " "; } },
                toolTip: { content: "<div class='wordwrap'>{name}: {y}</div>" },
                data: [{
                  type: "line",
                  indexLabel: "{y}",
                  indexLabelPlacement: "inside",
                  indexLabelFontWeight: "bolder",
                  indexLabelFontColor: "black",
                  dataPoints: res.data.group
                }]
              });
              groupChart.render();
          }
          /* TOP COMPETENCE */
          if (res.data.competency.length) {
            var topCompChart = new CanvasJS.Chart("top_comp", {
                colorSet: "colorcode",
                title: { text: "Top Competence"},
                animationEnabled: true,
                theme: "light2",
                backgroundColor: '#fff',
                axisX: { gridThickness: 0, tickLength: 0, lineThickness: 0, labelFontColor: "black", labelFormatter: function() { return " "; } },
                axisY: { gridThickness: 0, tickLength: 0, lineThickness: 0, labelFormatter: function() { return " "; } },
                toolTip: { content: "<div class='wordwrap'>{name}: {y}</div>" },
                data: [{
                  type: "doughnut",
                  indexLabel: "{y}",
                  indexLabelPlacement: "inside",
                  indexLabelFontWeight: "bolder",
                  indexLabelFontColor: "white",
                  dataPoints: res.data.competency
                }]
              });
              topCompChart.render();
          }
        }
      },error: function() {
        $.LoadingOverlay("hide");
        swal({text: 'Oops. something went wrong please try again.', buttons: false, icon: "error", timer: 2000});
      }
    });
  }
  
  var date = $('#reportrange').val();

  defaultGraphData(date);
  
  $('#reportrange').on('change', function() {
	  var date = $(this).val();
	  $.LoadingOverlay("show");
	  defaultGraphData(date);
  });
});
</script>
<?php 
require_once 'includes/footer.php';
