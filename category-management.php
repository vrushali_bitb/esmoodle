<?php 
session_start();
if (isset($_SESSION['username'])) {
	$user	= $_SESSION['username'];
	$role	= $_SESSION['role'];
	$userid	= $_SESSION['userId'];
}
else {
    header('location: index.php');
}
require_once 'config/db.class.php';
require_once 'includes/learners_menu.php';
require_once 'config/pagination.php';
$db  = new DBConnection();
$sqlr = "SELECT cat_id FROM category_tbl ORDER BY cat_id DESC";
$q = $db->prepare($sqlr); $q->execute();
$rowCount = $q->rowCount();
$count = ($sqlr) ? $rowCount : 0;
$page  = (isset($_REQUEST['page']) && ! empty($_REQUEST['page'])) ? $_REQUEST['page'] : 1;
$show  = 15;
$start = ($page - 1) * $show;
$pagination = getPaginationString($count, $show, $page, 1, basename(__FILE__), '?page=');
$sql = "SELECT * FROM category_tbl ORDER BY cat_id DESC LIMIT $start, $show";
$results = $db->prepare($sql); ?>
<div class="bottomheader">
    <div class="container-fluid">
        <ul class="breadcrumb">
			<?php echo $db->breadcrumbs(); ?>
         </ul>
     </div>
</div>
<div class="Group_managment">
    <div class="container-fluid">
		<div class="row">
			<div class="col-sm-12">
				<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
					<div class="panel panelCaollapse">
						<div class="panel-heading" role="tab" id="headingOne">
							<h4 class="panel-title">
								<a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
									Create New Category
								</a>
							</h4>
						</div>				
						<div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
							<div class="panel-body">
								<form method="post" id="NewCategory_form" name="NewCategory_form" enctype="multipart/form-data">
									<div class="usermanage-form">
										<div class="row">
											<div class="col-sm-6">
												<div class="form-group">
													<input type="text" class="form-control" name="category" id="category" placeholder="New Category">
												</div>
											</div>                                    
										</div>                            
									</div>
									<hr class="linebox">
									<div class="usermanage-add clearfix">
										<input type="hidden" name="create_category" value="1" />
										<input type="submit" name="create" id="create" class="btn btn-primary pull-right PullRcatagry" value="Add Category">
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
				<div class="usermanage_main">
					<div class="table-box">
						<div class="tableheader clearfix">
						   <div class="tableheader_left">List of Category</div>
						   <!--<div class="tableheader_right">
							   <div class="Searchbox"><input type="text" class="serchtext"></div>
							   <div class="sorttext"><span class="sort">Sort by</span><img src="img/down_arrow_white.png"></div>
						   </div>-->
						</div>
					</div>
					<div class="usermanage-form1">
					   <div class="table-responsive">
							<table class="table table-striped">
								<thead class="Theader">
									<tr>
										<th>#</th>
										<th>Category</th>
										<th>Created Date</th>
										<th>Edit</th>
										<th>Delete</th>
									</tr>
								 </thead>
								 <tbody>
								 <?php if ($results->execute() && $rowCount > 0):
										$i = $start + 1;
										foreach ($results->fetchAll(PDO::FETCH_ASSOC) as $row): ?>
									<tr>
										<td><?php echo $i ?></td>
										<td><?php echo $row['category'] ?></td>
										<td><?php echo $db->dateFormat($row['datetime'], 'd-M-y'); ?></td>
										<td><a class="update_cat" data-cat-id="<?php echo $row['cat_id'] ?>" href="javascript:void(0);" title="Update Category"><img class="svg" src="img/list/edit.svg"></a></td>
										<td><a class="delete_cat" data-cat-id="<?php echo $row['cat_id'] ?>" href="javascript:void(0);" title="Delete Category"><img class="svg" src="img/list/delete.svg"></a></td>
									</tr>
								<?php $i++; endforeach; else: ?>
								<tr><td><strong>No data available in database.</strong></td></tr>
								<?php endif; ?>
							   </tbody>
							</table>
						</div>
						<div class="pull-right">
							<nav aria-label="navigation"><?php echo ( ! empty($pagination) ) ? $pagination : '&nbsp;'; ?></nav>
						</div>
					</div>
				 </div>
			</div>
		</div>
    </div>
</div>

<div id="load_popup_modal_show" class="modal fade" tabindex="-1"></div>

<script type="text/javascript">
	$('.update_cat').click(function () {
		var id = $(this).attr('data-cat-id');
		if (id != '') {
			$.LoadingOverlay("show");
			var $modal = $('#load_popup_modal_show');
			$modal.load('edit-category-modal.php', {'data-id': id }, function(res) {
				if (res != '') {
					$.LoadingOverlay("hide");
					$modal.modal('show');
				}
				else {
					$.LoadingOverlay("hide");
					swal({text: 'Oops, something went wrong. Please try again later', buttons: false, icon: "error", timer: 1000});
				}
			});
		}
	});

	$('.delete_cat').click(function () {
		var id = $(this).attr('data-cat-id');
		if (id != '' && confirm('Are you sure to delete this Category.?') == true) {
			$.LoadingOverlay("show");
			$.ajax({
				url: 'includes/process.php',
				type: "POST",
				data: {'cat_id': id, 'delete_cat': true },
				success: function(result) {
					var res = $.parseJSON(result);
					$.LoadingOverlay("hide");
					if (res.success == true) {
						swal({text: res.msg, buttons: false, icon: "success", timer: 1000});
						setTimeout(function() { window.location.reload(); }, 500);
					}
					else if (res.success == false) {
						swal({text: res.msg, buttons: false, icon: "error", timer: 1000});
						setTimeout(function() { window.location.reload(); }, 500);
					}
				},error: function() {
					swal({text: 'Oops, something went wrong. Please try again later', buttons: false, icon: "error", timer: 1000});
				}
			});
		}
	});
</script>
<?php 
require_once 'includes/footer.php';
