<?php 
session_start();
ob_start();
if (isset($_SESSION['username'])):
	$user 	= $_SESSION['username'];
	$role 	= $_SESSION['role'];
	$userid = $_SESSION['userId'];
	$domain = (isset($_SESSION['domain_name'])) ? $_SESSION['domain_name'] : $_SERVER['HTTP_HOST'];
else:
    header('location: index.php');
endif;
require_once 'config/db.class.php';
require_once 'includes/header.php';
$db 		= new DBConnection();
$sim_id		= (isset($_GET['sim_id'])) ? $_GET['sim_id'] : die('Error');
$simData	= $db->getScenario($sim_id);
$simID 		= $simData['scenario_id'];
$uid 		= $db->UID();
$tAttempt	= $db->getTotalAttempt($simID, $userid);
$attempt	= $db->getAssignSimOtherData($simID, $userid)[0];
if ( ! empty($attempt['no_attempts']) && $tAttempt >= $attempt['no_attempts']):
	echo "<script>alert('Number of attempts of this simulation is completed.');</script>";
	echo "<script>window.location.href='learner-dashboard.php';</script>";
	exit;
endif;
$back_img	= ( ! empty($simData['sim_back_img'])) ? 'img/char_bg/'. $simData['sim_back_img'] : 'img/bg01.jpg';
$path		= $db->getBaseUrl('scenario/upload/'.$domain.'/');
$aPoster	= $db->getBaseUrl('scenario/img/audio-poster.jpg');
$vtheme		= 'forest'; #city, fantasy, forest, sea
$left_char	= '';
if ( ! empty($simData['sim_char_img'])):
	$left_char = 'img/char_bg/'. $simData['sim_char_img'];
elseif ( ! empty($simData['sim_left_char_img'])):
	$left_char = 'img/char_bg/'. $simData['sim_left_char_img'];
elseif ( ! empty($simData['char_own_choice'])):
	$left_char = $path . $simData['char_own_choice'];
endif; ?>
<script type="text/javascript">
	window.addEventListener('beforeunload', function (e) {
	  // Cancel the event
	  e.preventDefault(); // If you prevent default behavior in Mozilla Firefox prompt will always be shown
	  // Chrome requires returnValue to be set
	  e.returnValue = '';
	});	
	function disableF5(e) { if ((e.which || e.keyCode) == 116 || (e.which || e.keyCode) == 82) e.preventDefault(); };
	$(document).ready(function(){
		$(document).on("keydown", disableF5);
	});
	$(document).bind("contextmenu", function(e){
		return false;
	});
	history.pushState(null, null, location.href);
    window.onpopstate = function() {
        history.go(1);
    };
	$.LoadingOverlay("show");
</script>
<style type="text/css">
.linear_classic {
	background-size: cover;
	overflow:hidden;
	height:100vh;
	background: #000;
}
.video-js .vjs-time-control { display:block; }
.video-js .vjs-remaining-time { display:none; }
</style>
<div class="menu-banner-box videmenu-box">
	<div class="time-banner-box">
    	<button class="V_S-tetail" id="view_sim" data-sim-id="<?php echo $sim_id; ?>">
    		<img class="Deskview" src="img/list/view_scenario_b.svg" />
        	<img class="mobview" src="img/list/view_scenario.svg" />
        	View Scenario
    	</button>
  	</div>
    <!---SIM-Timer-->
    <div class="timer-banner">
    	<div class="time-text">
        	<span class="timer-pause" data-minutes-left="<?php echo $simData['duration'] ?>" style="pointer-events: none;"></span>
    	</div>
  	</div>
    <div class="close-banner">
    	<img class="svg" src="img/list/close_learner.svg" />
    </div>
</div>
<script type="text/javascript">
var timer = $('.timer-pause').startTimer({
  onComplete: function(element){
	$('.signbtn1').show();
  },
  allowPause: true
});
</script>
<div class="linear_classic videotempQus">
	<input type="hidden" name="uid" id="uid" value="<?php echo $uid; ?>" />
  	<input type="hidden" name="simId" id="simId" value="<?php echo $simID; ?>" />
  	<input type="hidden" name="userid" id="userid" value="<?php echo $userid; ?>" />
  	<input type="hidden" name="apiurl" id="apiurl" value="<?php echo $db->getBaseUrl('api-process.php') ?>" />
  	<?php 
	$qSql 								= $db->prepare("SELECT * FROM question_tbl WHERE scenario_id = '". $simID ."' ORDER BY qorder = 0, qorder"); $qSql->execute();
	$totalQ 							= $qSql->rowCount();
	if ($totalQ > 0):
		$q 								= 1; 
		$vqtime 						= 0;
		$qdata 							= $qSql->fetchAll(PDO::FETCH_ASSOC);
		foreach ($qdata as $qrow):
			$ansData 					= $db->getAnswersByQuestionId(md5($qrow['question_id']));
			$qtype	 					= $qrow['question_type'];
			$show    					= ($q == 1) ? 'style="display:block"' : 'style="display:none"';
			if ($vqtime == 0):
				$videoq_start_time 		= '00:00';
				$videoq_end_time   		= (isset($qdata[$vqtime])) ? $qdata[$vqtime]['videoq_cue_point'] : '00:00';
			else:
				$new_vtime 				= $vqtime - 1;
				$videoq_start_time 		= (isset($qdata[$new_vtime])) ? $qdata[$new_vtime]['videoq_cue_point'] : '00:00';
				$videoq_end_time   		= (isset($qdata[$vqtime])) ? $qdata[$vqtime]['videoq_cue_point'] : '00:00';
			endif; ?>
            <div class="MatchingQusBannerlunch MatchingQusBanner_<?php echo $q; echo ($q == 1) ? ' ques_1' : ''; ?>" <?php echo $show ?>>
                <div class="Matchingcontainer VideoMatchingcontainer">
					<div class="launchvideopatch" style="display:none;"></div>
                	<div class="videopatch">
						<?php if(!empty($qrow['videoq_cue_point'])){?>
						<video id="qvideo-<?php echo $q ?>" class="video-js vjs-theme-<?php echo $vtheme ?> videoque" webkit-playsinline="" playsinline="" preload="auto" width="640" height="264" data-vtype='single' data-setup="{}" <?php echo ($q == 1) ? 'autoplay="autoplay"' : ''; ?> data-start-time="<?php echo ($videoq_start_time == '00:00' || empty($videoq_start_time)) ? '0' : $db->hmsToSeconds($videoq_start_time); ?>" data-end-time="<?php echo $db->hmsToSeconds($videoq_end_time); ?>" data-type="video" data-qtype="<?php echo $qtype ?>">
							<source src="<?php echo $path . $qrow['videoq_media_file'] ?>" type="video/<?php echo strtolower(pathinfo($qrow['videoq_media_file'], PATHINFO_EXTENSION)); ?>" />
						</video>
						<?php }else{?>
						<video id="qvideo-<?php echo $q ?>" class="video-js vjs-theme-<?php echo $vtheme ?> videoque" webkit-playsinline="" playsinline="" preload="auto" width="640" height="264" data-vtype='multiple' data-setup="{}" <?php echo ($q == 1) ? 'autoplay="autoplay"' : ''; ?> data-start-time="" data-end-time="" data-type="video" data-qtype="<?php echo $qtype ?>">
							<source src="<?php echo $path . $qrow['videoq_media_file'] ?>" type="video/<?php echo strtolower(pathinfo($qrow['videoq_media_file'], PATHINFO_EXTENSION)); ?>" />
						</video>
						<?php }?>
                    </div>
                    <div class="overlay">
						<div class="Questmtf Videoquestion" style="display:none;">
							<div class="MatchingQusText fade-in">
								<div class="qus-text">
									<h3>QUESTION <?php echo $q .'/'. $totalQ; ?></h3>
									<p><?php echo stripslashes($qrow['questions']); ?></p>
								</div>
								<div class="qusassestbox">
									<div class="Qusimg">
									<?php if ( ! empty($qrow['audio'])): ?>
									<audio id="qvideo-<?php echo $q ?>" class="video-js vjs-theme-<?php echo $vtheme ?>" controls preload="auto" width="300px" height="100px" data-setup="{}" data-type="audio" poster="<?php echo $aPoster ?>">
										<source src="<?php echo $path . $qrow['audio'] ?>" type="audio/<?php echo strtolower(pathinfo($qrow['audio'], PATHINFO_EXTENSION)); ?>" />
									</audio>
									<?php elseif ( ! empty($qrow['video'])): ?>
									<a href="<?php echo $path . $qrow['video']; ?>" data-fancybox data-caption="<?php echo stripslashes($qrow['questions']); ?>" class="qassets_icon">
										<video id="qvideo-<?php echo $q ?>" class="video-js vjs-theme-<?php echo $vtheme ?>" controls preload="auto" width="640" height="264" data-setup="{}" data-type="video">
											<source src="<?php echo $path . $qrow['video']; ?>" type="video/<?php echo strtolower(pathinfo($qrow['video'], PATHINFO_EXTENSION)); ?>" />
										</video>
									</a>
									<?php elseif ( ! empty($qrow['screen'])): ?>
									<a href="<?php echo $path . $qrow['screen']; ?>" data-fancybox data-caption="<?php echo stripslashes($qrow['questions']); ?>" class="qassets_icon">
										<video id="qvideo-<?php echo $q ?>" class="video-js vjs-theme-<?php echo $vtheme ?>" controls preload="auto" width="200" height="100" data-setup="{}" data-type="video">
											<source src="<?php echo $path . $qrow['screen']; ?>" type="video/<?php echo strtolower(pathinfo($qrow['screen'], PATHINFO_EXTENSION)); ?>" />
										</video>
									</a>
									<?php elseif ( ! empty($qrow['image'])): ?>
									<a href="<?php echo $path . $qrow['image']; ?>" data-fancybox data-caption="<?php echo stripslashes($qrow['questions']); ?>" class="qassets_icon">
										<img class="img-fluid" src="<?php echo $path . $qrow['image']; ?>">
									</a>
									<?php elseif ( ! empty($qrow['document'])): ?>
									<a href="javascript:;" data-src="<?php echo $path . $qrow['document']; ?>" data-fancybox data-type="iframe" data-caption="<?php echo stripslashes($qrow['questions']); ?>" class="document">
										<img class="img-fluid pdfclissicsIm" src="img/icons/pdf_icon.svg" />
									</a>
									<?php elseif ( ! empty($qrow['speech_text'])): ?>
									<button onclick="speak('div.intro_<?php echo $q ?>')">
										<i id="playicon" class="fa fa-play playicon" aria-hidden="true"></i>
										<i id="pauseicon" class="fa fa-pause pauseicon" style="display:none" aria-hidden="true"></i></button>
										<div class="intro_<?php echo $q ?>" style="display:none;"><?php echo $qrow['speech_text'] ?></div>
									<?php endif; ?>
									</div>
								</div>
							</div>
						</div>
                        <?php #------MCQ------
						if ($qtype == 4): ?>
							<div class="MCQOption bannermcq" id="option_<?php echo $q; ?>" data-qtype="<?php echo $qtype ?>">
								<?php foreach ($ansData as $mcq_data): ?>
								<div class="row options VideoOptions" data-qtype="<?php echo $qtype ?>" data-qid="<?php echo $mcq_data['question_id'] ?>" data-aid="<?php echo $mcq_data['answer_id'] ?>" data-box-id="<?php echo $q; ?>" style="display:none;">
									<?php echo stripslashes($mcq_data['choice_option']); ?>
									<div class="Check-boxQB"><i class="fa fa-check" aria-hidden="true"></i></div>
								</div>
								<?php endforeach; ?>
								<div class="submitMTFcenter col-sm-12 Videoquestion_btn" style="display:none;">
									<button type="button" id="btn_<?php echo $q; ?>" class="btn submitbtn fade-in" data-show-report="<?php echo ($totalQ == $q) ? 'true' : 'false'; ?>" data-target="<?php echo $q + 1 ?>" data-cur="<?php echo $q ?>" data-sim-id="<?php echo $sim_id ?>" data-qid="<?php echo $qrow['question_id'] ?>">Submit</button>
								</div>
							</div>
						<?php #------SWIPING------
                		elseif ($qtype == 7): ?>
							<div class="SwipeOption" id="option_<?php echo $q; ?>" data-qtype="<?php echo $qtype ?>">
								<div class="home-demo">
									<div class="row">
										<div class="large-12 columns">
											<div class="owl-carousel zoomIn fadeIn delay-2s swipingcoursal">
												<?php foreach ($ansData as $swipe_data): ?>
													<div class="swipeoptions swipeVideoOPtion" data-qtype="<?php echo $qtype ?>" data-qid="<?php echo $swipe_data['question_id'] ?>" data-aid="<?php echo $swipe_data['answer_id'] ?>" style="display:none;">
														<?php if ( ! empty($swipe_data['image'])): ?>
															<img class="feedimage" src="<?php echo $path . $swipe_data['image'] ?>" />
														<?php elseif ( ! empty($swipe_data['video'])): ?>
															<video id="my-video" class="video-js vjs-theme-<?php echo $vtheme ?>" controls preload="auto" width="640" height="264">
																<source src="<?php echo $path . $swipe_data['video'] ?>" type="video/<?php echo strtolower(pathinfo($swipe_data['video'], PATHINFO_EXTENSION)); ?>" />
																<p class="vjs-no-js">To view this video please enable JavaScript, and consider upgrading to a web browser that <a href="https://videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a></p>
															</video>
														<?php endif; ?>
														<p><?php echo stripslashes($swipe_data['choice_option']); ?></p>
													</div>
												<?php endforeach; ?>
											</div>
											<div class="submitMTFcenter col-sm-12 Videoquestion_btn" style="display:none;">
												<button type="button" id="btn_<?php echo $q; ?>" class="btn submitbtn zoomIn fadeIn delay-3s" data-show-report="<?php echo ($totalQ == $q) ? 'true' : 'false'; ?>" data-target="<?php echo $q + 1 ?>" data-cur="<?php echo $q ?>" data-sim-id="<?php echo $sim_id ?>" data-qid="<?php echo $qrow['question_id'] ?>">Submit</button>
											</div>
										</div>
									</div>
								</div>
							</div>
                		<?php #------D&D------
						elseif ($qtype == 8): ?>
							<div class="Drag_Drop imgDNDS" id="option_<?php echo $q; ?>" data-qtype="<?php echo $qtype ?>" style="display:none;">
								<div class="row">
									<div class="flex33 DCbanner fade-in">	
										<?php 
										$dnd_choice_sql 		= "SELECT question_id, sub_option, image FROM sub_options_tbl WHERE question_id = '". $qrow['question_id'] ."'";
										$dnd_choice 			= $db->prepare($dnd_choice_sql); $dnd_choice->execute();
										if ($dnd_choice->rowCount() > 0):
											$dnd_choice_data = $dnd_choice->fetchAll(PDO::FETCH_ASSOC);
											foreach ($dnd_choice_data as $dnd_choice_row): ?>
												<div class="flexheight33 multiDBDbanner DndVideoOPtion">
													<?php if ( ! empty($dnd_choice_row['image'])): ?>
														<a href="<?php echo $path . $dnd_choice_row['image']; ?>" data-fancybox data-caption="<?php echo stripslashes($dnd_choice_row['sub_option']); ?>" class="qassets_icon">
														<div class="multiDBDbox"><img src="<?php echo $path . $dnd_choice_row['image']; ?>" />
															<div class="textwraping"><?php echo stripslashes($dnd_choice_row['sub_option']); ?></div>
														</div></a>
													<?php else:?>
														<a href="" data-fancybox data-caption="<?php echo stripslashes($dnd_choice_row['sub_option']); ?>" class="qassets_icon">
														<div class="multiDBDbox">
															<div class="textwraping"><?php echo stripslashes($dnd_choice_row['sub_option']); ?></div>
														</div></a>
													<?php endif; ?>
												</div>
											<?php 
											endforeach; 
										endif; ?>
										<div class="flexheight33 multiDBDbanner BGMUltTrans">
											<?php $dndi = 1; 
											foreach ($dnd_choice_data as $data): ?>
												<div class="dragBox drop dragBox<?php echo $dndi.'_'.$q ?>" data-qtype="<?php echo $qtype ?>" data-qid="<?php echo $data['question_id'] ?>"></div>
												<?php $dndi++; 
											endforeach; ?>
										</div>	
									</div>
									<div class="flex33 DROPclassicbanner videoMaTchboxbanner fade-in">
									<?php $dnd_match_sql = "SELECT answer_id, choice_option, image FROM answer_tbl WHERE question_id = '". $qrow['question_id'] ."' ORDER BY (CASE WHEN choice_order_no != '0' THEN choice_order_no WHEN choice_order_no = 0 THEN RAND() END)";
									$dnd_match = $db->prepare($dnd_match_sql); $dnd_match->execute();
									if ($dnd_match->rowCount() > 0):
									foreach ($dnd_match->fetchAll(PDO::FETCH_ASSOC) as $dnd_match_row): ?>
									<div class="multiDBDbox videoMaTchbox dragdrop" data-aid="<?php echo $dnd_match_row['answer_id'] ?>">
										<?php if ( ! empty($dnd_match_row['image'])): ?><img src="<?php echo $path . $dnd_match_row['image']; ?>" /><?php endif; ?>
										<div class="textwraping"><?php echo stripslashes($dnd_match_row['choice_option']); ?></div>
									</div>
									<?php endforeach; endif; ?>
									</div>
									<div class="submitMTFcenter col-sm-12 Videoquestion_btn" style="display:none;">
										<button type="button" id="btn_<?php echo $q; ?>" class="btn submitbtn fade-in" data-show-report="<?php echo ($totalQ == $q) ? 'true' : 'false'; ?>" data-target="<?php echo $q + 1 ?>" data-cur="<?php echo $q ?>" data-sim-id="<?php echo $sim_id ?>" data-qid="<?php echo $qrow['question_id'] ?>">Submit</button>
									</div>	
								</div>
							</div>				
						<?php endif; ?>
					</div>
                </div>
            </div>

			<?php if ($totalQ == $q): ?>
			<span class="report-view">
                <div class="signbtn1 signbtnRepot" style="display:none;">
                    <div class="repotView"><a href="learner-progress-single-video.php?report=true&sid=<?php echo $sim_id; ?>" target="_blank">
                        <img src="img/list/report_simicon.svg" />VIEW REPORT</a>
                    </div>
                </div>
			</span>
			<?php endif; ?>
			<?php if ($q == 1): ?>
				<script type="text/javascript">
				var qdata 		= document.getElementById('qvideo-1');
				var start_time	= qdata.getAttribute('data-start-time');
				var end_time	= qdata.getAttribute('data-end-time');
				var type 		= qdata.getAttribute('data-type');
				var qtype		= qdata.getAttribute('data-qtype');
				var vtype		= qdata.getAttribute('data-vtype');
				var newplayer 	= videojs('qvideo-1');
				newplayer.on('ended', function() {
					if(qtype == 4){
						$('.MatchingQusBanner_1 .Matchingcontainer .Videoquestion').css({'display':'block'});
						$('.MatchingQusBanner_1 .launchvideopatch').css({'display':'block'});
						$('.MatchingQusBanner_1 .Videoquestion_btn').css({'display':'block'});							
						$('.MatchingQusBanner_1 .MCQOption .VideoOptions').css({'display':'block'}).addClass('advertise_container fade-in');
					}else if(qtype == 8){
						$('.MatchingQusBanner_1 .Matchingcontainer .Videoquestion').css({'display':'block'});
						$('.MatchingQusBanner_1 .launchvideopatch').css({'display':'block'});
						$('.MatchingQusBanner_1 .Videoquestion_btn').css({'display':'block'});							
						$('.MatchingQusBanner_1 .Drag_Drop').css({'display':'block'}).addClass('advertise_container fade-in');
					}else if(qtype == 7){
						$('.MatchingQusBanner_1 .Matchingcontainer .Videoquestion').css({'display':'block'});
						$('.MatchingQusBanner_1 .launchvideopatch').css({'display':'block'});
						$('.MatchingQusBanner_1 .Videoquestion_btn').css({'display':'block'});							
						$('.MatchingQusBanner_1 .SwipeOption .swipeVideoOPtion').css({'display':'block'}).addClass('advertise_container fade-in');
					}
				});
				if(qtype == 4){//MCQ
					if (end_time != '' && type == 'video') {
						var qplayer = videojs('qvideo-1');
						if (start_time == 0) {
							qplayer.on('timeupdate', function(e){
								if (qplayer.currentTime() >= end_time){
									qplayer.pause();
									$('.MatchingQusBanner_1 .Matchingcontainer .Videoquestion').css({'display':'block'});
									$('.MatchingQusBanner_1 .launchvideopatch').css({'display':'block'});
									$('.MatchingQusBanner_1 .Videoquestion_btn').css({'display':'block'});							
									$('.MatchingQusBanner_1 .MCQOption .VideoOptions').css({'display':'block'}).addClass('advertise_container fade-in');
								}
							});
						}
						else if (start_time != 0) {
							qplayer.currentTime(start_time);
							qplayer.play();
							qplayer.on('timeupdate', function(e){
								if (qplayer.currentTime() >= end_time) {
									qplayer.pause();
									$('.MatchingQusBanner_1 .Matchingcontainer .Videoquestion').css({'display':'block'});
									$('.MatchingQusBanner_1 .launchvideopatch').css({'display':'block'});
									$('.MatchingQusBanner_1 .Videoquestion_btn').css({'display':'block'});
									$('.MatchingQusBanner_1 .MCQOption .VideoOptions').css({'display':'block'}).addClass('advertise_container fade-in');
								}
							});
						}
					}
				} else if(qtype == 8){//DND
					if (end_time != '' && type == 'video') {
						var qplayer = videojs('qvideo-1');
						if (start_time == 0) {
							qplayer.on('timeupdate', function(e){
								if (qplayer.currentTime() >= end_time){
									qplayer.pause();
									$('.MatchingQusBanner_1 .Matchingcontainer .Videoquestion').css({'display':'block'});
									$('.MatchingQusBanner_1 .launchvideopatch').css({'display':'block'});
									$('.MatchingQusBanner_1 .Videoquestion_btn').css({'display':'block'});							
									$('.MatchingQusBanner_1 .Drag_Drop').css({'display':'block'}).addClass('advertise_container fade-in');
								}
							});
						}
						else if (start_time != 0) {
							qplayer.currentTime(start_time);
							qplayer.play();
							qplayer.on('timeupdate', function(e){
								if (qplayer.currentTime() >= end_time) {
									qplayer.pause();
									$('.MatchingQusBanner_1 .Matchingcontainer .Videoquestion').css({'display':'block'});
									$('.MatchingQusBanner_1 .launchvideopatch').css({'display':'block'});
									$('.MatchingQusBanner_1 .Videoquestion_btn').css({'display':'block'});
									$('.MatchingQusBanner_1 .Drag_Drop').css({'display':'block'}).addClass('advertise_container fade-in');
								}
							});
						}
					}
				} else if(qtype == 7){//Swipe
					if (end_time != '' && type == 'video') {
						var qplayer = videojs('qvideo-1');
						if (start_time == 0) {
							qplayer.on('timeupdate', function(e){
								if (qplayer.currentTime() >= end_time){
									qplayer.pause();
									$('.MatchingQusBanner_1 .Matchingcontainer .Videoquestion').css({'display':'block'});
									$('.MatchingQusBanner_1 .launchvideopatch').css({'display':'block'});
									$('.MatchingQusBanner_1 .Videoquestion_btn').css({'display':'block'});							
									$('.MatchingQusBanner_1 .SwipeOption .swipeVideoOPtion').css({'display':'block'}).addClass('advertise_container fade-in');
								}
							});
						}
						else if (start_time != 0) {
							qplayer.currentTime(start_time);
							qplayer.play();
							qplayer.on('timeupdate', function(e){
								if (qplayer.currentTime() >= end_time) {
									qplayer.pause();
									$('.MatchingQusBanner_1 .Matchingcontainer .Videoquestion').css({'display':'block'});
									$('.MatchingQusBanner_1 .launchvideopatch').css({'display':'block'});
									$('.MatchingQusBanner_1 .Videoquestion_btn').css({'display':'block'});
									$('.MatchingQusBanner_1 .SwipeOption .swipeVideoOPtion').css({'display':'block'}).addClass('advertise_container fade-in');
								}
							});
						}
					}
				}
				</script>
			<?php endif; ?>
		<?php $q++; $vqtime++; endforeach; endif; ?>
	</div>
</div>

<div id="load_popup_modal_show" class="modal fade" tabindex="-1"></div>

<script src="content/js/articulate.js"></script>
<script src="content/js/owlcarousel/owl.carousel.min.js"></script>
<script src="content/js/launch-sim.js?v=<?php echo time() ?>"></script>
<script type="text/javascript">
	$('.options').click(function() {
		var aid = $(this).data('aid');
		var bid = $(this).data('box-id');
		$('#btn_'+ bid).attr('data-aid', aid);
		$('.options').removeClass('press');
		$(this).addClass('press');
		$('.btn').removeAttr('disabled', true);
	});
	
	//-----DRAG AND DROP----	
	$(function() {
		//draggable
        $('.dragdrop').draggable({
          revert: true,
          placeholder: true,
          droptarget: '.drop',
          drop: function(evt, droptarget) {
			if ($(droptarget).find('.dragdrop').length == 0){
				//== empty drop target, simply drop
				$(this).appendTo(droptarget).draggable();
				$(".flexheight33.multiDBDbanner.BGMUltTrans").addClass("BGMUltTrans1");
			}
			else {
				//== swapping drops
				var d1 = this;
				var d2 = $(droptarget).find('.dragdrop').eq(0);
				$(d2).appendTo($(d1).parent()).draggable();
				$(d1).appendTo(droptarget).draggable();
			}
          }
        });
	});

	//-----------Feedback-----------------
	$('body').on('click', '.submitbtn', function() {
		var cur		= $(this);
		var id  	= cur.data('cur');
		var option	= $('#option_' + id);
		var qtype 	= option.data('qtype');
		var report	= cur.data('show-report');
		var target	= cur.data('target');
		var current	= cur.data('cur');
		var sid		= cur.data('sim-id');
		var url 	= 'view-video-feedback-modal.php';
		var $modal	= $('#load_popup_modal_show');
		
		$.LoadingOverlay("show");
		
		/* MCQ */
		if (qtype == 4) {
			var mcqdata = $('#option_'+ id +' .press');
			if (mcqdata.data('aid')) {
				$modal.load(url, {'report': report, 'target': target, 'current': current, 'sid': sid, 'qid': mcqdata.data('qid'), 'aid': mcqdata.data('aid')}, function(res) {
					$.LoadingOverlay("hide");
					if (res != 'error') {
						$modal.modal('show', {backdrop: 'static', keyboard: false});
					}
					else { swal({text: 'Oops, something went wrong. Please try again later', buttons: false, icon: "error", timer: 1000}); }
				});
			}
			else { swal({text: 'Oops, something went wrong. Please try again later', buttons: false, icon: "error", timer: 1000}); }
		}
		/* SWIPING */
		else if (qtype == 7) {
			var swip = $('#option_'+ id +' .owl-stage .center .swipeoptions');
			if (swip.data('aid')) {
				$modal.load(url, {'report': report, 'target': target, 'current': current, 'sid': sid, 'qid': swip.data('qid'), 'aid': swip.data('aid')}, function(res) {
					$.LoadingOverlay("hide");
					if (res != 'error') {
						$modal.modal('show', {backdrop: 'static', keyboard: false});
					}
					else { swal({text: 'Oops, something went wrong. Please try again later', buttons: false, icon: "error", timer: 1000}); }
				});
			}
			else { swal({text: 'Oops, something went wrong. Please try again later', buttons: false, icon: "error", timer: 1000}); }
		}
		/* D&D */
		else if (qtype == 8) {
			var dd_data = $('#option_'+ id +' .dragBox');
			if (dd_data.children('div').data('aid')){
				$modal.load(url, {'report': report, 'target': target, 'current': current, 'sid': sid, 'qid': dd_data.data('qid'), 'aid': dd_data.children('div').data('aid')}, function(res) {
					$.LoadingOverlay("hide");
					if (res != 'error') {
						$modal.modal('show', {backdrop: 'static', keyboard: false});
					}
					else { swal({text: 'Oops, something went wrong. Please try again later', buttons: false, icon: "error", timer: 1000}); }
				});
			}
			else { swal({text: 'Oops, something went wrong. Please try again later', buttons: false, icon: "error", timer: 1000}); }
		}
	});
</script>
<?php 
require_once 'includes/footer.php';