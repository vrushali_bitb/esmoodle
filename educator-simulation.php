<?php 
session_start();
if (isset($_SESSION['username'])) {
    $user   = $_SESSION['username'];
    $role   = $_SESSION['role'];
    $userid = $_SESSION['userId'];
}
else {
    header('location: index.php');
}
require_once 'config/db.class.php';
require_once 'includes/learners_menu.php';
require_once 'config/pagination.php';
$db					= new DBConnection();
$type_icon_path		= 'img/type_icon/';
$action_icon_path	= 'img/list/'; ?>
<div class="bottomheader">
    <div class="container-fluid">
        <ul class="breadcrumb">
			<?php echo $db->breadcrumbs(); ?>
        </ul>
     </div>	
</div>
<div class="Group_managment">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
            	<div class="usermanage_main">
               		<div class="table-box">
                    	<div class="tableheader clearfix">
                        	<div class="tableheader_left">Evaluate Simulation</div>
                            <div class="tableheader_right">
                            	<div class="Searchbox">
                                	<form method="get">
                                        <input type="serch" class="serchtext" name="searchkey" required>
                                        <div class="input-group-append serchBTN">
                                            <button class="btn btn-Transprate" type="submit"><i class="fa fa-search" aria-hidden="true"></i></button>
                                        </div>
                                    </form>
                                </div>
                                <div class="btn-group btn-shortBY">
                                    <button type="button" class="btn btn-Transprate"><span class="sort">Sort by</span></button>
                                    <button type="button" class="btn btn-Transprate dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img  src="img/down_arrow_white.png"></button>
                                    <div class="dropdown-menu shortBYMenu">                                	
                                    <a class="dropdown-item" href="<?php echo $db->getBaseUrl($db->getFile()); ?>">Default Sorting</a>
                                    <a class="dropdown-item" href="<?php echo $db->getBaseUrl($db->getFile().'?orderby=type'); ?>">Type</a>
                                    <a class="dropdown-item" href="<?php echo $db->getBaseUrl($db->getFile().'?orderby=status'); ?>">Status</a>
                                  </div>
                                </div>
                           </div>
                    	</div>
                  	</div>
                    <div class="usermanage-form1">
                    	<div class="table-responsive">
						<?php 
							#----------Serch----------------
							$searchText = '';
							if (isset($_GET['searchkey']) && ! empty($_GET['searchkey'])):
								extract($_GET);
								$searchText = " AND ( c.category LIKE '%$searchkey%' OR s.Scenario_title LIKE '%$searchkey%' )";
							endif;
							#----------Sort by----------------
							if (isset($_GET['orderby']) && $_GET['orderby'] == 'type'):
								$orderby = "".(( ! empty($searchText)) ? $searchText : " ")." ORDER BY s.author_scenario_type";
							elseif (isset($_GET['orderby']) && $_GET['orderby'] == 'nstatus'):
								$orderby = "".(( ! empty($searchText)) ? $searchText : " ")." ORDER BY s.status";
							else:
								$orderby = "".(( ! empty($searchText)) ? $searchText : " ")." ORDER BY s.scenario_id DESC";
							endif;
							$getGroups = $db->getGroupByEducator("GROUP_CONCAT(DISTINCT group_id SEPARATOR ',') as col", $userid);							
							if ( ! empty($getGroups[0]['col'])):
                                $sim_id = $db->getAssignSimByGroupId($getGroups[0]['col']);
								$sqlr   = "SELECT DISTINCT s.scenario_id, s.Scenario_title, s.assign_start_date, s.assign_end_date, s.assign_reviewer, s.scenario_type, s.scenario_category, s.duration, s.status, s.created_on, s.sim_ques_type, s.update_sim, c.category, st.status_name, t.type_name, t.type_icon 
                                            FROM scenario_master s LEFT JOIN 
                                            category_tbl c ON c.cat_id = s.scenario_category LEFT JOIN 
                                            scenario_status st ON st.sid = s.status LEFT JOIN 
                                            scenario_type t ON t.st_id = s.author_scenario_type 
                                            WHERE s.author_scenario_type != '0' AND s.sim_ques_type = '3' AND s.sim_temp_type = '7' AND s.scenario_id IN ($sim_id) $orderby";
							    $q = $db->prepare($sqlr); $q->execute();
                                $rowCount = $q->rowCount();
                                $count	= ($sqlr) ? $rowCount : 0;
                                $page   = (isset($_REQUEST['page']) && ! empty($_REQUEST['page'])) ? $_REQUEST['page'] : 1;
                                $show  	= 15;
                                $start 	= ($page - 1) * $show;
                                $npagination = getPaginationString($count, $show, $page, 1, basename(__FILE__), '?page=');
                                $sql = $sqlr ." LIMIT $start, $show";
                                $results = $db->prepare($sql); ?>
                                <table class="table table-striped">
                                    <thead class="Theader">
                                        <tr>
                                            <th>Category Name</th>
                                            <th>Simulation Title</th>
                                            <th>Creation Date</th>
                                            <th>Start Date</th>
                                            <th>End Date</th>
                                            <th>Duration in min</th>
                                            <th>Simulation Type</th>
                                            <th>Status</th>
                                            <th>Evaluate</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php if ($results->execute() && $rowCount > 0):
                                        foreach ($results->fetchAll(PDO::FETCH_ASSOC) as $row): ?>
                                        <tr>
                                            <td><?php echo $row['category']; ?></td>
                                            <td title="<?php echo $row['Scenario_title']; ?>"><?php echo $db->truncateText($row['Scenario_title'], 40); ?></td>
                                            <td><?php echo $db->dateFormat($row['created_on'], 'd-M-y'); ?></td>
                                            <td><?php echo ($row['assign_start_date'] != '0000-00-00') ? $db->dateFormat($row['assign_start_date'], 'd-M-y') : '--' ?></td>
                                            <td><?php echo ($row['assign_end_date'] != '0000-00-00') ? $db->dateFormat($row['assign_end_date'], 'd-M-y') : '--' ?></td>
                                            <td><?php echo ( ! empty($row['duration'])) ? $row['duration'] : 0; ?></td>
                                            <td><img src="<?php echo $type_icon_path . $row['type_icon'] ?>" class="type_icon" title="<?php echo $row['type_name'] ?>" /></td>
                                            <td><?php echo $row['status_name']; ?></td>
                                            <td><a href="report-open-response-template.php?add_data=true&sim_id=<?php echo md5($row['scenario_id']) ?>"><img class="svg" src="img/list/create_sim.svg"></a></td>
                                        </tr>
                                        <?php endforeach; else: ?>
                                        <tr><td colspan="9" style="text-align:center"><strong>No data available in database.</strong></td></tr>
                                        <?php endif; ?>
                                    </tbody>
                                </table>
                                <?php else: ?>
                                <table class="table table-striped"><tr><td colspan="9" style="text-align:center"><strong>No data available in database.</strong></td></tr></table>
                                <?php endif; ?>
                            </div>
                            <div class="pull-right">
                                <nav aria-label="navigation"><?php echo ( ! empty($npagination)) ? $npagination : '&nbsp;'; ?></nav>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<style>
a.disabled {
	color: currentColor;
	cursor: not-allowed;
	opacity: 0.2;
	text-decoration: none;
}
.table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th {
    padding: 10px;
}
</style>
<div id="load_popup_modal_show" class="modal fade" tabindex="-1"></div>
<?php 
require_once 'includes/footer.php';
