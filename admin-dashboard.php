<?php 
ob_start();
session_start();
if (isset($_SESSION['username'])) {
  $user   = $_SESSION['username'];
  $role   = $_SESSION['role'];
  $userid = $_SESSION['userId'];
  $cid		= (isset($_SESSION['client_id'])) ? $_SESSION['client_id'] : exit('Oops. something went wrong please try again');
} else {
  header('location: index.php');
}

require_once 'config/db.class.php';
require_once 'includes/learners_menu.php';
$db = new DBConnection();

/* page access only administrator */
($db->checkAccountType() != 3) ? header('location: index') : '';
?>
<style>
  admin-dashboard.Ev2 .supDASHTOP {
      margin: 0px;
  }
  .cls-1 {
      stroke: transparent !important;
  }
  .flexRI.client.clients {
      display: flex;
  }
  .wordwrap {
    white-space: normal;
  }
  .owl-nav {
    display: none;
  }
  .owl-nav {
		display: none;
	}
  .owl-dots {
		display: block;
		margin-top: 5px;
	}
	.owl-theme .owl-dots .owl-dot.active span{		
		background: #000 !important;
	}
	.owl-theme .owl-dots .owl-dot span {
		width: 17px !important;
		height: 17px !important;
		border: 1px solid #000;
		background: transparent !important;
		border-radius:50% !important;
	}
</style>
<link rel="stylesheet" href="content/js/daterangepicker/daterangepicker.css" />
<link rel="stylesheet" href="content/css/owlcarousel/owl.carousel.min.css" />
<link rel="stylesheet" href="content/css/owlcarousel/owl.theme.default.css" />
<div class="admin-dashboard Ev2">
  <div class="container-fluid">
    <div class="L-calender">
      <div class="input-group calenderclient col-sm-3">
        <input type="text" id="reportrange" class="form-control" readonly="readonly" />
        <span class="input-group-addon"><span class="fa fa-calendar"></span></span></div>
    </div>
    <div class="flexItem flexItemsdMinDash admiNDASH">
      <div class="flextlistbaner hometab clearfix owl-carousel owl-theme active">
        <div class="item">
          <div class="flextlistitem">
            <div class="textFlex CAtextflex">
              <div class="clienttextbox">
                <div class="CTUser">USER</div>
                <div class="CTUserNo"><?php echo $db->getTotalUsers(); ?></div>
                <div class="CTGroupBanneR">
                  <div class="CTGroup">Group</div>
                  <div class="CTGroupNo"><?php echo $db->getTotalGroup(); ?></div>
                </div>
              </div>
              <ul class="flexLI clientflexLI">
                <li>Admin <span><?php echo $db->getTotalUsers('admin'); ?></span></li>
                <li>Author <span><?php echo $db->getTotalUsers('author'); ?></span></li>
                <li>Reviewer <span><?php echo $db->getTotalUsers('reviewer'); ?></span></li>
                <li>Educator <span><?php echo $db->getTotalUsers('educator'); ?></span></li>
                <li>Learner <span><?php echo $db->getTotalUsers('learner'); ?></span></li>
              </ul>
            </div>
          </div>
        </div>
        <div class="item">
          <div class="flextlistitem SIMulationT1">
            <div class="textFlex CAtextflex2">
              <div class="col-sm-6">
                <div class="Tright"><?php echo $db->getTotalCategory(); ?></div>
                <div class="A16F">SIMULATION<br>CATEGORIES</div>
              </div>
              <div class="col-sm-6">
                <div class="Tright"><?php echo $db->getTotalScenario(); ?></div>
                <div class="A16F">SIMULATIONS</div>
              </div>
            </div>
          </div>
        </div>
        <div class="item">
          <div class="flextlistitem">
            <div id="top_learner" style="height: 170px; width: 100%;"></div>
          </div>
        </div>
        <div class="item">
          <div class="flextlistitem T1">
            <div id="lead_group" style="height: 170px; width: 100%;"></div>
          </div>
        </div>
        <div class="item">
          <div class="flextlistitem">
            <div id="trand_sim" style="height: 170px; width: 100%;"></div>
          </div>
        </div>
        <div class="item">
          <div class="flextlistitem T1">
            <div id="top_comp" style="height: 170px; width: 100%;"></div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="tab-content">
    <div id="home" class="tab-pane fade in active">
      <div class="adminT">
        <div class="col-sm-2"> <a href="<?php echo $db->getBaseUrl('user-management.php') ?>">
          <div class="dashboard-box colorBoxW"> <img class="img-responsive width70 svg" src="img/dash_icon/manage_user.svg">
            <div class="dash-sub-title">Manage Users</div>
            <div class="dash-msg clearfix">
              <div class="notino"><img class="img-responsive" src="img/massage.png"> <span class="notifi-icon"><?php echo $db->getTotalNewUsers(); ?></span></div>
            </div>
          </div>
          </a> </div>
        <div class="col-sm-2"> <a href="<?php echo $db->getBaseUrl('group-management.php') ?>">
          <div class="dashboard-box colorBoxW"> <img class="img-responsive width70 svg" src="img/dash_icon/manage_group.svg">
            <div class="dash-sub-title">Manage Groups</div>
            <div class="dash-msg clearfix">
              <div class="notino"><img class="img-responsive" src="img/massage.png"> <span class="notifi-icon"><?php echo $db->getTotalGroup(); ?></span></div>
            </div>
          </div>
          </a> </div>
        <div class="col-sm-2"> <a href="<?php echo $db->getBaseUrl('manage-simulations.php') ?>">
          <div class="dashboard-box colorBoxW"> <img class="img-responsive width70 svg" src="img/dash_icon/manage_sim.svg">
            <div class="dash-sub-title">Manage Simulations</div>
            <div class="dash-msg clearfix">
              <div class="notino"> <img class="img-responsive" src="img/massage.png"> <span class="notifi-icon"><?php echo $db->getDraftScenario(2); ?></span></div>
            </div>
          </div>
          </a> </div>
        <div class="col-sm-2"> <a href="<?php echo $db->getBaseUrl('simulation-assignment.php') ?>">
          <div class="dashboard-box colorBoxW"> <img class="img-responsive width70 svg" src="img/dash_icon/assign_sim.svg">
            <div class="dash-sub-title">Assign Simulations</div>
            <div class="dash-msg clearfix">
              <div class="notino"><img class="img-responsive" src="img/massage.png"> <span class="notifi-icon"><?php echo $db->getAssignScenario(); ?></span></div>
            </div>
          </div>
          </a> </div>
        <div class="col-sm-2"> <a href="<?php echo $db->getBaseUrl('email-management.php') ?>">
          <div class="dashboard-box colorBoxW"> <img class="img-responsive width70 svg" src="img/dash_icon/manage_template.svg">
            <div class="dash-sub-title">Manage Templates</div>
            <div class="dash-msg clearfix">
              <div class="notino"><img class="img-responsive" src="img/massage.png"> <span class="notifi-icon"><?php echo $db->getTotalTemplate(); ?></span></div>
            </div>
          </div>
          </a></div>
        <div class="col-sm-2">
          <a href="<?php echo $db->getBaseUrl('category-management.php') ?>">
          <div class="dashboard-box colorBoxW"> <img class="img-responsive width70 svg" src="img/left_menu/category.svg">
            <div class="dash-sub-title">Manage Category</div>
            <div class="dash-msg clearfix"><div class="notino"><img class="img-responsive" src="img/massage.png"> <span class="notifi-icon"><?php echo $db->getTotalCategory(); ?></span></div></div>
          </div>
          </a>
        </div>
        <?php if ( ! empty($mhome) && ! empty($clientData['mhome'])): ?>
        <div class="col-sm-2">
          <a href="<?php echo $db->getBaseUrl('home-page-management.php') ?>">
          <div class="dashboard-box colorBoxW"> <img class="img-responsive width70 svg" src="img/dash_icon/homepage.svg">
            <div class="dash-sub-title">Manage Home Page</div>
            <div class="dash-msg clearfix"><div class="notino"><img class="img-responsive" src="img/massage.png"> <span class="notifi-icon">1</span> </div></div>
          </div>
          </a>
        </div>
        <?php endif; ?>
        <?php if ( ! empty($mbrand) && ! empty($clientData['mbrand'])): ?>
        <div class="col-sm-2">
          <a href="<?php echo $db->getBaseUrl('manage-branding.php') ?>">
            <div class="dashboard-box colorBoxW"> <img class="img-responsive width70 svg" src="img/dash_icon/branding.svg">
              <div class="dash-sub-title">Manage Branding</div>
              <div class="dash-msg clearfix"><div class="notino"><img class="img-responsive" src="img/massage.png"><span class="notifi-icon">1</span></div></div>
            </div>
          </a>
        </div>
        <?php endif; ?>
        <div class="col-sm-2">
          <a href="<?php echo $db->getBaseUrl('admin-report.php') ?>">
          <div class="dashboard-box colorBoxW"> <img class="img-responsive width70 svg" src="img/dash_icon/view_report1.svg">
            <div class="dash-sub-title">View Report</div>
            <div class="dash-msg clearfix">
              <div class="notino"><img class="img-responsive" src="img/massage.png"><span class="notifi-icon">1</span></div>
            </div>
          </div>
          </a>
        </div>
        <div class="col-sm-2">
          <a href="<?php echo $db->getBaseUrl('attention-report.php') ?>">
          <div class="dashboard-box colorBoxW"> <img class="img-responsive width70 svg" src="img/dash_icon/view_report1.svg">
            <div class="dash-sub-title">View Attention Report</div>
            <div class="dash-msg clearfix">
              <div class="notino"><img class="img-responsive" src="img/massage.png"><span class="notifi-icon">1</span></div>
            </div>
          </div>
          </a>
        </div>
      </div>
    </div>
  </div>
</div>
<script src="content/js/daterangepicker/moment.min.js"></script>
<script src="content/js/daterangepicker/daterangepicker.js"></script>
<script src="content/js/canvasjs.min.js"></script>
<script src="content/js/owlcarousel/owl.carousel.min.js"></script>
<script>
  $('.owl-carousel').owlCarousel({
		loop:false,
		margin:5,
		nav:true,
		mouseDrag:false,
		responsive:{
			0:{
				items:1
			},
			700:{
				items:3
			},
			1200:{
				items:6
			}
		}
	});
  
  var start = moment().subtract(1, 'days');
  var end   = moment();
  
  function cb(start, end) {
    $('#reportrange span').html(start.format('YYYY-MM-DD') + ' - ' + end.format('YYYY-MM-DD'));
  }
  
  $('#reportrange').daterangepicker({
    startDate: start,
    endDate: end,
    locale: { format: 'YYYY-MM-DD' },
    ranges: {
      'Today': [moment(), moment()],
      'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
      'Last 7 Days': [moment().subtract(6, 'days'), moment()],
      'Last 30 Days': [moment().subtract(29, 'days'), moment()],
      'This Month': [moment().startOf('month'), moment().endOf('month')],
      'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
    } }, cb);
  cb(start, end);
  
  function defaultGraphData(date){
    CanvasJS.addColorSet("colorcode", ["#6d3073", "#3b602d", "#c07744", "#678087", "#678087"]);
    /* TOP LEARNER */
    $("#top_learner").LoadingOverlay("show");
    //$("#lead_group").LoadingOverlay("show");
    $("#trand_sim").LoadingOverlay("show");
    $("#top_comp").LoadingOverlay("show");
    $.ajax({
      type: "POST",
      url: 'includes/client-admin-dash-grid-data.php',
      data: 'getTopLearner='+ true + '&date='+ date,
      cache: true,
      success: function (resdata) {
        $("#top_learner").LoadingOverlay("hide", true);
        var res = $.parseJSON(resdata);
        if (res.success == true) {
          if (res.data.learner.length) {
            var learnerChart = new CanvasJS.Chart("top_learner", {
                colorSet: "colorcode",
                title: { text: "Top 5 Learners"},
                animationEnabled: true,
                theme: "light2",
                backgroundColor: '#fff',
                axisX: { gridThickness: 0, tickLength: 0, lineThickness: 0, labelFontColor: "black", labelFormatter: function() { return " "; } },
                axisY: { gridThickness: 0, tickLength: 0, lineThickness: 0, labelFormatter: function() { return " "; } },
                toolTip: { content: "<div class='wordwrap'>{label}: {s}: {y} / {p}</div>" },
                data: [{
                  type: "column",
                  indexLabel: "{y}",
                  indexLabelPlacement: "inside",
                  indexLabelFontWeight: "bolder",
                  indexLabelFontColor: "white",
                  dataPoints: res.data.learner
                }]
              });
              learnerChart.render();
          }
        }
      },error: function() {
        swal({text: 'Oops. something went wrong please try again.', buttons: false, icon: "error", timer: 2000});
      }
    });

    /* TOP COMPETENCE */
    $.ajax({
      type: "POST",
      url: 'includes/client-admin-dash-grid-data.php',
      data: 'getTopCompData='+ true + '&date='+ date,
      cache: true,
      success: function (resdata) {
        $("#top_comp").LoadingOverlay("hide", true);
        var res = $.parseJSON(resdata);
        if (res.success == true) {
          if (res.data.competency.length) {
            var topCompChart = new CanvasJS.Chart("top_comp", {
                colorSet: "colorcode",
                title: { text: "Top Competencies"},
                animationEnabled: true,
                theme: "light2",
                backgroundColor: '#fff',
                axisX: { gridThickness: 0, tickLength: 0, lineThickness: 0, labelFontColor: "black", labelFormatter: function() { return " "; } },
                axisY: { gridThickness: 0, tickLength: 0, lineThickness: 0, labelFormatter: function() { return " "; } },
                toolTip: { content: "<div class='wordwrap'>{name}: {y}</div>" },
                data: [{
                  type: "doughnut",
                  indexLabel: "{y}",
                  indexLabelPlacement: "inside",
                  indexLabelFontWeight: "bolder",
                  indexLabelFontColor: "white",
                  dataPoints: res.data.competency
                }]
              });
              topCompChart.render();
          }
        }
      },error: function() {
        swal({text: 'Oops. something went wrong please try again.', buttons: false, icon: "error", timer: 2000});
      }
    });
    
    /* TRENDING SIM */
    $.ajax({
      type: "POST",
      url: 'includes/client-admin-dash-grid-data.php',
      data: 'getTrendSimData='+ true + '&date='+ date,
      cache: true,
      success: function (resdata) {
        $("#trand_sim").LoadingOverlay("hide", true);
        var res = $.parseJSON(resdata);
        if (res.success == true) {
          if (res.data.sim.length) {
            var trendingSimChart = new CanvasJS.Chart("trand_sim", {
                colorSet: "colorcode",
                title: { text: "Trending Simulations"},
                animationEnabled: true,
                theme: "light2",
                backgroundColor: '#fff',
                axisX: { gridThickness: 0, tickLength: 0, lineThickness: 0, labelFontColor: "black", labelFormatter: function() { return " "; } },
                axisY: { gridThickness: 0, tickLength: 0, lineThickness: 0, labelFormatter: function() { return " "; } },
                toolTip: { content: "<div class='wordwrap'>{label}: {y}</div>" },
                data: [{
                  type: "area",
                  indexLabel: "{y}",
                  indexLabelPlacement: "inside",
                  indexLabelFontWeight: "bolder",
                  indexLabelFontColor: "black",
                  dataPoints: res.data.sim
                }]
              });
              trendingSimChart.render();
          }
        }
      },error: function() {
        swal({text: 'Oops. something went wrong please try again.', buttons: false, icon: "error", timer: 2000});
      }
    });
    
    /* LEADING GROUPS */
    /* $.ajax({
      type: "POST",
      url: 'includes/client-admin-dash-grid-data.php',
      data: 'getLeadGroupData='+ true + '&date='+ date,
      cache: true,
      success: function (resdata) {
        var res = $.parseJSON(resdata);
        if (res.success == true) {
          $("#lead_group").LoadingOverlay("hide", true);
          if (res.data.group.length) {
            var groupChart = new CanvasJS.Chart("lead_group", {
                colorSet: "colorcode",
                title: { text: "Leading Groups"},
                animationEnabled: true,
                theme: "light2",
                backgroundColor: '#fff',
                axisX: { gridThickness: 0, tickLength: 0, lineThickness: 0, labelFontColor: "black", labelFormatter: function() { return " "; } },
                axisY: { gridThickness: 0, tickLength: 0, lineThickness: 0, labelFormatter: function() { return " "; } },
                toolTip: { content: "<div class='wordwrap'>{name}: {y}</div>" },
                data: [{
                  type: "line",
                  indexLabel: "{y}",
                  indexLabelPlacement: "inside",
                  indexLabelFontWeight: "bolder",
                  indexLabelFontColor: "black",
                  dataPoints: res.data.group
                }]
              });
              groupChart.render();
          }
        }
      },error: function() {
        swal({text: 'Oops. something went wrong please try again.', buttons: false, icon: "error", timer: 2000});
      }
    }); */
  }
  
  var date = $('#reportrange').val();

  defaultGraphData(date);
  
  $('#reportrange').on('change', function(){
	  var date = $(this).val();
	  $.LoadingOverlay("show");
	  defaultGraphData(date);
    $.LoadingOverlay("hide");
  });
</script>
<?php 
require_once 'includes/footer.php';
