<?php 
$input_id	= (isset($_POST['data-id'])) ? $_POST['data-id'] : '';
$assets_id	= (isset($_POST['assets-id'])) ? $_POST['assets-id'] : '';
$video_id	= (isset($_POST['video'])) ? $_POST['video'] : '';
?>

<link href="videojs/node_modules/video.js/dist/video-js.min.css" rel="stylesheet" />
<link href="videojs/dist/css/videojs.record.css" rel="stylesheet" />

<script src="videojs/node_modules/video.js/dist/video.min.js"></script>
<script src="videojs/node_modules/recordrtc/RecordRTC.js"></script>
<script src="videojs/node_modules/webrtc-adapter/out/adapter.js"></script>
<script src="videojs/dist/videojs.record.js"></script>
<script src="videojs/examples/browser-workarounds.js"></script>
<style>
  /* change player background color */
  #myScreenAudio {
      background-color: #9ab87a;
  }
</style>
<div id="load_popup_modal_contant" class="temppoupMOdel" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close stopRecord" data-dismiss="modal">&times;</button>
                <h3 class="modal-title">RECORD SCREEN</h3>
            </div>
            <div class="modal-body">
				<video id="myScreenAudio" playsinline class="video-js vjs-default-skin"></video>
                <button type="button" class="btn1 submitbtn1 upload action" disabled="disabled">Insert</button>
                <button type="button" class="btn1 submitbtn1 delete_rec_screen action" data-rec-screen="" title="Delete Record Screen" disabled="disabled">Remove</button>
                <button type="button" class="btn1 submitbtn1 stopRecord" data-dismiss="modal">Cancel</button>
                </div>                
            </div>
        </div>
     </div>
</div>
<script type="text/javascript">
var inputId  = '#<?php echo $input_id; ?>';
var assetsId = '<?php echo $assets_id; ?>';

var options = {
    controls: true,
    width: 565,
    height: 300,
    fluid: false,
    controlBar: {
        volumePanel: true,
        fullscreenToggle: true
    },
    plugins: {
        record: {
            audio: true,
            screen: true,
            maxLength: 10,
            debug: true,
            audioEngine: 'recordrtc',
			videoMimeType: 'video/webm;codecs=vp8,opus'
        }
    }
};

// apply some workarounds for opera browser
applyVideoWorkaround();
applyScreenWorkaround();

var player = videojs('myScreenAudio', options, function() {
    // print version information at startup
    var msg = 'Using video.js ' + videojs.VERSION +
        ' with videojs-record ' + videojs.getPluginVersion('record') +
        ' and recordrtc ' + RecordRTC.version;
    videojs.log(msg);
});

// error handling
player.on('deviceError', function() {
    console.log('device error:', player.deviceErrorCode);
});

player.on('error', function(element, error) {
    console.error(error);
});

// user clicked the record button and started recording
player.on('startRecord', function() {
    console.log('started recording!');
	$('.upload').removeAttr('disabled', true);
});

// stop device stream only
$('.stopRecord').on('click', function(){
	player.record().stopStream();
});

// user completed recording and stream is available
player.on('finishRecord', function() {
    // the blob object contains the recorded data that
    // can be downloaded by the user, stored on server etc.
    //console.log('screen+audio capture ready: ', player.recordedData);
	$('.action').removeAttr('disabled', true);
	
	$('.upload').on('click', function(e) {
		e.preventDefault();
		e.stopImmediatePropagation();
		var data = player.recordedData;
		$('.upload').attr('disabled', true);
		var serverUrl	= 'includes/upload.php';
    	var formData	= new FormData();
    	formData.append('file', data, data.name);
    	formData.append('rec_screen', '1');
		console.log('uploading recording:', data.name);
		$.ajax({
            url: serverUrl,
            method: 'POST',
            data: formData,
            cache: false,
            processData: false,
            contentType: false,
            success: function (resdata) {
				var res = $.parseJSON(resdata);
				if (res.success == true) {
					$(inputId).val(res.file_name);
					<?php if ($video_id): ?>
						var video	= assetsId.split('_');
						var videoQ	= video[0];
						var videoID	= video[2];
						$('.'+ assetsId).addClass("disabled").attr('disabled', true);
						$('.'+ videoQ +'_assets_container_'+ videoID).show();
						$('.'+ videoQ +'_assets_data_'+ videoID).text(res.file_name);
						$('.'+ videoQ +'_assets_container_'+ videoID +' a').attr('data-assets', res.file_name);
						$('.'+ videoQ +'_assets_container_'+ videoID +' a').attr('data-assets-id', assetsId);
						$('.'+ videoQ +'_assets_container_'+ videoID +' a').attr('data-input-id', inputId);
						$('.'+ videoQ +'_assets_container_'+ videoID +' a').attr('data-path', res.path);
					<?php else: ?>
						$('.'+ assetsId).addClass("disabled").attr('disabled', true);
						$('.'+ assetsId + '_container').show();
						$('.'+ assetsId + '_data').text(res.file_name);
						$('.'+ assetsId + '_container a').attr('data-assets', res.file_name);
						$('.'+ assetsId + '_container a').attr('data-assets-id', assetsId);
						$('.'+ assetsId + '_container a').attr('data-input-id', inputId);
						$('.'+ assetsId + '_container a').attr('data-path', res.path);
					<?php endif; ?>
					$('.delete_rec_screen').attr('data-rec-screen', res.file_name);
					swal({text: res.msg, buttons: false, icon: "success", timer: 2000});
				}
				else if (res.success == false) {
					swal({text: res.msg, buttons: false, icon: "warning", timer: 2000});
				}
			},error: function() {
				swal({text: 'Oops. something went wrong please try again.', buttons: false, icon: "error", timer: 2000 });
			}
        });
	});
	
	// stop device stream only
	$('.stopRecord').on('click', function(){
		player.record().stopStream();
	});
});

$('.delete_rec_screen').on('click', function () {
	var file_name	= $(this).attr("data-rec-screen");
	var dataString	= 'delete='+true+'&rec_screen='+file_name;
	if (file_name) {
		swal({
			title: "Are you sure?",
			text: "Delete this Record Screen.",
			icon: "warning",
			buttons: [true, 'Delete'],
			dangerMode: true, }).then((willDelete) => { if (willDelete) {
				$.LoadingOverlay("show");
				$.ajax({
					url: "includes/delete-file.php",
					type: "POST",
					data: dataString,
					cache: false,
					success: function(resdata) {
						var res = $.parseJSON(resdata);
						if (res.success == false) {
							$.LoadingOverlay("hide");
							swal({text: res.msg, buttons: false, icon: "error", timer: 2000});
						}
						else if (res.success == true) {
							$(inputId).val('');
							$('.action').attr('disabled', true);
							$('.delete_rec_screen').attr('data-rec-screen', '');
							$('.'+ assetsId).removeClass("disabled").removeAttr('disabled', true);
							$('.'+ assetsId + '_container').hide();
							$('.'+ assetsId + '_data').text('');
							$('.'+ assetsId + '_container a').attr('data-assets', '');
							$('.'+ assetsId + '_container a').attr('data-assets-id', '');
							$('.'+ assetsId + '_container a').attr('data-input-id', '');
							$.LoadingOverlay("hide");
							swal({text: res.msg, buttons: false, icon: "success", timer: 2000});
						}
					},error: function() {
						swal({text: 'Oops. something went wrong please try again.', buttons: false, icon: "error", timer: 2000});
					}
				});
			} else { swal({text: 'Your Record Screen is safe', buttons: false, timer: 2000}); }
		});
	}
});
</script>
